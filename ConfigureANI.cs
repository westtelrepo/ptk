using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Experient.E911
{
	/// <summary>
	/// Summary description for ConfigureANI.
	/// </summary>
	public class ConfigureANI : BaseForm
	{
		private System.Windows.Forms.Panel panel1;
		private Button btnClose;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private Button btnSet;
		public DateTimePicker timeEdit1;
		public DateTimePicker dateEdit1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ConfigureANI()
		{
			InitializeComponent();
			this.Text = "Set Date and Time";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigureANI));
            this.panel1 = new System.Windows.Forms.Panel();
            this.timeEdit1 = new System.Windows.Forms.DateTimePicker();
            this.dateEdit1 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSet = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.timeEdit1);
            this.panel1.Controls.Add(this.dateEdit1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(7, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(180, 68);
            this.panel1.TabIndex = 0;
            // 
            // timeEdit1
            // 
            this.timeEdit1.CustomFormat = "h:mm:ss tt";
            this.timeEdit1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeEdit1.Location = new System.Drawing.Point(67, 31);
            this.timeEdit1.Name = "timeEdit1";
            this.timeEdit1.ShowUpDown = true;
            this.timeEdit1.Size = new System.Drawing.Size(104, 20);
            this.timeEdit1.TabIndex = 10;
            this.timeEdit1.Value = new System.DateTime(2008, 8, 26, 0, 0, 0, 0);
            // 
            // dateEdit1
            // 
            this.dateEdit1.CustomFormat = "MM/dd/yyyy";
            this.dateEdit1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateEdit1.Location = new System.Drawing.Point(66, 6);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Size = new System.Drawing.Size(104, 20);
            this.dateEdit1.TabIndex = 9;
            this.dateEdit1.Value = new System.DateTime(2005, 5, 2, 0, 0, 0, 0);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 21);
            this.label2.TabIndex = 8;
            this.label2.Text = "New Time:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "New Date:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(118, 80);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(70, 22);
            this.btnClose.TabIndex = 14;
            this.btnClose.Text = "Cancel";
            // 
            // btnSet
            // 
            this.btnSet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSet.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSet.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSet.Location = new System.Drawing.Point(42, 80);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(70, 22);
            this.btnSet.TabIndex = 13;
            this.btnSet.Text = "Set";
            this.btnSet.Click += new System.EventHandler(this.btnSet_Click);
            // 
            // ConfigureANI
            // 
            this.AcceptButton = this.btnSet;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(194, 107);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSet);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "ConfigureANI";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Set Date & Time";
            this.Activated += new System.EventHandler(this.ConfigureANI_Activated);
            this.Enter += new System.EventHandler(this.ConfigureANI_Enter);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private void btnSet_Click(object sender, System.EventArgs e)
		{
		
		}

		private void ConfigureANI_Enter(object sender, System.EventArgs e)
		{
			

		}

		private void ConfigureANI_Activated(object sender, System.EventArgs e)
		{
			dateEdit1.Value = timeEdit1.Value = DateTime.Now;
		}
	}
}
