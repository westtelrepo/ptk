using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Xml;

namespace Experient.E911
{
	/// <summary>
	/// Summary description for MainForm.
	/// </summary>
	public class TDDMessages : System.Windows.Forms.Form // BaseForm
	{

        public delegate void OnTDDMessagesClosing();
        public event OnTDDMessagesClosing TDDMessagesClosing;

        public delegate void OnTDDMessageSelected(string Message);
        public event OnTDDMessageSelected TDDMessageSelected;


		ArrayList messages = new ArrayList();
		Hashtable languages = new Hashtable();
		Controller controller;
		private static int WM_QUERYENDSESSION = 0x11;

		#region Windows Control Declaraitons

        private System.Windows.Forms.ListBox lbMessages;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cbCategory;
        private Button btnSelectMessage;
        private Button btnCancel;
#endregion

		#region Constructor / Deconstructor
		public TDDMessages(Controller c)
		{
			controller = c;
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
            LoadMessages();
		}

		#endregion

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TDDMessages));
            this.cbCategory = new System.Windows.Forms.ComboBox();
            this.lbMessages = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSelectMessage = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbCategory
            // 
            this.cbCategory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCategory.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCategory.Items.AddRange(new object[] {
            "- All -",
            "PSAP",
            "Police Departments",
            "Fire Departments",
            "Towing"});
            this.cbCategory.Location = new System.Drawing.Point(7, 269);
            this.cbCategory.Name = "cbCategory";
            this.cbCategory.Size = new System.Drawing.Size(340, 22);
            this.cbCategory.TabIndex = 7;
            this.cbCategory.SelectedIndexChanged += new System.EventHandler(this.cbCategory_SelectedIndexChanged);
            this.cbCategory.Click += new System.EventHandler(this.cbCategory_Click);
            this.cbCategory.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cbCategory_KeyUp);
            // 
            // lbMessages
            // 
            this.lbMessages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbMessages.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMessages.ItemHeight = 16;
            this.lbMessages.Location = new System.Drawing.Point(7, 7);
            this.lbMessages.Name = "lbMessages";
            this.lbMessages.Size = new System.Drawing.Size(340, 228);
            this.lbMessages.TabIndex = 5;
            this.lbMessages.SelectedIndexChanged += new System.EventHandler(this.lbMessages_SelectedIndexChanged);
            this.lbMessages.DoubleClick += new System.EventHandler(this.lbMessages_DoubleClick);
            this.lbMessages.KeyUp += new System.Windows.Forms.KeyEventHandler(this.lbMessages_KeyUp);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 253);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(340, 16);
            this.label1.TabIndex = 8;
            this.label1.Text = "Select Language:";
            // 
            // btnSelectMessage
            // 
            this.btnSelectMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectMessage.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectMessage.Location = new System.Drawing.Point(178, 301);
            this.btnSelectMessage.Name = "btnSelectMessage";
            this.btnSelectMessage.Size = new System.Drawing.Size(104, 23);
            this.btnSelectMessage.TabIndex = 9;
            this.btnSelectMessage.Text = "Select Message";
            this.btnSelectMessage.Click += new System.EventHandler(this.btnSelectMessage_Click);
            this.btnSelectMessage.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btnSelectMessage_KeyUp);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(288, 301);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(59, 23);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // TDDMessages
            // 
            this.AcceptButton = this.btnSelectMessage;
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(354, 334);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSelectMessage);
            this.Controls.Add(this.cbCategory);
            this.Controls.Add(this.lbMessages);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TDDMessages";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Closing += new System.ComponentModel.CancelEventHandler(this.TDDMessages_Closing);
            this.Load += new System.EventHandler(this.TDDMessages_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TDDMessages_KeyDown);
            this.ResumeLayout(false);

		}
		#endregion


		public void LoadMessages()
		{
			try
			{
				cbCategory.Items.Clear();
				lbMessages.Items.Clear();
				cbCategory.Items.Add("- All -");
				cbCategory.SelectedIndex = 0;

                if (controller.settings.TDDMessages != null)
                {
                    foreach (XmlNode node in controller.settings.TDDMessages.ChildNodes)
				    {
					    string text     = node.Attributes[0].Value;
					    string category = node.Attributes[1].Value;

					    messages.Add(new Mrec(text, category));

					    if (!cbCategory.Items.Contains(category)) cbCategory.Items.Add(category);

					    lbMessages.Items.Add(text);
				    }
                }
				
                object defaultLanguage = controller.settings.TDDSettings["DefaultLanguage"];
                if (defaultLanguage == null) defaultLanguage = "English";

                cbCategory.SelectedItem = defaultLanguage.ToString();

				lbMessages.Focus();
			}
			catch (Exception ex)
			{
				Logging.ExceptionLogger("Error in TDDMessages.ReadData", ex, Console.LogID.TDD);

				string eMessage = "There was an error reading in TDDMessages.xml:" + Environment.NewLine + ex.Message;
				
				if (ex.InnerException != null)
					eMessage += Environment.NewLine + "More Info:" + ex.InnerException.Message;

				MessageBox.Show(this, eMessage, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
			}

			if (lbMessages.Items.Count > 0)
			{
				lbMessages.SelectedIndex = 0;
				btnSelectMessage.Enabled = true;
			}
			else
            {
				btnSelectMessage.Enabled = false;
            }
		}


		#region Form Event Handlers
		private void TDDMessages_Load(object sender, System.EventArgs e)
		{
            if (Owner != null)
            {
                Location = new System.Drawing.Point((Owner.Width  - Width)  / 2 + Owner.Left,
                                                    (Owner.Height - Height) / 2 + Owner.Top);
            }

			this.Text = "TDD Messages";
			lbMessages.Focus();
		}

		private void TDDMessages_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			e.Cancel = true;
			Hide();
            if (TDDMessagesClosing != null) TDDMessagesClosing(); // Inform app that the window is closing
			// tddForm.MessageClosed();
		}

		#endregion
			
		#region Combobox Event Handlers
		private void cbCategory_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cbCategory.SelectedIndex == 0)
			{
				lbMessages.Items.Clear();
				foreach(Mrec mr in messages)
					lbMessages.Items.Add(mr.Text);

				//lbMessages.Focus();
			}
			else
			{
				string SelectedCategory = cbCategory.Text;
				lbMessages.Items.Clear();
				foreach(Mrec mr in messages)
				{
					if (mr.Category == SelectedCategory)
						lbMessages.Items.Add(mr.Text);
				}
			}
			
			if (lbMessages.Items.Count > 0)
			{
				lbMessages.SelectedIndex = 0;
				btnSelectMessage.Enabled = true;
			}
			else 
				btnSelectMessage.Enabled = false;

			lbMessages.Focus();
		}

		#endregion

		#region ListBox Event Handlers
		private void lbMessages_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter) btnSelectMessage.Focus();
		}
		private void lbMessages_DoubleClick(object sender, System.EventArgs e)
		{
			btnSelectMessage.PerformClick();
		}
		#endregion
		
		#region Button Event Handlers
		private void btnSelectMessage_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter && lbMessages.SelectedValue != null)
				btnSelectMessage.PerformClick();
		}

		private void btnSelectMessage_Click(object sender, System.EventArgs e)
		{
			if (lbMessages.SelectedItem != null)
			{
                TDDMessageSelected(lbMessages.SelectedItem.ToString());
				//tddForm.Message = lbMessages.SelectedItem.ToString();
			}
			//MessageBox.Show("Message:" + lbMessages.SelectedItem);
			Close();
		}

		#endregion

		private void cbCategory_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			//if (e.KeyCode == Keys.Enter) lbMessages.Focus();
		}

		private void lbMessages_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			object item = lbMessages.SelectedItem;
			btnSelectMessage.Enabled = (item != null);

		}

		protected override void WndProc(ref Message m)
		{
			if (m.Msg == WM_QUERYENDSESSION)
			{
                if (controller.OkToStop) Controller.ExitApplication(controller);
			}
			base.WndProc (ref m);
		}

		private void cbCategory_Click(object sender, System.EventArgs e)
		{
			//lbMessages.Focus();
		}

		private void TDDMessages_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Escape) 
			{
				e.Handled = true;
				Close();
			}
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}
	}

	public struct Mrec
	{
		public string Text;
		public string Category;

		public Mrec(string text, string category)
		{
			Text = text;
			Category = category;
		}
	}
}
