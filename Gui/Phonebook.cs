using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace WestTel.E911
{
	/// <summary>
	/// Applet Form for PBX transfering.
	/// </summary>
	public class Phonebook : BaseForm
	{
        private class DialButton : Button
        {
            override protected void OnPaint(PaintEventArgs args)
            {
                base.OnPaint(args);

                Graphics  graphics = args.Graphics;
                Rectangle rect     = this.DisplayRectangle;

                StringFormat stringFormat = new StringFormat();

                switch (TextAlign)
                {
                    case ContentAlignment.TopLeft:      stringFormat.Alignment = StringAlignment.Near;   stringFormat.LineAlignment = StringAlignment.Near;   break;
                    case ContentAlignment.TopCenter:    stringFormat.Alignment = StringAlignment.Center; stringFormat.LineAlignment = StringAlignment.Near;   break;
                    case ContentAlignment.TopRight:     stringFormat.Alignment = StringAlignment.Far;    stringFormat.LineAlignment = StringAlignment.Near;   break;
                    case ContentAlignment.MiddleLeft:   stringFormat.Alignment = StringAlignment.Near;   stringFormat.LineAlignment = StringAlignment.Center; break;
                    case ContentAlignment.MiddleCenter: stringFormat.Alignment = StringAlignment.Center; stringFormat.LineAlignment = StringAlignment.Center; break;
                    case ContentAlignment.MiddleRight:  stringFormat.Alignment = StringAlignment.Far;    stringFormat.LineAlignment = StringAlignment.Center; break;
                    case ContentAlignment.BottomLeft:   stringFormat.Alignment = StringAlignment.Near;   stringFormat.LineAlignment = StringAlignment.Center; break;
                    case ContentAlignment.BottomCenter: stringFormat.Alignment = StringAlignment.Center; stringFormat.LineAlignment = StringAlignment.Far;    break;
                    case ContentAlignment.BottomRight:  stringFormat.Alignment = StringAlignment.Far;    stringFormat.LineAlignment = StringAlignment.Far;    break;
                    default:                            stringFormat.Alignment = StringAlignment.Center; stringFormat.LineAlignment = StringAlignment.Far;    break;
                }

                rect.Y      += Padding.Top;
                rect.Height -= Padding.Bottom;
                rect.X      += Padding.Left;
                rect.Width  -= Padding.Right;

                string[] sep = {"\r\n"};
                string[] strings = Tag.ToString().Split(sep, StringSplitOptions.None);

                Font boldFont = new Font(Font.FontFamily, Font.Size + 8, FontStyle.Bold, Font.Unit);

                int nStrings = strings.GetLength(0);

                if (nStrings == 1)
                {
                    graphics.DrawString(strings[0], boldFont, new SolidBrush(ForeColor), rect, stringFormat);
                }
                else
                {
                    graphics.DrawString(strings[0] + "\r\n ", boldFont, new SolidBrush(ForeColor), rect, stringFormat);
                    graphics.DrawString(" \r\n" + strings[1], Font,     new SolidBrush(ForeColor), rect, stringFormat);
                }
            }
        }

        private Dictionary<string,ArrayList> allEntries = new Dictionary<string,ArrayList>();
		public  Controller controller = null;

        private string                  currentCallId       = null;
        private Controller.TransferType currentTransferType = Controller.TransferType.Dial;

        private static System.Windows.Forms.Form confirmCancelDialog = null;
		
		Brush printerBrush;
		Font printerFont;
		int fontSize = 10;
		float LinePosition = 10;
		float LeftMargin = 30;
		float TopMargin = 50;


		// bool isStarting = true;
		bool systemShutdown = false;
		
		private string DefaultGroup = "All";
		private static int WM_QUERYENDSESSION = 0x11;

        private static int maxGroupStrWidth = 140;
        private static int maxNameStrWidth  = 140;

        public static bool showAttendedTransferButton   = true;
        public static bool showTandemTransferButton     = true;
        public static bool showBlindTransferButton      = true;
        public static bool showConferenceTransferButton = true;
        public static bool showFlashTransferButton      = true;
        public static bool showSpeedDialButton          = true;
        public static bool showDialPadButton            = true;
        public static bool showPhonebookAsButtons       = false;

        public bool blindEnabled      = false;
        public bool attendedEnabled   = false;
        public bool conferenceEnabled = false;
        public bool tandemEnabled     = false;
        public bool flashEnabled      = false;

        public bool transferEnabled = false;

        private AutoCompleteStringCollection dialHistory;
        private Microsoft.Win32.RegistryKey regSettings = null;

        private static int iconSpace = 20;
        private static int iconSize  = 16;

		#region Windows Control Declaraitons

        private MenuStrip MainMenu;
        private ToolStrip toolBar;
        private System.ComponentModel.IContainer components;
        private ToolTip toolTip;
        private ToolStripButton printButton;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem printMenu;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem closeMenu;
        private ToolStripMenuItem helpToolStripMenuItem;
        private ToolStripMenuItem aboutMenu;
        private ToolStripMenuItem usersManualMenu;
        private ToolStripSeparator helpMenuSep;
		private System.Windows.Forms.ListBox directoryListBox;
		private System.Drawing.Printing.PrintDocument printDoc;
        private System.Windows.Forms.PrintDialog printSetup;
        private ToolStripMenuItem transferMenu;
        private ToolStripMenuItem blindMenu;
        private ToolStripMenuItem attendedMenu;
        private ToolStripMenuItem conferenceMenu;
        private ToolStripMenuItem tandemMenu;
        private ToolStripMenuItem flashMenu;
        private ToolStripMenuItem speedDialMenu;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripSeparator toolStripSeparator4;
        private ToolStripButton tandemButton;
        private ToolStripButton blindButton;
        private ToolStripButton attendedButton;
        private ToolStripButton conferenceButton;
        private ToolStripButton flashButton;
        private ToolStripSeparator toolStripSeparator5;
        private ToolStripSeparator toolStripSeparator6;
        private ToolStripSeparator toolStripSeparator2;
        private TabControl tabControl;
        private TabPage directoryTab;
        private TabPage dialTab;
        //private ComboBox phoneField;
        private TextBox phoneField;
        private TableLayoutPanel dialLayoutPanel;
        private Phonebook.DialButton button9;
        private Phonebook.DialButton buttonAsterisk;
        private Phonebook.DialButton button7;
        private Phonebook.DialButton button5;
        private Phonebook.DialButton button6;
        private Phonebook.DialButton button2;
        private Phonebook.DialButton button1;
        private Phonebook.DialButton button4;
        private Phonebook.DialButton button3;
        private Phonebook.DialButton button0;
        private Phonebook.DialButton buttonPound;
        private Phonebook.DialButton button8;
        private Button backButton;
        private SplitContainer splitContainer1;
        private TreeView directoryTreeView;
        private ToolStripButton speedDialButton;
        private StatusStrip statusStrip;

        #endregion

        public static Settings.ColorInfo[] directoryGroupsColorInfoActive =
        {
            // State.Normal
            new Settings.ColorInfo(Color.FromArgb(255,255,180), // yellow
                                   Color.FromArgb(255,255,180), // yellow
                                   Color.Black, false),
            // State.Selected
            new Settings.ColorInfo(SystemColors.Highlight, SystemColors.Highlight,
                                   SystemColors.HighlightText, true),
            // State.Hovered
            new Settings.ColorInfo(SystemColors.Highlight, SystemColors.Highlight,
                                   SystemColors.HighlightText, true)
        };

        public static Settings.ColorInfo[] directoryGroupsColorInfoInactive =
        {
            // State.Normal
            new Settings.ColorInfo(Color.FromArgb(255,255,180), // yellow
                                   Color.FromArgb(255,255,180), // yellow
                                   Color.Black, false),
            // State.Selected
            new Settings.ColorInfo(SystemColors.Highlight, SystemColors.Highlight,
                                   SystemColors.HighlightText, true),
            // State.Hovered
            new Settings.ColorInfo(SystemColors.Highlight, SystemColors.Highlight,
                                   SystemColors.HighlightText, true)
        };

        public static Settings.ColorInfo[] directoryListColorInfoActive =
        {
            // State.Normal
            new Settings.ColorInfo(Color.White, Color.White, Color.Black, false),

            // State.Selected
            new Settings.ColorInfo(SystemColors.Highlight, SystemColors.Highlight,
                                   SystemColors.HighlightText, true),
            // State.Hovered
            new Settings.ColorInfo(SystemColors.Highlight, SystemColors.Highlight,
                                   SystemColors.HighlightText, true)
        };

        public static Settings.ColorInfo[] directoryListColorInfoInactive =
        {
            // State.Normal
            new Settings.ColorInfo(Color.White, Color.White, Color.Black, false),

            // State.Selected
            new Settings.ColorInfo(SystemColors.Highlight, SystemColors.Highlight,
                                   SystemColors.HighlightText, true),
            // State.Hovered
            new Settings.ColorInfo(SystemColors.Highlight, SystemColors.Highlight,
                                   SystemColors.HighlightText, true)
        };

        public static Settings.ColorInfo directoryGroupBackgroundColorInfo =
            new Settings.ColorInfo(Color.FromArgb(unchecked((int)0xFFF0F0F0)),
                                   Color.FromArgb(unchecked((int)0xFFF0F0F0)),
                                   Color.Black, false);

        public static Settings.ColorInfo directoryListBackgroundColorInfo =
            new Settings.ColorInfo(Color.FromArgb(unchecked((int)0xFFF0F0F0)),
                                   Color.FromArgb(unchecked((int)0xFFF0F0F0)),
                                   Color.Black, false);

        public static Settings.ColorInfo dialpadColorInfo =
            new Settings.ColorInfo(Color.White, Color.White, Color.Black, false);

        public static Settings.ColorInfo dialpadHoverColorInfo =
            new Settings.ColorInfo(Color.White, Color.White, Color.Black, false);

		#region Constructor / Deconstructor
		public Phonebook(Controller c)
		{
			controller = c;

            // Required for Windows Form Designer support
			InitializeComponent();
            setTranslatableText();

            this.     BackColor = SystemColors.Window;
            MainMenu. BackColor = SystemColors.Window;
            toolBar.  BackColor = SystemColors.Window;

            colorsUpdated();

            splitContainer1.SplitterWidth = 1;

            blindButton.      Tag = blindMenu.      Tag = Controller.TransferType.Blind;
            attendedButton.   Tag = attendedMenu.   Tag = Controller.TransferType.Attended;
            conferenceButton. Tag = conferenceMenu. Tag = Controller.TransferType.Conference;
            tandemButton.     Tag = tandemMenu.     Tag = Controller.TransferType.Tandem;
            flashButton.      Tag = flashMenu.      Tag = Controller.TransferType.FlashHook;
            speedDialButton.  Tag = speedDialMenu.  Tag = Controller.TransferType.Dial;

            attendedButton.   Visible = attendedMenu.   Visible = showAttendedTransferButton;
            tandemButton.     Visible = tandemMenu.     Visible = showTandemTransferButton;
            blindButton.      Visible = blindMenu.      Visible = showBlindTransferButton;
            conferenceButton. Visible = conferenceMenu. Visible = showConferenceTransferButton;
            flashButton.      Visible = flashMenu.      Visible = showFlashTransferButton;
            speedDialButton.  Visible = speedDialMenu.  Visible = showSpeedDialButton;

            if ( ! showDialPadButton) tabControl.Controls.Remove(dialTab);

// TODO flash: enable Flash button and menu?
flashButton.Visible = flashMenu.Visible = false;

            setEnabled(false, null);

            object defaultGroup = controller.settings.PhonebookSettings["DefaultGroup"];
            if (defaultGroup != null)
            {
                DefaultGroup = defaultGroup.ToString();
            }

            object viewMode = controller.settings.PhonebookSettings["View"];
            if (viewMode != null)
            {
                showPhonebookAsButtons = (viewMode.ToString().ToUpper() == "BUTTON");
            }

            statusStrip.Visible = false;

            if (showPhonebookAsButtons)
            {
                directoryListBox.  ItemHeight = 80;
                directoryTreeView. ItemHeight = 80;

                iconSpace = 30;
                iconSize  = 24;

                ClientSize = new Size(392, 590);
            }
            else
            {
                ClientSize = new Size(392, 397);
            }

            // hardcode minimum and maximum size
            MinimumSize = SizeFromClientSize(ClientSize);
            MaximumSize = SystemInformation.VirtualScreen.Size;

			ReadData();

            phoneField.Text = "";

            try
            {
                dialHistory = new AutoCompleteStringCollection();

                // NOTE: use non-translated company name here
                regSettings = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(
                    "Software\\" + System.Windows.Forms.Application.CompanyName + "\\PSAP Toolkit");

                if (regSettings != null)
                {
                    string[] dialHistoryStrList;
                    dialHistoryStrList = (string[])regSettings.GetValue("DialHistory");

                    if (dialHistoryStrList != null)
                    {
                        dialHistory.AddRange(dialHistoryStrList);

                        // TODO calls: should we add the call history back someday?  (Bill didn't like it...)
                        //phoneField.AutoCompleteCustomSource = dialHistory;
                        //phoneField.Items.AddRange(dialHistoryStrList);
                    }
                }
            }
            catch
            {
                regSettings = null;
            }

            try
            {
                // TODO calls: should we add the call history back someday?  (Bill didn't like it...)
                //phoneField.ContextMenu = new ContextMenu();
                //phoneField.ContextMenu.MenuItems.Add("&Clear History", clearDialHistory);
            }
            catch {}
		}

		#endregion

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Phonebook));
            this.toolBar = new System.Windows.Forms.ToolStrip();
            this.printButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tandemButton = new System.Windows.Forms.ToolStripButton();
            this.flashButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.blindButton = new System.Windows.Forms.ToolStripButton();
            this.attendedButton = new System.Windows.Forms.ToolStripButton();
            this.conferenceButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.speedDialButton = new System.Windows.Forms.ToolStripButton();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.directoryListBox = new System.Windows.Forms.ListBox();
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.closeMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.transferMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.tandemMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.flashMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.blindMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.attendedMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.conferenceMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.speedDialMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usersManualMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuSep = new System.Windows.Forms.ToolStripSeparator();
            this.aboutMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.printDoc = new System.Drawing.Printing.PrintDocument();
            this.printSetup = new System.Windows.Forms.PrintDialog();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.directoryTab = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.directoryTreeView = new System.Windows.Forms.TreeView();
            this.dialTab = new System.Windows.Forms.TabPage();
            this.dialLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.phoneField = new System.Windows.Forms.TextBox();
            this.button1 = new WestTel.E911.Phonebook.DialButton();
            this.button2 = new WestTel.E911.Phonebook.DialButton();
            this.button3 = new WestTel.E911.Phonebook.DialButton();
            this.button4 = new WestTel.E911.Phonebook.DialButton();
            this.button5 = new WestTel.E911.Phonebook.DialButton();
            this.button6 = new WestTel.E911.Phonebook.DialButton();
            this.button7 = new WestTel.E911.Phonebook.DialButton();
            this.button8 = new WestTel.E911.Phonebook.DialButton();
            this.button9 = new WestTel.E911.Phonebook.DialButton();
            this.buttonAsterisk = new WestTel.E911.Phonebook.DialButton();
            this.button0 = new WestTel.E911.Phonebook.DialButton();
            this.buttonPound = new WestTel.E911.Phonebook.DialButton();
            this.backButton = new System.Windows.Forms.Button();
            this.toolBar.SuspendLayout();
            this.MainMenu.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.directoryTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.dialTab.SuspendLayout();
            this.dialLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolBar
            // 
            this.toolBar.AllowMerge = false;
            this.toolBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left))));
            this.toolBar.AutoSize = true;
            this.toolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolBar.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.toolBar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolBar.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printButton,
            this.toolStripSeparator4,
            this.tandemButton,
            this.flashButton,
            this.toolStripSeparator5,
            this.blindButton,
            this.attendedButton,
            this.conferenceButton,
            this.toolStripSeparator6,
            this.speedDialButton});
            this.toolBar.Location = new System.Drawing.Point(0, 24);
            this.toolBar.Name = "toolBar";
            this.toolBar.Padding = new System.Windows.Forms.Padding(0);
            this.toolBar.Size = new System.Drawing.Size(392, 45);
            this.toolBar.Stretch = true;
            this.toolBar.TabIndex = 2;
            this.toolBar.Text = "Tool Bar";
            // 
            // printButton
            // 
            this.printButton.Enabled = false;
            this.printButton.Image = Properties.Resources.PrintIcon.ToBitmap();
            this.printButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.printButton.Name = "printButton";
            this.printButton.Padding = new System.Windows.Forms.Padding(2);
            this.printButton.Size = new System.Drawing.Size(36, 42);
            this.printButton.Text = "&Print";
            this.printButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.printButton.ToolTipText = "Print (Ctrl+P)";
            this.printButton.Click += new System.EventHandler(this.printMenu_ItemClick);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 45);
            // 
            // tandemButton
            // 
            this.tandemButton.Image = Properties.Resources.Phonebook911TandemTransferIcon.ToBitmap();
            this.tandemButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tandemButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.tandemButton.Name = "tandemButton";
            this.tandemButton.Padding = new System.Windows.Forms.Padding(2);
            this.tandemButton.Size = new System.Drawing.Size(40, 42);
            this.tandemButton.Text = "&9-1-1";
            this.tandemButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tandemButton.Click += new System.EventHandler(this.transferButton_Click);
            // 
            // flashButton
            // 
            this.flashButton.Image = Properties.Resources.PhonebookFlashTransferIcon.ToBitmap();
            this.flashButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.flashButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.flashButton.Name = "flashButton";
            this.flashButton.Padding = new System.Windows.Forms.Padding(2);
            this.flashButton.Size = new System.Drawing.Size(40, 42);
            this.flashButton.Text = "&Flash";
            this.flashButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.flashButton.Click += new System.EventHandler(this.transferButton_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 45);
            // 
            // blindButton
            // 
            this.blindButton.Image = Properties.Resources.PhonebookBlindTransferIcon.ToBitmap();
            this.blindButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.blindButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.blindButton.Name = "blindButton";
            this.blindButton.Padding = new System.Windows.Forms.Padding(2);
            this.blindButton.Size = new System.Drawing.Size(36, 42);
            this.blindButton.Text = "&Blind";
            this.blindButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.blindButton.Click += new System.EventHandler(this.transferButton_Click);
            // 
            // attendedButton
            // 
            this.attendedButton.Image = Properties.Resources.PhonebookAttendedTransferIcon.ToBitmap();
            this.attendedButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.attendedButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.attendedButton.Name = "attendedButton";
            this.attendedButton.Padding = new System.Windows.Forms.Padding(2);
            this.attendedButton.Size = new System.Drawing.Size(64, 42);
            this.attendedButton.Text = "&Attended";
            this.attendedButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.attendedButton.Click += new System.EventHandler(this.transferButton_Click);
            // 
            // conferenceButton
            // 
            this.conferenceButton.Image = Properties.Resources.PhonebookConferenceIcon.ToBitmap();
            this.conferenceButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.conferenceButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.conferenceButton.Name = "conferenceButton";
            this.conferenceButton.Padding = new System.Windows.Forms.Padding(2);
            this.conferenceButton.Size = new System.Drawing.Size(74, 42);
            this.conferenceButton.Text = "&Conference";
            this.conferenceButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.conferenceButton.Click += new System.EventHandler(this.transferButton_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 45);
            // 
            // speedDialButton
            // 
            this.speedDialButton.Image = Properties.Resources.PhonebookDialIcon.ToBitmap();
            this.speedDialButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.speedDialButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.speedDialButton.Name = "speedDialButton";
            this.speedDialButton.Padding = new System.Windows.Forms.Padding(2);
            this.speedDialButton.Size = new System.Drawing.Size(29, 42);
            this.speedDialButton.Text = "&Dial";
            this.speedDialButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.speedDialButton.Click += new System.EventHandler(this.transferButton_Click);
            // 
            // toolTip
            // 
            this.toolTip.AutomaticDelay = 1000;
            this.toolTip.AutoPopDelay = 20000;
            this.toolTip.InitialDelay = 1000;
            this.toolTip.ReshowDelay = 300;
            this.toolTip.ShowAlways = true;
            this.toolTip.StripAmpersands = true;
            // 
            // directoryListBox
            // 
            this.directoryListBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.directoryListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.directoryListBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.directoryListBox.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.directoryListBox.HorizontalScrollbar = true;
            this.directoryListBox.ItemHeight = 19;
            this.directoryListBox.Items.AddRange(new object[] {
            "Test",
            "Coconino County (CCDC) Intake",
            "End",
            "Item4",
            "Item5",
            "Item6",
            "Item7",
            "Item8",
            "Item9"});
            this.directoryListBox.Location = new System.Drawing.Point(0, 0);
            this.directoryListBox.Margin = new System.Windows.Forms.Padding(0);
            this.directoryListBox.Name = "directoryListBox";
            this.directoryListBox.Size = new System.Drawing.Size(194, 299);
            this.directoryListBox.TabIndex = 1;
            this.directoryListBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.listBox_DrawItem);
            this.directoryListBox.SelectedIndexChanged += new System.EventHandler(this.listBox_SelectedIndexChanged);
            this.directoryListBox.DataSourceChanged += new System.EventHandler(this.listBox_DataSourceChanged);
            this.directoryListBox.VisibleChanged += new System.EventHandler(this.tabContent_VisibleChanged);
            this.directoryListBox.DoubleClick += new System.EventHandler(this.listBox_DoubleClick);
            this.directoryListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listBox_KeyDown);
            this.directoryListBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseMove);
            this.directoryListBox.MouseLeave += DirectoryListBox_MouseLeave;
            // 
            // MainMenu
            // 
            this.MainMenu.AllowMerge = false;
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.transferMenu,
            this.helpToolStripMenuItem});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(392, 24);
            this.MainMenu.TabIndex = 1;
            this.MainMenu.Text = "Main Menu";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printMenu,
            this.toolStripSeparator1,
            this.closeMenu});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // printMenu
            // 
            this.printMenu.Enabled = false;
            this.printMenu.Image = new Icon(Properties.Resources.PrintIcon, 16, 16).ToBitmap();
            this.printMenu.Name = "printMenu";
            this.printMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.printMenu.Size = new System.Drawing.Size(149, 22);
            this.printMenu.Text = "&Print...";
            this.printMenu.Click += new System.EventHandler(this.printMenu_ItemClick);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(146, 6);
            // 
            // closeMenu
            // 
            this.closeMenu.Name = "closeMenu";
            this.closeMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.closeMenu.Size = new System.Drawing.Size(149, 22);
            this.closeMenu.Text = "&Close";
            this.closeMenu.Click += new System.EventHandler(this.menuClose_ItemClick);
            // 
            // transferMenu
            // 
            this.transferMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tandemMenu,
            this.flashMenu,
            this.toolStripSeparator2,
            this.blindMenu,
            this.attendedMenu,
            this.conferenceMenu,
            this.toolStripSeparator3,
            this.speedDialMenu});
            this.transferMenu.Name = "transferMenu";
            this.transferMenu.Size = new System.Drawing.Size(62, 20);
            this.transferMenu.Text = "&Transfer";
            // 
            // tandemMenu
            // 
            this.tandemMenu.Image = new Icon(Properties.Resources.Phonebook911TandemTransferIcon, 16, 16).ToBitmap();
            this.tandemMenu.Name = "tandemMenu";
            this.tandemMenu.Size = new System.Drawing.Size(144, 22);
            this.tandemMenu.Text = "&9-1-1...";
            this.tandemMenu.Click += new System.EventHandler(this.transferButton_Click);
            // 
            // flashMenu
            // 
            this.flashMenu.Image = new Icon(Properties.Resources.PhonebookFlashTransferIcon, 16, 16).ToBitmap();
            this.flashMenu.Name = "flashMenu";
            this.flashMenu.Size = new System.Drawing.Size(144, 22);
            this.flashMenu.Text = "&Flash...";
            this.flashMenu.Click += new System.EventHandler(this.transferButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(141, 6);
            // 
            // blindMenu
            // 
            this.blindMenu.Image = new Icon(Properties.Resources.PhonebookBlindTransferIcon, 16, 16).ToBitmap();
            this.blindMenu.Name = "blindMenu";
            this.blindMenu.Size = new System.Drawing.Size(144, 22);
            this.blindMenu.Text = "&Blind...";
            this.blindMenu.Click += new System.EventHandler(this.transferButton_Click);
            // 
            // attendedMenu
            // 
            this.attendedMenu.Image = new Icon(Properties.Resources.PhonebookAttendedTransferIcon, 16, 16).ToBitmap();
            this.attendedMenu.Name = "attendedMenu";
            this.attendedMenu.Size = new System.Drawing.Size(144, 22);
            this.attendedMenu.Text = "&Attended...";
            this.attendedMenu.Click += new System.EventHandler(this.transferButton_Click);
            // 
            // conferenceMenu
            // 
            this.conferenceMenu.Image = new Icon(Properties.Resources.PhonebookConferenceIcon, 16, 16).ToBitmap();
            this.conferenceMenu.Name = "conferenceMenu";
            this.conferenceMenu.Size = new System.Drawing.Size(144, 22);
            this.conferenceMenu.Text = "C&onference...";
            this.conferenceMenu.Click += new System.EventHandler(this.transferButton_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(141, 6);
            // 
            // speedDialMenu
            // 
            this.speedDialMenu.Image = new Icon(Properties.Resources.PhonebookDialIcon, 16, 16).ToBitmap();
            this.speedDialMenu.Name = "speedDialMenu";
            this.speedDialMenu.Size = new System.Drawing.Size(144, 22);
            this.speedDialMenu.Text = "&Dial";
            this.speedDialMenu.Click += new System.EventHandler(this.transferButton_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usersManualMenu,
            this.helpMenuSep,
            this.aboutMenu});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // usersManualMenu
            // 
            this.usersManualMenu.Image = new Icon(Properties.Resources.UserManualIcon, 16, 16).ToBitmap();
            this.usersManualMenu.Name = "usersManualMenu";
            this.usersManualMenu.Size = new System.Drawing.Size(140, 22);
            this.usersManualMenu.Text = "&User Manual";
            this.usersManualMenu.Click += new System.EventHandler(this.usersManualMenu_Click);
            // 
            // helpMenuSep
            // 
            this.helpMenuSep.Name = "helpMenuSep";
            this.helpMenuSep.Size = new System.Drawing.Size(137, 6);
            // 
            // aboutMenu
            // 
            this.aboutMenu.Image = new Icon(Properties.Resources.WestTelIcon, 16, 16).ToBitmap();
            this.aboutMenu.Name = "aboutMenu";
            this.aboutMenu.Size = new System.Drawing.Size(140, 22);
            this.aboutMenu.Text = "&About XXX";
            this.aboutMenu.Click += new System.EventHandler(this.menuHelpAbout_ItemClick);
            // 
            // statusStrip
            // 
            this.statusStrip.BackColor = System.Drawing.SystemColors.Window;
            this.statusStrip.Location = new System.Drawing.Point(0, 395);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(392, 22);
            this.statusStrip.TabIndex = 6;
            // 
            // printDoc
            // 
            this.printDoc.DocumentName = "Phonebook List";
            this.printDoc.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDoc_PrintPage);
            // 
            // printSetup
            // 
            this.printSetup.Document = this.printDoc;
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.tabControl.Controls.Add(this.directoryTab);
            this.tabControl.Controls.Add(this.dialTab);
            this.tabControl.Dock = DockStyle.Fill;
            this.tabControl.HotTrack = true;
            this.tabControl.Location = new System.Drawing.Point(0, 69);
            this.tabControl.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl.Multiline = true;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(392, 326);
            this.tabControl.TabIndex = 3;
            this.tabControl.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabChanged);
            this.tabControl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tabControl_KeyDown);
            // 
            // directoryTab
            // 
            this.directoryTab.Controls.Add(this.splitContainer1);
            this.directoryTab.Location = new System.Drawing.Point(4, 23);
            this.directoryTab.Margin = new System.Windows.Forms.Padding(0);
            this.directoryTab.Name = "directoryTab";
            this.directoryTab.Size = new System.Drawing.Size(384, 299);
            this.directoryTab.TabIndex = 0;
            this.directoryTab.Text = "Directory";
            this.directoryTab.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.directoryTreeView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.directoryListBox);
            this.splitContainer1.Size = new System.Drawing.Size(384, 299);
            this.splitContainer1.SplitterDistance = 186;
            this.splitContainer1.TabIndex = 4;
            this.splitContainer1.TabStop = false;
            // 
            // directoryTreeView
            // 
            this.directoryTreeView.BackColor = System.Drawing.SystemColors.Info;
            this.directoryTreeView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.directoryTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.directoryTreeView.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawAll;
            this.directoryTreeView.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.directoryTreeView.FullRowSelect = true;
            this.directoryTreeView.Indent = 10;
            this.directoryTreeView.ItemHeight = 19;
            this.directoryTreeView.Location = new System.Drawing.Point(0, 0);
            this.directoryTreeView.Name = "directoryTreeView";
            this.directoryTreeView.ShowLines = false;
            this.directoryTreeView.ShowRootLines = false;
            this.directoryTreeView.Size = new System.Drawing.Size(186, 299);
            this.directoryTreeView.TabIndex = 0;
            this.directoryTreeView.DrawNode += new System.Windows.Forms.DrawTreeNodeEventHandler(this.directoryTreeView_DrawNode);
            this.directoryTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.directoryTreeView_AfterSelect);
            this.directoryTreeView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.treeView_KeyDown);
            this.directoryTreeView.MouseLeave += DirectoryTreeView_MouseLeave;
            this.directoryTreeView.MouseMove += DirectoryTreeView_MouseMove;
            // 
            // dialTab
            // 
            this.dialTab.Controls.Add(this.dialLayoutPanel);
            this.dialTab.Location = new System.Drawing.Point(4, 22);
            this.dialTab.Margin = new System.Windows.Forms.Padding(0);
            this.dialTab.Name = "dialTab";
            this.dialTab.Size = new System.Drawing.Size(384, 300);
            this.dialTab.TabIndex = 1;
            this.dialTab.Text = "Dialpad";
            this.dialTab.UseVisualStyleBackColor = true;
            // 
            // dialLayoutPanel
            // 
            this.dialLayoutPanel.AutoSize = true;
            this.dialLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.dialLayoutPanel.ColumnCount = 6;
            this.dialLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.33333F));
            this.dialLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.dialLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.33333F));
            this.dialLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.dialLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.33333F));
            this.dialLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.dialLayoutPanel.Controls.Add(this.phoneField, 0, 0);
            this.dialLayoutPanel.Controls.Add(this.button1, 0, 1);
            this.dialLayoutPanel.Controls.Add(this.button2, 2, 1);
            this.dialLayoutPanel.Controls.Add(this.button3, 4, 1);
            this.dialLayoutPanel.Controls.Add(this.button4, 0, 2);
            this.dialLayoutPanel.Controls.Add(this.button5, 2, 2);
            this.dialLayoutPanel.Controls.Add(this.button6, 4, 2);
            this.dialLayoutPanel.Controls.Add(this.button7, 0, 3);
            this.dialLayoutPanel.Controls.Add(this.button8, 2, 3);
            this.dialLayoutPanel.Controls.Add(this.button9, 4, 3);
            this.dialLayoutPanel.Controls.Add(this.buttonAsterisk, 0, 4);
            this.dialLayoutPanel.Controls.Add(this.button0, 2, 4);
            this.dialLayoutPanel.Controls.Add(this.buttonPound, 4, 4);
            this.dialLayoutPanel.Controls.Add(this.backButton, 5, 0);
            this.dialLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dialLayoutPanel.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.dialLayoutPanel.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.dialLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.dialLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.dialLayoutPanel.Name = "dialLayoutPanel";
            this.dialLayoutPanel.RowCount = 5;
            this.dialLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.dialLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.dialLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.dialLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.dialLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.dialLayoutPanel.Size = new System.Drawing.Size(384, 300);
            this.dialLayoutPanel.TabIndex = 6;
            this.dialLayoutPanel.VisibleChanged += new System.EventHandler(this.tabContent_VisibleChanged);
            // 
            // phoneField
            // 
            this.dialLayoutPanel.SetColumnSpan(this.phoneField, 5);
            this.phoneField.Dock = System.Windows.Forms.DockStyle.Fill;
            this.phoneField.Font = new System.Drawing.Font("Arial", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.phoneField.Location = new System.Drawing.Point(6, 6);
            this.phoneField.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.phoneField.Name = "phoneField";
            this.phoneField.Size = new System.Drawing.Size(337, 40);
            this.phoneField.TabIndex = 0;
            this.phoneField.Text = "(123) 456-7890";
            this.phoneField.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.phoneField_KeyPress);
            this.phoneField.KeyUp += new System.Windows.Forms.KeyEventHandler(this.phoneField_KeyRelease);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.Window;
            this.dialLayoutPanel.SetColumnSpan(this.button1, 2);
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(0, 52);
            this.button1.Margin = new System.Windows.Forms.Padding(0);
            this.button1.Name = "button1";
            this.button1.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.button1.Size = new System.Drawing.Size(127, 62);
            this.button1.TabIndex = 2;
            this.button1.TabStop = false;
            this.button1.Tag = "1\r\n ";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.dialButton_Clicked);
            this.button1.MouseEnter += dialButton_MouseEnter;
            this.button1.MouseLeave += dialButton_MouseLeave;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.Window;
            this.dialLayoutPanel.SetColumnSpan(this.button2, 2);
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.Location = new System.Drawing.Point(127, 52);
            this.button2.Margin = new System.Windows.Forms.Padding(0);
            this.button2.Name = "button2";
            this.button2.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.button2.Size = new System.Drawing.Size(127, 62);
            this.button2.TabIndex = 3;
            this.button2.TabStop = false;
            this.button2.Tag = "2\r\na b c";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.dialButton_Clicked);
            this.button2.MouseEnter += dialButton_MouseEnter;
            this.button2.MouseLeave += dialButton_MouseLeave;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.Window;
            this.dialLayoutPanel.SetColumnSpan(this.button3, 2);
            this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button3.Location = new System.Drawing.Point(254, 52);
            this.button3.Margin = new System.Windows.Forms.Padding(0);
            this.button3.Name = "button3";
            this.button3.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.button3.Size = new System.Drawing.Size(130, 62);
            this.button3.TabIndex = 4;
            this.button3.TabStop = false;
            this.button3.Tag = "3\r\nd e f";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.dialButton_Clicked);
            this.button3.MouseEnter += dialButton_MouseEnter;
            this.button3.MouseLeave += dialButton_MouseLeave;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.Window;
            this.dialLayoutPanel.SetColumnSpan(this.button4, 2);
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.Location = new System.Drawing.Point(0, 114);
            this.button4.Margin = new System.Windows.Forms.Padding(0);
            this.button4.Name = "button4";
            this.button4.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.button4.Size = new System.Drawing.Size(127, 62);
            this.button4.TabIndex = 5;
            this.button4.TabStop = false;
            this.button4.Tag = "4\r\ng h i";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.dialButton_Clicked);
            this.button4.MouseEnter += dialButton_MouseEnter;
            this.button4.MouseLeave += dialButton_MouseLeave;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.Window;
            this.dialLayoutPanel.SetColumnSpan(this.button5, 2);
            this.button5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button5.Location = new System.Drawing.Point(127, 114);
            this.button5.Margin = new System.Windows.Forms.Padding(0);
            this.button5.Name = "button5";
            this.button5.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.button5.Size = new System.Drawing.Size(127, 62);
            this.button5.TabIndex = 6;
            this.button5.TabStop = false;
            this.button5.Tag = "5\r\nj k l";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.dialButton_Clicked);
            this.button5.MouseEnter += dialButton_MouseEnter;
            this.button5.MouseLeave += dialButton_MouseLeave;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.SystemColors.Window;
            this.dialLayoutPanel.SetColumnSpan(this.button6, 2);
            this.button6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button6.Location = new System.Drawing.Point(254, 114);
            this.button6.Margin = new System.Windows.Forms.Padding(0);
            this.button6.Name = "button6";
            this.button6.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.button6.Size = new System.Drawing.Size(130, 62);
            this.button6.TabIndex = 7;
            this.button6.TabStop = false;
            this.button6.Tag = "6\r\nm n o";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.dialButton_Clicked);
            this.button6.MouseEnter += dialButton_MouseEnter;
            this.button6.MouseLeave += dialButton_MouseLeave;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.Window;
            this.dialLayoutPanel.SetColumnSpan(this.button7, 2);
            this.button7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button7.Location = new System.Drawing.Point(0, 176);
            this.button7.Margin = new System.Windows.Forms.Padding(0);
            this.button7.Name = "button7";
            this.button7.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.button7.Size = new System.Drawing.Size(127, 62);
            this.button7.TabIndex = 8;
            this.button7.TabStop = false;
            this.button7.Tag = "7\r\np q r s";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.dialButton_Clicked);
            this.button7.MouseEnter += dialButton_MouseEnter;
            this.button7.MouseLeave += dialButton_MouseLeave;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.SystemColors.Window;
            this.dialLayoutPanel.SetColumnSpan(this.button8, 2);
            this.button8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button8.Location = new System.Drawing.Point(127, 176);
            this.button8.Margin = new System.Windows.Forms.Padding(0);
            this.button8.Name = "button8";
            this.button8.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.button8.Size = new System.Drawing.Size(127, 62);
            this.button8.TabIndex = 9;
            this.button8.TabStop = false;
            this.button8.Tag = "8\r\nt u v";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.dialButton_Clicked);
            this.button8.MouseEnter += dialButton_MouseEnter;
            this.button8.MouseLeave += dialButton_MouseLeave;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.SystemColors.Window;
            this.dialLayoutPanel.SetColumnSpan(this.button9, 2);
            this.button9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button9.Location = new System.Drawing.Point(254, 176);
            this.button9.Margin = new System.Windows.Forms.Padding(0);
            this.button9.Name = "button9";
            this.button9.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.button9.Size = new System.Drawing.Size(130, 62);
            this.button9.TabIndex = 10;
            this.button9.TabStop = false;
            this.button9.Tag = "9\r\nw x y z";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.dialButton_Clicked);
            this.button9.MouseEnter += dialButton_MouseEnter;
            this.button9.MouseLeave += dialButton_MouseLeave;
            // 
            // buttonAsterisk
            // 
            this.buttonAsterisk.BackColor = System.Drawing.SystemColors.Window;
            this.dialLayoutPanel.SetColumnSpan(this.buttonAsterisk, 2);
            this.buttonAsterisk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonAsterisk.Font = new System.Drawing.Font("Consolas", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.buttonAsterisk.Location = new System.Drawing.Point(0, 238);
            this.buttonAsterisk.Margin = new System.Windows.Forms.Padding(0);
            this.buttonAsterisk.Name = "buttonAsterisk";
            this.buttonAsterisk.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.buttonAsterisk.Size = new System.Drawing.Size(127, 62);
            this.buttonAsterisk.TabIndex = 11;
            this.buttonAsterisk.TabStop = false;
            this.buttonAsterisk.Tag = "*";
            this.buttonAsterisk.UseVisualStyleBackColor = false;
            this.buttonAsterisk.Click += new System.EventHandler(this.dialButton_Clicked);
            this.buttonAsterisk.MouseEnter += dialButton_MouseEnter;
            this.buttonAsterisk.MouseLeave += dialButton_MouseLeave;
            // 
            // button0
            // 
            this.button0.BackColor = System.Drawing.SystemColors.Window;
            this.dialLayoutPanel.SetColumnSpan(this.button0, 2);
            this.button0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button0.Location = new System.Drawing.Point(127, 238);
            this.button0.Margin = new System.Windows.Forms.Padding(0);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(127, 62);
            this.button0.TabIndex = 12;
            this.button0.TabStop = false;
            this.button0.Tag = "0";
            this.button0.UseVisualStyleBackColor = false;
            this.button0.Click += new System.EventHandler(this.dialButton_Clicked);
            this.button0.MouseEnter += dialButton_MouseEnter;
            this.button0.MouseLeave += dialButton_MouseLeave;
            // 
            // buttonPound
            // 
            this.buttonPound.BackColor = System.Drawing.SystemColors.Window;
            this.dialLayoutPanel.SetColumnSpan(this.buttonPound, 2);
            this.buttonPound.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonPound.Location = new System.Drawing.Point(254, 238);
            this.buttonPound.Margin = new System.Windows.Forms.Padding(0);
            this.buttonPound.Name = "buttonPound";
            this.buttonPound.Size = new System.Drawing.Size(130, 62);
            this.buttonPound.TabIndex = 13;
            this.buttonPound.TabStop = false;
            this.buttonPound.Tag = "#";
            this.buttonPound.UseVisualStyleBackColor = false;
            this.buttonPound.Click += new System.EventHandler(this.dialButton_Clicked);
            this.buttonPound.MouseEnter += dialButton_MouseEnter;
            this.buttonPound.MouseLeave += dialButton_MouseLeave;
            // 
            // backButton
            // 
            this.backButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.backButton.FlatAppearance.BorderSize = 0;
            this.backButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backButton.Image = Properties.Resources.BackButtonBitmap;
            this.backButton.Location = new System.Drawing.Point(343, 0);
            this.backButton.Margin = new System.Windows.Forms.Padding(0, 0, 3, 4);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(38, 48);
            this.backButton.TabIndex = 1;
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // Phonebook
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            this.ClientSize = new System.Drawing.Size(392, 417);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.toolBar);
            this.Controls.Add(this.MainMenu);
            this.Controls.Add(this.statusStrip);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Icon = Properties.Resources.PhonebookIcon;
            this.KeyPreview = true;
            this.MainMenuStrip = this.MainMenu;
            this.Name = "Phonebook";
            this.Text = "Phonebook";
            this.Activated += new System.EventHandler(this.Transfer_Activated);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.Transfer_Closing);
            this.Load += new System.EventHandler(this.Transfer_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Transfer_KeyDown);
            this.toolBar.ResumeLayout(false);
            this.toolBar.PerformLayout();
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.directoryTab.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.dialTab.ResumeLayout(false);
            this.dialTab.PerformLayout();
            this.dialLayoutPanel.ResumeLayout(false);
            this.dialLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
        #endregion

        private void setTranslatableText()
        {
            this.toolBar.Text               = (Translate)("Tool Bar");
            this.printButton.Text           = (Translate)("&Print");
            this.printButton.ToolTipText    = (Translate)("Print") + " (Ctrl+P)";
            this.tandemButton.Text          = (Translate)("&9-1-1");        
            this.flashButton.Text           = (Translate)("&Flash");        
            this.blindButton.Text           = (Translate)("&Blind");        
            this.attendedButton.Text        = (Translate)("&Attended");        
            this.conferenceButton.Text      = (Translate)("&Conference");        
            this.speedDialButton.Text       = (Translate)("&Dial");        
            this.MainMenu.Text              = (Translate)("Main Menu");        
            this.fileToolStripMenuItem.Text = (Translate)("&File");        
            this.printMenu.Text             = (Translate)("&Print...");        
            this.closeMenu.Text             = (Translate)("&Close");        
            this.transferMenu.Text          = (Translate)("&Transfer");        
            this.tandemMenu.Text            = (Translate)("&9-1-1...");        
            this.flashMenu.Text             = (Translate)("&Flash...");        
            this.blindMenu.Text             = (Translate)("&Blind...");        
            this.attendedMenu.Text          = (Translate)("&Attended...");        
            this.conferenceMenu.Text        = (Translate)("C&onference...");        
            this.speedDialMenu.Text         = (Translate)("&Dial");        
            this.helpToolStripMenuItem.Text = (Translate)("&Help");        
            this.usersManualMenu.Text       = (Translate)("&User Manual");        
            this.aboutMenu.Text             = (Translate)("&About XXX");        
            this.directoryTab.Text          = (Translate)("Directory");        
            this.dialTab.Text               = (Translate)("Dialpad");
        }

        System.Threading.Timer colorsUpdatedTimer = null;

        public void colorsUpdated()
        {
            if ( ! Visible)
            {
                if (colorsUpdatedTimer == null) colorsUpdatedTimer = new System.Threading.Timer(colorsUpdated);
                colorsUpdatedTimer.Change(500, -1);
            }
            else
            {
                colorsUpdated(null);
            }
        }

        private delegate void ColorsUpdatedDelegate();

        private void colorsUpdated(object notused)
        {
            try   { BeginInvoke(new ColorsUpdatedDelegate(ColorsUpdated)); }
            catch { try {ColorsUpdated();} catch{} }
        }

        private void ColorsUpdated()
        {
            try
            {
                button0.BackColor = dialpadColorInfo.backgroundColor1;
                button1.BackColor = dialpadColorInfo.backgroundColor1;
                button2.BackColor = dialpadColorInfo.backgroundColor1;
                button3.BackColor = dialpadColorInfo.backgroundColor1;
                button4.BackColor = dialpadColorInfo.backgroundColor1;
                button5.BackColor = dialpadColorInfo.backgroundColor1;
                button6.BackColor = dialpadColorInfo.backgroundColor1;
                button7.BackColor = dialpadColorInfo.backgroundColor1;
                button8.BackColor = dialpadColorInfo.backgroundColor1;
                button9.BackColor = dialpadColorInfo.backgroundColor1;

                buttonAsterisk. BackColor = dialpadColorInfo.backgroundColor1;
                buttonPound.    BackColor = dialpadColorInfo.backgroundColor1;

                button0.ForeColor = dialpadColorInfo.foregroundColor;
                button1.ForeColor = dialpadColorInfo.foregroundColor;
                button2.ForeColor = dialpadColorInfo.foregroundColor;
                button3.ForeColor = dialpadColorInfo.foregroundColor;
                button4.ForeColor = dialpadColorInfo.foregroundColor;
                button5.ForeColor = dialpadColorInfo.foregroundColor;
                button6.ForeColor = dialpadColorInfo.foregroundColor;
                button7.ForeColor = dialpadColorInfo.foregroundColor;
                button8.ForeColor = dialpadColorInfo.foregroundColor;
                button9.ForeColor = dialpadColorInfo.foregroundColor;

                buttonAsterisk. ForeColor = dialpadColorInfo.foregroundColor;
                buttonPound.    ForeColor = dialpadColorInfo.foregroundColor;

                directoryTreeView. BackColor = directoryGroupBackgroundColorInfo. backgroundColor1;
                directoryListBox.  BackColor = directoryListBackgroundColorInfo.  backgroundColor1;

                Refresh();
            }
            catch {}
        }

		private void ReadData()
		{
			try
			{
                // reset list box to updated datasource (the listbox caches data)
                directoryTreeView.Nodes.Clear();

                directoryListBox.DataSource = null;
                directoryListBox.Items.Clear();

                maxGroupStrWidth = maxNameStrWidth = 140;

                if (controller.settings.GroupList != null)
                {
                    SortedDictionary<int,GroupEntry> SortedGroupMap =
                        new SortedDictionary<int,GroupEntry>();

                    foreach (GroupEntry g in controller.settings.GroupList)
				    {
                        // don't show speed dial groups in the phonebook
                        if ( ! g.isSpeedDialGroup()) SortedGroupMap.Add(g.Priority, g);
                    }

                    foreach (KeyValuePair<int,GroupEntry> p in SortedGroupMap)
                    {
                        string groupName = p.Value.Name;

                        if ( ! allEntries.ContainsKey(groupName))
                        {
                            if (p.Value.ImagePath == null)
                            {
                                directoryTreeView.Nodes.Add(groupName, "");
                            }
                            else
                            {
                                directoryTreeView.Nodes.Add(groupName, "", p.Value.ImagePath);
                            }

                            int groupStrWidth = TextRenderer.MeasureText(groupName, directoryTreeView.Font).Width;
                            if (groupStrWidth > maxGroupStrWidth) maxGroupStrWidth = groupStrWidth;

                            allEntries.Add(groupName, new ArrayList());
                        }
                    }
                }

                if (controller.settings.PhonebookList != null)
                {
                    foreach (PhonebookEntry t in controller.settings.PhonebookList)
				    {
                        // don't show speed dial groups in the phonebook
                        if (GroupEntry.isSpeedDialGroup(t.Group)) continue;

                        ArrayList groupEntries = null;

                        if (allEntries.ContainsKey(t.Group))
                        {
                            groupEntries = allEntries[t.Group];
                        }
                        else
                        {
                            // this is a new Group
                            directoryTreeView.Nodes.Add(t.Group, "");

                            int groupStrWidth = TextRenderer.MeasureText(t.Group, directoryTreeView.Font).Width;
                            if (groupStrWidth > maxGroupStrWidth) maxGroupStrWidth = groupStrWidth;

                            groupEntries = new ArrayList();
                            allEntries.Add(t.Group, groupEntries);
                        }

                        int nameStrWidth = TextRenderer.MeasureText(t.Name, directoryListBox.Font).Width;
                        if (nameStrWidth > maxNameStrWidth) maxNameStrWidth = nameStrWidth;

                        groupEntries.Add(t);

                        if (directoryListBox.DataSource == null)
                        {
                            directoryListBox.DataSource = groupEntries;
                        }
                    }

                    // TODO gui: account for icons?
                    maxGroupStrWidth += iconSpace;
                    maxNameStrWidth  += iconSpace;

                    if (showPhonebookAsButtons)
                    {
                        maxGroupStrWidth = Math.Min((int)(maxGroupStrWidth * 0.6), 140);
                        maxNameStrWidth  = Math.Min((int)(maxNameStrWidth  * 0.6), 140);
                    }

                    directoryListBox.DisplayMember = "Name";

                    try
                    {
                        if (directoryListBox.Items.Count != 0) directoryListBox.SelectedIndex = 0;
                    }
                    catch {}

                    int marginPlusScrollBars = (SystemInformation.VerticalScrollBarWidth + 16) * 2;

                    int idealWidth = (maxGroupStrWidth + maxNameStrWidth + marginPlusScrollBars);
                    int extraWidth = idealWidth - splitContainer1.Width;

                    // if there is not enough width
                    if (extraWidth > 0)
                    {
                        ClientSize = new Size(ClientSize.Width + extraWidth, ClientSize.Height);

                        // hardcode minimum and maximum size
                        MinimumSize = SizeFromClientSize(ClientSize);
                        MaximumSize = SystemInformation.VirtualScreen.Size;

                        double ratio = (maxGroupStrWidth + marginPlusScrollBars / 2.0) / idealWidth;
                        splitContainer1.SplitterDistance = (int)(splitContainer1.Width * ratio + 0.5);
                    }
                    else
                    {
                        int margin = splitContainer1.Width - idealWidth;

                        splitContainer1.SplitterDistance =
                            maxGroupStrWidth + (int)((margin + marginPlusScrollBars) / 2.0 + 0.5);
                    }
                }

                tabControl.SelectTab(directoryTab);

                if (directoryTreeView.Nodes.ContainsKey(DefaultGroup))
                {
                    directoryTreeView.SelectedNode = directoryTreeView.Nodes[DefaultGroup];
                }
                else if (directoryTreeView.Nodes.Count != 0)
                {
                    directoryTreeView.SelectedNode = directoryTreeView.Nodes[0];
                }

                tabChanged(this, new TabControlEventArgs(
                    tabControl.SelectedTab, tabControl.SelectedIndex, TabControlAction.Selected));
			}
			catch (Exception ex)
			{
				Logging.ExceptionLogger("Error in Phonebook.ReadData", ex, Console.LogID.Init);
			}
		}

		#region Form Event Handlers

		protected override void WndProc(ref Message m)
		{
			if (m.Msg == WM_QUERYENDSESSION)
			{
				if (!controller.OkToStop) Controller.ExitApplication(controller);
			}
			base.WndProc (ref m);
		}

		private void Transfer_Load(object sender, System.EventArgs e)
		{
			this.Text = Application.ProductName + (Translate)(" - Phonebook");
			aboutMenu.Text = aboutMenu.Text.Replace("XXX",Application.ProductName);

            resolveTransferButtonsState();

            // remove unnecessary separators
            removeUnnecessarySeparators(toolBar);
        }

        private void Transfer_Activated(object sender, EventArgs e)
        {
            // make sure the menus are enabled correctly
            setEnabled(transferEnabled, currentCallId);
        }

		private void Transfer_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!systemShutdown)
			{
				e.Cancel = true;
				Hide();
			}

            resolveTransferButtonsState();
		}

		#endregion
			
        private void tabChanged(object sender, TabControlEventArgs e)
        {
            if (e.TabPage == dialTab)
            {
                System.Windows.Forms.Application.DoEvents();
                phoneField.Focus();
                phoneField.SelectAll();
            }
            else if (e.TabPage == directoryTab)
            {
                System.Windows.Forms.Application.DoEvents();
                directoryListBox.Focus();
            }

            resolveTransferButtonsState();
        }

        private bool in_tabWidget_KeyDown = false;

        private void tabControl_KeyDown(object unusedSender, KeyEventArgs e)
        {
            if (tabControl.SelectedTab == dialTab)
            {
                if (tabControl.Focused)
                {
                    if (in_tabWidget_KeyDown) return;
                    in_tabWidget_KeyDown = true;

                    switch (e.KeyCode)
                    {
                        case Keys.Down:
        				    e.Handled = true;
                            phoneField.Focus();
                            System.Windows.Forms.Application.DoEvents();
                            phoneField.SelectAll();
                            break;
                        case Keys.Enter:
        				    e.Handled = true;
                            doTransfer();
                            e.Handled = true;
                            break;
                    }

                    System.Windows.Forms.Application.DoEvents();

                    in_tabWidget_KeyDown = false;
                }
                else
                {
                    // if Up is pressed when on the first row, change focus
                    switch (e.KeyCode)
                    {
                        case Keys.Enter:
        				    e.Handled = true;
                            doTransfer();
                            break;
                    }
                }

                return;
            }

            if ( ! tabControl.Focused || tabControl.SelectedTab != directoryTab) return;

            if (in_tabWidget_KeyDown) return;
            in_tabWidget_KeyDown = true;

            try
            {
                switch (e.KeyCode)
                {
                    case Keys.Up:
				        e.Handled = true;
                        directoryListBox.Focus();
                        System.Windows.Forms.Application.DoEvents();
                        SendKeys.Send("{UP}");
                        break;
                    case Keys.Down:
				        e.Handled = true;
                        directoryListBox.Focus();
                        System.Windows.Forms.Application.DoEvents();
                        SendKeys.Send("{DOWN}");
                        break;
                    case Keys.PageUp:
				        e.Handled = true;
                        directoryListBox.Focus();
                        System.Windows.Forms.Application.DoEvents();
                        SendKeys.Send("{PGUP}");
                        break;
                    case Keys.PageDown:
				        e.Handled = true;
                        directoryListBox.Focus();
                        System.Windows.Forms.Application.DoEvents();
                        SendKeys.Send("{PGDN}");
                        break;
                    case Keys.Enter:
				        e.Handled = true;
                        doTransfer();
                        break;
                }
            }
            catch {}

            in_tabWidget_KeyDown = false;
        }

        private bool in_tabListBox_KeyDown = false;

        private void listBox_KeyDown(object unusedSender, KeyEventArgs e)
        {
            if (tabControl.Focused || in_tabListBox_KeyDown) return;
            in_tabListBox_KeyDown = true;

            try
            {
                switch (e.KeyData)
                {
                    case Keys.Left:
				        e.Handled = true;
                        directoryTreeView.Focus();
                        break;
                    case Keys.Right:
				        e.Handled = true;
                        // TODO gui: determine if the feature to Right Arrow to the Dial Tab should be completely removed
                        //if (tabControl.SelectedIndex != tabControl.TabCount - 1) tabControl.SelectedIndex++;
                        break;
			        case Keys.Enter:
				        e.Handled = true;
                        doTransfer();
                        break;
                    case Keys.Escape:
				        e.Handled = true;
				        this.Close();
                        break;
                }
            }
            catch {}

            in_tabListBox_KeyDown = false;
        }

        static int hoveredListIndex = -1;

        private void listBox_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                string listBoxToolTip = "";

                int nIdx = directoryListBox.IndexFromPoint(e.Location);

                if ((nIdx >= 0) && (nIdx < directoryListBox.Items.Count))
                {
                    PhonebookEntry entry = (PhonebookEntry)directoryListBox.Items[nIdx];
                    listBoxToolTip = entry.NumberFormatted;

                    // check hovered
                    if (nIdx != hoveredListIndex)
                    {
                        if (hoveredListIndex != -1)
                        {
                            invalidateItemBounds(
                                directoryListBox.GetItemRectangle(hoveredListIndex));
                        }

                        hoveredListIndex = nIdx;

                        invalidateItemBounds(
                            directoryListBox.GetItemRectangle(hoveredListIndex));
                    }
                }

                if ( ! toolTip.GetToolTip(directoryListBox).Equals(listBoxToolTip))
                {
                    toolTip.SetToolTip(directoryListBox, listBoxToolTip);
                }
            }
            catch {}
        }
        private void DirectoryListBox_MouseLeave(object sender, EventArgs e)
        {
            try
            {
                if (hoveredListIndex != -1)
                {
                    invalidateItemBounds(directoryListBox.GetItemRectangle(hoveredListIndex));
                }

                hoveredListIndex = -1;
            }
            catch {}
        }

        private void invalidateItemBounds(Rectangle bounds)
        {
            bounds.X = 0;
            bounds.Inflate(directoryListBox.Width, 0);
            directoryListBox.Invalidate(bounds);
        }

        private bool in_tabTreeView_KeyDown = false;

        private void treeView_KeyDown(object unusedSender, KeyEventArgs e)
        {
            if (tabControl.Focused || in_tabTreeView_KeyDown) return;
            in_tabTreeView_KeyDown = true;

            try
            {
                switch (e.KeyData)
                {
                    case Keys.Right:
                        if (directoryTreeView.SelectedNode           != null &&
                            directoryTreeView.SelectedNode.FirstNode == null)
                        {
				            e.Handled = true;
                            directoryListBox.Focus();
                        }
                        break;
			        case Keys.Enter:
				        e.Handled = true;
                        doTransfer();
                        break;
                    case Keys.Escape:
				        e.Handled = true;
				        this.Close();
                        break;
                }
            }
            catch {}

            in_tabTreeView_KeyDown = false;
        }

		private void listBox_DoubleClick(object sender, System.EventArgs e)
		{
			doTransfer();
		}

        private void doTransfer()
        {
            string selectedNumber = "";
            string selectedDesc   = "";
            bool   numberEnabled  = false;

            if (tabControl.SelectedTab == dialTab)
            {
                if (phoneField.TextLength == 0) return;

                selectedNumber = phoneField.Text;
                selectedDesc   = PhonebookEntry.formatNumber(selectedNumber);
                numberEnabled  = true;
            }
            else if (tabControl.SelectedTab == directoryTab)
            {
                if (directoryListBox.SelectedItem == null) return;

                PhonebookEntry selected = (PhonebookEntry)directoryListBox.SelectedItem;

                selectedNumber = selected.Number;
                selectedDesc   = selected.Desc;
                numberEnabled  = selected.Enabled;

                if ( ! numberEnabled)
                {
				    MessageBox.Show(this, selected.NumberFormatted +
                        (Translate)(" is disabled on this type of Trunk"),
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    return;
                }
            }
            else
            {
                return;
            }

            // ask for connection type
            TransferSelectionDialog transferSelectionDialog = new TransferSelectionDialog(selectedDesc);

            transferSelectionDialog.blindButton.      Visible = showBlindTransferButton;
            transferSelectionDialog.attendedButton.   Visible = showAttendedTransferButton;
            transferSelectionDialog.conferenceButton. Visible = showConferenceTransferButton;
            transferSelectionDialog.tandemButton.     Visible = showTandemTransferButton;
            transferSelectionDialog.flashButton.      Visible = showFlashTransferButton;
            transferSelectionDialog.dialButton.       Visible = showSpeedDialButton;

            transferSelectionDialog.blindButton.      Enabled = BlindEnabled;
            transferSelectionDialog.attendedButton.   Enabled = AttendedEnabled;
            transferSelectionDialog.conferenceButton. Enabled = ConferenceEnabled;
            transferSelectionDialog.tandemButton.     Enabled = TandemEnabled;
            transferSelectionDialog.flashButton.      Enabled = FlashEnabled;
            transferSelectionDialog.dialButton.       Enabled = DialOutEnabled;

            DialogResult result = transferSelectionDialog.ShowDialog(this);

            if (result == DialogResult.Cancel) return;

            currentTransferType = transferSelectionDialog.transferType;

            if (currentTransferType == Controller.TransferType.Dial)
            {
                controller.sendDialCommand(selectedNumber);

                resolveTransferButtonsState();

                // TODO phonebook: display cancel dialog for Dial?
            }
            else
            {
				TransferTo(selectedNumber, currentTransferType);

                resolveTransferButtonsState();

                displayPostTransferDialog(this, controller, currentTransferType, selectedDesc, selectedNumber);
                resolveTransferButtonsState();
            }
        }

        public static void displayPostTransferDialog(
            IWin32Window            owner,
            Controller              controller,
            Controller.TransferType transferType,
            string                  desc,
            string                  number
        )
        {
            // display dialog only for conference and attended
            if (transferType == Controller.TransferType.Conference || transferType == Controller.TransferType.Attended)
            {
                for (int i = 0; i < 20; ++i)
                {
                    string transferTypeStr =
                        (Translate)((transferType == Controller.TransferType.Tandem) ? "9-1-1" : transferType.ToString());

                    confirmCancelDialog = new ConfirmCancelDialog(
                        owner, (Translate)("Transferring to ") + desc +
                              " (" + transferTypeStr + ") ...", Application.ProductName);

                    ((Button)confirmCancelDialog.AcceptButton).Enabled = false;
                    ((Button)confirmCancelDialog.AcceptButton).Visible = (transferType == Controller.TransferType.Attended);

                    DialogResult Result = confirmCancelDialog.ShowDialog(owner);
                    confirmCancelDialog = null;

                    if (Result == DialogResult.No)
                    {
                        // confirm canceling the transfer
                        Result = MessageBox.Show(owner, (Translate)("Stop ") + transferTypeStr + (Translate)(" transfer to ") + desc + "?",
                            Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

                        if (Result == DialogResult.Yes) controller.CancelTransfer(number, transferType);
                        else                            continue;
                    }
                    else if (Result == DialogResult.Yes)
                    {
                        controller.CompleteTransfer(number);
                    }

                    break;
                }
            }
        }
		
		#region Button Event Handlers

        private void transferButton_Click(object sender, System.EventArgs e)
		{
            try
            {
                ToolStripButton button = (ToolStripButton)sender;
                TransferConfirm((Controller.TransferType)button.Tag);
            }
            catch {}
		}

		#endregion


        // Add this to the dial pad tab
		private void listBox_SelectedIndexChanged(object sender, System.EventArgs e)
		{
            // make sure the menus are enabled correctly
            setEnabled(transferEnabled, currentCallId);
		}

		private void TransferConfirm(Controller.TransferType transferType)
		{
            if ( ! transferEnabled && transferType != Controller.TransferType.Dial)
            {
                MessageBox.Show(this, (Translate)("Transfering is disabled") +
                    (Translate)(" (check the currently selected call)"),
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                return;
            }

            currentTransferType = transferType;

            string selectedNumber = "";
            string selectedDesc   = "";
            bool   numberEnabled  = false;

			if (tabControl.SelectedTab == dialTab)
            {
                if (phoneField.Text.Length == 0) return;

                selectedNumber = phoneField.Text;
                selectedDesc   = PhonebookEntry.formatNumber(selectedNumber);
                numberEnabled  = true;
            }
            else if (tabControl.SelectedTab == directoryTab)
            {
                if (directoryListBox.SelectedItem == null) return;

                PhonebookEntry selected = (PhonebookEntry)directoryListBox.SelectedItem;

                selectedNumber = selected.Number;
                selectedDesc   = selected.Desc;
                numberEnabled  = selected.Enabled;

                if ( ! numberEnabled)
                {
				    MessageBox.Show(this, selected.NumberFormatted +
                        (Translate)(" is disabled on this type of Trunk"),
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    return;
                }
            }
            else
            {
                return;
            }

            string msgStr;

            string transferTypeStr =
                (Translate)((transferType == Controller.TransferType.Tandem) ? "9-1-1" : transferType.ToString());

            if (transferType == Controller.TransferType.Dial) msgStr = (Translate)("Dial ");
            else                                              msgStr = transferTypeStr + (Translate)(" Transfer to ");

            DialogResult Result = MessageBox.Show(this, msgStr + selectedDesc + "?",
                Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

            if (Result == DialogResult.Yes)
			{
                if (transferType == Controller.TransferType.Dial)
                {
                    controller.sendDialCommand(selectedNumber);

                    resolveTransferButtonsState();

                    // TODO phonebook: display cancel dialog?
                }
                else
                {
					TransferTo(selectedNumber, transferType);

                    resolveTransferButtonsState();

                    displayPostTransferDialog(this, controller, transferType, selectedDesc, selectedNumber);
                    resolveTransferButtonsState();
                }
            }
		}

		public void TransferTo(string PhoneNumber, Controller.TransferType transferType)
		{
            if ( ! transferEnabled) return;

            controller.TransferTo(PhoneNumber, transferType);
		}

        public void reset(string callId, bool successful)
        {
            if (currentCallId == callId || callId == null)
            {
                if (confirmCancelDialog != null)
                {
                    if (currentTransferType == Controller.TransferType.Attended && successful)
                    {
                        // enable the complete transfer button
                        ((Button)confirmCancelDialog.AcceptButton).Enabled = true;
                    }
                    else
                    {
                        confirmCancelDialog.Close();
                    }
                }
            }
        }

        // this reset() is called from the Console when the Phonebook button is clicked
        public static void reset()
        {
            if (confirmCancelDialog != null) confirmCancelDialog.Close();
        }

		private void Transfer_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Escape)
			{
				e.Handled = true;
				this.Close();
			}
		}

		private void menuClose_ItemClick(object sender, EventArgs e)
		{
			Hide();
		}

		public void Stop()
		{
			try
			{
				Logging.AppTrace("Stopping Phonebook");
				systemShutdown = true;
				Close();
			}
			catch (Exception ex)
			{
				Logging.ExceptionLogger("Error in Phonebook.Stop", ex, Console.LogID.Cleanup);
				Logging.AppTrace("Error Stopping Phonebook:" + ex.ToString());
			}
		}

		private void menuHelpAbout_ItemClick(object sender, EventArgs e)
		{
			Splash.ShowAbout(controller.Position);
		}

		private void usersManualMenu_Click(object sender, EventArgs e)
		{
			Console.ShowUsersManual();
		}

		private void transferButton_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Escape)
            {
                e.Handled = true;
                this.Close();
            }
		}

		private void listBox_DataSourceChanged(object sender, System.EventArgs e)
		{
            ListBox senderListBox = (ListBox)sender;

            bool enabled = (senderListBox.DataSource != null && senderListBox.Items.Count > 0);

            if (enabled) senderListBox.SelectedIndex = 0;

            printButton.Enabled = enabled;

            // make sure the menus are enabled correctly
            setEnabled(transferEnabled, currentCallId);
		}

        public void setDefaultDial(string defaultDestNumber)
        {
            phoneField.Text = defaultDestNumber;
            phoneFieldLeftKeyCount = 0;

            resolveTransferButtonsState();
        }

		private void phoneField_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            e.Handled = ! phoneFieldCharIsValid(e.KeyChar);
            phoneFieldLeftKeyCount = 0;
        }

        private bool phoneFieldCharIsValid(char c)
		{
			Logging.AppTrace( "KeyChar:" + Convert.ToInt32(c));

            // TODO SIPVoipSDK: are all characters okay for SIP addresses?

            return true;
		}

        private int phoneFieldLeftKeyCount = 0;

		private void phoneField_KeyRelease(object sender, System.Windows.Forms.KeyEventArgs e)
		{
            resolveTransferButtonsState();

            if (e.KeyCode == Keys.Left)
            {
                // if the cursor is at the beginning of the field
                if (phoneField.SelectionStart == 0 && phoneField.SelectionLength == 0)
                {
                    ++phoneFieldLeftKeyCount;

                    // if Left is pressed twice, switch tabs
                    if ((phoneField.TextLength == 0 && phoneFieldLeftKeyCount == 2) ||
                        (phoneField.TextLength != 0 && phoneFieldLeftKeyCount == 3))
                    {
                        // TODO gui: determine if the feature to Left Arrow to the Directory Tab should be completely removed
                        //if (tabControl.SelectedIndex != 0) tabControl.SelectedIndex--;
                        phoneFieldLeftKeyCount = 0;
                    }
                }
            }
            else
            {
                phoneFieldLeftKeyCount = 0;
            }
        }

        private void phoneField_SelectedIndexChanged(object sender, EventArgs e)
        {
            resolveTransferButtonsState();
        }

        private void dialButton_Clicked(object sender, EventArgs e)
        {
            if (sender == null) return;

            phoneField.Focus();

            Button button = (Button)sender;

            if (phoneFieldCharIsValid(button.Tag.ToString()[0]))
            {
                int insertionIndex = phoneField.SelectionStart;

                string selectedText = phoneField.SelectedText;

                string newText  = phoneField.Text.Remove(phoneField.SelectionStart, phoneField.SelectionLength);
                phoneField.Text = newText.Insert(insertionIndex, button.Tag.ToString()[0].ToString());
                phoneFieldLeftKeyCount = 0;

                phoneField.SelectionStart = insertionIndex + 1;

                resolveTransferButtonsState();
            }
        }
        private void dialButton_MouseEnter(object sender, EventArgs e)
        {
            if (sender != null)
            {
                Button button = (Button)sender;
                button.ForeColor = dialpadHoverColorInfo.foregroundColor;
                button.BackColor = dialpadHoverColorInfo.backgroundColor1;
            }
        }
        private void dialButton_MouseLeave(object sender, EventArgs e)
        {
            if (sender != null)
            {
                Button button = (Button)sender;
                button.ForeColor = dialpadColorInfo.foregroundColor;
                button.BackColor = dialpadColorInfo.backgroundColor1;
            }
        }

		private void callButtonClick(object sender, System.EventArgs e)
		{
            if (phoneField.Text.Length != 0)
            {
                // remember dialed numbers
                dialHistory.Remove(phoneField.Text);
                dialHistory.Insert(0, phoneField.Text);

                // TODO calls: should we add the call history back someday?  (Bill didn't like it...)
                //phoneField.AutoCompleteCustomSource = dialHistory;

                string[] dialHistoryStrList = new string[dialHistory.Count];

                for (int i = 0; i < dialHistory.Count; ++i) dialHistoryStrList[i] = dialHistory[i];

                // TODO calls: should we add the call history back someday?  (Bill didn't like it...)
                //phoneField.Items.Clear();
                //phoneField.Items.AddRange(dialHistoryStrList);

                if (regSettings != null) regSettings.SetValue("DialHistory", dialHistoryStrList);

                controller.sendDialCommand(phoneField.Text);

                Close();
            }
        }

        private void clearDialHistory(object sender, EventArgs e)
        {
            dialHistory.Clear();

            // TODO calls: should we add the call history back someday?  (Bill didn't like it...)
            //phoneField.AutoCompleteCustomSource.Clear();
            //phoneField.Items.Clear();

            if (regSettings != null) regSettings.DeleteValue("DialHistory");
        }

		#region Printing
		private void printDoc_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
		{
            if (tabControl.SelectedTab == dialTab || tabControl.SelectedTab == null) return;

            printerFont = new Font("Lucida Console", fontSize );
			printerBrush = new SolidBrush(Color.Black);
			LinePosition = TopMargin;

			printerFont = new Font(printerFont,FontStyle.Bold);
            WriteToPrinter(e.Graphics, "Phonebook List - " + directoryTreeView.SelectedNode.Name);
			printerFont = new Font(printerFont,FontStyle.Regular);

			e.Graphics.DrawLine(Pens.Black,new PointF(LeftMargin,LinePosition),new PointF(e.PageBounds.Width - LeftMargin,LinePosition));
			LinePosition += 5;

			printerFont = new Font(printerFont,FontStyle.Regular);

            foreach (Object item in directoryListBox.Items)
			{
				PhonebookEntry tranRec = (PhonebookEntry)item;
				WriteToPrinter(e.Graphics, tranRec.Name.PadRight(45) + " " + tranRec.Number);
			}
		}

		public void WriteToPrinter(Graphics graphic, string Text)
		{
			graphic.DrawString(Text,printerFont, printerBrush, (float) LeftMargin, LinePosition);
			LinePosition += printerFont.Height + 5;
		}


		private void printMenu_ItemClick(object sender, EventArgs e)
		{
			if (printSetup.ShowDialog() == DialogResult.OK)
				printDoc.Print();
		}

		#endregion

        public void setEnabled(bool enabled, string callId)
        {
            // don't do this check, otherwise, menus get enabled incorrectly
            //if (transferEnabled == enabled) return;

            if (currentCallId != callId) currentCallId = callId;

            if ( ! enabled)
            {
                // TODO gui: close confirmation dialogs if any are currenly shown
            }

            transferEnabled = enabled;

            resolveTransferButtonsState();
        }

        private void resolveTransferButtonsState()
        {
            blindButton.      Enabled = blindMenu.      Enabled = BlindEnabled;
            attendedButton.   Enabled = attendedMenu.   Enabled = AttendedEnabled;
            conferenceButton. Enabled = conferenceMenu. Enabled = ConferenceEnabled;
            tandemButton.     Enabled = tandemMenu.     Enabled = TandemEnabled;
            flashButton.      Enabled = flashMenu.      Enabled = FlashEnabled;
            speedDialButton.  Enabled = speedDialMenu.  Enabled = DialOutEnabled;

            bool printEnabled = false;

            if (tabControl.SelectedTab == directoryTab)
            {
                printEnabled = (directoryListBox.DataSource != null &&
                                directoryListBox.Items.Count > 0);
            }

            printButton.Enabled = printMenu.Enabled = printEnabled;
        }

        private bool isButtonEnabled(PhonebookEntry.EnableCode code)
        {
            if (tabControl.SelectedTab == dialTab) return (phoneField.Text.Length != 0);

            if (tabControl.SelectedTab != directoryTab) return false;

            if (directoryListBox == null || directoryListBox.SelectedItem == null) return false;

            int selectedCode = ((PhonebookEntry)directoryListBox.SelectedItem).Code;

            return (selectedCode == 0) || (code.GetHashCode() & selectedCode) != 0;
        }

        private bool BlindEnabled      {get {return blindEnabled      && isButtonEnabled(PhonebookEntry.EnableCode.Blind)      && transferEnabled;} }
        private bool AttendedEnabled   {get {return attendedEnabled   && isButtonEnabled(PhonebookEntry.EnableCode.Attended)   && transferEnabled;} }
        private bool ConferenceEnabled {get {return conferenceEnabled && isButtonEnabled(PhonebookEntry.EnableCode.Conference) && transferEnabled;} }
        private bool TandemEnabled     {get {return tandemEnabled     && isButtonEnabled(PhonebookEntry.EnableCode.Tandem)     && transferEnabled;} }
        private bool FlashEnabled      {get {return flashEnabled      && isButtonEnabled(PhonebookEntry.EnableCode.Flash)      && transferEnabled;} }
        private bool DialOutEnabled    {get {return                      isButtonEnabled(PhonebookEntry.EnableCode.Dial_Out);} }

        private void backButton_Click(object sender, EventArgs e)
        {
            phoneField.Focus();

            int selectionStart  = phoneField.SelectionStart;
            int selectionLength = phoneField.SelectionLength;

            if (selectionLength == 0)
            {
                if (phoneField.SelectionStart != 0)
                {
                    --selectionStart;
                    selectionLength = 1;
                }
                else
                {
                    return;
                }
            }

            string newText = phoneField.Text.Remove(selectionStart, selectionLength);
            phoneField.Text = newText;

            phoneField.SelectionStart = selectionStart;
            phoneFieldLeftKeyCount    = 0;

            resolveTransferButtonsState();
        }

        System.Windows.Forms.Timer tabChangedTimer = new System.Windows.Forms.Timer();

        private void tabContent_VisibleChanged(object sender, EventArgs e)
        {
            tabChangedTimer.Stop();
            tabChangedTimer.Tick += new EventHandler(tabChangedTimer_Tick);
            tabChangedTimer.Interval = 10;
            tabChangedTimer.Start();
        }

        private void tabChangedTimer_Tick(Object unusedSender, EventArgs unusedArgs)
        {
            tabChangedTimer.Stop();

            tabChanged(this, new TabControlEventArgs(
                tabControl.SelectedTab, tabControl.SelectedIndex, TabControlAction.Selected));
        }

        static TreeNode hoveredTreeNode = null;

        private void DirectoryTreeView_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                TreeNode node = directoryTreeView.GetNodeAt(e.Location);

                if (node != hoveredTreeNode)
                {
                    if (hoveredTreeNode != null)
                    {
                        invalidateNodeBounds(hoveredTreeNode.Bounds);
                    }

                    hoveredTreeNode = node;
                    invalidateNodeBounds(hoveredTreeNode.Bounds);
                }
            }
            catch {}
        }

        private void DirectoryTreeView_MouseLeave(object sender, EventArgs e)
        {
            try
            {
                if (hoveredTreeNode != null) invalidateNodeBounds(hoveredTreeNode.Bounds);

                hoveredTreeNode = null;
            }
            catch {}
        }

        private void invalidateNodeBounds(Rectangle bounds)
        {
            bounds.X = 0;
            bounds.Inflate(directoryTreeView.Width, 0);
            directoryTreeView.Invalidate(bounds);
        }

        private void directoryTreeView_DrawNode(object sender, DrawTreeNodeEventArgs e)
        {
            bool mouseClicking = (directoryTreeView.Focused && MouseButtons == System.Windows.Forms.MouseButtons.Left);

            bool isSelected = ((   mouseClicking && (e.State & TreeNodeStates.Focused) != 0) ||
                               ( ! mouseClicking &&  e.Node.IsSelected));

            bool isHovered = (e.Node == hoveredTreeNode);

            Console.CallRowInfo.State state = Console.CallRowInfo.getState(isSelected, isHovered);

            Settings.ColorInfo colorInfo;

            if (directoryTreeView.Focused) colorInfo = directoryGroupsColorInfoActive   [(int)state];
            else                           colorInfo = directoryGroupsColorInfoInactive [(int)state];

            Brush fillBrush = colorInfo.getBackgroundBrush(e.Bounds, LinearGradientMode.Horizontal);

            e.Graphics.FillRectangle(fillBrush, e.Bounds);

            if (state == Console.CallRowInfo.State.Normal && showPhonebookAsButtons)
            {
                Color borderColor = Color.FromArgb(150,
                    directoryGroupsColorInfoActive[(int)state].foregroundColor);

                Rectangle borderRect = e.Bounds;
                borderRect.Width    -= 1;

                e.Graphics.DrawRectangle(new Pen(borderColor), borderRect);
            }

            int iconWidth = 0;

            if (e.Node.ImageKey != null && System.IO.File.Exists(e.Node.ImageKey))
            {
                iconWidth = iconSpace;

                RectangleF rect = e.Bounds;
                rect.Height     = iconSize;
                rect.Width      = iconSize;
                rect.X         += iconSize / 4;
                rect.Y         += (e.Bounds.Height - iconSize) / 2.0f;

                Image image = Image.FromFile(e.Node.ImageKey);
                e.Graphics.DrawImage(image, rect);
            }

            StringFormat format  = new StringFormat();
            format.Alignment     = StringAlignment.Near;
            format.LineAlignment = StringAlignment.Center;
            format.Trimming      = StringTrimming.EllipsisCharacter;

            Rectangle textRect = e.Bounds;
            textRect.X += (iconSize / 4) + iconWidth;
            textRect.Width = maxGroupStrWidth;

            bool scrollbarIsVisible =
                ((directoryTreeView.Nodes.Count * directoryTreeView.ItemHeight) > directoryTreeView.Height);

            if (scrollbarIsVisible) textRect.Width -= SystemInformation.VerticalScrollBarWidth;

            Font font = new Font(directoryTreeView.Font, colorInfo.bold ? FontStyle.Bold : FontStyle.Regular);

            e.Graphics.DrawString(e.Node.Name, font, colorInfo.getForegroundBrush(), textRect, format);
        }

        private void listBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e == null || e.Index == -1) return;

            PhonebookEntry item = (PhonebookEntry)directoryListBox.Items[e.Index];

            bool isSelected = e.State.HasFlag(DrawItemState.Selected) && item.Enabled;
            bool isHovered  = (e.Index == hoveredListIndex);

            Console.CallRowInfo.State state = Console.CallRowInfo.getState(isSelected, isHovered);

            Settings.ColorInfo colorInfo;

            if (directoryListBox.Focused) colorInfo = directoryListColorInfoActive   [(int)state];
            else                          colorInfo = directoryListColorInfoInactive [(int)state];

            Brush fillBrush = colorInfo.getBackgroundBrush(e.Bounds, LinearGradientMode.Horizontal);

            e.Graphics.FillRectangle(fillBrush, e.Bounds);

            // selected
            if (state == Console.CallRowInfo.State.Normal && showPhonebookAsButtons)
            {
                Color borderColor = Color.FromArgb(150,
                    directoryListColorInfoActive[(int)state].foregroundColor);

                Rectangle borderRect = e.Bounds;
                borderRect.Width    -= 1;

                e.Graphics.DrawRectangle(new Pen(borderColor), borderRect);
            }

            int iconWidth = 0;

            if (item.Image != null)
            {
                iconWidth = iconSpace;

                RectangleF rect = e.Bounds;
                rect.Height     = iconSize;
                rect.Width      = iconSize;
                rect.X         += iconSize / 4;
                rect.Y         += (e.Bounds.Height - iconSize) / 2.0f;

                e.Graphics.DrawImage(item.Image, rect);
            }

            StringFormat format  = new StringFormat();
            format.Alignment     = StringAlignment.Near;
            format.LineAlignment = StringAlignment.Center;
            format.Trimming      = StringTrimming.EllipsisCharacter;

            Rectangle textRect = e.Bounds;
            textRect.X += (iconSize / 4) + iconWidth;
            textRect.Width = maxNameStrWidth;

            bool scrollbarIsVisible =
                ((directoryListBox.Items.Count * directoryListBox.ItemHeight) > directoryListBox.Height);

            if (scrollbarIsVisible) textRect.Width -= SystemInformation.VerticalScrollBarWidth;

            Brush textBrush = colorInfo.getForegroundBrush();

            if ( ! item.Enabled) textBrush = new SolidBrush(Color.FromArgb(130, colorInfo.foregroundColor));

            Font font = new Font(directoryListBox.Font, colorInfo.bold ? FontStyle.Bold : FontStyle.Regular);

            e.Graphics.DrawString(item.Name, font, textBrush, textRect, format);
        }

        private void directoryTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                if (allEntries.ContainsKey(e.Node.Name)) directoryListBox.DataSource = allEntries[e.Node.Name];
                else                                     directoryListBox.DataSource = null;
            }
            catch
            {
                directoryListBox.DataSource = null;
            }

            resolveTransferButtonsState();
        }
    }

	public struct PhonebookEntry
	{
        public enum EnableCode
        {
            All        = 0,
            Blind      = 2,
            Attended   = 4,
            Conference = 8,
            Tandem     = 16,
            Dial_Out   = 32,
            Flash      = 64,
        }

        private string                  name;
		private string                  number;
		private string                  numberFormatted;
        private string                  desc;
		private string                  group;
        private int                     code;
        private bool                    enabled;
        private Image                   image;
        private Controller.TransferType speedDialAction;

        public string                  Name            {get {return name;            } }
        public string                  Number          {get {return number;          } }
        public string                  NumberFormatted {get {return numberFormatted; } }
        public string                  Desc            {get {return desc;            } }
        public string                  Group           {get {return group;           } }
        public int                     Code            {get {return code;            } }
        public bool                    Enabled         {get {return enabled;} set {enabled = value;} }
        public Image                   Image           {get {return image;           } }
        public Controller.TransferType SpeedDialAction {get {return speedDialAction; } }

		public PhonebookEntry(string NewName, string NewNumber, string NewGroup, int NewCode, string imageFile, string NewSpeedDialAction)
		{
            if (NewGroup.ToLower() == "all") NewGroup = "All";

			name            = NewName;
			number          = NewNumber;
			group           = NewGroup;
            code            = NewCode;
            enabled         = true;
            image           = null;
            speedDialAction = Controller.TransferType.Dial;

            if (imageFile != null && System.IO.File.Exists(imageFile)) image = Image.FromFile(imageFile);

            switch (NewSpeedDialAction.ToLower())
            {
                case "blind_transfer":      speedDialAction = Controller.TransferType.Blind;      break;
                case "attended_transfer":   speedDialAction = Controller.TransferType.Attended;   break;
                case "conference_transfer": speedDialAction = Controller.TransferType.Conference; break;
                case "tandem_transfer":     speedDialAction = Controller.TransferType.Tandem;     break;
                case "dial_out":            speedDialAction = Controller.TransferType.Dial;       break;
                default:                    speedDialAction = Controller.TransferType.Dial;       break;
            }

            numberFormatted = formatNumber(number);

            // set description
            if (name.Length == 0)
            {
                desc = numberFormatted;
            }
            else
            {
                desc = name;

                if (name != number)
                {
                    desc += " \"" + numberFormatted + "\"";
                }
            }
        }

        public static string formatNumber(string number)
        {
            bool isAllNumbers = true;
            int  numXs        = 0;
            int  xIndex       = -1;

            int i = 0;

            foreach (char c in number)
            {
                if (c < '0' || c > '9')
                {
                    if (c == 'x' || c == 'X')
                    {
                        ++numXs;
                        xIndex = i;
                    }
                    else
                    {
                        isAllNumbers = false;
                        break;
                    }
                }

                ++i;
            }

            if (isAllNumbers)
            {
                if (numXs == 0)
                {
                    switch (number.Length)
                    {
                        case 7:  return Convert.ToInt64(number).ToString("###-####");
                        case 10: return Convert.ToInt64(number).ToString("(###) ###-####");
                        case 11: return Convert.ToInt64(number).ToString("+# (###) ###-####");
                    }
                }
                else if (numXs == 1)
                {
                    string ext = number.Substring(xIndex + 1);
                    number     = number.Substring(0, xIndex);

                    switch (number.Length)
                    {
                        case 7:  return Convert.ToInt64(number).ToString("###-#### x")          + ext;
                        case 10: return Convert.ToInt64(number).ToString("(###) ###-#### x")    + ext;
                        case 11: return Convert.ToInt64(number).ToString("+# (###) ###-#### x") + ext;
                    }
                }
            }

            return number;
        }
    }

	class PhonebookEntrySort : IComparer
	{   
		public PhonebookEntrySort() {}

		public int Compare(object x,object y)
		{
			return ((PhonebookEntry)x).Name.CompareTo(((PhonebookEntry)y).Name);                
		}
	}

    public struct GroupEntry
	{
        private string name;
        private int    priority;
        private string imagePath;

        public string Name      {get {return name;      } }
        public int    Priority  {get {return priority;  } }
        public string ImagePath {get {return imagePath; } }

		public GroupEntry(string NewName, int NewPriority, string NewImagePath)
		{
			name      = NewName;
            priority  = NewPriority;
            imagePath = NewImagePath;
        }

        public bool isSpeedDialGroup() {return isSpeedDialGroup(Name);}

        public static bool isSpeedDialGroup(string name)
        {
            return name.ToLower().Replace(" ", "").StartsWith("speeddial");
        }
    }
}
