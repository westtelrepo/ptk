
using System.Drawing;
using System.Windows.Forms;

namespace WestTel.E911
{
    public class BaseForm : Form
    {
        public BaseForm()
        {
            InitializeComponent();

            try {CreateHandle();} catch {}
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // BaseForm
            // 
            this.ClientSize = new Size(284, 261);
            this.MaximizeBox = false;
            this.Name = "BaseForm";
            this.SizeGripStyle = SizeGripStyle.Hide;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(BaseForm_Load);
            this.FormClosing += new FormClosingEventHandler(this.BaseForm_FormClosing);
            this.ResumeLayout(false);

        }
        #endregion

        public void SetupForm()
        {
            // restore window position
            try
            {
                // NOTE: use non-translated company name here
                Microsoft.Win32.RegistryKey regSettings = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(
                    "Software\\" + System.Windows.Forms.Application.CompanyName + "\\PSAP Toolkit");

                if (regSettings != null)
                {
                    object positionXObj = regSettings.GetValue(Name + "_X");
                    object positionYObj = regSettings.GetValue(Name + "_Y");

                    if (positionXObj != null && positionYObj != null)
                    {
                        int positionX = System.Convert.ToInt32(positionXObj);
                        int positionY = System.Convert.ToInt32(positionYObj);

                        if (positionX < SystemInformation.VirtualScreen.X)
                        {
                            positionX = SystemInformation.VirtualScreen.X;
                        }

                        if (positionY < SystemInformation.VirtualScreen.Y)
                        {
                            positionY = SystemInformation.VirtualScreen.Y;
                        }

                        SetDesktopLocation(positionX, positionY);
                    }

                    object sizeWidthObj  = regSettings.GetValue(Name + "_Width");
                    object sizeHeightObj = regSettings.GetValue(Name + "_Height");

                    if (sizeWidthObj != null && sizeHeightObj != null)
                    {
                        int sizeWidth  = System.Convert.ToInt32(sizeWidthObj);
                        int sizeHeight = System.Convert.ToInt32(sizeHeightObj);

                        if (DesktopLocation.X + sizeWidth > SystemInformation.VirtualScreen.Right)
                        {
                            sizeWidth = SystemInformation.VirtualScreen.Right - DesktopLocation.X;
                        }

                        if (DesktopLocation.Y + sizeHeight > SystemInformation.VirtualScreen.Bottom)
                        {
                            sizeHeight = SystemInformation.VirtualScreen.Bottom - DesktopLocation.Y;
                        }

                        SetDesktopBounds(DesktopLocation.X, DesktopLocation.Y, sizeWidth, sizeHeight);
                    }

                    object positionShownObj = regSettings.GetValue(Name + "_Shown");
                    object positionStateObj = regSettings.GetValue(Name + "_State");

                    if (positionShownObj != null && positionStateObj != null)
                    {
                        FormWindowState state = (FormWindowState)System.Convert.ToInt32(positionStateObj);
                        bool            shown = System.Convert.ToBoolean(positionShownObj);

                        // restore the state before the visiblity so that
                        // windows don't flash up before getting minimized
                        WindowState = state;
                        Visible     = shown;
                    }
                }
            }
            catch {}
        }

        public static void removeUnnecessarySeparators(ToolStrip container) // could be a menu
        {
            try
            {
                if (container == null) return;

                bool lastVisibleItemIsSeparator = false;
                int  lastVisibleItemIndex       = -1;

                foreach (ToolStripItem item in container.Items)
                {
                    if ( ! item.Visible) continue;

                    bool thisItemIsSeparator = (item is ToolStripSeparator);

                    if (thisItemIsSeparator && lastVisibleItemIsSeparator)
                    {
                        container.Items[lastVisibleItemIndex].Visible = false;
                    }

                    lastVisibleItemIsSeparator = thisItemIsSeparator;
                    lastVisibleItemIndex       = container.Items.IndexOf(item);

                    if (item is ToolStripMenuItem)
                    {
                        ((ToolStripMenuItem)item).DropDownOpened += new System.EventHandler(Menu_Opened);
                    }
                }

                if (lastVisibleItemIsSeparator) container.Items[lastVisibleItemIndex].Visible = false;
            }
            catch {}
        }

        public static void removeUnnecessarySeparators(ToolStripMenuItem container)
        {
            try
            {
                if (container == null) return;

                bool lastVisibleItemIsSeparator = false;
                int  lastVisibleItemIndex       = -1;

                foreach (ToolStripItem item in container.DropDownItems)
                {
                    if ( ! item.Visible) continue;

                    bool thisItemIsSeparator = (item is ToolStripSeparator);

                    if (thisItemIsSeparator && lastVisibleItemIsSeparator)
                    {
                        container.DropDownItems[lastVisibleItemIndex].Visible = false;
                    }

                    lastVisibleItemIsSeparator = thisItemIsSeparator;
                    lastVisibleItemIndex       = container.DropDownItems.IndexOf(item);
                }

                if (lastVisibleItemIsSeparator) container.DropDownItems[lastVisibleItemIndex].Visible = false;
            }
            catch {}
        }

        private void BaseForm_Load(object sender, System.EventArgs e)
        {
            removeUnnecessarySeparators(this.MainMenuStrip);
        }

        private static void Menu_Opened(object sender, System.EventArgs e)
        {
            removeUnnecessarySeparators((ToolStripMenuItem)sender);
        }

        private void BaseForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                // NOTE: use non-translated company name here
                Microsoft.Win32.RegistryKey regSettings = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(
                    "Software\\" + System.Windows.Forms.Application.CompanyName + "\\PSAP Toolkit");

                if (regSettings != null)
                {
                    if (WindowState == FormWindowState.Normal)
                    {
                        regSettings.SetValue(Name + "_X",      DesktopLocation.X);
                        regSettings.SetValue(Name + "_Y",      DesktopLocation.Y);
                        regSettings.SetValue(Name + "_Width",  DesktopBounds.Width);
                        regSettings.SetValue(Name + "_Height", DesktopBounds.Height);
                    }

                    regSettings.SetValue(Name + "_Shown", Visible);
                    regSettings.SetValue(Name + "_State", (int)WindowState);
                }
            }
            catch {}
        }
    }
}
