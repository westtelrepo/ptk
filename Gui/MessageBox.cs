﻿using System;
using System.Windows.Forms;

namespace WestTel.E911
{
	public class MessageBox : System.Windows.Forms.Form
    {
        private Button button2;
        private Button button1;
        private Label message;
        private TableLayoutPanel tableLayoutPanel2;
        private FlowLayoutPanel flowLayoutPanel1;
        private TableLayoutPanel tableLayoutPanel1;
        private FlowLayoutPanel flowLayoutPanel2;
        private Button button3;

#pragma warning disable 162 // disable warning about unreachable code

        public static DialogResult Show(
            IWin32Window      owner,
            string            text,
            string            caption,
            MessageBoxButtons buttons,
            MessageBoxIcon    icon
        )
        {
            return System.Windows.Forms.MessageBox.Show(owner, text, caption, buttons, icon);

            try
            {
                MessageBox m = new MessageBox(owner, text, caption, buttons, icon);
                m.ShowDialog(owner);

                return m.DialogResult;
            }
            catch {}

            return DialogResult.Cancel;
        }

        public static DialogResult Show(
            IWin32Window            owner,
            string                  text,
            string                  caption,
            MessageBoxButtons       buttons,
            MessageBoxIcon          icon,
            MessageBoxDefaultButton def
        )
        {
            return System.Windows.Forms.MessageBox.Show(owner, text, caption, buttons, icon, def);

            try
            {
                MessageBox m = new MessageBox(owner, text, caption, buttons, icon);

                if      (def == MessageBoxDefaultButton.Button1) m.AcceptButton = m.button1;
                else if (def == MessageBoxDefaultButton.Button2) m.AcceptButton = m.button2;
                else if (def == MessageBoxDefaultButton.Button3) m.AcceptButton = m.button3;

                m.ShowDialog(owner);

                return m.DialogResult;
            }
            catch {}

            return DialogResult.Cancel;
        }

        public static DialogResult Show(
            IWin32Window            owner,
            string                  text,
            string                  caption,
            MessageBoxButtons       buttons,
            MessageBoxIcon          icon,
            MessageBoxDefaultButton def,
            MessageBoxOptions       opt)
        {
            return System.Windows.Forms.MessageBox.Show(owner, text, caption, buttons, icon, def, opt);

            try
            {
                MessageBox m = new MessageBox(owner, text, caption, buttons, icon);

                if      (def == MessageBoxDefaultButton.Button1) m.AcceptButton = m.button1;
                else if (def == MessageBoxDefaultButton.Button2) m.AcceptButton = m.button2;
                else if (def == MessageBoxDefaultButton.Button3) m.AcceptButton = m.button3;

                // TODO: handle MessageBoxOptions opt

                m.ShowDialog(owner);

                return m.DialogResult;
            }
            catch {}

            return DialogResult.Cancel;
        }

#pragma warning restore 162

		private MessageBox(IWin32Window owner, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
		{
			// Required for Windows Form Designer support
			InitializeComponent();

            if (owner == null) StartPosition = FormStartPosition.CenterScreen;

            try
            {
                Text         = caption;
                message.Text = text;
            }
            catch {}

            // TODO gui: Add icons to message boxes
            try
            {
                switch (icon)
                {
                    case MessageBoxIcon.Information: icon = MessageBoxIcon.Information; break;
                    case MessageBoxIcon.Question:    icon = MessageBoxIcon.Question;    break;
                    case MessageBoxIcon.Warning:     icon = MessageBoxIcon.Warning;     break;
                    case MessageBoxIcon.Error:       icon = MessageBoxIcon.Error;       break;
                }
            }
            catch {}

            try
            {
                switch (buttons)
                {
                    case MessageBoxButtons.OK:

                        button1.Text = (Translate)("&OK");
                        button1.DialogResult = DialogResult.OK;

                        AcceptButton = button1;
                        CancelButton = button1;

                        break;

                    case MessageBoxButtons.OKCancel:

                        button1.Text = (Translate)("&OK");
                        button1.DialogResult = DialogResult.OK;

                        button2.Text = (Translate)("&Cancel");
                        button2.DialogResult = DialogResult.Cancel;
                        button2.Visible = true;

                        AcceptButton = button1;
                        CancelButton = button2;

                        break;

                    case MessageBoxButtons.AbortRetryIgnore:

                        button1.Text = (Translate)("&Abort");
                        button1.DialogResult = DialogResult.Abort;

                        button2.Text = (Translate)("&Retry");
                        button2.DialogResult = DialogResult.Retry;
                        button2.Visible = true;

                        button3.Text = (Translate)("&Ignore");
                        button3.DialogResult = DialogResult.Ignore;
                        button3.Visible = true;

                        AcceptButton = button2;
                        CancelButton = button3;

                        break;

                    case MessageBoxButtons.YesNoCancel:

                        button1.Text = (Translate)("&Yes");
                        button1.DialogResult = DialogResult.Yes;

                        button2.Text = (Translate)("&No");
                        button2.DialogResult = DialogResult.No;
                        button2.Visible = true;

                        button3.Text = (Translate)("&Cancel");
                        button3.DialogResult = DialogResult.Cancel;
                        button3.Visible = true;

                        AcceptButton = button1;
                        CancelButton = button3;

                        break;

                    case MessageBoxButtons.YesNo:

                        button1.Text = (Translate)("&Yes");
                        button1.DialogResult = DialogResult.Yes;

                        button2.Text = (Translate)("&No");
                        button2.DialogResult = DialogResult.No;
                        button2.Visible = true;

                        AcceptButton = button1;
                        CancelButton = button2;

                        break;

                    case MessageBoxButtons.RetryCancel:

                        button1.Text = (Translate)("&Retry");
                        button1.DialogResult = DialogResult.Retry;

                        button2.Text = (Translate)("&Cancel");
                        button2.DialogResult = DialogResult.Cancel;
                        button2.Visible = true;

                        AcceptButton = button1;
                        CancelButton = button2;

                        break;
                }
            }
            catch {}
        }

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.message = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.button3, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.button1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.button2, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(12, 83);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 6F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(247, 36);
            this.tableLayoutPanel2.TabIndex = 19;
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(159, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(60, 24);
            this.button3.TabIndex = 16;
            this.button3.Text = "Button3";
            this.button3.Visible = false;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(27, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(60, 24);
            this.button1.TabIndex = 1;
            this.button1.Text = "Button1";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(93, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(60, 24);
            this.button2.TabIndex = 2;
            this.button2.Text = "Button2";
            this.button2.Visible = false;
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.Dock = System.Windows.Forms.DockStyle.Fill;
            this.message.Location = new System.Drawing.Point(3, 0);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(233, 39);
            this.message.TabIndex = 15;
            this.message.Text = "This is a test of a really really really long message.....................that ju" +
                "st keeps going past the end of the line...........";
            this.message.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(0, 0);
            this.flowLayoutPanel1.TabIndex = 20;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 9F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 9F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(271, 122);
            this.tableLayoutPanel1.TabIndex = 21;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.message);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(12, 13);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(247, 64);
            this.flowLayoutPanel2.TabIndex = 22;
            // 
            // MessageBox
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(271, 122);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MessageBox";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Caption";
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
	}
}
