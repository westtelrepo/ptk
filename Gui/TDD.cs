using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Data;
using System.Diagnostics;

namespace Experient.E911
{
	/// <summary>
	/// Summary description for MainForm.
	/// </summary>
	public class TDD : BaseForm
	{
		private System.Threading.TimerCallback timerDelegate;
		private System.Threading.Timer workerThread = null;

        delegate void ButtonCheckDelegate();


		public TDDMessages messages;
		Controller controller = null;
		Brush printerBrush;
		Font printerFont;
		int fontSize = 10;
		float LinePosition = 10;
		float LeftMargin = 30;
		float TopMargin = 50;
		bool systemShutdown = false;
		

		#region Windows Control Declaraitons

        private ToolTip toolTip;
        private ToolTip toolTipInvalidChar;
        private IContainer components;

	#endregion

        //private System.Threading.Thread WaitThread;

		public bool KeepChecking = false;

		public Color SendTextColor = Color.Blue;
		public Color ReceivedTextColor = Color.Red;
		private System.Drawing.Printing.PrintDocument printDoc;
		private System.Windows.Forms.PrintDialog printSetup;
        private System.Windows.Forms.ContextMenu cmBlank;

		private bool SendingText = false;
        private Panel panel2;
        private TableLayoutPanel tableLayoutPanel1;
        private RichTextBox Conversation;
        private MenuStrip MainMenu;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem printMenu;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem closeMenu;
        private ToolStripMenuItem tddMenu;
        private ToolStripMenuItem messagesMenu;
        private ToolStripSeparator toolStripSeparator6;
        private ToolStripMenuItem muteMenu;
        private ToolStripSeparator toolStripSeparator5;
        private ToolStripMenuItem sendMenu;
        private ToolStripMenuItem helpToolStripMenuItem;
        private ToolStripMenuItem aboutMenu;
        private ToolStrip toolBar;
        private ToolStripButton printButton;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripButton messagesButton;
        private ToolStripSeparator muteSensitivitySeparator;
        private ToolStripButton muteButton;
        private ToolStripButton unmuteButton;
        private Button sendButton;
        private RichTextBox RealTime;
        private RichTextBox TextToSend;
        private Panel panel1;
        private Panel ConversationSidePanel;

        static private string callerSaysRegExp     = "";
        static private string dispatcherSaysRegExp = "";

        private bool needToSendTDDModeOn = false;

		public TDD(Controller c)
		{
			controller = c;
			
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

            setEnabled(false);

            timerDelegate = new System.Threading.TimerCallback(SendButtonCheck);
			workerThread = new System.Threading.Timer(timerDelegate, null,System.Threading.Timeout.Infinite,System.Threading.Timeout.Infinite);

            if (dispatcherSaysRegExp.Length == 0) setDispatcherSaysPattern ("Dispatcher{position-number} ({time}):");
            if (callerSaysRegExp.Length     == 0) setCallerSaysPattern     ("Caller ({time}):");
		}

        private const string openBracketRegEx  = " *\\[?\\(? *";
        private const string closeBracketRegEx = " *\\)?\\]? *";

        // allow optional brackets or parens
        private const string positionRegExp = openBracketRegEx + "P?[0-9]*" + closeBracketRegEx;

        // allow optional spaces and brackets or parens (also optional am/pm)
        private const string timeRegExp = openBracketRegEx + "[0-9]+\\:[0-9]+(\\:[0-9]+)? *(am|pm|AM|PM)?" + closeBracketRegEx;

        public static void setCallerSaysPattern(string pattern)
        {
            // escape some typical regular expression characters that might show up
            pattern = pattern.Replace(":", "\\:");
            pattern = pattern.Replace("-", "\\-");
            pattern = pattern.Replace("[", "\\[");
            pattern = pattern.Replace("]", "\\]");
            pattern = pattern.Replace("{", "\\{");
            pattern = pattern.Replace("}", "\\}");
            pattern = pattern.Replace(".", "\\.");
            pattern = pattern.Replace(",", "\\,");
            pattern = pattern.Replace("*", "\\*");
            pattern = pattern.Replace("+", "\\+");

            // if there is a space before the time, remove it for the regexp
            pattern = pattern.Replace(" \\{time\\}", "\\{time\\}");
            pattern = pattern.Replace("\\{time\\} ", "\\{time\\}");
            pattern = pattern.Replace("\\{time\\}",  timeRegExp);

            callerSaysRegExp = pattern;
        }

        public static void setDispatcherSaysPattern(string pattern)
        {
            // escape some typical regular expression characters that might show up
            pattern = pattern.Replace(":", "\\:");
            pattern = pattern.Replace("-", "\\-");
            pattern = pattern.Replace("[", "\\[");
            pattern = pattern.Replace("]", "\\]");
            pattern = pattern.Replace("{", "\\{");
            pattern = pattern.Replace("}", "\\}");
            pattern = pattern.Replace(".", "\\.");
            pattern = pattern.Replace(",", "\\,");
            pattern = pattern.Replace("*", "\\*");
            pattern = pattern.Replace("+", "\\+");

            // if there is a space before or after, remove it for the regexp
            pattern = pattern.Replace(" \\{time\\}", "\\{time\\}");
            pattern = pattern.Replace("\\{time\\} ", "\\{time\\}");
            pattern = pattern.Replace("\\{time\\}",  timeRegExp);

            pattern = pattern.Replace(" \\{position\\-number\\}",  "\\{position\\-number\\}");
            pattern = pattern.Replace("\\{position\\-number\\} ", "\\{position\\-number\\}");
            pattern = pattern.Replace("\\{position\\-number\\}", positionRegExp);

            dispatcherSaysRegExp = pattern;
        }

        void messages_TDDMessageSelected(string Message)
        {
            TextToSend.Text = Message;
        }

        void messages_TDDMessagesClosing()
        {
            messagesButton.Checked = messagesMenu.Checked = false;
        }

        public bool Stop()
		{
			bool OkToStop = true;
			if (messages != null) messages.Close();
			try
			{
				systemShutdown = true;
				Logging.AppTrace("Stopping TDD Applet");
				
				KeepChecking = false;
				workerThread.Dispose();
				Close();
			}
			catch (Exception ex)
			{
				Logging.AppTrace("Error Stopping TDD:" + ex.ToString());
				Logging.ExceptionLogger("Error in TDD.Stop", ex, Console.LogID.Cleanup);
			}
			return OkToStop;
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TDD));
            this.cmBlank = new System.Windows.Forms.ContextMenu();
            this.printDoc = new System.Drawing.Printing.PrintDocument();
            this.printSetup = new System.Windows.Forms.PrintDialog();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.toolTipInvalidChar = new System.Windows.Forms.ToolTip(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.TextToSend = new System.Windows.Forms.RichTextBox();
            this.RealTime = new System.Windows.Forms.RichTextBox();
            this.Conversation = new System.Windows.Forms.RichTextBox();
            this.sendButton = new System.Windows.Forms.Button();
            this.toolBar = new System.Windows.Forms.ToolStrip();
            this.printButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.messagesButton = new System.Windows.Forms.ToolStripButton();
            this.muteSensitivitySeparator = new System.Windows.Forms.ToolStripSeparator();
            this.muteButton = new System.Windows.Forms.ToolStripButton();
            this.unmuteButton = new System.Windows.Forms.ToolStripButton();
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.closeMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.tddMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.messagesMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.muteMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.sendMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ConversationSidePanel = new System.Windows.Forms.Panel();
            this.toolBar.SuspendLayout();
            this.MainMenu.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // printDoc
            // 
            this.printDoc.DocumentName = "TDD Conversation";
            this.printDoc.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDoc_PrintPage);
            // 
            // printSetup
            // 
            this.printSetup.Document = this.printDoc;
            // 
            // toolTip
            // 
            this.toolTip.ShowAlways = true;
            this.toolTip.StripAmpersands = true;
            // 
            // toolTipInvalidChar
            // 
            this.toolTipInvalidChar.ShowAlways = true;
            this.toolTipInvalidChar.StripAmpersands = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Highlight;
            this.tableLayoutPanel1.SetColumnSpan(this.panel2, 2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 286);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(416, 10);
            this.panel2.TabIndex = 32;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(223)))), ((int)(((byte)(247)))));
            this.tableLayoutPanel1.SetColumnSpan(this.panel1, 2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 222);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(416, 10);
            this.panel1.TabIndex = 31;
            // 
            // TextToSend
            // 
            this.TextToSend.BackColor = System.Drawing.SystemColors.Window;
            this.TextToSend.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextToSend.ContextMenu = this.cmBlank;
            this.TextToSend.DetectUrls = false;
            this.TextToSend.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TextToSend.Location = new System.Drawing.Point(4, 300);
            this.TextToSend.Margin = new System.Windows.Forms.Padding(4);
            this.TextToSend.Name = "TextToSend";
            this.TextToSend.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.TextToSend.Size = new System.Drawing.Size(326, 68);
            this.TextToSend.TabIndex = 30;
            this.TextToSend.TabStop = false;
            this.TextToSend.Text = "";
            this.TextToSend.TextChanged += new System.EventHandler(this.TextToSend_TextChanged);
            this.TextToSend.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextToSend_KeyDown);
            this.TextToSend.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextToSend_KeyPress);
            // 
            // RealTime
            // 
            this.RealTime.BackColor = System.Drawing.SystemColors.Window;
            this.RealTime.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tableLayoutPanel1.SetColumnSpan(this.RealTime, 2);
            this.RealTime.ContextMenu = this.cmBlank;
            this.RealTime.Cursor = System.Windows.Forms.Cursors.Default;
            this.RealTime.DetectUrls = false;
            this.RealTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RealTime.Location = new System.Drawing.Point(4, 236);
            this.RealTime.Margin = new System.Windows.Forms.Padding(4);
            this.RealTime.Name = "RealTime";
            this.RealTime.ReadOnly = true;
            this.RealTime.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.RealTime.Size = new System.Drawing.Size(408, 46);
            this.RealTime.TabIndex = 28;
            this.RealTime.TabStop = false;
            this.RealTime.Text = "";
            this.RealTime.TextChanged += new System.EventHandler(this.RealTime_TextChanged);
            this.RealTime.Enter += new System.EventHandler(this.Conversation_GotFocus);
            // 
            // Conversation
            // 
            this.Conversation.BackColor = System.Drawing.SystemColors.Window;
            this.Conversation.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tableLayoutPanel1.SetColumnSpan(this.Conversation, 2);
            this.Conversation.ContextMenu = this.cmBlank;
            this.Conversation.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Conversation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Conversation.Location = new System.Drawing.Point(4, 69);
            this.Conversation.Margin = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.Conversation.Name = "Conversation";
            this.Conversation.ReadOnly = true;
            this.Conversation.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.Conversation.Size = new System.Drawing.Size(412, 153);
            this.Conversation.TabIndex = 27;
            this.Conversation.TabStop = false;
            this.Conversation.Text = "";
            this.Conversation.TextChanged += new System.EventHandler(this.Conversation_TextChanged);
            this.Conversation.GotFocus += new System.EventHandler(this.Conversation_GotFocus);
            // 
            // sendButton
            // 
            this.sendButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sendButton.Enabled = false;
            this.sendButton.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendButton.Location = new System.Drawing.Point(340, 302);
            this.sendButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 4);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(70, 66);
            this.sendButton.TabIndex = 1;
            this.sendButton.Text = "&Send";
            this.sendButton.UseVisualStyleBackColor = false;
            this.sendButton.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // toolBar
            // 
            this.toolBar.AllowMerge = false;
            this.tableLayoutPanel1.SetColumnSpan(this.toolBar, 2);
            this.toolBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolBar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolBar.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printButton,
            this.toolStripSeparator3,
            this.messagesButton,
            this.muteSensitivitySeparator,
            this.muteButton,
            this.unmuteButton});
            this.toolBar.Location = new System.Drawing.Point(0, 24);
            this.toolBar.Name = "toolBar";
            this.toolBar.Size = new System.Drawing.Size(416, 45);
            this.toolBar.Stretch = true;
            this.toolBar.TabIndex = 2;
            this.toolBar.Text = "Tool Bar";
            // 
            // printButton
            // 
            this.printButton.Enabled = false;
            this.printButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.printButton.Image = ((System.Drawing.Image)(resources.GetObject("printButton.Image")));
            this.printButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(36, 42);
            this.printButton.Text = "&Print";
            this.printButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.printButton.ToolTipText = "Print (Ctrl+P)";
            this.printButton.Click += new System.EventHandler(this.printMenu_ItemClick);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 45);
            // 
            // messagesButton
            // 
            this.messagesButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.messagesButton.Image = ((System.Drawing.Image)(resources.GetObject("messagesButton.Image")));
            this.messagesButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.messagesButton.Name = "messagesButton";
            this.messagesButton.Size = new System.Drawing.Size(62, 42);
            this.messagesButton.Text = "&Messages";
            this.messagesButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.messagesButton.ToolTipText = "Preprogrammed Messages";
            this.messagesButton.Click += new System.EventHandler(this.messageButton_ItemClick);
            // 
            // muteSensitivitySeparator
            // 
            this.muteSensitivitySeparator.Name = "muteSensitivitySeparator";
            this.muteSensitivitySeparator.Size = new System.Drawing.Size(6, 45);
            this.muteSensitivitySeparator.Visible = false;
            // 
            // muteButton
            // 
            this.muteButton.CheckOnClick = true;
            this.muteButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.muteButton.Image = ((System.Drawing.Image)(resources.GetObject("muteButton.Image")));
            this.muteButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.muteButton.Name = "muteButton";
            this.muteButton.Size = new System.Drawing.Size(39, 42);
            this.muteButton.Text = "M&ute";
            this.muteButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.muteButton.Visible = false;
            this.muteButton.CheckStateChanged += new System.EventHandler(this.muteButton_CheckStateChanged);
            // 
            // unmuteButton
            // 
            this.unmuteButton.CheckOnClick = true;
            this.unmuteButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.unmuteButton.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.unmuteButton.Image = ((System.Drawing.Image)(resources.GetObject("unmuteButton.Image")));
            this.unmuteButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.unmuteButton.Name = "unmuteButton";
            this.unmuteButton.Size = new System.Drawing.Size(28, 42);
            this.unmuteButton.Text = "&Unmute";
            this.unmuteButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.unmuteButton.Visible = false;
            // 
            // MainMenu
            // 
            this.MainMenu.AllowMerge = false;
            this.tableLayoutPanel1.SetColumnSpan(this.MainMenu, 2);
            this.MainMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.tddMenu,
            this.helpToolStripMenuItem});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(416, 24);
            this.MainMenu.TabIndex = 1;
            this.MainMenu.Text = "Main Menu";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printMenu,
            this.toolStripSeparator1,
            this.closeMenu});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // printMenu
            // 
            this.printMenu.Enabled = false;
            this.printMenu.Image = ((System.Drawing.Image)(resources.GetObject("printMenu.Image")));
            this.printMenu.Name = "printMenu";
            this.printMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.printMenu.Size = new System.Drawing.Size(149, 22);
            this.printMenu.Text = "&Print...";
            this.printMenu.Click += new System.EventHandler(this.printMenu_ItemClick);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(146, 6);
            // 
            // closeMenu
            // 
            this.closeMenu.Name = "closeMenu";
            this.closeMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.closeMenu.Size = new System.Drawing.Size(149, 22);
            this.closeMenu.Text = "&Close";
            this.closeMenu.Click += new System.EventHandler(this.menuClose_ItemClick);
            // 
            // tddMenu
            // 
            this.tddMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.messagesMenu,
            this.toolStripSeparator6,
            this.muteMenu,
            this.toolStripSeparator5,
            this.sendMenu});
            this.tddMenu.Name = "tddMenu";
            this.tddMenu.Size = new System.Drawing.Size(42, 20);
            this.tddMenu.Text = "&TDD";
            // 
            // messagesMenu
            // 
            this.messagesMenu.Image = ((System.Drawing.Image)(resources.GetObject("messagesMenu.Image")));
            this.messagesMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.messagesMenu.Name = "messagesMenu";
            this.messagesMenu.Size = new System.Drawing.Size(134, 22);
            this.messagesMenu.Text = "&Messages...";
            this.messagesMenu.Click += new System.EventHandler(this.messageButton_ItemClick);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(131, 6);
            this.toolStripSeparator6.Visible = false;
            // 
            // muteMenu
            // 
            this.muteMenu.CheckOnClick = true;
            this.muteMenu.Image = ((System.Drawing.Image)(resources.GetObject("muteMenu.Image")));
            this.muteMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.muteMenu.Name = "muteMenu";
            this.muteMenu.Size = new System.Drawing.Size(134, 22);
            this.muteMenu.Text = "M&ute";
            this.muteMenu.Visible = false;
            this.muteMenu.CheckStateChanged += new System.EventHandler(this.muteButton_CheckStateChanged);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(131, 6);
            // 
            // sendMenu
            // 
            this.sendMenu.Enabled = false;
            this.sendMenu.Name = "sendMenu";
            this.sendMenu.Size = new System.Drawing.Size(134, 22);
            this.sendMenu.Text = "S&end";
            this.sendMenu.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutMenu});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutMenu
            // 
            this.aboutMenu.Name = "aboutMenu";
            this.aboutMenu.Size = new System.Drawing.Size(131, 22);
            this.aboutMenu.Text = "&About XXX";
            this.aboutMenu.Click += new System.EventHandler(this.aboutMenu_ItemClick);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.Conversation, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.MainMenu, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.toolBar, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.sendButton, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.RealTime, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.TextToSend, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(416, 372);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // ConversationSidePanel
            // 
            this.ConversationSidePanel.BackColor = System.Drawing.SystemColors.Window;
            this.ConversationSidePanel.Location = new System.Drawing.Point(0, 55);
            this.ConversationSidePanel.Margin = new System.Windows.Forms.Padding(0);
            this.ConversationSidePanel.Name = "ConversationSidePanel";
            this.ConversationSidePanel.Size = new System.Drawing.Size(4, 182);
            this.ConversationSidePanel.TabIndex = 6;
            // 
            // TDD
            // 
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(416, 372);
            this.Controls.Add(this.ConversationSidePanel);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.MainMenu;
            this.Name = "TDD";
            this.Activated += new System.EventHandler(this.TDD_Activated);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.TDD_Closing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.VisibleChanged += new System.EventHandler(this.TDD_VisibleChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TDD_KeyDown);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.toolBar.ResumeLayout(false);
            this.toolBar.PerformLayout();
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		private void OnSendCheck()
		{
			SendButtonCheck(null);
		}

		private void MainForm_Load(object sender, System.EventArgs e)
		{
			this.Text = Application.ProductName + " - TDD";
			aboutMenu.Text = aboutMenu.Text.Replace("XXX",Application.ProductName);
		}

		private void MainForm_Resize(object sender, System.EventArgs e)
		{	
			// hide form on minimze
			if (WindowState == FormWindowState.Minimized) 
			{
				workerThread.Change(System.Threading.Timeout.Infinite,System.Threading.Timeout.Infinite);
			}
			else
            {
				SendCheck();
            }
		}

		private const int WM_QUERYENDSESSION = 0x11;

		protected override void WndProc(ref Message m)
		{
			if (m.Msg == WM_QUERYENDSESSION)
			{
				if (!controller.OkToStop) Controller.ExitApplication(controller);
			}
			base.WndProc (ref m);
		}

		private void TDD_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!systemShutdown)
			{
				if (messages != null) messages.Close();
				e.Cancel = true;
				Hide();
			}
		}

		public void CharacterReceived(string callId, string Character)
		{
            // make sure that this callId has an Info object
            if ( ! infoMap.ContainsKey(callId)) infoMap.Add(callId, new Info(this, callId));

            Info info = infoMap[callId];

			char ch = Character[0];
			Logging.AppTrace("Char = " + ch);

            // if not newline
            if (ch != '\r' && ch != '\n')
            {
                // backspace (null is backspace)
                if (ch == '\b' || ch == '\0')
                {
                    if (info.receivedText.Length > 0)
                    {
                        info.receivedText = info.receivedText.Remove(info.receivedText.Length - 1);
                    }
                }
                // normal character
                else
                {
				    info.receivedText += Character.ToUpper();
                }
			}

            if (callId == currentCallId)
            {
                if (currentInfo == null)
                {
                    currentInfo = info;
                    currentInfo.restore();
                }

                RealTime.Text = info.receivedText;
				RealTime.ScrollToCaret();
            }
		}

		[DllImport("user32", CharSet=CharSet.Auto)] 
		public static extern IntPtr SendMessage(IntPtr hWnd, int msg, int wParam, int lParam); 

		private void PushSentToConversation()
		{
            if (TextToSend.TextLength == 0) return;

            // send what the dispatcher said to the controller
            // (the controller will then push it back to everyone)
            controller.sendTDDStringToController(currentCallId, TextToSend.Text);

            TextToSend.Clear();
            TextToSend.Focus();
		}

		private const int WMV_SCROLL = 277;
		private const int SB_BOTTOM  = 7;

//		private bool ScrollFlag = false;

		private void ScrollText(RichTextBox rtb)
		{
			SendMessage(rtb.Handle, WMV_SCROLL, SB_BOTTOM, 0);
//			ScrollFlag = true;
//			Conversation.Focus();
//			Conversation.ScrollToCaret();
//			ScrollFlag = false;
		}

		private void TextToSend_TextChanged(object sender, System.EventArgs e)
		{
            toolTipInvalidChar.Hide(TextToSend);

            // remember the cursor position
            int caret = TextToSend.SelectionStart;

            string validText   = "";
            string invalidText = "";

            // only allow valid characters
            foreach (char c in TextToSend.Text)
            {
                if (isValidChar(c)) validText   += c;
                else                invalidText += c;
            }

            if (invalidText.Length != 0)
            {
                // tell the user the text was corrected
                string msg = (invalidText.Length == 1) ? "The character '"
                                                       : "These characters '";

                msg += invalidText + "' cannot be sent via TDD";

                // if the text was changed via paste rather than a key press
                if (TextToSend.TextLength - TextToSendLengthAtKeyDown != 1)
                {
                    toolTipInvalidChar.ToolTipTitle = msg;

                    msg = "\nSo this:\n    "               + TextToSend.Text +
                          "\n\nhas been changed to:\n    " + validText + "\n";

                    int toolTipMSec = (msg.Length + toolTipInvalidChar.ToolTipTitle.Length) * 100;

                    toolTipInvalidChar.Show(msg, TextToSend, 10, TextToSend.Height + 10, toolTipMSec);
                }
                else
                {
                    toolTipInvalidChar.ToolTipTitle = "";

                    int toolTipMSec = msg.Length * 100;

                    toolTipInvalidChar.Show(msg, TextToSend, 10, TextToSend.Height - 40, toolTipMSec);
                }

                TextToSend.Text = validText;

                // restore the cursor position
                int cursorPos = caret - invalidText.Length;
                if (cursorPos < 0) cursorPos = 0;

                TextToSend.SelectionStart = cursorPos;
            }

            if (currentCallId != null && currentInfo == null)
            {
                if (infoMap.ContainsKey(currentCallId))
                {
                    currentInfo = infoMap[currentCallId];
                    currentInfo.restore();
                }
                else
                {
                    currentInfo = new Info(this, currentCallId);
                    infoMap.Add(currentCallId, currentInfo);
                }
            }

            if (currentInfo != null) currentInfo.textToSend = TextToSend.Text;

			SendCheck();
		}

        private const string validSymbolStr = "- $'!:(\")=?+./;,";

        private bool isValidChar(char c)
        {
            if (c >= 'a' && c <= 'z') return true;
            if (c >= 'A' && c <= 'Z') return true;
            if (c >= '0' && c <= '9') return true;

            // no need to check for "\r\n\0"
            return (validSymbolStr.Contains(c.ToString()));
        }

		private void SendCheck()
		{
			KeepChecking = (TextToSend.TextLength > 0);

			if (KeepChecking) workerThread.Change(500,500);
			else              workerThread.Change(System.Threading.Timeout.Infinite,System.Threading.Timeout.Infinite);
			
            SendButtonCheck(null);
		}

		private void SendButtonCheck(object blank)
		{
            sendButton.BeginInvoke(new ButtonCheckDelegate(ButtonCheck));
		}

        private void ButtonCheck()
        {
            if (currentInfo == null) return;

            sendButton.Enabled = sendMenu.Enabled = ( ! SendingText && TextToSend.TextLength > 0);
        }

        private bool newConversation = true;

		private void btnSend_Click(object sender, System.EventArgs e)
		{
            if (currentInfo == null) return;

			try
			{
				SendingText = true;

				this.Cursor = Cursors.WaitCursor;

				if (newConversation)
				{
					// force CRLF first - fixes garbage on line issue
					newConversation = false;
				}

				// Force any received text to conversation window
				PushSentToConversation();
				SendingText = false;
				SendButtonCheck(null);
			}
			finally
			{
				this.Cursor = Cursors.Default;
			}
		}

		private void messageButton_ItemClick(object sender, EventArgs e)
		{
    		messagesButton.Checked = messagesMenu.Checked = true;

			if (messages == null || ! messages.Visible)
            {
                if (messages != null) messages.Dispose();

			    messages = new TDDMessages(controller);
                messages.TDDMessagesClosing += new TDDMessages.OnTDDMessagesClosing(messages_TDDMessagesClosing);
                messages.TDDMessageSelected += new TDDMessages.OnTDDMessageSelected(messages_TDDMessageSelected);

                messages.Show(this);
            }
            else
            {
                messages.BringToFront();
            }
		}

		public void MessageClosed()
		{
			messagesButton.Checked = messagesMenu.Checked = false;
		}

		private void Conversation_GotFocus(object sender, System.EventArgs e)
		{
            if (TextToSend.Enabled) TextToSend.Focus();
            else                    toolBar.Focus();
		}

		private void menuClose_ItemClick(object sender, EventArgs e)
		{
			Hide();
		}

        // TODO: remove Mute functionality once we're sure we don't need it (made invisible 25 May 2010 - elliott)
		private void muteButton_CheckStateChanged(object sender, EventArgs e)
		{
            if (sender == muteMenu)
            {
                muteButton.Checked = muteMenu.Checked;
                return;
            }

            if (currentCallId != null && currentInfo == null)
            {
                if ( ! infoMap.ContainsKey(currentCallId))
                {
                    infoMap.Add(currentCallId, new Info(this, currentCallId));
                }

                currentInfo = infoMap[currentCallId];
                currentInfo.restore();
            }

            if (currentInfo != null) currentInfo.muteState = muteButton.Checked;

            muteMenu.Checked = muteButton.Checked;

            if (currentInfo != null && ! currentInfo.inMuteChanged)
            {
                controller.TDDChangeMute(currentCallId, muteButton.Checked);
            }

            ComponentResourceManager resources = new ComponentResourceManager(typeof(TDD));

			if (muteButton.Checked)
            {
                muteButton.Image = ((System.Drawing.Image)(resources.GetObject("unmuteButton.Image")));
                muteMenu.  Image = ((System.Drawing.Image)(resources.GetObject("unmuteButton.Image")));
            }
			else
            {
                muteButton.Image = ((System.Drawing.Image)(resources.GetObject("muteButton.Image")));
                muteMenu.  Image = ((System.Drawing.Image)(resources.GetObject("muteButton.Image")));
            }
		}

		private void TDD_VisibleChanged(object sender, System.EventArgs e)
		{
            if (Visible) TextToSend.Focus();
		}

        private class Info
        {
            public Info(TDD _tdd, string _callId) {tdd = _tdd; callId = _callId;}

            public void remember()
            {
                conversation = tdd.Conversation.Rtf;

                selectionStart  = tdd.TextToSend.SelectionStart;
                selectionLength = tdd.TextToSend.SelectionLength;
            }

            public void restore()
            {
                tdd.Conversation.Rtf  = conversation;
                tdd.RealTime.    Text = receivedText;
                tdd.TextToSend.  Text = textToSend;

                if      (selectionStart    != -1) tdd.TextToSend.SelectionStart = selectionStart;
                else if (textToSend.Length != 0)  tdd.TextToSend.SelectionStart = textToSend.Length;

                tdd.TextToSend.SelectionLength = selectionLength;

                // if the conversation had to be cleared, rebuild it from raw
                if (conversation.Length == 0 && conversationRawText.Length != 0)
                {
                    string raw = conversationRawText;
                    conversationRawText = ""; // clear it since it will be appended to in SomeoneSays()

                    tdd.SomeoneSays(callId, raw);
                    conversation = tdd.Conversation.Rtf;
                }
            }

            private TDD tdd = null;

            public string callId = null;

            public string conversation    = "";
            public string receivedText    = "";
            public string textToSend      = "";
            public int    selectionStart  = -1;
            public int    selectionLength = 0;

            public string conversationRawText = "";
            public bool   muteState           = false;
            public bool   inMuteChanged       = false;
        }

        private System.Collections.Generic.Dictionary<string,Info> infoMap =
            new System.Collections.Generic.Dictionary<string,Info>();

        private Info   currentInfo = null;
        private string currentCallId = null;

        public void setCurrentCallId(string callId)
        {
            if (callId == null || callId.Length == 0)
            {
                // clear the window by restoring an empty call
                try { Info tempInfo = new Info(this, callId); tempInfo.restore(); }
                catch { }

                currentCallId = null;
                return;
            }

            if (callId == currentCallId) return;

            // remember current info
            if (currentInfo != null) currentInfo.remember();

            currentCallId = callId;

            if (Visible)
            {
                if (TextToSend.Enabled) controller.sendToController("TDDModeOn", currentCallId);
                else                    needToSendTDDModeOn = true;
            }

            // try to find Info for the new callId
            if ( ! infoMap.TryGetValue(callId, out currentInfo))
            {
                // if it wasn't found, clear the window by restoring an empty one
                try {Info tempInfo = new Info(this, callId); tempInfo.restore();} catch {}
            }
            else
            {
                currentInfo.restore();
            }
        }

		private void aboutMenu_ItemClick(object sender, EventArgs e)
		{
			Splash.ShowAbout(controller.Position);
			TextToSend.Focus();
		}

		private void PrintHeader(Graphics graphic)
		{
			LinePosition = 10;
			printerFont = new Font("Lucida Console", fontSize );
			printerBrush = new SolidBrush(Color.Black);
			LinePosition = TopMargin;

			printerFont = new Font(printerFont,FontStyle.Bold);
			WriteToPrinter(graphic, "TDD Conversation");
			printerFont = new Font(printerFont,FontStyle.Regular);

			graphic.DrawLine(Pens.Black,new PointF(LeftMargin,LinePosition),new PointF(printDoc.DefaultPageSettings.PaperSize.Width - LeftMargin,LinePosition));
			LinePosition += 20;
		}

		private void printDoc_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
		{
			printerFont = new Font("Lucida Console", fontSize );
			// force page header print
			LinePosition = printSetup.PrinterSettings.DefaultPageSettings.PaperSize.Height + 10;
			printerFont = new Font(printerFont,FontStyle.Regular);
			foreach (string LineOfText in Conversation.Lines) 
				WriteToPrinter(e.Graphics, LineOfText);
		}

		public void WriteToPrinter(Graphics graphic, string Text)
		{
			if (LinePosition > printSetup.PrinterSettings.DefaultPageSettings.PaperSize.Height)
				PrintHeader(graphic);

			graphic.DrawString(Text,printerFont, printerBrush, (float) LeftMargin, LinePosition);
			LinePosition += printerFont.Height + 5;
		}

		private void printMenu_ItemClick(object sender, EventArgs e)
		{
			if (printSetup.ShowDialog() == DialogResult.OK)
				printDoc.Print();

			TextToSend.Focus();
		}

        private int TextToSendLengthAtKeyDown = 0;

		private void TextToSend_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
            TextToSendLengthAtKeyDown = TextToSend.TextLength;

			Logging.AppTrace("TDD KeyValue: " + e.KeyValue.ToString());

			switch (e.KeyCode)
			{
				case Keys.Enter:

					if ( ! (e.Alt || e.Shift || e.Control))
					{
						if (TextToSend.Text.Length > 0 && sendButton.Enabled) sendButton.PerformClick();

                        e.Handled = true;
					}

					break;

				case Keys.Escape:
					e.Handled = true;
					this.Close();
					break;
			}
		}

		private const char TabKey = (char)9;

		private void TextToSend_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			switch (e.KeyChar)
			{
				case TabKey:
					e.Handled = true;
					sendButton.Focus();
					break;
			}
		}

		private void TDD_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Tab && sendButton.Enabled)
			{
				e.Handled = true;
				sendButton.Focus();
			}
            else if (e.KeyCode == Keys.Escape)
            {
                e.Handled = true;
                this.Close();
            }
		}

		private void Conversation_TextChanged(object sender, System.EventArgs e)
		{
			printButton.Enabled = printMenu.Enabled = (Conversation.Text.Length > 0);
		}

		private void TDD_Activated(object sender, System.EventArgs e)
		{
			messagesButton.Checked = messagesMenu.Checked = (messages != null && messages.Visible);

            if (TextToSend.Enabled)
            {
                TextToSend.Focus();
                controller.sendToController("TDDModeOn", currentCallId);
            }
            else
            {
                needToSendTDDModeOn = true;
            }
		}

		private void RealTime_TextChanged(object sender, System.EventArgs e)
		{
			RichTextBox rtb = (RichTextBox) sender;
			ScrollText(rtb);
		}

        public void setEnabled(bool enabled)
        {
            // enable the Conversation window if there is text (this must be first)
            Conversation.Enabled = enabled || (Conversation.TextLength != 0);
            ConversationSidePanel.Visible = Conversation.Enabled;

            if (TextToSend.Enabled == enabled) return;

            if ( ! enabled)
            {
                toolTip.SetToolTip(TextToSend, "Call is not active");
                toolTip.SetToolTip(RealTime,   "Call is not active");
                toolTip.SetToolTip(sendButton, "Call is not active");

                if (messages != null) messages.Close();

                this.BackColor = System.Drawing.SystemColors.Control;
            }
            else
            {
                toolTip.RemoveAll();

                this.BackColor = System.Drawing.SystemColors.Window;

                if (needToSendTDDModeOn && currentCallId != null)
                {
                    needToSendTDDModeOn = false;
                    controller.sendToController("TDDModeOn", currentCallId);
                }
            }

            TextToSend.        Enabled = enabled;
            RealTime.          Enabled = enabled;

            messagesMenu.      Enabled = enabled;
            muteMenu.          Enabled = enabled;

            messagesButton.    Enabled = enabled;
            muteButton.        Enabled = enabled;
        }

        private const char ETX = (char)3;  // end of text

        public void CallerSays(string callId, string str)
        {
            // make sure that this callId has an Info object
            if ( ! infoMap.ContainsKey(callId)) infoMap.Add(callId, new Info(this, callId));

            infoMap[callId].receivedText = "";

            if (callId == currentCallId) RealTime.Clear();

            SomeoneSays(callId, str);
        }

        public void DispatcherSays(string callId, string str)
        {
            // make sure that this callId has an Info object
            if ( ! infoMap.ContainsKey(callId)) infoMap.Add(callId, new Info(this, callId));

            SomeoneSays(callId, str);
        }

        private void SomeoneSays(string callId, string str)
        {
            try
            {
                int curIndex = 0;

                infoMap[callId].conversationRawText += str;

                if (callId != currentCallId)
                {
                    infoMap[callId].conversation = ""; // this will be rebuilt on restore()
                }
                else
                {
                    while (curIndex < str.Length)
                    {
                        if (Conversation.TextLength == 0) Conversation.SelectedText = Environment.NewLine;

                        int etxIndex = str.IndexOf(ETX, curIndex);
                        if (etxIndex == -1) etxIndex = str.Length - 1;

                        string subStr = str.Substring(curIndex, etxIndex - curIndex);

                        System.Text.RegularExpressions.Match callerMatch =
                            System.Text.RegularExpressions.Regex.Match(subStr, callerSaysRegExp);

                        bool isCaller = callerMatch.Success;

                        int speakerIndex      = -1;
                        int afterSpeakerIndex = -1;

                        if (isCaller)
                        {
                            string callerSaysPrefix = callerMatch.ToString();

                            speakerIndex      = str.IndexOf(callerSaysPrefix, curIndex, etxIndex - curIndex);
                            afterSpeakerIndex = speakerIndex + callerSaysPrefix.Length;
                        }
                        else
                        {
                            System.Text.RegularExpressions.Match dispatcherMatch =
                                System.Text.RegularExpressions.Regex.Match(subStr, dispatcherSaysRegExp);

                            if (dispatcherMatch.Success)
                            {
                                string dispatcherSaysPrefix = dispatcherMatch.ToString();

                                speakerIndex      = str.IndexOf(dispatcherSaysPrefix, curIndex, etxIndex - curIndex);
                                afterSpeakerIndex = speakerIndex + dispatcherSaysPrefix.Length;
                            }
                            else
                            {
                                speakerIndex = afterSpeakerIndex = curIndex;
                            }
                        }

                        string preSpeakerStr = str.Substring(curIndex,          speakerIndex      - curIndex);
                        string speakerStr    = str.Substring(speakerIndex,      afterSpeakerIndex - speakerIndex);
                        string textStr       = str.Substring(afterSpeakerIndex, etxIndex          - afterSpeakerIndex).Trim();

                        curIndex = etxIndex + 1;

                        Conversation.SelectionStart = Conversation.TextLength;

                        Conversation.SelectionIndent = 0;
                        Conversation.SelectionColor  = Color.DarkGray;
                        Conversation.SelectedText    = preSpeakerStr;

                        Conversation.SelectionColor  = Color.Black;
                        Conversation.SelectedText    = speakerStr + Environment.NewLine;

                        Conversation.SelectionIndent = 20;

                        if (isCaller)
                        {
                            Conversation.SelectionColor = ReceivedTextColor;
                            Conversation.SelectedText   = textStr.ToUpper() + Environment.NewLine + Environment.NewLine;
                        }
                        else
                        {
                            Conversation.SelectionColor = SendTextColor;
                            Conversation.SelectedText   = textStr.ToLower() + Environment.NewLine + Environment.NewLine;
                        }
                    }

                    Conversation.ScrollToCaret();
                    ScrollText(Conversation);
                }
            }
            catch
            {
            }
        }

        // TODO: remove Mute functionality once we're sure we don't need it (made invisible 25 May 2010 - elliott)
        public void MuteChanged(string callId, bool mute)
        {
            // make sure that this callId has an Info object
            if ( ! infoMap.ContainsKey(callId))
            {
                // if mute is false (the default), we don't need to keep track of it
                if ( ! mute) return;

                infoMap.Add(callId, new Info(this, callId));
            }

            Info info = infoMap[callId];

            info.inMuteChanged = true;

            // if this callId is the current callId, change the button
            // NOTE: do as little as possible inside the if due to multi-threading
            if (callId == currentCallId) muteButton.Checked = mute;
            else                         info.muteState     = mute;

            info.inMuteChanged = false;
        }

        public bool getMuteState(string callId)
        {
            Info info;
            if (infoMap.TryGetValue(callId, out info)) return info.muteState;

            return false;
        }

        // TODO now: Remove info objects from the infomap if the infomap gets too big...
        //           the infoMap shouldn't get very big generally since it only contains
        //           objects for calls that have interacted with the TDD dialog
    }
}
