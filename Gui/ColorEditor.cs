﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace WestTel.E911
{
    public partial class ColorEditor : BaseForm
    {
        public class ColorTreeNode : TreeNode
        {
            public ColorTreeNode(string name, Settings.ColorInfo _colorInfo):
              base(name)
            {
                colorInfo = _colorInfo;
            }

            public Settings.ColorInfo colorInfo = null;
        }

        public ColorEditor(Controller _controller)
        {
            controller = _controller;

            InitializeComponent();
            setTranslatableText();

            this.     BackColor = SystemColors.Window;
            MainMenu. BackColor = SystemColors.Window;
            //toolBar.  BackColor = SystemColors.Window; // TODO: add a toolbar
        }

        private void setTranslatableText()
        {
            this.aboutMenu.Text = (Translate)("&About XXX");
        }

        private bool systemShutdown = false;

        public void Stop()
		{
			try
			{
				Logging.AppTrace("Stopping Color Editor");
				systemShutdown = true;
				Close();
			}
			catch (Exception ex)
			{
				Logging.AppTrace("Error Stopping Color Editor:" + ex.ToString());
				Logging.ExceptionLogger("Error Stopping Color Editor", ex, Console.LogID.Cleanup);
			}
		}

		private void ColorEditor_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if ( ! systemShutdown)
			{
				e.Cancel = true;

                bool success = checkModified();

				if (success) Hide();
			}
		}

        private void setModified(bool _isModified)
        {
            if (isModified != _isModified)
            {
                isModified = _isModified;

                Text = controller.settings.getColorsXmlFilepath() +
                    (isModified ? "*" : "") + " - " + Application.ProductName;
            }
        }

        private bool checkModified()
        {
            try
            {
                if (isModified)
                {
                    DialogResult result = MessageBox.Show(this,
                        (Translate)"Settings have been modified, would you like to Save first?",
                        Application.ProductName, MessageBoxButtons.YesNoCancel,
                        MessageBoxIcon.Question, MessageBoxDefaultButton.Button3);

                    switch (result)
                    {
                        case DialogResult.Yes:    return save();
                        case DialogResult.No:     return true;
                        case DialogResult.Cancel: return false;
                    }
                }
            }
            catch {return false;}

            return true;
        }

		private void ColorEditor_Load(object sender, System.EventArgs e)
		{
            try
            {
			    aboutMenu.Text = aboutMenu.Text.Replace("XXX", Application.ProductName);

                // remove unnecessary separators
                //removeUnnecessarySeparators(toolBar);
            }
            catch {};
		}

		private void aboutMenu_Click(object sender, EventArgs e)
		{
			Splash.ShowAbout(controller.Position);
		}

		private void usersManualMenu_Click(object sender, EventArgs e)
		{
			Console.ShowUsersManual();
		}

        private bool inColorsUpdated = false;

        public void colorsUpdated()
        {
            if (inColorsUpdated) return;

            try   { BeginInvoke(new ColorsUpdatedDelegate(ColorsUpdated)); }
            catch { try {ColorsUpdated();} catch{} }
        }

        private delegate void ColorsUpdatedDelegate();

        private void ColorsUpdated()
        {
            try
            {
                if (updateTimer != null) updateTimer.Change(-1, -1); // stop
                updateTimerRunning = false;

                // TODO* color: save expanded state and selected node every time those change

                // TODO* color: restore expanded state and selected node
treeView.SelectedNode = treeView.Nodes[0];

                updateColorInfo();
            }
            catch {}
        }

        private void importMenu_Click(object sender, EventArgs e)
        {
            bool success = checkModified();
            if ( ! success) return;

            success = saveTemp(); // save current settings to temp directory 
            if ( ! success) return;

            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.AddExtension       = true;
            openFileDialog.AutoUpgradeEnabled = true;
            openFileDialog.DefaultExt         = ".xml";
            openFileDialog.FileName           = controller.settings.getColorsXmlFilepath();
            openFileDialog.Title              = "Open Colors XML File";

            DialogResult result = openFileDialog.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                setModified(false);
                controller.settings.loadColorsXmlFilepath(openFileDialog.FileName);
            }
        }

        private void saveMenu_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(this,
                (Translate)("Are you sure you want save your changes to\n\n\"") +
                controller.settings.getColorsXmlFilepath() + "\"?",
                Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes) save();
        }

        private bool save()
        {
            if ( ! isModified) return true;

            string filepath = "";
            string temppath = "";

            try
            {
                // save previous settings to temp directory 
                filepath = controller.settings.getColorsXmlFilepath();
                temppath = getTempFilepath();
                System.IO.File.Copy(filepath, temppath);
            }
            catch (Exception ex)
            {
                DialogResult result = MessageBox.Show(this,
                    (Translate)("Could not copy the current file to the backup location:") +
                    "\n\n    \"" + filepath + "\"\n\n    \"" + temppath + "\"\n\n    " +
                    ex.Message + "\n\nClick OK to ignore.",
                    Application.ProductName, MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Information, MessageBoxDefaultButton.Button2);

                string userResponseStr = "";

                if (result == DialogResult.OK) userResponseStr = " - User chose ignore";

                Controller.sendAlertToController("Error writing \"" + filepath + "\"" +
                    " (" + ex.Message + ")" + userResponseStr, Console.LogType.Info, Console.LogID.Gui);

                return (result != DialogResult.OK);
            }

            return saveAs(controller.settings.getColorsXmlFilepath());
        }

        private void exportMenu_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.AddExtension       = true;
            saveFileDialog.AutoUpgradeEnabled = true;
            saveFileDialog.DefaultExt         = ".xml";
            saveFileDialog.FileName           = controller.settings.getColorsXmlFilepath();
            saveFileDialog.Title              = "Export Colors";

            DialogResult result = saveFileDialog.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                bool success = saveAs(saveFileDialog.FileName);

                if ( ! success) exportMenu_Click(sender, e);
            }
        }

        private bool saveAs(string filepath)
        {
            // Note: don't clear the modified flag on save as

            return controller.settings.writeColorsXmlFile(filepath, this);
        }

        private bool saveTemp()
        {
            try   {return controller.settings.writeColorsXmlFile(getTempFilepath(), this);}
            catch {}

            return false;
        }

        private string getTempFilepath()
        {
            string tempDir = System.IO.Path.GetTempPath() + "WestTel\\ColorXML\\";
            System.IO.Directory.CreateDirectory(tempDir);

            string colorsXmlFilepath = controller.settings.getColorsXmlFilepath();
            string colorsXmlFilename = new System.IO.FileInfo(colorsXmlFilepath).Name;
            string colorsXmlFileExt  = new System.IO.FileInfo(colorsXmlFilepath).Extension;

            string colorsXmlFileBase =
                colorsXmlFilename.Remove(colorsXmlFilename.Length - colorsXmlFileExt.Length);

            string dateStr = System.DateTime.Now.ToString("yyyy-MM-dd-hh_mm_ss");

            return tempDir + colorsXmlFileBase + "-" + dateStr + colorsXmlFileExt;
        }

        private void resetToDefaultMenu_Click(object sender, EventArgs e)
        {
            bool success = checkModified();
            if ( ! success) return;

            success = saveTemp(); // save current settings to temp directory 
            if ( ! success) return;

            setModified(false);

            controller.settings.resetColorsXml();
        }

        private void viewAutoBackupsMenu_Click(object sender, EventArgs e)
        {
            try
            {
                string tempDir = new System.IO.FileInfo(getTempFilepath()).DirectoryName;

                System.Diagnostics.Process.Start(tempDir);
            }
            catch {}
        }

        public void clear()
        {
            setModified(false);

            treeView.Nodes.Clear();
        }

        public TreeNode addNode(string name) {return addNode(null, name, null);}

        public TreeNode addNode(string name, Settings.ColorInfo colorInfo)
        {
            return addNode(null, name, colorInfo);
        }

        public TreeNode addNode(TreeNode parentNode, string name)
        {
            return addNode(parentNode, name, null);
        }

        public TreeNode addNode(TreeNode parentNode, string name, Settings.ColorInfo colorInfo)
        {
            ColorTreeNode node = new ColorTreeNode(name, colorInfo);

            if (parentNode == null) treeView.   Nodes.Add(node);
            else                    parentNode. Nodes.Add(node);

            return node;
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            selectedNode = (ColorTreeNode)e.Node;
            updateColorInfo();
        }

        private void updateColorInfo()
        {
            // TODO color: support multi-selection?

            bool hasColorInfo = selectedNode != null && selectedNode.colorInfo != null;

            testLabel.        Visible = textButton.     Visible = hasColorInfo;
            backgroundButton. Visible = gradientButton. Visible = hasColorInfo;
            shadowButton.     Visible = boldCheckbox.   Visible = hasColorInfo;
            backgroundSlider. Visible =                           hasColorInfo;
            gradientSpinBox.  Visible = gradientLabel.  Visible = hasColorInfo;
            textSlider.       Visible = shadowSlider.   Visible = hasColorInfo;
            backgroundEqualLabel.                       Visible = hasColorInfo;

            bool isBackground = selectedNode.Text.ToLower().Equals("background");

            if (isBackground)
            {
                textButton.Visible = textSlider.Visible =
                    backgroundSlider.Visible = backgroundEqualLabel.Visible =
                    gradientButton.Visible   = gradientSpinBox.Visible = gradientLabel.Visible =
                    shadowButton.Visible     = shadowSlider.Visible =
                    boldCheckbox.Visible     = false;
            }

            // TODO color: support undo/redo

            if (hasColorInfo)
            {
                Settings.ColorInfo colorInfo = selectedNode.colorInfo;

                backgroundSlider. Value   = colorInfo.backgroundColor1.A;
                gradientSpinBox.  Value   = colorInfo.backgroundColor2.A;
                textSlider.       Value   = colorInfo.foregroundColor.A;
                shadowSlider.     Value   = colorInfo.shadowColor.A;
                boldCheckbox.     Checked = colorInfo.bold;

                if (backgroundEqualLabel.Visible)
                {
                    backgroundEqualLabel.Visible =
                        colorInfo.backgroundColor1.Equals(colorInfo.backgroundColor2);
                }

                testLabel.Refresh();
            }
        }

        private void modified(object notused)
        {
            // invoke on the Ui thread to avoid threading problems
            try   {BeginInvoke(new modified_delegate(modified));}
            catch {modified();}
        }

        private delegate void modified_delegate();

        private void modified()
        {
            setModified(true);

            if (updateTimer != null) updateTimer.Change(-1, -1); // stop
            updateTimerRunning = false;

            updateColorInfo();

            inColorsUpdated = true;
            controller.colorsUpdated();
            inColorsUpdated = false;
        }

        private void modified(int msec)
        {
            if (updateTimer == null) updateTimer = new System.Threading.Timer(modified);

            if ( ! updateTimerRunning)
            {
                updateTimerRunning = true;
                updateTimer.Change(msec, -1);
            }
        }

        private void backgroundButton_Click(object sender, EventArgs e)
        {
            Settings.ColorInfo colorInfo = selectedNode.colorInfo;

            ColorDialog colorDialog = new ColorDialog();
            colorDialog.Color       = colorInfo.backgroundColor1;
            colorDialog.FullOpen    = true;

            DialogResult result = colorDialog.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                if (colorInfo.backgroundColor1.Equals(colorInfo.backgroundColor2))
                {
                    colorInfo.backgroundColor2 =
                        Color.FromArgb(backgroundSlider.Value, colorDialog.Color);
                }

                colorInfo.backgroundColor1 =
                    Color.FromArgb(backgroundSlider.Value, colorDialog.Color);

                modified();
            }
        }

        private void gradientButton_Click(object sender, EventArgs e)
        {
            Settings.ColorInfo colorInfo = selectedNode.colorInfo;

            ColorDialog colorDialog = new ColorDialog();
            colorDialog.Color       = colorInfo.backgroundColor1;
            colorDialog.FullOpen    = true;

            DialogResult result = colorDialog.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                colorInfo.backgroundColor2 =
                    Color.FromArgb((int)gradientSpinBox.Value, colorDialog.Color);

                modified();
            }
        }

        private void textButton_Click(object sender, EventArgs e)
        {
            Settings.ColorInfo colorInfo = selectedNode.colorInfo;

            ColorDialog colorDialog = new ColorDialog();
            colorDialog.Color       = colorInfo.foregroundColor;
            colorDialog.FullOpen    = true;

            DialogResult result = colorDialog.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                colorInfo.foregroundColor =
                    Color.FromArgb(textSlider.Value, colorDialog.Color);

                modified();
            }
        }

        private void shadowButton_Click(object sender, EventArgs e)
        {
            Settings.ColorInfo colorInfo = selectedNode.colorInfo;

            ColorDialog colorDialog = new ColorDialog();
            colorDialog.Color       = colorInfo.shadowColor;
            colorDialog.FullOpen    = true;

            DialogResult result = colorDialog.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                colorInfo.shadowColor =
                    Color.FromArgb(shadowSlider.Value, colorDialog.Color);

                modified();
            }
        }

        private void boldCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (boldCheckbox.Checked != selectedNode.colorInfo.bold)
            {
                selectedNode.colorInfo.bold = boldCheckbox.Checked;
                modified();
            }
        }

        private void backgroundSlider_ValueChanged(object sender, EventArgs e)
        {
            Settings.ColorInfo colorInfo = selectedNode.colorInfo;

            if (backgroundSlider.Value != colorInfo.backgroundColor1.A)
            {
                if (colorInfo.backgroundColor1.Equals(colorInfo.backgroundColor2))
                {
                    colorInfo.backgroundColor1 =
                        Color.FromArgb(backgroundSlider.Value, colorInfo.backgroundColor1);

                    gradientSpinBox.Value = (int)backgroundSlider.Value;
                }
                else
                {
                    colorInfo.backgroundColor1 =
                        Color.FromArgb(backgroundSlider.Value, colorInfo.backgroundColor1);
                }

                modified(10);
            }
        }

        private void gradientSpinBox_ValueChanged(object sender, EventArgs e)
        {
            if (gradientSpinBox.Value != (int)selectedNode.colorInfo.backgroundColor2.A)
            {
                selectedNode.colorInfo.backgroundColor2 =
                    Color.FromArgb((int)gradientSpinBox.Value,
                                   selectedNode.colorInfo.backgroundColor2);
                modified(10);
            }
        }

        private void textSlider_ValueChanged(object sender, EventArgs e)
        {
            if (textSlider.Value != selectedNode.colorInfo.foregroundColor.A)
            {
                selectedNode.colorInfo.foregroundColor =
                    Color.FromArgb(textSlider.Value,
                                   selectedNode.colorInfo.foregroundColor);
                modified(10);
            }
        }

        private void shadowSlider_ValueChanged(object sender, EventArgs e)
        {
            if (shadowSlider.Value != selectedNode.colorInfo.foregroundColor.A)
            {
                selectedNode.colorInfo.shadowColor =
                    Color.FromArgb(shadowSlider.Value,
                                   selectedNode.colorInfo.shadowColor);
                modified(10);
            }
        }

        private void TestLabel_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            Rectangle bounds = testLabel.DisplayRectangle;
            bounds.Height -= 12;
            bounds.Y      += 6;

            // background
            e.Graphics.FillRectangle(Brushes.Black, bounds);

            Settings.ColorInfo colorInfo = selectedNode.colorInfo;

            Brush brush = new LinearGradientBrush(bounds,
                colorInfo.backgroundColor1, colorInfo.backgroundColor2,
                LinearGradientMode.Horizontal);

            e.Graphics.FillRectangle(brush, bounds);

            // text
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;

            Font font = new Font(testLabel.Font,
                colorInfo.bold ? FontStyle.Bold : FontStyle.Regular);

            e.Graphics.DrawString(
                testLabel.Text, font, colorInfo.getForegroundBrush(), bounds, format);

            // outline
            Rectangle outlineRect = bounds;
            outlineRect.Width    -= 1;
            outlineRect.Height   -= 1;
            e.Graphics.DrawRectangle(new Pen(colorInfo.foregroundColor), outlineRect);

            // shadow
            Color shadowColor = colorInfo.shadowColor;

            // top shadow
            Rectangle shadowRect = testLabel.DisplayRectangle;
            shadowRect.Height    = 6;

            e.Graphics.FillRectangle(new LinearGradientBrush(
                shadowRect, Color.FromArgb(0, shadowColor),
                Color.FromArgb(shadowColor.A / 3, shadowColor),
                LinearGradientMode.Vertical), shadowRect);

            // bottom shadow
            shadowRect.Y = testLabel.DisplayRectangle.Bottom - 6;

            e.Graphics.FillRectangle(new LinearGradientBrush(
                shadowRect, shadowColor, Color.FromArgb(0, shadowColor),
                LinearGradientMode.Vertical), shadowRect);
        }

        private void treeView_NodeMouseClick(object sender, System.Windows.Forms.TreeNodeMouseClickEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                // TODO color: create menu for expanding and collapsing all sub items
                if ( ! e.Node.IsExpanded) e.Node.ExpandAll();
            }
        }

        private void treeView_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyCode)
                {
                    case Keys.Right:
                    {
                        // this allows for easily expanding all nodes
                        if (treeView.SelectedNode                 != null &&
                            treeView.SelectedNode.Nodes.Count     == 0    &&
                            treeView.SelectedNode.NextVisibleNode != null)
                        {
                            treeView.SelectedNode = treeView.SelectedNode.NextVisibleNode;
                            e.Handled = true;
                        }

                        break;
                    }
                    case Keys.Left:
                    {
                        // this allows travel to siblings up the tree 
                        if (treeView.SelectedNode            != null &&
                            treeView.SelectedNode.IsExpanded == false &&
                            treeView.SelectedNode.PrevNode   != null)
                        {
                            treeView.SelectedNode = treeView.SelectedNode.PrevNode;
                            e.Handled = true;
                        }

                        break;
                    }
                }
            }
            catch {}
        }

        private void expandAllMenu_Click(object sender, EventArgs e)
        {
            treeView.ExpandAll();
        }

        private void collapseAllMenu_Click(object sender, EventArgs e)
        {
            treeView.CollapseAll();
        }


        private Controller    controller   = null;
        private ColorTreeNode selectedNode = null;

        // initialize to true so that the Title gets set properly on the first clear()
        private bool isModified = true;

        private System.Threading.Timer updateTimer;
        private bool                   updateTimerRunning;
    }
}
