using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Collections;
using System.Windows.Forms;

namespace WestTel.E911
{
	/// <summary>
	/// Summary description for AlarmDisplay.
	/// </summary>
	public class AlarmDisplay: BaseForm
	{
        private bool systemShutdown = false;
        private Grid gridAlarms = new Grid("Alarms");
        private BindingSource gridAlarmsBindingSource;

		private System.Windows.Forms.PrintDialog printSetup;
		private System.Drawing.Printing.PrintDocument printDoc;

		Brush printerBrush;
		Font printerFont;
		int fontSize = 10;
		float LinePosition = 10;
		float LeftMargin = 30;
		float TopMargin = 50;

        private DataGridViewTextBoxColumn TimeCol;
        private DataGridViewTextBoxColumn DescCol;
        private TableLayoutPanel tableLayoutPanel1;
        private MenuStrip MainMenu;
        private ToolStrip toolBar;
        private ToolStripButton printButton;
        private ToolStripButton clearButton;
        private ToolStripMenuItem fileMenu;
        private ToolStripMenuItem printMenu;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem closeMenu;
        private ToolStripMenuItem helpMenu;
        private ToolStripMenuItem aboutMenu;
        private ToolStripMenuItem usersManualMenu;
        private ToolStripSeparator helpMenuSep;
        private ToolStripMenuItem alertsMenu;
        private ToolStripMenuItem clearMenu;
        private StatusStrip statusStrip;

		private Controller controller;

        public static Settings.ColorInfo[] rowColorInfo =
        {
            // State.Normal
            new Settings.ColorInfo(Color.FromArgb(180,170,100), // darker yellow
                                   Color.FromArgb(180,170,100), // darker yellow
                                   Color.Black, false),
            // State.Selected
            new Settings.ColorInfo(Color.FromArgb(255,255,180), // yellow
                                   Color.FromArgb(255,255,180), // yellow
                                   Color.Black, false),
            // State.Hovered
            new Settings.ColorInfo(Color.FromArgb(255,255,180), // yellow
                                   Color.FromArgb(255,255,180), // yellow
                                   Color.Black, false),
        };

        public static Settings.ColorInfo[] altRowColorInfo =
        {
            // State.Normal
            new Settings.ColorInfo(Color.FromArgb(180,170,100), // darker yellow
                                   Color.FromArgb(180,170,100), // darker yellow
                                   Color.Black, false),
            // State.Selected
            new Settings.ColorInfo(Color.FromArgb(255,255,180), // yellow
                                   Color.FromArgb(255,255,180), // yellow
                                   Color.Black, false),
            // State.Hovered
            new Settings.ColorInfo(Color.FromArgb(255,255,180), // yellow
                                   Color.FromArgb(255,255,180), // yellow
                                   Color.Black, false)
        };

        public static Settings.ColorInfo backgroundColorInfo =
            new Settings.ColorInfo(Color.FromArgb(unchecked((int)0xFFE0E0E0)), // Grey 300
                                   Color.FromArgb(unchecked((int)0xFFE0E0E0)), // Grey 300 (transparent)
                                   Color.Black, false);
		public AlarmDisplay(Controller c)
		{
			controller = c;
			InitializeComponent();
			CustomInitializeComponent();
            setTranslatableText();

            colorsUpdated();

            statusStrip.Visible = false;

            // hardcode minimum and maximum size
            ClientSize  = new Size(640, 105 + (8 * 37)); // 8 rows
            MinimumSize = SizeFromClientSize(ClientSize);
            MaximumSize = SystemInformation.VirtualScreen.Size;

            // make the default size different than the minimum
            ClientSize = new Size(800, ClientSize.Height);

DataModel.logLockInfo(true, DataModel.alarmEventsSyncRoot);
            lock (DataModel.alarmEventsSyncRoot)
            {
                try
                {
                    gridAlarms.setSyncRoot(DataModel.alarmEventsSyncRoot);

                    DataTable alarmDataTable = controller.dataModel.ds.Tables["AlarmEvents"];

			        if (alarmDataTable != null)
			        {
                        gridAlarmsBindingSource = new BindingSource(controller.dataModel.ds, "AlarmEvents");
                        gridAlarms.DataSource   = gridAlarmsBindingSource;

                        gridAlarmsBindingSource.Sort = "timeStamp DESC";

                        // KLUDGE - 20 Jul 2009 elliott - if this kludge is not here, the Alarm Dialog locks up when the scroll bar appears
                        kludgeRowList = new ArrayList();

                        for (int i = 0; i < 10; ++i)
                        {
                            c.dataModel.AddAlarm("00:00:00", "*** GUI Test Alarm ***");

                            kludgeRowList.Add(alarmDataTable.Rows[alarmDataTable.Rows.Count - 1]);
                        }
			        }

                    // this refresh is needed in case the data table is populated before the Dialog is created
                    refreshGrid();
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.alarmEventsSyncRoot);
		}

        private ArrayList kludgeRowList = null;

		public void Stop()
		{
			try
			{
				Logging.AppTrace("Stopping Alarm Display");
				systemShutdown = true;
				Close();
			}
			catch (Exception ex)
			{
				Logging.AppTrace("Error Stopping Alarm Display:" + ex.ToString());
				Logging.ExceptionLogger("Error Stopping Alarm Display", ex, Console.LogID.Cleanup);
			}
		}

        System.Threading.Timer colorsUpdatedTimer = null;

        public void colorsUpdated()
        {
            if ( ! Visible)
            {
                if (colorsUpdatedTimer == null) colorsUpdatedTimer = new System.Threading.Timer(colorsUpdated);
                colorsUpdatedTimer.Change(500, -1);
            }
            else
            {
                colorsUpdated(null);
            }
        }

        private delegate void ColorsUpdatedDelegate();

        private void colorsUpdated(object notused)
        {
            try   { BeginInvoke(new ColorsUpdatedDelegate(ColorsUpdated)); }
            catch { try {ColorsUpdated();} catch{} }
        }

        private void ColorsUpdated()
        {
            try
            {
                if (gridAlarms != null)
                {
                    gridAlarms.BackgroundColor = backgroundColorInfo.backgroundColor1;
                }

                gridAlarms.Refresh();
            }
            catch {}
        }

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DescCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.printSetup = new System.Windows.Forms.PrintDialog();
            this.printDoc = new System.Drawing.Printing.PrintDocument();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.toolBar = new System.Windows.Forms.ToolStrip();
            this.printButton = new System.Windows.Forms.ToolStripButton();
            this.clearButton = new System.Windows.Forms.ToolStripButton();
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.printMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.closeMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.alertsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.clearMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.usersManualMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuSep = new System.Windows.Forms.ToolStripSeparator();
            this.aboutMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.tableLayoutPanel1.SuspendLayout();
            this.toolBar.SuspendLayout();
            this.MainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // TimeCol
            // 
            this.TimeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.TimeCol.DataPropertyName = "timeStamp";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.Format = "H:mm:ss\nM/d/yyyy";
            dataGridViewCellStyle1.NullValue = null;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TimeCol.DefaultCellStyle = dataGridViewCellStyle1;
            this.TimeCol.HeaderText = "Time/Date";
            this.TimeCol.Name = "TimeCol";
            this.TimeCol.ReadOnly = true;
            this.TimeCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.TimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.TimeCol.Width = 100;
            // 
            // DescCol
            // 
            this.DescCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DescCol.DataPropertyName = "alarmText";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DescCol.DefaultCellStyle = dataGridViewCellStyle2;
            this.DescCol.HeaderText = "Description";
            this.DescCol.Name = "DescCol";
            this.DescCol.ReadOnly = true;
            this.DescCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DescCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // printSetup
            // 
            this.printSetup.Document = this.printDoc;
            // 
            // printDoc
            // 
            this.printDoc.DocumentName = "Alarms";
            this.printDoc.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDoc_PrintPage);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.toolBar, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.MainMenu, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(486, 175);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // toolBar
            // 
            this.toolBar.AllowMerge = false;
            this.toolBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolBar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolBar.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printButton,
            this.clearButton});
            this.toolBar.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolBar.Location = new System.Drawing.Point(0, 24);
            this.toolBar.Name = "toolBar";
            this.toolBar.Size = new System.Drawing.Size(486, 45);
            this.toolBar.Stretch = true;
            this.toolBar.TabIndex = 2;
            this.toolBar.Text = "Tool Bar";
            // 
            // printButton
            // 
            this.printButton.Enabled = false;
            this.printButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.printButton.Image = Properties.Resources.PrintIcon.ToBitmap();
            this.printButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.printButton.Name = "printButton";
            this.printButton.Padding = new System.Windows.Forms.Padding(2);
            this.printButton.Size = new System.Drawing.Size(36, 42);
            this.printButton.Text = "&Print";
            this.printButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.printButton.Click += new System.EventHandler(this.printMenu_Click);
            // 
            // clearButton
            // 
            this.clearButton.Enabled = false;
            this.clearButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.clearButton.Image = Properties.Resources.AlertsClearIcon.ToBitmap();
            this.clearButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.clearButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.clearButton.Name = "clearButton";
            this.clearButton.Padding = new System.Windows.Forms.Padding(2);
            this.clearButton.Size = new System.Drawing.Size(37, 42);
            this.clearButton.Text = "Clear";
            this.clearButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.clearButton.Click += new System.EventHandler(this.menuClear_Click);
            // 
            // MainMenu
            // 
            this.MainMenu.AllowMerge = false;
            this.MainMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.alertsMenu,
            this.helpMenu});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(486, 24);
            this.MainMenu.TabIndex = 1;
            this.MainMenu.Text = "Main Menu";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printMenu,
            this.toolStripSeparator1,
            this.closeMenu});
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(37, 20);
            this.fileMenu.Text = "&File";
            // 
            // printMenu
            // 
            this.printMenu.Enabled = false;
            this.printMenu.Image = new Icon(Properties.Resources.PrintIcon, 16, 16).ToBitmap();
            this.printMenu.Name = "printMenu";
            this.printMenu.Size = new System.Drawing.Size(145, 22);
            this.printMenu.Text = "&Print...";
            this.printMenu.Click += new System.EventHandler(this.printMenu_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(142, 6);
            // 
            // closeMenu
            // 
            this.closeMenu.Name = "closeMenu";
            this.closeMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.closeMenu.Size = new System.Drawing.Size(145, 22);
            this.closeMenu.Text = "&Close";
            this.closeMenu.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // alertsMenu
            // 
            this.alertsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearMenu});
            this.alertsMenu.Name = "alertsMenu";
            this.alertsMenu.Size = new System.Drawing.Size(49, 20);
            this.alertsMenu.Text = "&Alerts";
            // 
            // clearMenu
            // 
            this.clearMenu.Enabled = false;
            this.clearMenu.Image = new Icon(Properties.Resources.AlertsClearIcon, 16, 16).ToBitmap();
            this.clearMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.clearMenu.Name = "clearMenu";
            this.clearMenu.Size = new System.Drawing.Size(110, 22);
            this.clearMenu.Text = "&Clear...";
            this.clearMenu.Click += new System.EventHandler(this.menuClear_Click);
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usersManualMenu,
            this.helpMenuSep,
            this.aboutMenu});
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(44, 20);
            this.helpMenu.Text = "&Help";
            // 
            // usersManualMenu
            // 
            this.usersManualMenu.Image = new Icon(Properties.Resources.UserManualIcon, 16, 16).ToBitmap();
            this.usersManualMenu.Name = "usersManualMenu";
            this.usersManualMenu.Size = new System.Drawing.Size(149, 22);
            this.usersManualMenu.Text = "&User Manual";
            this.usersManualMenu.Click += new System.EventHandler(this.usersManualMenu_Click);
            // 
            // helpMenuSep
            // 
            this.helpMenuSep.Name = "helpMenuSep";
            this.helpMenuSep.Size = new System.Drawing.Size(142, 6);
            // 
            // aboutMenu
            // 
            this.aboutMenu.Image = new Icon(Properties.Resources.WestTelIcon, 16, 16).ToBitmap();
            this.aboutMenu.Name = "aboutMenu";
            this.aboutMenu.Size = new System.Drawing.Size(149, 22);
            this.aboutMenu.Text = "&About XXX";
            this.aboutMenu.Click += new System.EventHandler(this.aboutMenu_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.BackColor = System.Drawing.SystemColors.Window;
            this.statusStrip.Location = new System.Drawing.Point(0, 153);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(486, 22);
            this.statusStrip.TabIndex = 6;
            // 
            // AlarmDisplay
            // 
            this.ClientSize = new System.Drawing.Size(486, 175);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Icon = Properties.Resources.AlertIcon;
            this.KeyPreview = true;
            this.MainMenuStrip = this.MainMenu;
            this.Name = "AlarmDisplay";
            this.Text = "Alarm Viewer";
            this.Activated += new System.EventHandler(this.AlarmDisplay_Activated);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.AlarmDisplay_Closing);
            this.Load += new System.EventHandler(this.AlarmDisplay_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AlarmDisplay_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AlarmDisplay_KeyPress);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.toolBar.ResumeLayout(false);
            this.toolBar.PerformLayout();
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
		#endregion

        private void setTranslatableText()
        {
            this.TimeCol.HeaderText   = (Translate)("Time/Date");
            this.DescCol.HeaderText   = (Translate)("Description");
            this.toolBar.Text         = (Translate)("Tool Bar");
            this.printButton.Text     = (Translate)("&Print");
            this.clearButton.Text     = (Translate)("Clear");
            this.MainMenu.Text        = (Translate)("Main Menu");
            this.fileMenu.Text        = (Translate)("&File");
            this.printMenu.Text       = (Translate)("&Print...");
            this.closeMenu.Text       = (Translate)("&Close");
            this.alertsMenu.Text      = (Translate)("&Alerts");
            this.clearMenu.Text       = (Translate)("&Clear...");
            this.helpMenu.Text        = (Translate)("&Help");
            this.usersManualMenu.Text = (Translate)("&User Manual");
            this.aboutMenu.Text       = (Translate)("&About XXX");
            this.Text                 = (Translate)("Alarm Viewer");
        }

        private void CustomInitializeComponent()
        {
            this.     BackColor = SystemColors.Window;
            MainMenu. BackColor = SystemColors.Window;
            toolBar.  BackColor = SystemColors.Window;

            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            ((System.ComponentModel.ISupportInitialize)(this.gridAlarms)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();

            // gridAlarms
            this.gridAlarms.AllowUserToAddRows = false;
            this.gridAlarms.AllowUserToDeleteRows = false;
            this.gridAlarms.AllowUserToResizeColumns = false;
            this.gridAlarms.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(250)))), ((int)(((byte)(255)))));
            this.gridAlarms.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.gridAlarms.BackgroundColor = System.Drawing.SystemColors.Window;
            this.gridAlarms.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridAlarms.CausesValidation = false;
            this.gridAlarms.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.gridAlarms.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.gridAlarms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.gridAlarms.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TimeCol,
            this.DescCol});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(250)))), ((int)(((byte)(220)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridAlarms.DefaultCellStyle = dataGridViewCellStyle5;
            this.gridAlarms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridAlarms.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.gridAlarms.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridAlarms.Location = new System.Drawing.Point(0, 69);
            this.gridAlarms.Margin = new System.Windows.Forms.Padding(0);
            this.gridAlarms.MultiSelect = false;
            this.gridAlarms.Name = "gridAlarms";
            this.gridAlarms.ReadOnly = true;
            this.gridAlarms.RowHeadersVisible = false;
            this.gridAlarms.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridAlarms.RowTemplate.DividerHeight = 1;
            this.gridAlarms.RowTemplate.Height = 37;
            this.gridAlarms.RowTemplate.ReadOnly = true;
            this.gridAlarms.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.gridAlarms.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.gridAlarms.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridAlarms.ShowCellErrors = false;
            this.gridAlarms.ShowEditingIcon = false;
            this.gridAlarms.ShowRowErrors = false;
            this.gridAlarms.TabIndex = 0;
            this.gridAlarms.ShowCellToolTips = true;
            this.gridAlarms.CellPainting += PaintGridCell;
            this.gridAlarms.RowPrePaint += PaintGridRowBackground;
            this.gridAlarms.RowPostPaint += PaintGridRowSelection;
            this.gridAlarms.CellToolTipTextNeeded += CellToolTipTextNeeded;

            this.tableLayoutPanel1.Controls.Add(this.gridAlarms, 0, 2);

            ((System.ComponentModel.ISupportInitialize)(this.gridAlarms)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
        }

        private void btnClose_Click(object sender, System.EventArgs e)
		{
			Close();
		}

        private delegate void refreshDelegate();

        public void refresh()
        {
            try {BeginInvoke(new refreshDelegate(refreshGrid));} catch {}
        }

        private void refreshGrid()
        {
DataModel.logLockInfo(true, DataModel.alarmEventsSyncRoot);
            lock (DataModel.alarmEventsSyncRoot)
            {
                try
                {
    			    clearButton.Enabled = clearMenu.Enabled =
                      printMenu.Enabled = printButton.Enabled = (gridAlarms.RowCount > 0);
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.alarmEventsSyncRoot);
        }

        private void AlarmDisplay_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    e.Handled = true;
                    this.Close();
                    break;
                case Keys.F10:
                    e.Handled = true;
                    break;
            }            
        }

        private void AlarmDisplay_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 1) this.Close();
        }

		private void AlarmDisplay_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if ( ! systemShutdown)
			{
				e.Cancel = true;
				Hide();
			}
		}

		private void AlarmDisplay_Load(object sender, System.EventArgs e)
		{
            // remove unnecessary separators
            removeUnnecessarySeparators(toolBar);

DataModel.logLockInfo(true, DataModel.alarmEventsSyncRoot);
            lock (DataModel.alarmEventsSyncRoot)
            {
                try
                {
                    Text = Application.ProductName + (Translate)(" - Alerts");
			        aboutMenu.Text = aboutMenu.Text.Replace("XXX",Application.ProductName);

                    // KLUDGE
                    DataTable alarmDataTable = controller.dataModel.ds.Tables["AlarmEvents"];

                    if (alarmDataTable.Rows.Count >= 10)
                    {
                        foreach (DataRow row in kludgeRowList)
                        {
                            try {alarmDataTable.Rows.Remove(row);} catch {}
                        }
                    }

                    refreshGrid();
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.alarmEventsSyncRoot);
        }

		private void aboutMenu_Click(object sender, EventArgs e)
		{
			Splash.ShowAbout(controller.Position);
		}

		private void usersManualMenu_Click(object sender, EventArgs e)
		{
			Console.ShowUsersManual();
		}

        private void printMenu_Click(object sender, EventArgs e)
		{
			if (printSetup.ShowDialog() == DialogResult.OK)
				printDoc.Print();
		}

		private void PrintHeader(Graphics graphic)
		{
			LinePosition = 10;
			printerFont = new Font("Lucida Console", fontSize );
			printerBrush = new SolidBrush(Color.Black);
			LinePosition = TopMargin;

			printerFont = new Font(printerFont,FontStyle.Bold);
			WriteToPrinter(graphic, "Alerts");
			printerFont = new Font(printerFont,FontStyle.Regular);

			graphic.DrawLine(Pens.Black,new PointF(LeftMargin,LinePosition),new PointF(printDoc.DefaultPageSettings.PaperSize.Width - LeftMargin,LinePosition));
			LinePosition += 20;
		}

		private void printDoc_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
		{
			printerFont = new Font("Lucida Console", fontSize );
			// force page header print
			LinePosition = printSetup.PrinterSettings.DefaultPageSettings.PaperSize.Height + 10;
			printerFont = new Font(printerFont,FontStyle.Regular);

DataModel.logLockInfo(true, DataModel.alarmEventsSyncRoot);
            lock (DataModel.alarmEventsSyncRoot)
            {
                try
                {
                    foreach (DataGridViewRow GRow in gridAlarms.Rows)
                    {
                        if ((GRow != null) && (GRow.DataBoundItem != null))
                        {
                            DataRowView row = (DataRowView) GRow.DataBoundItem;

                            if (row != null)
                            {
				                WriteToPrinter(e.Graphics, row["timeStamp"].ToString().PadRight(20) + " " +
                                                           row["alarmText"].ToString());
                            }
                        }
                    }
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.alarmEventsSyncRoot);
		}

		public void WriteToPrinter(Graphics graphic, string Text)
		{
			if (LinePosition > printSetup.PrinterSettings.DefaultPageSettings.PaperSize.Height)	PrintHeader(graphic);

			SizeF lineSize = graphic.MeasureString(Text, printerFont, new SizeF((float) printDoc.DefaultPageSettings.PaperSize.Width - LeftMargin, 99));
			graphic.DrawString(Text, printerFont, printerBrush, new RectangleF((float) LeftMargin, LinePosition, printDoc.DefaultPageSettings.PaperSize.Width - LeftMargin, LinePosition + 5));
			LinePosition += lineSize.Height + 5;
		}

		private void AlarmDisplay_Activated(object sender, System.EventArgs e)
		{
DataModel.logLockInfo(true, DataModel.alarmEventsSyncRoot);
            lock (DataModel.alarmEventsSyncRoot)
            {
                try
                {
                    gridAlarms.Focus();
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.alarmEventsSyncRoot);

            System.Windows.Forms.Application.DoEvents();

DataModel.logLockInfo(true, DataModel.alarmEventsSyncRoot);
            lock (DataModel.alarmEventsSyncRoot)
            {
                try
                {
                    gridAlarms.Focus();
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.alarmEventsSyncRoot);
        }

		private void menuClear_Click(object sender, EventArgs e)
		{
			DialogResult result = MessageBox.Show(this, "Are you sure you want to clear all alerts?",
#if DEBUG
                Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
#else
                Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
#endif

			if (result == DialogResult.Yes)
            {
DataModel.logLockInfo(true, DataModel.alarmEventsSyncRoot);
                lock (DataModel.alarmEventsSyncRoot)
                {
                    try
                    {
                        controller.dataModel.ds.Tables["AlarmEvents"].Clear();			
                        refresh();
                    }
                    catch {}
                }
DataModel.logLockInfo(false, DataModel.alarmEventsSyncRoot);
            }
		}

        private Settings.ColorInfo getColorInfo(bool isAlt, Console.CallRowInfo.State state)
        {
            if ( ! isAlt) return (rowColorInfo    [(int)state]);
            else          return (altRowColorInfo [(int)state]);
        }

        private void PaintGridRowBackground(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            try
            {
                // if row or cell is invalid, return
                if (e.RowIndex >= 0 && sender != null)
                {
                    Grid grid = (Grid)sender;

                    // background (draw custom selection)
                    bool isAlt      = e.RowIndex % 2 != 0;
                    bool isSelected = (e.State & DataGridViewElementStates.Selected) > 0;

                    Console.CallRowInfo.State state =
                        Console.CallRowInfo.getState(isSelected, grid.isHovered(e.RowIndex));

                    Settings.ColorInfo colorInfo = getColorInfo(isAlt, state);

                    bool selectedRowIsAlt = grid.SelectedRows[0].Index % 2 != 0;

                    Settings.ColorInfo selectedColorInfo =
                        getColorInfo(selectedRowIsAlt, Console.CallRowInfo.State.Selected);

                    grid.paintRowBackground(e, colorInfo, selectedColorInfo, isSelected);
                }
            }
            catch {}
        }

        private void PaintGridRowSelection(object sender, DataGridViewRowPostPaintEventArgs e)
        {
        }

        private void PaintGridCell(object sender, DataGridViewCellPaintingEventArgs e)
        {
            try
            {
                // if row and cell is valid
                if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
                {
                    e.Handled = true;

                    Grid grid = (Grid)sender;

                    // background (draw custom selection)
                    bool isAlt      = e.RowIndex % 2 != 0;
                    bool isSelected = (e.State & DataGridViewElementStates.Selected) > 0;

                    Console.CallRowInfo.State state =
                        Console.CallRowInfo.getState(isSelected, grid.isHovered(e.RowIndex));

                    Settings.ColorInfo colorInfo = getColorInfo(isAlt, state);

                    Font font = new Font(e.CellStyle.Font, colorInfo.bold ? FontStyle.Bold : FontStyle.Regular);

                    string valueStr = e.Value.ToString();

                    Brush brush = new SolidBrush(colorInfo.foregroundColor);

                    StringFormat stringFormat  = new StringFormat();
                    stringFormat.Alignment     = StringAlignment.Near;
                    stringFormat.LineAlignment = StringAlignment.Center;
                    stringFormat.Trimming      = StringTrimming.EllipsisCharacter;

                    Rectangle bounds = e.CellBounds;
                    bounds.X     += 1;
                    bounds.Width -= 1;

                    bounds.Y      += 5;
                    bounds.Height -= 10;

                    // make room for selection arrow
                    if (e.ColumnIndex == 0)
                    {
                        bounds.X     += 15;
                        bounds.Width -= 15;
                    }

                    e.Graphics.DrawString(e.FormattedValue.ToString(), font, brush, bounds, stringFormat);
                }
            }
            catch {}
        }

        private void CellToolTipTextNeeded(object sender, DataGridViewCellToolTipTextNeededEventArgs e)
        {
DataModel.logLockInfo(true, DataModel.alarmEventsSyncRoot);
            lock (DataModel.alarmEventsSyncRoot)
            {
                try
                {
                    if (e.RowIndex >= 0)
                    {
                        DataGridViewRow GRow = gridAlarms.Rows[e.RowIndex];
                        {
                            if ((GRow != null) && (GRow.DataBoundItem != null))
                            {
                                DataRowView row = (DataRowView)GRow.DataBoundItem;

                                e.ToolTipText = row["alarmText"].ToString();

                                // if the tool tip is too long, add some line breaks
                                int remainingLength = e.ToolTipText.Length;
                                int i               = 0;

                                const int MAX_LINE_LENGTH = 80;

                                while (remainingLength > MAX_LINE_LENGTH)
                                {
                                    remainingLength -= MAX_LINE_LENGTH;

                                    for (i += MAX_LINE_LENGTH; i != e.ToolTipText.Length; ++i, --remainingLength)
                                    {
                                        if (e.ToolTipText[i] == ' ')
                                        {
                                            e.ToolTipText = e.ToolTipText.Remove(i,1);
                                            e.ToolTipText = e.ToolTipText.Insert(i,"\n");
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.alarmEventsSyncRoot);
        }
	}
}
