﻿namespace WestTel.E911
{
    partial class TransferSelectionDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TransferSelectionDialog));
            this.cancelButton = new System.Windows.Forms.Button();
            this.dialButton = new System.Windows.Forms.Button();
            this.conferenceButton = new System.Windows.Forms.Button();
            this.attendedButton = new System.Windows.Forms.Button();
            this.blindButton = new System.Windows.Forms.Button();
            this.tandemButton = new System.Windows.Forms.Button();
            this.flashButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cancelButton.AutoSize = true;
            this.cancelButton.BackColor = System.Drawing.SystemColors.Window;
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(214, 78);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 25);
            this.cancelButton.TabIndex = 5;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = false;
            // 
            // dialButton
            // 
            this.dialButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dialButton.AutoSize = true;
            this.dialButton.BackColor = System.Drawing.SystemColors.Window;
            this.dialButton.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.dialButton.FlatAppearance.BorderSize = 0;
            this.dialButton.Image = ((System.Drawing.Image)(resources.GetObject("dialButton.Image")));
            this.dialButton.Location = new System.Drawing.Point(417, 12);
            this.dialButton.Name = "dialButton";
            this.dialButton.Size = new System.Drawing.Size(75, 60);
            this.dialButton.TabIndex = 4;
            this.dialButton.Text = "&Dial";
            this.dialButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.dialButton.UseVisualStyleBackColor = false;
            this.dialButton.Click += new System.EventHandler(this.dialButton_Click);
            // 
            // conferenceButton
            // 
            this.conferenceButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.conferenceButton.AutoSize = true;
            this.conferenceButton.BackColor = System.Drawing.SystemColors.Window;
            this.conferenceButton.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.conferenceButton.FlatAppearance.BorderSize = 0;
            this.conferenceButton.Image = ((System.Drawing.Image)(resources.GetObject("conferenceButton.Image")));
            this.conferenceButton.Location = new System.Drawing.Point(336, 12);
            this.conferenceButton.Name = "conferenceButton";
            this.conferenceButton.Size = new System.Drawing.Size(75, 60);
            this.conferenceButton.TabIndex = 3;
            this.conferenceButton.Text = "&Conference";
            this.conferenceButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.conferenceButton.UseVisualStyleBackColor = false;
            this.conferenceButton.Click += new System.EventHandler(this.conferenceButton_Click);
            // 
            // attendedButton
            // 
            this.attendedButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.attendedButton.AutoSize = true;
            this.attendedButton.BackColor = System.Drawing.SystemColors.Window;
            this.attendedButton.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.attendedButton.FlatAppearance.BorderSize = 0;
            this.attendedButton.Image = ((System.Drawing.Image)(resources.GetObject("attendedButton.Image")));
            this.attendedButton.Location = new System.Drawing.Point(255, 12);
            this.attendedButton.Name = "attendedButton";
            this.attendedButton.Size = new System.Drawing.Size(75, 60);
            this.attendedButton.TabIndex = 2;
            this.attendedButton.Text = "&Attended";
            this.attendedButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.attendedButton.UseVisualStyleBackColor = false;
            this.attendedButton.Click += new System.EventHandler(this.attendedButton_Click);
            // 
            // blindButton
            // 
            this.blindButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.blindButton.AutoSize = true;
            this.blindButton.BackColor = System.Drawing.SystemColors.Window;
            this.blindButton.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.blindButton.FlatAppearance.BorderSize = 0;
            this.blindButton.Image = ((System.Drawing.Image)(resources.GetObject("blindButton.Image")));
            this.blindButton.Location = new System.Drawing.Point(174, 12);
            this.blindButton.Name = "blindButton";
            this.blindButton.Size = new System.Drawing.Size(75, 60);
            this.blindButton.TabIndex = 1;
            this.blindButton.Text = "&Blind";
            this.blindButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.blindButton.UseVisualStyleBackColor = false;
            this.blindButton.Click += new System.EventHandler(this.blindButton_Click);
            // 
            // tandemButton
            // 
            this.tandemButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tandemButton.AutoSize = true;
            this.tandemButton.BackColor = System.Drawing.SystemColors.Window;
            this.tandemButton.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.tandemButton.FlatAppearance.BorderSize = 0;
            this.tandemButton.Image = ((System.Drawing.Image)(resources.GetObject("tandemButton.Image")));
            this.tandemButton.Location = new System.Drawing.Point(12, 12);
            this.tandemButton.Name = "tandemButton";
            this.tandemButton.Size = new System.Drawing.Size(75, 60);
            this.tandemButton.TabIndex = 0;
            this.tandemButton.Text = "&9-1-1";
            this.tandemButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tandemButton.UseVisualStyleBackColor = false;
            this.tandemButton.Click += new System.EventHandler(this.tandemButton_Click);
            // 
            // flashButton
            // 
            this.flashButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.flashButton.AutoSize = true;
            this.flashButton.BackColor = System.Drawing.SystemColors.Window;
            this.flashButton.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.flashButton.FlatAppearance.BorderSize = 0;
            this.flashButton.Image = ((System.Drawing.Image)(resources.GetObject("flashButton.Image")));
            this.flashButton.Location = new System.Drawing.Point(93, 12);
            this.flashButton.Name = "flashButton";
            this.flashButton.Size = new System.Drawing.Size(75, 60);
            this.flashButton.TabIndex = 0;
            this.flashButton.Text = "&Flash";
            this.flashButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.flashButton.UseVisualStyleBackColor = false;
            this.flashButton.Click += new System.EventHandler(this.flashButton_Click);
            // 
            // TransferSelectionDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(503, 119);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.dialButton);
            this.Controls.Add(this.conferenceButton);
            this.Controls.Add(this.attendedButton);
            this.Controls.Add(this.blindButton);
            this.Controls.Add(this.tandemButton);
            this.Controls.Add(this.flashButton);
            this.Icon = global::WestTel.E911.Properties.Resources.PhonebookIcon;
            this.MinimizeBox = false;
            this.Name = "TransferSelectionDialog";
            this.Text = "Select Transfer Type";
            this.Shown += new System.EventHandler(this.TransferSelectionDialog_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cancelButton;
        public System.Windows.Forms.Button tandemButton;
        public System.Windows.Forms.Button flashButton;
        public System.Windows.Forms.Button blindButton;
        public System.Windows.Forms.Button attendedButton;
        public System.Windows.Forms.Button conferenceButton;
        public System.Windows.Forms.Button dialButton;
    }
}