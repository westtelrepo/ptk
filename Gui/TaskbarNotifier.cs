// C# TaskbarNotifier Class v1.0
// by John O'Byrne - 02 december 2002
// 01 april 2003 : Small fix in the OnMouseUp handler
// 11 january 2003 : Patrick Vanden Driessche <pvdd@devbrains.be> added a few enhancements
//           Small Enhancements/Bugfix
//           Small bugfix: When Content text measures larger than the corresponding ContentRectangle
//                         the focus rectangle was not correctly drawn. This has been solved.
//           Added KeepVisibleOnMouseOver
//           Added ReShowOnMouseOver
//           Added If the Title or Content are too long to fit in the corresponding Rectangles,
//                 the text is truncateed and the ellipses are appended (StringTrimming).

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;

namespace WestTel.E911
{
	/// <summary>
	/// TaskbarNotifier allows to display MSN style/Skinned instant messaging popups
	/// </summary>
	public class TaskbarNotifier : System.Windows.Forms.Form
	{
        public static Settings.ColorInfo windowColorInfo =
            new Settings.ColorInfo(Color.FromArgb(unchecked((int)0xFF1976D2)), // Blue 700
                                   Color.FromArgb(unchecked((int)0x401976D2)), // Blue 700 (transparent)
                                   Color.FromArgb(220, Color.White),
                                   false);

		#region HideWindowExterns
		[DllImport("user32.dll")] 
		public static extern int SetWindowLong( IntPtr window, int index, int value); 
		[DllImport("user32.dll")] 
		public static extern int GetWindowLong( IntPtr window, int index); 
		#endregion

		#region Hide Window Constants
		public const int GWL_EXSTYLE = -20; 
		public const int WS_EX_TOOLWINDOW = 0x00000080; 
		public const int WS_EX_APPWINDOW = 0x00040000; 
		#endregion

		#region TaskbarNotifier Protected Members
		protected const int PreferredWidth  = 190;
		protected const int PreferredHeight = 160;
        protected Icon BackgroundIcon = null;
		protected Bitmap CloseBitmap = null;
		protected Point CloseBitmapLocation;
		protected Size CloseBitmapSize;
		protected Rectangle RealTitleRectangle;
		protected Rectangle RealContentRectangle;
		protected Rectangle WorkAreaRectangle;
		protected System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
		protected TaskbarStates taskbarState = TaskbarStates.hidden;
		protected string titleText;
		protected string contentText;
		protected Font normalTitleFont = new Font("Arial",12,FontStyle.Regular,GraphicsUnit.Pixel);
		protected Font normalContentFont = new Font("Arial",11,FontStyle.Regular,GraphicsUnit.Pixel);
		protected int nShowEvents;
		protected int nHideEvents;
		protected int nVisibleEvents;
		protected int nIncrementShow;
		protected int nIncrementHide;
		protected bool bIsMouseOverPopup = false;
		protected bool bIsMouseOverClose = false;
		protected bool bIsMouseOverContent = false;
		protected bool bIsMouseOverTitle = false;
		protected bool bIsMouseDown = false;
		protected bool bKeepVisibleOnMouseOver = true;			// Added Rev 002
		protected bool bReShowOnMouseOver = false;				// Added Rev 002
		#endregion
						
		#region TaskbarNotifier Public Members
		public Rectangle TitleRectangle;
		public Rectangle ContentRectangle;
		public bool TitleClickable = false;
		public bool ContentClickable = true;
		public bool CloseClickable = true;
		public bool EnableSelectionRectangle = true;
		public event EventHandler CloseClick = null;
		public event EventHandler TitleClick = null;
		public event EventHandler ContentClick = null;
		#endregion
	
		#region TaskbarNotifier Enums
		/// <summary>
		/// List of the different popup animation status
		/// </summary>
		public enum TaskbarStates
		{
			hidden = 0,
			appearing = 1,
			visible = 2,
			disappearing = 3
		}
		#endregion

		#region TaskbarNotifier Constructor
		/// <summary>
		/// The Constructor for TaskbarNotifier
		/// </summary>
		public TaskbarNotifier()
		{
            this.Visible = false;

			// Window Style
			FormBorderStyle = FormBorderStyle.None;
			WindowState = FormWindowState.Minimized;
			
			TopMost = true;
			MaximizeBox = false;
			MinimizeBox = false;
			ControlBox = false;
			ShowInTaskbar = false;

            timer.Enabled = true;
			timer.Tick += new EventHandler(OnTimer);

            try {CreateHandle();} catch {}
		}
		#endregion

		public delegate void OnExitMessageReceived();
		public event OnExitMessageReceived ExitMessageReceived;

        public void init(OnExitMessageReceived exitMessageReceived)
        {
            try
            {
		        Width  = PreferredWidth;
		        Height = PreferredHeight;

                SetCloseBitmap(new Bitmap(GetType(), "Gui.Resources.close.bmp"),
                               Color.FromArgb(255, 0, 255), new Point(PreferredWidth - 20, 8));

                BackgroundIcon = new Icon(Properties.Resources.WestTelIcon, 24, 24);
            }
            catch {}

            TitleRectangle   = new Rectangle(37, 9, PreferredWidth - 57, 25);
            ContentRectangle = new Rectangle(8, 40, PreferredWidth - 14, PreferredHeight - 44);

            CloseClickable     = true;
            bReShowOnMouseOver = true;

            ExitMessageReceived += new TaskbarNotifier.OnExitMessageReceived(exitMessageReceived);
        }

		#region TaskbarNotifier Properties
		/// <summary>
		/// Get the current TaskbarState (hidden, showing, visible, hiding)
		/// </summary>
		public TaskbarStates TaskbarState
		{
			get {return taskbarState;}
		}

		/// <summary>
		/// Get/Set the popup Title Text
		/// </summary>
		public string TitleText
		{
			get {return titleText;}
			set {titleText=value; Refresh();}
		}

		/// <summary>
		/// Get/Set the popup Content Text
		/// </summary>
		public string ContentText
		{
			get {return contentText;}
			set {contentText=value; Refresh();}
		}

		#endregion

		#region TaskbarNotifier Public Methods
		[DllImport("user32.dll")]
		private static extern Boolean ShowWindow(IntPtr hWnd,Int32 nCmdShow);
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        public static Screen screen = Screen.PrimaryScreen;

        /// <summary>
		/// Displays the popup for a certain amount of time
		/// </summary>
		/// <param name="strTitle">The string which will be shown as the title of the popup</param>
		/// <param name="strContent">The string which will be shown as the content of the popup</param>
		/// <param name="nTimeToShow">Duration of the showing animation (in milliseconds)</param>
		/// <param name="nTimeToStay">Duration of the visible state before collapsing (in milliseconds)</param>
		/// <param name="nTimeToHide">Duration of the hiding animation (in milliseconds)</param>
		/// <returns>Nothing</returns>
		public void Show(string strTitle, string strContent, int nTimeToShow, int nTimeToStay, int nTimeToHide)
		{
            IntPtr taskbarHandle = FindWindow("Shell_TrayWnd", null);
            if (taskbarHandle != null) screen = Screen.FromHandle(taskbarHandle);

            WorkAreaRectangle = screen.WorkingArea;

            titleText      = strTitle;
			contentText    = strContent;
			nVisibleEvents = nTimeToStay;

            CalculateMouseRectangles();

			// We calculate the pixel increment and the timer value for the showing animation
			int nEvents;
			if (nTimeToShow > 10)
			{
				nEvents = Math.Min((nTimeToShow / 10), PreferredHeight);
				nShowEvents = nTimeToShow / nEvents;
				nIncrementShow = PreferredHeight / nEvents;
			}
			else
			{
				nShowEvents = 10;
				nIncrementShow = PreferredHeight;
			}

			// We calculate the pixel increment and the timer value for the hiding animation
			if( nTimeToHide > 10)
			{
				nEvents = Math.Min((nTimeToHide / 10), PreferredHeight);
				nHideEvents = nTimeToHide / nEvents;
				nIncrementHide = PreferredHeight / nEvents;
			}
			else
			{
				nHideEvents = 10;
				nIncrementHide = PreferredHeight;
			}

			switch (taskbarState)
			{
				case TaskbarStates.hidden:
					taskbarState = TaskbarStates.appearing;
					SetBounds(WorkAreaRectangle.Right-PreferredWidth-17, WorkAreaRectangle.Bottom-1, PreferredWidth, 0);
					timer.Interval = nShowEvents;
					timer.Start();
					// We Show the popup without stealing focus
					ShowWindow(this.Handle, 4);
					break;

				case TaskbarStates.appearing:
					Refresh();
					break;

				case TaskbarStates.visible:
					timer.Stop();
					timer.Interval = nVisibleEvents;
					timer.Start();
					Refresh();
					break;

				case TaskbarStates.disappearing:
					timer.Stop();
					taskbarState = TaskbarStates.visible;
					SetBounds(WorkAreaRectangle.Right-PreferredWidth-17, WorkAreaRectangle.Bottom-PreferredHeight-1, PreferredWidth, PreferredHeight);
					timer.Interval = nVisibleEvents;
					timer.Start();
					Refresh();
					break;
			}
		}

		/// <summary>
		/// Hides the popup
		/// </summary>
		/// <returns>Nothing</returns>
		public new void Hide()
		{
			if (taskbarState != TaskbarStates.hidden)
			{
				timer.Stop();
				taskbarState = TaskbarStates.hidden;
				base.Hide();
			}
		}

		/// <summary>
		/// Sets the 3-State Close Button bitmap, its transparency color and its coordinates
		/// </summary>
		/// <param name="strFilename">Path of the 3-state Close button Bitmap on the disk (width must a multiple of 3)</param>
		/// <param name="transparencyColor">Color of the Bitmap which won't be visible</param>
		/// <param name="position">Location of the close button on the popup</param>
		/// <returns>Nothing</returns>
		public void SetCloseBitmap(string strFilename, Color transparencyColor, Point position)
		{
			CloseBitmap = new Bitmap(strFilename);
			CloseBitmap.MakeTransparent(transparencyColor);
			CloseBitmapSize = new Size(CloseBitmap.Width/3, CloseBitmap.Height);
			CloseBitmapLocation = position;
		}

		/// <summary>
		/// Sets the 3-State Close Button bitmap, its transparency color and its coordinates
		/// </summary>
		/// <param name="image">Image/Bitmap object which represents the 3-state Close button Bitmap (width must be a multiple of 3)</param>
		/// <param name="transparencyColor">Color of the Bitmap which won't be visible</param>
		/// /// <param name="position">Location of the close button on the popup</param>
		/// <returns>Nothing</returns>
		public void SetCloseBitmap(Image image, Color transparencyColor, Point position)
		{
			CloseBitmap = new Bitmap(image);
			CloseBitmap.MakeTransparent(transparencyColor);
			CloseBitmapSize = new Size(CloseBitmap.Width/3, CloseBitmap.Height);
			CloseBitmapLocation = position;
		}
		#endregion

		#region TaskbarNotifier Protected Methods
		protected void DrawCloseButton(Graphics grfx)
		{
			if (CloseBitmap != null)
			{	
				Rectangle rectDest = new Rectangle(CloseBitmapLocation, CloseBitmapSize);
				Rectangle rectSrc;

				if (bIsMouseOverClose)
				{
					if (bIsMouseDown)
						rectSrc = new Rectangle(new Point(CloseBitmapSize.Width*2, 0), CloseBitmapSize);
					else
						rectSrc = new Rectangle(new Point(CloseBitmapSize.Width, 0), CloseBitmapSize);
				}		
				else
					rectSrc = new Rectangle(new Point(0, 0), CloseBitmapSize);
					

				grfx.DrawImage(CloseBitmap, rectDest, rectSrc, GraphicsUnit.Pixel);
			}
		}

		protected void DrawText(Graphics grfx)
		{
			if (titleText != null && titleText.Length != 0)
			{
				StringFormat sf = new StringFormat();
				sf.Alignment = StringAlignment.Near;
				sf.LineAlignment = StringAlignment.Near;
				sf.Trimming = StringTrimming.Word;				// Added Rev 002

                if (windowColorInfo.bold) normalTitleFont = new Font(normalTitleFont, FontStyle.Bold);
                else                      normalTitleFont = new Font(normalTitleFont, FontStyle.Regular);

                grfx.DrawString(titleText, normalTitleFont,
                    windowColorInfo.getForegroundBrush(), TitleRectangle, sf);
			}

			if (contentText != null && contentText.Length != 0)
			{
				StringFormat sf = new StringFormat();
				sf.Alignment = StringAlignment.Center;
				sf.LineAlignment = StringAlignment.Center;
				sf.FormatFlags = StringFormatFlags.MeasureTrailingSpaces;
				sf.Trimming = StringTrimming.EllipsisCharacter;							// Added Rev 002

                if (windowColorInfo.bold) normalContentFont = new Font(normalContentFont, FontStyle.Bold);
                else                      normalContentFont = new Font(normalContentFont, FontStyle.Regular);

                grfx.DrawString(contentText, normalContentFont, windowColorInfo.getForegroundBrush(), ContentRectangle, sf);
			}
		}

		protected void CalculateMouseRectangles()
		{
			Graphics grfx = CreateGraphics();
			StringFormat sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			sf.FormatFlags = StringFormatFlags.MeasureTrailingSpaces;
			SizeF sizefTitle = grfx.MeasureString(titleText, normalTitleFont, TitleRectangle.Width, sf);
			SizeF sizefContent = grfx.MeasureString(contentText, normalContentFont, ContentRectangle.Width, sf);
			grfx.Dispose();

			// Added Rev 002
	        //We should check if the title size really fits inside the pre-defined title rectangle
			if (sizefTitle.Height > TitleRectangle.Height)
			{
				RealTitleRectangle = new Rectangle(TitleRectangle.Left, TitleRectangle.Top, TitleRectangle.Width , TitleRectangle.Height );
			} 
			else
			{
				RealTitleRectangle = new Rectangle(TitleRectangle.Left, TitleRectangle.Top, (int)sizefTitle.Width, (int)sizefTitle.Height);
			}
			RealTitleRectangle.Inflate(0,2);

			// Added Rev 002
			//We should check if the Content size really fits inside the pre-defined Content rectangle
			if (sizefContent.Height > ContentRectangle.Height)
			{
				RealContentRectangle = new Rectangle((ContentRectangle.Width-(int)sizefContent.Width)/2+ContentRectangle.Left, ContentRectangle.Top, (int)sizefContent.Width, ContentRectangle.Height );
			}
			else
			{
				RealContentRectangle = new Rectangle((ContentRectangle.Width-(int)sizefContent.Width)/2+ContentRectangle.Left, (ContentRectangle.Height-(int)sizefContent.Height)/2+ContentRectangle.Top, (int)sizefContent.Width, (int)sizefContent.Height);
			}
			RealContentRectangle.Inflate(0,2);
		}
        #endregion

        #region TaskbarNotifier Events Overrides
        protected void OnTimer(Object obj, EventArgs ea)
		{
			switch (taskbarState)
			{
				case TaskbarStates.appearing:
					if (Height < PreferredHeight)
						SetBounds(Left, Top-nIncrementShow ,Width, Height + nIncrementShow);
					else
					{
						timer.Stop();
						Height = PreferredHeight;
						timer.Interval = nVisibleEvents;
						taskbarState = TaskbarStates.visible;
						timer.Start();
					}
					break;

				case TaskbarStates.visible:
					timer.Stop();
					timer.Interval = nHideEvents;
					// Added Rev 002
					if ((bKeepVisibleOnMouseOver && !bIsMouseOverPopup ) || (!bKeepVisibleOnMouseOver))
					{
						taskbarState = TaskbarStates.disappearing;
					} 
					//taskbarState = TaskbarStates.disappearing;		// Rev 002
					timer.Start();
					break;

				case TaskbarStates.disappearing:
					// Added Rev 002
					if (bReShowOnMouseOver && bIsMouseOverPopup) 
					{
						taskbarState = TaskbarStates.appearing;
					} 
					else 
					{
						if (Top < WorkAreaRectangle.Bottom)
							SetBounds(Left, Top + nIncrementHide, Width, Height - nIncrementHide);
						else
							Hide();
					}
					break;
			}
		}

		protected override void OnMouseEnter(EventArgs ea)
		{
			base.OnMouseEnter(ea);
			bIsMouseOverPopup = true;
			Refresh();
		}

		protected override void OnMouseLeave(EventArgs ea)
		{
			base.OnMouseLeave(ea);
			bIsMouseOverPopup = false;
			bIsMouseOverClose = false;
			bIsMouseOverTitle = false;
			bIsMouseOverContent = false;
			Refresh();
		}

		protected override void OnMouseMove(MouseEventArgs mea)
		{
			base.OnMouseMove(mea);

			bool bContentModified = false;
			
			if ( (mea.X > CloseBitmapLocation.X) && (mea.X < CloseBitmapLocation.X + CloseBitmapSize.Width) && (mea.Y > CloseBitmapLocation.Y) && (mea.Y < CloseBitmapLocation.Y + CloseBitmapSize.Height) && CloseClickable )
			{
				if (!bIsMouseOverClose)
				{
					bIsMouseOverClose = true;
					bIsMouseOverTitle = false;
					bIsMouseOverContent = false;
					Cursor = Cursors.Hand;
					bContentModified = true;
				}
			}
			else if (RealContentRectangle.Contains(new Point(mea.X, mea.Y)) && ContentClickable)
			{
				if (!bIsMouseOverContent)
				{
					bIsMouseOverClose = false;
					bIsMouseOverTitle = false;
					bIsMouseOverContent = true;
					Cursor = Cursors.Hand;
					bContentModified = true;
				}
			}
			else if (RealTitleRectangle.Contains(new Point(mea.X, mea.Y)) && TitleClickable)
			{
				if (!bIsMouseOverTitle)
				{
					bIsMouseOverClose = false;
					bIsMouseOverTitle = true;
					bIsMouseOverContent = false;
					Cursor = Cursors.Hand;
					bContentModified = true;
				}
			}
			else
			{
				if (bIsMouseOverClose || bIsMouseOverTitle || bIsMouseOverContent)
					bContentModified = true;

				bIsMouseOverClose = false;
				bIsMouseOverTitle = false;
				bIsMouseOverContent = false;
				Cursor = Cursors.Default;
			}

			if (bContentModified)
				Refresh();
		}

		protected override void OnMouseDown(MouseEventArgs mea)
		{
			base.OnMouseDown(mea);
			bIsMouseDown = true;
			
			if (bIsMouseOverClose)
				Refresh();
		}

		protected override void OnMouseUp(MouseEventArgs mea)
		{
			base.OnMouseUp(mea);
			bIsMouseDown = false;

			if (bIsMouseOverClose)
			{
				Hide();
							
				if (CloseClick != null) CloseClick(this, new EventArgs());
			}
			else if (bIsMouseOverTitle)
			{
				if (TitleClick != null) TitleClick(this, new EventArgs());
			}
			else if (bIsMouseOverContent)
			{
				if (ContentClick != null) ContentClick(this, new EventArgs());
			}
		}

		private void InitializeComponent()
		{
            this.SuspendLayout();
            // 
            // TaskbarNotifier
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.ControlBox = false;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = Properties.Resources.WestTelIcon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TaskbarNotifier";
            this.ShowInTaskbar = false;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.TaskbarNotifier_Load);
            this.ResumeLayout(false);

		}

        System.Threading.Timer colorsUpdatedTimer = null;

        public void colorsUpdated()
        {
            if ( ! Visible)
            {
                if (colorsUpdatedTimer == null) colorsUpdatedTimer = new System.Threading.Timer(colorsUpdated);
                colorsUpdatedTimer.Change(500, -1);
            }
            else
            {
                colorsUpdated(null);
            }
        }

        private delegate void ColorsUpdatedDelegate();

        private void colorsUpdated(object notused)
        {
            try   { BeginInvoke(new ColorsUpdatedDelegate(Refresh)); }
            catch { try {Refresh();} catch{} }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
		{
			Graphics grfx = e.Graphics;
			grfx.PageUnit = GraphicsUnit.Pixel;
			
            Bitmap   offscreenBitmap   = new Bitmap(PreferredWidth, PreferredHeight);
            Graphics offScreenGraphics = Graphics.FromImage(offscreenBitmap);

            Rectangle rect = new Rectangle(0,0,PreferredWidth,PreferredHeight);

            Color topColor    = windowColorInfo.backgroundColor1;
            Color bottomColor = windowColorInfo.backgroundColor2;

            offScreenGraphics.FillRectangle(Brushes.Black, rect);

            offScreenGraphics.FillRectangle(new LinearGradientBrush(
                rect, topColor, bottomColor, LinearGradientMode.Vertical), rect);

            offScreenGraphics.DrawIcon(BackgroundIcon, 10, 10);
            offScreenGraphics.DrawRectangle(new Pen(windowColorInfo.foregroundColor, 0.5f), rect);
                
            DrawCloseButton(offScreenGraphics);
            DrawText(offScreenGraphics);

            grfx.DrawImage(offscreenBitmap, 0, 0);
		}
		#endregion

		private void TaskbarNotifier_Load(object sender, System.EventArgs e)
		{
			int windowStyle = GetWindowLong(Handle, GWL_EXSTYLE); 
			SetWindowLong(Handle, GWL_EXSTYLE, windowStyle | WS_EX_TOOLWINDOW); 
		}

		private static int WM_QUERYENDSESSION = 0x11;
		protected override void WndProc(ref Message m)
		{
			if (m.Msg == WM_QUERYENDSESSION)
			{
				if (ExitMessageReceived != null) ExitMessageReceived();
			}
			base.WndProc (ref m);
		}

#if DEBUG
        public void MoveOver() {Left -= Width + 5;}
#else
        public void MoveOver() {Left -= 10;}
#endif
    }
}
