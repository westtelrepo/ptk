﻿namespace WestTel.E911
{
    partial class ColorEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitter = new System.Windows.Forms.SplitContainer();
            this.treeView = new System.Windows.Forms.TreeView();
            this.gradientLabel = new System.Windows.Forms.Label();
            this.gradientSpinBox = new System.Windows.Forms.NumericUpDown();
            this.backgroundEqualLabel = new System.Windows.Forms.Label();
            this.shadowSlider = new System.Windows.Forms.TrackBar();
            this.textSlider = new System.Windows.Forms.TrackBar();
            this.testLabel = new System.Windows.Forms.Label();
            this.boldCheckbox = new System.Windows.Forms.CheckBox();
            this.shadowButton = new System.Windows.Forms.Button();
            this.gradientButton = new System.Windows.Forms.Button();
            this.backgroundButton = new System.Windows.Forms.Button();
            this.textButton = new System.Windows.Forms.Button();
            this.backgroundSlider = new System.Windows.Forms.TrackBar();
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.importMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.saveMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.exportMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.resetToDefaultMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.viewAutoBackupsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.expandAllMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.collapseAllMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.usersManualMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuSep = new System.Windows.Forms.ToolStripSeparator();
            this.aboutMenu = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitter)).BeginInit();
            this.splitter.Panel1.SuspendLayout();
            this.splitter.Panel2.SuspendLayout();
            this.splitter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gradientSpinBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shadowSlider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textSlider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backgroundSlider)).BeginInit();
            this.MainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitter
            // 
            this.splitter.BackColor = System.Drawing.SystemColors.Highlight;
            this.splitter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitter.Location = new System.Drawing.Point(0, 24);
            this.splitter.Margin = new System.Windows.Forms.Padding(0);
            this.splitter.Name = "splitter";
            // 
            // splitter.Panel1
            // 
            this.splitter.Panel1.BackColor = System.Drawing.SystemColors.Window;
            this.splitter.Panel1.Controls.Add(this.treeView);
            // 
            // splitter.Panel2
            // 
            this.splitter.Panel2.AutoScroll = true;
            this.splitter.Panel2.BackColor = System.Drawing.SystemColors.Window;
            this.splitter.Panel2.Controls.Add(this.gradientLabel);
            this.splitter.Panel2.Controls.Add(this.gradientSpinBox);
            this.splitter.Panel2.Controls.Add(this.backgroundEqualLabel);
            this.splitter.Panel2.Controls.Add(this.shadowSlider);
            this.splitter.Panel2.Controls.Add(this.textSlider);
            this.splitter.Panel2.Controls.Add(this.testLabel);
            this.splitter.Panel2.Controls.Add(this.boldCheckbox);
            this.splitter.Panel2.Controls.Add(this.shadowButton);
            this.splitter.Panel2.Controls.Add(this.gradientButton);
            this.splitter.Panel2.Controls.Add(this.backgroundButton);
            this.splitter.Panel2.Controls.Add(this.textButton);
            this.splitter.Panel2.Controls.Add(this.backgroundSlider);
            this.splitter.Size = new System.Drawing.Size(459, 466);
            this.splitter.SplitterDistance = 158;
            this.splitter.TabIndex = 0;
            // 
            // treeView
            // 
            this.treeView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.HideSelection = false;
            this.treeView.HotTracking = true;
            this.treeView.Location = new System.Drawing.Point(0, 0);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(158, 466);
            this.treeView.TabIndex = 0;
            this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterSelect);
            this.treeView.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView_NodeMouseClick);
            this.treeView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.treeView_KeyDown);
            // 
            // gradientLabel
            // 
            this.gradientLabel.AutoSize = true;
            this.gradientLabel.Location = new System.Drawing.Point(131, 115);
            this.gradientLabel.Name = "gradientLabel";
            this.gradientLabel.Size = new System.Drawing.Size(55, 14);
            this.gradientLabel.TabIndex = 11;
            this.gradientLabel.Text = "Intensity";
            // 
            // gradientSpinBox
            // 
            this.gradientSpinBox.Location = new System.Drawing.Point(192, 113);
            this.gradientSpinBox.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.gradientSpinBox.Name = "gradientSpinBox";
            this.gradientSpinBox.Size = new System.Drawing.Size(46, 22);
            this.gradientSpinBox.TabIndex = 10;
            this.gradientSpinBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.gradientSpinBox.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.gradientSpinBox.Visible = false;
            this.gradientSpinBox.ValueChanged += new System.EventHandler(this.gradientSpinBox_ValueChanged);
            // 
            // backgroundEqualLabel
            // 
            this.backgroundEqualLabel.AutoSize = true;
            this.backgroundEqualLabel.Location = new System.Drawing.Point(119, 89);
            this.backgroundEqualLabel.Name = "backgroundEqualLabel";
            this.backgroundEqualLabel.Size = new System.Drawing.Size(16, 14);
            this.backgroundEqualLabel.TabIndex = 9;
            this.backgroundEqualLabel.Text = "=";
            this.backgroundEqualLabel.Visible = false;
            // 
            // shadowSlider
            // 
            this.shadowSlider.Location = new System.Drawing.Point(15, 269);
            this.shadowSlider.Margin = new System.Windows.Forms.Padding(0);
            this.shadowSlider.Maximum = 255;
            this.shadowSlider.Name = "shadowSlider";
            this.shadowSlider.Size = new System.Drawing.Size(104, 45);
            this.shadowSlider.TabIndex = 8;
            this.shadowSlider.TickStyle = System.Windows.Forms.TickStyle.None;
            this.shadowSlider.Value = 255;
            this.shadowSlider.Visible = false;
            this.shadowSlider.ValueChanged += new System.EventHandler(this.shadowSlider_ValueChanged);
            // 
            // textSlider
            // 
            this.textSlider.Location = new System.Drawing.Point(15, 191);
            this.textSlider.Margin = new System.Windows.Forms.Padding(0);
            this.textSlider.Maximum = 255;
            this.textSlider.Name = "textSlider";
            this.textSlider.Size = new System.Drawing.Size(104, 45);
            this.textSlider.TabIndex = 8;
            this.textSlider.TickStyle = System.Windows.Forms.TickStyle.None;
            this.textSlider.Value = 255;
            this.textSlider.Visible = false;
            this.textSlider.ValueChanged += new System.EventHandler(this.textSlider_ValueChanged);
            // 
            // testLabel
            // 
            this.testLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testLabel.Location = new System.Drawing.Point(35, 14);
            this.testLabel.Name = "testLabel";
            this.testLabel.Size = new System.Drawing.Size(226, 49);
            this.testLabel.TabIndex = 5;
            this.testLabel.Text = "This is what it will look like";
            this.testLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.testLabel.Visible = false;
            this.testLabel.Paint += new System.Windows.Forms.PaintEventHandler(this.TestLabel_Paint);
            // 
            // boldCheckbox
            // 
            this.boldCheckbox.AutoSize = true;
            this.boldCheckbox.Location = new System.Drawing.Point(134, 166);
            this.boldCheckbox.Name = "boldCheckbox";
            this.boldCheckbox.Size = new System.Drawing.Size(49, 18);
            this.boldCheckbox.TabIndex = 4;
            this.boldCheckbox.Text = "Bold";
            this.boldCheckbox.UseVisualStyleBackColor = true;
            this.boldCheckbox.Visible = false;
            this.boldCheckbox.CheckedChanged += new System.EventHandler(this.boldCheckbox_CheckedChanged);
            // 
            // shadowButton
            // 
            this.shadowButton.BackColor = System.Drawing.SystemColors.Window;
            this.shadowButton.Location = new System.Drawing.Point(15, 239);
            this.shadowButton.Name = "shadowButton";
            this.shadowButton.Size = new System.Drawing.Size(104, 27);
            this.shadowButton.TabIndex = 3;
            this.shadowButton.Text = "Shadow";
            this.shadowButton.UseVisualStyleBackColor = false;
            this.shadowButton.Visible = false;
            this.shadowButton.Click += new System.EventHandler(this.shadowButton_Click);
            // 
            // gradientButton
            // 
            this.gradientButton.BackColor = System.Drawing.SystemColors.Window;
            this.gradientButton.Location = new System.Drawing.Point(134, 83);
            this.gradientButton.Name = "gradientButton";
            this.gradientButton.Size = new System.Drawing.Size(104, 27);
            this.gradientButton.TabIndex = 2;
            this.gradientButton.Text = "Gradient";
            this.gradientButton.UseVisualStyleBackColor = false;
            this.gradientButton.Visible = false;
            this.gradientButton.Click += new System.EventHandler(this.gradientButton_Click);
            // 
            // backgroundButton
            // 
            this.backgroundButton.BackColor = System.Drawing.SystemColors.Window;
            this.backgroundButton.Location = new System.Drawing.Point(15, 83);
            this.backgroundButton.Name = "backgroundButton";
            this.backgroundButton.Size = new System.Drawing.Size(104, 27);
            this.backgroundButton.TabIndex = 1;
            this.backgroundButton.Text = "Background";
            this.backgroundButton.UseVisualStyleBackColor = false;
            this.backgroundButton.Visible = false;
            this.backgroundButton.Click += new System.EventHandler(this.backgroundButton_Click);
            // 
            // textButton
            // 
            this.textButton.BackColor = System.Drawing.SystemColors.Window;
            this.textButton.Location = new System.Drawing.Point(15, 161);
            this.textButton.Name = "textButton";
            this.textButton.Size = new System.Drawing.Size(104, 27);
            this.textButton.TabIndex = 0;
            this.textButton.Text = "Text";
            this.textButton.UseVisualStyleBackColor = false;
            this.textButton.Visible = false;
            this.textButton.Click += new System.EventHandler(this.textButton_Click);
            // 
            // backgroundSlider
            // 
            this.backgroundSlider.Location = new System.Drawing.Point(15, 113);
            this.backgroundSlider.Margin = new System.Windows.Forms.Padding(0);
            this.backgroundSlider.Maximum = 255;
            this.backgroundSlider.Name = "backgroundSlider";
            this.backgroundSlider.Size = new System.Drawing.Size(104, 45);
            this.backgroundSlider.TabIndex = 6;
            this.backgroundSlider.TickStyle = System.Windows.Forms.TickStyle.None;
            this.backgroundSlider.Value = 255;
            this.backgroundSlider.Visible = false;
            this.backgroundSlider.ValueChanged += new System.EventHandler(this.backgroundSlider_ValueChanged);
            // 
            // MainMenu
            // 
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.viewMenu,
            this.helpMenu});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(459, 24);
            this.MainMenu.TabIndex = 1;
            this.MainMenu.Text = "MainMenu";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importMenu,
            this.toolStripSeparator2,
            this.saveMenu,
            this.exportMenu,
            this.toolStripSeparator1,
            this.resetToDefaultMenu,
            this.viewAutoBackupsMenu});
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(37, 20);
            this.fileMenu.Text = "&File";
            // 
            // importMenu
            // 
            this.importMenu.Name = "importMenu";
            this.importMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.importMenu.Size = new System.Drawing.Size(177, 22);
            this.importMenu.Text = "Imp&ort";
            this.importMenu.Click += new System.EventHandler(this.importMenu_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(174, 6);
            // 
            // saveMenu
            // 
            this.saveMenu.Name = "saveMenu";
            this.saveMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveMenu.Size = new System.Drawing.Size(177, 22);
            this.saveMenu.Text = "&Save";
            this.saveMenu.Click += new System.EventHandler(this.saveMenu_Click);
            // 
            // exportMenu
            // 
            this.exportMenu.Name = "exportMenu";
            this.exportMenu.Size = new System.Drawing.Size(177, 22);
            this.exportMenu.Text = "&Export";
            this.exportMenu.Click += new System.EventHandler(this.exportMenu_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(174, 6);
            // 
            // resetToDefaultMenu
            // 
            this.resetToDefaultMenu.Name = "resetToDefaultMenu";
            this.resetToDefaultMenu.Size = new System.Drawing.Size(177, 22);
            this.resetToDefaultMenu.Text = "&Reset to Default";
            this.resetToDefaultMenu.Click += new System.EventHandler(this.resetToDefaultMenu_Click);
            // 
            // viewAutoBackupsMenu
            // 
            this.viewAutoBackupsMenu.Name = "viewAutoBackupsMenu";
            this.viewAutoBackupsMenu.Size = new System.Drawing.Size(177, 22);
            this.viewAutoBackupsMenu.Text = "View Auto-&Backups";
            this.viewAutoBackupsMenu.Click += new System.EventHandler(this.viewAutoBackupsMenu_Click);
            // 
            // viewMenu
            // 
            this.viewMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.expandAllMenu,
            this.collapseAllMenu});
            this.viewMenu.Name = "viewMenu";
            this.viewMenu.Size = new System.Drawing.Size(44, 20);
            this.viewMenu.Text = "&View";
            // 
            // expandAllMenu
            // 
            this.expandAllMenu.Name = "expandAllMenu";
            this.expandAllMenu.Size = new System.Drawing.Size(136, 22);
            this.expandAllMenu.Text = "&Expand All";
            this.expandAllMenu.Click += new System.EventHandler(this.expandAllMenu_Click);
            // 
            // collapseAllMenu
            // 
            this.collapseAllMenu.Name = "collapseAllMenu";
            this.collapseAllMenu.Size = new System.Drawing.Size(136, 22);
            this.collapseAllMenu.Text = "&Collapse All";
            this.collapseAllMenu.Click += new System.EventHandler(this.collapseAllMenu_Click);
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usersManualMenu,
            this.helpMenuSep,
            this.aboutMenu});
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(44, 20);
            this.helpMenu.Text = "&Help";
            // 
            // usersManualMenu
            // 
            this.usersManualMenu.Image = new System.Drawing.Icon(Properties.Resources.UserManualIcon, 16, 16).ToBitmap();
            this.usersManualMenu.Name = "usersManualMenu";
            this.usersManualMenu.Size = new System.Drawing.Size(140, 22);
            this.usersManualMenu.Text = "&User Manual";
            this.usersManualMenu.Click += new System.EventHandler(this.usersManualMenu_Click);
            // 
            // helpMenuSep
            // 
            this.helpMenuSep.Name = "helpMenuSep";
            this.helpMenuSep.Size = new System.Drawing.Size(137, 6);
            // 
            // aboutMenu
            // 
            this.aboutMenu.Image = new System.Drawing.Icon(Properties.Resources.WestTelIcon, 16, 16).ToBitmap();
            this.aboutMenu.Name = "aboutMenu";
            this.aboutMenu.Size = new System.Drawing.Size(140, 22);
            this.aboutMenu.Text = "&About XXX";
            this.aboutMenu.Click += new System.EventHandler(this.aboutMenu_Click);
            // 
            // ColorEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 490);
            this.Controls.Add(this.splitter);
            this.Controls.Add(this.MainMenu);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.Icon = global::WestTel.E911.Properties.Resources.ColorEditorIcon;
            this.KeyPreview = true;
            this.MainMenuStrip = this.MainMenu;
            this.Name = "ColorEditor";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Choose Application Colors";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.ColorEditor_Closing);
            this.Load += new System.EventHandler(this.ColorEditor_Load);
            this.splitter.Panel1.ResumeLayout(false);
            this.splitter.Panel2.ResumeLayout(false);
            this.splitter.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitter)).EndInit();
            this.splitter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gradientSpinBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shadowSlider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textSlider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backgroundSlider)).EndInit();
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.SplitContainer splitter;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.Button textButton;
        private System.Windows.Forms.Button gradientButton;
        private System.Windows.Forms.Button backgroundButton;
        private System.Windows.Forms.CheckBox boldCheckbox;
        private System.Windows.Forms.Button shadowButton;
        private System.Windows.Forms.Label testLabel;
        private System.Windows.Forms.TrackBar textSlider;
        private System.Windows.Forms.TrackBar backgroundSlider;
        private System.Windows.Forms.TrackBar shadowSlider;
        private System.Windows.Forms.Label backgroundEqualLabel;
        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem importMenu;
        private System.Windows.Forms.ToolStripMenuItem viewMenu;
        private System.Windows.Forms.ToolStripMenuItem expandAllMenu;
        private System.Windows.Forms.ToolStripMenuItem collapseAllMenu;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem saveMenu;
        private System.Windows.Forms.ToolStripMenuItem exportMenu;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem resetToDefaultMenu;
        private System.Windows.Forms.ToolStripMenuItem helpMenu;
        private System.Windows.Forms.ToolStripMenuItem usersManualMenu;
        private System.Windows.Forms.ToolStripMenuItem aboutMenu;
        private System.Windows.Forms.ToolStripSeparator helpMenuSep;
        private System.Windows.Forms.ToolStripMenuItem viewAutoBackupsMenu;
        private System.Windows.Forms.Label gradientLabel;
        private System.Windows.Forms.NumericUpDown gradientSpinBox;
    }
}