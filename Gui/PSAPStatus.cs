using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Collections;
using System.Threading;
using System.Windows.Forms;

namespace WestTel.E911
{
	/// <summary>
	/// Summary description for PSAPStatus.
	/// </summary>
	public class PSAPStatus : BaseForm
	{
        private class PSAPGrid : Grid
        {
            public PSAPGrid(): base("PSAPStatus") {}

            override protected bool isEqual(object key1, object key2)
            {
                return Convert.ToString(key1) == Convert.ToString(key2);
            }

            override public void RefreshRowSelection(string context)
            {
                ClearSelection();
                Refresh();
            }

            override protected void OnMouseDown (MouseEventArgs e) {}
            override protected void OnKeyDown   (KeyEventArgs   e) {}
        }

		private bool systemShutdown = false;
        private PSAPGrid gridPSAPStatus = new PSAPGrid();
        private BindingSource gridBindingSource;

		private System.Windows.Forms.PrintDialog printSetup;
		private System.Drawing.Printing.PrintDocument printDoc;

		Brush printerBrush;
		Font printerFont;
		int fontSize = 10;
		float LinePosition = 10;
		float LeftMargin = 30;
		float TopMargin = 50;

        int fullWidth = 640;
        int curWidth  = 640;

        private DataGridViewTextBoxColumn NameCol;
        private DataGridViewTextBoxColumn PositionsOnlineCol;
        private DataGridViewTextBoxColumn PositionsOn911Col;
        private DataGridViewTextBoxColumn PositionsOnAdminCol;
        private TableLayoutPanel tableLayoutPanel1;
        private MenuStrip MainMenu;
        private ToolStrip toolBar;
        private ToolStripButton printButton;
        private ToolStripMenuItem fileMenu;
        private ToolStripMenuItem printMenu;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem closeMenu;
        private ToolStripMenuItem helpMenu;
        private ToolStripMenuItem aboutMenu;
        private ToolStripMenuItem usersManualMenu;
        private ToolStripSeparator helpMenuSep;

        private StatusStrip statusStrip;

		private Controller controller;

        public enum StatusType {Available, Partial, Unavailable};

        public static Settings.ColorInfo availableColorInfo =
            new Settings.ColorInfo(Color.FromArgb(209,255,205), // green
                                   Color.FromArgb(209,255,205), // green
                                   Color.Black, false);

        public static Settings.ColorInfo partialColorInfo =
            new Settings.ColorInfo(Color.FromArgb(255,255,180), // yellow
                                   Color.FromArgb(255,255,180), // yellow
                                   Color.Black, false);

        public static Settings.ColorInfo unavailableColorInfo =
            new Settings.ColorInfo(Color.FromArgb(232,180,180), // red
                                   Color.FromArgb(232,180,180), // red
                                   Color.Black, false);

        public static Settings.ColorInfo availableHoverColorInfo   = availableColorInfo;
        public static Settings.ColorInfo partialHoverColorInfo     = partialColorInfo;
        public static Settings.ColorInfo unavailableHoverColorInfo = unavailableColorInfo;

        public static Settings.ColorInfo backgroundColorInfo =
            new Settings.ColorInfo(Color.FromArgb(unchecked((int)0xFFE0E0E0)), // Grey 300
                                   Color.FromArgb(unchecked((int)0xFFE0E0E0)), // Grey 300 (transparent)
                                   Color.Black, false);

		public PSAPStatus(Controller c)
		{
			controller = c;
			InitializeComponent();
            setTranslatableText();
			CustomInitializeComponent();

            colorsUpdated();

            statusStrip.Visible = false;

            showAdminCol(false);

            // hardcode size (not resizable)
            ClientSize = new Size(curWidth, 105 + (8 * 37)); // 8 rows

DataModel.logLockInfo(true, DataModel.psapStatusEventsSyncRoot);
            lock (DataModel.psapStatusEventsSyncRoot)
            {
                try
                {
                    gridPSAPStatus.setSyncRoot(DataModel.psapStatusEventsSyncRoot);

                    DataTable psapStatusDataTable = controller.dataModel.ds.Tables["PSAPStatusEvents"];

			        if (psapStatusDataTable != null)
			        {
                        gridBindingSource = new BindingSource(controller.dataModel.ds, "PSAPStatusEvents");
                        gridPSAPStatus.DataSource = gridBindingSource;

                        // KLUDGE - 20 Jul 2009 elliott - if this kludge is not here, the Dialog locks up when the scroll bar appears
                        kludgeRowList = new ArrayList();

                        for (int i = 0; i < 10; ++i)
                        {
                            c.dataModel.AddPSAPStatusEvent("Test" + i.ToString(), 0, 0, 0);

                            kludgeRowList.Add(psapStatusDataTable.Rows[psapStatusDataTable.Rows.Count - 1]);
                        }
			        }

                    // this refresh is needed in case the data table is populated before the Dialog is created
                    refreshGrid();
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.psapStatusEventsSyncRoot);
        }

        private ArrayList kludgeRowList = null;

		public void Stop()
		{
			try
			{
				Logging.AppTrace("Stopping PSAP Status Display");
				systemShutdown = true;
				Close();
			}
			catch (Exception ex)
			{
				Logging.AppTrace("Error Stopping PSAP Status Display:" + ex.ToString());
				Logging.ExceptionLogger("Error Stopping PSAP Status Display", ex, Console.LogID.Cleanup);
			}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PSAPStatus));
            this.NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PositionsOnlineCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PositionsOn911Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PositionsOnAdminCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.printSetup = new System.Windows.Forms.PrintDialog();
            this.printDoc = new System.Drawing.Printing.PrintDocument();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.toolBar = new System.Windows.Forms.ToolStrip();
            this.printButton = new System.Windows.Forms.ToolStripButton();
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.printMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.closeMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.usersManualMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuSep = new System.Windows.Forms.ToolStripSeparator();
            this.aboutMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.tableLayoutPanel1.SuspendLayout();
            this.toolBar.SuspendLayout();
            this.MainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // NameCol
            // 
            this.NameCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NameCol.DataPropertyName = "Name";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.NullValue = null;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.NameCol.DefaultCellStyle = dataGridViewCellStyle1;
            this.NameCol.HeaderText = "PSAP";
            this.NameCol.Name = "NameCol";
            this.NameCol.ReadOnly = true;
            this.NameCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.NameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // PositionsOnlineCol
            // 
            this.PositionsOnlineCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.PositionsOnlineCol.DataPropertyName = "PositionsOnline";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PositionsOnlineCol.DefaultCellStyle = dataGridViewCellStyle2;
            this.PositionsOnlineCol.HeaderText = "Positions Active";
            this.PositionsOnlineCol.Name = "PositionsOnlineCol";
            this.PositionsOnlineCol.ReadOnly = true;
            this.PositionsOnlineCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.PositionsOnlineCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.PositionsOnlineCol.Width = 120;
            // 
            // PositionsOn911Col
            // 
            this.PositionsOn911Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.PositionsOn911Col.DataPropertyName = "PositionsOn911";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PositionsOn911Col.DefaultCellStyle = dataGridViewCellStyle2;
            this.PositionsOn911Col.HeaderText = "Positions on 9-1-1 Calls";
            this.PositionsOn911Col.Name = "PositionsOn911Col";
            this.PositionsOn911Col.ReadOnly = true;
            this.PositionsOn911Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.PositionsOn911Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.PositionsOn911Col.Width = 165;
            // 
            // PositionsOnAdminCol
            // 
            this.PositionsOnAdminCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.PositionsOnAdminCol.DataPropertyName = "PositionsOnAdmin";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PositionsOnAdminCol.DefaultCellStyle = dataGridViewCellStyle2;
            this.PositionsOnAdminCol.HeaderText = "Positions on Admin Calls";
            this.PositionsOnAdminCol.Name = "PositionsOnAdminCol";
            this.PositionsOnAdminCol.ReadOnly = true;
            this.PositionsOnAdminCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.PositionsOnAdminCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.PositionsOnAdminCol.Visible = false;
            this.PositionsOnAdminCol.Width = 165;
            // 
            // printSetup
            // 
            this.printSetup.Document = this.printDoc;
            // 
            // printDoc
            // 
            this.printDoc.DocumentName = "PSAPStatus";
            this.printDoc.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDoc_PrintPage);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.toolBar, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.MainMenu, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(486, 153);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // toolBar
            // 
            this.toolBar.AllowMerge = false;
            this.toolBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolBar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolBar.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printButton});
            this.toolBar.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolBar.Location = new System.Drawing.Point(0, 24);
            this.toolBar.Name = "toolBar";
            this.toolBar.Size = new System.Drawing.Size(486, 45);
            this.toolBar.Stretch = true;
            this.toolBar.TabIndex = 2;
            this.toolBar.Text = "Tool Bar";
            // 
            // printButton
            // 
            this.printButton.Enabled = false;
            this.printButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.printButton.Image = Properties.Resources.PrintIcon.ToBitmap();
            this.printButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.printButton.Name = "printButton";
            this.printButton.Padding = new System.Windows.Forms.Padding(2);
            this.printButton.Size = new System.Drawing.Size(36, 42);
            this.printButton.Text = "&Print";
            this.printButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.printButton.Click += new System.EventHandler(this.printMenu_Click);
            // 
            // MainMenu
            // 
            this.MainMenu.AllowMerge = false;
            this.MainMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.helpMenu});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(486, 24);
            this.MainMenu.TabIndex = 1;
            this.MainMenu.Text = "Main Menu";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printMenu,
            this.toolStripSeparator1,
            this.closeMenu});
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(37, 20);
            this.fileMenu.Text = "&File";
            // 
            // printMenu
            // 
            this.printMenu.Enabled = false;
            this.printMenu.Image = new Icon(Properties.Resources.PrintIcon, 16, 16).ToBitmap();
            this.printMenu.Name = "printMenu";
            this.printMenu.Size = new System.Drawing.Size(145, 22);
            this.printMenu.Text = "&Print...";
            this.printMenu.Click += new System.EventHandler(this.printMenu_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(142, 6);
            // 
            // closeMenu
            // 
            this.closeMenu.Name = "closeMenu";
            this.closeMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.closeMenu.Size = new System.Drawing.Size(145, 22);
            this.closeMenu.Text = "&Close";
            this.closeMenu.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usersManualMenu,
            this.helpMenuSep,
            this.aboutMenu});
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(44, 20);
            this.helpMenu.Text = "&Help";
            // 
            // usersManualMenu
            // 
            this.usersManualMenu.Image = new Icon(Properties.Resources.UserManualIcon, 16, 16).ToBitmap();
            this.usersManualMenu.Name = "usersManualMenu";
            this.usersManualMenu.Size = new System.Drawing.Size(140, 22);
            this.usersManualMenu.Text = "&User Manual";
            this.usersManualMenu.Click += new System.EventHandler(this.usersManualMenu_Click);
            // 
            // helpMenuSep
            // 
            this.helpMenuSep.Name = "helpMenuSep";
            this.helpMenuSep.Size = new System.Drawing.Size(137, 6);
            // 
            // aboutMenu
            // 
            this.aboutMenu.Image = new Icon(Properties.Resources.WestTelIcon, 16, 16).ToBitmap();
            this.aboutMenu.Name = "aboutMenu";
            this.aboutMenu.Size = new System.Drawing.Size(140, 22);
            this.aboutMenu.Text = "&About XXX";
            this.aboutMenu.Click += new System.EventHandler(this.aboutMenu_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.BackColor = System.Drawing.SystemColors.Window;
            this.statusStrip.Location = new System.Drawing.Point(0, 153);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(486, 22);
            this.statusStrip.SizingGrip = false;
            this.statusStrip.TabIndex = 6;
            // 
            // PSAPStatus
            // 
            this.ClientSize = new System.Drawing.Size(486, 175);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.statusStrip);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = Properties.Resources.PSAPStatusIcon;
            this.KeyPreview = true;
            this.MainMenuStrip = this.MainMenu;
            this.Name = "PSAPStatus";
            this.Text = "PSAP Status";
            this.Activated += new System.EventHandler(this.PSAPStatus_Activated);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.PSAPStatus_Closing);
            this.Load += new System.EventHandler(this.PSAPStatus_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PSAPStatus_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PSAPStatus_KeyPress);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.toolBar.ResumeLayout(false);
            this.toolBar.PerformLayout();
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
		#endregion

        private void setTranslatableText()
        {
            this.NameCol.HeaderText             = (Translate)("PSAP");
            this.PositionsOnlineCol.HeaderText  = (Translate)("Positions Active");
            this.PositionsOn911Col.HeaderText   = (Translate)("Positions on 9-1-1 Calls");
            this.PositionsOnAdminCol.HeaderText = (Translate)("Positions on Admin Calls");
            this.toolBar.Text                   = (Translate)("Tool Bar");
            this.printButton.Text               = (Translate)("&Print");
            this.MainMenu.Text                  = (Translate)("Main Menu");
            this.fileMenu.Text                  = (Translate)("&File");
            this.printMenu.Text                 = (Translate)("&Print...");
            this.closeMenu.Text                 = (Translate)("&Close");
            this.helpMenu.Text                  = (Translate)("&Help");
            this.usersManualMenu.Text           = (Translate)("&User Manual");
            this.aboutMenu.Text                 = (Translate)("&About XXX");
        }

        private void CustomInitializeComponent()
		{
            this.     BackColor = SystemColors.Window;
            MainMenu. BackColor = SystemColors.Window;
            toolBar.  BackColor = SystemColors.Window;

            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            ((System.ComponentModel.ISupportInitialize)(this.gridPSAPStatus)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();

            // gridPSAPStatus
            this.gridPSAPStatus.AllowUserToAddRows = false;
            this.gridPSAPStatus.AllowUserToDeleteRows = false;
            this.gridPSAPStatus.AllowUserToResizeColumns = false;
            this.gridPSAPStatus.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(247)))), ((int)(((byte)(209)))));
            this.gridPSAPStatus.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.gridPSAPStatus.BackgroundColor = System.Drawing.SystemColors.Window;
            this.gridPSAPStatus.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridPSAPStatus.CausesValidation = false;
            this.gridPSAPStatus.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.gridPSAPStatus.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.gridPSAPStatus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.gridPSAPStatus.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameCol,
            this.PositionsOnlineCol,
            this.PositionsOn911Col,
            this.PositionsOnAdminCol});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Ivory;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridPSAPStatus.DefaultCellStyle = dataGridViewCellStyle5;
            this.gridPSAPStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPSAPStatus.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.gridPSAPStatus.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridPSAPStatus.Location = new System.Drawing.Point(0, 69);
            this.gridPSAPStatus.Margin = new System.Windows.Forms.Padding(0);
            this.gridPSAPStatus.MultiSelect = false;
            this.gridPSAPStatus.Name = "gridPSAPStatus";
            this.gridPSAPStatus.ReadOnly = true;
            this.gridPSAPStatus.RowHeadersVisible = false;
            this.gridPSAPStatus.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridPSAPStatus.RowTemplate.DividerHeight = 1;
            this.gridPSAPStatus.RowTemplate.Height = 37;
            this.gridPSAPStatus.RowTemplate.ReadOnly = true;
            this.gridPSAPStatus.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.gridPSAPStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.gridPSAPStatus.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridPSAPStatus.ShowCellErrors = false;
            this.gridPSAPStatus.ShowEditingIcon = false;
            this.gridPSAPStatus.ShowRowErrors = false;
            this.gridPSAPStatus.TabIndex = 0;
            this.gridPSAPStatus.CellPainting += PaintGridCell;
            this.gridPSAPStatus.RowPrePaint += PaintGridRowBackground;
            this.gridPSAPStatus.RowPostPaint += PaintGridRowSelection;

            this.tableLayoutPanel1.Controls.Add(this.gridPSAPStatus, 0, 2);

            ((System.ComponentModel.ISupportInitialize)(this.gridPSAPStatus)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
        }

        System.Threading.Timer colorsUpdatedTimer = null;

        public void colorsUpdated()
        {
            if ( ! Visible)
            {
                if (colorsUpdatedTimer == null) colorsUpdatedTimer = new System.Threading.Timer(colorsUpdated);
                colorsUpdatedTimer.Change(500, -1);
            }
            else
            {
                colorsUpdated(null);
            }
        }

        private delegate void ColorsUpdatedDelegate();

        private void colorsUpdated(object notused)
        {
            try   { BeginInvoke(new ColorsUpdatedDelegate(ColorsUpdated)); }
            catch { try {ColorsUpdated();} catch{} }
        }

        private void ColorsUpdated()
        {
            try
            {
                if (gridPSAPStatus != null)
                {
                    gridPSAPStatus.BackgroundColor = backgroundColorInfo.backgroundColor1;
                }

                gridPSAPStatus.Refresh();
            }
            catch {}
        }

        private delegate void showAdminColDelegate(bool showCol);

        public void showAdminColumn(bool showCol)
        {
            try   {BeginInvoke(new showAdminColDelegate(showAdminCol), showCol);}
            catch {try {showAdminCol(showCol);} catch {} }
        }

        private void showAdminCol(bool showCol)
        {
            PositionsOnAdminCol.Visible = showCol;

            curWidth = fullWidth - (showCol ? 0 : PositionsOnAdminCol.Width);
        }

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			Close();
		}

        private delegate void refreshDelegate();

        public void refresh()
        {
            try   {BeginInvoke(new refreshDelegate(refreshGrid));}
            catch {try {refreshGrid();} catch {} }
        }

        private void refreshGrid()
        {
DataModel.logLockInfo(true, DataModel.psapStatusEventsSyncRoot);
            lock (DataModel.psapStatusEventsSyncRoot)
            {
                try
                {
                    // set window to a fixed size from a minimum of 1 row to a maximum of 16 rows
                    int nPsapRows = gridPSAPStatus.RowCount;

                    if      (nPsapRows <  1) nPsapRows =  1;
                    else if (nPsapRows > 16) nPsapRows = 16;

                    ClientSize = new Size(curWidth, 105 + (nPsapRows * 37));

                    printMenu.Enabled = printButton.Enabled = (gridPSAPStatus.RowCount > 0);
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.psapStatusEventsSyncRoot);
        }

        private void PSAPStatus_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    e.Handled = true;
                    this.Close();
                    break;
                case Keys.F10:
                    e.Handled = true;
                    break;
            }            
        }

        private void PSAPStatus_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 1) this.Close();
        }

		private void PSAPStatus_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if ( ! systemShutdown)
			{
				e.Cancel = true;
				Hide();
			}
		}

		private void PSAPStatus_Load(object sender, System.EventArgs e)
		{
            // remove unnecessary separators
            removeUnnecessarySeparators(toolBar);

DataModel.logLockInfo(true, DataModel.psapStatusEventsSyncRoot);
            lock (DataModel.psapStatusEventsSyncRoot)
            {
                try
                {
                    Text = Application.ProductName + (Translate)(" - PSAP Status");
			        aboutMenu.Text = aboutMenu.Text.Replace("XXX",Application.ProductName);

                    // KLUDGE
                    DataTable psapStatusDataTable = controller.dataModel.ds.Tables["PSAPStatusEvents"];

                    if (psapStatusDataTable.Rows.Count >= 10)
                    {
                        foreach (DataRow row in kludgeRowList)
                        {
                            try {psapStatusDataTable.Rows.Remove(row);} catch {}
                        }
                    }

                    refreshGrid();
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.psapStatusEventsSyncRoot);
		}

        private Settings.ColorInfo getColorInfo(DataGridViewRow GRow, bool isHovered)
        {
            try
            {
                DataRowView row = (DataRowView)GRow.DataBoundItem;

                UInt32 nOnline  = row["PositionsOnline"].  ToString() == "-" ? 0 : Convert.ToUInt32(row["PositionsOnline"]);
                UInt32 nOn911   = row["PositionsOn911"].   ToString() == "-" ? 0 : Convert.ToUInt32(row["PositionsOn911"]);
                UInt32 nOnAdmin = row["PositionsOnAdmin"]. ToString() == "-" ? 0 : Convert.ToUInt32(row["PositionsOnAdmin"]);

                if (isHovered)
                {
                    if       (nOn911 >= nOnline)             return unavailableHoverColorInfo;
                    else if ((nOn911 + nOnAdmin) >= nOnline) return partialHoverColorInfo;
                    else                                     return availableHoverColorInfo;
                }
                else
                {
                    if       (nOn911 >= nOnline)             return unavailableColorInfo;
                    else if ((nOn911 + nOnAdmin) >= nOnline) return partialColorInfo;
                    else                                     return availableColorInfo;
                }
            }
            catch {}

            return new Settings.ColorInfo(Color.White, Color.White, Color.Black, false);
        }

        private void PaintGridRowBackground(object sender, DataGridViewRowPrePaintEventArgs e)
        {
DataModel.logLockInfo(true, DataModel.psapStatusEventsSyncRoot);
            lock (DataModel.psapStatusEventsSyncRoot)
            {
                try
                {
                    // if row or cell is invalid, return
                    if (sender != null && e.RowIndex >= 0)
                    {
                        DataGridViewRow GRow = gridPSAPStatus.Rows[e.RowIndex];

                        if (GRow != null && GRow.DataBoundItem != null)
                        {
                            Grid grid = (Grid)sender;
                            bool isHovered = grid.isHovered(e.RowIndex);

                            Settings.ColorInfo colorInfo = getColorInfo(GRow, isHovered);

                            grid.paintRowBackground(e, colorInfo, colorInfo, false);
                        }
                    }
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.psapStatusEventsSyncRoot);
        }

        private void PaintGridRowSelection(object sender, DataGridViewRowPostPaintEventArgs e)
        {
        }

        private void PaintGridCell(object sender, DataGridViewCellPaintingEventArgs e)
        {
DataModel.logLockInfo(true, DataModel.psapStatusEventsSyncRoot);
            lock (DataModel.psapStatusEventsSyncRoot)
            {
                try
                {
                    // if row and cell is valid
                    if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
                    {
                        e.Handled = true;

                        DataGridViewRow GRow = gridPSAPStatus.Rows[e.RowIndex];

                        if (GRow != null && GRow.DataBoundItem != null)
                        {
                            bool isHovered = gridPSAPStatus.isHovered(e.RowIndex);

                            Settings.ColorInfo colorInfo = getColorInfo(GRow, isHovered);

                            Font font = new Font(e.CellStyle.Font, colorInfo.bold ? FontStyle.Bold : FontStyle.Regular);

                            string valueStr = e.Value.ToString();

                            Brush brush = new SolidBrush((valueStr == "-" || valueStr == "0")
                                ? Color.FromArgb(100, colorInfo.foregroundColor)
                                : colorInfo.foregroundColor);

                            StringFormat stringFormat  = new StringFormat();
                            stringFormat.Alignment     = StringAlignment.Center;
                            stringFormat.LineAlignment = StringAlignment.Center;

                            Rectangle bounds = e.CellBounds;

                            if (e.ColumnIndex == 0)
                            {
                                stringFormat.Alignment = StringAlignment.Near;

                                bounds.X += 3;
                            }

                            e.Graphics.DrawString(e.FormattedValue.ToString(), font, brush, bounds, stringFormat);
                        }
                    }
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.psapStatusEventsSyncRoot);
        }

		private void aboutMenu_Click(object sender, EventArgs e)
		{
			Splash.ShowAbout(controller.Position);
		}

		private void usersManualMenu_Click(object sender, EventArgs e)
		{
			Console.ShowUsersManual();
		}

        private void printMenu_Click(object sender, EventArgs e)
		{
			if (printSetup.ShowDialog() == DialogResult.OK)
				printDoc.Print();
		}

		private void PrintHeader(Graphics graphic)
		{
			LinePosition = 10;
			printerFont = new Font("Lucida Console", fontSize );
			printerBrush = new SolidBrush(Color.Black);
			LinePosition = TopMargin;

			printerFont = new Font(printerFont,FontStyle.Bold);
			WriteToPrinter(graphic, "PSAP Status");
			printerFont = new Font(printerFont,FontStyle.Regular);

			graphic.DrawLine(Pens.Black,new PointF(LeftMargin,LinePosition),new PointF(printDoc.DefaultPageSettings.PaperSize.Width - LeftMargin,LinePosition));
			LinePosition += 20;
		}

		private void printDoc_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
		{
			printerFont = new Font("Lucida Console", fontSize );
			// force page header print
			LinePosition = printSetup.PrinterSettings.DefaultPageSettings.PaperSize.Height + 10;
			printerFont = new Font(printerFont,FontStyle.Regular);

DataModel.logLockInfo(true, DataModel.psapStatusEventsSyncRoot);
            lock (DataModel.psapStatusEventsSyncRoot)
            {
                try
                {
                    // TODO printing: test printing
                    WriteToPrinter(e.Graphics, NameCol.             HeaderText. PadRight(25) + " " +
                                               PositionsOnlineCol.  HeaderText. PadRight(25) + " " +
                                               PositionsOn911Col.   HeaderText. PadRight(25) + " " +
                                               PositionsOnAdminCol. HeaderText);

                    foreach (DataGridViewRow GRow in gridPSAPStatus.Rows)
                    {
                        if ((GRow != null) && (GRow.DataBoundItem != null))
                        {
                            DataRowView row = (DataRowView) GRow.DataBoundItem;

                            if (row != null)
                            {
                                WriteToPrinter(e.Graphics,
                                    row["Name"].             ToString().PadRight(25) + " " +
                                    row["PositionsOnline"].  ToString().PadRight(25) + " " +
                                    row["PositionsOn911"].   ToString().PadRight(25) + " " +
                                    row["PositionsOnAdmin"]. ToString());
                            }
                        }
                    }
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.psapStatusEventsSyncRoot);
		}

		public void WriteToPrinter(Graphics graphic, string Text)
		{
			if (LinePosition > printSetup.PrinterSettings.DefaultPageSettings.PaperSize.Height)	PrintHeader(graphic);

			SizeF lineSize = graphic.MeasureString(Text, printerFont, new SizeF((float) printDoc.DefaultPageSettings.PaperSize.Width - LeftMargin, 99));
			graphic.DrawString(Text, printerFont, printerBrush, new RectangleF((float) LeftMargin, LinePosition, printDoc.DefaultPageSettings.PaperSize.Width - LeftMargin, LinePosition + 5));
			LinePosition += lineSize.Height + 5;
		}

		private void PSAPStatus_Activated(object sender, System.EventArgs e)
		{
DataModel.logLockInfo(true, DataModel.psapStatusEventsSyncRoot);
            lock (DataModel.psapStatusEventsSyncRoot)
            {
                try
                {
                    gridPSAPStatus.Focus();
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.psapStatusEventsSyncRoot);

            System.Windows.Forms.Application.DoEvents();

DataModel.logLockInfo(true, DataModel.psapStatusEventsSyncRoot);
            lock (DataModel.psapStatusEventsSyncRoot)
            {
                try
                {
                    gridPSAPStatus.Focus();
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.psapStatusEventsSyncRoot);
        }
	}
}
