﻿using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace WestTel.E911
{
    public class Translate
    {
        private static Dictionary<string, string> translationDictionary = new Dictionary<string,string>();

        public static void loadTranslationFile(string filename)
        {
            string[] translationLines = File.ReadAllLines(filename);

            foreach (string line in translationLines)
            {
                string[] pair = line.Split(",".ToCharArray());

                if (pair.Length >= 2) translationDictionary.Add(pair[0], pair[1]);
                else                  translationDictionary.Add(pair[0], "");
            }
        }

        // ignore these characters at the beginning or end
        private static char[] NON_TRANSLATABLE_CHARS = "&?!. -+*/|#=:%\r\n<>[]{}()\"'".ToCharArray();

        private string translate()
        {
            if (translationDictionary.Count() == 0) return origStr;

            int origStrLength = origStr.Length;

            // handle 'XXX' or 'XXXX'
            bool endsWithXXX = origStr.EndsWith("XXX");
            if (endsWithXXX)   origStr = origStr.TrimEnd("X".ToCharArray());

            string noPreChars = origStr.TrimStart(NON_TRANSLATABLE_CHARS);
            string preChars   = origStr.Substring(0, origStr.Length - noPreChars.Length);

            string noPostChars = noPreChars.TrimEnd(NON_TRANSLATABLE_CHARS);
            string postChars   = noPreChars.Substring(noPostChars.Length);

            if (endsWithXXX) for(int i = 0; i != origStrLength - origStr.Length; ++i) postChars += 'X';

            bool hasAmpersand   = noPostChars.Contains('&');
            string translateStr = noPostChars.Replace("&", "");

            // TODO translate: further split strings using NON_TRANSLATABLE_CHARS, and translate each string separately?

            // translate the string
            string translatedStr;
            bool found = translationDictionary.TryGetValue(translateStr, out translatedStr);

            if ( ! found)
            {
                // TODO translate: warning, unexpected string not found in translation file
                Controller.sendAlertToController(
                    "Translation found unexpected text: \"" + translateStr + "\"",
                    Console.LogType.Info, Console.LogID.Gui);

                return origStr;
            }

            translatedStr = translatedStr.Trim();

            if (translatedStr.Length == 0 || translateStr == translatedStr) return origStr;

            if (hasAmpersand) return preChars + '&' + translatedStr + postChars;
            else              return preChars +       translatedStr + postChars;
        }

        // support cast syntax like this (Translate)"Some Text"
        public static implicit operator Translate(string str) {return new Translate(str);}
        public static implicit operator string(Translate t)   {return t.translate();}

        public override string ToString() {return translate();}

        private Translate(string str) {origStr = str;}
        private string origStr;
    }
}
