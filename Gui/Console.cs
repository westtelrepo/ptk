#define WideSubnet 

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;


// TODO gui: make the GUI start even if it can�t find the Controller on startup

// TODO statusbar:
//i.  In the status bar display the name and phone number of the item highlighted.  Example: �Tow Trucks � ABC Towing: (303) 818-3060
//    What I�m thinking is we put it in the Controller config file formatted so it will visually display correctly but the controller will strip out the formatting before dialing the number

// TODO TDD/SMS:
//i.   Merge TDD and SMS
//ii.  Ditch the TDD icon(s) and use the SMS icons for both SMS and TDD
//iii. I�m thinking we need to add a switch in the Controller to change the applet title bar, menus, etc. to say �SMS/TDD�  (the default will be just �TDD�)
//iv.  Remove the TDD incoming text window were the characters are displayed one at a time.
//v.   In the status bar display something like the following to identify the type of call and when the caller is typing:
//     �TDD Call: Caller is typing��,  �SMS Message: Incoming text��  
//vi.  Add a better separator line between the send and conversation area
//vii. We are going to need to add messages for both SMS and TDD and display the appropriate message list depending if TDD or SMS �call�.  For now until Bob can change the controller just use the TDD messages

// TODO statusbar:
//i.   add the alert count in the status bar?

// Note: use this regexp -> (            this\..+\.)Name = \"(.+)\";
//       replace with    -> $1Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);\n$1Name = "$2";\n$1Padding = new System.Windows.Forms.Padding(2);


namespace WestTel.E911
{
    public class Console : BaseForm
    {
        private static bool   alreadyCheckedDemoMode = false;
        private static string demoModeStr            = "false";
        private static bool   inDemoMode             = false;

        private string sessionUsername = Environment.UserName;

        private int numberOfPositions = 2;

        private System.Threading.Timer timeTimer;
        private System.Threading.Timer heartbeatTimer;
        private System.Threading.Timer msgTimer;
        private System.Threading.Timer tcpCloseTimer;
        private System.Threading.Timer tcpReconnectTimer;
        private System.Threading.Timer positionToolTipTimer;
        private int                    heartbeatTimeout = 2000; // make it short to help initial connection

        enum HoldFlashState {NORMAL, SUBDUED};
        private HoldFlashState holdFlashState = HoldFlashState.NORMAL;

        private int       gridToolTipColumnIndex = -1;
        private int       gridToolTipRowIndex    = -1;
        private ArrayList conferenceLegend;
        private float     conferenceLegendNumberColRatio = 0.5f;

        public DataModel dataModel = null;

        private Label       speakerLabel     = null;
        private TrackBar    speakerSlider    = null;
        private CheckBox    speakerCheckBox  = null;
        private ProgressBar speakerVolumeBar = null;
        private Label       micLabel         = null;
        private TrackBar    micSlider        = null;
        private CheckBox    micCheckBox      = null;
        private ProgressBar micVolumeBar     = null;
        private int CurrentPosition = 1;

        private Dictionary<string, ToolStrip> speedDialToolBarDictionary =
            new Dictionary<string, ToolStrip>();

        public int getCurrentPosition() {return CurrentPosition;}

        private bool waitingToDisconnect = false;

        int maxNumberOfRows        = 200;
        int maxAbandonedCalls      = 50;
        int maxRecordAgeOutSeconds = 86400;

        Brush printerBrush;
        Font  printerFont;
        int   fontSize     = 10;
        float LinePosition = 10;
        float LeftMargin   = 30;
        float TopMargin    = 50;

        public enum CallState
        {
            Undefined        = 0,
            Ringing          = 1,
            Connected        = 2,
            Disconnected     = 3,
            Abandoned        = 4,
            ManualBid        = 5,
            Dialing          = 6,
            OnHold           = 7,
            AbandonedCleared = 8,
            Canceled         = 9
        }

        // Line
        private string LineFilter = "ShowInLineView AND LineNumber <> 0";

        // Connected
        private string ConnectedFilter = "CallStateCode = " + CallState.Connected. GetHashCode().ToString() +
                                     " OR CallStateCode = " + CallState.OnHold.    GetHashCode().ToString();

        // Abandoned and no positions
        private string AbandonedFilter = "CallStateCode = " + CallState.Abandoned.GetHashCode().ToString() +
                                         " AND PositionsHistory NOT LIKE '*P*'";

        // All (except for blank rows used for the Line View)
        private string AllFilter = "Call_ID > 100 OR Call_ID < 1";

        // Position
        private string PositionFilter = "(PositionsHistory LIKE '*\tP1\t*') OR " +
                                        "(PositionsHistory LIKE '\tP1\t*') OR " +
                                        "(PositionsHistory LIKE '*\tP1\t')";

        private System.Drawing.Printing.PrintDocument printDoc;
        private System.Windows.Forms.PrintDialog printSetup;
        private Controller controller = null;
        private IContainer components;
        private ToolTip toolTip;
        private ToolStripContainer toolStripContainer;
        private TableLayoutPanel tableLayoutPanel1;
        private ToolStrip toolBar;
        private Label line;
        private ToolStripButton printButton;
        private ToolStripButton rebidALIButton;
        private ToolStripButton mapALIButton;
        private ToolStripButton alertsButton;
        private ToolStripButton sendToCADButton;
        private ToolStripButton sendEraseToCADButton;
        private ToolStripButton phonebookButton;
        private ToolStripButton psapButton;
        private ToolStripButton tddButton;
        private ToolStripButton smsButton;
        private ToolStripButton irrButton;
        private ToolStripButton abandonedButton;
        private ToolStripButton callbackButton;
        private ToolStripButton connectButton;
        private ToolStripButton disconnectButton;
        private ToolStripButton bargeButton;
        private ToolStripButton flashHookButton;
        private ToolStripButton holdButton;
        private ToolStripButton muteButton;
        private ToolStripDropDownButton volumeButton;
        private MenuStrip MainMenu;
        private ToolStripMenuItem fileMenu;
        private ToolStripMenuItem printMenu;
        private ToolStripMenuItem resetMenu;
        private ToolStripMenuItem debugScriptMenu;
        private ToolStripSeparator fileMenuSep1;
        private ToolStripSeparator fileMenuSep2;
        private ToolStripMenuItem closeMenu;
        private ToolStripMenuItem helpMenu;
        private ToolStripMenuItem aboutMenu;
        private ToolStripMenuItem usersManualMenu;
        private ToolStripSeparator helpMenuSep;
        private ToolStripMenuItem alertMsgMenu;
        public ConsoleGrid LineGrid;      // calls by line including ringing (all positions)
        public ConsoleGrid ConnectedGrid; // calls currently connected       (all positions)
        public ConsoleGrid AbandonedGrid; // calls abandoned including ones taken control of
        public ConsoleGrid PositionGrid;  // calls the current position has answered or joined
        public ConsoleGrid AllGrid;       // calls by time including ringing (all positions)
        private ToolStripMenuItem aliMenu;
        private ToolStripMenuItem callMenu;
        private ToolStripMenuItem toolsMenu;
        private ToolStripMenuItem tddMenu;
        private ToolStripMenuItem smsMenu;
        private ToolStripMenuItem irrMenu;
        private ToolStripSeparator toolsMenuSep;
        private ToolStripMenuItem colorEditorMenu;
        private ToolStripMenuItem psapStatusMenu;
        private ToolStripMenuItem phonebookMenu;
        private ToolStripMenuItem alertsMenu;
        private ToolStripMenuItem rebidALIMenu;
        private ToolStripSeparator aliMenuSep1;
        private ToolStripSeparator aliMenuSep2;
        private ToolStripMenuItem mapALIMenu;
        private ToolStripMenuItem manualBidMenu;
        private ToolStripMenuItem sendToCADMenu;
        private ToolStripMenuItem sendEraseToCADMenu;
        private ToolStripMenuItem abandonedMenu;
        private ToolStripMenuItem callbackMenu;
        private ToolStripMenuItem connectMenu;
        private ToolStripMenuItem disconnectMenu;
        private ToolStripMenuItem participantMenu;
        private ToolStripMenuItem bargeMenu;
        private ToolStripMenuItem holdMenu;
        private ToolStripMenuItem muteMenu;
        private ToolStripMenuItem volumeMenu;
        private ToolStripMenuItem radioStateRadioMenu;
        private ToolStripMenuItem radioStateTelephoneMenu;
        private ToolStripMenuItem forceDisconnectMenu;
        private ToolStripMenuItem flashHookMenu;
        private TabControl tabWidget;
        private TabPage lineTab;
        private TabPage connectedTab;
        private TabPage abandonedTab;
        private TabPage positionTab;
        private TabPage allTab;
        private TabPage onStartupTab    = null;
        private TabPage onDisconnectTab = null;
        private TabPage onConnectTab    = null;
        private bool    onConnectTabIfAbandoned = true;

        private class GradientTextBox : TextBox
        {
            public bool subdued = false;

            public GradientTextBox()
            {
                SetStyle(ControlStyles.UserPaint, true);
            }

            public override Rectangle DisplayRectangle
            {
                get
                {
                    Rectangle rect = base.DisplayRectangle;

                    // margin
                    int margin      = 12;
                    int smallMargin = 5;

                    rect.X      += margin;
                    rect.Y      += margin;
                    rect.Width  -= margin + smallMargin;
                    rect.Height -= margin + smallMargin;

                    return rect;
                }
            }

            protected override void OnPaintBackground(PaintEventArgs e)
            {
                e.Graphics.FillRectangle(Brushes.Black, ClientRectangle);

                Settings.ColorInfo colorInfo = subdued ? aliColorInfoPending : aliColorInfo;

                e.Graphics.FillRectangle(colorInfo.getBackgroundBrush(
                    ClientRectangle, LinearGradientMode.Vertical), ClientRectangle);

                // TODO gui: draw shadow at the top of the ALI display?
                //Rectangle shadowRect = ClientRectangle;
                //shadowRect.Height    = 16;

                //int gray = 205;

                //e.Graphics.FillRectangle(new LinearGradientBrush(
                //    shadowRect, Color.FromArgb(gray,gray,gray), Color.FromArgb(0,gray,gray,gray), LinearGradientMode.Vertical), shadowRect);
            }

            protected override void OnPaint(PaintEventArgs e)
            {
                e.Graphics.DrawString(Text, Font, new SolidBrush(ForeColor), DisplayRectangle);
            }
        }

        private GradientTextBox RawDisplay;

        private CodeProject.SystemHotkey.SystemHotkey ConsoleHotKey    = new CodeProject.SystemHotkey.SystemHotkey();
		private CodeProject.SystemHotkey.SystemHotkey TDDHotKey        = new CodeProject.SystemHotkey.SystemHotkey();
		private CodeProject.SystemHotkey.SystemHotkey SMSHotKey        = new CodeProject.SystemHotkey.SystemHotkey();
		private CodeProject.SystemHotkey.SystemHotkey IRRHotKey        = new CodeProject.SystemHotkey.SystemHotkey();
		private CodeProject.SystemHotkey.SystemHotkey PSAPStatusHotKey = new CodeProject.SystemHotkey.SystemHotkey();
		private CodeProject.SystemHotkey.SystemHotkey TransferHotKey   = new CodeProject.SystemHotkey.SystemHotkey();
		private CodeProject.SystemHotkey.SystemHotkey AlertsHotKey     = new CodeProject.SystemHotkey.SystemHotkey();
		private CodeProject.SystemHotkey.SystemHotkey AbandonHotKey    = new CodeProject.SystemHotkey.SystemHotkey();
		private CodeProject.SystemHotkey.SystemHotkey RebidHotKey      = new CodeProject.SystemHotkey.SystemHotkey();
        private CodeProject.SystemHotkey.SystemHotkey CallbackHotKey   = new CodeProject.SystemHotkey.SystemHotkey();
        private CodeProject.SystemHotkey.SystemHotkey ConnectHotKey    = new CodeProject.SystemHotkey.SystemHotkey();
        private CodeProject.SystemHotkey.SystemHotkey BargeHotKey      = new CodeProject.SystemHotkey.SystemHotkey();
        private CodeProject.SystemHotkey.SystemHotkey FlashHookHotKey  = new CodeProject.SystemHotkey.SystemHotkey();
        private CodeProject.SystemHotkey.SystemHotkey HoldHotKey       = new CodeProject.SystemHotkey.SystemHotkey();
        private CodeProject.SystemHotkey.SystemHotkey DisconnectHotKey = new CodeProject.SystemHotkey.SystemHotkey();

        System.Media.SoundPlayer ringing911Player   = null;
        System.Media.SoundPlayer ringingAdminPlayer = null;

        bool ringing911Call   = false;
        bool ringingAdminCall = false;

        private delegate void SetButtonEnabledDelegate(ToolStripButton button, bool Enabled);

        public Console(Controller c, string _demoMode, bool debugMode, bool startRecordingDebugScript)
        {
            demoModeStr = _demoMode;
            inDemoMode  = (demoModeStr.ToLower() != "false");

            controller = c;

            InitializeComponent();
            setTranslatableText();

            // the designer does not like the way we inherit Grids
            CustomInitializeComponent();

            colorsUpdated();

            debugScriptMenu.Visible = debugMode;

            if (startRecordingDebugScript) debugScriptMenu_Click(this, null);

            tabWidget.SelectedTab = onStartupTab;

            // hardcode window size to handle at lease 6 rows
            ClientSize  = new Size(1024, 144 + 6 * 37);
            MinimumSize = SizeFromClientSize(ClientSize);
            MaximumSize = SystemInformation.VirtualScreen.Size;

            // set NumberOfPositions so the menu gets initialized
            setNumberOfPositions(numberOfPositions);

            // look for ringing sound files
            try {ringing911Player   = new System.Media.SoundPlayer("911.wav");   } catch {}
            try {ringingAdminPlayer = new System.Media.SoundPlayer("admin.wav"); } catch {}

            // volume menu
            speakerLabel = new Label();
            speakerLabel.Image = Properties.Resources.ConsoleSpeakerBitmap;
            speakerLabel.Size = speakerLabel.Image.Size;
            speakerLabel.Dock = DockStyle.Fill;

            speakerSlider = new TrackBar();
            speakerSlider.AutoSize = true;
            speakerSlider.Minimum = 0;
            speakerSlider.Maximum = 100;
            speakerSlider.TickFrequency = 10;
            speakerSlider.TickStyle = TickStyle.TopLeft;
            speakerSlider.Maximum = 100;
// TODO SIPVoipSDK:            speakerSlider.ValueChanged += speakerSlider_ValueChanged;
            speakerSlider.Dock = DockStyle.Fill;

            speakerCheckBox = new CheckBox();
            speakerCheckBox.AutoSize = true;
            speakerCheckBox.Dock = DockStyle.Fill;
            speakerCheckBox.Checked = true;
// TODO SIPVoipSDK:            speakerCheckBox.CheckedChanged += speakerCheckBox_Changed;

            micLabel = new Label();
            micLabel.Image = Properties.Resources.ConsoleMicBitmap;
            micLabel.Size = speakerLabel.Image.Size;
            micLabel.Dock = DockStyle.Fill;

            micSlider = new TrackBar();
            micSlider.AutoSize = true;
            micSlider.Minimum = 0;
            micSlider.Maximum = 100;
            micSlider.TickFrequency = 10;
            micSlider.TickStyle = TickStyle.TopLeft;
            micSlider.Maximum = 100;
// TODO SIPVoipSDK:            micSlider.ValueChanged += micSlider_ValueChanged;
            micSlider.Dock = DockStyle.Fill;

            micCheckBox = new CheckBox();
            micCheckBox.AutoSize = true;
            micCheckBox.Dock = DockStyle.Fill;
            micCheckBox.Checked = true;
// TODO SIPVoipSDK:            micCheckBox.CheckedChanged += micCheckBox_Changed;

            TableLayoutPanel volumePanel = new TableLayoutPanel();
            volumePanel.ColumnCount = 3;
            volumePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.AutoSize));
            volumePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            volumePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.AutoSize));
            volumePanel.Controls.Add(speakerLabel, 0, 0);
            volumePanel.Controls.Add(speakerSlider, 1, 0);
            volumePanel.Controls.Add(speakerCheckBox, 2, 0);
            volumePanel.Controls.Add(micLabel, 0, 1);
            volumePanel.Controls.Add(micSlider, 1, 1);
            volumePanel.Controls.Add(micCheckBox, 2, 1);
            volumePanel.RowCount = 2;
            volumePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            volumePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            volumePanel.AutoSize = true;
            volumePanel.Dock = DockStyle.Fill;

            ToolStripDropDown    volumeDropDown     = new ToolStripDropDown();
            ToolStripControlHost volumeMenuControls = new ToolStripControlHost(volumePanel);

            volumeDropDown.Items.Add(volumeMenuControls);

            volumeMenu.  DropDown = volumeDropDown;
            volumeButton.DropDown = volumeDropDown;

            toolTip.SetToolTip(speakerLabel, (Translate)("Speaker"));
            toolTip.SetToolTip(micLabel,     (Translate)("Microphone"));

            toolTip.SetToolTip(speakerCheckBox, (Translate)("Uncheck to mute speaker"));
            toolTip.SetToolTip(micCheckBox,     (Translate)("Uncheck to mute microphone"));

            // volume bars
            speakerVolumeBar = new ProgressBar();
            speakerVolumeBar.Style   = ProgressBarStyle.Continuous;
            speakerVolumeBar.Maximum = 1000;
            speakerVolumeBar.Size = new Size(60, 12);
            speakerVolumeBar.Step = 1;

            micVolumeBar = new ProgressBar();
            micVolumeBar.Style   = ProgressBarStyle.Continuous;
            micVolumeBar.Maximum = 1000;
            micVolumeBar.Size = new Size(60, 12);
            micVolumeBar.Step = 1;

            ToolStripControlHost speakerVolumeBarHost = new ToolStripControlHost(speakerVolumeBar);
            ToolStripControlHost micVolumeBarHost     = new ToolStripControlHost(micVolumeBar);
            speakerVolumeBarHost.AutoSize = false;
            micVolumeBarHost.AutoSize = false;

            toolBar.Items.Add(speakerVolumeBarHost);
            toolBar.Items.Add(micVolumeBarHost);

            speakerVolumeBar. Visible = false;
            micVolumeBar.     Visible = false;

            toolTip.SetToolTip(speakerVolumeBar, (Translate)("Speaker Activity"));
            toolTip.SetToolTip(micVolumeBar,     (Translate)("Microphone Activity"));

            // setup timers
            heartbeatTimer = new System.Threading.Timer(SendHeartbeat,   null, 1000,  Timeout.Infinite);
            msgTimer       = new System.Threading.Timer(MsgTimerExpired, null, 20000, Timeout.Infinite);

            tcpCloseTimer     = new System.Threading.Timer(TCPCloseTimerExpired,     null, Timeout.Infinite, Timeout.Infinite);
            tcpReconnectTimer = new System.Threading.Timer(TCPReconnectTimerExpired, null, Timeout.Infinite, Timeout.Infinite);

            // Hot keys
            ConsoleHotKey.    ShortcutKey = Keys.Control | Keys.Shift | Keys.F5;
			TDDHotKey.        ShortcutKey = Keys.Control | Keys.Shift | Keys.F6;
			TransferHotKey.   ShortcutKey = Keys.Control | Keys.Shift | Keys.F7;
			AlertsHotKey.     ShortcutKey = Keys.Control | Keys.Shift | Keys.F8;
            PSAPStatusHotKey. ShortcutKey = Keys.Control | Keys.Shift | Keys.F9;
            SMSHotKey.        ShortcutKey = Keys.Control | Keys.Shift | Keys.F10;
            IRRHotKey.        ShortcutKey = Keys.Control | Keys.Shift | Keys.I;
			AbandonHotKey.    ShortcutKey = Keys.Control | Keys.Shift | Keys.D;
			RebidHotKey.      ShortcutKey = Keys.Control | Keys.Shift | Keys.R;
            CallbackHotKey.   ShortcutKey = Keys.Control | Keys.Shift | Keys.C;
            ConnectHotKey.    ShortcutKey = Keys.Control | Keys.Shift | Keys.A;
            BargeHotKey.      ShortcutKey = Keys.Control | Keys.Shift | Keys.B;
            FlashHookHotKey.  ShortcutKey = Keys.Control | Keys.Shift | Keys.F;
            HoldHotKey.       ShortcutKey = Keys.Control | Keys.Shift | Keys.H;
            DisconnectHotKey. ShortcutKey = Keys.Control | Keys.Shift | Keys.G;

            ConsoleHotKey.    Pressed += ConsoleHotKey_Pressed;
			TDDHotKey.        Pressed += btnTDDClick;
            SMSHotKey.        Pressed += btnSMSClick;
            IRRHotKey.        Pressed += irrButtonClick;
            PSAPStatusHotKey. Pressed += btnPSAPStatusClick;
			TransferHotKey.   Pressed += btnPhonebookClick;
			AlertsHotKey.     Pressed += btnAlertsClick;
			AbandonHotKey.    Pressed += abandonedButtonClick;
			RebidHotKey.      Pressed += btnReBidClick;
            CallbackHotKey.   Pressed += callbackButtonClick;
            ConnectHotKey.    Pressed += connectButtonClick;
            BargeHotKey.      Pressed += bargeButtonClick;
            FlashHookHotKey.  Pressed += flashHookButtonClick;
            HoldHotKey.       Pressed += holdCheckedChanged;
            DisconnectHotKey. Pressed += disconnectButtonClick;

            try {Controller.DebugWindowMsg("  Session User: " + sessionUsername);} catch {}

            registerForSessionChangeEvents();
        }

        private void ConsoleHotKey_Pressed(object sender, System.EventArgs e) {controller.ShowConsole(true);}

        private enum TabStartupBehavior
        {
            All               = 0,  // 000 000 000 000 000
            Connected         = 1,  // 000 000 000 000 001
            Line              = 2,  // 000 000 000 000 010
            Position          = 3,  // 000 000 000 000 011
            Abandoned         = 4,  // 000 000 000 000 100
            //Reserved        = 5,  // 000 000 000 000 101
            //Reserved        = 6,  // 000 000 000 000 110
            //Reserved        = 7,  // 000 000 000 000 111
            Mask              = 7   // 000 000 000 000 111
        }

        private enum TabOnDisconnectBehavior
        {
            DoNothing         = 0,  // 000 000 000 000 000
            All               = 8,  // 000 000 000 001 000
            Connected         = 16, // 000 000 000 010 000
            Line              = 24, // 000 000 000 011 000
            Position          = 32, // 000 000 000 100 000
            Abandoned         = 40, // 000 000 000 100 000
            // Reserved       = 48, // 000 000 000 101 000
            // Reserved       = 56, // 000 000 000 110 000
            Mask              = 56  // 000 000 000 111 000
        }

        private enum TabOnConnectBehavior
        {
            All                    = 0,   // 000 000 000 000 000
            Connected              = 64,  // 000 000 001 000 000
            Line                   = 128, // 000 000 010 000 000
            Position               = 192, // 000 000 011 000 000
            All_If_Abandoned       = 256, // 000 000 100 000 000
            Connected_If_Abandoned = 320, // 000 000 101 000 000
            Line_If_Abandoned      = 384, // 000 000 110 000 000
            Position_If_Abandoned  = 448, // 000 000 111 000 000
            Mask                   = 448  // 000 000 111 000 000
        };

        // TODO*: implement TabOnRingBehavior
        private enum TabOnRingBehavior // this behavior only applies if not on another call
        {
            DoNothing              = 0,    // 000 000 000 000 000
            All                    = 512,  // 000 001 000 000 000
            Line                   = 1024, // 000 010 000 000 000
            Position               = 1536, // 000 011 000 000 000
            //Reserved             = 2048, // 000 100 000 000 000
            //Reserved             = 2560, // 000 101 000 000 000
            //Reserved             = 3072, // 000 110 000 000 000
            //Reserved             = 3584, // 000 111 000 000 000
            Mask                   = 3584  // 000 111 000 000 000
        };

        // TODO*: implement TabOnTakeControlBehavior
        private enum TabOnTakeControlBehavior
        {
            DoNothing              = 0,     // 000 000 000 000 000
            All                    = 4096,  // 001 000 000 000 000
            Position               = 8192,  // 010 000 000 000 000
            //Reserved             = 12288, // 011 000 000 000 000
            //Reserved             = 16384, // 100 000 000 000 000
            //Reserved             = 20480, // 101 000 000 000 000
            //Reserved             = 24576, // 110 000 000 000 000
            //Reserved             = 28672, // 111 000 000 000 000
            Mask                   = 28672  // 111 000 000 000 000
        };

        System.Threading.Timer colorsUpdatedTimer = null;

        public void colorsUpdated()
        {
            if ( ! Visible)
            {
                if (colorsUpdatedTimer == null) colorsUpdatedTimer = new System.Threading.Timer(colorsUpdated);
                colorsUpdatedTimer.Change(500, -1);
            }
            else
            {
                colorsUpdated(null);
            }
        }

        private delegate void ColorsUpdatedDelegate();

        private void colorsUpdated(object notused)
        {
            try   { BeginInvoke(new ColorsUpdatedDelegate(ColorsUpdated)); }
            catch { try {ColorsUpdated();} catch{} }
        }

        private void ColorsUpdated()
        {
            try
            {
                if (LineGrid      != null) LineGrid.      BackgroundColor = gridBackgroundColorInfo.backgroundColor1;
                if (ConnectedGrid != null) ConnectedGrid. BackgroundColor = gridBackgroundColorInfo.backgroundColor1;
                if (AbandonedGrid != null) AbandonedGrid. BackgroundColor = gridBackgroundColorInfo.backgroundColor1;
                if (PositionGrid  != null) PositionGrid.  BackgroundColor = gridBackgroundColorInfo.backgroundColor1;
                if (AllGrid       != null) AllGrid.       BackgroundColor = gridBackgroundColorInfo.backgroundColor1;

                if (RawDisplay != null)
                {
                    subdueALIWindow(RawDisplay.subdued);
                    RawDisplay.Refresh();
                }
            }
            catch {}
        }

        public void MsgReceived()
        {
            try
            {
                Invoke(new msgReceivedDelegate(msgReceived));
            }
            catch {try{msgReceived();} catch{}}
        }

        private delegate void msgReceivedDelegate();

        private string NoMsgWarningHeader =
            (Translate)("Communication failed with the 9-1-1 Server");

        private void msgReceived()
        {
            try
            {
                connectionEstablished = true;

                // start a timer so that if an Ack is not received in the specified time, warn the user
                int msgTimeout = System.Math.Max(Convert.ToInt32(heartbeatTimeout * 2.5), 15000);
                msgTimer.Change(msgTimeout, Timeout.Infinite);

                tcpCloseTimer.Change(
                    Convert.ToUInt16(controller.settings.GetSetting("TCPCloseTimeout", "30")) * 1000, Timeout.Infinite);

                if (alertMsgMenu.Visible && alertMsgMenu.Text.StartsWith(NoMsgWarningHeader)) alertMsgMenu.Visible = false;

                if (communicationWithServerHasFailed)
                {
                    communicationWithServerHasFailed = false;

                    if (controller != null)
                    {
                        string alert = (Translate)("Communication established with the 9-1-1 Server");

                        controller.dataModel.AddAlarm(DateTime.Now.ToLocalTime().ToShortTimeString(), alert);
                        controller.showNotice(alert, true);
                    }
                }
            }
            catch {}

            // send saved alerts upon receiving the first message from the Controller
            try {Controller.sendSavedAlerts();} catch {}
        }

        private bool communicationWithServerHasFailed = false;

        public void MsgTimerExpired(object obj)
        {
            try   {Invoke(new msgTimerExpiredDelegate(msgTimerExpired));}
            catch {try {msgTimerExpired();} catch{}}
        }

        private delegate void msgTimerExpiredDelegate();

        private void msgTimerExpired()
        {
            try
            {
                if (inDemoMode) return;

                communicationWithServerHasFailed = true;

                markAllLiveCallsUnknown();

                // test ping
                string serverHost  = controller.settings.GetSetting("ServerIPAddress", "192.168.62.11");
                bool   pingSuccess = (serverHost == "127.0.0.1");

                if ( ! pingSuccess)
                {
                    try
                    {
                        pingSuccess = (new Ping().Send(serverHost, 1000).Status == IPStatus.Success);
                    }
                    catch {}
                }

                string pingMsg = pingSuccess ? " [ping " + (Translate)("success") + "]"
                                             : " [ping " + (Translate)("failed")  + "]";

                int msgTimeout  = System.Math.Max(Convert.ToInt32(heartbeatTimeout * 2.5) / 1000, 15);
                string alertStr = NoMsgWarningHeader + pingMsg +
                    (Translate)(" (no message received for at least ") + msgTimeout + (Translate)(" seconds)");

                Controller.showAlert(alertStr, Console.LogType.Alarm, Console.LogID.Connection);

                if ( ! alertMsgMenu.Visible)
                {
                    alertMsgMenu.Text    = NoMsgWarningHeader + pingMsg;
                    alertMsgMenu.Visible = true;
                }
            }
            catch {}
        }

        public void TCPCloseTimerExpired(object obj)
        {
            try   {Invoke(new tcpCloseTimerExpiredDelegate(tcpCloseTimerExpired));}
            catch {try {tcpCloseTimerExpired();} catch{}}
        }

        private delegate void tcpCloseTimerExpiredDelegate();

        private void tcpCloseTimerExpired()
        {
            socketAllowedToReconnect = false;

            UInt16 reconnectSec = 10;

            try   {reconnectSec = Convert.ToUInt16(controller.settings.GetSetting("TCPReconnectWait", "10"));}
            catch {}

            try   {if (tcpReconnectTimer != null) tcpReconnectTimer.Change(reconnectSec * 1000, Timeout.Infinite);}
            catch {}

            try
            {
Controller.DebugWindowMsg("TCP Close Timer expired, attempt to reconnect in " + reconnectSec.ToString() + " seconds");

                closeSocket();

                if (controller != null)
                {
                    controller.XmlMessageServer.Stop();
Controller.DebugWindowMsg("  XmlMessageServer.Stop");
                }
            }
            catch {}
        }

        public void TCPReconnectTimerExpired(object obj)
        {
            try   {Invoke(new tcpReconnectTimerExpiredDelegate(tcpReconnectTimerExpired));}
            catch {try {tcpReconnectTimerExpired();} catch{}}
        }

        private delegate void tcpReconnectTimerExpiredDelegate();

        private void tcpReconnectTimerExpired()
        {
            try
            {
                socketAllowedToReconnect = true;

Controller.DebugWindowMsg("TCP Reconnect Timer expired, attempting to reconnect");
                SendHeartbeat(null);
            }
            catch {}
        }

        public void SettingsReceived(Settings settings)
        {
            try
            {
                Invoke(new settingsReceivedDelegate(settingsReceived), settings);
            }
            catch {try{settingsReceived(settings);} catch{}}

            timeTimer = new System.Threading.Timer(UpdateTime, null, 1000, Timeout.Infinite);
        }

        private delegate void settingsReceivedDelegate(Settings settings);

        private static bool settingsHaveBeenReceived = false;

        private void settingsReceived(Settings settings)
        {
            settingsHaveBeenReceived = true;

            try {registerForSessionChangeEvents();} catch {}

            try
            {
                object heartbeatInterval = settings.GeneralSettings["HeartbeatInterval"];
                if (heartbeatInterval != null) setHeartbeatInterval(Convert.ToInt32(heartbeatInterval) * 1000);

                object showManualRequestButtonObj = settings.GeneralSettings["ShowManualRequestButton"];
                object showCADButtonObj           = settings.GeneralSettings["ShowCADButton"];
                object showCADeraseButtonObj      = settings.GeneralSettings["ShowCADeraseButton"];
                object showRadioButtonObj         = settings.GeneralSettings["ShowRadioButton"];
                object showMapButtonObj           = settings.GeneralSettings["ShowMapButton"];
                object showCallBackButtonObj      = settings.GeneralSettings["ShowCallBackButton"];
                object showAnswerButtonObj        = settings.GeneralSettings["ShowAnswerButton"];
                object showBargeButtonObj         = settings.GeneralSettings["ShowBargeButton"];
                object showHoldButtonObj          = settings.GeneralSettings["ShowHoldButton"];
                object showHangUpButtonObj        = settings.GeneralSettings["ShowHangUpButton"];
                object showMuteButtonObj          = settings.GeneralSettings["ShowMuteButton"];
                object showVolumeButtonObj        = settings.GeneralSettings["ShowVolumeButton"];
                object showAlertButtonObj         = settings.GeneralSettings["ShowAlertButton"];
                object showPSAPStatusButtonObj    = settings.GeneralSettings["ShowPSAPstatusApplet"];
                object showTDDButtonObj           = settings.GeneralSettings["ShowTDDbutton"];
                object showSMSButtonObj           = settings.GeneralSettings["ShowSMSapplet"];
                object showIrrButtonObj           = settings.GeneralSettings["ShowIRRButton"];
                object showColorEditorObj         = settings.GeneralSettings["ShowColorEditorApplet"];
                object showFlashHookButtonObj     = settings.GeneralSettings["ShowFlashHookButton"];

                bool showManualRequestButton = (showManualRequestButtonObj != null ? toBoolean(showManualRequestButtonObj) : false);
                bool showCADButton           = (showCADButtonObj           != null ? toBoolean(showCADButtonObj)           : false);
                bool showCADeraseButton      = (showCADeraseButtonObj      != null ? toBoolean(showCADeraseButtonObj)      : false);
                bool showRadioButton         = (showRadioButtonObj         != null ? toBoolean(showRadioButtonObj)         : false);
                bool showMapButton           = (showMapButtonObj           != null ? toBoolean(showMapButtonObj)           : false);
                bool showCallBackButton      = (showCallBackButtonObj      != null ? toBoolean(showCallBackButtonObj)      : false);
                bool showAnswerButton        = (showAnswerButtonObj        != null ? toBoolean(showAnswerButtonObj)        : false);
                bool showBargeButton         = (showBargeButtonObj         != null ? toBoolean(showBargeButtonObj)         : false);
                bool showHoldButton          = (showHoldButtonObj          != null ? toBoolean(showHoldButtonObj)          : false);
                bool showHangUpButton        = (showHangUpButtonObj        != null ? toBoolean(showHangUpButtonObj)        : false);
                bool showMuteButton          = (showMuteButtonObj          != null ? toBoolean(showMuteButtonObj)          : false);
                bool showVolumeButton        = (showVolumeButtonObj        != null ? toBoolean(showVolumeButtonObj)        : false);
                bool showAlertButton         = (showAlertButtonObj         != null ? toBoolean(showAlertButtonObj)         : true);
                bool showPSAPStatusButton    = (showPSAPStatusButtonObj    != null ? toBoolean(showPSAPStatusButtonObj)    : false);
                bool showTDDButton           = (showTDDButtonObj           != null ? toBoolean(showTDDButtonObj)           : true);
                bool showSMSButton           = (showSMSButtonObj           != null ? toBoolean(showSMSButtonObj)           : false);
                bool showIrrButton           = (showIrrButtonObj           != null ? toBoolean(showIrrButtonObj)           : true);
                bool showColorEditor         = (showColorEditorObj         != null ? toBoolean(showColorEditorObj)         : true);
                bool showFlashHookButton     = (showFlashHookButtonObj     != null ? toBoolean(showFlashHookButtonObj)     : false);

                                               manualBidMenu.           Visible = showManualRequestButton;
                sendToCADButton.     Visible = sendToCADMenu.           Visible = showCADButton;
                sendEraseToCADButton.Visible = sendEraseToCADMenu.      Visible = showCADeraseButton;
                radioStateRadioMenu. Visible = radioStateTelephoneMenu. Visible = showRadioButton;
                mapALIButton.        Visible = mapALIMenu.              Visible = showMapButton;
                phonebookButton.     Visible = phonebookMenu.           Visible = true;
                callbackButton.      Visible = callbackMenu.            Visible = showCallBackButton;
                connectButton.       Visible = connectMenu.             Visible = showAnswerButton;
                bargeButton.         Visible = bargeMenu.               Visible = showBargeButton;
                holdButton.          Visible = holdMenu.                Visible = showHoldButton;
                disconnectButton.    Visible = disconnectMenu.          Visible = showHangUpButton;
                muteButton.          Visible = muteMenu.                Visible = showMuteButton;
                volumeButton.        Visible = volumeMenu.              Visible = showVolumeButton;
                alertsButton.        Visible = alertsMenu.              Visible = showAlertButton;
                tddButton.           Visible = tddMenu.                 Visible = showTDDButton;
                smsButton.           Visible = smsMenu.                 Visible = showSMSButton;
                irrButton.           Visible = irrMenu.                 Visible = showIrrButton;
                                               colorEditorMenu.         Visible = showColorEditor;
                psapButton.          Visible = psapStatusMenu.          Visible = showPSAPStatusButton;
                flashHookButton.     Visible = flashHookMenu.           Visible = showFlashHookButton;

// TODO gui: enable the flash hook button and menu?
flashHookButton.Visible = flashHookMenu.Visible = false;

                if ( ! showAlertButton)      controller.noForm.menuViewAlarms.     Visible = false;
                if ( ! showTDDButton)        controller.noForm.menuTrayTDD.        Visible = false;
                if ( ! showSMSButton)        controller.noForm.menuTraySMS.        Visible = false;
                if ( ! showPSAPStatusButton) controller.noForm.menuTrayPSAPStatus. Visible = false;
                if ( ! showColorEditor)      controller.noForm.menuTrayColorEditor.Visible = false;
                if ( ! showCallBackButton)   controller.noForm.menuCallback.       Visible = false;
                if ( ! showAnswerButton)     controller.noForm.menuConnect.        Visible = false;
                if ( ! showBargeButton)      controller.noForm.menuBarge.          Visible = false;
                if ( ! showFlashHookButton)  controller.noForm.menuFlashHook.      Visible = false;
                if ( ! showHoldButton)       controller.noForm.menuHold.           Visible = false;
                if ( ! showHangUpButton)     controller.noForm.menuDisconnect.     Visible = false;

                object showAttendedTransferButtonObj   = settings.GeneralSettings["ShowAttendedTransferButton"];
                object showTandemTransferButtonObj     = settings.GeneralSettings["ShowTandemTransferButton"];
                object showBlindTransferButtonObj      = settings.GeneralSettings["ShowBlindTransferButton"];
                object showConferenceTransferButtonObj = settings.GeneralSettings["ShowConferenceTransferButton"];
                object showFlashTransferButtonObj      = settings.GeneralSettings["ShowFlashTransferButton"];
                object showSpeedDialButtonObj          = settings.GeneralSettings["ShowSpeedDialButton"];
                object showDialPadButtonObj            = settings.GeneralSettings["ShowDialPadButton"];

                Phonebook.showAttendedTransferButton   = (showAttendedTransferButtonObj   != null ? toBoolean(showAttendedTransferButtonObj)   : false);
                Phonebook.showTandemTransferButton     = (showTandemTransferButtonObj     != null ? toBoolean(showTandemTransferButtonObj)     : false);
                Phonebook.showBlindTransferButton      = (showBlindTransferButtonObj      != null ? toBoolean(showBlindTransferButtonObj)      : false);
                Phonebook.showConferenceTransferButton = (showConferenceTransferButtonObj != null ? toBoolean(showConferenceTransferButtonObj) : false);
                Phonebook.showFlashTransferButton      = (showFlashTransferButtonObj      != null ? toBoolean(showFlashTransferButtonObj)      : false);
                Phonebook.showSpeedDialButton          = (showSpeedDialButtonObj          != null ? toBoolean(showSpeedDialButtonObj)          : true);
                Phonebook.showDialPadButton            = (showDialPadButtonObj            != null ? toBoolean(showDialPadButtonObj)            : true);
            }
            catch (Exception e)
            {
                Controller.sendAlertToController(
                    "Buttons initialization from Init message did not complete (" + e.Message + ")",
                    LogType.Warning, LogID.Init);
            }

            try
            {
                // hide these tool buttons, but leave the menus for now - 5/12/09 elliott
                sendToCADButton.      Visible = false;
                sendEraseToCADButton. Visible = false;
            }
            catch {}

            try
            {
                object tabShowLineViewObj         = settings.GeneralSettings["TabSetup.ShowLineViewTab"];
                object tabBehaviorCodeObj         = settings.GeneralSettings["TabSetup.BehaviorCode"];
                object tabNumberOfRowsObj         = settings.GeneralSettings["TabSetup.NumberOfRows"];
                object tabAbandonedCallsMaxObj    = settings.GeneralSettings["TabSetup.AbandonedCallsWithPositionZeroMax"];
                object tabRecordAgeOutSecondsObj  = settings.GeneralSettings["TabSetup.RecordAgeOutSeconds"];

                bool tabShowLineView   = (tabShowLineViewObj        != null ? toBoolean(tabShowLineViewObj)              : false);
                int tabBehaviorCode    = (tabBehaviorCodeObj        != null ? Convert.ToInt32(tabBehaviorCodeObj)        : 0);
                maxNumberOfRows        = (tabNumberOfRowsObj        != null ? Convert.ToInt32(tabNumberOfRowsObj)        : maxNumberOfRows);
                maxAbandonedCalls      = (tabAbandonedCallsMaxObj   != null ? Convert.ToInt32(tabAbandonedCallsMaxObj)   : maxAbandonedCalls);
                maxRecordAgeOutSeconds = (tabRecordAgeOutSecondsObj != null ? Convert.ToInt32(tabRecordAgeOutSecondsObj) : maxRecordAgeOutSeconds);

                switch (tabBehaviorCode & (int)TabStartupBehavior.Mask)
                {
                    case (int)TabStartupBehavior.All:       onStartupTab = allTab;       break;
                    case (int)TabStartupBehavior.Connected: onStartupTab = connectedTab; break;
                    case (int)TabStartupBehavior.Line:      onStartupTab = lineTab;      break;
                    case (int)TabStartupBehavior.Position:  onStartupTab = positionTab;  break;
                    case (int)TabStartupBehavior.Abandoned: onStartupTab = abandonedTab; break;
                    default:
                        Controller.sendAlertToController(
                            "Invalid startup view behavior code (defaulting to All View)",
                            LogType.Warning, LogID.Init);

                        onStartupTab = allTab;
                        break;
                }

                if ( ! tabShowLineView)
                {
                    tabWidget.Controls.Remove(lineTab);

                    if (onStartupTab == lineTab)
                    {
                        Controller.sendAlertToController("Invalid startup view behavior " +
                            "(Line View is hidden and also the Startup View -- defaulting to All View)",
                            LogType.Warning, LogID.Init);

                        onStartupTab = allTab;
                    }
                }

                tabWidget.SelectedTab = onStartupTab;

                switch (tabBehaviorCode & (int)TabOnConnectBehavior.Mask)
                {
                    case (int)TabOnConnectBehavior.All:                    onConnectTab = allTab;       onConnectTabIfAbandoned = false; break;
                    case (int)TabOnConnectBehavior.Connected:              onConnectTab = connectedTab; onConnectTabIfAbandoned = false; break;
                    case (int)TabOnConnectBehavior.Line:                   onConnectTab = lineTab;      onConnectTabIfAbandoned = false; break;
                    case (int)TabOnConnectBehavior.Position:               onConnectTab = positionTab;  onConnectTabIfAbandoned = false; break;
                    case (int)TabOnConnectBehavior.All_If_Abandoned:       onConnectTab = allTab;       onConnectTabIfAbandoned = true;  break;
                    case (int)TabOnConnectBehavior.Connected_If_Abandoned: onConnectTab = connectedTab; onConnectTabIfAbandoned = true;  break;
                    case (int)TabOnConnectBehavior.Line_If_Abandoned:      onConnectTab = lineTab;      onConnectTabIfAbandoned = true;  break;
                    case (int)TabOnConnectBehavior.Position_If_Abandoned:  onConnectTab = positionTab;  onConnectTabIfAbandoned = true;  break;

                    default:
                    {
                        Controller.sendAlertToController(
                            "Invalid on-connect behavior code (defaulting to All-View-if-in-Abandoned-View)",
                            LogType.Warning, LogID.Init);

                        onConnectTab            = allTab;
                        onConnectTabIfAbandoned = true;
                        break;
                    }
                }

                switch (tabBehaviorCode & (int)TabOnDisconnectBehavior.Mask)
                {
                    case (int)TabOnDisconnectBehavior.DoNothing: onDisconnectTab = null;         break;
                    case (int)TabOnDisconnectBehavior.All:       onDisconnectTab = allTab;       break;
                    case (int)TabOnDisconnectBehavior.Connected: onDisconnectTab = connectedTab; break;
                    case (int)TabOnDisconnectBehavior.Line:      onDisconnectTab = lineTab;      break;
                    case (int)TabOnDisconnectBehavior.Position:  onDisconnectTab = positionTab;  break;
                    case (int)TabOnDisconnectBehavior.Abandoned: onDisconnectTab = abandonedTab; break;

                    default:
                    {
                        Controller.sendAlertToController(
                            "Invalid on-disconnect behavior code (defaulting to do nothing on disconnect)",
                            LogType.Warning, LogID.Init);

                        onDisconnectTab = null;
                        break;
                    }
                }

                if ( ! tabShowLineView)
                {
                    if (onConnectTab == lineTab)
                    {
                        Controller.sendAlertToController("Invalid on-connect behavior " +
                            "(Line View is hidden and also the on-connect View -- defaulting to All-View-if-in-Abandoned-View)",
                            LogType.Warning, LogID.Init);

                        onConnectTab            = allTab;
                        onConnectTabIfAbandoned = true;
                    }

                    if (onDisconnectTab == lineTab)
                    {
                        Controller.sendAlertToController("Invalid on-disconnect behavior " +
                            "(Line View is hidden and also the on-disconnect View -- defaulting to do nothing on disconnect)",
                            LogType.Warning, LogID.Init);

                        onDisconnectTab = null;
                    }
                }
            }
            catch (Exception e)
            {
                Controller.sendAlertToController(
                    "Tab initialization from Init message did not complete (" + e.Message + ")",
                    LogType.Warning, LogID.Init);
            }

            addBlankLineRows();
        }

        private void addSeparators()
        {
            try
            {
                // toolbar separators
                bool needSep = (printButton.Visible || rebidALIButton.Visible || mapALIButton.Visible);

                if (abandonedButton.Visible || callbackButton.Visible)
                {
                    if (needSep) toolBar.Items.Insert(toolBar.Items.IndexOf(abandonedButton), new ToolStripSeparator());

                    needSep = true;
                }

                if (connectButton.Visible || bargeButton.Visible || flashHookButton.Visible || holdButton.Visible || disconnectButton.Visible || muteButton.Visible || volumeButton.Visible)
                {
                    if (needSep) toolBar.Items.Insert(toolBar.Items.IndexOf(connectButton), new ToolStripSeparator());

                    needSep = true;
                }

                if (tddButton.Visible || phonebookButton.Visible || smsButton.Visible || alertsButton.Visible || psapButton.Visible || irrButton.Visible)
                {
                    if (needSep) toolBar.Items.Insert(toolBar.Items.IndexOf(tddButton), new ToolStripSeparator());

                    needSep = true;
                }
            }
            catch {}

            try
            {
/*

TODO gui: Title Bar change to   "PSAP Toolkit" ???

*/
                // menu separators (use button states since the menus are not currently 'Visible')
                bool needSep = abandonedButton.Visible || callbackButton.Visible;

                if (connectButton.Visible || bargeButton.Visible || flashHookButton.Visible || holdButton.Visible || disconnectButton.Visible || muteButton.Visible || volumeButton.Visible)
                {
                    if (needSep) callMenu.DropDownItems.Insert(callMenu.DropDownItems.IndexOf(connectMenu), new ToolStripSeparator());

                    needSep = true;
                }

                if (participantMenu != null || forceDisconnectMenu != null)
                {
                    if (needSep) callMenu.DropDownItems.Insert(callMenu.DropDownItems.IndexOf(participantMenu), new ToolStripSeparator());

                    needSep = true;
                }
            }
            catch {}
        }

        static public bool toBoolean(object obj)
        {
            switch (obj.ToString().ToLower())
            {
                case "1":        return true;
                case "on":       return true;
                case "yes":      return true;
                case "true":     return true;
                case "enable":   return true;
                case "enabled":  return true;

                case "0":        return false;
                case "off":      return false;
                case "no":       return false;
                case "false":    return false;
                case "disable":  return false;
                case "disabled": return false;

                default: throw new Exception("Failed to convert boolean value '" + obj + "'");
            }
        }

        static public int toBooleanInt(object obj) {return toBoolean(obj) ? 1 : 0;}

        private void addBlankLineRows()
        {
            try
            {
                // add blank rows for each Line
                foreach (ushort lineNumber in Controller.lineNumberDictionary.Keys)
                {
                    string callData = "<CallData Call-ID=\"" + lineNumber.ToString() +
                                          "\" LineNumber=\"" + lineNumber.ToString() + "\" />";

                    controller.XmlMessageServer.ProcessMessage(
                        "<?xml version=\"1.0\" encoding=\"utf-8\"?><Event id=\"\" XMLspec=\"" +
                        XMLMessage.SPEC_VERSION + "\" ><CallInfo> " + callData + " </CallInfo></Event>");
                }
            }
            catch (Exception e)
            {
                Controller.sendAlertToController(
                    "Failed to add some or all blank rows for Line View (" + e.Message + ")",
                    LogType.Warning, LogID.Init);
            }
        }

        public void setHeartbeatInterval(int ms) {heartbeatTimeout = ms; heartbeatTimer.Change(heartbeatTimeout, Timeout.Infinite);}

        protected override void Dispose( bool disposing )
        {
            if (disposing)
            {
                closeSocket();

                if (sessionChangesRegistered)
                {
                    WTSUnRegisterSessionNotification(Handle);
                    sessionChangesRegistered = false;
                }
            }

            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Console));
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.printSetup = new System.Windows.Forms.PrintDialog();
            this.printDoc = new System.Drawing.Printing.PrintDocument();
            this.toolStripContainer = new System.Windows.Forms.ToolStripContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.line = new System.Windows.Forms.Label();
            this.RawDisplay = new WestTel.E911.Console.GradientTextBox();
            this.tabWidget = new System.Windows.Forms.TabControl();
            this.lineTab = new System.Windows.Forms.TabPage();
            this.connectedTab = new System.Windows.Forms.TabPage();
            this.abandonedTab = new System.Windows.Forms.TabPage();
            this.positionTab = new System.Windows.Forms.TabPage();
            this.allTab = new System.Windows.Forms.TabPage();
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.debugScriptMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.resetMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.fileMenuSep1 = new System.Windows.Forms.ToolStripSeparator();
            this.printMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.fileMenuSep2 = new System.Windows.Forms.ToolStripSeparator();
            this.closeMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.aliMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.rebidALIMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.aliMenuSep1 = new System.Windows.Forms.ToolStripSeparator();
            this.mapALIMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.manualBidMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.aliMenuSep2 = new System.Windows.Forms.ToolStripSeparator();
            this.sendToCADMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.sendEraseToCADMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.callMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.abandonedMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.callbackMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.connectMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.bargeMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.holdMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.flashHookMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.disconnectMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.muteMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.volumeMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.participantMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.forceDisconnectMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.radioStateRadioMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.radioStateTelephoneMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.tddMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.phonebookMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.alertsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.psapStatusMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.smsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.irrMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenuSep = new System.Windows.Forms.ToolStripSeparator();
            this.colorEditorMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.usersManualMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuSep = new System.Windows.Forms.ToolStripSeparator();
            this.aboutMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.alertMsgMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolBar = new System.Windows.Forms.ToolStrip();
            this.printButton = new System.Windows.Forms.ToolStripButton();
            this.rebidALIButton = new System.Windows.Forms.ToolStripButton();
            this.mapALIButton = new System.Windows.Forms.ToolStripButton();
            this.abandonedButton = new System.Windows.Forms.ToolStripButton();
            this.callbackButton = new System.Windows.Forms.ToolStripButton();
            this.connectButton = new System.Windows.Forms.ToolStripButton();
            this.bargeButton = new System.Windows.Forms.ToolStripButton();
            this.flashHookButton = new System.Windows.Forms.ToolStripButton();
            this.holdButton = new System.Windows.Forms.ToolStripButton();
            this.disconnectButton = new System.Windows.Forms.ToolStripButton();
            this.muteButton = new System.Windows.Forms.ToolStripButton();
            this.volumeButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.tddButton = new System.Windows.Forms.ToolStripButton();
            this.phonebookButton = new System.Windows.Forms.ToolStripButton();
            this.alertsButton = new System.Windows.Forms.ToolStripButton();
            this.psapButton = new System.Windows.Forms.ToolStripButton();
            this.smsButton = new System.Windows.Forms.ToolStripButton();
            this.irrButton = new System.Windows.Forms.ToolStripButton();
            this.sendToCADButton = new System.Windows.Forms.ToolStripButton();
            this.sendEraseToCADButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer.ContentPanel.SuspendLayout();
            this.toolStripContainer.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabWidget.SuspendLayout();
            this.MainMenu.SuspendLayout();
            this.toolBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolTip
            // 
            this.toolTip.ShowAlways = true;
            this.toolTip.StripAmpersands = true;
            this.toolTip.Draw += new System.Windows.Forms.DrawToolTipEventHandler(this.toolTip_Draw);
            // 
            // printSetup
            // 
            this.printSetup.Document = this.printDoc;
            // 
            // printDoc
            // 
            this.printDoc.DocumentName = "ANIInfo";
            this.printDoc.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDoc_PrintPage);
            // 
            // toolStripContainer
            // 
            // 
            // toolStripContainer.ContentPanel
            // 
            this.toolStripContainer.ContentPanel.Controls.Add(this.tableLayoutPanel1);
            this.toolStripContainer.ContentPanel.Size = new System.Drawing.Size(1024, 262);
            this.toolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer.Margin = new System.Windows.Forms.Padding(0);
            this.toolStripContainer.Name = "toolStripContainer";
            this.toolStripContainer.Size = new System.Drawing.Size(1024, 344);
            this.toolStripContainer.TabIndex = 0;
            // 
            // toolStripContainer.TopToolStripPanel
            // 
            this.toolStripContainer.TopToolStripPanel.Controls.Add(this.MainMenu);
            this.toolStripContainer.TopToolStripPanel.Controls.Add(this.toolBar);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.line, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.RawDisplay, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tabWidget, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1024, 262);
            this.tableLayoutPanel1.TabIndex = 15;
            // 
            // MainMenu
            // 
            this.MainMenu.AllowMerge = false;
            this.MainMenu.Dock = System.Windows.Forms.DockStyle.None;
            this.MainMenu.GripMargin = new System.Windows.Forms.Padding(0);
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.aliMenu,
            this.callMenu,
            this.toolsMenu,
            this.helpMenu,
            this.alertMsgMenu});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(1024, 24);
            this.MainMenu.TabIndex = 0;
            this.MainMenu.Text = "Main Menu";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.debugScriptMenu,
            this.resetMenu,
            this.fileMenuSep1,
            this.printMenu,
            this.fileMenuSep2,
            this.closeMenu});
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(37, 20);
            this.fileMenu.Text = "&File";
            // 
            // debugScriptMenu
            // 
            this.debugScriptMenu.Name = "debugScriptMenu";
            this.debugScriptMenu.Size = new System.Drawing.Size(235, 22);
            this.debugScriptMenu.Text = "Start Recording &Debug Script...";
            this.debugScriptMenu.Click += new System.EventHandler(this.debugScriptMenu_Click);
            // 
            // resetMenu
            // 
            this.resetMenu.Name = "resetMenu";
            this.resetMenu.Size = new System.Drawing.Size(235, 22);
            this.resetMenu.Text = "&Reset Console...";
            this.resetMenu.Click += new System.EventHandler(this.resetMenu_Click);
            // 
            // fileMenuSep1
            // 
            this.fileMenuSep1.Name = "fileMenuSep1";
            this.fileMenuSep1.Size = new System.Drawing.Size(232, 6);
            // 
            // printMenu
            // 
            this.printMenu.Enabled = false;
            this.printMenu.Image = new Icon(Properties.Resources.PrintIcon, 16, 16).ToBitmap();
            this.printMenu.Name = "printMenu";
            this.printMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.printMenu.Size = new System.Drawing.Size(235, 22);
            this.printMenu.Text = "&Print...";
            this.printMenu.Click += new System.EventHandler(this.printMenu_Click);
            // 
            // fileMenuSep2
            // 
            this.fileMenuSep2.Name = "fileMenuSep2";
            this.fileMenuSep2.Size = new System.Drawing.Size(232, 6);
            // 
            // closeMenu
            // 
            this.closeMenu.Name = "closeMenu";
            this.closeMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.closeMenu.Size = new System.Drawing.Size(235, 22);
            this.closeMenu.Text = "&Close";
            this.closeMenu.Click += new System.EventHandler(this.closeMenu_Click);
            // 
            // aliMenu
            // 
            this.aliMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rebidALIMenu,
            this.aliMenuSep1,
            this.mapALIMenu,
            this.manualBidMenu,
            this.aliMenuSep2,
            this.sendToCADMenu,
            this.sendEraseToCADMenu});
            this.aliMenu.Name = "aliMenu";
            this.aliMenu.Size = new System.Drawing.Size(36, 20);
            this.aliMenu.Text = "&ALI";
            // 
            // rebidALIMenu
            // 
            this.rebidALIMenu.Enabled = false;
            this.rebidALIMenu.Image = new Icon(Properties.Resources.ConsoleALIRebidIcon, 16, 16).ToBitmap();
            this.rebidALIMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.rebidALIMenu.Name = "rebidALIMenu";
            this.rebidALIMenu.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.R)));
            this.rebidALIMenu.Size = new System.Drawing.Size(177, 22);
            this.rebidALIMenu.Text = "Re&bid";
            this.rebidALIMenu.Click += new System.EventHandler(this.btnReBidClick);
            // 
            // aliMenuSep1
            // 
            this.aliMenuSep1.Name = "aliMenuSep1";
            this.aliMenuSep1.Size = new System.Drawing.Size(174, 6);
            // 
            // mapALIMenu
            // 
            this.mapALIMenu.Enabled = false;
            this.mapALIMenu.Image = new Icon(Properties.Resources.ConsoleMapALIIcon, 16, 16).ToBitmap();
            this.mapALIMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mapALIMenu.Name = "mapALIMenu";
            this.mapALIMenu.Size = new System.Drawing.Size(177, 22);
            this.mapALIMenu.Text = "&Map";
            this.mapALIMenu.Click += new System.EventHandler(this.btnMapALIClick);
            // 
            // manualBidMenu
            // 
            this.manualBidMenu.Image = new Icon(Properties.Resources.ConsoleManualALIRequestIcon, 16, 16).ToBitmap();
            this.manualBidMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.manualBidMenu.Name = "manualBidMenu";
            this.manualBidMenu.Size = new System.Drawing.Size(177, 22);
            this.manualBidMenu.Text = "Manual &Request...";
            this.manualBidMenu.Click += new System.EventHandler(this.btnManualBidClick);
            // 
            // aliMenuSep2
            // 
            this.aliMenuSep2.Name = "aliMenuSep2";
            this.aliMenuSep2.Size = new System.Drawing.Size(174, 6);
            // 
            // sendToCADMenu
            // 
            this.sendToCADMenu.Enabled = false;
            this.sendToCADMenu.Image = new Icon(Properties.Resources.ConsoleSendALIToCADIcon, 16, 16).ToBitmap();
            this.sendToCADMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.sendToCADMenu.Name = "sendToCADMenu";
            this.sendToCADMenu.Size = new System.Drawing.Size(177, 22);
            this.sendToCADMenu.Text = "&Send to CAD";
            this.sendToCADMenu.Click += new System.EventHandler(this.btnSendToCADClick);
            // 
            // sendEraseToCADMenu
            // 
            this.sendEraseToCADMenu.Enabled = false;
            this.sendEraseToCADMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.sendEraseToCADMenu.Name = "sendEraseToCADMenu";
            this.sendEraseToCADMenu.Size = new System.Drawing.Size(177, 22);
            this.sendEraseToCADMenu.Text = "Send &Erase to CAD";
            this.sendEraseToCADMenu.Click += new System.EventHandler(this.btnSendEraseToCADClick);
            // 
            // callMenu
            // 
            this.callMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abandonedMenu,
            this.callbackMenu,
            this.connectMenu,
            this.bargeMenu,
            this.holdMenu,
            this.flashHookMenu,
            this.disconnectMenu,
            this.muteMenu,
            this.volumeMenu,
            this.participantMenu,
            this.forceDisconnectMenu,
            this.radioStateRadioMenu,
            this.radioStateTelephoneMenu});
            this.callMenu.Name = "callMenu";
            this.callMenu.Size = new System.Drawing.Size(39, 20);
            this.callMenu.Text = "&Call";
            // 
            // abandonedMenu
            // 
            this.abandonedMenu.Enabled = false;
            this.abandonedMenu.Image = new Icon(Properties.Resources.ConsoleAbandonIcon, 16, 16).ToBitmap();
            this.abandonedMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.abandonedMenu.Name = "abandonedMenu";
            this.abandonedMenu.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.D)));
            this.abandonedMenu.Size = new System.Drawing.Size(210, 22);
            this.abandonedMenu.Text = "A&bandoned";
            this.abandonedMenu.Click += new System.EventHandler(this.abandonedButtonClick);
            // 
            // callbackMenu
            // 
            this.callbackMenu.Enabled = false;
            this.callbackMenu.Image = new Icon(Properties.Resources.ConsoleCallbackIcon, 16, 16).ToBitmap();
            this.callbackMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.callbackMenu.Name = "callbackMenu";
            this.callbackMenu.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.C)));
            this.callbackMenu.Size = new System.Drawing.Size(210, 22);
            this.callbackMenu.Text = "&Callback...";
            this.callbackMenu.Visible = false;
            this.callbackMenu.Click += new System.EventHandler(this.callbackButtonClick);
            // 
            // connectMenu
            // 
            this.connectMenu.Enabled = false;
            this.connectMenu.Image = new Icon(Properties.Resources.ConsoleCallAnswerIcon, 16, 16).ToBitmap();
            this.connectMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.connectMenu.Name = "connectMenu";
            this.connectMenu.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.A)));
            this.connectMenu.Size = new System.Drawing.Size(210, 22);
            this.connectMenu.Text = "&Answer";
            this.connectMenu.Visible = false;
            this.connectMenu.Click += new System.EventHandler(this.connectButtonClick);
            // 
            // bargeMenu
            // 
            this.bargeMenu.Enabled = false;
            this.bargeMenu.Image = new Icon(Properties.Resources.ConsoleCallBargeIcon, 16, 16).ToBitmap();
            this.bargeMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bargeMenu.Name = "bargeMenu";
            this.bargeMenu.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.B)));
            this.bargeMenu.Size = new System.Drawing.Size(210, 22);
            this.bargeMenu.Text = "&Barge";
            this.bargeMenu.Visible = false;
            this.bargeMenu.Click += new System.EventHandler(this.bargeButtonClick);
            // 
            // holdMenu
            // 
            this.holdMenu.Enabled = false;
            this.holdMenu.Image = new Icon(Properties.Resources.ConsoleCallHoldIcon, 16, 16).ToBitmap();
            this.holdMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.holdMenu.Name = "holdMenu";
            this.holdMenu.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.H)));
            this.holdMenu.Size = new System.Drawing.Size(210, 22);
            this.holdMenu.Text = "&Hold";
            this.holdMenu.Visible = false;
            this.holdMenu.Click += new System.EventHandler(this.holdCheckedChanged);
            // 
            // flashHookMenu
            // 
            this.flashHookMenu.Enabled = false;
            this.flashHookMenu.Image = new Icon(Properties.Resources.ConsoleCallFlashHookIcon, 16, 16).ToBitmap();
            this.flashHookMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.flashHookMenu.Name = "flashHookMenu";
            this.flashHookMenu.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F)));
            this.flashHookMenu.Size = new System.Drawing.Size(210, 22);
            this.flashHookMenu.Text = "&Flash Hook";
            this.flashHookMenu.Visible = false;
            this.flashHookMenu.Click += new System.EventHandler(this.flashHookButtonClick);
            // 
            // disconnectMenu
            // 
            this.disconnectMenu.Enabled = false;
            this.disconnectMenu.Image = new Icon(Properties.Resources.ConsoleCallHangUpIcon, 16, 16).ToBitmap();
            this.disconnectMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.disconnectMenu.Name = "disconnectMenu";
            this.disconnectMenu.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.G)));
            this.disconnectMenu.Size = new System.Drawing.Size(210, 22);
            this.disconnectMenu.Text = "Han&g-Up...";
            this.disconnectMenu.Visible = false;
            this.disconnectMenu.Click += new System.EventHandler(this.disconnectButtonClick);
            // 
            // muteMenu
            // 
            this.muteMenu.Enabled = false;
            this.muteMenu.Image = new Icon(Properties.Resources.ConsoleCall_MuteIcon, 16, 16).ToBitmap();
            this.muteMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.muteMenu.Name = "muteMenu";
            this.muteMenu.Size = new System.Drawing.Size(210, 22);
            this.muteMenu.Text = "&Mute";
            this.muteMenu.Visible = false;
            this.muteMenu.Click += new System.EventHandler(this.muteButtonClick);
            // 
            // volumeMenu
            // 
            this.volumeMenu.Enabled = false;
            this.volumeMenu.Image = Properties.Resources.ConsoleVolumeBitmap;
            this.volumeMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.volumeMenu.Name = "volumeMenu";
            this.volumeMenu.Size = new System.Drawing.Size(210, 22);
            this.volumeMenu.Text = "&Volume";
            this.volumeMenu.Visible = false;
            // 
            // participantMenu
            // 
            this.participantMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.participantMenu.Name = "participantMenu";
            this.participantMenu.Size = new System.Drawing.Size(210, 22);
            this.participantMenu.Text = "Participants";
            this.participantMenu.Visible = false;
            this.participantMenu.Click += new System.EventHandler(this.participantButtonClick);
            // 
            // forceDisconnectMenu
            // 
            this.forceDisconnectMenu.Image = new Icon(Properties.Resources.ConsoleForceDisconnectIcon, 16, 16).ToBitmap();
            this.forceDisconnectMenu.Name = "forceDisconnectMenu";
            this.forceDisconnectMenu.Size = new System.Drawing.Size(210, 22);
            this.forceDisconnectMenu.Text = "&Force Disconnect...";
            this.forceDisconnectMenu.Click += new System.EventHandler(this.btnForceDisconnectClick);
            // 
            // radioStateRadioMenu
            // 
            this.radioStateRadioMenu.Name = "radioStateRadioMenu";
            this.radioStateRadioMenu.Size = new System.Drawing.Size(210, 22);
            this.radioStateRadioMenu.Text = "Switch to &Radio";
            this.radioStateRadioMenu.Click += new System.EventHandler(this.btnRadioStateRadioClick);
            // 
            // radioStateTelephoneMenu
            // 
            this.radioStateTelephoneMenu.Name = "radioStateTelephoneMenu";
            this.radioStateTelephoneMenu.Size = new System.Drawing.Size(210, 22);
            this.radioStateTelephoneMenu.Text = "Switch to &Call";
            this.radioStateTelephoneMenu.Click += new System.EventHandler(this.btnRadioStateTelephoneClick);
            // 
            // toolsMenu
            // 
            this.toolsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tddMenu,
            this.phonebookMenu,
            this.alertsMenu,
            this.psapStatusMenu,
            this.smsMenu,
            this.irrMenu,
            this.toolsMenuSep,
            this.colorEditorMenu});
            this.toolsMenu.Name = "toolsMenu";
            this.toolsMenu.Size = new System.Drawing.Size(47, 20);
            this.toolsMenu.Text = "&Tools";
            // 
            // tddMenu
            // 
            this.tddMenu.Image = new Icon(Properties.Resources.TDDIcon, 16, 16).ToBitmap();
            this.tddMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tddMenu.Name = "tddMenu";
            this.tddMenu.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F6)));
            this.tddMenu.Size = new System.Drawing.Size(215, 22);
            this.tddMenu.Text = "T&DD";
            this.tddMenu.Click += new System.EventHandler(this.btnTDDClick);
            // 
            // phonebookMenu
            // 
            this.phonebookMenu.Image = new Icon(Properties.Resources.PhonebookIcon, 16, 16).ToBitmap();
            this.phonebookMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.phonebookMenu.Name = "phonebookMenu";
            this.phonebookMenu.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F7)));
            this.phonebookMenu.Size = new System.Drawing.Size(215, 22);
            this.phonebookMenu.Text = "&Phonebook";
            this.phonebookMenu.Click += new System.EventHandler(this.btnPhonebookClick);
            // 
            // alertsMenu
            // 
            this.alertsMenu.Image = new Icon(Properties.Resources.AlertIcon, 16, 16).ToBitmap();
            this.alertsMenu.Name = "alertsMenu";
            this.alertsMenu.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F8)));
            this.alertsMenu.Size = new System.Drawing.Size(215, 22);
            this.alertsMenu.Text = "&Alerts";
            this.alertsMenu.Click += new System.EventHandler(this.btnAlertsClick);
            // 
            // psapStatusMenu
            // 
            this.psapStatusMenu.Image = new Icon(Properties.Resources.PSAPStatusIcon, 16, 16).ToBitmap();
            this.psapStatusMenu.Name = "psapStatusMenu";
            this.psapStatusMenu.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F9)));
            this.psapStatusMenu.Size = new System.Drawing.Size(215, 22);
            this.psapStatusMenu.Text = "PSAP &Status";
            this.psapStatusMenu.Click += new System.EventHandler(this.btnPSAPStatusClick);
            // 
            // smsMenu
            // 
            this.smsMenu.Image = new Icon(Properties.Resources.SMSIcon, 16, 16).ToBitmap();
            this.smsMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.smsMenu.Name = "smsMenu";
            this.smsMenu.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F10)));
            this.smsMenu.Size = new System.Drawing.Size(215, 22);
            this.smsMenu.Text = "&SMS";
            this.smsMenu.Click += new System.EventHandler(this.btnSMSClick);
            // 
            // irrMenu
            // 
            this.irrMenu.Image = Properties.Resources.IRRIcon;
            this.irrMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.irrMenu.Name = "irrMenu";
            this.irrMenu.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.I)));
            this.irrMenu.Size = new System.Drawing.Size(210, 22);
            this.irrMenu.Text = "&IRR";
            this.irrMenu.Click += new System.EventHandler(this.irrButtonClick);
            // 
            // toolsMenuSep
            // 
            this.toolsMenuSep.Name = "toolsMenuSep";
            this.toolsMenuSep.Size = new System.Drawing.Size(232, 6);
            // 
            // colorEditorMenu
            // 
            this.colorEditorMenu.Image = new Icon(Properties.Resources.ColorEditorIcon, 16, 16).ToBitmap();
            this.colorEditorMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.colorEditorMenu.Name = "colorEditorMenu";
            this.colorEditorMenu.Size = new System.Drawing.Size(215, 22);
            this.colorEditorMenu.Text = "&Choose Application Colors";
            this.colorEditorMenu.Click += new System.EventHandler(this.btnColorEditorClick);
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usersManualMenu,
            this.helpMenuSep,
            this.aboutMenu});
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(44, 20);
            this.helpMenu.Text = "&Help";
            // 
            // usersManualMenu
            // 
            this.usersManualMenu.Image = new Icon(Properties.Resources.UserManualIcon, 16, 16).ToBitmap();
            this.usersManualMenu.Name = "usersManualMenu";
            this.usersManualMenu.Size = new System.Drawing.Size(140, 22);
            this.usersManualMenu.Text = "&User Manual";
            this.usersManualMenu.Click += new System.EventHandler(this.usersManualMenu_Click);
            // 
            // helpMenuSep
            // 
            this.helpMenuSep.Name = "helpMenuSep";
            this.helpMenuSep.Size = new System.Drawing.Size(137, 6);
            // 
            // aboutMenu
            // 
            this.aboutMenu.Image = new Icon(Properties.Resources.WestTelIcon, 16, 16).ToBitmap();
            this.aboutMenu.Name = "aboutMenu";
            this.aboutMenu.Size = new System.Drawing.Size(140, 22);
            this.aboutMenu.Text = "&About XXX";
            this.aboutMenu.Click += new System.EventHandler(this.aboutMenu_Click);
            // 
            // alertMsgMenu
            // 
            this.alertMsgMenu.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.alertMsgMenu.ForeColor = System.Drawing.Color.Red;
            this.alertMsgMenu.Name = "alertMsgMenu";
            this.alertMsgMenu.Size = new System.Drawing.Size(44, 20);
            this.alertMsgMenu.Text = "Alert";
            this.alertMsgMenu.Visible = false;
            // 
            // toolBar
            // 
            this.toolBar.AllowMerge = false;
            this.toolBar.Dock = System.Windows.Forms.DockStyle.None;
            this.toolBar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolBar.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printButton,
            this.rebidALIButton,
            this.mapALIButton,
            this.abandonedButton,
            this.callbackButton,
            this.connectButton,
            this.bargeButton,
            this.flashHookButton,
            this.holdButton,
            this.disconnectButton,
            this.muteButton,
            this.volumeButton,
            this.tddButton,
            this.phonebookButton,
            this.alertsButton,
            this.psapButton,
            this.smsButton,
            this.irrButton,
            this.sendToCADButton,
            this.sendEraseToCADButton});
            this.toolBar.Location = new System.Drawing.Point(0, 24);
            this.toolBar.Name = "toolBar";
            this.toolBar.Size = new System.Drawing.Size(1024, 58);
            this.toolBar.Stretch = true;
            this.toolBar.TabIndex = 1;
            // 
            // line
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.line, 2);
            this.line.BackColor = SystemColors.MenuHighlight;
            this.line.Dock = System.Windows.Forms.DockStyle.Fill;
            this.line.Margin = new Padding(0);
            this.line.Name = "line";
            this.line.Size = new Size(900, 1);
            // 
            // printButton
            // 
            this.printButton.Enabled = false;
            this.printButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.printButton.Image = Properties.Resources.PrintIcon.ToBitmap();
            this.printButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.printButton.Name = "printButton";
            this.printButton.Padding = new System.Windows.Forms.Padding(2);
            this.printButton.Size = new System.Drawing.Size(40, 46);
            this.printButton.Text = "&Print";
            this.printButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.printButton.ToolTipText = "Print ALI (Ctrl+P)";
            this.printButton.Click += new System.EventHandler(this.printMenu_Click);
            // 
            // rebidALIButton
            // 
            this.rebidALIButton.Enabled = false;
            this.rebidALIButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.rebidALIButton.Image = Properties.Resources.ConsoleALIRebidIcon.ToBitmap();
            this.rebidALIButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.rebidALIButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.rebidALIButton.Name = "rebidALIButton";
            this.rebidALIButton.Padding = new System.Windows.Forms.Padding(2);
            this.rebidALIButton.Size = new System.Drawing.Size(33, 46);
            this.rebidALIButton.Text = "ALI";
            this.rebidALIButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.rebidALIButton.ToolTipText = "Rebid ALI (Ctrl+Shift+R)";
            this.rebidALIButton.Click += new System.EventHandler(this.btnReBidClick);
            // 
            // mapALIButton
            // 
            this.mapALIButton.Enabled = false;
            this.mapALIButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.mapALIButton.Image = Properties.Resources.ConsoleMapALIIcon.ToBitmap();
            this.mapALIButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mapALIButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.mapALIButton.Name = "mapALIButton";
            this.mapALIButton.Padding = new System.Windows.Forms.Padding(2);
            this.mapALIButton.Size = new System.Drawing.Size(37, 46);
            this.mapALIButton.Text = "Map";
            this.mapALIButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.mapALIButton.ToolTipText = "Map ALI";
            this.mapALIButton.Click += new System.EventHandler(this.btnMapALIClick);
            // 
            // abandonedButton
            // 
            this.abandonedButton.Enabled = false;
            this.abandonedButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.abandonedButton.Image = Properties.Resources.ConsoleAbandonIcon.ToBitmap();
            this.abandonedButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.abandonedButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.abandonedButton.Name = "abandonedButton";
            this.abandonedButton.Padding = new System.Windows.Forms.Padding(2);
            this.abandonedButton.Size = new System.Drawing.Size(74, 42);
            this.abandonedButton.Text = "Abandoned";
            this.abandonedButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.abandonedButton.ToolTipText = "Clear Abandoned Call (Ctrl+Shift+D)";
            this.abandonedButton.Click += new System.EventHandler(this.abandonedButtonClick);
            // 
            // callbackButton
            // 
            this.callbackButton.Enabled = false;
            this.callbackButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.callbackButton.Image = Properties.Resources.ConsoleCallbackIcon.ToBitmap();
            this.callbackButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.callbackButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.callbackButton.Name = "callbackButton";
            this.callbackButton.Padding = new System.Windows.Forms.Padding(2);
            this.callbackButton.Size = new System.Drawing.Size(53, 43);
            this.callbackButton.Text = "Callback";
            this.callbackButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.callbackButton.ToolTipText = "Callback Last Outbound Call (Ctrl+Shift+C)";
            this.callbackButton.Visible = false;
            this.callbackButton.Click += new System.EventHandler(this.callbackButtonClick);
            // 
            // connectButton
            // 
            this.connectButton.Enabled = false;
            this.connectButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.connectButton.Image = Properties.Resources.ConsoleCallAnswerIcon.ToBitmap();
            this.connectButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.connectButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.connectButton.Name = "connectButton";
            this.connectButton.Padding = new System.Windows.Forms.Padding(2);
            this.connectButton.Size = new System.Drawing.Size(52, 43);
            this.connectButton.Text = "Answer";
            this.connectButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.connectButton.ToolTipText = "Connect to Call (Ctrl+Shift+A)";
            this.connectButton.Visible = false;
            this.connectButton.Click += new System.EventHandler(this.connectButtonClick);
            // 
            // bargeButton
            // 
            this.bargeButton.Enabled = false;
            this.bargeButton.Image = Properties.Resources.ConsoleCallBargeIcon.ToBitmap();
            this.bargeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bargeButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.bargeButton.Name = "bargeButton";
            this.bargeButton.Padding = new System.Windows.Forms.Padding(2);
            this.bargeButton.Size = new System.Drawing.Size(41, 43);
            this.bargeButton.Text = "Barge";
            this.bargeButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bargeButton.ToolTipText = "Barge on Call (Ctrl+Shift+B)";
            this.bargeButton.Visible = false;
            this.bargeButton.Click += new System.EventHandler(this.bargeButtonClick);
            // 
            // flashHookButton
            // 
            this.flashHookButton.Enabled = false;
            this.flashHookButton.Image = Properties.Resources.ConsoleCallFlashHookIcon.ToBitmap();
            this.flashHookButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.flashHookButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.flashHookButton.Name = "flashHookButton";
            this.flashHookButton.Padding = new System.Windows.Forms.Padding(2);
            this.flashHookButton.Size = new System.Drawing.Size(70, 43);
            this.flashHookButton.Text = "Flash Hook";
            this.flashHookButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.flashHookButton.ToolTipText = "Flash Hook (Ctrl+Shift+F)";
            this.flashHookButton.Visible = false;
            this.flashHookButton.Click += new System.EventHandler(this.flashHookButtonClick);
            // 
            // holdButton
            // 
            this.holdButton.Enabled = false;
            this.holdButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.holdButton.Image = Properties.Resources.ConsoleCallHoldIcon.ToBitmap();
            this.holdButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.holdButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.holdButton.Name = "holdButton";
            this.holdButton.Padding = new System.Windows.Forms.Padding(2);
            this.holdButton.Size = new System.Drawing.Size(35, 42);
            this.holdButton.Text = "Hold";
            this.holdButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.holdButton.ToolTipText = "Hold/Resume Connected Call (Ctrl+Shift+H)";
            this.holdButton.Visible = false;
            this.holdButton.Click += new System.EventHandler(this.holdCheckedChanged);
            // 
            // disconnectButton
            // 
            this.disconnectButton.Enabled = false;
            this.disconnectButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.disconnectButton.Image = Properties.Resources.ConsoleCallHangUpIcon.ToBitmap();
            this.disconnectButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.disconnectButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.disconnectButton.Name = "disconnectButton";
            this.disconnectButton.Padding = new System.Windows.Forms.Padding(2);
            this.disconnectButton.Size = new System.Drawing.Size(58, 42);
            this.disconnectButton.Text = "Hang-Up";
            this.disconnectButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.disconnectButton.ToolTipText = "Disconnect from Call (Ctrl+Shift+G)";
            this.disconnectButton.Visible = false;
            this.disconnectButton.Click += new System.EventHandler(this.disconnectButtonClick);
            // 
            // muteButton
            // 
            this.muteButton.Enabled = false;
            this.muteButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.muteButton.Image = Properties.Resources.ConsoleCall_MuteIcon.ToBitmap();
            this.muteButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.muteButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.muteButton.Name = "muteButton";
            this.muteButton.Padding = new System.Windows.Forms.Padding(2);
            this.muteButton.Size = new System.Drawing.Size(39, 42);
            this.muteButton.Text = "Mute";
            this.muteButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.muteButton.ToolTipText = "Mute Connected Call";
            this.muteButton.Visible = false;
            this.muteButton.Click += new System.EventHandler(this.muteButtonClick);
            // 
            // volumeButton
            // 
            this.volumeButton.Enabled = false;
            this.volumeButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.volumeButton.Image = Properties.Resources.ConsoleVolumeBitmap;
            this.volumeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.volumeButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.volumeButton.Name = "volumeButton";
            this.volumeButton.Padding = new System.Windows.Forms.Padding(2);
            this.volumeButton.Size = new System.Drawing.Size(61, 42);
            this.volumeButton.Text = "Volume";
            this.volumeButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.volumeButton.ToolTipText = "Volume Controls";
            this.volumeButton.Visible = false;
            // 
            // tddButton
            // 
            this.tddButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.tddButton.Image = Properties.Resources.ConsoleCallTDDIcon.ToBitmap();
            this.tddButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tddButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.tddButton.Name = "tddButton";
            this.tddButton.Padding = new System.Windows.Forms.Padding(2);
            this.tddButton.Size = new System.Drawing.Size(35, 42);
            this.tddButton.Text = "TDD";
            this.tddButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tddButton.ToolTipText = "Display TDD Window (Ctrl+Shift+F6)";
            this.tddButton.Click += new System.EventHandler(this.btnTDDClick);
            // 
            // phonebookButton
            // 
            this.phonebookButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.phonebookButton.Image = Properties.Resources.ConsolePhonebookIcon.ToBitmap();
            this.phonebookButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.phonebookButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.phonebookButton.Name = "phonebookButton";
            this.phonebookButton.Padding = new System.Windows.Forms.Padding(2);
            this.phonebookButton.Size = new System.Drawing.Size(73, 42);
            this.phonebookButton.Text = "Phonebook";
            this.phonebookButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.phonebookButton.ToolTipText = "Display Phonebook Window (Ctrl+Shift+F7)";
            this.phonebookButton.Click += new System.EventHandler(this.btnPhonebookClick);
            // 
            // alertsButton
            // 
            this.alertsButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.alertsButton.Image = Properties.Resources.AlertIcon.ToBitmap();
            this.alertsButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.alertsButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.alertsButton.Name = "alertsButton";
            this.alertsButton.Padding = new System.Windows.Forms.Padding(2);
            this.alertsButton.Size = new System.Drawing.Size(42, 42);
            this.alertsButton.Text = "Alerts";
            this.alertsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.alertsButton.ToolTipText = "Display Alerts Window (Ctrl+Shift+F8)";
            this.alertsButton.Click += new System.EventHandler(this.btnAlertsClick);
            // 
            // psapButton
            // 
            this.psapButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.psapButton.Image = Properties.Resources.PSAPStatusIcon.ToBitmap();
            this.psapButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.psapButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.psapButton.Name = "psapButton";
            this.psapButton.Padding = new System.Windows.Forms.Padding(2);
            this.psapButton.Size = new System.Drawing.Size(40, 42);
            this.psapButton.Text = "PSAP";
            this.psapButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.psapButton.ToolTipText = "Display PSAP Status Window (Ctrl+Shift+F9)";
            this.psapButton.Visible = false;
            this.psapButton.Click += new System.EventHandler(this.btnPSAPStatusClick);
            // 
            // smsButton
            // 
            this.smsButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.smsButton.Image = Properties.Resources.ConsoleSMSIcon.ToBitmap();
            this.smsButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.smsButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.smsButton.Name = "smsButton";
            this.smsButton.Padding = new System.Windows.Forms.Padding(2);
            this.smsButton.Size = new System.Drawing.Size(34, 42);
            this.smsButton.Text = "SMS";
            this.smsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.smsButton.ToolTipText = "Display SMS Window (Ctrl+Shift+F10)";
            this.smsButton.Click += new System.EventHandler(this.btnSMSClick);
            // 
            // irrButton
            // 
            this.irrButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.irrButton.Image = Properties.Resources.IRRIcon;
            this.irrButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.irrButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.irrButton.Name = "irrButton";
            this.irrButton.Padding = new System.Windows.Forms.Padding(2);
            this.irrButton.Size = new System.Drawing.Size(39, 42);
            this.irrButton.Text = "IRR";
            this.irrButton.ToolTipText = "IRR (Ctrl+Shift+I)";
            this.irrButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.irrButton.ToolTipText = "IRR";
            this.irrButton.Click += new System.EventHandler(this.irrButtonClick);
            // 
            // sendToCADButton
            // 
            this.sendToCADButton.Enabled = false;
            this.sendToCADButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.sendToCADButton.Image = Properties.Resources.ConsoleSendALIToCADIcon.ToBitmap();
            this.sendToCADButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.sendToCADButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.sendToCADButton.Name = "sendToCADButton";
            this.sendToCADButton.Padding = new System.Windows.Forms.Padding(2);
            this.sendToCADButton.Size = new System.Drawing.Size(34, 42);
            this.sendToCADButton.Text = "CAD";
            this.sendToCADButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.sendToCADButton.ToolTipText = "Send ALI to CAD";
            this.sendToCADButton.Click += new System.EventHandler(this.btnSendToCADClick);
            // 
            // sendEraseToCADButton
            // 
            this.sendEraseToCADButton.Enabled = false;
            this.sendEraseToCADButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.sendEraseToCADButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.sendEraseToCADButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.sendEraseToCADButton.Name = "sendEraseToCADButton";
            this.sendEraseToCADButton.Padding = new System.Windows.Forms.Padding(2);
            this.sendEraseToCADButton.Size = new System.Drawing.Size(34, 42);
            this.sendEraseToCADButton.Text = "CAD";
            this.sendEraseToCADButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.sendEraseToCADButton.ToolTipText = "Send Erase ALI to CAD";
            this.sendEraseToCADButton.Click += new System.EventHandler(this.btnSendEraseToCADClick);
            // 
            // RawDisplay
            // 
            this.RawDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.RawDisplay.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RawDisplay.HideSelection = true;
            this.RawDisplay.CausesValidation = false;
            this.RawDisplay.Cursor = System.Windows.Forms.Cursors.Default;
            this.RawDisplay.Font = new System.Drawing.Font("Lucida Console", 14.0F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.RawDisplay.Location = new System.Drawing.Point(3, 72);
            this.RawDisplay.Margin = new Padding(0);
            this.RawDisplay.Multiline = true;
            this.RawDisplay.Name = "RawDisplay";
            this.RawDisplay.ReadOnly = true;
            this.RawDisplay.Size = new System.Drawing.Size(338, 268);
            this.RawDisplay.TabIndex = 14;
            this.RawDisplay.TabStop = false;
            this.RawDisplay.Text = "12345678901234567890123456789012\r\n2\r\n3\r\n4\r\n5\r\n6\r\n7\r\n8\r\n9\r\n0\r\n1\r\n2\r\n3\r\n4\r\n5\r\n6";
            this.RawDisplay.WordWrap = false;
            this.RawDisplay.TextChanged += new System.EventHandler(this.RawDisplay_TextChanged);
            this.RawDisplay.Enter += new System.EventHandler(this.RawDisplay_Enter);
            // 
            // tabWidget
            // 
            this.tabWidget.Controls.Add(this.lineTab);
            this.tabWidget.Controls.Add(this.connectedTab);
            this.tabWidget.Controls.Add(this.abandonedTab);
            this.tabWidget.Controls.Add(this.positionTab);
            this.tabWidget.Controls.Add(this.allTab);
            this.tabWidget.Dock = System.Windows.Forms.DockStyle.Fill;
//            this.tabWidget.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabWidget.HotTrack = true;
            this.tabWidget.Location = new System.Drawing.Point(344, 69);
            this.tabWidget.Margin = new System.Windows.Forms.Padding(0);
            this.tabWidget.Name = "tabWidget";
            this.tabWidget.Padding = new System.Drawing.Point(22, 8);
            this.tabWidget.SelectedIndex = 0;
            this.tabWidget.ShowToolTips = true;
            this.tabWidget.Size = new System.Drawing.Size(600, 200);
            this.tabWidget.TabIndex = 16;
            this.tabWidget.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.drawTab);
            this.tabWidget.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabChanged);
            this.tabWidget.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tabWidget_KeyDown);
//            this.tabWidget.MouseMove += TabWidget_MouseMove;
            // 
            // lineTab
            // 
            this.lineTab.Location = new System.Drawing.Point(4, 25);
            this.lineTab.Margin = new System.Windows.Forms.Padding(0);
            this.lineTab.Name = "lineTab";
            this.lineTab.Size = new System.Drawing.Size(672, 245);
            this.lineTab.TabIndex = 0;
            this.lineTab.Text = "Line Number (0)";
            this.lineTab.UseVisualStyleBackColor = true;
            // 
            // connectedTab
            // 
            this.connectedTab.Location = new System.Drawing.Point(4, 25);
            this.connectedTab.Margin = new System.Windows.Forms.Padding(0);
            this.connectedTab.Name = "connectedTab";
            this.connectedTab.Size = new System.Drawing.Size(672, 245);
            this.connectedTab.TabIndex = 1;
            this.connectedTab.Text = "Connected (0)";
            this.connectedTab.UseVisualStyleBackColor = true;
            // 
            // abandonedTab
            // 
            this.abandonedTab.Location = new System.Drawing.Point(4, 25);
            this.abandonedTab.Margin = new System.Windows.Forms.Padding(0);
            this.abandonedTab.Name = "abandonedTab";
            this.abandonedTab.Size = new System.Drawing.Size(672, 245);
            this.abandonedTab.TabIndex = 2;
            this.abandonedTab.Text = "Abandoned (0)";
            this.abandonedTab.UseVisualStyleBackColor = true;
            // 
            // positionTab
            // 
            this.positionTab.Location = new System.Drawing.Point(4, 25);
            this.positionTab.Margin = new System.Windows.Forms.Padding(0);
            this.positionTab.Name = "positionTab";
            this.positionTab.Size = new System.Drawing.Size(672, 245);
            this.positionTab.TabIndex = 4;
            this.positionTab.Text = "My Position (0)";
            this.positionTab.UseVisualStyleBackColor = true;
            // 
            // allTab
            // 
            this.allTab.Location = new System.Drawing.Point(4, 25);
            this.allTab.Margin = new System.Windows.Forms.Padding(0);
            this.allTab.Name = "allTab";
            this.allTab.Size = new System.Drawing.Size(672, 245);
            this.allTab.TabIndex = 3;
            this.allTab.Text = "All (0)";
            this.allTab.UseVisualStyleBackColor = true;
            // 
            // Console
            // 
            this.ClientSize = new System.Drawing.Size(1024, 344);
            this.Controls.Add(this.toolStripContainer);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Icon = Properties.Resources.ConsoleIcon;
            this.KeyPreview = true;
            this.MainMenuStrip = this.MainMenu;
            this.Name = "Console";
            this.Activated += new System.EventHandler(this.Console_Activated);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.Console_Closing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Enter += new System.EventHandler(this.Console_Enter);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Console_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Console_KeyPress);
            this.toolStripContainer.ContentPanel.ResumeLayout(false);
            this.toolStripContainer.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer.TopToolStripPanel.PerformLayout();
            this.toolStripContainer.ResumeLayout(false);
            this.toolStripContainer.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabWidget.ResumeLayout(false);
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.toolBar.ResumeLayout(false);
            this.toolBar.PerformLayout();
            this.ResumeLayout(false);

        }

        private void setTranslatableText()
        {
            this.MainMenu.Text                    = (Translate)("Main Menu");
            this.fileMenu.Text                    = (Translate)("&File");
            this.printMenu.Text                   = (Translate)("&Print...");
            this.resetMenu.Text                   = (Translate)("&Reset Console...");
            this.debugScriptMenu.Text             = (Translate)("Start Recording &Debug Script...");
            this.closeMenu.Text                   = (Translate)("&Close");
            this.aliMenu.Text                     = (Translate)("&ALI");
            this.rebidALIMenu.Text                = (Translate)("Re&bid");
            this.mapALIMenu.Text                  = (Translate)("&Map");
            this.manualBidMenu.Text               = (Translate)("Manual &Request...");
            this.sendToCADMenu.Text               = (Translate)("&Send to CAD");
            this.sendEraseToCADMenu.Text          = (Translate)("Send &Erase to CAD");
            this.callMenu.Text                    = (Translate)("&Call");
            this.abandonedMenu.Text               = (Translate)("A&bandoned");
            this.phonebookMenu.Text               = (Translate)("&Phonebook");
            this.callbackMenu.Text                = (Translate)("&Callback...");
            this.connectMenu.Text                 = (Translate)("&Answer");
            this.bargeMenu.Text                   = (Translate)("&Barge");
            this.flashHookMenu.Text               = (Translate)("&Flash Hook");
            this.holdMenu.Text                    = (Translate)("&Hold");
            this.disconnectMenu.Text              = (Translate)("Han&g-Up...");
            this.participantMenu.Text             = (Translate)("Participants");
            this.muteMenu.Text                    = (Translate)("&Mute");
            this.volumeMenu.Text                  = (Translate)("&Volume");
            this.tddMenu.Text                     = (Translate)("T&DD");
            this.smsMenu.Text                     = (Translate)("&SMS");
            this.irrMenu.Text                     = (Translate)("&IRR");
            this.colorEditorMenu.Text             = (Translate)("&Choose Application Colors");
            this.radioStateRadioMenu.Text         = (Translate)("Switch to &Radio");
            this.radioStateTelephoneMenu.Text     = (Translate)("Switch to &Call");
            this.forceDisconnectMenu.Text         = (Translate)("&Force Disconnect...");
            this.toolsMenu.Text                   = (Translate)("&Tools");
            this.psapStatusMenu.Text              = (Translate)("PSAP &Status");
            this.alertsMenu.Text                  = (Translate)("&Alerts");
            this.helpMenu.Text                    = (Translate)("&Help");
            this.aboutMenu.Text                   = (Translate)("&About XXX");
            this.usersManualMenu.Text             = (Translate)("&User Manual");
            this.alertMsgMenu.Text                = (Translate)("Alert");
            this.printButton.Text                 = (Translate)("&Print");
            this.printButton.ToolTipText          = (Translate)("Print ALI") + " (Ctrl+P)";
            this.rebidALIButton.Text              = (Translate)("ALI");
            this.rebidALIButton.ToolTipText       = (Translate)("Rebid ALI") + " (Ctrl+Shift+R)";
            this.mapALIButton.Text                = (Translate)("Map");
            this.mapALIButton.ToolTipText         = (Translate)("Map ALI");
            this.abandonedButton.Text             = (Translate)("Abandoned");
            this.abandonedButton.ToolTipText      = (Translate)("Clear Abandoned Call") + " (Ctrl+Shift+D)";
            this.phonebookButton.Text             = (Translate)("Phonebook");
            this.phonebookButton.ToolTipText      = (Translate)("Display Phonebook Window") + " (Ctrl+Shift+F7)";
            this.callbackButton.Text              = (Translate)("Callback");
            this.callbackButton.ToolTipText       = (Translate)("Callback Last Outbound Call") + " (Ctrl+Shift+C)";
            this.connectButton.Text               = (Translate)("Answer");
            this.connectButton.ToolTipText        = (Translate)("Connect to Call") + " (Ctrl+Shift+A)";
            this.bargeButton.Text                 = (Translate)("Barge");
            this.bargeButton.ToolTipText          = (Translate)("Barge on Call") + " (Ctrl+Shift+B)";
            this.flashHookButton.Text             = (Translate)("Flash Hook");
            this.flashHookButton.ToolTipText      = (Translate)("Flash Hook") + " (Ctrl+Shift+F)";
            this.holdButton.Text                  = (Translate)("Hold");
            this.holdButton.ToolTipText           = (Translate)("Hold/Resume Connected Call") + " (Ctrl+Shift+H)";
            this.disconnectButton.Text            = (Translate)("Hang-Up");
            this.disconnectButton.ToolTipText     = (Translate)("Disconnect from Call") + " (Ctrl+Shift+G)";
            this.muteButton.Text                  = (Translate)("Mute");
            this.muteButton.ToolTipText           = (Translate)("Mute Connected Call");
            this.volumeButton.Text                = (Translate)("Volume");
            this.volumeButton.ToolTipText         = (Translate)("Volume Controls");
            this.tddButton.Text                   = (Translate)("TDD");
            this.tddButton.ToolTipText            = (Translate)("Display TDD Window") + " (Ctrl+Shift+F6)";
            this.smsButton.Text                   = (Translate)("SMS");
            this.smsButton.ToolTipText            = (Translate)("Display SMS Window") + " (Ctrl+Shift+F10)";
            this.irrButton.Text                   = (Translate)("IRR");
            this.irrButton.ToolTipText            = (Translate)("IRR") + " (Ctrl+Shift+I)";
            this.alertsButton.Text                = (Translate)("Alerts");
            this.alertsButton.ToolTipText         = (Translate)("Display Alerts Window") + " (Ctrl+Shift+F8)";
            this.psapButton.Text                  = (Translate)("PSAP");
            this.psapButton.ToolTipText           = (Translate)("Display PSAP Status Window") + " (Ctrl+Shift+F9)";
            this.sendToCADButton.Text             = (Translate)("CAD");
            this.sendToCADButton.ToolTipText      = (Translate)("Send ALI to CAD");
            this.sendEraseToCADButton.Text        = (Translate)("CAD");
            this.sendEraseToCADButton.ToolTipText = (Translate)("Send Erase ALI to CAD");
            this.lineTab.Text                     = (Translate)("Line Number") + " (0)";
            this.connectedTab.Text                = (Translate)("Connected") + " (0)";
            this.abandonedTab.Text                = (Translate)("Abandoned") + " (0)";
            this.positionTab.Text                 = (Translate)("My Position") + " (0)";
            this.allTab.Text                      = (Translate)("All") + " (0)";
        }

        // Colors from Google Color Guidelines (https://www.google.com/design/spec/style/color.html)
        public static Settings.ColorInfo aliColorInfo =
            new Settings.ColorInfo(Color.White, Color.White, Color.Black, false);

        public static Settings.ColorInfo aliColorInfoPending =
            new Settings.ColorInfo(Color.White, Color.White, Color.Black, false);

        public static Settings.ColorInfo gridBackgroundColorInfo =
            new Settings.ColorInfo(Color.FromArgb(unchecked((int)0xFFE0E0E0)), // Grey 300
                                   Color.FromArgb(unchecked((int)0xFFE0E0E0)), // Grey 300 (transparent)
                                   Color.White, false);

        public class CallRowInfo
        {
            public enum CallType {_911, Admin, Abandoned, ManualBid, Unknown}
            public enum State    {Normal, Hovered, Selected, NumStates, FirstState = Normal}

            public CallRowInfo(CallType _callType, bool _isActive, State _state)
            {
                callType = _callType;
                isActive = _isActive;
                state    = _state;
            }

            public static State getState(bool isSelected, bool isHovered)
            {
                // selected trumps hovered
                return ( ! isSelected && ! isHovered) ? State.Normal
                                                      : (isSelected ? State.Selected
                                                                    : State.Hovered);
            }

            public CallType callType;
            public bool     isActive;
            public State    state;

            public class Comparer : EqualityComparer<CallRowInfo>
            {
                public override bool Equals(CallRowInfo info1, CallRowInfo info2)
                {
                    if (info1 == null && info2 == null) return true;
                    if (info1 == null || info2 == null) return false;

                    return info1.callType == info2.callType &&
                           info1.isActive == info2.isActive &&
                           info1.state    == info2.state;
                }

                public override int GetHashCode(CallRowInfo info)
                {
                    if (info == null) return 0;

                    return (info.callType.GetHashCode() * (info.isActive ? 1 : 2)
                                                        * (info.state + 3).GetHashCode());
                }
            }
        }

        public static Dictionary<CallRowInfo, Settings.ColorInfo> callColorDictionary =
            new Dictionary<CallRowInfo, Settings.ColorInfo>(new CallRowInfo.Comparer());

        private void CustomInitializeComponent()
        {
            this.     BackColor = SystemColors.Window;
            MainMenu. BackColor = SystemColors.Window;
            toolBar.  BackColor = SystemColors.Window;
            line.     BackColor = Color.DodgerBlue;

            if      ( ! aliColorInfo.backgroundColor1.Equals(Color.White)) line.BackColor = aliColorInfo.backgroundColor1;
            else if ( ! aliColorInfo.backgroundColor2.Equals(Color.White)) line.BackColor = aliColorInfo.backgroundColor2;

            this.LineGrid      = new ConsoleGrid("LineNumber");
            this.ConnectedGrid = new ConsoleGrid("Connected");
            this.AbandonedGrid = new ConsoleGrid("Abandoned");
            this.PositionGrid  = new ConsoleGrid("Position");
            this.AllGrid       = new ConsoleGrid("All");

            this.onStartupTab = allTab;
            this.onConnectTab = allTab;

            this.toolStripContainer.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.MainMenu.SuspendLayout();
            this.toolBar.SuspendLayout();
            this.tabWidget.SuspendLayout();
            this.lineTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LineGrid)).BeginInit();
            this.connectedTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ConnectedGrid)).BeginInit();
            this.abandonedTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AbandonedGrid)).BeginInit();
            this.positionTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PositionGrid)).BeginInit();
            this.allTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AllGrid)).BeginInit();
            this.SuspendLayout();

            this.lineTab.Controls.Add(this.LineGrid);

            // LineGrid
            this.LineGrid.BackgroundColor = Color.LightGray;
            this.LineGrid.Name = "LineGrid";
            this.LineGrid.Size = new System.Drawing.Size(652, 242);
            this.LineGrid.ShowCellToolTips = false;
            this.LineGrid.TabIndex = 0;
            this.LineGrid.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler(this.Grid_CellContextMenuStripNeeded);
            this.LineGrid.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Grid_CellMouseClick);
            this.LineGrid.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grid_CellMouseEnter);
            this.LineGrid.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grid_CellMouseLeave);
            this.LineGrid.CellMouseMove += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Grid_CellMouseMove);
            this.LineGrid.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.PaintGridCell);
            this.LineGrid.RowPrePaint += PaintGridRowBackground;
            this.LineGrid.RowPostPaint += PostPaintGridRow;

            this.connectedTab.Controls.Add(this.ConnectedGrid);

            // ConnectedGrid
            this.ConnectedGrid.BackgroundColor = Color.LightGray;
            this.ConnectedGrid.Name = "ConnectedGrid";
            this.ConnectedGrid.Size = new System.Drawing.Size(652, 242);
            this.ConnectedGrid.ShowCellToolTips = false;
            this.ConnectedGrid.TabIndex = 0;
            this.ConnectedGrid.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler(this.Grid_CellContextMenuStripNeeded);
            this.ConnectedGrid.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Grid_CellMouseClick);
            this.ConnectedGrid.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grid_CellMouseEnter);
            this.ConnectedGrid.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grid_CellMouseLeave);
            this.ConnectedGrid.CellMouseMove += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Grid_CellMouseMove);
            this.ConnectedGrid.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.PaintGridCell);
            this.ConnectedGrid.RowPrePaint += PaintGridRowBackground;
            this.ConnectedGrid.RowPostPaint += PostPaintGridRow;

            this.abandonedTab.Controls.Add(this.AbandonedGrid);

            // AbandonedGrid
            this.AbandonedGrid.BackgroundColor = Color.LightGray;
            this.AbandonedGrid.Name = "AbandonedGrid";
            this.AbandonedGrid.Size = new System.Drawing.Size(652, 242);
            this.AbandonedGrid.ShowCellToolTips = false;
            this.AbandonedGrid.TabIndex = 0;
            this.AbandonedGrid.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler(this.Grid_CellContextMenuStripNeeded);
            this.AbandonedGrid.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Grid_CellMouseClick);
            this.AbandonedGrid.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grid_CellMouseEnter);
            this.AbandonedGrid.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grid_CellMouseLeave);
            this.AbandonedGrid.CellMouseMove += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Grid_CellMouseMove);
            this.AbandonedGrid.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.PaintGridCell);
            this.AbandonedGrid.RowPrePaint += PaintGridRowBackground;
            this.AbandonedGrid.RowPostPaint += PostPaintGridRow;

            this.positionTab.Controls.Add(this.PositionGrid);

            // PositionGrid
            this.PositionGrid.BackgroundColor = Color.LightGray;
            this.PositionGrid.Name = "PositionGrid";
            this.PositionGrid.Size = new System.Drawing.Size(652, 242);
            this.PositionGrid.ShowCellToolTips = false;
            this.PositionGrid.TabIndex = 0;
            this.PositionGrid.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler(this.Grid_CellContextMenuStripNeeded);
            this.PositionGrid.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Grid_CellMouseClick);
            this.PositionGrid.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grid_CellMouseEnter);
            this.PositionGrid.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grid_CellMouseLeave);
            this.PositionGrid.CellMouseMove += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Grid_CellMouseMove);
            this.PositionGrid.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.PaintGridCell);
            this.PositionGrid.RowPrePaint += PaintGridRowBackground;
            this.PositionGrid.RowPostPaint += PostPaintGridRow;

            this.allTab.Controls.Add(this.AllGrid);

            // AllGrid
            this.AllGrid.BackgroundColor = Color.LightGray;
            this.AllGrid.Name = "AllGrid";
            this.AllGrid.Size = new System.Drawing.Size(652, 242);
            this.AllGrid.ShowCellToolTips = false;
            this.AllGrid.TabIndex = 0;
            this.AllGrid.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler(this.Grid_CellContextMenuStripNeeded);
            this.AllGrid.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Grid_CellMouseClick);
            this.AllGrid.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grid_CellMouseEnter);
            this.AllGrid.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grid_CellMouseLeave);
            this.AllGrid.CellMouseMove += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Grid_CellMouseMove);
            this.AllGrid.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.PaintGridCell);
            this.AllGrid.RowPrePaint += PaintGridRowBackground;
            this.AllGrid.RowPostPaint += PostPaintGridRow;

            this.toolStripContainer.ResumeLayout(false);
            this.toolStripContainer.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.toolBar.ResumeLayout(false);
            this.toolBar.PerformLayout();
            this.tabWidget.ResumeLayout(false);
            this.lineTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LineGrid)).EndInit();
            this.connectedTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ConnectedGrid)).EndInit();
            this.abandonedTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AbandonedGrid)).EndInit();
            this.positionTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PositionGrid)).EndInit();
            this.allTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AllGrid)).EndInit();
            this.ResumeLayout(false);
        }

        public void Initialize()
        {
DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    dataModel = controller.dataModel;

                    object showLineViewObj = controller.settings.GeneralSettings["TabSetup.ShowLineViewTab"];
                    bool   showLineView    = (showLineViewObj != null ? toBoolean(showLineViewObj) : false);

                    LineGrid.      Initialize(this, dataModel, showLineView);
                    ConnectedGrid. Initialize(this, dataModel, showLineView);
                    AbandonedGrid. Initialize(this, dataModel, showLineView);
                    PositionGrid.  Initialize(this, dataModel, showLineView);
                    AllGrid.       Initialize(this, dataModel, showLineView);

                    LineGrid.      getBindingSource().Filter = LineFilter;
                    ConnectedGrid. getBindingSource().Filter = ConnectedFilter;
                    AbandonedGrid. getBindingSource().Filter = AbandonedFilter;
                    PositionGrid.  getBindingSource().Filter = PositionFilter;
                    AllGrid.       getBindingSource().Filter = AllFilter;

                    LineGrid.getBindingSource().Sort = "LineNumber";

                    RawDisplay.Clear();
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
        }

        public enum ActiveIncludes {OnHold, OffHold}

        public DataRowView getActiveCall(ActiveIncludes activeIncludes)
        {
DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    foreach (DataGridViewRow GRow in ConnectedGrid.Rows)
                    {
                        if (GRow != null && GRow.DataBoundItem != null)
                        {
                            DataRowView row = (DataRowView)GRow.DataBoundItem;

                            bool isActive = (activeIncludes == ActiveIncludes.OnHold);

                            if ( ! isActive)
                            {
                                CallState callState = (CallState)Convert.ToInt32(row["CallStateCode"]);
                                isActive = (callState != CallState.OnHold);
                            }

                            // if this call is active
                            if (isActive)
                            {
                                if (row["PositionsActive"].ToString().Contains("\tP" + CurrentPosition.ToString() + "\t"))
                                {
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
                                    return row;
                                }
                            }
                        }
                    }
                }
                catch {}
            }

DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
            return null;
        }

        private ConsoleGrid getCurrentGrid()
        {
            if (tabWidget.SelectedTab != null && tabWidget.SelectedTab.Controls.Count > 0)
            {
                return (ConsoleGrid)(tabWidget.SelectedTab.Controls[0]);
            }

            return null;
        }

        public DataRowView getCurrentRow()
        {
DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    ConsoleGrid currentGrid = getCurrentGrid();

                    DataGridViewRow row = (currentGrid != null) ? getCurrentGrid().CurrentRow : null;

                    if (row != null && row.DataBoundItem != null)
                    {
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
                        return (DataRowView)row.DataBoundItem;
                    }
                }
                catch {}
            }

DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
            return null;
        }

        public DataRowView getRow(string callId)
        {
DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    foreach (DataGridViewRow GRow in ConnectedGrid.Rows)
                    {
                        if (GRow != null && GRow.DataBoundItem != null)
                        {
                            DataRowView row = (DataRowView)GRow.DataBoundItem;

                            if (row["Call_ID"].ToString() == callId)
                            {
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
                                return row;
                            }
                        }
                    }
                }
                catch {}
            }

DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
            return null;
        }

        public void CheckForRowUpdate(MessageInfo messageInfo)
        {
            try
            {   // TODO threads: maybe some should be Invokes...look into this more
                BeginInvoke(new checkForRowUpdateDelegate(checkForRowUpdate), messageInfo);
            }
            catch {}
        }

        private delegate void checkForRowUpdateDelegate(MessageInfo messageInfo);

        private void checkForRowUpdate(MessageInfo messageInfo)
        {
            switch (messageInfo.psapMessage.Event)
            {
                case "ClearConsole":
                {
                    if (Convert.ToInt32(messageInfo.psapMessage.Position) == CurrentPosition)
                    {
                        RawDisplay.Clear();
                        controller.clearConsole();

                        addBlankLineRows();
                    }

                    break;
                }
                case "KillSocket":
                {
                    Thread killSocketThread = new Thread(new ThreadStart(killSocketProcess));
                    killSocketThread.Name = "Kill Socket Thread";
                    killSocketThread.Start();

                    return;
                }
                case "PSAPappletData":
                {
                    try
                    {
                        if (messageInfo.psapMessage.psapStatusList != null)
                        {
                            foreach (PSAPStatusInfo info in messageInfo.psapMessage.psapStatusList)
                            {
                                dataModel.AddPSAPStatusEvent(info.name, info.positionsOnline, info.positionsOn911, info.positionsOnAdmin);
                            }
                        }

                        controller.updatePSAPStatus();
                    }
                    catch {}

                    break;
                }
                case "Alarm":
				{
                    // only send "Alert" messages to the Alert window, "Popup" and "Shutdown" don't
					if (dataModel != null && messageInfo.psapMessage.AlarmType == "Alert")
                    {
                        dataModel.AddAlarm(
                            messageInfo.psapMessage.EventDateTime, messageInfo.psapMessage.AlarmText);
                    }

					break;
				}
				case "DeleteRecord":
				{
                    if (dataModel != null) dataModel.RemovePsapEvent(messageInfo.psapMessage);
					break;
				}
            }

            LineGrid.      checkForRowUpdate(messageInfo);
            ConnectedGrid. checkForRowUpdate(messageInfo);
            AbandonedGrid. checkForRowUpdate(messageInfo);
            PositionGrid.  checkForRowUpdate(messageInfo);
            AllGrid.       checkForRowUpdate(messageInfo);

DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    updateTabCounts();

                    processCall(messageInfo.psapMessage.Call_ID,
                        (CallState)Convert.ToInt32(messageInfo.psapMessage.CallStateCode),
                        Convert.ToUInt16(messageInfo.psapMessage.Trunk),
                        Convert.ToUInt16(messageInfo.psapMessage.LineNumber),
                        messageInfo.psapMessage.ConferenceNumber);
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
        }

        private void killSocketProcess()
        {
            handleLogOff();

            Thread.Sleep(2000);

            System.Windows.Forms.MessageBox.Show((Translate)("Multiple users are running") + " \"" + Application.ProductName +
                "\" " + (Translate)("on this workstation.") + "\n\n" + (Translate)("Please select OK to continue."), Application.ProductName,
                MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1,
                MessageBoxOptions.ServiceNotification);

            handleLogOn();
        }

        private void processCall(string callId, CallState callState, ushort trunk, ushort lineNumber, string conferenceNumber)
        {
            if (dataModel != null)
            {
                bool foundRinging911Call   = false;
                bool foundRingingAdminCall = false;

                object activeCallObj = dataModel.ds.Tables["CallEvents"].Compute("Count(Call_ID)",
                        "CallStateCode = " + CallState.Dialing.   GetHashCode().ToString() +
                    " OR CallStateCode = " + CallState.Connected. GetHashCode().ToString() +
                    " OR CallStateCode = " + CallState.OnHold.    GetHashCode().ToString());

                // if no active calls, check for ringing calls, otherwise stop all ring tones
                if (((int)activeCallObj) == 0)
                {
                    DataRow[] ringingCallRows = dataModel.ds.Tables["CallEvents"].Select(
                        "CallStateCode = " + CallState.Ringing.GetHashCode().ToString());

                    for (int i = 0; i != ringingCallRows.Length; ++i)
                    {
                        try
                        {
                            DataRow ringingRow = ringingCallRows[i];

                            try
                            {
                                object callDurationObj = ringingRow["CallDuration"];

                                if (callDurationObj != DBNull.Value && ((TimeSpan)callDurationObj).TotalSeconds > 60.0) continue;
                            }
                            catch {}

                            string ringingCallTypeStr = ringingRow["CallType"].ToString();

                            if (ringingCallTypeStr.Contains((Translate)("9-1-1")) ||
                                ringingCallTypeStr.Contains((Translate)("911")))
                            {
                                foundRinging911Call = true;
                                break;
                            }
                            else
                            {
                                foundRingingAdminCall = true;
                            }
                        }
                        catch {}
                    }
                }

                if (foundRinging911Call)
                {
                    if (ringingAdminCall) ringingAdminPlayer.Stop();
                    ringingAdminCall = false;

                    if ( ! ringing911Call)
                    {
                        ringing911Player.PlayLooping();
                        ringing911Call = true;
                    }
                }
                else if (foundRingingAdminCall)
                {
                    if (ringing911Call) ringing911Player.Stop();
                    ringing911Call = false;

                    if ( ! ringingAdminCall)
                    {
                        ringingAdminPlayer.PlayLooping();
                        ringingAdminCall = true;
                    }
                }
                else
                {
                    stopAllRingTones();
                }
            }

            // loop over all rows (disconnecting conflicting calls)
            switch (callState)
            {
                case CallState.Ringing:   case CallState.Dialing:
                case CallState.Connected: case CallState.OnHold:
                {
                    foreach (DataGridViewRow GRow in ConnectedGrid.Rows)
                    {
                        if ((GRow != null) && (GRow.DataBoundItem != null))
                        {
                            DataRowView row = (DataRowView) GRow.DataBoundItem;

                            // if this is not the same callId, but is on the same trunk or line or conference number, make sure it's not active
                            if (row["Call_ID"].ToString() != callId &&
                                ((lineNumber != 0 && (ushort)row["LineNumber"] == lineNumber) ||
                                 (row["ConferenceNumber"].ToString() == conferenceNumber)))
                            {
                                CallState rowCallState = (CallState)Convert.ToInt32(row["CallStateCode"]);

                                // if this row is not disconneted, disconnect it
                                switch (rowCallState)
                                {
                                    case CallState.Ringing:   case CallState.Dialing:
                                    case CallState.Connected: case CallState.OnHold:
                                    {
                                        string statusStr = row["Status"].ToString();
                                        string firstRow  = statusStr;

                                        int newLineIndex = statusStr.IndexOf('\n');
                                        if (newLineIndex != -1) firstRow = statusStr.Substring(0, newLineIndex);

                                        statusStr = statusStr.Replace(firstRow, (Translate)("Disconnected"));

                                        string callData = "<CallData Call-ID=\"" + row["Call_ID"].ToString() + "\" " +
                                            "ConferenceNumber=\"0\" CallStatus=\"" + statusStr + "\" " +
                                            "CallStatusCode=\"" + StatusCode.Disconnected.GetHashCode().ToString() + "\" " +
                                            "CallStateCode=\""  + CallState.Disconnected.GetHashCode().ToString()  + "\" " +
                                            "LineNumber=\"" + row["LineNumber"].ToString() + "\" />";

                                        string windowData = "<WindowData CADButton=\"off\" EraseCADButton=\"off\" TransferWindow=\"disable\" " +
                                            "TDDWindow=\"disable\" SMSWindow=\"disable\" ALIButton=\"off\" TDDSendButton=\"off\" SMSSendButton=\"off\" " +
                                            "TakeControlButton=\"off\" FlashHookButton=\"off\" TandemXferButton=\"off\" AttendedXferButton=\"off\" " +
                                            "BlindXferButton=\"off\" ConferenceXferButton=\"off\" FlashTransferButton=\"off\" />";

                                        controller.XmlMessageServer.ProcessMessage(
                                            "<?xml version=\"1.0\" encoding=\"utf-8\"?><Event id=\"\" XMLspec=\"" +
                                            XMLMessage.SPEC_VERSION + "\" ><CallInfo> " + callData + windowData +
                                            " </CallInfo></Event>");

                                        break;
                                    }
                                }
                            }
                        }
                    }

                    break;
                }
            }

            // if this position just disconnected, go to the disconnect tab
            bool hasActiveCall = (getActiveCall(ActiveIncludes.OnHold) != null);

            if (waitingToDisconnect && ! hasActiveCall)
            {
                if (onDisconnectTab != null) tabWidget.SelectedTab = onDisconnectTab;

                if (tabWidget.SelectedTab != null) tabWidget.SelectedTab.VerticalScroll.Value = 0;
            }

            waitingToDisconnect = hasActiveCall;
        }

        private void stopAllRingTones()
        {
            try {ringing911Player.   Stop(); ringing911Call   = false;} catch {}
            try {ringingAdminPlayer. Stop(); ringingAdminCall = false;} catch {}
        }

        private void markAllLiveCallsUnknown()
        {
            stopAllRingTones();

DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    DataRow[] liveCallRows = dataModel.ds.Tables["CallEvents"].Select(
                            "CallStateCode = " + CallState.Ringing.   GetHashCode().ToString() +
                        " OR CallStateCode = " + CallState.Dialing.   GetHashCode().ToString() +
                        " OR CallStateCode = " + CallState.Connected. GetHashCode().ToString() +
                        " OR CallStateCode = " + CallState.OnHold.    GetHashCode().ToString());

                    foreach (DataRow row in liveCallRows)
                    {
                        string statusStr = row["Status"].ToString();
                        string firstRow  = statusStr;

                        int newLineIndex = statusStr.IndexOf('\n');
                        if (newLineIndex != -1) firstRow = statusStr.Substring(0, newLineIndex);

                        statusStr = statusStr.Replace(firstRow, (Translate)("Unknown"));

                        string callData = "<CallData Call-ID=\"" + row["Call_ID"].ToString() + "\" " +
                            "ConferenceNumber=\"0\" CallStatus=\"" + statusStr + "\" " +
                            "CallStatusCode=\"" + StatusCode.Unknown.GetHashCode().ToString() + "\" " +
                            "CallStateCode=\""  + CallState.Undefined.GetHashCode().ToString()  + "\" " +
                            "LineNumber=\"" + row["LineNumber"].ToString() + "\" />";

                        string windowData = "<WindowData CADButton=\"off\" EraseCADButton=\"off\" TransferWindow=\"disable\" " +
                            "TDDWindow=\"disable\" SMSWindow=\"disable\" ALIButton=\"off\" TDDSendButton=\"off\" SMSSendButton=\"off\" " +
                            "TakeControlButton=\"off\" FlashHookButton=\"off\" TandemXferButton=\"off\" AttendedXferButton=\"off\" " +
                            "BlindXferButton=\"off\" ConferenceXferButton=\"off\" FlashTransferButton=\"off\" />";

                        controller.XmlMessageServer.ProcessMessage(
                            "<?xml version=\"1.0\" encoding=\"utf-8\"?><Event id=\"\" XMLspec=\"" +
                            XMLMessage.SPEC_VERSION + "\" ><CallInfo> " + callData + windowData +
                            " </CallInfo></Event>");
                    }
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
        }

        public void GridSelectionChanged(DataRowView row)
        {
            try
            {
                Invoke(new gridSelectionChangedDelegate(gridSelectionChanged), row);
            }
            catch {try {gridSelectionChanged(row);} catch {}}
        }

        private delegate void gridSelectionChangedDelegate(DataRowView row);

        private void gridSelectionChanged(DataRowView row)
        {
            try
            {
                BeginInvoke(new UpdateALIDisplayDelegate(UpdateALIDisplay));
            }
            catch {}

DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    row = getCurrentRow();

                    bool positionIsOnCall =
                        (row != null && row["PositionsActive"].ToString().Contains("\tP" + CurrentPosition.ToString() + "\t"));

                    bool positionIsOnHold =
                        (row != null && row["PositionsOnHold"].ToString().Contains("\tP" + CurrentPosition.ToString() + "\t"));

                    CallState callState = (row != null) ? (CallState)Convert.ToInt32(row["CallStateCode"]) : CallState.Undefined;

                    MessageWindow tdd = controller.getTDD();
                    MessageWindow sms = controller.getSMS();

                    string callId = null;

                    if (row != null)
                    {
                        callId = row["Call_ID"].ToString();

                        tdd.setCurrentCallId(callId);
                        sms.setCurrentCallId(callId);

                        string latStr, lonStr;

                        string aliRecord = row["ALIRecord"].ToString();

                        if (row["Encoding_ALI"].ToString() == "Base64") aliRecord = Utility.DecodeBase64(aliRecord);

                        ButtonEnabled(rebidALIButton,       toBoolean(row["ALIButton"]));
                        ButtonEnabled(mapALIButton,         parseTextForLatLon(aliRecord, out latStr, out lonStr));
                        ButtonEnabled(sendToCADButton,      toBoolean(row["CADButton"]));
                        ButtonEnabled(sendEraseToCADButton, toBoolean(row["EraseCADButton"]));
                        ButtonEnabled(abandonedButton,      toBoolean(row["TakeControlButton"]));
                        ButtonEnabled(flashHookButton,      toBoolean(row["FlashHookButton"]));
                    }
                    else
                    {
                        ButtonEnabled(rebidALIButton,       false);
                        ButtonEnabled(mapALIButton,         false);
                        ButtonEnabled(sendToCADButton,      false);
                        ButtonEnabled(sendEraseToCADButton, false);
                        ButtonEnabled(abandonedButton,      false);
                    }

/* TODO* messages: implement KillChannel and Volume

from 6.71

<?xml version="1.0" encoding="utf-8"?>
<Event id="" XMLspec="6.69">
    <KillChannel Participant ="D2" Call-ID="123456789012345678" />
</Event>

<?xml version="1.0" encoding="utf-8"?>
<Event id="" XMLspec="6.65">
    <Volume Position="2" value="up/down"/>
</Event>

*/


                    bool enableTransfer = row != null && toBoolean(row["TransferWindow"]);
                    bool enableTDD      = row != null && toBoolean(row["TDDWindow"]);
                    bool enableSMS      = row != null && toBoolean(row["SMSWindow"]);

                    if (enableTransfer || enableTDD || enableSMS)
                    {
                        // if this position is NOT on this call or this call is on hold, disable Phonebook, TDD, and SMS
                        if ( ! positionIsOnCall || positionIsOnHold) enableTransfer = enableTDD = enableSMS = false;
                    }

                    Phonebook phonebook = controller.phonebook;

                    if (phonebook != null)
                    {
                        phonebook.blindEnabled      = row != null && toBoolean(row["BlindXferButton"]);
                        phonebook.attendedEnabled   = row != null && toBoolean(row["AttendedXferButton"]);
                        phonebook.conferenceEnabled = row != null && toBoolean(row["ConferenceXferButton"]);
                        phonebook.tandemEnabled     = row != null && toBoolean(row["TandemXferButton"]);
                        phonebook.flashEnabled      = row != null && toBoolean(row["FlashTransferButton"]);

                        phonebook.setEnabled(enableTransfer, callId);

                        foreach (ToolStrip speedDialToolBar in speedDialToolBarDictionary.Values)
                        {
                            foreach (ToolStripItem speedDialButton in speedDialToolBar.Items)
                            {
                                PhonebookEntry phonebookEntry = (PhonebookEntry)speedDialButton.Tag;

                                bool enabled = (enableTransfer || phonebookEntry.SpeedDialAction == Controller.TransferType.Dial);

                                switch (phonebookEntry.SpeedDialAction)
                                {
                                    case Controller.TransferType.Tandem:     if ( ! phonebook.tandemEnabled)     enabled = false; break;
                                    case Controller.TransferType.Attended:   if ( ! phonebook.attendedEnabled)   enabled = false; break;
                                    case Controller.TransferType.Conference: if ( ! phonebook.conferenceEnabled) enabled = false; break;
                                    case Controller.TransferType.Blind:      if ( ! phonebook.blindEnabled)      enabled = false; break;
                                    case Controller.TransferType.FlashHook:  if ( ! phonebook.flashEnabled)      enabled = false; break;
                                }

                                speedDialButton.Enabled = enabled;
                            }
                        }
                    }

                    tdd.setEnabled(enableTDD);
                    sms.setEnabled(enableSMS);

                    tdd.SendButtonEnabled = row != null && toBoolean(row["TDDSendButton"]);
                    sms.SendButtonEnabled = row != null && toBoolean(row["SMSSendButton"]);

                    bool hasActiveCall  = (getActiveCall(ActiveIncludes.OffHold) != null);
                    bool hasRingingCall = false;

                    if (dataModel != null)
                    {
                        object ringingCallObj = dataModel.ds.Tables["CallEvents"].Compute("Count(Call_ID)",
                            "CallStateCode = " + CallState.Ringing.GetHashCode().ToString());

                        hasRingingCall = ((int)ringingCallObj) != 0;
                    }

                    // TODO gui: Decide whether Hold should be a toggle or a button

                    ButtonEnabled(connectButton,    hasRingingCall);
                    ButtonEnabled(disconnectButton, positionIsOnCall && ! positionIsOnHold);
                    ButtonEnabled(holdButton,       positionIsOnCall);
                    ButtonEnabled(flashHookButton,  positionIsOnCall);
                    ButtonEnabled(bargeButton,      ! positionIsOnCall && (callState == CallState.Connected    || callState == CallState.OnHold));
                    ButtonEnabled(callbackButton,   ! hasActiveCall    && (callState == CallState.Abandoned    || callState == CallState.AbandonedCleared ||
                                                                           callState == CallState.Disconnected || callState == CallState.Canceled ||
                                                                           callState == CallState.ManualBid));
                }
                catch (Exception ex)
                {
                    Logging.AppTrace("Error in Grid Selection Changed:" + ex.Message);
                    Logging.ExceptionLogger("Error in Console.GridSelectionChanged", ex, LogID.Gui);
                }
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
        }

        private void ButtonEnabled(ToolStripButton button, bool Enabled)
        {
            try
            {
                SetButtonEnabledDelegate SetButtonEnabled1 = new SetButtonEnabledDelegate(this.SetButtonEnabled);
                BeginInvoke(SetButtonEnabled1,new object[] {button, Enabled});
            }
            catch {}
        }

        private void SetButtonEnabled(ToolStripItem button, bool Enabled)
        {
            try
            {
                if      (button == printButton)          button.Enabled = printMenu.          Enabled = Enabled;
                else if (button == rebidALIButton)       button.Enabled = rebidALIMenu.       Enabled = controller.noForm.menuRebid.      Enabled = Enabled;
                else if (button == mapALIButton)         button.Enabled = mapALIMenu.         Enabled = Enabled;
                else if (button == sendToCADButton)      button.Enabled = sendToCADMenu.      Enabled = Enabled;
                else if (button == sendEraseToCADButton) button.Enabled = sendEraseToCADMenu. Enabled = Enabled;
                else if (button == abandonedButton)      button.Enabled = abandonedMenu.      Enabled = controller.noForm.menuAbandon.    Enabled = Enabled;
                else if (button == callbackButton)       button.Enabled = callbackMenu.       Enabled = controller.noForm.menuCallback.   Enabled = Enabled;
                else if (button == connectButton)        button.Enabled = connectMenu.        Enabled = controller.noForm.menuConnect.    Enabled = Enabled;
                else if (button == bargeButton)          button.Enabled = bargeMenu.          Enabled = controller.noForm.menuBarge.      Enabled = Enabled;
                else if (button == flashHookButton)      button.Enabled = flashHookMenu.      Enabled = controller.noForm.menuFlashHook.  Enabled = Enabled;
                else if (button == holdButton)           button.Enabled = holdMenu.           Enabled = controller.noForm.menuHold.       Enabled = Enabled;
                else if (button == disconnectButton)     button.Enabled = disconnectMenu.     Enabled = controller.noForm.menuDisconnect. Enabled = Enabled;
                else if (button == muteButton)           button.Enabled = muteMenu.           Enabled = Enabled;
                else if (button == irrButton)            button.Enabled = irrMenu.            Enabled = Enabled;
            }
            catch {}
        }

        public void subdueALIWindow(bool subdue)
        {
            bool doRefresh = (RawDisplay.subdued != subdue);

            RawDisplay.subdued = subdue;
            Settings.ColorInfo colorInfo = subdue ? aliColorInfoPending : aliColorInfo;

            RawDisplay.ForeColor = colorInfo.foregroundColor;

            if ( ! subdue) 
            {
                if ( ! aliColorInfoPending.foregroundColorExplicitAlpha)
                {
                    RawDisplay.ForeColor = Color.FromArgb(100, aliColorInfoPending.foregroundColor);
                }
            }

            if (colorInfo.bold) RawDisplay.Font = new Font(RawDisplay.Font, FontStyle.Bold);
            else                RawDisplay.Font = new Font(RawDisplay.Font, FontStyle.Regular);

            if (doRefresh) RawDisplay.Refresh();
        }

        private delegate void UpdateALIDisplayDelegate();

        private void UpdateALIDisplay()
        {
DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                bool found = false;

                try
                {
                    DataRowView row = getCurrentRow();

                    if (row != null)
                    {
                        bool aliRequested = false;

                        ALIStateCode aliStateCode = (ALIStateCode) Convert.ToInt32(row["ALIStateCode"]);

                        switch (aliStateCode)
                        {
                            case ALIStateCode.ALI_Request:               aliRequested = true; break;
                            case ALIStateCode.ALI_Request_Failed:        aliRequested = true; break;
                            case ALIStateCode.Manual_ALI_Request:        aliRequested = true; break;
                            case ALIStateCode.Manual_ALI_Request_Failed: aliRequested = true; break;
                            case ALIStateCode.Auto_ALI_Request:          aliRequested = true; break;
                            case ALIStateCode.Auto_ALI_Request_Failed:   aliRequested = true; break;
                        }

                        subdueALIWindow(aliRequested);

                        string mime = row["ALIRecord"].ToString();

                        if (mime.Length != 0)
                        {
                            found = true;

                            string rawDisplayText        = mime;
                            string rawDisplayTextWrapped = "";

                            if (row["Encoding_ALI"].ToString() == "Base64") rawDisplayText = Utility.DecodeBase64(mime);

//string str = "SWEET GRASS CNTY AMBULANCE                                       \r                               \r" +
//             "                               \r                               \r                               \r";

//       str += (char)0x005;
//       str += " \r                                          \n           \r                                          \n         ending  \r";
//       str += ETX;
//rawDisplayText = str;

                            // replace ALI separator [0x005] with newline
                            rawDisplayText = rawDisplayText.Replace((char)0x005, '\n').TrimEnd();

                            int nRows        = 1;
                            int nCharsOnLine = 0;

                            bool prevWasCR = false;

                            foreach (char c in rawDisplayText)
                            {
                                if (c == '\n')
                                {
                                    // if the previous char was not <CR>, add it
                                    if ( ! prevWasCR)
                                    {
                                        rawDisplayTextWrapped += '\r';
                                        nCharsOnLine = 0;

                                        ++nRows;
                                    }
                                    else
                                    {
                                        prevWasCR = false;
                                    }
                                }
                                else
                                {
                                    // if the previous char was <CR>, but this is not <LF>, add it
                                    if (prevWasCR) rawDisplayTextWrapped += '\n';

                                    prevWasCR = false;

                                    // ignore NULL, STX, and ETX
                                    switch (c)
                                    {
                                        case (char)0x000: continue;
                                        case (char)0x002: continue;
                                        case (char)0x003: continue;

                                        // remember <CR>
                                        case '\r':

                                            nCharsOnLine = 0;
                                            prevWasCR = true;

                                            ++nRows;
                                            break;

                                        // else if this is a normal character
                                        default:

                                            // if we need to wrap, add a line break
                                            if (nCharsOnLine == 32)
                                            {
                                                rawDisplayTextWrapped += "\r\n";
                                                nCharsOnLine = 0;

                                                ++nRows;
                                            }

                                            nCharsOnLine++;
                                            break;
                                    }
                                }

                                rawDisplayTextWrapped += c;
                            }

                            // if the last char was <CR>, add <LF>
                            if (prevWasCR) rawDisplayTextWrapped += '\n';

                            RawDisplay.Text = rawDisplayTextWrapped;

                            if (nRows <= 16) RawDisplay.ScrollBars = System.Windows.Forms.ScrollBars.None;
                            else             RawDisplay.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
                        }
                    }

                    if ( ! found) RawDisplay.Clear();
                }
                catch (Exception ex)
                {
                    RawDisplay.Clear();

                    Logging.AppTrace("Error in Console.UpdateALIDisplay:" + ex.Message);
                    Logging.ExceptionLogger("Error in Console.UpdateALIDisplay", ex, LogID.Gui);
                }
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
        }

        public void setNumberOfPositions(int nPositions)
        {
            numberOfPositions = nPositions;
        }

        public void setPosition(int newPosition)
        {
            if (newPosition != CurrentPosition)
            {
                CurrentPosition = newPosition;
                controller.XmlMessageServer.setCurrentPosition(newPosition);

                SetTitleBarText();

                PositionFilter = "(PositionsHistory LIKE '*\tP" + CurrentPosition.ToString() + "\t*') OR " +
                                 "(PositionsHistory LIKE '\tP"  + CurrentPosition.ToString() + "\t*') OR " +
                                 "(PositionsHistory LIKE '*\tP" + CurrentPosition.ToString() + "\t')";

                PositionGrid.getBindingSource().Filter = PositionFilter;
            }
        }

        public void Stop()
        {
            try
            {
                Logging.AppTrace("Disposing of Console Timers");

                if (timeTimer != null)
                {
                    timeTimer.Dispose();
                    timeTimer = null;
                }

                if (heartbeatTimer != null)
                {
                    heartbeatTimer.Dispose();
                    heartbeatTimer = null;
                }

                if (msgTimer != null)
                {
                    msgTimer.Dispose();
                    msgTimer = null;
                }

                if (tcpCloseTimer != null)
                {
                    tcpCloseTimer.Dispose();
                    tcpCloseTimer = null;
                }

                if (tcpReconnectTimer != null)
                {
                    tcpReconnectTimer.Dispose();
                    tcpReconnectTimer = null;
                }
            }
            catch (Exception ex)
            {
                Logging.AppTrace("Error Stopping Console: " + ex.ToString());
                Logging.ExceptionLogger("Error in Console.Stop", ex, LogID.Cleanup);
            }
        }

        public string getXMLHeader(string eventIdTag, int eventId)
        {
            return "<?xml version=\"1.0\" encoding=\"utf-8\"?><Event id=\"" +
                eventIdTag + CurrentPosition.ToString().PadLeft(2, '0') +
                eventId + "\" XMLspec=\"" + XMLMessage.SPEC_VERSION +
                "\" WindowsUser=\"" + sessionUsername + "\" >";
        }

        private bool socketAllowedToReconnect = true;

        private bool askIfTheUserWantsToKeepTryingToConnect = true;

        private Thread heartbeatThread              = null;
        private bool   heartbeatMessageBoxIsUp      = false;
        public  static Socket socket                = null;
        private static object socketWriteLock       = new object();
        private static bool   connectionEstablished = false; // set on first message received

        private int nHeartbeatsSkipped = 0;
        private int heartbeatId        = 1;
        private int initMsgId          = 1;

        public static void closeSocket()
        {
            connectionEstablished = false;

            lock (socketWriteLock)
            {
                try {if (socket != null) socket.Close();} catch {}

                socket = null;
            }
        }

        private void SendHeartbeat(object notUsedObj)
        {
            if ( ! sessionActive || ! socketAllowedToReconnect)
            {
                if (heartbeatTimer != null) heartbeatTimer.Change(500, Timeout.Infinite);

                return;
            }

            if (heartbeatTimer != null) heartbeatTimer.Change(heartbeatTimeout, Timeout.Infinite);

            // if the previous heartbeat thread is still running
            if (heartbeatThread != null)
            {
                if (heartbeatMessageBoxIsUp)
                {
                    nHeartbeatsSkipped = 0;
                    return;
                }

                if (++nHeartbeatsSkipped > 6)
                {
Controller.DebugWindowMsg("Aborting " + heartbeatThread.Name + "[" + heartbeatThread.ManagedThreadId + "]");
                    heartbeatThread.Abort();
                }
                else
                {
Controller.DebugWindowMsg("Skipping heartbeat " + nHeartbeatsSkipped + "x (" + heartbeatThread.Name + "[" + heartbeatThread.ManagedThreadId + "] still active)");
                    return;
                }
            }

            heartbeatThread = new Thread(new ThreadStart(sendHeartbeatSeparateThread));
            heartbeatThread.Name = "Heartbeat Thread";

            // check again to make sure nothing has changed
            if ( ! sessionActive || ! socketAllowedToReconnect)
            {
                if (heartbeatTimer != null) heartbeatTimer.Change(500, Timeout.Infinite);

                nHeartbeatsSkipped = 0;
                heartbeatThread = null;
                return;
            }

            heartbeatThread.Start();

            nHeartbeatsSkipped = 0;
        }

        private const char ETX = (char)3;  // end of text

        private System.Threading.Timer smsTestTimer = null;

        private void SendTestSMS(object notused)
        {
            if (controller == null || controller.XmlMessageServer == null) return;

            string callData = "<CallData Call-ID=\"123456789012345673\" />";
            string smsData  = "<SMSdata EncodingSMS=\"Base64\" SMSCallerSays=\"" + Utility.EncodeBase64("21:44:48 (Caller) This is a test" + ETX) + "\" />";

            controller.XmlMessageServer.ProcessMessage(
                "<?xml version=\"1.0\" encoding=\"utf-8\"?><Event id=\"\" XMLspec=\"" +
                XMLMessage.SPEC_VERSION + "\" ><CallInfo> " + callData + smsData + " </CallInfo></Event>");

            if (smsTestTimer == null) smsTestTimer = new System.Threading.Timer(SendTestSMS, null, 2000, 2000);
        }

        private string generateCallData(int i, string posStr, string posPStr, StatusCode statusCode, CallState callState)
        {
            string iStr = i.ToString();

            return "<CallData RingDateTime=\"2015/12/18 22:06:4" + iStr + ".812 UTC\" EventDateTime=\"2015/12/18 22:06:4" + iStr + ".812 UTC\" " +
                   "TransmitDateTime=\"2015/12/18 22:06:4" + iStr + ".812 UTC\" Call-ID=\"12345678901234567" + iStr + "\" Encoding=\"Text\" " +
                   (i % 2 == 0 ? "ALIRecord=\"This is a test on Shoup Rd\n\n+39.012999-104.704891\n\nHere are some special characters\n&lt;&amp;&gt;\" "
                               : "ALIRecord=\"This is a test with the X and Y swapped as well as leading zeros\n\nX = -082.832901  Y = 040.387415\" ") +
                   "Trunk=\"0" + iStr + "\" CallOriginator=\"C1\" " + "Position=\"" + posStr + "\" PositionsActive=\"\tP2\t" + posPStr + "\" " +
                   "PositionsHistory=\"" + posPStr + "\tP2\t\tP11\t" + "\" " + "PositionsOnHold=\"" + (callState == CallState.OnHold ? posPStr : "") + "\" " +
                   "ConferenceNumber=\"" + iStr + "\" Callback=\"303513102" + iStr + "\" CallbackDisplay=\"(303) 513-102" + iStr +
                   "\" CallBackVerified=\"true\" pANI=\"**********\" TDDmute=\"off\" ANIPort=\"01\" CallStatus=\"" + callState.ToString() +
                   "\" CallStatusCode=\"" + statusCode.GetHashCode().ToString() + "\" CallStateCode=\"" + callState.GetHashCode().ToString() +
                   (callState == CallState.ManualBid ? "\" ALIStateCode=\"" + ALIStateCode.Manual_ALI_Request.GetHashCode().ToString() : "") +
                   "\" CallType=\"" + ((i / 2 % 2) == 0 ? "9-1-1 Wireless/VoIP [TDD]" : "Admin Caller") + "\" LineNumber=\"0" + iStr + "\" />";
        }

        private void initDemo()
        {
            heartbeatMessageBoxIsUp = true;

Controller.DebugWindowMsg("  Demo Mode (" + demoModeStr + ")");

            MessageBox.Show(null, "Demo Mode (" + demoModeStr + ") " + getWindowsOS(),
                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information,
                MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);

            heartbeatMessageBoxIsUp = false;

            System.Xml.XmlDocument xd = new System.Xml.XmlDocument();

            // TODO demo: put this into a simple demo script
            string initStr = "<Initialization><General><GeneralSetup NumberOfPositions=\"9\" HeartbeatInterval=\"6\" />" +
                "<AppletSetup ShowPSAPstatusApplet=\"true\" ShowPSAPappletAdmin=\"true\" ShowSMSapplet=\"true\" ShowColorEditorApplet=\"true\" /> " +
                "<ButtonSetup ShowManualRequestButton=\"true\" ShowCADButton=\"true\" ShowTDDbutton=\"true\" " +
                "ShowAlertButton=\"true\" ShowCADeraseButton=\"true\" ShowMapButton=\"true\" ShowPhoneBookButton=\"true\" " +
                "ShowCallBackButton=\"true\" ShowSpeedDialButton=\"true\" ShowDialPadButton=\"true\" " +
                "ShowAnswerButton=\"true\" ShowBargeButton=\"true\" " +
                "ShowAttendedTransferButton=\"true\" ShowTandemTransferButton=\"true\" ShowBlindTransferButton=\"true\" " +
                "ShowConferenceTransferButton=\"true\" ShowFlashTransferButton=\"false\" " +
                "ShowFlashHookButton=\"true\" ShowHoldButton=\"true\" " +
                "ShowHangUpButton=\"true\" ShowMuteButton=\"true\" ShowVolumeButton=\"true\" " + " />" +
                "<TabSetup ShowLineViewTab=\"true\" /></General>";

            initStr += "<TDD>" +
                            "<TDDSetup AutoDetect=\"off\" DefaultLanguage=\"English\" SendDelay=\"2\" SendTextColor=\"blue\" " +
                            "ReceivedTextColor=\"red\" CPSThreshold=\"4\" DispatcherSaysPattern=\"{time} (Position {position-number})\" " +
                            "CallerSaysPrefix=\"{time} (Caller)\" />" +
                            "<TDDMessageList>" +
                            "<TDDMessage Text=\"911 what is your emergency?\" Language=\"English\"/>" +
                            "<TDDMessage Text=\"what is your phone number?\" Language=\"English\"/>" +
                            "</TDDMessageList>" +
                        "</TDD>";

            initStr += "<SMS>" +
                            "<SMSsetup DefaultLanguage=\"English\" SendTextColor=\"blue\" " +
                            "ReceivedTextColor=\"red\" DispatcherSaysPattern=\"{time} (Position {position-number})\" " +
                            "CallerSaysPrefix=\"{time} (Caller)\" />" +
                            "<SMSMessageList>" +
                            "<SMSMessage Text=\"911 what is your emergency?\" Language=\"English\"/>" +
                            "<SMSMessage Text=\"what is your phone number?\" Language=\"English\"/>" +
                            "</SMSMessageList>" +
                        "</SMS>";

            initStr += "<Phonebook>" +
                            "<PhonebookSetup DefaultGroup=\"PSAP\" View=\"BUTTON\" />" +
                            "<PhonebookList>" +
                            "<PhonebookEntry Name=\"Local PSAP\" Number=\"1001\" Group=\"PSAP\" Code=\"0\" />" +
                            "<PhonebookEntry Name=\"Coconino County (CCDC) Intake\" Number=\"*03\" Group=\"PSAP\" Code=\"0\" />" +
                            "<PhonebookEntry Name=\"Bob\" Number=\"bob@psap.com\" Group=\"PSAP\" Code=\"0\" Speed_Dial_Action=\"none\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"5551212\" Group=\"LE Intelligence Centers\" Code=\"0\" icon_file=\"C:\\Dev\\911\\data\\test\\911.ico\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"3035551212\" Group=\"LE Intelligence Centers\" Code=\"0\" icon_file=\"C:\\Dev\\911\\data\\test\\911.ico\" />" +
                            "<PhonebookEntry Name=\"Western Arizona Regional Medical Center Main\" Number=\"13035551212x123\" Group=\"LE Intelligence Centers\" Code=\"0\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"13035551212\" Group=\"LE Intelligence Centers\" Code=\"0\" icon_file=\"C:\\Dev\\911\\data\\test\\911.ico\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"13035551212x123\" Group=\"LE Intelligence Centers\" Code=\"0\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*08\" Group=\"LE Intelligence Centers\" Code=\"0\" Speed_Dial_Action=\"none\" icon_file=\"C:\\Dev\\911\\data\\test\\911.ico\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*08\" Group=\"LE Intelligence Centers\" Code=\"0\" Speed_Dial_Action=\"none\" icon_file=\"C:\\Dev\\911\\data\\test\\911.ico\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*08\" Group=\"LE Intelligence Centers\" Code=\"0\" Speed_Dial_Action=\"none\" icon_file=\"C:\\Dev\\911\\data\\test\\911.ico\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*08\" Group=\"LE Intelligence Centers\" Code=\"0\" Speed_Dial_Action=\"none\" icon_file=\"C:\\Dev\\911\\data\\test\\911.ico\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*08\" Group=\"LE Intelligence Centers\" Code=\"0\" Speed_Dial_Action=\"none\" icon_file=\"C:\\Dev\\911\\data\\test\\911.ico\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*08\" Group=\"LE Intelligence Centers\" Code=\"0\" Speed_Dial_Action=\"none\" icon_file=\"C:\\Dev\\911\\data\\test\\911.ico\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*08\" Group=\"LE Intelligence Centers\" Code=\"0\" Speed_Dial_Action=\"none\" icon_file=\"none\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*08\" Group=\"LE Intelligence Centers\" Code=\"0\" Speed_Dial_Action=\"none\" icon_file=\"none\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*08\" Group=\"LE Intelligence Centers\" Code=\"0\" Speed_Dial_Action=\"none\" icon_file=\"C:\\Dev\\911\\data\\test\\911.ico\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*08\" Group=\"LE Intelligence Centers\" Code=\"0\" Speed_Dial_Action=\"none\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*08\" Group=\"LE Intelligence Centers\" Code=\"0\" Speed_Dial_Action=\"none\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*08\" Group=\"LE Intelligence Centers\" Code=\"0\" Speed_Dial_Action=\"none\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*08\" Group=\"LE Intelligence Centers\" Code=\"0\" Speed_Dial_Action=\"none\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*08\" Group=\"LE Intelligence Centers\" Code=\"0\" Speed_Dial_Action=\"none\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*08\" Group=\"LE Intelligence Centers\" Code=\"0\" Speed_Dial_Action=\"none\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*01\" Group=\"Highway Maint / Emergency\" Code=\"0\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*02\" Group=\"Test 1\" Code=\"2\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*03\" Group=\"Test 2\" Code=\"4\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*04\" Group=\"Test 3\" Code=\"8\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*05\" Group=\"Test 4\" Code=\"16\" Speed_Dial_Action=\"none\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*06\" Group=\"Test 5\" Code=\"32\" Speed_Dial_Action=\"none\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*07\" Group=\"Test 6\" Code=\"64\" Speed_Dial_Action=\"none\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*08\" Group=\"Test 7\" Code=\"6\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*09\" Group=\"Test 8\" Code=\"10\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*10\" Group=\"Test 9\" Code=\"12\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*11\" Group=\"Test 10\" Code=\"14\" Speed_Dial_Action=\"none\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*12\" Group=\"Test 11\" Code=\"20\" Speed_Dial_Action=\"none\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*13\" Group=\"Test 12\" Code=\"24\" Speed_Dial_Action=\"none\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*14\" Group=\"Test 13\" Code=\"18\" Speed_Dial_Action=\"none\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*15\" Group=\"Test 14\" Code=\"22\" Speed_Dial_Action=\"none\" />" +
                            "<PhonebookEntry Name=\"Test\" Number=\"*16\" Group=\"Test 15\" Code=\"28\" Speed_Dial_Action=\"none\" />" +
//                          "<PhonebookEntry Name=\"Local PSAP\"   Number=\"1001\"            icon_file=\"\\\\192.168.60.206/Dev/Elliott/WestTel.ico\" Group=\"Speed Dial\" Code=\"0\" Speed_Dial_Action=\"dial_out\" />" +
                            "<PhonebookEntry Name=\"Coconino Co\"  Number=\"*03\"             icon_file=\"C:\\Dev\\911\\data\\icons\\Updated GUI Icons with SMS icon\\Icons - PNG\\24\\Console - Phonebook (24x24).png\" Group=\"Speed Dial\" Code=\"0\" Speed_Dial_Action=\"blind_transfer\" />" +
                            "<PhonebookEntry Name=\"Bob\"          Number=\"bob@psap.com\"    icon_file=\"C:\\Dev\\911\\data\\icons\\Updated GUI Icons with SMS icon\\Icons - PNG\\24\\Console - Phonebook (24x24).png\" Group=\"Speed Dial\" Code=\"0\" Speed_Dial_Action=\"attended_transfer\" />" +
                            "<PhonebookEntry Name=\"Test1\"        Number=\"5551212\"         icon_file=\"C:\\Dev\\911\\data\\icons\\Updated GUI Icons with SMS icon\\Icons - PNG\\24\\Console - Phonebook (24x24).png\" Group=\"Speed Dial\" Code=\"0\" Speed_Dial_Action=\"conference_transfer\" />" +
                            "<PhonebookEntry Name=\"\"             Number=\"3035551212\"      icon_file=\"C:\\Dev\\911\\data\\icons\\Updated GUI Icons with SMS icon\\Icons - PNG\\24\\Console - Phonebook (24x24).png\" Group=\"Speed Dial 2\" Code=\"0\" Speed_Dial_Action=\"tandem_transfer\" />" +
                            "<PhonebookEntry Name=\"W AZ Medical\" Number=\"13035551212x123\" icon_file=\"C:\\Dev\\911\\data\\icons\\Updated GUI Icons with SMS icon\\Icons - PNG\\24\\Console - Phonebook (24x24).png\" Group=\"Speed Dial 2\" Code=\"0\" Speed_Dial_Action=\"dial_out\" />" +
                            "<PhonebookEntry Name=\"Test3\"        Number=\"13035551212\"     Group=\"Speed Dial\" Code=\"0\" Speed_Dial_Action=\"dial_out\" />" +
                            "<PhonebookEntry Name=''               Number=\"13035551212x123\" icon_file=\"C:\\Dev\\911\\data\\icons\\Updated GUI Icons with SMS icon\\Icons - PNG\\24\\Console - Phonebook (24x24).png\" Group=\"Speed Dial\" Code=\"0\" Speed_Dial_Action=\"dial_out\" />" +
                            "</PhonebookList>" +
                            "<GroupList>" +
                            "<GroupEntry Name=\"PSAP\" Priority=\"2\" icon_file=\"C:\\Dev\\911\\data\\icons\\Updated GUI Icons with SMS icon\\Icons - PNG\\24\\Console - Phonebook (24x24).png\" />" +
                            "<GroupEntry Name=\"LE Intelligence Centers\" Priority=\"1\" />" +
                            "</GroupList>" +
                        "</Phonebook>";

            initStr += "<LineView>";

            for (int i = 1; i <= 10; ++i)
            {
                initStr += "<LineViewRow Row=\"" + i.ToString() + "\" Display=\"Line " + i.ToString() + " - 9-1-1\" />";
            }

            initStr += "</LineView></Initialization>";

            xd.LoadXml("<?xml version=\"1.0\" encoding=\"utf-8\"?><Event id=\"\" XMLspec=\"" +
                XMLMessage.SPEC_VERSION + "\" >" + initStr + "</Event>");

            controller.XmlMessageServer_SettingsReceived(xd);

            if (dataModel != null)
            {
                const int nCalls = 10;

                for (int i = 1; i != nCalls; ++i)
                {
                    string pStr    = i.ToString();
                    string posPStr = "\tC1\t\tP" + pStr + '\t';

                    StatusCode statusCode = StatusCode.Unknown;
                    CallState  callState  = CallState.Undefined;

                    switch (i)
                    {
                        case 7: callState = CallState.OnHold;           statusCode = StatusCode.On_Hold;                          break;
                        case 6: callState = CallState.Ringing;          statusCode = StatusCode.Ringing;            posPStr = ""; break;
                        case 5: callState = CallState.Disconnected;     statusCode = StatusCode.Disconnected;                     break;
                        case 4: callState = CallState.Abandoned;        statusCode = StatusCode.Abandoned;          posPStr = ""; break;
                        case 3: callState = CallState.ManualBid;        statusCode = StatusCode.Manual_ALI_Request; posPStr = ""; break;
                        case 2: callState = CallState.Canceled;         statusCode = StatusCode.Dialing;            posPStr = ""; break;
                        case 1: callState = CallState.AbandonedCleared; statusCode = StatusCode.Abandoned;          posPStr = ""; break;

                        default: callState = CallState.Disconnected; statusCode = StatusCode.Disconnected; posPStr = ""; break;
                    }

                    string callData, windowData;

                    // unconnected calls
                    if (i <= 7)
                    {
                        string posStr = this.getCurrentPosition().ToString().PadLeft(2, '0');
                        callData      = generateCallData(i, posStr, posPStr, statusCode, callState);

                        windowData = "<WindowData CADButton=\"on\" EraseCADButton=\"off\" TransferWindow=\"disable\" " +
                            "TDDWindow=\"disable\" SMSWindow=\"disable\" ALIButton=\"on\" TDDSendButton=\"off\" SMSSendButton=\"off\" " +
                            "TakeControlButton=\"off\" FlashHookButton=\"off\" TandemXferButton=\"off\" AttendedXferButton=\"off\" " +
                            "BlindXferButton=\"off\" ConferenceXferButton=\"off\" FlashTransferButton=\"off\" />";
                    }
                    else // connected calls
                    {
                        string posStr = pStr.PadLeft(2, '0');
                        callData      = generateCallData(i, posStr, posPStr, StatusCode.Connected, CallState.Connected);

                        windowData = "<WindowData CADButton=\"on\" EraseCADButton=\"off\" TransferWindow=\"enable\" " +
                            "TDDWindow=\"enable\" SMSWindow=\"enable\" ALIButton=\"on\" TDDSendButton=\"on\" SMSSendButton=\"on\" " +
                            "TakeControlButton=\"off\" FlashHookButton=\"on\" TandemXferButton=\"on\" AttendedXferButton=\"on\" " +
                            "BlindXferButton=\"on\" ConferenceXferButton=\"on\" FlashTransferButton=\"on\" />";
                    }

                    string tddData = "<TDDdata EncodingTDD=\"Base64\" TDDCallerSays=\"" +
                        Utility.EncodeBase64("21:43:57 (Caller) help ga" + ETX +
                                                "21:44:05 (Position 1) what is your emergency q ga" + ETX +
                                                "21:44:21 (Caller) hurt, need help ga" + ETX +
                                                "21:44:34 (Position 1) what address to send help q ga" + ETX +
                                                "21:45:01 (Caller) 931 Gibson Ct ga" + ETX +
                                                "21:45:52 (Position 1) what kind of hurt q ga" + ETX +
                                                "21:46:29 (Caller) red everywhere ga" + ETX +
                                                "21:46:47 (Position 1) who is hurt q ga" + ETX) +
                                                "\"" +
                                                " TDDReceived=\""   + Utility.EncodeBase64("okay...") + "\" />";

                    string smsData = "<SMSdata EncodingSMS=\"Base64\" SMSCallerSays=\"" +
                        Utility.EncodeBase64("21:43:57 (Caller) Help!" + ETX +
                                                "21:44:12 (Position 1) Where is your emergency?" + ETX +
                                                "21:44:43 (Caller) 1800 Centennial Dr" + ETX +
                                                "21:45:01 (Position 1) Is this a residence or business?" + ETX +
                                                "21:45:13 (Caller) Home" + ETX +
                                                "21:45:34 (Position 1) What is the nature of your emergency?" + ETX +
                                                "21:45:59 (Caller) Need an ambulance - fell and broke my leg" + ETX) +
                                                "\" />";

                    string streamInfo =
                        "<StreamInfo>" +
                        "<StreamID ID=\"song.wav:L\" Epoch=\"1504742636\" Caller=\"3038183060\" />" +
                        "<StreamID ID=\"song.wav:L\" Epoch=\"1504742636\" Caller=\"3035551212\" />" +
                        "<StreamID ID=\"song.wav:L\" Epoch=\"1504742638\" Caller=\"3035551213\" />" +
                        "</StreamInfo>";

                    string conferenceInfo = "";
                        //"<ConferenceData>" +
                        //  "<ConferenceMember Legend=\"P2\" Callback=\"1504742636\" Name=\"3038183060\" />" +
                        //  "<ConferenceMember Legend=\"P1\" Callback=\"1504742636\" Name=\"3038183060\" />" +
                        //"</ConferenceData>";

                    controller.XmlMessageServer.ProcessMessage(
                        "<?xml version=\"1.0\" encoding=\"utf-8\"?><Event id=\"\" XMLspec=\"" +
                        XMLMessage.SPEC_VERSION + "\" ><CallInfo> " + callData + windowData + tddData + smsData +
                                                                      streamInfo + conferenceInfo +
                        " </CallInfo></Event>");
                }

                SendTestSMS(null);

                // PSAP Status
                string psapAppletData = "<PSAPappletData>" +
                    "<PSAPdata Name=\"Huron County\"  PositionsOnline=\"3\" PositionsOn911=\"2\" PositionsOnAdmin=\"2\" />" +
                    "<PSAPdata Name=\"Norwalk PD\"    PositionsOnline=\"1\" PositionsOn911=\"0\" PositionsOnAdmin=\"0\" />" +
                    "<PSAPdata Name=\"Willard PD\"    PositionsOnline=\"0\" PositionsOn911=\"0\" PositionsOnAdmin=\"0\" />" +
                    "<PSAPdata Name=\"Test County 1\" PositionsOnline=\"2\" PositionsOn911=\"2\" PositionsOnAdmin=\"0\" />" +
                    "<PSAPdata Name=\"Test County 2\" PositionsOnline=\"4\" PositionsOn911=\"2\" PositionsOnAdmin=\"2\" />" +
                    "<PSAPdata Name=\"Test County 3\" PositionsOnline=\"3\" PositionsOn911=\"2\" PositionsOnAdmin=\"3\" />" +
                    "</PSAPappletData>";

                controller.XmlMessageServer.ProcessMessage(
                    "<?xml version=\"1.0\" encoding=\"utf-8\"?><Event id=\"\" XMLspec=\"" +
                    XMLMessage.SPEC_VERSION + "\" >" + psapAppletData + "</Event>");
            }
        }

        private void sendHeartbeatSeparateThread()
        {
            try
            {
                if ( ! alreadyCheckedDemoMode)
                {
                    alreadyCheckedDemoMode = true;

                    if (inDemoMode) initDemo();
                }

                string serverHost = null;

                if (socket == null && ! inDemoMode)
                {
                    lock (socketWriteLock)
                    {
                        serverHost     = controller.settings.GetSetting("ServerIPAddress", "192.168.62.11");
                        int serverPort = Convert.ToInt32(controller.settings.GetSetting("ServerPort", "50020"));

                        // check if multicast
                        socket = connectMulticast(serverHost, serverPort);

                        if (socket == null)
                        {
                            try
                            {
                                int sendBufferSize = Convert.ToInt32(controller.settings.GetSetting("SendBufferSize", "65536")); // 64 KiB
                                int recvBufferSize = Convert.ToInt32(controller.settings.GetSetting("RecvBufferSize", "3145728")); // 3 MiB

                                Socket newSocket = null;

                                if (Controller.protocol == ProtocolType.Tcp)
                                {
                                    newSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                                    newSocket.NoDelay           = true;
                                    newSocket.SendTimeout       = 2000;
                                    newSocket.ReceiveBufferSize = recvBufferSize;
                                    newSocket.SendBufferSize    = sendBufferSize;

                                    // Connect using a 5 second timeout
                                    IAsyncResult result = newSocket.BeginConnect(serverHost, serverPort, null, null);

                                    result.AsyncWaitHandle.WaitOne(5000, true);

                                    newSocket.EndConnect(result); // this will throw an exception if the Connect failed
Controller.DebugWindowMsg("  connected to " + serverHost);
                                }
                                else
                                {
                                    // test IP Address
                                    bool pingSuccess = (serverHost == "127.0.0.1");

                                    using (Ping ping = new Ping())
                                    {
                                        // try to ping up to 10 times
                                        for (int i = 0; i != 10 && ! pingSuccess; ++i)
                                        {
Controller.DebugWindowMsg("  about to ping[" + i.ToString() + "] " + serverHost + "...");
                                            try {pingSuccess = (ping.Send(serverHost, 1000).Status == IPStatus.Success);}
                                            catch {}
                                        }
                                    }

                                    if ( ! pingSuccess)
                                    {
Controller.DebugWindowMsg("  ping failed");
                                        throw new Exception("ping " + (Translate)("failed"));
                                    }

Controller.DebugWindowMsg("  ping success");
                                    newSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                                    newSocket.SendTimeout    = 2000;
                                    newSocket.SendBufferSize = sendBufferSize;

                                    newSocket.Connect(serverHost, serverPort);
                                }

                                socket = newSocket;
                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    // only ask if the Gui just came up, and if they haven't been asked before
                                    if ( ! settingsHaveBeenReceived && askIfTheUserWantsToKeepTryingToConnect)
                                    {
                                        askIfTheUserWantsToKeepTryingToConnect = false; // only ask once

                                        heartbeatMessageBoxIsUp = true;

                                        bool pingSuccess = (serverHost == "127.0.0.1");

                                        if ( ! pingSuccess)
                                        {
                                            try
                                            {
                                                using (Ping ping = new Ping())
                                                {
                                                    pingSuccess = (ping.Send(serverHost, 1000).Status == IPStatus.Success);
                                                }
                                            }
                                            catch {}
                                        }

                                        string pingMsg = pingSuccess ? " [ping " + (Translate)("success") + "]"
                                                                     : " [ping " + (Translate)("failed")  + "]";

                                        if (ex.Message.Equals("ping " + (Translate)("failed"))) pingMsg = "";

                                        string msg = (Translate)("Failed to connect to ") +
                                            serverHost + ":" + serverPort.ToString() + pingMsg + " (" + ex.Message + ")";

                                        // ask the user if they want to retry or exit
                                        DialogResult result = MessageBox.Show(null, msg +
                                            (Translate)("\n\nWould you like to keep trying or cancel and exit?"),
                                            Application.ProductName, MessageBoxButtons.RetryCancel, MessageBoxIcon.Question,
                                            MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);

                                        heartbeatMessageBoxIsUp = false;

                                        if (result == DialogResult.Cancel)
                                        {
                                            controller.HideSystemTray();
                                            System.Windows.Forms.Application.Exit();

                                            Process current = Process.GetCurrentProcess();
                                            if (current != null) current.Kill();
                                        }
                                        else
                                        {
                                            Controller.sendAlertToController(msg, Console.LogType.Alarm, Console.LogID.Connection);
                                        }
                                    }
                                }
                                catch {}
                            }
                        }
                    } // end socketWriteLock

                    if (socket != null)
                    {
Controller.DebugWindowMsg("  starting XmlMessageServer...");
                        controller.XmlMessageServer.Start(
                            Convert.ToInt32(controller.settings.GetSetting("ListenPort", "50021")),
                            controller.settings.GetSetting("ServerIPAddress", "192.168.62.11"),
                            Convert.ToInt32(controller.settings.GetSetting("ServerPort", "50020")),
                            Convert.ToInt32(controller.settings.GetSetting("SendBufferSize", "65536")), // 64 KiB
                            Convert.ToInt32(controller.settings.GetSetting("RecvBufferSize", "3145728")) // 3 MiB
                        );

                        if (Controller.protocol == ProtocolType.Tcp)
                        {
                            // start TCP close timeout
                            tcpCloseTimer.Change(
                                Convert.ToUInt16(controller.settings.GetSetting("TCPCloseTimeout", "30")) * 1000, Timeout.Infinite);
                        }

// TODO gui: Still need to figure out user switching on Windows 7...
Controller.DebugWindowMsg("  XmlMessageServer started");
                    }
                }

                if (socket != null)
                {
                    registerForSessionChangeEvents();

                    // if the socket was just created, add valid IP addresses
                    if (serverHost != null)
                    {
                        bool firewallEnabled = toBoolean(controller.settings.GetSetting("Firewall", "off"));

                        if (firewallEnabled)
                        {
Controller.DebugWindowMsg("  adding " + serverHost + " as a valid IP address to XmlMessageServer");
                            controller.XmlMessageServer.addServerIPAddresses(System.Net.Dns.GetHostAddresses(serverHost));

                            string validIPAddresses = controller.settings.GetSetting("ValidIPAddresses", "");

                            char[] delimeters = {' ',',',';','\t'};
                            string[] validIPList = validIPAddresses.Split(delimeters, StringSplitOptions.RemoveEmptyEntries);

                            foreach (string validIPStr in validIPList)
                            {
Controller.DebugWindowMsg("  adding " + validIPStr + " as a valid IP address to XmlMessageServer");
                                controller.XmlMessageServer.addServerIPAddresses(System.Net.Dns.GetHostAddresses(validIPStr));
                            }
                        }
                        else
                        {
Controller.DebugWindowMsg("  firewall is disabled");
                        }
                    }

Controller.DebugWindowMsg("  sending heartbeat...");
                    string heartbeatMessage = getXMLHeader("HB", heartbeatId++) + "<Heartbeat ListenPort=\"" +
                        controller.settings.GetSetting("ListenPort", "50021") + "\" " +
                        "Position=\"" + controller.settings.GetSetting("Position", "1").PadLeft(2, '0') + "\" " +
                        "OS=\"" + getWindowsOS() + "\" " +
                        "GUIversion=\"" + Application.ProductVersion + "\" /></Event>";

                    sendToController(heartbeatMessage);
Controller.DebugWindowMsg("  heartbeat sent");

                    // ask for init settings
                    if ( ! controller.hasReceivedSettings())
                    {
Controller.DebugWindowMsg("  sending init request...");
                        string sendInitializationMsg = getXMLHeader("Init", initMsgId++) + "<SendInitializationXML ListenPort=\"" +
                                                       controller.settings.GetSetting("ListenPort", "50021") + "\" " +
                                                       "Position=\"" + controller.settings.GetSetting("Position", "1").PadLeft(2, '0') + "\" " +
                                                       "OS=\"" + getWindowsOS() + "\" " +
                                                       "/></Event>";

                        sendToController(sendInitializationMsg);
Controller.DebugWindowMsg("  init request sent");
                    }
                }
            }
            catch {}

            heartbeatThread = null;
        }

        public void clearSpeedDialButtons()
        {
            foreach (ToolStrip speedDialToolBar in speedDialToolBarDictionary.Values)
            {
                speedDialToolBar.Items.Clear();
                speedDialToolBar.Visible = false;
            }

            speedDialToolBarDictionary.Clear();
        }

        public void addSpeedDialButton(PhonebookEntry phonebookEntry)
        {
            ToolStripItem newSpeedDialButton =
                new ToolStripButton(phonebookEntry.Name, phonebookEntry.Image);

            newSpeedDialButton.Font = new System.Drawing.Font("Tahoma", 9F);
            newSpeedDialButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            newSpeedDialButton.Padding = new System.Windows.Forms.Padding(2);
            newSpeedDialButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            newSpeedDialButton.Click += SpeedDialButtonClick;
            newSpeedDialButton.Tag = phonebookEntry;

            Controller.TransferType transferType = phonebookEntry.SpeedDialAction;

            if (transferType == Controller.TransferType.Dial)
            {
                newSpeedDialButton.ToolTipText = "Dial -> " + phonebookEntry.NumberFormatted;
            }
            else
            {
                newSpeedDialButton.ToolTipText =
                    transferType.ToString() + " Transfer -> " + phonebookEntry.NumberFormatted;
            }

            ToolStrip speedDialToolBar;

            if (speedDialToolBarDictionary.ContainsKey(phonebookEntry.Group))
            {
                speedDialToolBar = speedDialToolBarDictionary[phonebookEntry.Group];
            }
            else
            {
                speedDialToolBar = new System.Windows.Forms.ToolStrip();
                speedDialToolBarDictionary.Add(phonebookEntry.Group, speedDialToolBar);

                int nToolbars = speedDialToolBarDictionary.Count;

                speedDialToolBar.AllowItemReorder = true;
                speedDialToolBar.BackColor = SystemColors.Window;
                speedDialToolBar.Dock = System.Windows.Forms.DockStyle.None;
                speedDialToolBar.ImageScalingSize = new System.Drawing.Size(24, 24);
                speedDialToolBar.Location = new System.Drawing.Point(0, 24 + (58 * nToolbars));
                speedDialToolBar.Name = phonebookEntry.Group + " Toolbar";
                speedDialToolBar.Stretch = true;
                speedDialToolBar.Size = new System.Drawing.Size(1024, 58);
                speedDialToolBar.TabIndex = 1 + nToolbars;

                switch (nToolbars)
                {
                    case 1:  toolStripContainer.TopToolStripPanel.    Controls.Add(speedDialToolBar); break;
                    default: toolStripContainer.BottomToolStripPanel. Controls.Add(speedDialToolBar); break;
                }
            }

            speedDialToolBar.Items.Add(newSpeedDialButton);
        }

        private void SpeedDialButtonClick(object sender, EventArgs e)
        {
            ToolStripButton speedDialButton = (ToolStripButton)sender;
            PhonebookEntry  phonebookEntry  = (PhonebookEntry)(speedDialButton).Tag;

            if (controller != null && controller.phonebook != null)
            {
                bool enabled = (phonebookEntry.SpeedDialAction == Controller.TransferType.Dial ||
                                controller.phonebook.transferEnabled);

                switch (phonebookEntry.SpeedDialAction)
                {
                    case Controller.TransferType.Tandem:     if ( ! controller.phonebook.tandemEnabled)     enabled = false; break;
                    case Controller.TransferType.Attended:   if ( ! controller.phonebook.attendedEnabled)   enabled = false; break;
                    case Controller.TransferType.Conference: if ( ! controller.phonebook.conferenceEnabled) enabled = false; break;
                    case Controller.TransferType.Blind:      if ( ! controller.phonebook.blindEnabled)      enabled = false; break;
                    case Controller.TransferType.FlashHook:  if ( ! controller.phonebook.flashEnabled)      enabled = false; break;
                }

                if ( ! enabled)
                {
                    MessageBox.Show(this, speedDialButton.ToolTipText + " is disabled for the currently selected row.",
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    return;
                }
            }

            if ( ! phonebookEntry.Enabled)
            {
                MessageBox.Show(this, phonebookEntry.NumberFormatted +
                    (Translate)(" is disabled on this type of Trunk"),
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                return;
            }

            Controller.TransferType transferType = phonebookEntry.SpeedDialAction;

            if (transferType == Controller.TransferType.Dial)
            {
                controller.sendDialCommand(phonebookEntry.Number);

                // TODO phonebook: display cancel dialog for Dial?
            }
            else
            {
                controller.TransferTo(phonebookEntry.Number, transferType);

                Phonebook.displayPostTransferDialog(
                    this, controller, transferType, phonebookEntry.Desc, phonebookEntry.Number);
            }
        }

        static string windowsOS = null;

        private static int getOSArchitecture()
        {
            string pa = Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE");
            return ((String.IsNullOrEmpty(pa) || String.Compare(pa, 0, "x86", 0, 3, true) == 0) ? 32 : 64);
        }

        public static string getWindowsOS()
        {
            try
            {
                if (windowsOS != null) return windowsOS;

                // get Operating system information
                OperatingSystem os = Environment.OSVersion;
                Version         vs = os.Version;

                if (os.Platform == PlatformID.Win32Windows)
                {
                    // this is a pre-NT version of Windows
                    switch (vs.Minor)
                    {
                        case 0:  windowsOS = "Win95"; break;
                        case 10:
                            if (vs.Revision.ToString() == "2222A") windowsOS = "Win98SE";
                            else                                   windowsOS = "Win98";
                            break;
                        case 90: windowsOS = "WinMe"; break;
                        default: break;
                    }
                }
                else if (os.Platform == PlatformID.Win32NT)
                {
                    string test = os.VersionString;

                    switch (vs.Major)
                    {
                        case 3: windowsOS = "NT 3.51"; break;
                        case 4: windowsOS = "NT 4.0";  break;
                        case 5:
                        {
                            if (vs.Minor == 0) windowsOS = "2000";
                            else               windowsOS = "XP";
                            break;
                        }
                        case 6:
                        {
                            switch (vs.Minor)
                            {
                                case 0:  windowsOS = "Vista";   break;
                                case 1:  windowsOS = "Win7";    break;
                                case 2:  windowsOS = "Win8";    break;
                                case 3:  windowsOS = "Win8.1";  break;
                                default: windowsOS = "Win8.1+"; break;
                            }

                            break;
                        }
                        case 10: windowsOS = "Win10"; break;

                        default:
                        {
                            windowsOS = "Win??";
                            break;
                        }
                    }
                }

                if (windowsOS != "")
                {
                    // if there is a service pack, append it
                    if (os.ServicePack != "") windowsOS += " " + os.ServicePack.Replace("Service Pack ", "SP");
                }

                return windowsOS;
            }
            catch {}

            windowsOS = "UNKNOWN";

            return windowsOS;
        }

        private Socket connectMulticast(string serverHost, int serverPort)
        {
            char[] delimeter = {'.'};

            string[] octetStrList = serverHost.Split(delimeter);

            int firstOctet = Convert.ToInt32(octetStrList[0]);

            if (firstOctet >= 224 && firstOctet <= 239)
            {
Controller.DebugWindowMsg("  begin connectMulticast()");

                try
                {
                    int sendBufferSize = Convert.ToInt32(controller.settings.GetSetting("SendBufferSize", "65536")); // 64 KiB
                    int recvBufferSize = Convert.ToInt32(controller.settings.GetSetting("RecvBufferSize", "3145728")); // 3 MiB

                    Socket newSocket = null;

                    if (Controller.protocol == ProtocolType.Tcp)
                    {
                        newSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                        newSocket.NoDelay           = true;
                        newSocket.ReceiveBufferSize = recvBufferSize;
                    }
                    else
                    {
                        newSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                        const int timeToLiveHops = 1; // must be on the same subnet
                        newSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastTimeToLive, timeToLiveHops);
                    }

                    newSocket.SendTimeout    = 2000;
                    newSocket.SendBufferSize = sendBufferSize;

                    newSocket.Connect(serverHost, serverPort);

                    return socket = newSocket;
                }
                catch (Exception ex)
                {
                    Controller.showAlert((Translate)("Encountered a socket exception (") + ex.Message + ")",
                        LogType.Warning, LogID.Connection);
                }

Controller.DebugWindowMsg("  end connectMulticast()");
            }

            return null;
        }

        bool inSendToController = false;

        public bool sendToController(string message)
        {
            if (inSendToController) return false;

            inSendToController = true;

            bool sent = false;

            try
            {
                if (socket != null)
                {
                    lock (socketWriteLock)
                    {
                        if ( ! socket.Connected) throw new Exception("socket not connected");

                        string headerStr = Client.HDR_LENGTH_STR +
                            message.Length.ToString().PadLeft(Client.HDR_LENGTH_SIZE) +
                            Client.HDR_TYPE_STR + "text/xml".PadLeft(Client.HDR_TYPE_SIZE) +
                            Client.HDR_DELIMITER;

                        string messageToSend = headerStr + message;

                        byte[] data = Encoding.ASCII.GetBytes(messageToSend);

                        int nBytesSent = socket.Send(data);

                        sent = (nBytesSent == messageToSend.Length);
                    }
                }
                else
                {
Controller.DebugWindowMsg("    Failed to send \"" + message + "\" (socket is null)");
                    Controller.showAlert((Translate)("Could not send a message to the server") +
                                         (Translate)(" (socket is null)"),
                                         LogType.Warning, LogID.Connection);
                }
            }
            catch (Exception ex)
            {
Controller.DebugWindowMsg("    Failed to send \"" + message + "\" (socket exception: " + ex.Message + ")");
                Controller.showAlert((Translate)("Could not send a message to the server (") + ex.Message + ")",
                    LogType.Warning, LogID.Connection);

                try
                {
                    closeSocket();

                    if (controller != null)
                    {
                        controller.XmlMessageServer.Stop();
Controller.DebugWindowMsg("  XmlMessageServer.Stop");
                    }

                    SendHeartbeat(null); // this will try to reconnect the socket
                }
                catch {}
            }

            inSendToController = false;
            return sent;
        }

        public void sendTDDStringToController(string messageName, string callId, string tddString)
        {
            string optionalElement = "<TDDdata";

            // encode characters
            bool encoded;
            tddString = Utility.EncodeBase64(tddString, out encoded);

            if (encoded) optionalElement += " EncodingTDD=\"" + "Base64\"";

            optionalElement += " TDDdispatcherSays=\"" + tddString + "\" />";

            sendToController(messageName, callId, optionalElement);
        }

        public void sendTransferToController(
            string                  messageName,
            DataRowView             row,
            string                  transferToNumber,
            Controller.TransferType transferType
        )
        {
            string optionalAttributes = " TransferToNumber=\"" + transferToNumber. ToString() + "\"" +
                                        " Type=\""             + transferType.     ToString() + "\"";

            sendToController(messageName, row["Call_ID"].ToString(), "", optionalAttributes);
        }

        private static int dialMsgId = 1;

        public void sendDialToController(string numberToDial)
        {
            sendToController(getXMLHeader("Dial", dialMsgId++) + "<Dial Number=\"" + numberToDial + "\" Position=\"" + 
                    CurrentPosition.ToString().PadLeft(2, '0') + "\" /></Event>");
        }
        
        public void sendFlashHookToController(DataRowView row)
        {
            if (row == null) return;

            sendToController("FlashHook", row["Call_ID"].ToString());
        }
        
        public void sendAnswerOrHoldToController(string messageName, DataRowView row)
        {
            if (row == null) return;

            string optionalAttributes = " Position=\"" + CurrentPosition.ToString().PadLeft(2, '0') + "\"";

            sendToController(messageName, row["Call_ID"].ToString(), "", optionalAttributes);
        }

        private static int pickupMsgId = 1;

        public void sendPickupToController(string messageName)
        {
            sendToController(getXMLHeader("Pickup", pickupMsgId++) + "<" + messageName + " Position=\"" + 
                CurrentPosition.ToString().PadLeft(2, '0') + "\" /></Event>");
        }

        public void sendHangupToController(string messageName, DataRowView row)
        {
            if (row == null) return;

            string optionalAttributes = " Position=\"" + CurrentPosition.ToString().PadLeft(2, '0') + "\"";

            sendToController(messageName, row["Call_ID"].ToString(), "", optionalAttributes);
        }

        public enum LogType
        {
            Info,
            Warning,
            Alarm,
            TDD,
            SMS
        }

        public enum LogID // 751 - 799
        {
            TDD = 751,
            Init = 752,
            Update = 753,
            Connection = 754,
            Gui = 755,
            Cleanup = 756,
            SMS = 757
        }

        private static int logMsgId = 1;

        public bool sendLogMsgToController(LogType logType, LogID logId, string logMsg, bool encoded, DataRowView row)
        {
            if ( ! connectionEstablished) return false;

            bool sent = false;

DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    string message =
                        getXMLHeader("Log", logMsgId++) + "<Log Type=\"" + logType.ToString() +
                        "\" MsgNumber=\""  + ((int)logId).ToString() +
                        (encoded ?
                        "\" Encoding=\"Base64" : "") +
                        "\" MsgText=\"User " + sessionUsername + " - " + logMsg;

                    if (row != null)
		            {
                        message += "\" Call-ID=\""          + row["Call_ID"].          ToString() +
                                   "\" Callback=\""         + row["Callback"].         ToString() +
                                   "\" ConferenceNumber=\"" + row["ConferenceNumber"]. ToString() +
                                   "\" Trunk=\""            + row["Trunk"].            ToString() +
                                   "\" pANI=\""             + row["pANI"].             ToString() +
                                   "\" LineNumber=\""       + row["LineNumber"].       ToString();
                    }

                    message += "\" /></Event>";

                    sent = sendToController(message);
		        }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);

            return sent;
        }

        public bool isRebidEnabled() {return rebidALIButton.Enabled;}

        public bool sendToController(string messageName, string callId)
        {
            return sendToController(messageName, callId, "", "");
        }

        public bool sendToController(string messageName, string callId, string optionalElements)
        {
            return sendToController(messageName, callId, optionalElements, "");
        }

        private static int msgId = 1;

        public bool sendToController(string messageName, string callId, string optionalElements, string optionalAttributes)
        {
            if (socket == null)
            {
                Controller.showAlert((Translate)("Failed to send ") + messageName +
                    (Translate)(" message") + (Translate)(" (socket is null)"),
                    Console.LogType.Warning, Console.LogID.Connection);
                return false;
            }

            if (callId == null)
            {
                Controller.showAlert((Translate)("Failed to send ") + messageName +
                    (Translate)(" message") + (Translate)(" (call ID is null)"),
                    Console.LogType.Warning, Console.LogID.Connection);
                return false;
            }

            bool sent = false;

DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    string message = getXMLHeader("Msg", msgId++) + "<" + messageName +
                                     " Call-ID=\"" + callId + "\"" + optionalAttributes;

                    if (optionalElements.Length == 0) message += "/></Event>";
                    else                              message += ">" + optionalElements + "</" + messageName + "></Event>";

                    sent = sendToController(message);
                }
                catch (Exception ex)
                {
                    Controller.showAlert((Translate)("Failed to send ") + messageName + (Translate)(" message (") + ex.Message + ")",
                        Console.LogType.Warning, Console.LogID.Connection);
                }
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);

            return sent;
        }

        private void UpdateTime(object obj)
        {
            try
            {
                BeginInvoke(new updateGridDelegate(updateGrid), false);
            }
            catch
            {
                timeTimer.Change(200, Timeout.Infinite);
            }
        }

        private delegate void updateGridDelegate(bool filterChanged);

        private int      shortUpdateCount         = 0;
        private DateTime lastHoldStateChangedTime = DateTime.Now;

        private void updateGrid(bool filterChanged)
        {
DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    // draw position number faded for a shorter period of time
                    if (DateTime.Now.Subtract(lastHoldStateChangedTime).TotalMilliseconds >= 600.0)
                    {
                        lastHoldStateChangedTime = DateTime.Now;

                        if (holdFlashState == HoldFlashState.NORMAL) holdFlashState = HoldFlashState.SUBDUED;
                        else                                         holdFlashState = HoldFlashState.NORMAL;
                    }

                    if (dataModel != null) dataModel.UpdateTime();

                    ConsoleGrid currentGrid = getCurrentGrid();

                    if (currentGrid != null && currentGrid.Handle != null)
                    {
                        updateTabCounts();

                        bool rowSelected = false;

                        if (currentGrid.RowCount == 0)
                        {
                            RawDisplay.Clear();
                        }
                        else if (currentGrid.getSelectedCallID() != 0)
                        {
                            rowSelected = true;

                            // try to reselect the call that was selected before the position filter change
                            if (filterChanged)
                            {
                                // if the current row is not the selectedCallID
                                if (currentGrid.CurrentRow == null || currentGrid.CurrentRow.DataBoundItem == null ||
                                    Convert.ToUInt64(((System.Data.DataRowView)currentGrid.CurrentRow.DataBoundItem)["Call_ID"]) != currentGrid.getSelectedCallID())
                                {
                                    rowSelected = false;

                                    foreach (DataGridViewRow GRow in currentGrid.Rows)
                                    {
                                        if ((GRow != null) && (GRow.DataBoundItem != null))
                                        {
                                            DataRowView row = (DataRowView)GRow.DataBoundItem;

                                            if (row != null && Convert.ToUInt64(row["Call_ID"]) == currentGrid.getSelectedCallID())
                                            {
                                                currentGrid.CurrentCell = GRow.Cells[0];
System.Diagnostics.Trace.WriteLine(currentGrid.Name + " - attempting to select " + currentGrid.getSelectedCallID().ToString() + " Console:2514", "Info");

                                                rowSelected = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        // if no row was selected, and no row is selected, select the first row
                        if ( ! rowSelected && currentGrid.Rows.Count != 0 && currentGrid.CurrentRow == null)
                        {
                            DataGridViewRow GRow = currentGrid.Rows[currentGrid.Rows.GetFirstRow(DataGridViewElementStates.Visible)];

                            if (GRow != null && GRow.DataBoundItem != null)
                            {
                                currentGrid.setSelectedCallID(currentGrid.getRowKey(GRow), "Console:2534");
                                currentGrid.CurrentCell = GRow.Cells[0];
System.Diagnostics.Trace.WriteLine(currentGrid.Name + " - attempting to select " + currentGrid.getRowKey(GRow).ToString() + " Console:2536", "Info");
                            }
                        }

                        currentGrid.RefreshRowSelection(null);
                        GridSelectionChanged(null);
                    }
                    else
                    {
                        RawDisplay.Clear();
                    }
                }
                catch {}

                try
                {
                    // check row counts and row age every other second (every 10 updates)
                    if (++shortUpdateCount % 10 == 0)
                    {
                        shortUpdateCount = 0;

                        // remove abandoned calls when there are too many
                        if (AbandonedGrid.RowCount > maxAbandonedCalls)
                        {
                            int nRowsToRemove = AbandonedGrid.RowCount - maxAbandonedCalls;

                            for (int i = 0; i < nRowsToRemove; ++i)
                            {
                                int lastRow = AbandonedGrid.Rows.GetLastRow(DataGridViewElementStates.Visible);

                                if (lastRow == -1) break;

                                AbandonedGrid.Rows.RemoveAt(lastRow);
                            }
                        }

                        // remove active calls when there are too many
                        if (AllGrid.RowCount > maxNumberOfRows)
                        {
                            int nRowsToRemove = AllGrid.RowCount - maxNumberOfRows;

                            for (int i = 0; i < nRowsToRemove; ++i)
                            {
                                int lastRow = AllGrid.Rows.GetLastRow(DataGridViewElementStates.Visible);

                                if (lastRow == -1) break;

                                AllGrid.Rows.RemoveAt(lastRow);
                            }
                        }

                        // remove any calls that are too old
                        for (int lastRow = AllGrid.Rows.GetLastRow(DataGridViewElementStates.Visible);
                             lastRow != -1; lastRow = AllGrid.Rows.GetLastRow(DataGridViewElementStates.Visible))
                        {
                            DataGridViewRow GRow = AllGrid.Rows[lastRow];

                            if (GRow != null && GRow.DataBoundItem != null)
                            {
                                DataRowView row = (DataRowView)GRow.DataBoundItem;

                                int ageSeconds = DateTime.Now.Subtract((DateTime)row["RingDateTimeLocal"]).Seconds;

                                if (ageSeconds > maxRecordAgeOutSeconds)
                                {
                                    AllGrid.Rows.Remove(GRow);
                                    continue;
                                }
                            }

                            break;
                        }
                    }
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);

            if (timeTimer != null) timeTimer.Change(200, Timeout.Infinite);
        }

        private int             abandonedCount        = 0;
        private System.DateTime lastDeadSoundDateTime = System.DateTime.Now;

        private void updateTabCounts()
        {
            try
            {
                updateTabCount(lineTab,      LineGrid,      "Line Number");
                updateTabCount(connectedTab, ConnectedGrid, "Connected");
                updateTabCount(abandonedTab, AbandonedGrid, "Abandoned");
                updateTabCount(positionTab,  PositionGrid,  "My Position");
                updateTabCount(allTab,       AllGrid,       "All");

                // if the number of Abandoned calls changed
                if (abandonedTab != null && AbandonedGrid != null && abandonedCount != AbandonedGrid.RowCount)
                {
                    // if the number of Abandoned calls increases (and the sound hasn't been played for at least a second)
                    if (abandonedCount < AbandonedGrid.RowCount &&
                        System.DateTime.Now.Subtract(lastDeadSoundDateTime).TotalSeconds > 1)
                    {
                        System.Media.SystemSounds.Beep.Play();
                        lastDeadSoundDateTime = System.DateTime.Now;
                    }

                    // remember how many abaonded calls there are
                    abandonedCount = AbandonedGrid.RowCount;
                }
            }
            catch {}
        }

        private void updateTabCount(TabPage tab, ConsoleGrid grid, string name)
        {
            if (tab != null && grid != null)
            {
                int rowCount = grid.RowCount;

                if (dataModel != null && tab == lineTab)
                {
                    rowCount = 0;

                    foreach (DataGridViewRow GRow in grid.Rows)
                    {
                        if (GRow != null)
                        {
                            DataRowView row = (DataRowView)GRow.DataBoundItem;
                            if ((CallState)Convert.ToUInt16(row["CallStateCode"]) != CallState.Undefined) ++rowCount;
                        }
                    }
                }

                string rowCountStr = " (" + rowCount + ")";

                if ( ! tab.Text.EndsWith(rowCountStr)) tab.Text = (Translate)(name) + rowCountStr;
            }
        }

        public void handleFormsBuilt()
        {
            if (inDemoMode)
            {
                ButtonEnabled(printButton,          true);
                ButtonEnabled(rebidALIButton,       true);
                manualBidMenu.Enabled =             true;
                ButtonEnabled(sendToCADButton,      true);
                ButtonEnabled(mapALIButton,         true);
                ButtonEnabled(sendEraseToCADButton, true);
                controller.getTDD().setEnabled     (true);
                controller.getSMS().setEnabled     (true);
                controller.phonebook.setEnabled    (true, null);
                ButtonEnabled(abandonedButton,      true);
                ButtonEnabled(connectButton,        true);
                ButtonEnabled(disconnectButton,     true);
                ButtonEnabled(holdButton,           true);
                ButtonEnabled(callbackButton,       true);
            }
        }

        public static void ShowUsersManual()
        {
            try
            {
                string exePath = System.Windows.Forms.Application.ExecutablePath.Replace('\\', '/');
                exePath = exePath.Remove(exePath.LastIndexOf('/'));

                string usersManualPath = exePath + "/911.pdf";

                if (System.IO.File.Exists(usersManualPath))
                {
                    System.Diagnostics.Process.Start(usersManualPath);
                }
                else
                {
                    MessageBox.Show(null, (Translate)("Could not find User Manual") + " \"" + usersManualPath + "\"",
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    Controller.sendAlertToController(
                        "Could not find User Manual \"" + usersManualPath + "\"", LogType.Info, LogID.Gui);
                }
            }
            catch (Exception ex)
            {
                Logging.AppTrace("Error in Show User Manual:" + ex.Message);
                Logging.ExceptionLogger("Error in Console.ShowUsersManual", ex, LogID.Gui);
            }
        }

        private void SetTitleBarText()
        {
            string newTitle = Application.ProductName + " - Console " + (Translate)("[Position ") + CurrentPosition.ToString() + "]";

            if (this.Text != newTitle) this.Text = newTitle;
        }

        private void MainForm_Load(object sender, System.EventArgs e)
        {
            //barCurrentView.Caption = "View: Position " + CurrentPosition;
            SetTitleBarText();
            aboutMenu.Text = aboutMenu.Text.Replace("XXX",Application.ProductName);

            // TODO gui: get rid of this nonsense of adding just the right separators since we now have removeUnnecessarySeparators()
            addSeparators();

            // remove unnecessary separators
            removeUnnecessarySeparators(toolBar);
        }

        private void Console_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        private void closeMenu_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private static int muteMsgId = 1;

        private void muteButtonClick(object sender, EventArgs e)
        {
            sendToController(getXMLHeader("Mute", muteMsgId++) + "<Mute Position=\"" + 
                CurrentPosition.ToString().PadLeft(2, '0') + "\" /></Event>");
        }

        private void btnReBidClick(object sender, EventArgs e)
        {
DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    // TODO** calls: Rebid is using current row, should it use active call
                    if (rebidALIButton.Enabled) controller.Rebid(getCurrentRow()["Call_ID"].ToString());
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
        }

        private void btnSendToCADClick(object sender, EventArgs e)
        {
DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    // TODO** calls: SendToCAD is using current row, should it use active call
                    controller.SendToCAD(getCurrentRow());
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
        }

        private void btnSendEraseToCADClick(object sender, EventArgs e)
        {
            controller.SendEraseToCAD();
        }

        private void btnRadioStateRadioClick(object sender, EventArgs e)
        {
            controller.SendRadioContactClosure("Radio");
        }

        private void btnRadioStateTelephoneClick(object sender, EventArgs e)
        {
            controller.SendRadioContactClosure("Telephone");
        }

        private void btnForceDisconnectClick(object sender, EventArgs e)
        {
DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    DataRowView curRow = getCurrentRow();

                    if (curRow == null)
                    {
                        MessageBox.Show(this, (Translate)("There must be a call selected to Force Disconnect."),
                            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        DialogResult result = MessageBox.Show(this,
                            (Translate)("Are you sure you want to force this call to disconnect?"),
                            Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (result == DialogResult.No) return;

                        // TODO** calls: ForceDisconnect is using current row, should it use active call
                        sendToController("ForceDisconnect", curRow["Call_ID"].ToString());
                    }
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
        }

        private void btnManualBidClick(object sender, EventArgs e)
        {
            controller.ManualBid();
        }

        private bool parseTextForLatLon(string text, out string latStr, out string lonStr)
        {
            latStr = lonStr = null;

            // find something that looks like "X = +39.0134  Y = -104.6637"
            Match xMatch = Regex.Match(text, "X *\\=* *([\\-\\+]*[0-9]+\\.[0-9]+)");
            Match yMatch = Regex.Match(text, "Y *\\=* *([\\-\\+]*[0-9]+\\.[0-9]+)");

            string xStr = null, yStr = null;

            if (xMatch.Success && yMatch.Success)
            {
                xStr = xMatch.Groups[1].Value;
                yStr = yMatch.Groups[1].Value;
            }
            else
            {
                // find something that looks like "+39.0134  -104.6637"
                Match xyMatch = Regex.Match(text, "([\\+\\-][0-9]+\\.[0-9]+) *([\\+\\-][0-9]+\\.[0-9]+)");

                if (xyMatch.Success)
                {
                    xStr = xyMatch.Groups[1].Value;
                    yStr = xyMatch.Groups[2].Value;
                }
            }

            if (xStr != null && yStr != null)
            {
                xStr = xStr.Replace("+","");
                yStr = yStr.Replace("+","");

                double xVal = Convert.ToDouble(xStr);
                double yVal = Convert.ToDouble(yStr);

                if (xVal < -40.0)
                {
                    lonStr = xVal.ToString();
                    latStr = yVal.ToString();
                }
                else
                {
                    latStr = xVal.ToString();
                    lonStr = yVal.ToString();
                }

                return true;
            }

            return false;
        }

        private static int htmlCount = 1;

        private void btnMapALIClick(object sender, EventArgs e)
        {
            string callType  = (Translate)("Unknown Call Type");
            string aliRecord = "";

DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    DataRowView row = getCurrentRow();

                    if (row != null)
                    {
                        callType  = row["CallType"].ToString() + (Translate)(" Call");
                        aliRecord = row["ALIRecord"].ToString();

                        if (row["Encoding_ALI"].ToString() == "Base64") aliRecord = Utility.DecodeBase64(aliRecord);
                    }
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);

            try
            {
                if (aliRecord.Length != 0)
                {
                    string latStr, lonStr;

                    bool found = parseTextForLatLon(aliRecord, out latStr, out lonStr);

                    if (found)
                    {
                        callType = System.Security.SecurityElement.Escape(callType);
                        latStr   = System.Security.SecurityElement.Escape(latStr);
                        lonStr   = System.Security.SecurityElement.Escape(lonStr);

                        string rawDisplayText =
                            System.Security.SecurityElement.Escape(RawDisplay.Text).Replace("\r","").Replace("\n","<br>");

                        string titleStr     = callType;
                        string popupHtmlStr = "<font face=\"Arial\"><b>" + callType + "</b><br>Latitude: " + latStr +
                                              "<br>Longitude: " + lonStr + "</font>" + "<br><pre>" + rawDisplayText + "</pre>";
                        string mapHtmlStr =
                            "<!-- saved from url=(0014)about:internet -->\r\n" +
                            "<!DOCTYPE html>\r\n" +
                            "<html>\r\n" +
                            "  <head>\r\n" +
                            "    <title>" + titleStr + "</title>\r\n" +
                            "    <meta name='viewport' content='initial-scale=1.0'>\r\n" +
                            "    <meta charset='utf-8'>\r\n" +
                            "    <style>\r\n" +
                            "      html, body\r\n" +
                            "      {\r\n" +
                            "        height: 100%;\r\n" +
                            "        margin: 0;\r\n" +
                            "        padding: 0;\r\n" +
                            "      }\r\n" +
                            "      #map\r\n" +
                            "      {\r\n" +
                            "        height: 100%;\r\n" +
                            "        width: 100%;\r\n" +
                            "        position: relative;\r\n" +
                            "      }\r\n" +
                            "      #pano\r\n" +
                            "      {\r\n" +
                            "        bottom: 0;\r\n" +
                            "        left: 0;\r\n" +
                            "        height: 30%;\r\n" +
                            "        width: 30%;\r\n" +
                            "        position: absolute;\r\n" +
                            "      }\r\n" +
                            "    </style>\r\n" +
                            "  </head>\r\n" +
                            "  <body>\r\n" +
                            "    <div id='map'></div>\r\n" +
                            "    <div id='pano'></div>\r\n" +
                            "    <script>\r\n" +
                            "\r\n" +
                            "      var map;\r\n" +
                            "      var panorama;\r\n" +
                            "      var mapCenter;\r\n" +
                            "      var validPosition = true;\r\n" +
                            "\r\n" +
                            "      function initMap()\r\n" +
                            "      {\r\n" +
                            "        // map center\r\n" +
                            "        mapCenter = {lat: " + latStr + ", lng: " + lonStr + "};\r\n" +
                            "\r\n" +
                            "        map = new google.maps.Map(document.getElementById('map'),\r\n" +
                            "        {\r\n" +
                            "          zoom: 16,\r\n" +
                            "          center: mapCenter,\r\n" +
                            "          mapTypeId: google.maps.MapTypeId.ROADMAP,\r\n" +
                            "          scaleControl: true\r\n" +
                            "        });\r\n" +
                            "\r\n" +
                            "        // Traffic Layer\r\n" +
                            "        var trafficLayer = new google.maps.TrafficLayer();\r\n" +
                            "        trafficLayer.setMap(map);\r\n" +
                            "\r\n" +
                            "        // Street View Overlay\r\n" +
                            "        panorama = new google.maps.StreetViewPanorama(document.getElementById('pano'),\r\n" +
                            "        {\r\n" +
                            "          position: mapCenter,\r\n" +
                            "          enableCloseButton: true,\r\n" +
                            "          fullscreenControl: false,\r\n" +
                            "          zoomControl: false,\r\n" +
                            "          linksControl: false\r\n" +
                            "        });\r\n" +
                            "\r\n" +
                            "        var sv = new google.maps.StreetViewService();\r\n" +
                            "\r\n" +
                            "        panorama.addListener('visible_changed', function()\r\n" +
                            "        {\r\n" +
                            "          validPosition = false;\r\n" +
                            "\r\n" +
                            "          setTimeout(updatePanoVisibility, 150); // for responsiveness\r\n" +
                            "          setTimeout(updatePanoVisibility, 500); // for robustness\r\n" +
                            "        });\r\n" +
                            "\r\n" +
                            "        panorama.addListener('position_changed', function()\r\n" +
                            "        {\r\n" +
                            "          validPosition = true;\r\n" +
                            "          setTimeout(setPanoVisible, 100); // for if visible_changed comes later (MS Edge)\r\n" +
                            "        });\r\n" +
                            "\r\n" +
                            "        map.setStreetView(panorama);\r\n" +
                            "\r\n" +
                            "        // initialize to the correct heading\r\n" +
                            "        sv.getPanorama({location: mapCenter, radius: 200}, calculateHeading);\r\n" +
                            "\r\n" +
                            "        // info popup\r\n" +
                            "        var contentString = '" + popupHtmlStr + "';\r\n" +
                            "\r\n" +
                            "        var infowindow = new google.maps.InfoWindow({content: contentString});\r\n" +
                            "\r\n" +
                            "        var marker = new google.maps.Marker({\r\n" +
                            "          position: mapCenter,\r\n" +
                            "          map: map,\r\n" +
                            "          title: '" + titleStr + "'\r\n" +
                            "        });\r\n" +
                            "\r\n" +
                            "        google.maps.event.addListener(marker, 'click', function()\r\n" +
                            "        {\r\n" +
                            "          infowindow.open(map,marker);\r\n" +
                            "        });\r\n" +
                            "\r\n" +
                            "        infowindow.open(map, marker);\r\n" +
                            "      }\r\n" +
                            "\r\n" +
                            "      function updatePanoVisibility()\r\n" +
                            "      {\r\n" +
                            "        if (validPosition) pano.style.visibility = 'visible';\r\n" +
                            "        else               pano.style.visibility = 'hidden';\r\n" +
                            "      };\r\n" +
                            "\r\n" +
                            "      function setPanoVisible()\r\n" +
                            "      {\r\n" +
                            "        validPosition         = true;\r\n" +
                            "        pano.style.visibility = 'visible';\r\n" +
                            "      };\r\n" +
                            "\r\n" +
                            "      function calculateHeading(data, status)\r\n" +
                            "      {\r\n" +
                            "        if (status === google.maps.StreetViewStatus.OK)\r\n" +
                            "        {\r\n" +
                            "          <!-- calculate heading -->\r\n" +
                            "          var panoHeading = google.maps.geometry.spherical.computeHeading(\r\n" +
                            "                              data.location.latLng, panorama.position);\r\n" +
                            "\r\n" +
                            "          var pov = {heading:panoHeading, pitch:0};\r\n" +
                            "          panorama.setPov(pov);\r\n" +
                            "        }\r\n" +
                            "      }\r\n" +
                            "\r\n" +
                            "    </script>\r\n" +
                            "    <script src='https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyCe_nb3fEA-cOmB3RChqZUx0hdYxae8tYw&callback=initMap'\r\n" +
                            "            async defer></script>\r\n" +
                            "  </body>\r\n" +
                            "</html>\r\n";

                        // allow up to 20 temp html files for each callType before reusing them
                        if (htmlCount == 20) htmlCount = 1;

                        string callTypeStr = callType.Replace('\\','-').Replace('/','-').Replace(' ','-');

                        string htmlTempDir  = System.IO.Path.GetTempPath() + "WestTel\\";
                        string htmlFilename = htmlTempDir +  callTypeStr + "-" + htmlCount.ToString() + ".html";

                        System.IO.Directory.CreateDirectory(htmlTempDir);
                        System.IO.FileStream htmlFile = System.IO.File.Create(htmlFilename);

                        byte[] htmlBytes = Encoding.ASCII.GetBytes(mapHtmlStr);
                        htmlFile.Write(htmlBytes, 0, htmlBytes.Length);
                        htmlFile.Close();

			            Thread processThread = new Thread(new ParameterizedThreadStart(launchProcess));
			            processThread.Name = "Map Thread";
			            processThread.Start(htmlFilename);

                        ++htmlCount;
                    }

                    // TODO messages: send to controller?  controller.MapALI(getCurrentRow());
                }
            }
            catch {}
        }

        private void launchProcess(object processFilename)
        {
            System.Diagnostics.Process.Start(processFilename.ToString());
        }

        public void abandonedButtonClick(object sender, EventArgs e)
        {
            if (abandonedButton.Enabled) sendToController("TakeControl", getCurrentRow()["Call_ID"].ToString());
        }

        public void callbackButtonClick(object sender, EventArgs e)
        {
            if ( ! callbackButton.Enabled) return;

            string callbackNumber  = null;
            string callbackDisplay = null;

DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    DataRowView row = getCurrentRow();

                    if (row != null)
                    {
                        callbackNumber  = row["Callback"].ToString();
                        callbackDisplay = row["CallbackDisplay"].ToString();
                    }
                }
                catch {callbackNumber = null;}
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);

            if (callbackNumber == null) return;

            DialogResult result = MessageBox.Show(this, (Translate)("Are you sure you want to callback ") + callbackDisplay + "?",
                    Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

            if (result == DialogResult.Yes) sendDialToController(callbackNumber);
        }

        public void connectButtonClick(object sender, EventArgs e)
        {
            if ( ! connectButton.Enabled) return;

            CallState callState = CallState.Undefined;

DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
                lock (DataModel.callEventsSyncRoot)
                {
                    try
                    {
                        DataRowView row = getCurrentRow();

                        if (row != null) callState = (CallState)Convert.ToInt32(row["CallStateCode"]);
                    }
                    catch {}
                }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);

            if (callState == CallState.Ringing) controller.sendAnswerCommand();
            else                                controller.sendPickupCommand();
        }

        public void bargeButtonClick(object sender, EventArgs e)
        {
            if ( ! bargeButton.Enabled) return;

            CallState callState = CallState.Undefined;
            string    callId = "";

DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
                lock (DataModel.callEventsSyncRoot)
                {
                    try
                    {
                        DataRowView row = getCurrentRow();

                        if (row != null)
                        {
                            callState = (CallState)Convert.ToInt32(row["CallStateCode"]);
                            callId = row["Call_ID"].ToString();
                        }
                    }
                    catch {}
                }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);

            if (callState == CallState.Connected || callState == CallState.OnHold)
            {
                string optionalAttributes = " Position=\"" + CurrentPosition.ToString().PadLeft(2, '0') + "\"";

                sendToController("Barge", callId, "", optionalAttributes);
            }
        }

        public void flashHookButtonClick(object sender, EventArgs e)
        {
            if ( ! flashHookButton.Enabled) return;

            controller.sendFlashHookCommand();
        }

        public void disconnectButtonClick(object sender, EventArgs e)
        {
            if ( ! disconnectButton.Enabled) return;

            DialogResult result = MessageBox.Show(this,
                (Translate)("Hang-Up the currently selected call?"),
                Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes) controller.sendHangupCommand();
        }

        public void holdCheckedChanged(object sender, EventArgs e)
        {
            if ( ! holdButton.Enabled) return;

            // TODO gui: Decide whether Hold should be a toggle or a button
            //if (sender == holdMenu)
            //{
            //    holdButton.Checked = holdMenu.Checked;
            //    return;
            //}
            //else
            //{
            //    holdMenu.Checked = holdButton.Checked;
            //}

            //if (holdButton.Checked)
            //{
            //    holdButton.Text = (Translate)("Off-Hold");
            //    holdMenu.  Text = (Translate)("Off-Hold");

            //    holdButton.ToolTipText = (Translate)("Take Call Off Hold");

            //    holdButton.Image = Properties.Resources.ConsoleOffHoldIcon.ToBitmap();
            //    holdMenu.  Image = Properties.Resources.ConsoleOffHoldIcon.ToBitmap();
            //}
            //else
            //{
            //    holdButton.Text = (Translate)("Hold");
            //    holdMenu.  Text = (Translate)("Hold");

            //    holdButton.ToolTipText = (Translate)("Put Call On Hold");

            //    holdButton.Image = Properties.Resources.ConsoleHoldIcon.ToBitmap();
            //    holdMenu.  Image = Properties.Resources.ConsoleHoldIcon.ToBitmap();
            //}

            controller.sendHoldCommand(); // TODO messages: should hold send boolean?
        }

        private void participantButtonClick(object sender, EventArgs e)
        {
            // TODO gui: show participant info
            MessageBox.Show(this, (Translate)("Participant Information is not implemented yet"),
                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnTDDClick(object sender, EventArgs e)
        {
            if (tddButton.Enabled && tddButton.Visible) controller.ShowTDD();
        }

        private void btnSMSClick(object sender, EventArgs e)
        {
            if (smsButton.Enabled && smsButton.Visible) controller.ShowSMS();
        }

        private void irrButtonClick(object sender, EventArgs e)
        {
            if (irrButton.Enabled && irrButton.Visible) controller.ShowIRR();
        }

        private void btnPhonebookClick(object sender, EventArgs e)
        {
            if (phonebookButton.Enabled && phonebookButton.Visible) controller.ShowPhonebook();
        }
        
        private void btnAlertsClick(object sender, EventArgs e)
        {
            if (alertsButton.Enabled && alertsButton.Visible) controller.ViewAlarms();
        }

        private void btnPSAPStatusClick(object sender, EventArgs e)
        {
            if (psapButton.Enabled && psapButton.Visible) controller.ViewPSAPStatus();
        }

        private void btnColorEditorClick(object sender, EventArgs e)
        {
            if (colorEditorMenu.Enabled) controller.ShowColorEditor();
        }

        private void aboutMenu_Click(object sender, EventArgs e)
        {
            Splash.ShowAbout(getCurrentPosition());
        }

        private void usersManualMenu_Click(object sender, EventArgs e)
        {
            Console.ShowUsersManual();
        }

        #region Form Event Handlers

        [DllImport("WtsApi32.dll")]
        private static extern bool WTSRegisterSessionNotification(IntPtr hWnd, [MarshalAs(UnmanagedType.U4)]int dwFlags);

        [DllImport("WtsApi32.dll")]
        private static extern bool WTSUnRegisterSessionNotification(IntPtr hWnd);

        [DllImport("WtsApi32.dll")]
        private static extern bool WTSQuerySessionInformation(IntPtr hServer, int SessionId, int WTSInfoClass, out IntPtr ppBuffer, out uint pBytesReturned);

        [DllImport("kernel32.dll")]
        private static extern int WTSGetActiveConsoleSessionId();

        [DllImport("wtsapi32.dll")]
        private static extern void WTSFreeMemory(IntPtr memory);

        private const int WM_QUERYENDSESSION   = 0x0011;
        private const int WM_ENDSESSION        = 0x0016;
        private const int WM_WTSSESSION_CHANGE = 0x02B1;

        private const int NOTIFY_FOR_THIS_SESSION = 0;
        private const int NOTIFY_FOR_ALL_SESSIONS = 1;

        private const int WTS_CONSOLE_CONNECT    = 0x1;
        private const int WTS_CONSOLE_DISCONNECT = 0x2;
        private const int WTS_REMOTE_CONNECT     = 0x3;
        private const int WTS_REMOTE_DISCONNECT  = 0x4;
        private const int WTS_SESSION_LOGON      = 0x5;
        private const int WTS_SESSION_LOGOFF     = 0x6;
        private const int WTS_SESSION_LOCK       = 0x7;
        private const int WTS_SESSION_UNLOCK     = 0x8;

        private bool sessionChangesRegistered = false;
        private int  registrationAttempts     = 0;

        private void registerForSessionChangeEvents()
        {
            if (sessionChangesRegistered) return;

            // catch session change events
            sessionChangesRegistered = WTSRegisterSessionNotification(Handle, NOTIFY_FOR_ALL_SESSIONS);

            if (controller != null)
            {
                if (sessionChangesRegistered) Controller.DebugWindowMsg("Registered successfully for session events");
                else                          Controller.DebugWindowMsg("Failed to register for session events");
            }

            ++registrationAttempts;

            if (sessionChangesRegistered)
            {
                if (registrationAttempts > 5)
                {
                    Controller.sendAlertToController("Successfully registered Session Change Events",
                        Console.LogType.Info, Console.LogID.Init);
                }
            }
            else if (registrationAttempts == 5)
            {
                Controller.sendAlertToController("Failed to register Session Change Events (will keep trying...)",
                    Console.LogType.Warning, Console.LogID.Init);
            }
        }

        private static bool sessionActive = true;

        protected override void WndProc(ref Message m)
        {
            try
            {
                switch (m.Msg)
                {
                    case WM_ENDSESSION:
    
                        if (m.WParam.ToInt32() != 0 && ! controller.OkToStop) Controller.ExitApplication(controller);
                        break;

                    case WM_WTSSESSION_CHANGE:

                        string typeStr = null;

                        bool logon  = false;
                        bool logoff = false;

                        switch (m.WParam.ToInt32())
                        {
                            case WTS_SESSION_LOCK:       typeStr = "Session Lock";       logoff = true; break;
                            case WTS_CONSOLE_DISCONNECT: typeStr = "Console Disconnect"; logoff = true; break;
                            case WTS_REMOTE_DISCONNECT:  typeStr = "Remote Disconnect";  logoff = true; break;
                            case WTS_SESSION_LOGOFF:     typeStr = "Session Logoff";     logoff = true; break;

                            case WTS_SESSION_UNLOCK:     typeStr = "Session Unlock";     logon  = true; break;
                            case WTS_CONSOLE_CONNECT:    typeStr = "Console Connect";    logon  = true; break;
                            case WTS_REMOTE_CONNECT:     typeStr = "Remote Connect";     logon  = true; break;
                            case WTS_SESSION_LOGON:      typeStr = "Session Logon";      logon  = true; break;

                            default:                     typeStr = "Unknown Type [" + m.WParam.ToString() + "]"; break;
                        }

                        int eventSessionId   = WTSGetActiveConsoleSessionId();
                        int currentSessionId = Process.GetCurrentProcess().SessionId;

                        bool forThisSession = (eventSessionId == currentSessionId);

                        if (eventSessionId == -1)
                        {
                            if (logoff && sessionActive) forThisSession = true;
                        }

                        if (forThisSession)
                        {
Controller.DebugWindowMsg("[" + typeStr + "] for " + sessionUsername + " (session:" + eventSessionId + ")" + "(current session:" + currentSessionId + ")");
                        }
                        else
                        {
Controller.DebugWindowMsg("[" + typeStr + "] for session:" + eventSessionId + "(current session:" + currentSessionId + ")");
                        }

                        if (logon)
                        {
                            if (forThisSession) handleLogOn();

                            if (socket == null)
                            {
                                if (forThisSession)
                                {
                                    Controller.sendAlertToController("GUI Connect (Session Change Event, Type:" +
                                        typeStr + " - Session:" + eventSessionId + "curSes:" + currentSessionId +")",
                                        Console.LogType.Info, Console.LogID.Connection);
                                }
                                else
                                {
                                    Controller.sendAlertToController("Session Change Event detected " +
                                        "for another user (no action taken), Type:" +
                                        typeStr + " - Session:" + eventSessionId + "curSes:" + currentSessionId + ")",
                                        Console.LogType.Info, Console.LogID.Connection);
                                }
                            }
                        }
                        else if (logoff)
                        {
                            if (socket != null)
                            {
                                if (forThisSession)
                                {
                                    Controller.sendAlertToController("GUI Disconnect (Session Change Event, Type: " +
                                        typeStr + " - Session:" + eventSessionId + "curSes:" + currentSessionId + ")",
                                        Console.LogType.Info, Console.LogID.Connection);
                                }
                                else
                                {
                                    Controller.sendAlertToController("Session Change Event detected " +
                                        "for another user (no action taken), Type:" +
                                        typeStr + " - Session:" + eventSessionId + "curSes:" + currentSessionId + ")",
                                        Console.LogType.Info, Console.LogID.Connection);
                                }
                            }

                            if (forThisSession) handleLogOff();
                        }

                        break;
                }
            }
            catch {}

            base.WndProc(ref m);
        }

        private void handleLogOn()
        {
            try
            {
                sessionActive = true;

                if (socket == null) SendHeartbeat(null);
            }
            catch {}
        }

        private void handleLogOff()
        {
            killConnection();
        }

        public void killConnection()
        {
            try
            {
                sessionActive = false;

                if (heartbeatThread != null) heartbeatThread.Abort();

                closeSocket();

                if (controller != null) controller.XmlMessageServer.Stop();
Controller.DebugWindowMsg("  XmlMessageServer.Stop");

                if (heartbeatTimer != null) heartbeatTimer.Change(Timeout.Infinite, Timeout.Infinite);
            }
            catch {}
        }

        private string getUsername(int sessionId)
        {
            try
            {
                IntPtr WTS_CURRENT_SERVER_HANDLE = (IntPtr)null;
                int    WTSUserName               = 5;

                IntPtr buffer;
                uint   nBytes = 0;

                bool success = WTSQuerySessionInformation(WTS_CURRENT_SERVER_HANDLE , sessionId, WTSUserName, out buffer, out nBytes);

                if (success)
                {
                    string username = "";

                    for (int i = 0; i < nBytes - 1; ++i)
                    {
                        username += (char)Marshal.ReadByte(buffer, i);
                    }

                    WTSFreeMemory(buffer);

                    return username;
                }
            }
            catch {}

            return "[Unknown Username for Session:" + sessionId + "]";
        }
        #endregion

        #region RawDisplay Event Handlers
        private void RawDisplay_Enter(object sender, System.EventArgs e)
        {
DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    //if (RawDisplay.SelectionLength == 0)
                    {
                        // TODO* gui: decide if the focus should be allowed to stay in the ALI Text Box (this allows Ctrl+C to work) ... maybe use a timer here?

                        ConsoleGrid currentGrid = getCurrentGrid();
                        if (currentGrid != null) currentGrid.Focus();
                    }
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
        }

        private void RawDisplay_TextChanged(object sender, System.EventArgs e)
        {
            bool hasText = (RawDisplay.TextLength > 0);
            ButtonEnabled(printButton, hasText);
        }
        #endregion

        #region Grid Methods

        private void Console_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    e.Handled = true;
                    this.Close();
                    break;
                case Keys.F10:
                    e.Handled = true;
                    break;
            }            
        }

        private void Console_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 1)
                this.Close();

        }
        #endregion


        #region Printing
        private void printDoc_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            printerFont = new Font("Lucida Console", fontSize );
            printerBrush = new SolidBrush(Color.Black);
            LinePosition = TopMargin;

            printerFont = new Font(printerFont,FontStyle.Bold);
            WriteToPrinter(e.Graphics, "Call Detail");
            printerFont = new Font(printerFont,FontStyle.Regular);

            e.Graphics.DrawLine(Pens.Black,new PointF(LeftMargin,LinePosition),new PointF(e.PageBounds.Width - LeftMargin,LinePosition));
            LinePosition += 5;
//            DataRowView row = getCurrentRow();
//            WriteToPrinter(e.Graphics, "   Print Date/Time:" + DateTime.Now.Date.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() );

//            if (row != null)
//            {
//                WriteToPrinter(e.Graphics, "    Call Date/Time:" + row.Cells["Start"].Value);
//                WriteToPrinter(e.Graphics, "      Phone Number:" + row.Cells["Phone"].Value.ToString());
//            }

            LinePosition += 20;
//            printerFont = new Font(printerFont, FontStyle.Underline);
//            WriteToPrinter(e.Graphics, "ANI");
            printerFont = new Font(printerFont,FontStyle.Regular);

            foreach (string LineOfText in RawDisplay.Lines) 
                WriteToPrinter(e.Graphics, LineOfText);
        }

        public void WriteToPrinter(Graphics graphic, string Text)
        {
            graphic.DrawString(Text,printerFont, printerBrush, (float) LeftMargin, LinePosition);
            LinePosition += printerFont.Height + 5;
        }

        private void printMenu_Click(object sender, EventArgs e)
        {
            if (printSetup.ShowDialog() == DialogResult.OK) printDoc.Print();
        }

        private void resetMenu_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(this,
                (Translate)("Are you sure you want to reset the Console?") + "\n" + (Translate)("This will restart the application."),
                Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

            //if (result == DialogResult.Yes) resetConsole();
            if (result == DialogResult.Yes) restartApplication();
        }

        private static int resetMsgId = 1;

        private void resetConsole()
        {
DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    // inject a ClearConsole message for this position
                    controller.XmlMessageServer.ProcessMessage(
                        "<?xml version=\"1.0\" encoding=\"utf-8\"?><Event id=\"\" XMLspec=\"" +
                        XMLMessage.SPEC_VERSION + "\" ><ClearConsole Position=\"" +
                        CurrentPosition.ToString().PadLeft(2, '0') + "\"/></Event>");

                    sendToController(getXMLHeader("Reset", resetMsgId++) + "<ReloadConsole /></Event>");

                    // TODO gui: reload and reapply settings
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
        }

        private void restartApplication()
        {
            string exePath = System.Windows.Forms.Application.ExecutablePath.Replace('\\', '/');

            Controller.restartApplication(exePath);
        }

        private string debugScript = null;

        private void debugScriptMenu_Click(object unusedSender, EventArgs unusedArgs)
        {
            try
            {
                // if not recording a debug script, start
                if (debugScript == null)
                {
                    DialogResult result = MessageBox.Show(this,
                        (Translate)("Are you sure you want to start recording a debug script?"),
                        Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.No) return;

                    debugScript = "";
                    debugScriptMenu.Text = (Translate)("Stop Recording &Debug Script...");

                    controller.XmlMessageServer.LogXML += recordInboundDebugScript;
                }
                // else, ask to save it
                else
                {
                    string debugScriptToSave = debugScript;

                    // stop recording the script
                    debugScript = null;
                    debugScriptMenu.Text = "Start Recording &Debug Script...";

                    controller.XmlMessageServer.LogXML -= recordInboundDebugScript;

                    if (debugScriptToSave.Length == 0)
                    {
                        MessageBox.Show(this, (Translate)("There was nothing recorded to save."),
                            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                        return;
                    }

                    // save the recorded script
                    SaveFileDialog saveFileDialog = new SaveFileDialog();

                    // setup initial directory to My Documents/WestTel/Scripts
                    string initialDirectory =
                        Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\WestTel";

                    if ( ! System.IO.Directory.Exists(initialDirectory))
                    {
                        System.IO.Directory.CreateDirectory(initialDirectory);
                    }

                    initialDirectory += "\\Scripts";

                    if ( ! System.IO.Directory.Exists(initialDirectory))
                    {
                        System.IO.Directory.CreateDirectory(initialDirectory);
                    }

                    saveFileDialog.AddExtension       = true;
                    saveFileDialog.AutoUpgradeEnabled = true;
                    saveFileDialog.DefaultExt         = ".txt";
                    saveFileDialog.FileName           = "Debug Script.txt";
                    saveFileDialog.InitialDirectory   = initialDirectory;
                    saveFileDialog.Title              = "Save Debug Script";

                    DialogResult result = saveFileDialog.ShowDialog(this);

                    if (result == DialogResult.OK)
                    {
                        saveFileDialog.OpenFile().Write(
                            Encoding.ASCII.GetBytes(debugScriptToSave), 0, debugScriptToSave.Length);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(this, e.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void recordInboundDebugScript(string xml, string name)
        {
            if (debugScript != null)
            {
                name = name.Replace ("\r\n"," ").Replace('\n',' ');
                xml  = xml.Replace  ("\r\n"," ").Replace('\n',' ');

                debugScript += "[" + name + "]\r\n" + xml + "\r\n\r\n";
            }
        }


        #endregion

        private void Console_Activated(object sender, System.EventArgs e)
        {
            FixFocus();
            System.Windows.Forms.Application.DoEvents();
            FixFocus();
        }

        public void FixFocus()
        {
            try
            {
                ConsoleGrid currentGrid = getCurrentGrid();
                if (currentGrid != null) currentGrid.Focus();
            }
            catch {}
        }

        private void Console_Enter(object sender, System.EventArgs e)
        {
            try
            {
                ConsoleGrid currentGrid = getCurrentGrid();
                if (currentGrid != null) currentGrid.Focus();
            }
            catch {}
        }

        public void NewActive(string positions)
        {
            try
            {
                BeginInvoke(new newActiveDelegate(newActive), positions);
            }
            catch {try{newActive(positions);} catch{}}
        }

        private delegate void newActiveDelegate(string positions);

        private void newActive(string PositionsActive)
        {
DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    // go to the onConnectTab
                    if (PositionsActive != null &&
                        PositionsActive.Contains("\tP" + CurrentPosition.ToString() + "\t"))
                    {
                        if ( ! onConnectTabIfAbandoned || tabWidget.SelectedTab == abandonedTab)
                        {
                            tabWidget.SelectedTab = onConnectTab;
                        }
                    }
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
        }
        private void TabWidget_MouseMove(object sender, MouseEventArgs e)
        {
            tabWidget.Invalidate();
        }

        private void drawTab(object sender, DrawItemEventArgs args)
        {
            try
            {
                if (args == null || args.Index == -1) return;

                TabPage   curTab  = tabWidget.TabPages  [args.Index];
                Rectangle tabRect = tabWidget.GetTabRect(args.Index);

                bool isSelected = (args.State & DrawItemState.Selected) != 0;
                bool isHovered  = (tabRect.Contains(tabWidget.PointToClient(Control.MousePosition)));

                // set string format
                StringFormat stringFormat  = new StringFormat(StringFormatFlags.NoClip);
                stringFormat.LineAlignment = StringAlignment.Center;
                stringFormat.Alignment     = StringAlignment.Center;

                Font       font  = args.Font;
                SolidBrush brush = new SolidBrush(isSelected ? Color.White :
                                                  isHovered  ? SystemColors.MenuHighlight :
                                                               SystemColors.Control);
                if ( ! curTab.Text.Contains("(0)"))
                {
                    font = new Font(args.Font, FontStyle.Bold);

                    // TODO gui: Change the Tab color?  (Bill didn't like it)
                    //if (curTab == lineTab)
                    //{
                    //    object ringingCallObj = dataModel.ds.Tables["CallEvents"].Compute("Count(Call_ID)",
                    //        "CallStateCode = " + CallState.Ringing.GetHashCode().ToString());

                    //    // if there are ringing calls, change the color
                    //    if (((int)ringingCallObj) != 0) brush = new SolidBrush(ringingColor);
                    //}
                    //else if (curTab == connectedTab) brush = new SolidBrush(connectedStyle.BackColor);
                    //else if (curTab == abandonedTab) brush = new SolidBrush(abandonedStyle.BackColor);
                }

                // draw background
                args.Graphics.FillRectangle(brush, tabRect);

                // draw text
                args.Graphics.DrawString(
                    curTab.Text, font, SystemBrushes.ControlText, tabRect, stringFormat);
            }
            catch
            {
                try
                {
                    args.Graphics.DrawString(
                        tabWidget.TabPages[args.Index].Text, args.Font, SystemBrushes.ControlText, args.Bounds);
                }
                catch {}
            }
        }

        private Settings.ColorInfo getColorInfo(DataRowView row, bool isSelected, bool isHovered)
        {
            try
            {
                bool isActive = false;

                CallRowInfo.State state = CallRowInfo.getState(isSelected, isHovered);

                switch ((CallState)Convert.ToUInt16(row["CallStateCode"]))
                {
                    // active
                    case CallState.Ringing:      isActive = true; break;
                    case CallState.Dialing:      isActive = true; break;
                    case CallState.Connected:    isActive = true; break;
                    case CallState.OnHold:       isActive = true; break;

                    // inactive
                    case CallState.Disconnected: isActive = false; break;
                    case CallState.Canceled:     isActive = false; break;

                    // manual bid
                    case CallState.ManualBid:
                    {
                        return callColorDictionary[
                            new CallRowInfo(CallRowInfo.CallType.ManualBid, false, state)];
                    }

                    // abandoned
                    case CallState.Abandoned:
                    {
                        return callColorDictionary[
                            new CallRowInfo(CallRowInfo.CallType.Abandoned, true, state)];
                    }

                    case CallState.AbandonedCleared:
                    {
                        return callColorDictionary[
                            new CallRowInfo(CallRowInfo.CallType.Abandoned, false, state)];
                    }

                    // unknown
                    default:
                    {
                        return callColorDictionary[
                            new CallRowInfo(CallRowInfo.CallType.Unknown, false, state)];
                    }
                }

                string callTypeStr = row["CallType"].ToString();

                if (callTypeStr.Contains((Translate)("9-1-1")) ||
                    callTypeStr.Contains((Translate)("911")))
                {
                    return callColorDictionary[
                        new CallRowInfo(CallRowInfo.CallType._911, isActive, state)];
                }
                else
                {
                    return callColorDictionary[
                        new CallRowInfo(CallRowInfo.CallType.Admin, isActive, state)];
                }
            }
            catch {}

            return new Settings.ColorInfo(Color.White, Color.White, Color.Black, false);
        }

        private void PaintGridRowBackground(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            // if row or cell is invalid, return
            if (sender == null || e.RowIndex < 0) return;

DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    ConsoleGrid CurGrid = (ConsoleGrid)sender;

                    DataGridViewRow GRow = CurGrid.Rows[e.RowIndex];
                    DataRowView     row  = null;

                    if (GRow != null && GRow.DataBoundItem != null) row = (DataRowView)GRow.DataBoundItem;

                    // color rows appropriately
                    bool isSelected = GRow.Selected;
                    bool isHovered  = CurGrid.isHovered(e.RowIndex);

                    if (sender != null)
                    {
                        Settings.ColorInfo colorInfo =
                            getColorInfo(row, isSelected, isHovered);

                        DataRowView selectedRow =
                            (DataRowView)CurGrid.SelectedRows[0].DataBoundItem;

                        Settings.ColorInfo selectedColorInfo =
                            getColorInfo(selectedRow, true, false);

                        ((Grid)sender).paintRowBackground(
                            e, colorInfo, selectedColorInfo, isSelected);
                    }
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
        }

        private void PostPaintGridRow(object sender, DataGridViewRowPostPaintEventArgs e)
        {
DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    //((Grid)sender).paintSelectionOverlay(e.RowIndex, e.RowBounds, e.Graphics);
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
        }

        private const int GRID_CELL_X_MARGIN = 0;
        private const int GRID_CELL_Y_MARGIN = 3;
        private const int GRID_ROW_HEIGHT    = 15;

        private void PaintGridCell(object sender, DataGridViewCellPaintingEventArgs e)
        {
            // if row or cell is invalid, return
            if (sender == null || e.RowIndex < 0 || e.Value == null) return;

DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    ConsoleGrid     CurGrid = (ConsoleGrid)sender;
                    DataGridViewRow GRow    = CurGrid.Rows[e.RowIndex];

                    bool isSelected = GRow.Selected;
                    bool isHovered  = CurGrid.isHovered(e.RowIndex);

                    DataRowView row = null;

                    if (GRow != null && GRow.DataBoundItem != null)
                    {
                        row = (DataRowView)GRow.DataBoundItem;
                    }

                    Settings.ColorInfo colorInfo = getColorInfo(row, isSelected, isHovered);

                    bool  isCallActive = false;

                    switch ((CallState)Convert.ToUInt16(row["CallStateCode"]))
                    {
                        // active
                        case CallState.Ringing:   isCallActive = true; break;
                        case CallState.Dialing:   isCallActive = true; break;
                        case CallState.Connected: isCallActive = true; break;
                        case CallState.OnHold:    isCallActive = true; break;
                        case CallState.Abandoned: isCallActive = true; break;
                    }

                    Brush activePosBrush = new SolidBrush(colorInfo.foregroundColor);

                    // if the alpha was not explicitly specified, set it automatically
                    if ( ! colorInfo.foregroundColorExplicitAlpha)
                    {
                        if ( ! isCallActive && ! isSelected)
                        {
                            activePosBrush = new SolidBrush(
                                Color.FromArgb(120, colorInfo.foregroundColor));
                        }
                    }

                    e.Handled = true;

                    int x = e.CellBounds.X + GRID_CELL_X_MARGIN;
                    int y = e.CellBounds.Y + GRID_CELL_Y_MARGIN;

                    int contentWidth = e.CellBounds.Width;

                    FontStyle fontStyle = colorInfo.bold ? FontStyle.Bold : FontStyle.Regular;
                    Font      font      = new Font(e.CellStyle.Font, fontStyle);

                    Brush brush = activePosBrush;

                    // if this is the Type / Number column
                    if (e.ColumnIndex == CurGrid.Columns["CallTypeNumberCol"].Index)
                    {
                        // NOTE: this ignores Alignment passed in and assumes Left alignment

                        bool callbackVerified = toBoolean(row["CallbackVerified"]);

                        char[] newLineSep = {'\n'};
                        string[] strList = e.FormattedValue.ToString().Split(newLineSep, 2);

                        StringFormat stringFormat  = new StringFormat();
                        stringFormat.LineAlignment = StringAlignment.Center;
                        stringFormat.Trimming      = StringTrimming.EllipsisCharacter;

                        // if there is only one row
                        if (strList.Length == 1)
                        {
                            // if callback NOT verified and only one row, italicize it
                            if ( ! callbackVerified) font = new Font(font, FontStyle.Italic | fontStyle);

                            e.Graphics.DrawString(strList[0], font, brush, e.CellBounds, stringFormat);
                        }
                        else
                        {
                            RectangleF bounds = new RectangleF(x, y, contentWidth, GRID_ROW_HEIGHT);

                            // if callback NOT verified, italicize first row
                            if ( ! callbackVerified)
                            {
                                e.Graphics.DrawString(strList[0],
                                    new Font(font, FontStyle.Italic | fontStyle), brush, bounds, stringFormat);
                            }
                            else
                            {
                                e.Graphics.DrawString(strList[0], font, brush, bounds, stringFormat);
                            }

                            y += GRID_ROW_HEIGHT;

                            string str = e.FormattedValue.ToString().Remove(0, strList[0].Length + 1);

                            bounds = new RectangleF(x, y, contentWidth, GRID_ROW_HEIGHT);
                            e.Graphics.DrawString(str, font, brush, bounds, stringFormat);
                        }
                    }
                    // else if this is the Position column
                    else if (e.ColumnIndex == CurGrid.Columns["PositionCol"].Index)
                    {
                        // NOTE: this ignores Alignment passed in and assumes Left alignment

                        // setup brushes
                        Brush inactivePosBrush = new SolidBrush(Color.FromArgb(90, colorInfo.foregroundColor));

                        string displayStr = e.Value.ToString();

                        char[] delimeter = {','};
                        string[] displayStrList = displayStr.Split(delimeter, StringSplitOptions.RemoveEmptyEntries);

                        bool containsPositionsOnHold   = false;
                        bool containsInactivePositions = false;

                        for (int i = 0; i < displayStrList.Length; ++i)
                        {
                            string curStr = "\t" + displayStrList[i] + "\t";

                            if (holdFlashState == HoldFlashState.SUBDUED && row["PositionsOnHold"].ToString().Contains(curStr))
                            {
                                containsPositionsOnHold   = true;
                                containsInactivePositions = true;
                                break;
                            }

                            if (  row["PositionsActive"].ToString().Length != 0 &&
                                ! row["PositionsActive"].ToString().Contains(curStr))
                            {
                                containsInactivePositions = true;
                            }
                        }

                        const int RIGHT_SPACE_FOR_LINE_NUM = 8;

#if DEBUG
                        bool debugParticipants = false;
#else
                        bool debugParticipants = false;
#endif

                        // if nothing is inactive, simple draw
                        if ( ! containsInactivePositions && debugParticipants)
                        {
                            displayStr = "";

                            for (int i = 0; i < displayStrList.Length; ++i)
                            {
                                // add brackets around Call Originator
                                if (row != null && displayStrList[i] == row["CallOriginator"].ToString())
                                {
                                    displayStr += "[" + displayStrList[i] + "]";
                                }
                                else
                                {
                                    displayStr += displayStrList[i];
                                }

                                // if this is not the last string, append a comma
                                if (i != displayStrList.Length - 1) displayStr += ",";
                            }

                            StringFormat stringFormat  = new StringFormat();
                            stringFormat.LineAlignment = StringAlignment.Center;
                            stringFormat.Trimming      = StringTrimming.EllipsisCharacter;
                            stringFormat.FormatFlags   = StringFormatFlags.LineLimit;

                            // subtract RIGHT_SPACE_FOR_LINE_NUM to give a little space between the line number
                            RectangleF cellBounds = e.CellBounds;
                            cellBounds.Width -= RIGHT_SPACE_FOR_LINE_NUM;

                            e.Graphics.DrawString(displayStr, font, brush, cellBounds, stringFormat);
                        }
                        else
                        {
                            double GRID_CHAR_WIDTH     = colorInfo.bold ?  8.0 : 7.0;
                            double GRID_COMMA_WIDTH    = colorInfo.bold ?  4.0 : 4.0;
                            double GRID_BRACKETS_WIDTH = colorInfo.bold ? 10.0 : 10.0;

                            int nCommas = displayStrList.Length - 1;
                            int nChars  = displayStr.Length - nCommas;

                            // subtract RIGHT_SPACE_FOR_LINE_NUM to give a little space between the line number
                            contentWidth -= RIGHT_SPACE_FOR_LINE_NUM;

                            // we assume there will be brackets
                            double displayStrWidth =
                                GRID_BRACKETS_WIDTH + (nChars) * GRID_CHAR_WIDTH + nCommas * GRID_COMMA_WIDTH;

                            // if the string will fit on one row
                            if (displayStrWidth <= contentWidth) y += (int)(GRID_ROW_HEIGHT / 2.0 + 0.5);

                            bool onSecondRow = false;
                            bool flashHold   = false;

                            for (int i = 0; i < displayStrList.Length; ++i)
                            {
                                string origStr     = displayStrList[i];
                                string curStr      = origStr;
                                double curStrWidth = curStr.Length * GRID_CHAR_WIDTH;

                                // add brackets around Call Originator
                                if (row != null && origStr == row["CallOriginator"].ToString())
                                {
                                    curStr = "[" + origStr + "]";
                                    curStrWidth += GRID_BRACKETS_WIDTH;
                                }

                                // if this won't fit
                                if (x + curStrWidth > e.CellBounds.Right - RIGHT_SPACE_FOR_LINE_NUM)
                                {
                                    if ( ! onSecondRow)
                                    {
                                        onSecondRow = true;

                                        y += GRID_ROW_HEIGHT;
                                        x = e.CellBounds.X + GRID_CELL_X_MARGIN;
                                    }
                                    else
                                    {
                                        e.Graphics.DrawString("...", font, activePosBrush, x, y);
                                        break;
                                    }
                                }
                                else if (onSecondRow)
                                {
                                    if (flashHold) e.Graphics.DrawString(",", font, inactivePosBrush, x, y);
                                    else           e.Graphics.DrawString(",", font, brush,            x, y);

                                    x += (int)GRID_COMMA_WIDTH;
                                }

                                // if there are inactive positions, and this position is not in the active list
                                if (containsInactivePositions && ! row["PositionsActive"].ToString().Contains("\t" + origStr + "\t"))
                                {
                                    brush = inactivePosBrush;
                                }
                                else if (brush != activePosBrush)
                                {
                                    brush = activePosBrush;
                                }

                                flashHold = false;

                                // if hold flash state is SUBDUED, check for Hold status
                                if (row != null && containsPositionsOnHold)
                                {
                                    // if this position is on hold, draw appropriately
                                    if (row["PositionsOnHold"].ToString().Contains("\t" + origStr + "\t")) flashHold = true;
                                }

                                if (flashHold) e.Graphics.DrawString(curStr, font, inactivePosBrush, x, y);
                                else           e.Graphics.DrawString(curStr, font, brush,            x, y);

                                x += (int)(curStrWidth);

                                if ( ! onSecondRow)
                                {
                                    if (i != displayStrList.Length - 1)
                                    {
                                        if (flashHold) e.Graphics.DrawString(",", font, inactivePosBrush, x, y);
                                        else           e.Graphics.DrawString(",", font, brush,            x, y);

                                        x += (int)GRID_COMMA_WIDTH;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        StringFormat stringFormat  = new StringFormat();
                        stringFormat.LineAlignment = StringAlignment.Center;
                        stringFormat.Alignment     = StringAlignment.Near;
                        stringFormat.Trimming      = StringTrimming.EllipsisCharacter;

                        Rectangle bounds = e.CellBounds;
                        bounds.X += 1;

                        // make room for selection arrow
                        if (e.ColumnIndex == 0) bounds.X += 15;

                        // TODO gui: decide whether to use Center on any columns

                        e.Graphics.DrawString(e.FormattedValue.ToString(), font, brush, bounds, stringFormat);
                    }
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
        }

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        static extern System.IntPtr LoadCursorFromFile(String lpFileName);

        private Cursor rightMouseCursor = null;

        // remember entering positions cells
        private void Grid_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    ConsoleGrid CurGrid = (ConsoleGrid)sender;

                    if (CurGrid != null && e.RowIndex >= 0 && e.ColumnIndex >= 0 &&
                        CurGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                    {
                        gridToolTipColumnIndex = e.ColumnIndex;
                        gridToolTipRowIndex    = e.RowIndex;

                        if (callTypeToolTipIsNecessary())
                        {
                            if (rightMouseCursor == null)
                            {
                                // set the cursor to non-null so that if it doesn't work, don't keep trying
                                rightMouseCursor = Cursors.Default;

                                string applicationDir = System.IO.Path.GetDirectoryName(
                                    System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                                applicationDir = applicationDir.Replace("file:\\", "");

                                System.IntPtr cursorHandle = LoadCursorFromFile(applicationDir + "\\RightMouse.cur");
                                rightMouseCursor = new Cursor(cursorHandle);
                            }

                            CurGrid.Cursor = rightMouseCursor;
                        }
                    }
                }
                catch
                {
                    gridToolTipColumnIndex = gridToolTipRowIndex = -1;
                }
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
        }

        private bool callTypeToolTipIsNecessary()
        {
            ConsoleGrid CurGrid = getCurrentGrid();

            if (CurGrid != null && gridToolTipColumnIndex != -1 && gridToolTipRowIndex != -1)
            {
                bool toolTipIsNecessary = (gridToolTipColumnIndex == CurGrid.Columns["PositionCol"].Index);

                if (gridToolTipColumnIndex == CurGrid.Columns["CallTypeNumberCol"].Index)
                {
                    string cellDisplayText = CurGrid.Rows[gridToolTipRowIndex].
                        Cells[gridToolTipColumnIndex].FormattedValue.ToString();

                    char[] newLineSep = {'\n'};
                    string[] strList = cellDisplayText.Split(newLineSep, 2);

                    if (strList.Length > 2)
                    {
                        toolTipIsNecessary = true;
                    }
                    else
                    {
                        double cellWidth = CurGrid.Rows[gridToolTipRowIndex].
                            Cells[gridToolTipColumnIndex].ContentBounds.Width;

                        Font cellFont = CurGrid.Rows[gridToolTipRowIndex].
                            Cells[gridToolTipColumnIndex].Style.Font;

                        foreach (string str in strList)
                        {
                            if (TextRenderer.MeasureText(str, cellFont).Width > cellWidth) toolTipIsNecessary = true;
                        }
                    }
                }

                return toolTipIsNecessary;
            }

            return false;
        }


        // monitor leaving position cells
        private void Grid_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (gridToolTipColumnIndex == e.ColumnIndex &&
                    gridToolTipRowIndex    == e.RowIndex)
                {
                    gridToolTipColumnIndex = gridToolTipRowIndex = -1;

                    toolTip.Hide(this);

                    ConsoleGrid CurGrid = (ConsoleGrid)sender;
                    if (CurGrid != null) CurGrid.Cursor = Cursors.Default;

                    if (positionToolTipTimer != null) positionToolTipTimer.Change(Timeout.Infinite, Timeout.Infinite);
                }
            }
            catch {}
        }

        private void Grid_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                ConsoleGrid CurGrid = (ConsoleGrid)sender;

                // if currently in a Position cell, set a timer
                if (gridToolTipColumnIndex != -1 && gridToolTipRowIndex != -1 &&
                    CurGrid != null && gridToolTipColumnIndex == CurGrid.Columns["PositionCol"].Index)
                {
                    if (positionToolTipTimer == null)
                    {
                        positionToolTipTimer = new System.Threading.Timer(ShowPositionToolTip, null, Timeout.Infinite, Timeout.Infinite);
                    }

                    positionToolTipTimer.Change(150, Timeout.Infinite);
                }
            }
            catch {}
        }

        private void Grid_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                gridToolTipColumnIndex = gridToolTipRowIndex = -1;
                toolTip.Hide(this);

                if (positionToolTipTimer != null) positionToolTipTimer.Change(Timeout.Infinite, Timeout.Infinite);
            }
            catch {}
        }

        private void Grid_CellContextMenuStripNeeded(object sender, DataGridViewCellContextMenuStripNeededEventArgs e)
        {
DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    ConsoleGrid CurGrid = (ConsoleGrid)sender;

                    if (CurGrid != null && e.RowIndex >= 0 && e.ColumnIndex >= 0 &&
                        CurGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                    {
                        gridToolTipColumnIndex = e.ColumnIndex;
                        gridToolTipRowIndex    = e.RowIndex;

                        try {BeginInvoke(new showCellToolTipDelegate(showCellToolTip));}
                        catch {}
                    }
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
        }

        private void ShowPositionToolTip(object obj)
        {
            // TODO gui: tooltips
            try {/*BeginInvoke(new showPositionToolTipDelegate(showPositionToolTip));*/}
            catch {}
        }

        private delegate void showCellToolTipDelegate();

        private void showCellToolTip()
        {
DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    ConsoleGrid CurGrid = getCurrentGrid();

                    if (CurGrid != null && gridToolTipColumnIndex != -1 && gridToolTipRowIndex != -1)
                    {
                        if (gridToolTipColumnIndex == CurGrid.Columns["PositionCol"].Index)
                        {
                            conferenceLegend = null;

                            DataGridViewRow GRow = CurGrid.Rows[gridToolTipRowIndex];

                            if (GRow != null && GRow.DataBoundItem != null)
                            {
                                DataRowView row  = (DataRowView)GRow.DataBoundItem;
                                conferenceLegend = (ArrayList)row["ConferenceLegend"];
                            }

                            if (conferenceLegend == null || conferenceLegend.Count == 0) return;

                            int nRows = conferenceLegend.Count;

                            // calcualte size needed for Tool Tip
                            int maxNumberLength = 35; // leave at least enough space for a phone number
                            int maxNameLength   = 50;

                            for (int row = 0; row < nRows; ++row)
                            {
                                ConferenceInfo info = ((ConferenceInfo)conferenceLegend[row]);

                                if (info.callback. Length > maxNumberLength) maxNumberLength = info.callback. Length;
                                if (info.name.     Length > maxNameLength)   maxNameLength   = info.name.     Length;
                            }

                            // remember size ratio of Number and Name columns
                            conferenceLegendNumberColRatio = (float)maxNumberLength / (float)(maxNameLength + maxNumberLength);

                            string sizeTip = "".PadRight(11 + (maxNumberLength + maxNameLength) / 2, 'x');                     // horizontal
                            sizeTip = sizeTip.PadRight(sizeTip.Length + (int)((double)nRows * 2.0), '\n').Replace("\n","\nx"); // vertical

                            toolTip.OwnerDraw = true;
                            toolTip.Show(sizeTip, this, (MousePosition.X - Location.X) + 15, (MousePosition.Y - Location.Y) - 15);
                            toolTip.OwnerDraw = false;
                        }
                        else if (gridToolTipColumnIndex == CurGrid.Columns["CallTypeNumberCol"].Index)
                        {
                            if (callTypeToolTipIsNecessary())
                            {
                                // TODO gui: translate cellText tooltip?
                                string cellText = CurGrid.Rows[gridToolTipRowIndex].Cells[gridToolTipColumnIndex].Value.ToString();

                                toolTip.Show(cellText, this, (MousePosition.X - Location.X) + 15, (MousePosition.Y - Location.Y) - 15);
                            }
                        }
                    }
                }
                catch
                {
                    gridToolTipColumnIndex = gridToolTipRowIndex = -1;
                }
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
        }

        private static float MARGIN    = 4;
        private static float MARGINx2  = MARGIN * 2;
        private static float KEY_WIDTH = 35;

        private static float COL_1 = 0;
        private static float COL_2 = COL_1 + KEY_WIDTH;

        private void toolTip_Draw(object sender, DrawToolTipEventArgs e)
        {
            try
            {
                e.DrawBackground();

                StringFormat stringFormatCenter  = new StringFormat();
                stringFormatCenter.Alignment     = StringAlignment.Center;
                stringFormatCenter.LineAlignment = StringAlignment.Center;
                stringFormatCenter.Trimming      = StringTrimming.EllipsisCharacter;

                StringFormat stringFormatLeft    = new StringFormat();
                stringFormatLeft.LineAlignment   = StringAlignment.Center;
                stringFormatLeft.Trimming        = StringTrimming.EllipsisCharacter;

                Font normalFont = new Font(e.Font, FontStyle.Regular);
                Font boldFont   = new Font(e.Font, FontStyle.Bold);

                int nRows = conferenceLegend.Count;

                float ROW_HEIGHT      = (float)(e.Bounds.Height - 1) / (float)nRows;
                float TEXT_ROW_HEIGHT = ROW_HEIGHT - MARGINx2;

                float NUMBER_WIDTH = (e.Bounds.Width - COL_2) * conferenceLegendNumberColRatio;
                float COL_3        = COL_2 + NUMBER_WIDTH;
                float NAME_WIDTH   = e.Bounds.Width - 1 - COL_3;

                float y = 0;

                for (int row = 0; row < nRows; ++row)
                {
                    ConferenceInfo info = ((ConferenceInfo)conferenceLegend[row]);

                    string key    = info.key;
                    string number = info.callback;
                    string name   = info.name;

                    // draw cell borders
                    e.Graphics.DrawRectangle(new Pen(Brushes.Black), COL_1, y, KEY_WIDTH,    ROW_HEIGHT);
                    e.Graphics.DrawRectangle(new Pen(Brushes.Black), COL_2, y, NUMBER_WIDTH, ROW_HEIGHT);
                    e.Graphics.DrawRectangle(new Pen(Brushes.Black), COL_3, y, NAME_WIDTH,   ROW_HEIGHT);

                    // draw text
                    y += MARGIN;

                    RectangleF bounds = new RectangleF(COL_1 + MARGIN, y, KEY_WIDTH - MARGINx2, TEXT_ROW_HEIGHT);
                    e.Graphics.DrawString(key, boldFont, Brushes.Black, bounds, stringFormatCenter);

                    bounds = new RectangleF(COL_2 + MARGIN, y, NUMBER_WIDTH - MARGINx2, TEXT_ROW_HEIGHT);
                    e.Graphics.DrawString(number, normalFont, Brushes.Black, bounds, stringFormatLeft);

                    bounds = new RectangleF(COL_3 + MARGIN, y, NAME_WIDTH   - MARGINx2, TEXT_ROW_HEIGHT);
                    e.Graphics.DrawString(name, normalFont, Brushes.Black, bounds, stringFormatLeft);

                    y += MARGIN + TEXT_ROW_HEIGHT;
                }
            }
            catch {}
        }

        private void tabChanged(object unusedSender, TabControlEventArgs args)
        {
            ConsoleGrid currentGrid = getCurrentGrid();
            if (currentGrid != null) currentGrid.GridSelectionChanged(getCurrentRow());
        }

        private bool in_tabWidget_KeyDown = false;

        private void tabWidget_KeyDown(object unusedSender, KeyEventArgs e)
        {
            ConsoleGrid currentGrid = getCurrentGrid();
            if (currentGrid == null) return;

            if ( ! tabWidget.Focused)
            {
                // if Up is pressed when on the first row, change focus
                if (e.KeyCode == Keys.Up && currentGrid.Rows[0].Selected) tabWidget.Focus();

                return;
            }

            if (in_tabWidget_KeyDown) return;
            in_tabWidget_KeyDown = true;

            try
            {
                switch (e.KeyCode)
                {
                    case Keys.Up:
				        e.Handled = true;
                        currentGrid.Focus();
                        System.Windows.Forms.Application.DoEvents();
                        SendKeys.Send("{UP}");
                        break;
                    case Keys.Down:
				        e.Handled = true;
                        currentGrid.Focus();
                        System.Windows.Forms.Application.DoEvents();
                        SendKeys.Send("{DOWN}");
                        break;
                    case Keys.PageUp:
				        e.Handled = true;
                        currentGrid.Focus();
                        System.Windows.Forms.Application.DoEvents();
                        SendKeys.Send("{PGUP}");
                        break;
                    case Keys.PageDown:
				        e.Handled = true;
                        currentGrid.Focus();
                        System.Windows.Forms.Application.DoEvents();
                        SendKeys.Send("{PGDN}");
                        break;
                }
            }
            catch {}

            in_tabWidget_KeyDown = false;
        }

        private bool in_handleLeftRightKeyFromGrid = false;

        public void handleLeftRightKeyFromGrid(Keys keyCode)
        {
            if (tabWidget.Focused) return;

            if (in_handleLeftRightKeyFromGrid) return;
            in_handleLeftRightKeyFromGrid = true;

            tabWidget.Focus();
            System.Windows.Forms.Application.DoEvents();

            try
            {
                switch (keyCode)
                {
                    case Keys.Left:
                        SendKeys.Send("{LEFT}");
                        break;
                    case Keys.Right:
                        SendKeys.Send("{RIGHT}");
                        break;
                }      
            }
            catch {}

            in_handleLeftRightKeyFromGrid = false;
        }

        private const int SPEAKER = 0;
        private const int MIC     = 1;

// TODO SIPVoipSDK: hopefully this will not be necessary once integrated with system sound
//speakerSlider.Value = 100;
//micSlider.    Value = 100;

  //          speakerVolumeBar.   Visible = true;
  //          volumeBarSeparator. Visible = true;
  //          micVolumeBar.       Visible = true;

        private void onVolumeUpdated(int type, int level)
        {
            try {BeginInvoke(new OnVolumeBarUpdateDelegate(OnVolumeBarUpdate), type, level);}
            catch {}
        }

        private delegate void OnVolumeBarUpdateDelegate(int type, int level);

        private void OnVolumeBarUpdate(int type, int level)
        {
            try
            {
                if (level < 0)     level = 0;
                if (level > 30000) level = 30000;

                // convert level to a curve so that quiet noises are emphasized
                if (level > 0) level = (int)(Math.Pow(level / 30000.0, 0.4) * 1000.0 + 0.5);

                if (type == MIC) micVolumeBar.    Value = level;
                else             speakerVolumeBar.Value = level;
            }
            catch {}
        }

        // TODO SIPVoipSDK:
        //private void micSlider_ValueChanged     (object sender, EventArgs e) {if (voipManager != null) voipManager.RecordVolume   = micSlider.Value;}
        //private void speakerSlider_ValueChanged (object sender, EventArgs e) {if (voipManager != null) voipManager.PlaybackVolume = speakerSlider.Value;}

        //private void micCheckBox_Changed(object sender, EventArgs e)
        //{
        //    if (voipManager != null) voipManager.RecordMuted = micCheckBox.Checked ? 0 : 1;

        //    micLabel.  Enabled = micCheckBox.Checked;
        //    micSlider. Enabled = micCheckBox.Checked;
        //}

        //private void speakerCheckBox_Changed(object sender, EventArgs e)
        //{
        //    if (voipManager != null) voipManager.PlaybackVolume = speakerCheckBox.Checked ? 0 : 1;

        //    speakerLabel.  Enabled = speakerCheckBox.Checked;
        //    speakerSlider. Enabled = speakerCheckBox.Checked;
        //}
    }
}
