using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Xml;

namespace WestTel.E911
{
	/// <summary>
	/// Summary description for MainForm.
	/// </summary>
	public class CannedMessages : System.Windows.Forms.Form // BaseForm
	{
        public delegate void OnCannedMessagesClosing();
        public event OnCannedMessagesClosing CannedMessagesClosing;

        public delegate void OnCannedMessageSelected(string Message);
        public event OnCannedMessageSelected CannedMessageSelected;

		Controller controller;
        string     messagesTypeStr;

		private static int WM_QUERYENDSESSION = 0x11;

		#region Windows Control Declaraitons

        private System.Windows.Forms.ListBox lbMessages;
        private Button btnSelectMessage;
        private Button btnCancel;
#endregion

		#region Constructor / Deconstructor
		public CannedMessages(Controller c, XmlNode cannedMessagesXml, MessageWindow.WindowType windowType)
		{
			controller      = c;
            messagesTypeStr = windowType.ToString();
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
            setTranslatableText();
            LoadMessages(cannedMessagesXml);

            if (windowType == MessageWindow.WindowType.TDD) Icon = Properties.Resources.TDDMessagesIcon;
            else                                            Icon = Properties.Resources.SMSMessagesIcon;
		}

		#endregion

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lbMessages = new System.Windows.Forms.ListBox();
            this.btnSelectMessage = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbMessages
            // 
            this.lbMessages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbMessages.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMessages.ItemHeight = 16;
            this.lbMessages.Location = new System.Drawing.Point(7, 7);
            this.lbMessages.Name = "lbMessages";
            this.lbMessages.Size = new System.Drawing.Size(340, 276);
            this.lbMessages.TabIndex = 5;
            this.lbMessages.SelectedIndexChanged += new System.EventHandler(this.lbMessages_SelectedIndexChanged);
            this.lbMessages.DoubleClick += new System.EventHandler(this.lbMessages_DoubleClick);
            this.lbMessages.KeyUp += new System.Windows.Forms.KeyEventHandler(this.lbMessages_KeyUp);
            // 
            // btnSelectMessage
            // 
            this.btnSelectMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectMessage.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectMessage.Location = new System.Drawing.Point(178, 291);
            this.btnSelectMessage.Name = "btnSelectMessage";
            this.btnSelectMessage.Size = new System.Drawing.Size(104, 23);
            this.btnSelectMessage.TabIndex = 9;
            this.btnSelectMessage.Text = "Select Message";
            this.btnSelectMessage.Click += new System.EventHandler(this.btnSelectMessage_Click);
            this.btnSelectMessage.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btnSelectMessage_KeyUp);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(288, 291);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(59, 23);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // CannedMessages
            // 
            this.AcceptButton = this.btnSelectMessage;
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            this.BackColor = System.Drawing.SystemColors.Window;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(354, 324);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSelectMessage);
            this.Controls.Add(this.lbMessages);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CannedMessages";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Closing += new System.ComponentModel.CancelEventHandler(this.CannedMessages_Closing);
            this.Load += new System.EventHandler(this.CannedMessages_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CannedMessages_KeyDown);
            this.ResumeLayout(false);

		}
		#endregion

        private void setTranslatableText()
        {
            this.btnSelectMessage.Text = (Translate)("Select Message");
            this.btnCancel.Text        = (Translate)("Cancel");
        }

		private void LoadMessages(XmlNode cannedMessagesXml)
		{
			try
			{
				lbMessages.Items.Clear();

                if (cannedMessagesXml != null)
                {
                    foreach (XmlNode node in cannedMessagesXml.ChildNodes)
				    {
					    string text = node.Attributes[0].Value;
					    lbMessages.Items.Add(text);
				    }
                }
				
				lbMessages.Focus();
			}
			catch (Exception ex)
			{
				Logging.ExceptionLogger("Error in " + messagesTypeStr +
                    " CannedMessages.ReadData", ex, Console.LogID.Init);

				string eMessage = "There was an error reading in " + messagesTypeStr +
                    " CannedMessages.xml:" + Environment.NewLine + ex.Message;
				
				if (ex.InnerException != null)
					eMessage += Environment.NewLine + "More Info:" + ex.InnerException.Message;

				MessageBox.Show(this, eMessage, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
			}

			if (lbMessages.Items.Count > 0)
			{
				lbMessages.SelectedIndex = 0;
				btnSelectMessage.Enabled = true;
			}
			else
            {
				btnSelectMessage.Enabled = false;
            }
		}


		#region Form Event Handlers
		private void CannedMessages_Load(object sender, System.EventArgs e)
		{
            if (Owner != null)
            {
                Location = new System.Drawing.Point((Owner.Width  - Width)  / 2 + Owner.Left,
                                                    (Owner.Height - Height) / 2 + Owner.Top);
            }

			this.Text = (Translate)(messagesTypeStr) + (Translate)(" Messages");
			lbMessages.Focus();
		}

		private void CannedMessages_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			e.Cancel = true;
			Hide();
            if (CannedMessagesClosing != null) CannedMessagesClosing(); // Inform app that the window is closing
			// cannedForm.MessageClosed();
		}

		#endregion

		#region ListBox Event Handlers
		private void lbMessages_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter) btnSelectMessage.Focus();
		}
		private void lbMessages_DoubleClick(object sender, System.EventArgs e)
		{
			btnSelectMessage.PerformClick();
		}
		#endregion
		
		#region Button Event Handlers
		private void btnSelectMessage_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter && lbMessages.SelectedValue != null)
				btnSelectMessage.PerformClick();
		}

		private void btnSelectMessage_Click(object sender, System.EventArgs e)
		{
			if (lbMessages.SelectedItem != null)
			{
                CannedMessageSelected(lbMessages.SelectedItem.ToString());
				//cannedForm.Message = lbMessages.SelectedItem.ToString();
			}
			//MessageBox.Show("Message:" + lbMessages.SelectedItem);
			Close();
		}

		#endregion

		private void lbMessages_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			object item = lbMessages.SelectedItem;
			btnSelectMessage.Enabled = (item != null);

		}

		protected override void WndProc(ref Message m)
		{
			if (m.Msg == WM_QUERYENDSESSION)
			{
                if (controller.OkToStop) Controller.ExitApplication(controller);
			}
			base.WndProc (ref m);
		}

		private void CannedMessages_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Escape) 
			{
				e.Handled = true;
				Close();
			}
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}
	}
}
