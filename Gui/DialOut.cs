﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Experient.E911
{
	/// <summary>
	/// Dialog box for entering # for dial out.
	/// </summary>
	public class DialOut : System.Windows.Forms.Form
	{
        private static int WM_QUERYENDSESSION = 0x11;
        private IContainer components;
        Controller controller;

        private TableLayoutPanel tableLayoutPanel1;
        private Button button9;
        private Button buttonAsterisk;
        private Button button7;
        private Button button5;
        private Button button6;
        private Button button2;
        private Button button1;
        private Button button4;
        private Button button3;
        private Button button0;
        private Button buttonPound;
        private Button button8;

        private Button callButton;
        private Button cancelButton;
        public  ComboBox phoneField;
        private AutoCompleteStringCollection dialHistory;

        private Microsoft.Win32.RegistryKey regSettings = null;

		public DialOut(Controller c)
		{
            controller = c;

            // Required for Windows Form Designer support
			InitializeComponent();

            try
            {
                dialHistory = new AutoCompleteStringCollection();

                regSettings = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(
                    "Software\\" + Application.CompanyName + "\\" + Application.ProductName);

                if (regSettings != null)
                {
                    string[] dialHistoryStrList;
                    dialHistoryStrList = (string[])regSettings.GetValue("DialHistory");

                    if (dialHistoryStrList != null)
                    {
                        dialHistory.AddRange(dialHistoryStrList);
                        phoneField.AutoCompleteCustomSource = dialHistory;
                        phoneField.Items.AddRange(dialHistoryStrList);
                    }
                }
            }
            catch
            {
                regSettings = null;
            }

            // TODO: SIPVoipSDK: add 'Clear History' to the phone field popup menu
            //try
            //{
            //    phoneField.ContextMenu = new ContextMenu();
            //    phoneField.ContextMenu.MenuItems.Add("&Clear History", clearDialHistory);
            //}
            //catch {}
        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if (disposing)
			{
				if (components != null) components.Dispose();
			}

            base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialOut));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.button0 = new System.Windows.Forms.Button();
            this.buttonPound = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.buttonAsterisk = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.callButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.phoneField = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.Controls.Add(this.button0, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.buttonPound, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.button8, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.button9, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.buttonAsterisk, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.button7, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.button5, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.button6, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.button2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.button1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.button4, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.button3, 0, 0);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 42);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(272, 227);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // button0
            // 
            this.button0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button0.Location = new System.Drawing.Point(94, 172);
            this.button0.Margin = new System.Windows.Forms.Padding(4);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(82, 51);
            this.button0.TabIndex = 10;
            this.button0.TabStop = false;
            this.button0.Tag = "0";
            this.button0.UseVisualStyleBackColor = true;
            this.button0.Click += new System.EventHandler(this.dialButton_Clicked);
            this.button0.Paint += new System.Windows.Forms.PaintEventHandler(this.buttonPaint);
            // 
            // buttonPound
            // 
            this.buttonPound.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonPound.Location = new System.Drawing.Point(184, 172);
            this.buttonPound.Margin = new System.Windows.Forms.Padding(4);
            this.buttonPound.Name = "buttonPound";
            this.buttonPound.Size = new System.Drawing.Size(84, 51);
            this.buttonPound.TabIndex = 11;
            this.buttonPound.TabStop = false;
            this.buttonPound.Tag = "#";
            this.buttonPound.UseVisualStyleBackColor = true;
            this.buttonPound.Click += new System.EventHandler(this.dialButton_Clicked);
            this.buttonPound.Paint += new System.Windows.Forms.PaintEventHandler(this.buttonPaint);
            // 
            // button8
            // 
            this.button8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button8.Location = new System.Drawing.Point(94, 116);
            this.button8.Margin = new System.Windows.Forms.Padding(4);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(82, 48);
            this.button8.TabIndex = 7;
            this.button8.TabStop = false;
            this.button8.Tag = "8\r\nt u v";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.dialButton_Clicked);
            this.button8.Paint += new System.Windows.Forms.PaintEventHandler(this.buttonPaint);
            // 
            // button9
            // 
            this.button9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button9.Location = new System.Drawing.Point(184, 116);
            this.button9.Margin = new System.Windows.Forms.Padding(4);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(84, 48);
            this.button9.TabIndex = 8;
            this.button9.TabStop = false;
            this.button9.Tag = "9\r\nw x y z";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.dialButton_Clicked);
            this.button9.Paint += new System.Windows.Forms.PaintEventHandler(this.buttonPaint);
            // 
            // buttonAsterisk
            // 
            this.buttonAsterisk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonAsterisk.Font = new System.Drawing.Font("Consolas", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.buttonAsterisk.Location = new System.Drawing.Point(4, 172);
            this.buttonAsterisk.Margin = new System.Windows.Forms.Padding(4);
            this.buttonAsterisk.Name = "buttonAsterisk";
            this.buttonAsterisk.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
            this.buttonAsterisk.Size = new System.Drawing.Size(82, 51);
            this.buttonAsterisk.TabIndex = 9;
            this.buttonAsterisk.TabStop = false;
            this.buttonAsterisk.Tag = "*";
            this.buttonAsterisk.UseVisualStyleBackColor = true;
            this.buttonAsterisk.Click += new System.EventHandler(this.dialButton_Clicked);
            this.buttonAsterisk.Paint += new System.Windows.Forms.PaintEventHandler(this.buttonPaint);
            // 
            // button7
            // 
            this.button7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button7.Location = new System.Drawing.Point(4, 116);
            this.button7.Margin = new System.Windows.Forms.Padding(4);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(82, 48);
            this.button7.TabIndex = 6;
            this.button7.TabStop = false;
            this.button7.Tag = "7\r\np q r s";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.dialButton_Clicked);
            this.button7.Paint += new System.Windows.Forms.PaintEventHandler(this.buttonPaint);
            // 
            // button5
            // 
            this.button5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button5.Location = new System.Drawing.Point(94, 60);
            this.button5.Margin = new System.Windows.Forms.Padding(4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(82, 48);
            this.button5.TabIndex = 4;
            this.button5.TabStop = false;
            this.button5.Tag = "5\r\nj k l";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.dialButton_Clicked);
            this.button5.Paint += new System.Windows.Forms.PaintEventHandler(this.buttonPaint);
            // 
            // button6
            // 
            this.button6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button6.Location = new System.Drawing.Point(184, 60);
            this.button6.Margin = new System.Windows.Forms.Padding(4);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(84, 48);
            this.button6.TabIndex = 5;
            this.button6.TabStop = false;
            this.button6.Tag = "6\r\nm n o";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.dialButton_Clicked);
            this.button6.Paint += new System.Windows.Forms.PaintEventHandler(this.buttonPaint);
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.Location = new System.Drawing.Point(94, 4);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(82, 48);
            this.button2.TabIndex = 1;
            this.button2.TabStop = false;
            this.button2.Tag = "2\r\na b c";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.dialButton_Clicked);
            this.button2.Paint += new System.Windows.Forms.PaintEventHandler(this.buttonPaint);
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(4, 4);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(82, 48);
            this.button1.TabIndex = 0;
            this.button1.TabStop = false;
            this.button1.Tag = "1\r\n ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.dialButton_Clicked);
            this.button1.Paint += new System.Windows.Forms.PaintEventHandler(this.buttonPaint);
            // 
            // button4
            // 
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.Location = new System.Drawing.Point(4, 60);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(82, 48);
            this.button4.TabIndex = 3;
            this.button4.TabStop = false;
            this.button4.Tag = "4\r\ng h i";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.dialButton_Clicked);
            this.button4.Paint += new System.Windows.Forms.PaintEventHandler(this.buttonPaint);
            // 
            // button3
            // 
            this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button3.Location = new System.Drawing.Point(184, 4);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(84, 48);
            this.button3.TabIndex = 2;
            this.button3.TabStop = false;
            this.button3.Tag = "3\r\nd e f";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.dialButton_Clicked);
            this.button3.Paint += new System.Windows.Forms.PaintEventHandler(this.buttonPaint);
            // 
            // 
            // callButton
            // 
            this.callButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.callButton.AutoSize = true;
            this.callButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.callButton.Enabled = false;
            this.callButton.Location = new System.Drawing.Point(168, 275);
            this.callButton.Name = "callButton";
            this.callButton.Size = new System.Drawing.Size(50, 23);
            this.callButton.TabIndex = 5;
            this.callButton.Text = "&Call";
            this.callButton.UseVisualStyleBackColor = true;
            this.callButton.Click += new System.EventHandler(this.callButtonClick);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.AutoSize = true;
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(224, 275);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(50, 23);
            this.cancelButton.TabIndex = 6;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButtonClick);
            // 
            // phoneField
            // 
            this.phoneField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.phoneField.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.phoneField.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.phoneField.Font = new System.Drawing.Font("Century Gothic", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.phoneField.Location = new System.Drawing.Point(8, 9);
            this.phoneField.Name = "phoneField";
            this.phoneField.Size = new System.Drawing.Size(266, 27);
            this.phoneField.TabIndex = 3;
            this.phoneField.Text = "(123) 456-7890";
            this.phoneField.SelectedIndexChanged += new System.EventHandler(this.phoneField_SelectedIndexChanged);
            this.phoneField.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.phoneField_KeyPress);
            this.phoneField.KeyUp += new System.Windows.Forms.KeyEventHandler(this.phoneField_KeyRelease);
            // 
            // DialOut
            // 
            this.AcceptButton = this.callButton;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(281, 306);
            this.Controls.Add(this.phoneField);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.callButton);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DialOut";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Dial Pad";
            this.Activated += new System.EventHandler(this.onActivated);
            this.Load += new System.EventHandler(this.onLoad);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

        protected override void WndProc(ref Message m)
		{
			if (m.Msg == WM_QUERYENDSESSION)
			{
				if (!controller.OkToStop) Controller.ExitApplication(controller);
			}
			base.WndProc (ref m);
		}

        private void onLoad(object sender, EventArgs e)
        {
            if (Owner != null)
            {
                Location = new System.Drawing.Point((Owner.Width  - Width)  / 2 + Owner.Left,
                                                    (Owner.Height - Height) / 2 + Owner.Top);
            }
        }

        private void onActivated(object sender, EventArgs e)
        {
            phoneField.SelectAll();
			callButton.Focus();
        }

        public void setDefaults(string defaultDestNumber)
        {
            phoneField.Text = defaultDestNumber;

            callButton.Enabled = (phoneField.Text.Length > 0);
        }

		private void phoneField_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            e.Handled = ! phoneFieldCharIsValid(e.KeyChar);
        }

        private bool phoneFieldCharIsValid(char c)
		{
			Logging.AppTrace( "KeyChar:" + Convert.ToInt32(c));

            // TODO: SIPVoipSDK: are all characters okay for SIP addresses?

            return true;
		}

		private void phoneField_KeyRelease(object sender, System.Windows.Forms.KeyEventArgs e)
		{
            callButton.Enabled = (phoneField.Text.Length > 0);
		}

        private void phoneField_SelectedIndexChanged(object sender, EventArgs e)
        {
            callButton.Enabled = (phoneField.Text.Length > 0);
        }

        private void dialButton_Clicked(object sender, EventArgs e)
        {
            if (sender == null) return;

            Button button = (Button)sender;

            if (phoneFieldCharIsValid(button.Text[0]))
            {
                // TODO: SIPVoipSDK: make the selected text actually get replaced when a dial button is pressed
                phoneField.SelectedText = button.Text[0].ToString();

                callButton.Enabled = true;
            }
        }

        private void buttonPaint(object sender, PaintEventArgs args)
        {
            Button   button   = (Button)sender;
            Graphics graphics = args.Graphics;

            StringFormat stringFormat = new StringFormat();

            switch (button.TextAlign)
            {
                case ContentAlignment.TopLeft:      stringFormat.Alignment = StringAlignment.Near;   stringFormat.LineAlignment = StringAlignment.Near;   break;
                case ContentAlignment.TopCenter:    stringFormat.Alignment = StringAlignment.Center; stringFormat.LineAlignment = StringAlignment.Near;   break;
                case ContentAlignment.TopRight:     stringFormat.Alignment = StringAlignment.Far;    stringFormat.LineAlignment = StringAlignment.Near;   break;
                case ContentAlignment.MiddleLeft:   stringFormat.Alignment = StringAlignment.Near;   stringFormat.LineAlignment = StringAlignment.Center; break;
                case ContentAlignment.MiddleCenter: stringFormat.Alignment = StringAlignment.Center; stringFormat.LineAlignment = StringAlignment.Center; break;
                case ContentAlignment.MiddleRight:  stringFormat.Alignment = StringAlignment.Far;    stringFormat.LineAlignment = StringAlignment.Center; break;
                case ContentAlignment.BottomLeft:   stringFormat.Alignment = StringAlignment.Near;   stringFormat.LineAlignment = StringAlignment.Center; break;
                case ContentAlignment.BottomCenter: stringFormat.Alignment = StringAlignment.Center; stringFormat.LineAlignment = StringAlignment.Far;    break;
                case ContentAlignment.BottomRight:  stringFormat.Alignment = StringAlignment.Far;    stringFormat.LineAlignment = StringAlignment.Far;    break;
                default:                            stringFormat.Alignment = StringAlignment.Center; stringFormat.LineAlignment = StringAlignment.Far;    break;
            }

            string[] sep = {"\r\n"};
            string[] strings = button.Tag.ToString().Split(sep, StringSplitOptions.None);

            Font boldFont = new Font(button.Font.FontFamily, button.Font.Size + 2, FontStyle.Bold, button.Font.Unit);

            if (strings.GetLength(0) == 1)
            {
                graphics.DrawString(strings[0], boldFont, new SolidBrush(button.ForeColor), args.ClipRectangle, stringFormat);
            }
            else
            {
                graphics.DrawString(strings[0] + "\r\n ", boldFont,    new SolidBrush(button.ForeColor), args.ClipRectangle, stringFormat);
                graphics.DrawString(" \r\n" + strings[1], button.Font, new SolidBrush(button.ForeColor), args.ClipRectangle, stringFormat);
            }
        }

		private void callButtonClick(object sender, System.EventArgs e)
		{
            if (phoneField.Text.Length != 0)
            {
                // remember dialed numbers
                dialHistory.Remove(phoneField.Text);
                dialHistory.Insert(0, phoneField.Text);

                phoneField.AutoCompleteCustomSource = dialHistory;

                string[] dialHistoryStrList = new string[dialHistory.Count];

                for (int i = 0; i < dialHistory.Count; ++i) dialHistoryStrList[i] = dialHistory[i];

                phoneField.Items.Clear();
                phoneField.Items.AddRange(dialHistoryStrList);

                if (regSettings != null) regSettings.SetValue("DialHistory", dialHistoryStrList);

                controller.sendDialCommand(phoneField.Text);

                Close();
            }
        }

        private void cancelButtonClick(object sender, EventArgs e)
        {
            Close();
        }

        private void clearDialHistory(object sender, EventArgs e)
        {
            dialHistory.Clear();

            phoneField.AutoCompleteCustomSource.Clear();
            phoneField.Items.Clear();

            if (regSettings != null) regSettings.DeleteValue("DialHistory");
        }
    }
}
