using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Configuration;

namespace WestTel.E911
{
	/// <summary>
	/// Summary description for Splash.
	/// </summary>
	public class Splash : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panel1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Label copyrightLabel;
        private System.Windows.Forms.PictureBox splashImage;
		private System.Windows.Forms.Label Version;
        private Button closeButton;
        private TextBox contactTextBox;
        private System.Windows.Forms.Label labelApplicationName;

		public Splash(int psapPosition)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
            setTranslatableText();

			DateTime now = DateTime.Now;
			string ThisYear = now.Year.ToString();

            string positionStr = "";
            if (psapPosition != -1) positionStr = " - Position " + psapPosition;

			labelApplicationName.Text = Application.ProductName + positionStr;
			copyrightLabel.Text = copyrightLabel.Text.Replace("XXXX",ThisYear);

			Version.Text = (Translate)("Version: ") + Application.ProductVersion + " (XML Spec " + XMLMessage.SPEC_VERSION;

            try
            {
                foreach (System.Diagnostics.ProcessModule module in System.Diagnostics.Process.GetCurrentProcess().Modules)
                {
                    if (module.ModuleName.ToLower() == "sipvoipsdk.dll")
                    {
                        Version.Text += ", SIP SDK " + module.FileVersionInfo.ProductVersion;
                        break;
                    }
                }
            }
            catch {}

            Version.Text += ")";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Splash));
            this.panel1 = new System.Windows.Forms.Panel();
            this.Version = new System.Windows.Forms.Label();
            this.labelApplicationName = new System.Windows.Forms.Label();
            this.contactTextBox = new System.Windows.Forms.TextBox();
            this.closeButton = new System.Windows.Forms.Button();
            this.copyrightLabel = new System.Windows.Forms.Label();
            this.splashImage = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splashImage)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(59)))), ((int)(((byte)(108)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.Version);
            this.panel1.Controls.Add(this.labelApplicationName);
            this.panel1.Controls.Add(this.contactTextBox);
            this.panel1.Controls.Add(this.closeButton);
            this.panel1.Controls.Add(this.copyrightLabel);
            this.panel1.Controls.Add(this.splashImage);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.ForeColor = System.Drawing.Color.White;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(322, 279);
            this.panel1.TabIndex = 1;
            // 
            // Version
            // 
            this.Version.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Version.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.Version.Location = new System.Drawing.Point(8, 185);
            this.Version.Name = "Version";
            this.Version.Size = new System.Drawing.Size(310, 14);
            this.Version.TabIndex = 12;
            this.Version.Text = "Version:";
            this.Version.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelApplicationName
            // 
            this.labelApplicationName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelApplicationName.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.labelApplicationName.Location = new System.Drawing.Point(8, 168);
            this.labelApplicationName.Name = "labelApplicationName";
            this.labelApplicationName.Size = new System.Drawing.Size(310, 17);
            this.labelApplicationName.TabIndex = 8;
            this.labelApplicationName.Text = "WestTel PSAP Toolkit";
            this.labelApplicationName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // contactTextBox
            // 
            this.contactTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.contactTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.contactTextBox.Font = new System.Drawing.Font("Calibri", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contactTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(59)))), ((int)(((byte)(108)))));
            this.contactTextBox.Location = new System.Drawing.Point(30, 119);
            this.contactTextBox.Multiline = true;
            this.contactTextBox.Name = "contactTextBox";
            this.contactTextBox.ReadOnly = true;
            this.contactTextBox.Size = new System.Drawing.Size(277, 45);
            this.contactTextBox.TabIndex = 14;
            this.contactTextBox.TabStop = false;
            this.contactTextBox.Text = "Toll Free (855) WESTTEL (937-8835)";
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.closeButton.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.closeButton.Location = new System.Drawing.Point(220, 245);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(90, 26);
            this.closeButton.TabIndex = 13;
            this.closeButton.Text = "OK";
            this.closeButton.KeyDown += new System.Windows.Forms.KeyEventHandler(this.closeButton_KeyDown);
            // 
            // copyrightLabel
            // 
            this.copyrightLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.copyrightLabel.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.copyrightLabel.Location = new System.Drawing.Point(9, 208);
            this.copyrightLabel.Name = "copyrightLabel";
            this.copyrightLabel.Size = new System.Drawing.Size(309, 70);
            this.copyrightLabel.TabIndex = 5;
            this.copyrightLabel.Text = "Copyright � WestTel International 2002-XXXX  All rights reserved. This program is" +
    " protected by U.S. and International copyright laws as described in the licensin" +
    "g agreement.";
            // 
            // splashImage
            // 
            this.splashImage.Dock = System.Windows.Forms.DockStyle.Top;
            this.splashImage.Image = ((System.Drawing.Image)(resources.GetObject("splashImage.Image")));
            this.splashImage.Location = new System.Drawing.Point(0, 0);
            this.splashImage.Name = "splashImage";
            this.splashImage.Size = new System.Drawing.Size(320, 164);
            this.splashImage.TabIndex = 9;
            this.splashImage.TabStop = false;
            // 
            // Splash
            // 
            this.AcceptButton = this.closeButton;
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(322, 279);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MinimizeBox = false;
            this.Name = "Splash";
            this.Opacity = 0D;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splashImage)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

        private void setTranslatableText()
        {
		    this.closeButton.Text          = (Translate)("OK");
		    this.Version.Text              = (Translate)("Version:");
            this.labelApplicationName.Text = (Translate)(Application.ProductName);

            this.copyrightLabel.Text = (Translate)("Copyright") + " � " +
                Application.CompanyName + " 2002-XXXX. " + (Translate)("All rights reserved.") +
                (Translate)(" This program is protected by U.S. and International " +
                              "copyright laws as described in the licensing agreement.");
        }

		public static void ShowAbout(int position)
		{
			Splash splash = new Splash(position);
            splash.Opacity = 1.0;
			splash.ShowDialog();
            splash.Dispose();
		}

		public static void ShowSplash(int fadeInMsec, int durationMsec, int fadeOutMsec)
		{
			Splash splash = new Splash(-1);

            splash.closeButton.Dispose();
            splash.TopMost = true;

            splash.Show();

            // fade in
            for (int i = 0; i != 20; ++i) 
			{
				splash.Opacity = i * 0.05;
				splash.Refresh();

                System.Threading.Thread.Sleep(fadeInMsec / 20);
			}

            splash.Opacity = 1.0;

            System.Threading.Thread.Sleep(durationMsec);

            // fade out
            for (int i = 0; i != 20; ++i) 
			{
				splash.Opacity = 1.0 - i * 0.05;
				splash.Refresh();

                System.Threading.Thread.Sleep(fadeOutMsec / 20);
			}

            splash.Opacity = 0.0;
            splash.Dispose();
        }

		private void Splash_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			e.Handled = true;
		}

		private void closeButton_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			e.Handled = true;
			if (e.KeyCode == Keys.Escape) Close();
		}

		private void closeButton_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			e.Handled = true;
		}

		private void Splash_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			e.Handled = true;
		}
    }
}
