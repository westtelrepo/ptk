using System;
using System.Windows.Forms;

namespace WestTel.E911
{
	public class ManualALIRequest : System.Windows.Forms.Form
	{
		public MaskedTextBox phoneField;
		private Button btnCancel;
		private Button btnRequest;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
		private static int WM_QUERYENDSESSION = 0x11;

        Controller controller;

		public ManualALIRequest(Controller c)
		{
            controller = c;

			// Required for Windows Form Designer support
			InitializeComponent();
            setTranslatableText();
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.phoneField = new System.Windows.Forms.MaskedTextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnRequest = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // phoneField
            // 
            this.phoneField.AllowDrop = true;
            this.phoneField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.phoneField.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.phoneField.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phoneField.Location = new System.Drawing.Point(104, 10);
            this.phoneField.Mask = "(000) 000-0000";
            this.phoneField.Name = "phoneField";
            this.phoneField.Size = new System.Drawing.Size(115, 21);
            this.phoneField.TabIndex = 1;
            this.phoneField.TextChanged += new System.EventHandler(this.phoneField_TextChanged);
            this.phoneField.KeyUp += new System.Windows.Forms.KeyEventHandler(this.phoneField_KeyUp);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.SystemColors.Window;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCancel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(161, 105);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 22);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.closeForm);
            // 
            // btnRequest
            // 
            this.btnRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRequest.BackColor = System.Drawing.SystemColors.Window;
            this.btnRequest.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnRequest.Enabled = false;
            this.btnRequest.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnRequest.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRequest.Location = new System.Drawing.Point(80, 105);
            this.btnRequest.Name = "btnRequest";
            this.btnRequest.Size = new System.Drawing.Size(75, 22);
            this.btnRequest.TabIndex = 13;
            this.btnRequest.Text = "&Request";
            this.btnRequest.UseVisualStyleBackColor = false;
            this.btnRequest.Click += new System.EventHandler(this.closeForm);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label2.Location = new System.Drawing.Point(3, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(225, 53);
            this.label2.TabIndex = 15;
            this.label2.Text = "Your entry must be a valid telephone number.\r\nThis is for landline calls only. Us" +
    "e Rebid for\r\nother currently connected calls.";
            // 
            // label4
            // 
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label4.Location = new System.Drawing.Point(3, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 18);
            this.label4.TabIndex = 18;
            this.label4.Text = "Telephone number:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.phoneField);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(7, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(229, 93);
            this.panel1.TabIndex = 17;
            // 
            // ManualALIRequest
            // 
            this.AcceptButton = this.btnRequest;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.SystemColors.Window;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(243, 133);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnRequest);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = Properties.Resources.ConsoleManualALIRequestIcon;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ManualALIRequest";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Manual ALI Request";
            this.Activated += new System.EventHandler(this.ManualALIRequest_Activated);
            this.Load += new System.EventHandler(this.ManualALIRequest_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

        private void setTranslatableText()
        {
            this.btnCancel.Text  = (Translate)("&Cancel");
            this.btnRequest.Text = (Translate)("&Request");
            this.label2.Text     = (Translate)("Your entry must be a valid telephone number.\r\n") +
                                   (Translate)("This is for landline calls only. Use Rebid for\r\n") +
                                   (Translate)("other currently connected calls.");
            this.label4.Text     = (Translate)("Telephone number:");
            this.Text            = (Translate)("Manual ALI Request");
        }

        private void ManualALIRequest_Load(object sender, EventArgs e)
        {
            if (Owner != null)
            {
                Location = new System.Drawing.Point((Owner.Width  - Width)  / 2 + Owner.Left,
                                                    (Owner.Height - Height) / 2 + Owner.Top);
            }
        }

        private void ManualALIRequest_Activated(object sender, EventArgs e)
        {
			phoneField.Focus();
            phoneField.SelectionStart = 1;
        }

        private void closeForm(object sender, EventArgs e)
        {
            Close();
        }

        private void phoneField_TextChanged(object sender, EventArgs e)
        {
            string phoneNumber = phoneField.Text;

            phoneNumber = phoneNumber.Replace("(", "");
            phoneNumber = phoneNumber.Replace(")", "");
            phoneNumber = phoneNumber.Replace(" ", "");
            phoneNumber = phoneNumber.Replace("-", "");
            phoneNumber = phoneNumber.Replace("_", "");
            phoneNumber = phoneNumber.Replace(".", "");

            btnRequest.Enabled = (phoneNumber.Length == 10);
        }

        private void phoneField_KeyUp(object sender, KeyEventArgs e)
        {
            if (phoneField.SelectionLength == 0)
            {
                switch (e.KeyValue)
                {
                    case '0': case '1': case '2': case '3': case '4':
                    case '5': case '6': case '7': case '8': case '9':
                    {
                        if      (phoneField.SelectionStart == 4) phoneField.SelectionStart = 6;
                        else if (phoneField.SelectionStart == 9) phoneField.SelectionStart = 10;

                        break;
                    }
                    case '\b':
                    {
                        if      (phoneField.SelectionStart == 6)  phoneField.SelectionStart = 4;
                        else if (phoneField.SelectionStart == 10) phoneField.SelectionStart = 9;

                        break;
                    }
                }
            }
        }

		protected override void WndProc(ref Message m)
		{
			if (m.Msg == WM_QUERYENDSESSION)
			{
				if (!controller.OkToStop) Controller.ExitApplication(controller);
			}
			base.WndProc (ref m);
		}
	}
}
