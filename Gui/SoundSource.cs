﻿using System;
using System.IO;

namespace WestTel.E911
{
    public class SoundSource : CSCore.IWaveSource
    {
        private readonly CSCore.WaveFormat _waveFormat;
        private          MemoryStream      _stream;
        private          long              _position     = 0;
        private          long              _stopPosition = -1;

        public SoundSource(int sampleRate, int nChannels)
        {
            _waveFormat = new CSCore.WaveFormat(sampleRate, 16, nChannels);
            _stream     = new MemoryStream();
        }

        public SoundSource(int sampleRate, SoundSource oldSoundSource)
        {
            _waveFormat = new CSCore.WaveFormat(sampleRate,
                oldSoundSource.WaveFormat.BitsPerSample, oldSoundSource.WaveFormat.Channels);

            _stream       = oldSoundSource._stream;
            _position     = oldSoundSource._position;
            _stopPosition = oldSoundSource._stopPosition;

            oldSoundSource._stream = null;
        }

        public bool CanSeek { get { return true; } }

        public long Position
        {
            get
            {
                try
                {
                    // make sure the position is on a valid block boundary
                    long remainder = _position % _waveFormat.BytesPerBlock;
                    _position -= remainder;

                    return _position;
                }
                catch {return _position = 0;}
            }

            set
            {
                try
                {
                    if (value >= Length) _position = Length - _waveFormat.BytesPerBlock;
                    else if (value < 0)  _position = 0;
                    else
                    {
                        // make sure the position is on a valid block boundary
                        long remainder = value % _waveFormat.BytesPerBlock;

                        _position = value - remainder;
                    }
                }
                catch {_position = 0;}
            }
        }


        public long StopPosition
        {
            get
            {
                try
                {
                    if (_stopPosition == -1) return _stopPosition;

                    // make sure the position is on a valid block boundary
                    long remainder = _stopPosition % _waveFormat.BytesPerBlock;
                    _stopPosition -= remainder;

                    return _stopPosition;
                }
                catch {return _stopPosition = 0;}
            }

            set
            {
                try
                {
                    if (value == -1)
                    {
                        _stopPosition = value;
                    }
                    else
                    {
                        // make sure the position is on a valid block boundary
                        long remainder = value % _waveFormat.BytesPerBlock;

                        _stopPosition = value - remainder;
                    }
                }
                catch {_stopPosition = 0;}
            }
        }

        public long Length
        {
            get
            {
                if (_stream == null) return 0;
                else                 return _stream.Length;
            }
        }

        public CSCore.WaveFormat WaveFormat { get { return _waveFormat; } }

        public void addSample(long sampleId, short sample)
        {
            try
            {
                if (_stream != null)
                {
                    _stream.Seek(sampleId * _waveFormat.BytesPerSample, SeekOrigin.Begin);
                    _stream.Write(BitConverter.GetBytes(sample), 0, _waveFormat.BytesPerSample);
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR audio failed to add a sample", ex, Console.LogID.Gui);
            }
        }

        public void clear()
        {
            try
            {
                _position     = 0;
                _stopPosition = -1;

                if (_stream != null) _stream.Dispose();

                _stream = new MemoryStream();
            }
            catch {}
        }

        public int Read(byte[] buffer, int offset, int count)
        {
            if (_stream == null) return 0;

            try
            {
                if (_position + count >= Length)
                {
                    count = (int)(Length - _position - 1);

                    if (count <= 0) return 0;
                }

                // make sure the position is on a valid block boundary
                int remainder = count % _waveFormat.BytesPerBlock;
                count -= remainder;

                for (int i = 0; i != count; ++i, ++_position)
                {
                    // this will emit Stopped
                    if (_stopPosition != -1 && _position >= _stopPosition) return i;

                    buffer[offset + i] = _stream.GetBuffer()[_position];
                }

                return count;
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR audio failed to read", ex, Console.LogID.Gui);
                return 0;
            }
        }

        public void Dispose()
        {
            if (_stream != null)
            {
                _stream.Dispose();
                _stream = null;
            }
        }
    }
}
