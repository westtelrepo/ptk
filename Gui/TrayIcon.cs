using System;
using System.Drawing;
using System.Windows.Forms;

namespace WestTel.E911
{
    public class NoForm
    {
        public  System.Windows.Forms.NotifyIcon SystemTray;
        private System.Windows.Forms.ContextMenuStrip menuTray;
        public  System.Windows.Forms.ToolStripMenuItem menuTrayConsole;
        public  System.Windows.Forms.ToolStripMenuItem menuTrayPhonebook;
        public  System.Windows.Forms.ToolStripMenuItem menuTrayTDD;
        public  System.Windows.Forms.ToolStripMenuItem menuTraySMS;
        public  System.Windows.Forms.ToolStripMenuItem menuTrayColorEditor;
        public  System.Windows.Forms.ToolStripMenuItem menuTrayPSAPStatus;
        public  System.Windows.Forms.ToolStripMenuItem menuViewAlarms;
        public  System.Windows.Forms.ToolStripMenuItem menuCallback;
        public  System.Windows.Forms.ToolStripMenuItem menuConnect;
        public  System.Windows.Forms.ToolStripMenuItem menuBarge;
        public  System.Windows.Forms.ToolStripMenuItem menuFlashHook;
        public  System.Windows.Forms.ToolStripMenuItem menuHold;
        public  System.Windows.Forms.ToolStripMenuItem menuDisconnect;
        public  System.Windows.Forms.ToolStripMenuItem menuAbandon;
        public  System.Windows.Forms.ToolStripMenuItem menuRebid;
        public  System.Windows.Forms.ToolStripMenuItem menuTrayViewLog;
        public  System.Windows.Forms.ToolStripMenuItem menuTrayDebugWindow;
        private System.Windows.Forms.ToolStripSeparator menuSep0;
        private System.Windows.Forms.ToolStripSeparator menuSep1;
        private System.Windows.Forms.ToolStripSeparator menuSep2;
        private System.Windows.Forms.ToolStripSeparator menuSep3;
        private System.Windows.Forms.ToolStripSeparator menuSep4;
        private System.Windows.Forms.ToolStripSeparator menuSep5;
        private System.Windows.Forms.ToolStripMenuItem menuTrayAbout;
        private System.Windows.Forms.ToolStripMenuItem menuTrayUsersManual;
        private System.Windows.Forms.ToolStripMenuItem menuTrayExit;

        private Controller controller;

        private System.ComponentModel.IContainer components;

        public NoForm(Controller c, bool showDebugData)
        {
            controller = c;
            InitializeComponents();
            setTranslatableText();

            menuTrayDebugWindow.Visible = showDebugData;

            // hide Call menu items to make the menu simpler
            menuCallback.   Visible = false;
            menuConnect.    Visible = false;
            menuBarge.      Visible = false;
            menuFlashHook.  Visible = false;
            menuHold.       Visible = false;
            menuDisconnect. Visible = false;
            menuAbandon.    Visible = false;
            menuRebid.      Visible = false;

            // ensure the system tray icon is always visible
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "TrayManager\\Win7\\TrayManager.exe";
            startInfo.Arguments = "-p 911.exe 2";
            process.StartInfo = startInfo;
            process.Start();

            startInfo.FileName = "TrayManager\\Win8\\TrayManager.exe";
            process.StartInfo = startInfo;
            process.Start();
        }

        public void enableLogging()
        {
            menuTrayViewLog.Visible = true;
        }

        private void InitializeComponents()
        {
            this.components = new System.ComponentModel.Container();
            this.SystemTray = new System.Windows.Forms.NotifyIcon(this.components);
            this.menuTray = new System.Windows.Forms.ContextMenuStrip();
            this.menuTrayConsole = new System.Windows.Forms.ToolStripMenuItem();
            this.menuTrayTDD = new System.Windows.Forms.ToolStripMenuItem();
            this.menuTraySMS = new System.Windows.Forms.ToolStripMenuItem();
            this.menuTrayColorEditor = new System.Windows.Forms.ToolStripMenuItem();
            this.menuTrayPhonebook = new System.Windows.Forms.ToolStripMenuItem();
            this.menuViewAlarms = new System.Windows.Forms.ToolStripMenuItem();
            this.menuTrayPSAPStatus = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCallback = new System.Windows.Forms.ToolStripMenuItem();
            this.menuConnect = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBarge = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFlashHook = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHold = new System.Windows.Forms.ToolStripMenuItem();
            this.menuDisconnect = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAbandon = new System.Windows.Forms.ToolStripMenuItem();
            this.menuRebid = new System.Windows.Forms.ToolStripMenuItem();
            this.menuTrayViewLog = new System.Windows.Forms.ToolStripMenuItem();
            this.menuTrayDebugWindow = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSep0 = new System.Windows.Forms.ToolStripSeparator();
            this.menuSep1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuSep2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuSep3 = new System.Windows.Forms.ToolStripSeparator();
            this.menuSep4 = new System.Windows.Forms.ToolStripSeparator();
            this.menuSep5 = new System.Windows.Forms.ToolStripSeparator();
            this.menuTrayAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.menuTrayUsersManual = new ToolStripMenuItem();
            this.menuTrayExit = new System.Windows.Forms.ToolStripMenuItem();

            // 
            // SystemTray
            // 
            this.SystemTray.ContextMenuStrip = this.menuTray;
            this.SystemTray.Icon = Properties.Resources.WestTelIcon;
            this.SystemTray.Visible = false;
            this.SystemTray.MouseClick += SystemTray_Click;
    // 
    // Debug
    // 
    //System.Windows.Forms.MenuItem menuTrayDebug1 = new System.Windows.Forms.MenuItem();
    //menuTrayDebug1.Enabled = true;
    //menuTrayDebug1.Text = "Debug Window &1";
    //menuTrayDebug1.Click += new System.EventHandler(this.menuDebug1_Click);
    //menuTrayDebug1.Visible = true;
    
    //System.Windows.Forms.MenuItem menuTrayDebug2 = new System.Windows.Forms.MenuItem();
    //menuTrayDebug2.Enabled = true;
    //menuTrayDebug2.Text = "Debug Window &2";
    //menuTrayDebug2.Click += new System.EventHandler(this.menuDebug2_Click);
    //menuTrayDebug2.Visible = true;
    
    //System.Windows.Forms.MenuItem menuTrayDebug3 = new System.Windows.Forms.MenuItem();
    //menuTrayDebug3.Enabled = true;
    //menuTrayDebug3.Text = "Debug Window &3";
    //menuTrayDebug3.Click += new System.EventHandler(this.menuDebug3_Click);
    //menuTrayDebug3.Visible = true;
    
    //System.Windows.Forms.MenuItem menuTrayDebug4 = new System.Windows.Forms.MenuItem();
    //menuTrayDebug4.Enabled = true;
    //menuTrayDebug4.Text = "Debug Window &4";
    //menuTrayDebug4.Click += new System.EventHandler(this.menuDebug4_Click);
    //menuTrayDebug4.Visible = true;
            // 
            // menuTray
            // 
            this.menuTray.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuTrayConsole,
            this.menuTrayTDD,
            this.menuTrayPhonebook,
            this.menuViewAlarms,
            this.menuTrayPSAPStatus,
            this.menuTraySMS,
            this.menuSep0,
            this.menuTrayColorEditor,
            this.menuSep1,
            this.menuRebid,
            this.menuAbandon,
            this.menuSep2,
            this.menuCallback,
            this.menuSep3,
            this.menuConnect,
            this.menuBarge,
            this.menuFlashHook,
            this.menuHold,
            this.menuDisconnect,
            this.menuSep4,
            this.menuTrayViewLog,
            this.menuTrayDebugWindow,
            //menuTrayDebug1,
            //menuTrayDebug2,
            //menuTrayDebug3,
            //menuTrayDebug4,
            this.menuTrayUsersManual,
            this.menuTrayAbout,
            this.menuSep5,
            this.menuTrayExit});
            menuTray.Opened += MenuTray_Opened;
            // 
            // menuTrayConsole
            // 
            this.menuTrayConsole.Enabled = false;
            this.menuTrayConsole.Image = new Icon(Properties.Resources.ConsoleIcon, 16, 16).ToBitmap();
            this.menuTrayConsole.ShortcutKeys = (Keys.Control | Keys.Shift | Keys.F5);
            this.menuTrayConsole.Text = "&Console";
            this.menuTrayConsole.Click += new System.EventHandler(this.menuTrayConsole_Click);
            // 
            // menuTrayTDD
            // 
            this.menuTrayTDD.Enabled = false;
            this.menuTrayTDD.Image = new Icon(Properties.Resources.TDDIcon, 16, 16).ToBitmap();
            this.menuTrayTDD.ShortcutKeys = (Keys.Control | Keys.Shift | Keys.F6);
            this.menuTrayTDD.Text = "&TDD";
            this.menuTrayTDD.Click += new System.EventHandler(this.menuTrayTDD_Click);
            // 
            // menuTraySMS
            // 
            this.menuTraySMS.Enabled = false;
            this.menuTraySMS.Image = new Icon(Properties.Resources.SMSIcon, 16, 16).ToBitmap();
            this.menuTraySMS.ShortcutKeys = (Keys.Control | Keys.Shift | Keys.F10);
            this.menuTraySMS.Text = "&SMS";
            this.menuTraySMS.Click += new System.EventHandler(this.menuTraySMS_Click);
            // 
            // menuTrayColorEditor
            // 
            this.menuTrayColorEditor.Enabled = true;
            this.menuTrayColorEditor.Image = new Icon(Properties.Resources.ColorEditorIcon, 16, 16).ToBitmap();
            this.menuTrayColorEditor.Text = "&Choose Application Colors";
            this.menuTrayColorEditor.Click += new System.EventHandler(this.menuTrayColorEditor_Click);
            // 
            // menuTrayPhonebook
            // 
            this.menuTrayPhonebook.Enabled = false;
            this.menuTrayPhonebook.Image = new Icon(Properties.Resources.PhonebookIcon, 16, 16).ToBitmap();
            this.menuTrayPhonebook.ShortcutKeys = (Keys.Control | Keys.Shift | Keys.F7);
            this.menuTrayPhonebook.Text = "&Phonebook";
            this.menuTrayPhonebook.Click += new System.EventHandler(this.menuTrayPhonebook_Click);
            // 
            // menuTrayPSAPStatus
            // 
            this.menuTrayPSAPStatus.Enabled = false;
            this.menuTrayPSAPStatus.Image = new Icon(Properties.Resources.PSAPStatusIcon, 16, 16).ToBitmap();
            this.menuTrayPSAPStatus.Text = "PSAP &Status";
            this.menuTrayPSAPStatus.ShortcutKeys = (Keys.Control | Keys.Shift | Keys.F9);
            this.menuTrayPSAPStatus.Click += new System.EventHandler(this.menuTrayPSAPStatus_Click);
            // 
            // menuViewAlarms
            // 
            this.menuViewAlarms.Enabled = false;
            this.menuViewAlarms.Image = new Icon(Properties.Resources.AlertIcon, 16, 16).ToBitmap();
            this.menuViewAlarms.Text = "&Alerts";
            this.menuViewAlarms.ShortcutKeys = (Keys.Control | Keys.Shift | Keys.F8);
            this.menuViewAlarms.Click += new System.EventHandler(this.menuViewAlarms_Click);
            // 
            // menuCallback
            // 
            this.menuCallback.Enabled = false;
            this.menuCallback.Image = new Icon(Properties.Resources.ConsoleCallbackIcon, 16, 16).ToBitmap();
            this.menuCallback.Text = "&Callback...";
            this.menuCallback.ShortcutKeys = (Keys.Control | Keys.Shift | Keys.C);
            this.menuCallback.Click += new System.EventHandler(this.menuCallback_Click);
            // 
            // menuConnect
            // 
            this.menuConnect.Enabled = false;
            this.menuConnect.Image = new Icon(Properties.Resources.ConsoleCallAnswerIcon, 16, 16).ToBitmap();
            this.menuConnect.Text = "&Answer";
            this.menuConnect.ShortcutKeys = (Keys.Control | Keys.Shift | Keys.A);
            this.menuConnect.Click += new System.EventHandler(this.menuConnect_Click);
            // 
            // menuBarge
            // 
            this.menuBarge.Enabled = false;
            this.menuBarge.Image = new Icon(Properties.Resources.ConsoleCallBargeIcon, 16, 16).ToBitmap();
            this.menuBarge.Text = "&Barge";
            this.menuBarge.ShortcutKeys = (Keys.Control | Keys.Shift | Keys.B);
            this.menuBarge.Click += new System.EventHandler(this.menuBarge_Click);
            // 
            // menuFlashHook
            // 
            this.menuFlashHook.Enabled = false;
            this.menuFlashHook.Image = new Icon(Properties.Resources.ConsoleCallFlashHookIcon, 16, 16).ToBitmap();
            this.menuFlashHook.Text = "&Flash Hook";
            this.menuFlashHook.ShortcutKeys = (Keys.Control | Keys.Shift | Keys.F);
            this.menuFlashHook.Click += new System.EventHandler(this.menuFlashHook_Click);
            // 
            // menuHold
            // 
            this.menuHold.Enabled = false;
            this.menuHold.Image = new Icon(Properties.Resources.ConsoleCallHoldIcon, 16, 16).ToBitmap();
            this.menuHold.Text = "&Hold";
            this.menuHold.ShortcutKeys = (Keys.Control | Keys.Shift | Keys.H);
            this.menuHold.Click += new System.EventHandler(this.menuHold_Click);
            // 
            // menuDisconnect
            // 
            this.menuDisconnect.Enabled = false;
            this.menuDisconnect.Image = new Icon(Properties.Resources.ConsoleCallHangUpIcon, 16, 16).ToBitmap();
            this.menuDisconnect.Text = "Han&g-Up...";
            this.menuDisconnect.ShortcutKeys = (Keys.Control | Keys.Shift | Keys.G);
            this.menuDisconnect.Click += new System.EventHandler(this.menuDisconnect_Click);
            // 
            // menuAbandon
            // 
            this.menuAbandon.Enabled = false;
            this.menuAbandon.Image = new Icon(Properties.Resources.ConsoleAbandonIcon, 16, 16).ToBitmap();
            this.menuAbandon.Text = "A&bandon";
            this.menuAbandon.ShortcutKeys = (Keys.Control | Keys.Shift | Keys.D);
            this.menuAbandon.Click += new System.EventHandler(this.menuAbandon_Click);
            // 
            // menuRebid
            // 
            this.menuRebid.Enabled = false;
            this.menuRebid.Image = new Icon(Properties.Resources.ConsoleALIRebidIcon, 16, 16).ToBitmap();
            this.menuRebid.Text = "&Rebid";
            this.menuRebid.ShortcutKeys = (Keys.Control | Keys.Shift | Keys.R);
            this.menuRebid.Click += new System.EventHandler(this.menuRebid_Click);
            // 
            // View Log
            // 
            this.menuTrayViewLog.Enabled = true;
            this.menuTrayViewLog.Text = "View &Log";
            this.menuTrayViewLog.Click += new System.EventHandler(this.menuViewLog_Click);
            this.menuTrayViewLog.Visible = false;
            // 
            // Debug XML
            // 
            this.menuTrayDebugWindow.Enabled = true;
            this.menuTrayDebugWindow.Text = "&Debug Window";
            this.menuTrayDebugWindow.Click += new System.EventHandler(this.menuViewXML_Click);
            this.menuTrayDebugWindow.Visible = false;
            // 
            // menuTrayAbout
            // 
            this.menuTrayAbout.Enabled = true;
            this.menuTrayAbout.Image = new Icon(Properties.Resources.WestTelIcon, 16, 16).ToBitmap();
            this.menuTrayAbout.Text = "A&bout";
            this.menuTrayAbout.Click += new System.EventHandler(this.menuTrayAbout_Click);
            // 
            // menuTrayUsersManual
            // 
            this.menuTrayUsersManual.Image = new Icon(Properties.Resources.UserManualIcon, 16, 16).ToBitmap();
            this.menuTrayUsersManual.Enabled = true;
            this.menuTrayUsersManual.Text = "&User Manual";
            this.menuTrayUsersManual.Click += new System.EventHandler(this.menuTrayUsersManual_Click);
            // 
            // menuTrayExit
            // 
            this.menuTrayExit.Enabled = true;
            this.menuTrayExit.Text = "E&xit...";
            this.menuTrayExit.Click += new System.EventHandler(this.menuTrayExit_Click);
        }

        private void MenuTray_Opened(object sender, EventArgs e)
        {
            BaseForm.removeUnnecessarySeparators(menuTray);
        }

        private void setTranslatableText()
        {
            this.menuTrayConsole.Text     = (Translate)("&Console");
            this.menuTrayTDD.Text         = (Translate)("&TDD");        
            this.menuTraySMS.Text         = (Translate)("&SMS");        
            this.menuTrayPhonebook.Text   = (Translate)("&Phonebook");        
            this.menuTrayPSAPStatus.Text  = (Translate)("PSAP &Status");        
            this.menuTrayColorEditor.Text = (Translate)("&Choose Application Colors");        
            this.menuViewAlarms.Text      = (Translate)("&Alerts");        
            this.menuCallback.Text        = (Translate)("&Callback...");        
            this.menuConnect.Text         = (Translate)("&Answer");        
            this.menuBarge.Text           = (Translate)("&Barge");        
            this.menuFlashHook.Text       = (Translate)("&Flash Hook");        
            this.menuHold.Text            = (Translate)("&Hold");        
            this.menuDisconnect.Text      = (Translate)("Han&g-Up...");        
            this.menuAbandon.Text         = (Translate)("A&bandon");        
            this.menuRebid.Text           = (Translate)("&Rebid");        
            this.menuTrayViewLog.Text     = (Translate)("View &Log");        
            this.menuTrayDebugWindow.Text = (Translate)("&Debug Window");        
            this.menuTrayAbout.Text       = (Translate)("A&bout");        
            this.menuTrayUsersManual.Text = (Translate)("&User Manual");        
            this.menuTrayExit.Text        = (Translate)("E&xit...");        
        }

        private void SystemTray_Click          (object sender, MouseEventArgs e) {if (e.Button == MouseButtons.Left) controller.ShowConsole();}
        private void menuTrayConsole_Click     (object sender, EventArgs e) {controller.ShowConsole();}
        private void menuTrayPhonebook_Click   (object sender, EventArgs e) {controller.ShowPhonebook();}
        private void menuTrayTDD_Click         (object sender, EventArgs e) {controller.ShowTDD();}
        private void menuTraySMS_Click         (object sender, EventArgs e) {controller.ShowSMS();}
        private void menuTrayColorEditor_Click (object sender, EventArgs e) {controller.ShowColorEditor();}
        private void menuTrayAbout_Click       (object sender, EventArgs e) {Splash.ShowAbout(controller.Position);}
        private void menuTrayUsersManual_Click (object sender, EventArgs e) {Console.ShowUsersManual();}
        private void menuTrayExit_Click        (object sender, EventArgs e) {controller.Shutdown();}
        private void menuViewAlarms_Click      (object sender, EventArgs e) {controller.ViewAlarms();}
        private void menuTrayPSAPStatus_Click  (object sender, EventArgs e) {controller.ViewPSAPStatus();}
        private void menuCallback_Click        (object sender, EventArgs e) {controller.Callback();}
        private void menuConnect_Click         (object sender, EventArgs e) {controller.Connect();}
        private void menuBarge_Click           (object sender, EventArgs e) {controller.Barge();}
        private void menuFlashHook_Click       (object sender, EventArgs e) {controller.FlashHook();}
        private void menuHold_Click            (object sender, EventArgs e) {controller.Hold();}
        private void menuDisconnect_Click      (object sender, EventArgs e) {controller.Disconnect();}
        private void menuAbandon_Click         (object sender, EventArgs e) {controller.Abandon();}
        private void menuRebid_Click           (object sender, EventArgs e) {controller.Rebid();}
        private void menuViewLog_Click         (object sender, EventArgs e) {controller.ShowLog();}
        private void menuViewXML_Click         (object sender, EventArgs e) {controller.ShowXML();}


        //private void menuDebug1_Click(object sender, EventArgs e)
        //{
        //    Form form = new Form();
        //    form.SetBounds(0, 0, 500, 300);
        //    form.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        //    form.SizeGripStyle = SizeGripStyle.Show;
        //    form.Text = "Resizable Dialog (500 x 300)";

        //    form.Show();
        //    MessageBox.Show(form, "This window's height is " + form.Height + ". It should be 300.");
        //}

        //private void menuDebug2_Click(object sender, EventArgs e)
        //{
        //    Form form = new Form();
        //    form.SetBounds(0, 0, 500, 300);
        //    form.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        //    form.SizeGripStyle = SizeGripStyle.Hide;
        //    form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
        //    form.Text = "Fixed Dialog (500 x 300)";

        //    form.Show();
        //    MessageBox.Show(form, "This window's height is " + form.Height + ". It should be 300.");
        //}

        //private void menuDebug3_Click(object sender, EventArgs e)
        //{
        //    BaseForm form = new BaseForm();
        //    form.SetBounds(0, 0, 500, 300);
        //    form.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        //    form.SizeGripStyle = SizeGripStyle.Show;
        //    form.Text = "Resizable BaseForm (500 x 300)";

        //    form.Show();
        //    MessageBox.Show(form, "This window's height is " + form.Height + ". It should be 300.");
        //}

        //private void menuDebug4_Click(object sender, EventArgs e)
        //{
        //    BaseForm form = new BaseForm();
        //    form.SetBounds(0, 0, 500, 300);
        //    form.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        //    form.SizeGripStyle = SizeGripStyle.Hide;
        //    form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
        //    form.Text = "Fixed BaseForm (500 x 300)";

        //    form.Show();
        //    MessageBox.Show(form, "This window's height is " + form.Height + ". It should be 300.");
        //}
    }
}