using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;

namespace Experient.E911
{
	/// <summary>
	/// Applet Form for PBX transfering.
	/// </summary>
	public class Transfer : BaseForm
	{
        private class DialButton : Button
        {
            override protected void OnPaint(PaintEventArgs args)
            {
                base.OnPaint(args);

                Graphics  graphics = args.Graphics;
                Rectangle rect     = this.DisplayRectangle;

                StringFormat stringFormat = new StringFormat();

                switch (TextAlign)
                {
                    case ContentAlignment.TopLeft:      stringFormat.Alignment = StringAlignment.Near;   stringFormat.LineAlignment = StringAlignment.Near;   break;
                    case ContentAlignment.TopCenter:    stringFormat.Alignment = StringAlignment.Center; stringFormat.LineAlignment = StringAlignment.Near;   break;
                    case ContentAlignment.TopRight:     stringFormat.Alignment = StringAlignment.Far;    stringFormat.LineAlignment = StringAlignment.Near;   break;
                    case ContentAlignment.MiddleLeft:   stringFormat.Alignment = StringAlignment.Near;   stringFormat.LineAlignment = StringAlignment.Center; break;
                    case ContentAlignment.MiddleCenter: stringFormat.Alignment = StringAlignment.Center; stringFormat.LineAlignment = StringAlignment.Center; break;
                    case ContentAlignment.MiddleRight:  stringFormat.Alignment = StringAlignment.Far;    stringFormat.LineAlignment = StringAlignment.Center; break;
                    case ContentAlignment.BottomLeft:   stringFormat.Alignment = StringAlignment.Near;   stringFormat.LineAlignment = StringAlignment.Center; break;
                    case ContentAlignment.BottomCenter: stringFormat.Alignment = StringAlignment.Center; stringFormat.LineAlignment = StringAlignment.Far;    break;
                    case ContentAlignment.BottomRight:  stringFormat.Alignment = StringAlignment.Far;    stringFormat.LineAlignment = StringAlignment.Far;    break;
                    default:                            stringFormat.Alignment = StringAlignment.Center; stringFormat.LineAlignment = StringAlignment.Far;    break;
                }

                rect.Y      += Padding.Top;
                rect.Height -= Padding.Bottom;
                rect.X      += Padding.Left;
                rect.Width  -= Padding.Right;

                string[] sep = {"\r\n"};
                string[] strings = Tag.ToString().Split(sep, StringSplitOptions.None);

                Font boldFont = new Font(Font.FontFamily, Font.Size + 8, FontStyle.Bold, Font.Unit);

                int nStrings = strings.GetLength(0);

                if (nStrings == 1)
                {
                    graphics.DrawString(strings[0], boldFont, new SolidBrush(ForeColor), rect, stringFormat);
                }
                else
                {
                    graphics.DrawString(strings[0] + "\r\n ", boldFont, new SolidBrush(ForeColor), rect, stringFormat);
                    graphics.DrawString(" \r\n" + strings[1], Font,     new SolidBrush(ForeColor), rect, stringFormat);
                }
            }
        }

        private ArrayList  allTransfers = new ArrayList();
		public  Controller controller   = null;

        private string                  currentCallId       = null;
        private Controller.TransferType currentTransferType = Controller.TransferType.Dial;

        private static System.Windows.Forms.Form confirmCancelDialog = null;
		
		Brush printerBrush;
		Font printerFont;
		int fontSize = 10;
		float LinePosition = 10;
		float LeftMargin = 30;
		float TopMargin = 50;


		// bool isStarting = true;
		bool systemShutdown = false;
		
		private string DefaultGroup = "All";
		private static int WM_QUERYENDSESSION = 0x11;

        public static bool showAttendedTransferButton = true;

        public bool blindEnabled      = false;
        public bool attendedEnabled   = false;
        public bool conferenceEnabled = false;
        public bool tandemEnabled     = false;

        private bool transferEnabled  = false;

        private AutoCompleteStringCollection dialHistory;
        private Microsoft.Win32.RegistryKey regSettings = null;

		#region Windows Control Declaraitons

        private MenuStrip MainMenu;
        private ToolStrip toolBar;
        private ToolStripButton printButton;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem printMenu;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem closeMenu;
        private ToolStripMenuItem helpToolStripMenuItem;
        private ToolStripMenuItem aboutMenu;
		private System.Windows.Forms.ListBox allListBox;
		private System.Drawing.Printing.PrintDocument printDoc;
        private System.Windows.Forms.PrintDialog printSetup;
        private ToolStripMenuItem transferMenu;
        private ToolStripMenuItem blindMenu;
        private ToolStripMenuItem attendedMenu;
        private ToolStripMenuItem conferenceMenu;
        private ToolStripMenuItem tandemMenu;
        private ToolStripMenuItem speedDialMenu;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripSeparator toolStripSeparator4;
        private ToolStripButton tandemButton;
        private ToolStripButton blindButton;
        private ToolStripButton attendedButton;
        private ToolStripButton conferenceButton;
        private ToolStripSeparator toolStripSeparator5;
        private ToolStripSeparator toolStripSeparator6;
        private ToolStripSeparator toolStripSeparator2;
        private TabControl tabControl;
        private TabPage allTab;
        private TabPage dialTab;
        //private ComboBox phoneField;
        private TextBox phoneField;
        private TableLayoutPanel dialLayoutPanel;
        private Transfer.DialButton button9;
        private Transfer.DialButton buttonAsterisk;
        private Transfer.DialButton button7;
        private Transfer.DialButton button5;
        private Transfer.DialButton button6;
        private Transfer.DialButton button2;
        private Transfer.DialButton button1;
        private Transfer.DialButton button4;
        private Transfer.DialButton button3;
        private Transfer.DialButton button0;
        private Transfer.DialButton buttonPound;
        private Transfer.DialButton button8;
        private Button backButton;
        private ToolStripButton speedDialButton;

        #endregion

		#region Constructor / Deconstructor
		public Transfer(Controller c)
		{
			controller = c;

            // Required for Windows Form Designer support
			InitializeComponent();

            blindButton      .Tag = blindMenu      .Tag = Controller.TransferType.Blind;
            attendedButton   .Tag = attendedMenu   .Tag = Controller.TransferType.Attended;
            conferenceButton .Tag = conferenceMenu .Tag = Controller.TransferType.Conference;
            tandemButton     .Tag = tandemMenu     .Tag = Controller.TransferType.Tandem;
            speedDialButton  .Tag = speedDialMenu  .Tag = Controller.TransferType.Dial;

            attendedButton.Visible = attendedMenu.Visible = showAttendedTransferButton;

            setEnabled(false, null);

            object defaultGroup = controller.settings.PhonebookSettings["DefaultGroup"];
            if (defaultGroup == null) defaultGroup = "All";

            DefaultGroup = defaultGroup.ToString();

			ReadData();

            phoneField.Text = "";

            try
            {
                dialHistory = new AutoCompleteStringCollection();

                regSettings = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(
                    "Software\\" + Application.CompanyName + "\\" + Application.ProductName);

                if (regSettings != null)
                {
                    string[] dialHistoryStrList;
                    dialHistoryStrList = (string[])regSettings.GetValue("DialHistory");

                    if (dialHistoryStrList != null)
                    {
                        dialHistory.AddRange(dialHistoryStrList);

                        // TODO: should be add the call history back someday?  (Bill didn't like it...)
                        //phoneField.AutoCompleteCustomSource = dialHistory;
                        //phoneField.Items.AddRange(dialHistoryStrList);
                    }
                }
            }
            catch
            {
                regSettings = null;
            }

            try
            {
                // TODO: should be add the call history back someday?  (Bill didn't like it...)
                //phoneField.ContextMenu = new ContextMenu();
                //phoneField.ContextMenu.MenuItems.Add("&Clear History", clearDialHistory);
            }
            catch {}
		}

		#endregion

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Transfer));
            this.toolBar = new System.Windows.Forms.ToolStrip();
            this.printButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tandemButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.blindButton = new System.Windows.Forms.ToolStripButton();
            this.attendedButton = new System.Windows.Forms.ToolStripButton();
            this.conferenceButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.speedDialButton = new System.Windows.Forms.ToolStripButton();
            this.allListBox = new System.Windows.Forms.ListBox();
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.closeMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.transferMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.tandemMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.blindMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.attendedMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.conferenceMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.speedDialMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.printDoc = new System.Drawing.Printing.PrintDocument();
            this.printSetup = new System.Windows.Forms.PrintDialog();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.allTab = new System.Windows.Forms.TabPage();
            this.dialTab = new System.Windows.Forms.TabPage();
            this.dialLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.phoneField = new System.Windows.Forms.TextBox();
            this.button1 = new Experient.E911.Transfer.DialButton();
            this.button2 = new Experient.E911.Transfer.DialButton();
            this.button3 = new Experient.E911.Transfer.DialButton();
            this.button4 = new Experient.E911.Transfer.DialButton();
            this.button5 = new Experient.E911.Transfer.DialButton();
            this.button6 = new Experient.E911.Transfer.DialButton();
            this.button7 = new Experient.E911.Transfer.DialButton();
            this.button8 = new Experient.E911.Transfer.DialButton();
            this.button9 = new Experient.E911.Transfer.DialButton();
            this.buttonAsterisk = new Experient.E911.Transfer.DialButton();
            this.button0 = new Experient.E911.Transfer.DialButton();
            this.buttonPound = new Experient.E911.Transfer.DialButton();
            this.backButton = new System.Windows.Forms.Button();
            this.toolBar.SuspendLayout();
            this.MainMenu.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.allTab.SuspendLayout();
            this.dialTab.SuspendLayout();
            this.dialLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolBar
            // 
            this.toolBar.AllowMerge = false;
            this.toolBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.toolBar.AutoSize = false;
            this.toolBar.Dock = System.Windows.Forms.DockStyle.None;
            this.toolBar.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.toolBar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolBar.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printButton,
            this.toolStripSeparator4,
            this.tandemButton,
            this.toolStripSeparator5,
            this.blindButton,
            this.attendedButton,
            this.conferenceButton,
            this.toolStripSeparator6,
            this.speedDialButton});
            this.toolBar.Location = new System.Drawing.Point(0, 24);
            this.toolBar.Name = "toolBar";
            this.toolBar.Padding = new System.Windows.Forms.Padding(0);
            this.toolBar.Size = new System.Drawing.Size(334, 45);
            this.toolBar.Stretch = true;
            this.toolBar.TabIndex = 2;
            this.toolBar.Text = "Tool Bar";
            // 
            // printButton
            // 
            this.printButton.Enabled = false;
            this.printButton.Image = ((System.Drawing.Image)(resources.GetObject("printButton.Image")));
            this.printButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(36, 42);
            this.printButton.Text = "&Print";
            this.printButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.printButton.ToolTipText = "Print (Ctrl+P)";
            this.printButton.Click += new System.EventHandler(this.printMenu_ItemClick);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 45);
            // 
            // tandemButton
            // 
            this.tandemButton.Image = ((System.Drawing.Image)(resources.GetObject("tandemButton.Image")));
            this.tandemButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tandemButton.Name = "tandemButton";
            this.tandemButton.Size = new System.Drawing.Size(40, 42);
            this.tandemButton.Text = "&9-1-1";
            this.tandemButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tandemButton.Click += new System.EventHandler(this.transferButton_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 45);
            // 
            // blindButton
            // 
            this.blindButton.Image = ((System.Drawing.Image)(resources.GetObject("blindButton.Image")));
            this.blindButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.blindButton.Name = "blindButton";
            this.blindButton.Size = new System.Drawing.Size(36, 42);
            this.blindButton.Text = "&Blind";
            this.blindButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.blindButton.Click += new System.EventHandler(this.transferButton_Click);
            // 
            // attendedButton
            // 
            this.attendedButton.Image = ((System.Drawing.Image)(resources.GetObject("attendedButton.Image")));
            this.attendedButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.attendedButton.Name = "attendedButton";
            this.attendedButton.Size = new System.Drawing.Size(64, 42);
            this.attendedButton.Text = "&Attended";
            this.attendedButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.attendedButton.Click += new System.EventHandler(this.transferButton_Click);
            // 
            // conferenceButton
            // 
            this.conferenceButton.Image = ((System.Drawing.Image)(resources.GetObject("conferenceButton.Image")));
            this.conferenceButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.conferenceButton.Name = "conferenceButton";
            this.conferenceButton.Size = new System.Drawing.Size(74, 42);
            this.conferenceButton.Text = "&Conference";
            this.conferenceButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.conferenceButton.Click += new System.EventHandler(this.transferButton_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 45);
            // 
            // speedDialButton
            // 
            this.speedDialButton.Image = ((System.Drawing.Image)(resources.GetObject("speedDialButton.Image")));
            this.speedDialButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.speedDialButton.Name = "speedDialButton";
            this.speedDialButton.Size = new System.Drawing.Size(29, 42);
            this.speedDialButton.Text = "&Dial";
            this.speedDialButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.speedDialButton.Click += new System.EventHandler(this.transferButton_Click);
            // 
            // allListBox
            // 
            this.allListBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.allListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.allListBox.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.allListBox.ItemHeight = 21;
            this.allListBox.Items.AddRange(new object[] {
            "Test",
            "Longer Item Here",
            "End"});
            this.allListBox.Location = new System.Drawing.Point(0, 0);
            this.allListBox.Margin = new System.Windows.Forms.Padding(0);
            this.allListBox.Name = "allListBox";
            this.allListBox.Size = new System.Drawing.Size(326, 315);
            this.allListBox.TabIndex = 1;
            this.allListBox.SelectedIndexChanged += new System.EventHandler(this.listBox_SelectedIndexChanged);
            this.allListBox.DataSourceChanged += new System.EventHandler(this.listBox_DataSourceChanged);
            this.allListBox.VisibleChanged += new System.EventHandler(this.tabContent_VisibleChanged);
            this.allListBox.DoubleClick += new System.EventHandler(this.listBox_DoubleClick);
            this.allListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listBox_KeyDown);
            // 
            // MainMenu
            // 
            this.MainMenu.AllowMerge = false;
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.transferMenu,
            this.helpToolStripMenuItem});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(334, 24);
            this.MainMenu.TabIndex = 1;
            this.MainMenu.Text = "Main Menu";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printMenu,
            this.toolStripSeparator1,
            this.closeMenu});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // printMenu
            // 
            this.printMenu.Enabled = false;
            this.printMenu.Image = ((System.Drawing.Image)(resources.GetObject("printMenu.Image")));
            this.printMenu.Name = "printMenu";
            this.printMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.printMenu.Size = new System.Drawing.Size(149, 22);
            this.printMenu.Text = "&Print...";
            this.printMenu.Click += new System.EventHandler(this.printMenu_ItemClick);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(146, 6);
            // 
            // closeMenu
            // 
            this.closeMenu.Name = "closeMenu";
            this.closeMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.closeMenu.Size = new System.Drawing.Size(149, 22);
            this.closeMenu.Text = "&Close";
            this.closeMenu.Click += new System.EventHandler(this.menuClose_ItemClick);
            // 
            // transferMenu
            // 
            this.transferMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tandemMenu,
            this.toolStripSeparator2,
            this.blindMenu,
            this.attendedMenu,
            this.conferenceMenu,
            this.toolStripSeparator3,
            this.speedDialMenu});
            this.transferMenu.Name = "transferMenu";
            this.transferMenu.Size = new System.Drawing.Size(62, 20);
            this.transferMenu.Text = "&Transfer";
            // 
            // tandemMenu
            // 
            this.tandemMenu.Image = ((System.Drawing.Image)(resources.GetObject("tandemMenu.Image")));
            this.tandemMenu.Name = "tandemMenu";
            this.tandemMenu.Size = new System.Drawing.Size(144, 22);
            this.tandemMenu.Text = "&9-1-1...";
            this.tandemMenu.Click += new System.EventHandler(this.transferButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(141, 6);
            // 
            // blindMenu
            // 
            this.blindMenu.Image = ((System.Drawing.Image)(resources.GetObject("blindMenu.Image")));
            this.blindMenu.Name = "blindMenu";
            this.blindMenu.Size = new System.Drawing.Size(144, 22);
            this.blindMenu.Text = "&Blind...";
            this.blindMenu.Click += new System.EventHandler(this.transferButton_Click);
            // 
            // attendedMenu
            // 
            this.attendedMenu.Image = ((System.Drawing.Image)(resources.GetObject("attendedMenu.Image")));
            this.attendedMenu.Name = "attendedMenu";
            this.attendedMenu.Size = new System.Drawing.Size(144, 22);
            this.attendedMenu.Text = "&Attendend...";
            this.attendedMenu.Click += new System.EventHandler(this.transferButton_Click);
            // 
            // conferenceMenu
            // 
            this.conferenceMenu.Image = ((System.Drawing.Image)(resources.GetObject("conferenceMenu.Image")));
            this.conferenceMenu.Name = "conferenceMenu";
            this.conferenceMenu.Size = new System.Drawing.Size(144, 22);
            this.conferenceMenu.Text = "C&onference...";
            this.conferenceMenu.Click += new System.EventHandler(this.transferButton_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(141, 6);
            // 
            // speedDialMenu
            // 
            this.speedDialMenu.Image = ((System.Drawing.Image)(resources.GetObject("speedDialMenu.Image")));
            this.speedDialMenu.Name = "speedDialMenu";
            this.speedDialMenu.Size = new System.Drawing.Size(144, 22);
            this.speedDialMenu.Text = "&Dial...";
            this.speedDialMenu.Click += new System.EventHandler(this.transferButton_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutMenu});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutMenu
            // 
            this.aboutMenu.Name = "aboutMenu";
            this.aboutMenu.Size = new System.Drawing.Size(131, 22);
            this.aboutMenu.Text = "&About XXX";
            this.aboutMenu.Click += new System.EventHandler(this.menuHelpAbout_ItemClick);
            // 
            // printDoc
            // 
            this.printDoc.DocumentName = "Transfer List";
            this.printDoc.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDoc_PrintPage);
            // 
            // printSetup
            // 
            this.printSetup.Document = this.printDoc;
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.allTab);
            this.tabControl.Controls.Add(this.dialTab);
            this.tabControl.HotTrack = true;
            this.tabControl.Location = new System.Drawing.Point(0, 69);
            this.tabControl.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl.Multiline = true;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(334, 342);
            this.tabControl.TabIndex = 3;
            this.tabControl.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabChanged);
            this.tabControl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tabControl_KeyDown);
            // 
            // allTab
            // 
            this.allTab.Controls.Add(this.allListBox);
            this.allTab.Location = new System.Drawing.Point(4, 23);
            this.allTab.Margin = new System.Windows.Forms.Padding(0);
            this.allTab.Name = "allTab";
            this.allTab.Size = new System.Drawing.Size(326, 315);
            this.allTab.TabIndex = 0;
            this.allTab.Text = "All";
            this.allTab.UseVisualStyleBackColor = true;
            // 
            // dialTab
            // 
            this.dialTab.Controls.Add(this.dialLayoutPanel);
            this.dialTab.Location = new System.Drawing.Point(4, 22);
            this.dialTab.Margin = new System.Windows.Forms.Padding(0);
            this.dialTab.Name = "dialTab";
            this.dialTab.Size = new System.Drawing.Size(326, 316);
            this.dialTab.TabIndex = 1;
            this.dialTab.Text = "Dial";
            this.dialTab.UseVisualStyleBackColor = true;
            // 
            // dialLayoutPanel
            // 
            this.dialLayoutPanel.AutoSize = true;
            this.dialLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.dialLayoutPanel.ColumnCount = 6;
            this.dialLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.33333F));
            this.dialLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.dialLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.33333F));
            this.dialLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.dialLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.33333F));
            this.dialLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.dialLayoutPanel.Controls.Add(this.phoneField, 0, 0);
            this.dialLayoutPanel.Controls.Add(this.button1, 0, 1);
            this.dialLayoutPanel.Controls.Add(this.button2, 2, 1);
            this.dialLayoutPanel.Controls.Add(this.button3, 4, 1);
            this.dialLayoutPanel.Controls.Add(this.button4, 0, 2);
            this.dialLayoutPanel.Controls.Add(this.button5, 2, 2);
            this.dialLayoutPanel.Controls.Add(this.button6, 4, 2);
            this.dialLayoutPanel.Controls.Add(this.button7, 0, 3);
            this.dialLayoutPanel.Controls.Add(this.button8, 2, 3);
            this.dialLayoutPanel.Controls.Add(this.button9, 4, 3);
            this.dialLayoutPanel.Controls.Add(this.buttonAsterisk, 0, 4);
            this.dialLayoutPanel.Controls.Add(this.button0, 2, 4);
            this.dialLayoutPanel.Controls.Add(this.buttonPound, 4, 4);
            this.dialLayoutPanel.Controls.Add(this.backButton, 5, 0);
            this.dialLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dialLayoutPanel.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.dialLayoutPanel.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.dialLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.dialLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.dialLayoutPanel.Name = "dialLayoutPanel";
            this.dialLayoutPanel.RowCount = 5;
            this.dialLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.dialLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.dialLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.dialLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.dialLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.dialLayoutPanel.Size = new System.Drawing.Size(326, 316);
            this.dialLayoutPanel.TabIndex = 6;
            this.dialLayoutPanel.VisibleChanged += new System.EventHandler(this.tabContent_VisibleChanged);
            // 
            // phoneField
            // 
            this.dialLayoutPanel.SetColumnSpan(this.phoneField, 5);
            this.phoneField.Dock = System.Windows.Forms.DockStyle.Fill;
            this.phoneField.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.phoneField.Location = new System.Drawing.Point(6, 6);
            this.phoneField.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.phoneField.Name = "phoneField";
            this.phoneField.Size = new System.Drawing.Size(286, 35);
            this.phoneField.TabIndex = 0;
            this.phoneField.Text = "(123) 456-7890";
            this.phoneField.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.phoneField_KeyPress);
            this.phoneField.KeyUp += new System.Windows.Forms.KeyEventHandler(this.phoneField_KeyRelease);
            // 
            // button1
            // 
            this.dialLayoutPanel.SetColumnSpan(this.button1, 2);
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(6, 53);
            this.button1.Margin = new System.Windows.Forms.Padding(6);
            this.button1.Name = "button1";
            this.button1.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.button1.Size = new System.Drawing.Size(96, 55);
            this.button1.TabIndex = 2;
            this.button1.TabStop = false;
            this.button1.Tag = "1\r\n ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.dialButton_Clicked);
            // 
            // button2
            // 
            this.dialLayoutPanel.SetColumnSpan(this.button2, 2);
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.Location = new System.Drawing.Point(114, 53);
            this.button2.Margin = new System.Windows.Forms.Padding(6);
            this.button2.Name = "button2";
            this.button2.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.button2.Size = new System.Drawing.Size(96, 55);
            this.button2.TabIndex = 3;
            this.button2.TabStop = false;
            this.button2.Tag = "2\r\na b c";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.dialButton_Clicked);
            // 
            // button3
            // 
            this.dialLayoutPanel.SetColumnSpan(this.button3, 2);
            this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button3.Location = new System.Drawing.Point(222, 53);
            this.button3.Margin = new System.Windows.Forms.Padding(6);
            this.button3.Name = "button3";
            this.button3.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.button3.Size = new System.Drawing.Size(98, 55);
            this.button3.TabIndex = 4;
            this.button3.TabStop = false;
            this.button3.Tag = "3\r\nd e f";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.dialButton_Clicked);
            // 
            // button4
            // 
            this.dialLayoutPanel.SetColumnSpan(this.button4, 2);
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.Location = new System.Drawing.Point(6, 120);
            this.button4.Margin = new System.Windows.Forms.Padding(6);
            this.button4.Name = "button4";
            this.button4.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.button4.Size = new System.Drawing.Size(96, 55);
            this.button4.TabIndex = 5;
            this.button4.TabStop = false;
            this.button4.Tag = "4\r\ng h i";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.dialButton_Clicked);
            // 
            // button5
            // 
            this.dialLayoutPanel.SetColumnSpan(this.button5, 2);
            this.button5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button5.Location = new System.Drawing.Point(114, 120);
            this.button5.Margin = new System.Windows.Forms.Padding(6);
            this.button5.Name = "button5";
            this.button5.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.button5.Size = new System.Drawing.Size(96, 55);
            this.button5.TabIndex = 6;
            this.button5.TabStop = false;
            this.button5.Tag = "5\r\nj k l";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.dialButton_Clicked);
            // 
            // button6
            // 
            this.dialLayoutPanel.SetColumnSpan(this.button6, 2);
            this.button6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button6.Location = new System.Drawing.Point(222, 120);
            this.button6.Margin = new System.Windows.Forms.Padding(6);
            this.button6.Name = "button6";
            this.button6.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.button6.Size = new System.Drawing.Size(98, 55);
            this.button6.TabIndex = 7;
            this.button6.TabStop = false;
            this.button6.Tag = "6\r\nm n o";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.dialButton_Clicked);
            // 
            // button7
            // 
            this.dialLayoutPanel.SetColumnSpan(this.button7, 2);
            this.button7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button7.Location = new System.Drawing.Point(6, 187);
            this.button7.Margin = new System.Windows.Forms.Padding(6);
            this.button7.Name = "button7";
            this.button7.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.button7.Size = new System.Drawing.Size(96, 55);
            this.button7.TabIndex = 8;
            this.button7.TabStop = false;
            this.button7.Tag = "7\r\np q r s";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.dialButton_Clicked);
            // 
            // button8
            // 
            this.dialLayoutPanel.SetColumnSpan(this.button8, 2);
            this.button8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button8.Location = new System.Drawing.Point(114, 187);
            this.button8.Margin = new System.Windows.Forms.Padding(6);
            this.button8.Name = "button8";
            this.button8.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.button8.Size = new System.Drawing.Size(96, 55);
            this.button8.TabIndex = 9;
            this.button8.TabStop = false;
            this.button8.Tag = "8\r\nt u v";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.dialButton_Clicked);
            // 
            // button9
            // 
            this.dialLayoutPanel.SetColumnSpan(this.button9, 2);
            this.button9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button9.Location = new System.Drawing.Point(222, 187);
            this.button9.Margin = new System.Windows.Forms.Padding(6);
            this.button9.Name = "button9";
            this.button9.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.button9.Size = new System.Drawing.Size(98, 55);
            this.button9.TabIndex = 10;
            this.button9.TabStop = false;
            this.button9.Tag = "9\r\nw x y z";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.dialButton_Clicked);
            // 
            // buttonAsterisk
            // 
            this.dialLayoutPanel.SetColumnSpan(this.buttonAsterisk, 2);
            this.buttonAsterisk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonAsterisk.Font = new System.Drawing.Font("Consolas", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.buttonAsterisk.Location = new System.Drawing.Point(6, 254);
            this.buttonAsterisk.Margin = new System.Windows.Forms.Padding(6);
            this.buttonAsterisk.Name = "buttonAsterisk";
            this.buttonAsterisk.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.buttonAsterisk.Size = new System.Drawing.Size(96, 56);
            this.buttonAsterisk.TabIndex = 11;
            this.buttonAsterisk.TabStop = false;
            this.buttonAsterisk.Tag = "*";
            this.buttonAsterisk.UseVisualStyleBackColor = true;
            this.buttonAsterisk.Click += new System.EventHandler(this.dialButton_Clicked);
            // 
            // button0
            // 
            this.dialLayoutPanel.SetColumnSpan(this.button0, 2);
            this.button0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button0.Location = new System.Drawing.Point(114, 254);
            this.button0.Margin = new System.Windows.Forms.Padding(6);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(96, 56);
            this.button0.TabIndex = 12;
            this.button0.TabStop = false;
            this.button0.Tag = "0";
            this.button0.UseVisualStyleBackColor = true;
            this.button0.Click += new System.EventHandler(this.dialButton_Clicked);
            // 
            // buttonPound
            // 
            this.dialLayoutPanel.SetColumnSpan(this.buttonPound, 2);
            this.buttonPound.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonPound.Location = new System.Drawing.Point(222, 254);
            this.buttonPound.Margin = new System.Windows.Forms.Padding(6);
            this.buttonPound.Name = "buttonPound";
            this.buttonPound.Size = new System.Drawing.Size(98, 56);
            this.buttonPound.TabIndex = 13;
            this.buttonPound.TabStop = false;
            this.buttonPound.Tag = "#";
            this.buttonPound.UseVisualStyleBackColor = true;
            this.buttonPound.Click += new System.EventHandler(this.dialButton_Clicked);
            // 
            // backButton
            // 
            this.backButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.backButton.FlatAppearance.BorderSize = 0;
            this.backButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backButton.Image = ((System.Drawing.Image)(resources.GetObject("backButton.Image")));
            this.backButton.Location = new System.Drawing.Point(292, 6);
            this.backButton.Margin = new System.Windows.Forms.Padding(0, 6, 3, 6);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(31, 35);
            this.backButton.TabIndex = 1;
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // Transfer
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            this.ClientSize = new System.Drawing.Size(334, 411);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.toolBar);
            this.Controls.Add(this.MainMenu);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Transfer";
            this.Text = "Phonebook";
            this.Activated += new System.EventHandler(this.Transfer_Activated);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.Transfer_Closing);
            this.Load += new System.EventHandler(this.Transfer_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Transfer_KeyDown);
            this.toolBar.ResumeLayout(false);
            this.toolBar.PerformLayout();
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.allTab.ResumeLayout(false);
            this.dialTab.ResumeLayout(false);
            this.dialTab.PerformLayout();
            this.dialLayoutPanel.ResumeLayout(false);
            this.dialLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void ReadData()
		{
			try
			{
                // reset list box to updated datasource (the listbox caches data)
                allListBox.DataSource = null;
                allListBox.Items.Clear();

                tabControl.TabPages.Clear();
                tabControl.TabPages.Add(allTab);
                tabControl.TabPages.Add(dialTab);

                allTab.Name  = "All";
                dialTab.Name = "Dial";

                allTab.Tag = allListBox;

                if (controller.settings.PhonebookList != null)
                {
                    allListBox.DataSource = allTransfers;

                    foreach (PhonebookEntry t in controller.settings.PhonebookList)
				    {
                        allTransfers.Add(t);

                        TabPage groupTabPage = tabControl.TabPages[t.Group];

    				    if (groupTabPage != null)
                        {
                            if (groupTabPage != allTab && groupTabPage.Tag != null && ((ListBox)groupTabPage.Tag).DataSource != null)
                            {
                                ((ArrayList)((ListBox)groupTabPage.Tag).DataSource).Add(t);
                            }
                        }
                        // create new group
                        else
                        {
                    		ArrayList newTransfers = new ArrayList();
                            newTransfers.Add(t);

                            int newTabIndex = tabControl.TabCount - 1;
                            tabControl.TabPages.Insert(newTabIndex, t.Group, t.Group);
                            TabPage newTab = tabControl.TabPages[newTabIndex];

                            ListBox newListBox     = new ListBox();
                            newListBox.BorderStyle = allListBox.BorderStyle;
                            newListBox.DataSource  = newTransfers;
                            newListBox.DisplayMember="Name";
                            newListBox.Dock        = allListBox.Dock;
                            newListBox.Font        = allListBox.Font;
                            newListBox.Margin      = allListBox.Margin;
                            newListBox.SelectedIndexChanged += new System.EventHandler(listBox_SelectedIndexChanged);
                            newListBox.DataSourceChanged += new System.EventHandler(listBox_DataSourceChanged);
                            newListBox.DoubleClick += new System.EventHandler(listBox_DoubleClick);
                            newListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(listBox_KeyDown);
                            newListBox.VisibleChanged += new System.EventHandler(tabContent_VisibleChanged);

                            newTab.Controls.Add(newListBox);
                            newTab.Tag = newListBox;
                        }
				    }

                    allListBox.DisplayMember = "Name";
                    allListBox.SelectedIndex = 0;
                }

				PhonebookEntrySort phonebookEntrySort = new PhonebookEntrySort();

				allTransfers.Sort(phonebookEntrySort);

                if (tabControl.TabPages.ContainsKey(DefaultGroup)) tabControl.SelectTab(DefaultGroup);
                else                                               tabControl.SelectTab(allTab);

                tabChanged(this, new TabControlEventArgs(
                    tabControl.SelectedTab, tabControl.SelectedIndex, TabControlAction.Selected));

                // TODO: remove all tab completely?
                tabControl.TabPages.Remove(allTab);
			}
			catch (Exception ex)
			{
				Logging.ExceptionLogger("Error in Phonebook.ReadData", ex, Console.LogID.Init);
			}
		}

		#region Form Event Handlers

		protected override void WndProc(ref Message m)
		{
			if (m.Msg == WM_QUERYENDSESSION)
			{
				if (!controller.OkToStop) Controller.ExitApplication(controller);
			}
			base.WndProc (ref m);
		}

		private void Transfer_Load(object sender, System.EventArgs e)
		{
			this.Text = Application.ProductName + " - Phonebook";
			aboutMenu.Text = aboutMenu.Text.Replace("XXX",Application.ProductName);

            resolveTransferButtonsState();
        }

        private void Transfer_Activated(object sender, EventArgs e)
        {
            // make sure the menus are enabled correctly
            setEnabled(transferEnabled, currentCallId);
        }

		private void Transfer_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!systemShutdown)
			{
				e.Cancel = true;
				Hide();
			}

            resolveTransferButtonsState();
		}

		#endregion
			
        private void tabChanged(object sender, TabControlEventArgs e)
        {
            if (e.TabPage == dialTab)
            {
                Application.DoEvents();
                phoneField.Focus();
                phoneField.SelectAll();
            }
            else if (e.TabPage != null)
            {
                Application.DoEvents();
                ListBox tabListBox = (ListBox)e.TabPage.Tag;

                if (tabListBox != null) tabListBox.Focus();
            }

            resolveTransferButtonsState();
        }

        private bool in_tabWidget_KeyDown = false;

        private void tabControl_KeyDown(object unusedSender, KeyEventArgs e)
        {
            if (tabControl.SelectedTab == dialTab)
            {
                if (tabControl.Focused)
                {
                    if (in_tabWidget_KeyDown) return;
                    in_tabWidget_KeyDown = true;

                    switch (e.KeyCode)
                    {
                        case Keys.Down:
        				    e.Handled = true;
                            phoneField.Focus();
                            Application.DoEvents();
                            phoneField.SelectAll();
                            break;
                        case Keys.Enter:
        				    e.Handled = true;
                            doTransfer();
                            e.Handled = true;
                            break;
                    }

                    Application.DoEvents();

                    in_tabWidget_KeyDown = false;
                }
                else
                {
                    // if Up is pressed when on the first row, change focus
                    switch (e.KeyCode)
                    {
                        case Keys.Enter:
        				    e.Handled = true;
                            doTransfer();
                            break;
                    }
                }

                return;
            }

            if ( ! tabControl.Focused) return;

            if (tabControl.SelectedTab == null || tabControl.SelectedTab.Tag == null) return;

            ListBox tabListBox = (ListBox)tabControl.SelectedTab.Tag;

            if (in_tabWidget_KeyDown) return;
            in_tabWidget_KeyDown = true;

            try
            {
                switch (e.KeyCode)
                {
                    case Keys.Up:
				        e.Handled = true;
                        tabListBox.Focus();
                        Application.DoEvents();
                        SendKeys.Send("{UP}");
                        break;
                    case Keys.Down:
				        e.Handled = true;
                        tabListBox.Focus();
                        Application.DoEvents();
                        SendKeys.Send("{DOWN}");
                        break;
                    case Keys.PageUp:
				        e.Handled = true;
                        tabListBox.Focus();
                        Application.DoEvents();
                        SendKeys.Send("{PGUP}");
                        break;
                    case Keys.PageDown:
				        e.Handled = true;
                        tabListBox.Focus();
                        Application.DoEvents();
                        SendKeys.Send("{PGDN}");
                        break;
                    case Keys.Enter:
				        e.Handled = true;
                        doTransfer();
                        break;
                }
            }
            catch {}

            in_tabWidget_KeyDown = false;
        }

        private bool in_tabListBox_KeyDown = false;

        private void listBox_KeyDown(object unusedSender, KeyEventArgs e)
        {
            if (tabControl.Focused || in_tabListBox_KeyDown) return;
            in_tabListBox_KeyDown = true;

            try
            {
                switch (e.KeyData)
                {
                    case Keys.Left:
				        e.Handled = true;
                        if (tabControl.SelectedIndex != 0) tabControl.SelectedIndex--;
                        break;
                    case Keys.Right:
				        e.Handled = true;
                        if (tabControl.SelectedIndex != tabControl.TabCount - 1) tabControl.SelectedIndex++;
                        break;
			        case Keys.Enter:
				        e.Handled = true;
                        doTransfer();
                        break;
                    case Keys.Escape:
				        e.Handled = true;
				        this.Close();
                        break;
                }
            }
            catch {}

            in_tabListBox_KeyDown = false;
        }

		private void listBox_DoubleClick(object sender, System.EventArgs e)
		{
			doTransfer();
		}

        private void doTransfer()
        {
            string selectedNumber = "";
            string selectedName   = "";
            bool   numberEnabled  = false;

            if (tabControl.SelectedTab == dialTab)
            {
                if (phoneField.TextLength == 0) return;

                selectedNumber = phoneField.Text;
                selectedName   = phoneField.Text;
                numberEnabled  = true;
            }
            else
            {
                if (tabControl.SelectedTab == null) return;

                ListBox curListBox = (ListBox)tabControl.SelectedTab.Tag;

                if (curListBox.SelectedItem == null) return;

                PhonebookEntry selected = (PhonebookEntry) curListBox.SelectedItem;

                selectedNumber = selected.Number;
                selectedName   = selected.Name;
                numberEnabled  = selected.Enabled;

                if ( ! numberEnabled)
                {
				    MessageBox.Show(this, selectedNumber + " is disabled on this type of Trunk",
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    return;
                }
            }

            // ask for connection type
            TransferSelectionDialog transferSelectionDialog = new TransferSelectionDialog(selectedName);

            transferSelectionDialog.blindButton.      Enabled = BlindEnabled;
            transferSelectionDialog.attendedButton.   Enabled = AttendedEnabled;
            transferSelectionDialog.conferenceButton. Enabled = ConferenceEnabled;
            transferSelectionDialog.tandemButton.     Enabled = TandemEnabled;
            transferSelectionDialog.dialButton.       Enabled = DialOutEnabled;

            DialogResult result = transferSelectionDialog.ShowDialog(this);

            if (result == DialogResult.Cancel) return;

            currentTransferType = transferSelectionDialog.transferType;

            if (currentTransferType == Controller.TransferType.Dial)
            {
                controller.sendDialCommand(selectedNumber);

                resolveTransferButtonsState();

                // TODO: display cancel dialog?
            }
            else
            {
				TransferTo(selectedNumber, currentTransferType);

                resolveTransferButtonsState();

                displayPostTransferDialog(currentTransferType, selectedName, selectedNumber);
            }
        }

        private void displayPostTransferDialog(Controller.TransferType transferType, string selectedName, string selectedNumber)
        {
            // display dialog only for conference and attended
            if (transferType == Controller.TransferType.Conference || transferType == Controller.TransferType.Attended)
            {
                for (int i = 0; i < 20; ++i)
                {
                    string transferTypeStr = (transferType == Controller.TransferType.Tandem) ? "9-1-1" : transferType.ToString();

                    confirmCancelDialog = new ConfirmCancelDialog(
                        this, "Transferring to " + selectedName + " (" + transferTypeStr + ") ...", Application.ProductName);

                    ((Button)confirmCancelDialog.AcceptButton).Enabled = false;
                    ((Button)confirmCancelDialog.AcceptButton).Visible = (transferType == Controller.TransferType.Attended);

                    DialogResult Result = confirmCancelDialog.ShowDialog(this);
                    confirmCancelDialog = null;

                    if (Result == DialogResult.No)
                    {
                        // confirm canceling the transfer
                        Result = MessageBox.Show(this, "Stop " + transferTypeStr + " transfer to " + selectedName + "?",
                            Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

                        if (Result == DialogResult.Yes) controller.CancelTransfer(selectedNumber, transferType);
                        else                            continue;
                    }
                    else if (Result == DialogResult.Yes)
                    {
                        controller.CompleteTransfer(selectedNumber, currentCallId);
                    }

                    resolveTransferButtonsState();
                    break;
                }
            }
        }
		
		#region Button Event Handlers

        private void transferButton_Click(object sender, System.EventArgs e)
		{
            try
            {
                ToolStripButton button = (ToolStripButton)sender;
                TransferConfirm((Controller.TransferType)button.Tag);
            }
            catch {}
		}

		#endregion


        // Add this to the dial pad tab
		private void listBox_SelectedIndexChanged(object sender, System.EventArgs e)
		{
            // make sure the menus are enabled correctly
            setEnabled(transferEnabled, currentCallId);
		}

		private void TransferConfirm(Controller.TransferType transferType)
		{
            if ( ! transferEnabled && transferType != Controller.TransferType.Dial)
            {
                MessageBox.Show(this, "Transfering is disabled (check the currently selected call)",
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                return;
            }

            currentTransferType = transferType;

            string selectedNumber = "";
            string selectedName   = "";
            bool   numberEnabled  = false;

			if (tabControl.SelectedTab == dialTab)
            {
                if (phoneField.Text.Length == 0) return;

                selectedNumber = phoneField.Text;
                selectedName   = phoneField.Text;
                numberEnabled  = true;
            }
            else
            {
                if (tabControl.SelectedTab == null) return;

                ListBox curListBox = (ListBox)tabControl.SelectedTab.Tag;

                if (curListBox.SelectedItem == null) return;

                PhonebookEntry selected = (PhonebookEntry) curListBox.SelectedItem;

                selectedNumber = selected.Number;
                selectedName   = selected.Name;
                numberEnabled  = selected.Enabled;

                if ( ! numberEnabled)
                {
				    MessageBox.Show(this, selectedNumber + " is disabled on this type of Trunk",
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    return;
                }
            }

            string msgStr;

            string transferTypeStr = (transferType == Controller.TransferType.Tandem) ? "9-1-1" : transferType.ToString();

            if (transferType == Controller.TransferType.Dial) msgStr = "Dial ";
            else                                              msgStr = transferTypeStr + " Transfer to ";

            DialogResult Result = MessageBox.Show(this, msgStr + selectedName + "?",
                Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

            if (Result == DialogResult.Yes)
			{
                if (transferType == Controller.TransferType.Dial)
                {
                    controller.sendDialCommand(selectedNumber);

                    resolveTransferButtonsState();

                    // TODO: display cancel dialog?
                }
                else
                {
					TransferTo(selectedNumber, transferType);

                    resolveTransferButtonsState();

                    displayPostTransferDialog(transferType, selectedName, selectedNumber);
                }
            }
		}

		public void TransferTo(string PhoneNumber, Controller.TransferType transferType)
		{
            if ( ! transferEnabled) return;

            controller.TransferTo(PhoneNumber, transferType);
		}

        public void reset(string callId, bool successful)
        {
            if (currentCallId == callId || callId == null)
            {
                if (confirmCancelDialog != null)
                {
                    if (currentTransferType == Controller.TransferType.Attended && successful)
                    {
                        // enable the complete transfer button
                        ((Button)confirmCancelDialog.AcceptButton).Enabled = true;
                    }
                    else
                    {
                        confirmCancelDialog.Close();
                    }
                }
            }
        }

        // this reset() is called from the Console when the Phonebook button is clicked
        public static void reset()
        {
            if (confirmCancelDialog != null) confirmCancelDialog.Close();
        }

		private void Transfer_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Escape)
			{
				e.Handled = true;
				this.Close();
			}
		}

		private void menuClose_ItemClick(object sender, EventArgs e)
		{
			Hide();
		}

		public void Stop()
		{
			try
			{
				Logging.AppTrace("Stopping Phonebook");
				systemShutdown = true;
				Close();
			}
			catch (Exception ex)
			{
				Logging.ExceptionLogger("Error in Phonebook.Stop", ex, Console.LogID.Cleanup);
				Logging.AppTrace("Error Stopping Phonebook:" + ex.ToString());
			}
		}

		private void menuHelpAbout_ItemClick(object sender, EventArgs e)
		{
			Splash.ShowAbout(controller.Position);
		}

		private void transferButton_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Escape)
            {
                e.Handled = true;
                this.Close();
            }
		}

		private void listBox_DataSourceChanged(object sender, System.EventArgs e)
		{
            ListBox senderListBox = (ListBox)sender;

            bool enabled = (senderListBox.DataSource != null && senderListBox.Items.Count > 0);

            if (enabled) senderListBox.SelectedIndex = 0;

            printButton.Enabled = enabled;

            // make sure the menus are enabled correctly
            setEnabled(transferEnabled, currentCallId);
		}

        public void setDefaultDial(string defaultDestNumber)
        {
            phoneField.Text = defaultDestNumber;
            phoneFieldLeftKeyCount = 0;

            resolveTransferButtonsState();
        }

		private void phoneField_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            e.Handled = ! phoneFieldCharIsValid(e.KeyChar);
            phoneFieldLeftKeyCount = 0;
        }

        private bool phoneFieldCharIsValid(char c)
		{
			Logging.AppTrace( "KeyChar:" + Convert.ToInt32(c));

            // TODO: SIPVoipSDK: are all characters okay for SIP addresses?

            return true;
		}

        private int phoneFieldLeftKeyCount = 0;

		private void phoneField_KeyRelease(object sender, System.Windows.Forms.KeyEventArgs e)
		{
            resolveTransferButtonsState();

            if (e.KeyCode == Keys.Left)
            {
                // if the cursor is at the beginning of the field
                if (phoneField.SelectionStart == 0 && phoneField.SelectionLength == 0)
                {
                    ++phoneFieldLeftKeyCount;

                    // if Left is pressed twice, switch tabs
                    if ((phoneField.TextLength == 0 && phoneFieldLeftKeyCount == 2) ||
                        (phoneField.TextLength != 0 && phoneFieldLeftKeyCount == 3))
                    {
                        if (tabControl.SelectedIndex != 0) tabControl.SelectedIndex--;
                        phoneFieldLeftKeyCount = 0;
                    }
                }
            }
            else
            {
                phoneFieldLeftKeyCount = 0;
            }
        }

        private void phoneField_SelectedIndexChanged(object sender, EventArgs e)
        {
            resolveTransferButtonsState();
        }

        private void dialButton_Clicked(object sender, EventArgs e)
        {
            if (sender == null) return;

            phoneField.Focus();

            Button button = (Button)sender;

            if (phoneFieldCharIsValid(button.Tag.ToString()[0]))
            {
                int insertionIndex = phoneField.SelectionStart;

                string selectedText = phoneField.SelectedText;

                string newText  = phoneField.Text.Remove(phoneField.SelectionStart, phoneField.SelectionLength);
                phoneField.Text = newText.Insert(insertionIndex, button.Tag.ToString()[0].ToString());
                phoneFieldLeftKeyCount = 0;

                phoneField.SelectionStart = insertionIndex + 1;

                resolveTransferButtonsState();
            }
        }

		private void callButtonClick(object sender, System.EventArgs e)
		{
            if (phoneField.Text.Length != 0)
            {
                // remember dialed numbers
                dialHistory.Remove(phoneField.Text);
                dialHistory.Insert(0, phoneField.Text);

                // TODO: should be add the call history back someday?  (Bill didn't like it...)
                //phoneField.AutoCompleteCustomSource = dialHistory;

                string[] dialHistoryStrList = new string[dialHistory.Count];

                for (int i = 0; i < dialHistory.Count; ++i) dialHistoryStrList[i] = dialHistory[i];

                // TODO: should be add the call history back someday?  (Bill didn't like it...)
                //phoneField.Items.Clear();
                //phoneField.Items.AddRange(dialHistoryStrList);

                if (regSettings != null) regSettings.SetValue("DialHistory", dialHistoryStrList);

                controller.sendDialCommand(phoneField.Text);

                Close();
            }
        }

        private void clearDialHistory(object sender, EventArgs e)
        {
            dialHistory.Clear();

            // TODO: should be add the call history back someday?  (Bill didn't like it...)
            //phoneField.AutoCompleteCustomSource.Clear();
            //phoneField.Items.Clear();

            if (regSettings != null) regSettings.DeleteValue("DialHistory");
        }

		#region Printing
		private void printDoc_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
		{
            if (tabControl.SelectedTab == dialTab || tabControl.SelectedTab == null) return;

            ListBox curListBox = (ListBox)tabControl.SelectedTab.Tag;

            printerFont = new Font("Lucida Console", fontSize );
			printerBrush = new SolidBrush(Color.Black);
			LinePosition = TopMargin;

			printerFont = new Font(printerFont,FontStyle.Bold);
            WriteToPrinter(e.Graphics, "Transfer List - " + tabControl.SelectedTab.Text);
			printerFont = new Font(printerFont,FontStyle.Regular);

			e.Graphics.DrawLine(Pens.Black,new PointF(LeftMargin,LinePosition),new PointF(e.PageBounds.Width - LeftMargin,LinePosition));
			LinePosition += 5;

			printerFont = new Font(printerFont,FontStyle.Regular);

            foreach (Object item in curListBox.Items)
			{
				PhonebookEntry tranRec = (PhonebookEntry) item;
				WriteToPrinter(e.Graphics, tranRec.Name.PadRight(45) + " " + tranRec.Number);
			}
		}

		public void WriteToPrinter(Graphics graphic, string Text)
		{
			graphic.DrawString(Text,printerFont, printerBrush, (float) LeftMargin, LinePosition);
			LinePosition += printerFont.Height + 5;
		}


		private void printMenu_ItemClick(object sender, EventArgs e)
		{
			if (printSetup.ShowDialog() == DialogResult.OK)
				printDoc.Print();
		}

		#endregion

        public void setEnabled(bool enabled, string callId)
        {
            // don't do this check, otherwise, menus get enabled incorrectly
            //if (transferEnabled == enabled) return;

            if (currentCallId != callId) currentCallId = callId;

            if ( ! enabled)
            {
                // TODO: close confirmation dialogs if any are currenly shown
            }

            transferEnabled = enabled;

            resolveTransferButtonsState();
        }

        private void resolveTransferButtonsState()
        {
            blindButton.      Enabled = blindMenu.      Enabled = BlindEnabled;
            attendedButton.   Enabled = attendedMenu.   Enabled = AttendedEnabled;
            conferenceButton. Enabled = conferenceMenu. Enabled = ConferenceEnabled;
            tandemButton.     Enabled = tandemMenu.     Enabled = TandemEnabled;
            speedDialButton.  Enabled = speedDialMenu.  Enabled = DialOutEnabled;

            bool printEnabled = false;

            if (tabControl.SelectedTab != dialTab && tabControl.SelectedTab != null &&
                tabControl.SelectedTab.Tag != null)
            {
                printEnabled = (((ListBox)tabControl.SelectedTab.Tag).DataSource != null &&
                                ((ListBox)tabControl.SelectedTab.Tag).Items.Count > 0);
            }

            printButton.Enabled = printMenu.Enabled = printEnabled;
        }

        private bool isButtonEnabled(PhonebookEntry.EnableCode code)
        {
            if (tabControl.SelectedTab == dialTab) return (phoneField.Text.Length != 0);

            if (tabControl.SelectedTab == null) return false;

            ListBox curListBox = (ListBox)tabControl.SelectedTab.Tag;

            if (curListBox == null || curListBox.SelectedItem == null) return false;

            int selectedCode = ((PhonebookEntry)curListBox.SelectedItem).Code;

            if (selectedCode == 0) return true;

            return (code.GetHashCode() & selectedCode) != 0;
        }

        private bool BlindEnabled      {get {return blindEnabled      && isButtonEnabled(PhonebookEntry.EnableCode.Blind)      && transferEnabled;} }
        private bool AttendedEnabled   {get {return attendedEnabled   && isButtonEnabled(PhonebookEntry.EnableCode.Attended)   && transferEnabled;} }
        private bool ConferenceEnabled {get {return conferenceEnabled && isButtonEnabled(PhonebookEntry.EnableCode.Conference) && transferEnabled;} }
        private bool TandemEnabled     {get {return tandemEnabled     && isButtonEnabled(PhonebookEntry.EnableCode.Tandem)     && transferEnabled;} }
        private bool DialOutEnabled    {get {return                      isButtonEnabled(PhonebookEntry.EnableCode.Dial_Out);} }

        private void backButton_Click(object sender, EventArgs e)
        {
            phoneField.Focus();

            int selectionStart  = phoneField.SelectionStart;
            int selectionLength = phoneField.SelectionLength;

            if (selectionLength == 0)
            {
                if (phoneField.SelectionStart != 0)
                {
                    --selectionStart;
                    selectionLength = 1;
                }
                else
                {
                    return;
                }
            }

            string newText = phoneField.Text.Remove(selectionStart, selectionLength);
            phoneField.Text = newText;

            phoneField.SelectionStart = selectionStart;
            phoneFieldLeftKeyCount    = 0;

            resolveTransferButtonsState();
        }

        Timer tabChangedTimer = new Timer();

        private void tabContent_VisibleChanged(object sender, EventArgs e)
        {
            tabChangedTimer.Stop();
            tabChangedTimer.Tick += new EventHandler(tabChangedTimer_Tick);
            tabChangedTimer.Interval = 10;
            tabChangedTimer.Start();
        }

        private void tabChangedTimer_Tick(Object unusedSender, EventArgs unusedArgs)
        {
            tabChangedTimer.Stop();

            tabChanged(this, new TabControlEventArgs(
                tabControl.SelectedTab, tabControl.SelectedIndex, TabControlAction.Selected));
        }
    }

	public struct PhonebookEntry
	{
        public enum EnableCode
        {
            All        = 0,
            Blind      = 2,
            Attended   = 4,
            Conference = 8,
            Tandem     = 16,
            Dial_Out   = 32
        }

        private string name;
		private string number;
		private string group;
        private int    code;
        private bool   enabled;

        public string Name    {get {return name;   } }
        public string Number  {get {return number; } }
        public string Group   {get {return group;  } }
        public int    Code    {get {return code;   } }
        public bool   Enabled {get {return enabled;} set {enabled = value;} }

		public PhonebookEntry(string NewName, string NewNumber, string NewGroup, int NewCode)
		{
            if (NewGroup.ToLower() == "all") NewGroup = "All";

			name    = NewName;
			number  = NewNumber;
			group   = NewGroup;
            code    = NewCode;
            enabled = true;
		}
	}

	class PhonebookEntrySort : IComparer
	{   
		public PhonebookEntrySort() {}

		public int Compare(object x,object y)
		{
			return ((PhonebookEntry)x).Name.CompareTo(((PhonebookEntry)y).Name);                
		}
	}
}
