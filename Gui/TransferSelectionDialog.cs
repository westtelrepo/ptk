﻿using System.Windows.Forms;
using System.Drawing;

namespace WestTel.E911
{
    public partial class TransferSelectionDialog : BaseForm
    {
        public Controller.TransferType transferType = Controller.TransferType.Dial;

        public TransferSelectionDialog(string transferName)
        {
            InitializeComponent();
            setTranslatableText();

            Text = (Translate)("Connect to ") + transferName + "?";
        }

        private void setTranslatableText()
        {
            this.cancelButton.Text     = (Translate)("Cancel");
            this.dialButton.Text       = (Translate)("&Dial");
            this.conferenceButton.Text = (Translate)("&Conference");        
            this.attendedButton.Text   = (Translate)("&Attended");
            this.blindButton.Text      = (Translate)("&Blind");
            this.tandemButton.Text     = (Translate)("&9-1-1");
            this.flashButton.Text      = (Translate)("&Flash");
        }

        private void tandemButton_Click(object sender, System.EventArgs e)
        {
            transferType = Controller.TransferType.Tandem;
            Close();
        }

        private void flashButton_Click(object sender, System.EventArgs e)
        {
            transferType = Controller.TransferType.FlashHook;
            Close();
        }

        private void blindButton_Click(object sender, System.EventArgs e)
        {
            transferType = Controller.TransferType.Blind;
            Close();
        }

        private void attendedButton_Click(object sender, System.EventArgs e)
        {
            transferType = Controller.TransferType.Attended;
            Close();
        }

        private void conferenceButton_Click(object sender, System.EventArgs e)
        {
            transferType = Controller.TransferType.Conference;
            Close();
        }

        private void dialButton_Click(object sender, System.EventArgs e)
        {
            transferType = Controller.TransferType.Dial;
            Close();
        }

        private void TransferSelectionDialog_Shown(object sender, System.EventArgs e)
        {
            if      (tandemButton.     Visible && tandemButton.     Enabled) tandemButton.     Focus();
            else if (flashButton.      Visible && flashButton.      Enabled) flashButton.      Focus();
            else if (blindButton.      Visible && blindButton.      Enabled) blindButton.      Focus();
            else if (attendedButton.   Visible && attendedButton.   Enabled) attendedButton.   Focus();
            else if (conferenceButton. Visible && conferenceButton. Enabled) conferenceButton. Focus();
            else if (dialButton.       Visible && dialButton.       Enabled) dialButton.       Focus();

            // layout buttons
            int Y       = tandemButton.Location.Y;
            int startX  = tandemButton.Location.X;
            int spacing = flashButton.Location.X - startX;

            int n = 0;

            if (tandemButton.     Visible) ++n;
            if (flashButton.      Visible) ++n;
            if (blindButton.      Visible) ++n;
            if (attendedButton.   Visible) ++n;
            if (conferenceButton. Visible) ++n;
            if (dialButton.       Visible) ++n;

            int desiredWidth = ClientSize.Width - (6 - n) * spacing;

            int newWidth = (desiredWidth > 200) ? desiredWidth : 200;

            ClientSize = new Size(newWidth, ClientSize.Height);

            int curX = startX + (newWidth - desiredWidth) / 2;

            if (tandemButton.     Visible) {tandemButton.     Location = new Point(curX, Y); curX += spacing;}
            if (flashButton.      Visible) {flashButton.      Location = new Point(curX, Y); curX += spacing;}
            if (blindButton.      Visible) {blindButton.      Location = new Point(curX, Y); curX += spacing;}
            if (attendedButton.   Visible) {attendedButton.   Location = new Point(curX, Y); curX += spacing;}
            if (conferenceButton. Visible) {conferenceButton. Location = new Point(curX, Y); curX += spacing;}
            if (dialButton.       Visible) {dialButton.       Location = new Point(curX, Y); curX += spacing;}
        }
    }
}
