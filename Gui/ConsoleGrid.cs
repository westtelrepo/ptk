﻿
using System;
using System.Windows.Forms;

namespace WestTel.E911
{
    public class ConsoleGrid : Grid
    {
        private Console console;

        private BindingSource bindingSource;

        private DataGridViewTextBoxColumn StartCol;
        private DataGridViewTextBoxColumn CallTypeNumberCol;
        private DataGridViewTextBoxColumn StatusCol;
        private DataGridViewTextBoxColumn PositionCol;
        private DataGridViewTextBoxColumn LineConfCol;
        private DataGridViewTextBoxColumn DurationCol;

        public ConsoleGrid(string name): base(name)
        {
            InitializeComponent();
            setTranslatableText();
        }

        private void InitializeComponent()
        {
            this.StartCol               = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CallTypeNumberCol      = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusCol              = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PositionCol            = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LineConfCol            = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DurationCol            = new System.Windows.Forms.DataGridViewTextBoxColumn();

            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ConsoleGrid
            // 
            this.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.StartCol,
            this.CallTypeNumberCol,
            this.StatusCol,
            this.PositionCol,
            this.LineConfCol,
            this.DurationCol});
            // 
            // StartCol
            // 
            this.StartCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.StartCol.DataPropertyName = "RingDateTimeLocal";
            this.StartCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.StartCol.DefaultCellStyle.Format = "H:mm:ss\nM/d/yyyy";
            this.StartCol.DefaultCellStyle.NullValue = null;
            this.StartCol.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.StartCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.StartCol.HeaderText = "Time/Date";
            this.StartCol.Name = "StartCol";
            this.StartCol.ReadOnly = true;
            this.StartCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.StartCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.StartCol.Width = 100;
            // 
            // CallTypeNumberCol
            // 
            this.CallTypeNumberCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CallTypeNumberCol.DataPropertyName = "CallTypeNumber"; // CallType and Callback
            this.CallTypeNumberCol.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.CallTypeNumberCol.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.CallTypeNumberCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.CallTypeNumberCol.HeaderText = "Description and Type";
            this.CallTypeNumberCol.Name = "CallTypeNumberCol";
            this.CallTypeNumberCol.ReadOnly = true;
            this.CallTypeNumberCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.CallTypeNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.CallTypeNumberCol.Width = 175;
            // 
            // StatusCol
            // 
            this.StatusCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.StatusCol.DataPropertyName = "Status";
            this.StatusCol.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.StatusCol.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.StatusCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.StatusCol.HeaderText = "Status";
            this.StatusCol.Name = "StatusCol";
            this.StatusCol.ReadOnly = true;
            this.StatusCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.StatusCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.StatusCol.Width = 125;
            // 
            // PositionCol
            // 
            this.PositionCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.PositionCol.DataPropertyName = "PositionStr";
            this.PositionCol.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.PositionCol.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PositionCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.PositionCol.HeaderText = "Participants";
            this.PositionCol.Name = "PositionCol";
            this.PositionCol.ReadOnly = true;
            this.PositionCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.PositionCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.PositionCol.Width = 100;
            // 
            // LineConfCol
            // 
            this.LineConfCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.LineConfCol.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.LineConfCol.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.LineConfCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.LineConfCol.Name = "LineConfCol";
            this.LineConfCol.ReadOnly = true;
            this.LineConfCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.LineConfCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.LineConfCol.Width = 100;
            // 
            // DurationCol
            // 
            this.DurationCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DurationCol.DataPropertyName = "CallDuration";
            this.DurationCol.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DurationCol.DefaultCellStyle.Format = "hh\\:mm\\:ss";
            this.DurationCol.DefaultCellStyle.NullValue = null;
            this.DurationCol.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DurationCol.FillWeight = 100F;
            this.DurationCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.DurationCol.HeaderText = "Duration";
            this.DurationCol.Name = "DurationCol";
            this.DurationCol.ReadOnly = true;
            this.DurationCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DurationCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;

            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
        }

        private void setTranslatableText()
        {
            this.StartCol.HeaderText          = (Translate)("Time/Date");
            this.CallTypeNumberCol.HeaderText = (Translate)("Description and Type");
            this.StatusCol.HeaderText         = (Translate)("Status");
            this.PositionCol.HeaderText       = (Translate)("Participants");
            this.DurationCol.HeaderText       = (Translate)("Duration");
        }

        public void Initialize(Console _console, DataModel dataModel, bool showLineView)
        {
            try
            {
                console = _console;
                setSyncRoot(DataModel.callEventsSyncRoot);

                bindingSource = new BindingSource(dataModel.ds, "CallEvents");
                DataSource    = bindingSource;

                bindingSource.Sort = "RingDateTime DESC";

                if (showLineView)
                {
                    LineConfCol.DataPropertyName = "LineNumberStr";
                    LineConfCol.HeaderText       = (Translate)("Line");

                    LineConfCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                }
                else
                {
                    LineConfCol.DataPropertyName = "ConferenceNumberStr";
                    LineConfCol.HeaderText       = (Translate)("Conf #");

                    LineConfCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
            }
            catch {} // TODO* calls: why does an exception get thrown here for sorting one of the grids?
        }

        public BindingSource getBindingSource()  {return bindingSource;}
        public UInt64        getSelectedCallID() {if (selectedRowKey == null) return 0; return Convert.ToUInt64(selectedRowKey);}

        public void setSelectedCallID(object newKey, string context)
        {
            if (newKey != null && Convert.ToUInt64(newKey) == 0) newKey = null;

            if (Convert.ToUInt64(selectedRowKey) == Convert.ToUInt64(newKey)) return;

            string selectedRowKeyStr = "null";
            string newKeyStr         = "null";

            if (selectedRowKey != null) selectedRowKeyStr = selectedRowKey.ToString();
            if (newKey         != null) newKeyStr         = newKey.ToString();

            System.Diagnostics.Trace.WriteLine(Name + " - selected key set " +
                                               selectedRowKeyStr + " -> " +
                                               newKeyStr         + " " + context, "Info");
            selectedRowKey = newKey;
        }

        public void checkForRowUpdate(MessageInfo messageInfo)
        {
            if (console == null) return;

DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    if (messageInfo.psapMessage.Event == "ClearConsole")
                    {
                        if (Convert.ToInt32(messageInfo.psapMessage.Position) == console.getCurrentPosition())
                        {
                            ClearSelection();
                            Refresh();
                        }
                    }
                    else if (messageInfo.psapMessage.Event == "DeleteRecord")
                    {
                        if (selectedRowKey != null &&
                            selectedRowKey.ToString() == messageInfo.psapMessage.Call_ID)
                        {
                            setSelectedCallID(null, "ConsoleGrid:222");
                        }
                    }
                    else if (messageInfo.psapMessage.Event == "CallInfo")
                    {
                        DataGridViewRow         GCurrentRow = CurrentRow;
                        System.Data.DataRowView currentRow  = null;

                        if (GCurrentRow != null && GCurrentRow.DataBoundItem != null)
                        {
                            currentRow = (System.Data.DataRowView)GCurrentRow.DataBoundItem;

                            if (selectedRowKey == null) setSelectedCallID(getRowKey(CurrentRow), "ConsoleGrid:234");
System.Diagnostics.Trace.WriteLine(Name + " - current row  " + getRowKey(CurrentRow).ToString() + " ConsoleGrid:235", "Info");
System.Diagnostics.Trace.WriteLine(Name + " - current key  " + selectedRowKey.ToString()        + " ConsoleGrid:236", "Info");
                        }

                        bool forCurrentPosition = false;

                        if (messageInfo.psapMessage.PositionsActive != null && messageInfo.psapMessage.PositionsOnHold != null)
                        {
                            forCurrentPosition =
                                (messageInfo.psapMessage.PositionsActive.Contains("\tP" + console.getCurrentPosition().ToString() + "\t") &&
                               ! messageInfo.psapMessage.PositionsOnHold.Contains("\tP" + console.getCurrentPosition().ToString() + "\t"));
                        }

                        bool messageForCurrentRow = (currentRow != null && messageInfo.psapMessage.Call_ID == currentRow["Call_ID"].ToString());

                        foreach (DataGridViewRow GRow in Rows)
                        {
                            if ((GRow != null) && (GRow.DataBoundItem != null))
                            {
                                System.Data.DataRowView row = (System.Data.DataRowView) GRow.DataBoundItem;

                                // if this is the row for the call
                                if (row["Call_ID"].ToString() == messageInfo.psapMessage.Call_ID)
                                {
                                    // if this message is for the current position, select this row, unless it's on hold
                                    if (forCurrentPosition && ! messageForCurrentRow)
                                    {
                                        setSelectedCallID(getRowKey(GRow), "ConsoleGrid:253");
                                        CurrentCell    = GRow.Cells[0];
System.Diagnostics.Trace.WriteLine(Name + " - attempting to select " + selectedRowKey.ToString() + " ConsoleGrid:255", "Info");
                                        currentRow     = row;
                                    }

                                    break;
                                }
                            }
                        }

                        // if no row is selected, select the first row
                        if (Rows.Count != 0 && CurrentRow == null)
                        {
                            DataGridViewRow GRow = Rows[Rows.GetFirstRow(DataGridViewElementStates.Visible)];

                            if (GRow != null && GRow.DataBoundItem != null)
                            {
                                System.Data.DataRowView row = (System.Data.DataRowView)GRow.DataBoundItem;

                                setSelectedCallID(getRowKey(GRow), "ConsoleGrid:272");
                                CurrentCell    = GRow.Cells[0];
System.Diagnostics.Trace.WriteLine(Name + " - attempting to select " + selectedRowKey.ToString() + " ConsoleGrid:274", "Info");
                                currentRow     = row;
                            }
                        }

                        RefreshRowSelection("ConsoleGrid:279");

                        // determine if the row updated is the current row
                        if (currentRow != null)
                        {
                            Logging.AppTrace("Console.MessageReceived");
                            Logging.AppTrace("Message Status:" + messageInfo.psapMessage.Status);

                            if (messageInfo.psapMessage.Call_ID == currentRow["Call_ID"].ToString())
                            {
                                console.GridSelectionChanged(currentRow);
                            }
                        }
                    }
                }
                catch {}
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
        }

        override public void GridSelectionChanged(System.Data.DataRowView row)
        {
            try
            {
                if (console != null) console.GridSelectionChanged(row);

                base.GridSelectionChanged(row);
            } catch {}
        }

        new public void ClearSelection()
        {
DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (DataModel.callEventsSyncRoot)
            {
                try
                {
                    if (selectedRowKey != null)
                    {
                        setSelectedCallID(null, "ConsoleGrid:318");
                        base.ClearSelection();
                    }
                }
                catch (Exception ex)
                {
                    Logging.AppTrace("Error in Grid ClearSelection():" + ex.Message);    
                    Logging.ExceptionLogger("Error in ConsoleGrid.ClearSelection", ex, Console.LogID.Gui);
                }
            }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);
        }

        override public object getRowKey(DataGridViewRow GRow)
        {
            if (GRow != null && GRow.Index >= 0 && GRow.DataBoundItem != null)
            {
                System.Data.DataRowView row = (System.Data.DataRowView)GRow.DataBoundItem;

                return row["Call_ID"];
            }

            return null;
        }

        override protected bool isEqual(object key1, object key2) {return Convert.ToUInt64(key1) == Convert.ToUInt64(key2);}

        override protected void handleLeftRightKey(Keys keyCode) {console.handleLeftRightKeyFromGrid(keyCode);}

        override protected void newRowAdded(DataGridViewRow GRow)
        {
            if (GRow != null && GRow.DataBoundItem != null)
            {
                System.Data.DataRowView row = (System.Data.DataRowView)GRow.DataBoundItem;

                StatusCode statusCode = (StatusCode)Convert.ToInt32(row["StatusCode"]);

                // if this row just connected for this position, select it
                if (statusCode == StatusCode.Connected &&
                    (row["PositionsActive"].ToString().Contains("\tP" + console.getCurrentPosition().ToString() + "\t")))
                {
                    setSelectedCallID(getRowKey(GRow), "ConsoleGrid:356");
                }
            }
        }

        override protected bool isRowInTable(DataGridViewRow row)
        {
            // TODO calls: figure out how to tell if a row is filtered or not...
            return bindingSource.Contains(row.DataBoundItem);
        }
    }
}
