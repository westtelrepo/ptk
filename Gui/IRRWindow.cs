using System;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;
using CSCore;

namespace WestTel.E911
{
	/// <summary>
	/// Summary description for IRRWindow.
	/// </summary>
	public class IRRWindow : BaseForm
	{
        // configurable colors
        public static Settings.ColorInfo avgWavColorInfo =
            new Settings.ColorInfo(Color.Transparent, Color.Transparent, Color.DarkGreen, false);

        public static Settings.ColorInfo maxWavColorInfo =
            new Settings.ColorInfo(Color.Transparent, Color.Transparent, Color.FromArgb(40, Color.DodgerBlue), false);

        public static Settings.ColorInfo unselectedColorInfo =
            new Settings.ColorInfo(Color.FromArgb(100, Color.DarkGray), Color.Transparent, Color.Transparent, false);

        public static Settings.ColorInfo locationColorInfo =
            new Settings.ColorInfo(Color.Transparent, Color.Transparent, Color.FromArgb(80, Color.White), false);

        public static Settings.ColorInfo backgroundColorInfo =
            new Settings.ColorInfo(Color.Black, Color.Black, Color.Transparent, false);


        private bool  pendingGetCmd  = false;
        private ulong lastGetPieceId = 0;

        private Concentus.Structs.OpusDecoder opusDecoder;

        private CSCore.SoundOut.WasapiOut soundOut;
        private SoundSource               soundSource;

        private bool soundOutTempStop = false;

        const int sampleRate = 48000;
        const int nChannels  = 1;

        int   speedIncrements = 10;
        float speed           = 1.0f;

        private const int frameSize    = 960;
        private short[]   decodeBuffer = new short[frameSize];

        private System.Threading.Timer updateTimer;

        private int  selectionStartX = -1;
        private long selectionStart  = -1;
        private long selectionEnd    = -1;

        private const int samplesPerPixel = 3;

        private Pen   avgWaveformPen  = new Pen(avgWavColorInfo.   foregroundColor, 1.0f / samplesPerPixel);
        private Pen   maxWaveformPen  = new Pen(maxWavColorInfo.   foregroundColor, 1.0f / samplesPerPixel);
        private Pen   locationPen     = new Pen(locationColorInfo. foregroundColor, 1.0f);
        private Brush locationBrush   =         locationColorInfo.getForegroundBrush();
        private Brush unselectedBrush = new SolidBrush(unselectedColorInfo.backgroundColor1);

        private double waveformWidth = 0.0f;

        private readonly StringFormat rightToLeftStringFormat =
            new StringFormat(StringFormatFlags.DirectionRightToLeft);

        private StringFormat selectionStringFormat = new StringFormat();

        float       maxSample             = 0.5f;
        List<float> maxSampleList         = new List<float>();
        List<float> minSampleList         = new List<float>();
        List<float> positiveAvgSampleList = new List<float>();
        List<float> negativeAvgSampleList = new List<float>();

		private bool systemShutdown = false;

        private TableLayoutPanel tableLayoutPanel1;
        private MenuStrip MainMenu;
        private ToolStrip toolBar;
        private ToolStripMenuItem fileMenu;
        private ToolStripMenuItem closeMenu;
        private ToolStripMenuItem helpMenu;
        private ToolStripMenuItem aboutMenu;
        private ToolStripMenuItem usersManualMenu;
        private ToolStripSeparator helpMenuSep;

        private StatusStrip statusStrip;

		private Controller controller;

        private Label           waveformLabel;
        private ToolStripButton playToolButton;
        private ToolStripButton beginningToolButton;
        private ToolStripButton backToolButton;
        private ToolStripButton forwardToolButton;
        private ToolStripButton loopToolButton;
        private ToolStripButton slowerToolButton;
        private ToolStripButton fasterToolButton;
        private ToolStripButton saveAsToolButton;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripMenuItem saveAsMenu;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripDropDownButton optionsToolButton;
        private ToolStripMenuItem chooseAudioDeviceToolStripMenuItem;
        private ToolStripButton pauseToolButton;

		public IRRWindow(Controller c)
		{
			controller = c;
			InitializeComponent();
            setTranslatableText();

            this.     BackColor = SystemColors.Window;
            MainMenu. BackColor = SystemColors.Window;
            toolBar.  BackColor = SystemColors.Window;

            colorsUpdated();

            statusStrip.Visible = false;

            selectionStringFormat.Alignment     = StringAlignment.Center;
            selectionStringFormat.LineAlignment = StringAlignment.Near;

            try
            {
                // subscribe to relevant events
                controller.irrClient.receivedJsonOk    += new IrrClient.ReceivedJsonOk    (handleJsonOk);
                controller.irrClient.receivedJsonNoGet += new IrrClient.ReceivedJsonNoGet (handleJsonNoGet);
                controller.irrClient.receivedJsonError += new IrrClient.ReceivedJsonError (handleJsonError);
                controller.irrClient.receivedOpusPiece += new IrrClient.ReceivedOpusPiece (handleOpusPiece);
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to connect to IRR client events",
                                        ex, Console.LogID.Init);
            }

            try
            {
                // initialize OPUS decoder
                opusDecoder = new Concentus.Structs.OpusDecoder(sampleRate, nChannels);
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to create opus decoder", ex, Console.LogID.Init);
            }

            try
            {
                soundSource = new SoundSource(sampleRate, nChannels);

                soundOut = new CSCore.SoundOut.WasapiOut() { Latency = 10 };
                soundOut.Initialize(soundSource);
                soundOut.Stopped += SoundOut_Stopped;
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to create sound source", ex, Console.LogID.Init);
            }

            updateTimer = new System.Threading.Timer(refreshWaveformLabel);

            // do this AFTER soundOut has been initialized
            CustomInitializeComponent();
        }

        public void Stop()
		{
			try
			{
				Logging.AppTrace("Stopping IRR Display");
                if (soundOut != null) soundOut.Pause();
				systemShutdown = true;
				Close();
			}
			catch (Exception ex)
			{
				Logging.AppTrace("Error Stopping IRR Display:" + ex.ToString());
				Logging.ExceptionLogger("Error Stopping IRR Display", ex, Console.LogID.Cleanup);
			}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IRRWindow));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.toolBar = new System.Windows.Forms.ToolStrip();
            this.saveAsToolButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.playToolButton = new System.Windows.Forms.ToolStripButton();
            this.pauseToolButton = new System.Windows.Forms.ToolStripButton();
            this.beginningToolButton = new System.Windows.Forms.ToolStripButton();
            this.backToolButton = new System.Windows.Forms.ToolStripButton();
            this.forwardToolButton = new System.Windows.Forms.ToolStripButton();
            this.slowerToolButton = new System.Windows.Forms.ToolStripButton();
            this.fasterToolButton = new System.Windows.Forms.ToolStripButton();
            this.loopToolButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.optionsToolButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.chooseAudioDeviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.closeMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.usersManualMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuSep = new System.Windows.Forms.ToolStripSeparator();
            this.aboutMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.waveformLabel = new System.Windows.Forms.Label();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.tableLayoutPanel1.SuspendLayout();
            this.toolBar.SuspendLayout();
            this.MainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.toolBar, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.MainMenu, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.waveformLabel, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(534, 173);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // toolBar
            // 
            this.toolBar.AllowMerge = false;
            this.toolBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolBar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolBar.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveAsToolButton,
            this.toolStripSeparator2,
            this.playToolButton,
            this.pauseToolButton,
            this.beginningToolButton,
            this.backToolButton,
            this.forwardToolButton,
            this.slowerToolButton,
            this.fasterToolButton,
            this.loopToolButton,
            this.toolStripSeparator3,
            this.optionsToolButton});
            this.toolBar.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolBar.Location = new System.Drawing.Point(0, 24);
            this.toolBar.Name = "toolBar";
            this.toolBar.Size = new System.Drawing.Size(534, 58);
            this.toolBar.Stretch = true;
            this.toolBar.TabIndex = 2;
            this.toolBar.Text = "Tool Bar";
            // 
            // saveAsToolButton
            // 
            this.saveAsToolButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveAsToolButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.saveAsToolButton.Image = ((System.Drawing.Image)(resources.GetObject("saveAsToolButton.Image")));
            this.saveAsToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveAsToolButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.saveAsToolButton.Name = "saveAsToolButton";
            this.saveAsToolButton.Padding = new System.Windows.Forms.Padding(2);
            this.saveAsToolButton.Size = new System.Drawing.Size(32, 46);
            this.saveAsToolButton.Text = "&Save As";
            this.saveAsToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.saveAsToolButton.ToolTipText = "Save As (Ctrl+S)";
            this.saveAsToolButton.Click += new System.EventHandler(this.saveAsMenu_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 58);
            // 
            // playToolButton
            // 
            this.playToolButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.playToolButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.playToolButton.Image = ((System.Drawing.Image)(resources.GetObject("playToolButton.Image")));
            this.playToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.playToolButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.playToolButton.Name = "playToolButton";
            this.playToolButton.Padding = new System.Windows.Forms.Padding(2);
            this.playToolButton.Size = new System.Drawing.Size(32, 46);
            this.playToolButton.Text = "&Play";
            this.playToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.playToolButton.ToolTipText = "Play (Space)";
            this.playToolButton.Click += new System.EventHandler(this.playToolButton_Click);
            // 
            // pauseToolButton
            // 
            this.pauseToolButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pauseToolButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.pauseToolButton.Image = ((System.Drawing.Image)(resources.GetObject("pauseToolButton.Image")));
            this.pauseToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pauseToolButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.pauseToolButton.Name = "pauseToolButton";
            this.pauseToolButton.Padding = new System.Windows.Forms.Padding(2);
            this.pauseToolButton.Size = new System.Drawing.Size(32, 46);
            this.pauseToolButton.Text = "&Pause";
            this.pauseToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.pauseToolButton.ToolTipText = "Pause (Space)";
            this.pauseToolButton.Visible = false;
            this.pauseToolButton.Click += new System.EventHandler(this.pauseToolButton_Click);
            // 
            // beginningToolButton
            // 
            this.beginningToolButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.beginningToolButton.Image = ((System.Drawing.Image)(resources.GetObject("beginningToolButton.Image")));
            this.beginningToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.beginningToolButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.beginningToolButton.Name = "beginningToolButton";
            this.beginningToolButton.Padding = new System.Windows.Forms.Padding(2);
            this.beginningToolButton.Size = new System.Drawing.Size(42, 46);
            this.beginningToolButton.Text = "S&tart";
            this.beginningToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.beginningToolButton.ToolTipText = "Start (Home)";
            this.beginningToolButton.Click += new System.EventHandler(this.beginningToolButton_Click);
            // 
            // backToolButton
            // 
            this.backToolButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.backToolButton.Image = ((System.Drawing.Image)(resources.GetObject("backToolButton.Image")));
            this.backToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.backToolButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.backToolButton.Name = "backToolButton";
            this.backToolButton.Padding = new System.Windows.Forms.Padding(2);
            this.backToolButton.Size = new System.Drawing.Size(44, 46);
            this.backToolButton.Text = "&5 sec";
            this.backToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.backToolButton.ToolTipText = "5 sec (Left)";
            this.backToolButton.Click += new System.EventHandler(this.backToolButton_Click);
            // 
            // forwardToolButton
            // 
            this.forwardToolButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.forwardToolButton.Image = ((System.Drawing.Image)(resources.GetObject("forwardToolButton.Image")));
            this.forwardToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.forwardToolButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.forwardToolButton.Name = "forwardToolButton";
            this.forwardToolButton.Padding = new System.Windows.Forms.Padding(2);
            this.forwardToolButton.Size = new System.Drawing.Size(44, 46);
            this.forwardToolButton.Text = "&5 sec";
            this.forwardToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.forwardToolButton.ToolTipText = "5 sec (Right)";
            this.forwardToolButton.Click += new System.EventHandler(this.forwardToolButton_Click);
            // 
            // slowerToolButton
            // 
            this.slowerToolButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.slowerToolButton.Image = ((System.Drawing.Image)(resources.GetObject("slowerToolButton.Image")));
            this.slowerToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.slowerToolButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.slowerToolButton.Name = "slowerToolButton";
            this.slowerToolButton.Padding = new System.Windows.Forms.Padding(2);
            this.slowerToolButton.Size = new System.Drawing.Size(52, 46);
            this.slowerToolButton.Text = "Slo&wer";
            this.slowerToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.slowerToolButton.ToolTipText = "Slower (Down)";
            this.slowerToolButton.Click += new System.EventHandler(this.slowerToolButton_Click);
            // 
            // fasterToolButton
            // 
            this.fasterToolButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.fasterToolButton.Image = ((System.Drawing.Image)(resources.GetObject("fasterToolButton.Image")));
            this.fasterToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fasterToolButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.fasterToolButton.Name = "fasterToolButton";
            this.fasterToolButton.Padding = new System.Windows.Forms.Padding(2);
            this.fasterToolButton.Size = new System.Drawing.Size(48, 46);
            this.fasterToolButton.Text = "&Faster";
            this.fasterToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.fasterToolButton.ToolTipText = "Faster (Up)";
            this.fasterToolButton.Click += new System.EventHandler(this.fasterToolButton_Click);
            // 
            // loopToolButton
            // 
            this.loopToolButton.Checked = true;
            this.loopToolButton.CheckOnClick = true;
            this.loopToolButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.loopToolButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.loopToolButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.loopToolButton.Image = ((System.Drawing.Image)(resources.GetObject("loopToolButton.Image")));
            this.loopToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.loopToolButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.loopToolButton.Name = "loopToolButton";
            this.loopToolButton.Padding = new System.Windows.Forms.Padding(2);
            this.loopToolButton.Size = new System.Drawing.Size(32, 46);
            this.loopToolButton.Text = "&Loop";
            this.loopToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 58);
            // 
            // optionsToolButton
            // 
            this.optionsToolButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chooseAudioDeviceToolStripMenuItem});
            this.optionsToolButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.optionsToolButton.Image = ((System.Drawing.Image)(resources.GetObject("optionsToolButton.Image")));
            this.optionsToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.optionsToolButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.optionsToolButton.Name = "optionsToolButton";
            this.optionsToolButton.Padding = new System.Windows.Forms.Padding(2);
            this.optionsToolButton.Size = new System.Drawing.Size(66, 46);
            this.optionsToolButton.Text = "&Options";
            this.optionsToolButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // chooseAudioDeviceToolStripMenuItem
            // 
            this.chooseAudioDeviceToolStripMenuItem.Name = "chooseAudioDeviceToolStripMenuItem";
            this.chooseAudioDeviceToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.chooseAudioDeviceToolStripMenuItem.Text = "Choose Audio Device";
            // 
            // MainMenu
            // 
            this.MainMenu.AllowMerge = false;
            this.MainMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.helpMenu});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(534, 24);
            this.MainMenu.TabIndex = 1;
            this.MainMenu.Text = "Main Menu";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveAsMenu,
            this.toolStripSeparator1,
            this.closeMenu});
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(37, 20);
            this.fileMenu.Text = "&File";
            // 
            // saveAsMenu
            // 
            this.saveAsMenu.Image = ((System.Drawing.Image)(resources.GetObject("saveAsMenu.Image")));
            this.saveAsMenu.Name = "saveAsMenu";
            this.saveAsMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveAsMenu.Size = new System.Drawing.Size(163, 22);
            this.saveAsMenu.Text = "&Save As...";
            this.saveAsMenu.Click += new System.EventHandler(this.saveAsMenu_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(160, 6);
            // 
            // closeMenu
            // 
            this.closeMenu.Name = "closeMenu";
            this.closeMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.closeMenu.Size = new System.Drawing.Size(163, 22);
            this.closeMenu.Text = "&Close";
            this.closeMenu.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usersManualMenu,
            this.helpMenuSep,
            this.aboutMenu});
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(44, 20);
            this.helpMenu.Text = "&Help";
            // 
            // usersManualMenu
            // 
            this.usersManualMenu.Image = ((System.Drawing.Image)(resources.GetObject("usersManualMenu.Image")));
            this.usersManualMenu.Name = "usersManualMenu";
            this.usersManualMenu.Size = new System.Drawing.Size(140, 22);
            this.usersManualMenu.Text = "&User Manual";
            this.usersManualMenu.Click += new System.EventHandler(this.usersManualMenu_Click);
            // 
            // helpMenuSep
            // 
            this.helpMenuSep.Name = "helpMenuSep";
            this.helpMenuSep.Size = new System.Drawing.Size(137, 6);
            // 
            // aboutMenu
            // 
            this.aboutMenu.Image = ((System.Drawing.Image)(resources.GetObject("aboutMenu.Image")));
            this.aboutMenu.Name = "aboutMenu";
            this.aboutMenu.Size = new System.Drawing.Size(140, 22);
            this.aboutMenu.Text = "&About XXX";
            this.aboutMenu.Click += new System.EventHandler(this.aboutMenu_Click);
            // 
            // waveformLabel
            // 
            this.waveformLabel.BackColor = System.Drawing.Color.Black;
            this.waveformLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.waveformLabel.ForeColor = System.Drawing.Color.White;
            this.waveformLabel.Location = new System.Drawing.Point(0, 82);
            this.waveformLabel.Margin = new System.Windows.Forms.Padding(0);
            this.waveformLabel.Name = "waveformLabel";
            this.waveformLabel.Size = new System.Drawing.Size(534, 91);
            this.waveformLabel.TabIndex = 3;
            this.waveformLabel.Paint += new System.Windows.Forms.PaintEventHandler(this.waveformLabel_Paint);
            this.waveformLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.WaveformLabel_MouseDown);
            this.waveformLabel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.WaveformLabel_MouseMove);
            this.waveformLabel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.WaveformLabel_MouseUp);
            // 
            // statusStrip
            // 
            this.statusStrip.BackColor = System.Drawing.SystemColors.Window;
            this.statusStrip.Location = new System.Drawing.Point(0, 173);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(534, 22);
            this.statusStrip.SizingGrip = false;
            this.statusStrip.TabIndex = 6;
            // 
            // IRRWindow
            // 
            this.ClientSize = new System.Drawing.Size(534, 195);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.statusStrip);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.Icon = global::WestTel.E911.Properties.Resources.ConsoleIRRIcon;
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(0, 0);
            this.MainMenuStrip = this.MainMenu;
            this.Name = "IRRWindow";
            this.Text = "IRR";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.IRR_Closing);
            this.Load += new System.EventHandler(this.IRR_Load);
            this.VisibleChanged += new System.EventHandler(this.IRRWindow_VisibleChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.IRR_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IRR_KeyPress);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.toolBar.ResumeLayout(false);
            this.toolBar.PerformLayout();
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private void setTranslatableText()
        {
            try
            {
                this.toolBar.Text         = (Translate)("Tool Bar");
                this.MainMenu.Text        = (Translate)("Main Menu");
                this.fileMenu.Text        = (Translate)("&File");
                this.closeMenu.Text       = (Translate)("&Close");
                this.helpMenu.Text        = (Translate)("&Help");
                this.usersManualMenu.Text = (Translate)("&User Manual");
                this.aboutMenu.Text       = (Translate)("&About XXX");
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to set translated text", ex, Console.LogID.Init);
            }
        }

        private void CustomInitializeComponent()
        {
            try
            {
                // TODO IRR*: remember selected audio device between runs

                // iterate optional audio devices
                using (var mmdeviceEnumerator = new CSCore.CoreAudioAPI.MMDeviceEnumerator())
                {
                    using (var mmdeviceCollection = mmdeviceEnumerator.EnumAudioEndpoints(
                           CSCore.CoreAudioAPI.DataFlow.Render, CSCore.CoreAudioAPI.DeviceState.Active))
                    {
                        foreach (var device in mmdeviceCollection)
                        {
                            ToolStripMenuItem deviceMenuItem = new ToolStripMenuItem(device.FriendlyName);
                            chooseAudioDeviceToolStripMenuItem.DropDownItems.Add(deviceMenuItem);

                            if (device.DeviceID == soundOut.Device.DeviceID)
                            {
                                deviceMenuItem.Checked = true;
                            }

                            deviceMenuItem.Tag    = device;
                            deviceMenuItem.Click += deviceMenuItem_Click;
                            deviceMenuItem.CheckedChanged += deviceMenuItem_CheckedChanged;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to create device menu", ex, Console.LogID.Init);
            }
        }

        private void deviceMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (sender == null) return;

                ToolStripMenuItem deviceMenuItem = sender as ToolStripMenuItem;
                if (deviceMenuItem == null) return;

                deviceMenuItem.Checked = true;

                CSCore.CoreAudioAPI.MMDevice device = deviceMenuItem.Tag as CSCore.CoreAudioAPI.MMDevice;
                if (device == null) return;

                if (soundOut.Device == device) return;

                bool wasPlaying = (soundOut.PlaybackState == CSCore.SoundOut.PlaybackState.Playing);

                soundOut.Device = device;

                if (soundOut.PlaybackState != CSCore.SoundOut.PlaybackState.Stopped)
                {
                    soundOutTempStop = true;
                    soundOut.Stop();
                }

                soundOut.Initialize(soundOut.WaveSource);

                if (wasPlaying) soundOut.Play();
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to handle device selection", ex, Console.LogID.Gui);
            }
        }

        private void deviceMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (sender == null) return;

                ToolStripMenuItem deviceMenuItem = sender as ToolStripMenuItem;
                if (deviceMenuItem == null) return;

                if ( ! deviceMenuItem.Checked) return;

                foreach (ToolStripItem item in deviceMenuItem.GetCurrentParent().Items)
                {
                    if (item == deviceMenuItem) continue;

                    ToolStripMenuItem otherDeviceMenuItem = item as ToolStripMenuItem;
                    if (otherDeviceMenuItem == null) continue;

                    if (otherDeviceMenuItem.Checked)
                    {
                        otherDeviceMenuItem.Checked = false;
                        return;
                    }
                }
            }
            catch {} // if this method fails, the check won't be correct (not important)
        }

        System.Threading.Timer colorsUpdatedTimer = null;

        public void colorsUpdated()
        {
            if ( ! Visible)
            {
                if (colorsUpdatedTimer == null) colorsUpdatedTimer = new System.Threading.Timer(colorsUpdated);
                colorsUpdatedTimer.Change(500, -1);
            }
            else
            {
                colorsUpdated(null);
            }
        }

        private delegate void ColorsUpdatedDelegate();

        private void colorsUpdated(object notused)
        {
            try   { BeginInvoke(new ColorsUpdatedDelegate(ColorsUpdated)); }
            catch { ColorsUpdated(); }
        }

        private void ColorsUpdated()
        {
            try
            {
                avgWaveformPen.  Dispose();
                maxWaveformPen.  Dispose();
                locationPen.     Dispose();
                locationBrush.   Dispose();
                unselectedBrush. Dispose();

                avgWaveformPen = new Pen(avgWavColorInfo.   foregroundColor, 1.0f / samplesPerPixel);
                maxWaveformPen = new Pen(maxWavColorInfo.   foregroundColor, 1.0f / samplesPerPixel);
                locationPen    = new Pen(locationColorInfo. foregroundColor, 1.0f / samplesPerPixel);
                locationBrush  =         locationColorInfo. getForegroundBrush();

                unselectedBrush = new SolidBrush(unselectedColorInfo.backgroundColor1);

                waveformLabel.Refresh();
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to handle updated colors", ex, Console.LogID.Gui);
            }
        }

        private void btnClose_Click(object sender, System.EventArgs e) {Close();}

		private void IRR_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if ( ! systemShutdown)
			{
				e.Cancel = true;
				Hide();
			}
		}

		private void IRR_Load(object sender, System.EventArgs e)
		{
            // remove unnecessary separators
            removeUnnecessarySeparators(toolBar);

            try
            {
                Text = Application.ProductName + (Translate)(" - IRR");
                aboutMenu.Text = aboutMenu.Text.Replace("XXX",Application.ProductName);
            }
            catch {}
		}

        private void aboutMenu_Click(object sender, EventArgs e)
		{
			Splash.ShowAbout(controller.Position);
		}

		private void usersManualMenu_Click(object sender, EventArgs e)
		{
			Console.ShowUsersManual();
		}

        private void IRRWindow_VisibleChanged(object sender, EventArgs e)
        {
            try
            {
                if ( ! Visible)
                {
                    if (soundOut != null)
                    {
                        // stop receiving new OPUS pieces for the previous stream ID
                        if (currentStreamId.Length != 0) controller.irrClient.sendStopCommand(currentStreamId);

                        // stop any previous sound playback
                        soundOut.Pause();
                    }

                    pendingGetCmd   = false;
                    currentStreamId = "";

                    maxSample = 0.5f;
                    maxSampleList.Clear();
                    minSampleList.Clear();
                    positiveAvgSampleList.Clear();
                    negativeAvgSampleList.Clear();
                    clearSelection();

                    soundSource.clear();

                    playToolButton.Visible  = true;
                    pauseToolButton.Visible = false;

                    playToolButton.Enabled = pauseToolButton.Enabled   = beginningToolButton.Enabled = false;
                    backToolButton.Enabled = forwardToolButton.Enabled = loopToolButton.Enabled      = false;
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to clear call audio", ex, Console.LogID.Gui);
            }
        }

        private void IRR_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyCode)
                {
                    case Keys.Escape:   e.Handled = true; clearSelection();     break;
                    case Keys.F10:      e.Handled = true;                       break;
                    case Keys.Home:     e.Handled = true; moveToStart(e.Shift); break;
                    case Keys.Right:    e.Handled = true; move( 1.0f, e.Shift); break;
                    case Keys.Left:     e.Handled = true; move(-1.0f, e.Shift); break;
                    case Keys.End:      e.Handled = true; moveToEnd  (e.Shift); break;
                    case Keys.Space:    e.Handled = true; togglePlay();         break;
                    case Keys.Add:      e.Handled = true; changeSpeedBy( 1);    break;
                    case Keys.Subtract: e.Handled = true; changeSpeedBy(-1);    break;
                    case Keys.Up:       e.Handled = true; changeSpeedBy( 1);    break;
                    case Keys.Down:     e.Handled = true; changeSpeedBy(-1);    break;
                }
            }
            catch {}
        }

        private void IRR_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 1) Close();
        }

        private void WaveformLabel_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                if (soundSource.Length != 0 && e.Button == MouseButtons.Left)
                {
                    selectionStartX = e.X;
                    clearSelection();

                    selectionStart = (long)(e.X / waveformWidth * soundSource.Length + 0.5);

                    waveformLabel.Refresh();
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to handle mouse down", ex, Console.LogID.Gui);
            }
        }

        private void WaveformLabel_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (soundSource.Length != 0 && e.Button == MouseButtons.Left)
                {
                    // if a drag is detected
                    if (selectionEnd != -1 || Math.Abs(selectionStartX - e.X) > 2)
                    {
                        selectionEnd = (long)(e.X / waveformWidth * soundSource.Length + 0.5);

                        if      (selectionEnd < 0)                  selectionEnd = 0;
                        else if (selectionEnd > soundSource.Length) selectionEnd = soundSource.Length;
                    }

                    waveformLabel.Refresh();
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to handle mouse move", ex, Console.LogID.Gui);
            }
        }

        private void WaveformLabel_MouseUp(object sender, MouseEventArgs e)
        {
            try
            {
                if (soundSource.Length != 0 && e.Button == MouseButtons.Left)
                {
                    // if no selection, move the play position
                    if (selectionEnd == -1)
                    {
                        soundSource.Position = (long)(e.X / waveformWidth * soundSource.Length + 0.5);
                    }
                    else
                    {
                        // if end is before start, swap them
                        if (selectionEnd < selectionStart)
                        {
                            var tempStart  = selectionStart;
                            selectionStart = selectionEnd;
                            selectionEnd   = tempStart;
                        }

                        if (soundSource.Position < selectionStart || soundSource.Position > selectionEnd)
                        {
                            soundSource.Position = selectionStart;
                        }

                        soundSource.StopPosition = selectionEnd;
                    }

                    waveformLabel.Refresh();
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to handle mouse up", ex, Console.LogID.Gui);
            }
        }

        private System.Data.DataRowView currentCallRow  = null;
        private string                  currentStreamId = "";

        public void setCurrentCallId(System.Data.DataRowView callRow, bool isActiveCall)
        {
            try
            {
                currentCallRow = callRow;

                ArrayList streamList = null;

                if (currentCallRow != null)
                {
                    object streamListObj = currentCallRow["StreamList"];

                    if (streamListObj != DBNull.Value) streamList = (ArrayList)streamListObj;
                }

                if (streamList == null || streamList.Count == 0) return;

                playToolButton.Enabled = pauseToolButton.Enabled   = beginningToolButton.Enabled = true;
                backToolButton.Enabled = forwardToolButton.Enabled = loopToolButton.Enabled    = true;

                // TODO IRR: show call info somewhere?

                // TODO IRR: check cache for this stream and return if found and not-active

                // TODO IRR: handle multiple streams

                // ask for stream 0
                currentStreamId = ((StreamInfo)streamList[0]).id;
                pendingGetCmd   = true;

                opusDecoder.ResetState();

                // TODO IRR*: rework this according to Matt's recommendations

                // send a play command (even for non-active calls in case the IRR server is still indexing the file)
                controller.irrClient.sendPlayCommand(currentStreamId);

                // send a streamInfo command for redundancy to ask for num_pieces
                controller.irrClient.sendStreamInfoCommand(currentStreamId);
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to setup call audio", ex, Console.LogID.Gui);
            }
        }

        // handle JSON
        private void handleJsonOk(IrrClient.JsonOk jsonOk)
        {
            try
            {
                if (pendingGetCmd && jsonOk.num_pieces != 0)
                {
                    pendingGetCmd = false;

                    lastGetPieceId = jsonOk.num_pieces - 1;
                    controller.irrClient.sendGetCommand(currentStreamId, 0, lastGetPieceId);
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to handle JSON message",
                                        ex, Console.LogID.Connection);
            }
        }

        private void handleJsonNoGet(IrrClient.JsonNoGet jsonNoGet)
        {
            handleOpusPiece(jsonNoGet.stream, jsonNoGet.piece_num, null, 0);
        }

        private void handleJsonError(IrrClient.JsonError jsonError)
        {
            if (jsonError.e == "EXIST")
            {
                // TODO IRR*: handle EXIST errors from PLAY and STREAMINFO
            }
            else
            {
                Controller.sendAlertToController("IRR Client received unknown JSON error: " + jsonError.e,
                    Console.LogType.Warning, Console.LogID.Connection);
            }
        }

        class OpusPiece
        {
            public string StreamId   { get; set; }
            public ulong  PieceId    { get; set; }
            public byte[] PieceBytes { get; set; }
            public ushort PieceSize  { get; set; }
        }

        private ArrayList storedOpusPieces = new ArrayList();

        // handle OPUS piece
        private void handleOpusPiece(string streamId, ulong pieceId, byte[] pieceBytes, ushort pieceSize)
        {
            try
            {
                if (pendingGetCmd)
                {
                    pendingGetCmd = false;

                    lastGetPieceId = pieceId - 1;
                    controller.irrClient.sendGetCommand(currentStreamId, 0, lastGetPieceId);
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to issue GET command", ex, Console.LogID.Connection);
            }

            try
            {
                // store pieces that are in the future (from PLAY) until all the GET pieces come in
                if (pieceId > lastGetPieceId)
                {
                    storedOpusPieces.Add(new OpusPiece
                    {
                        StreamId = streamId, PieceId = pieceId, PieceBytes = pieceBytes, PieceSize = pieceSize
                    });

                    return;
                }

                int nSamplesDecoded = opusDecoder.Decode(
                    pieceBytes, 0, pieceSize, decodeBuffer, 0, frameSize);

                long samplePositiveTotal = 0, samplePositiveCount = 0,
                     sampleNegativeTotal = 0, sampleNegativeCount = 0;

                float sampleMax = 0.0f, sampleMin = 0.0f;

                for (int i = 0; i != nSamplesDecoded; ++i)
                {
                    short sample = decodeBuffer[i];

                    // add sample to sound source
                    soundSource.addSample((long)pieceId * frameSize + i, sample);

                    if (sample > 0)
                    {
                        ++samplePositiveCount;
                        samplePositiveTotal += sample;

                        if (sample > sampleMax) sampleMax = sample;
                    }
                    else
                    {
                        ++sampleNegativeCount;
                        sampleNegativeTotal += sample;

                        if (sample < sampleMin) sampleMin = sample;
                    }
                }

                float samplePositiveAvg = 0.0f, sampleNegativeAvg = 0.0f;

                if (samplePositiveCount != 0) samplePositiveAvg = (float)(samplePositiveTotal / samplePositiveCount) / short.MaxValue;
                if (sampleNegativeCount != 0) sampleNegativeAvg = (float)(sampleNegativeTotal / sampleNegativeCount) / short.MaxValue;

                sampleMax /= short.MaxValue;
                sampleMin /= short.MaxValue;

                // remember overall max
                if (sampleMax > maxSample) maxSample = sampleMax;

                // store piece calculations for display purposes
                maxSampleList.Add(sampleMax);
                minSampleList.Add(sampleMin);

                positiveAvgSampleList.Add(samplePositiveAvg);
                negativeAvgSampleList.Add(sampleNegativeAvg);

                if (pieceId % 50 == 0)
                {
                    waveformLabel.BeginInvoke(new refreshDelegate(refreshWaveformLabel));
                }

                // if not playing, make sure the final pieces get drawn
                if (soundOut.PlaybackState != CSCore.SoundOut.PlaybackState.Playing)
                {
                    updateTimer.Change(100, -1);
                }

                if (pieceId == lastGetPieceId)
                {
                    ++lastGetPieceId;

                    if (storedOpusPieces.Count != 0)
                    {
                        OpusPiece p = (OpusPiece)storedOpusPieces[0];
                        storedOpusPieces.RemoveAt(0);

                        lastGetPieceId = p.PieceId;
                        handleOpusPiece(p.StreamId, p.PieceId, p.PieceBytes, p.PieceSize);
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to handle OPUS piece", ex, Console.LogID.Connection);
            }
        }

        private void playToolButton_Click(object sender, EventArgs e) {play();}

        private void play()
        {
            try
            {
                if (soundOut != null && soundSource.Length != 0)
                {
                    playToolButton.  Visible = false;
                    pauseToolButton. Visible = true;

                    soundOut.Play();

                    waveformLabel.Refresh();
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to play", ex, Console.LogID.Gui);
            }
        }

        private void pauseToolButton_Click(object sender, EventArgs e) {pause();}

        private void pause()
        {
            try
            {
                if (soundOut != null)
                {
                    pauseToolButton. Visible = false;
                    playToolButton.  Visible = true;

                    soundOut.Pause();

                    waveformLabel.Refresh();
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to pause", ex, Console.LogID.Gui);
            }
        }

        private void togglePlay()
        {
            if (soundOut != null)
            {
                if (soundOut.PlaybackState == CSCore.SoundOut.PlaybackState.Playing) pause();
                else                                                                 play();
            }
        }

        private void beginningToolButton_Click (object sender, EventArgs e) {moveToStart();}
        private void backToolButton_Click      (object sender, EventArgs e) {move(-5.0f);}
        private void forwardToolButton_Click   (object sender, EventArgs e) {move( 5.0f);}
        private void slowerToolButton_Click    (object sender, EventArgs e) {changeSpeedBy(-5);}
        private void fasterToolButton_Click    (object sender, EventArgs e) {changeSpeedBy( 5);}

        void moveToStart(bool moveSelection = false)
        {
            try
            {
                if (soundSource.Length != 0)
                {
                    if (moveSelection)
                    {
                        selectionStart = 0;
                        selectionEnd   = soundSource.Position;

                        soundSource.Position     = selectionStart;
                        soundSource.StopPosition = selectionEnd;
                    }
                    else
                    {
                        if (selectionEnd != -1) soundSource.Position = selectionStart;
                        else                    soundSource.Position = 0;
                    }

                    waveformLabel.Refresh();
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to move to start", ex, Console.LogID.Gui);
            }
        }

        void moveToEnd(bool moveSelection = false)
        {
            try
            {
                if (soundSource.Length != 0)
                {
                    if (moveSelection)
                    {
                        selectionStart = soundSource.Position;
                        selectionEnd   = soundSource.Length;

                        soundSource.Position     = selectionStart;
                        soundSource.StopPosition = selectionEnd;
                    }
                    else
                    {
                        if (selectionEnd != -1) soundSource.Position = selectionEnd;
                        else                    soundSource.Position = soundSource.Length;
                    }

                    waveformLabel.Refresh();
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to move to end", ex, Console.LogID.Gui);
            }
        }

        void move(float sec, bool moveSelection = false)
        {
            try
            {
                if (soundSource.Length != 0)
                {
                    long newPos = soundSource.Position +
                        (long)(sec * soundSource.WaveFormat.BytesPerSecond / speed);

                    if (moveSelection)
                    {
                        if (selectionEnd == -1)
                        {
                            if (newPos < soundSource.Position)
                            {
                                selectionStart = newPos;
                                selectionEnd   = soundSource.Position;
                            }
                            else
                            {
                                selectionStart = soundSource.Position;
                                selectionEnd   = newPos;
                            }
                        }
                        else
                        {
                            if      (newPos < selectionStart) selectionStart = newPos;
                            else if (newPos > selectionEnd)   selectionEnd   = newPos;
                        }

                        if (selectionStart < 0)                  selectionStart = 0;
                        if (selectionEnd   > soundSource.Length) selectionEnd   = soundSource.Length;

                        soundSource.StopPosition = selectionEnd;
                    }
                    else
                    {
                        if (selectionEnd != -1)
                        {
                            if      (newPos < selectionStart) newPos = selectionStart;
                            else if (newPos > selectionEnd)   newPos = selectionEnd;
                        }
                    }

                    soundSource.Position = newPos;
                    waveformLabel.Refresh();
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to move position in audio", ex, Console.LogID.Gui);
            }
        }

        private void clearSelection()
        {
            try
            {
                if (selectionEnd != -1)
                {
                    selectionStart = selectionEnd = -1;
                    soundSource.StopPosition      = -1;
                    waveformLabel.Refresh();
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to clear selection", ex, Console.LogID.Gui);
            }
        }

        private void changeSpeedBy(int delta) {changeSpeedTo(speedIncrements + delta);}

        private void changeSpeedTo(int speedInc)
        {
            try
            {
                speedIncrements = speedInc;
                speed           = speedIncrements * 0.1f;

                if      (speed < 0.3f) {speed = 0.3f; speedIncrements = 3;}
                else if (speed > 3.0f) {speed = 3.0f; speedIncrements = 30;}

                bool wasPlaying = soundOut.PlaybackState == CSCore.SoundOut.PlaybackState.Playing;

                if (soundOut.PlaybackState != CSCore.SoundOut.PlaybackState.Stopped)
                {
                    soundOutTempStop = true;
                    soundOut.Stop();
                }

                // create a new soundSource that takes control from the old soundSource
                soundSource = new SoundSource((int)(sampleRate * speed + 0.5), soundSource);

                CSCore.Streams.Effects.PitchShifter pitchShifter;

                CSCore.ISampleSource sampleSource = soundSource.ToSampleSource().
                    AppendSource(x => new CSCore.Streams.Effects.PitchShifter(x), out pitchShifter);

                pitchShifter.PitchShiftFactor = 1.0f / speed;

                soundOut.Initialize(sampleSource.ToWaveSource());

                if (wasPlaying) soundOut.Play();

                waveformLabel.Refresh();
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to change audio speed", ex, Console.LogID.Gui);
            }
        }

        private void SoundOut_Stopped(object sender, CSCore.SoundOut.PlaybackStoppedEventArgs e)
        {
            try
            {
                if (soundOutTempStop) { soundOutTempStop = false; return; }

                moveToStart();

                if (loopToolButton.Checked) play();
                else                        pause();
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to handle audio stopped", ex, Console.LogID.Gui);
            }
        }

        private delegate void refreshDelegate();

        private void refreshWaveformLabel(object obj = null)
        {
            waveformLabel.BeginInvoke(new refreshDelegate(refreshWaveformLabel));
        }

        private void refreshWaveformLabel() {waveformLabel.Refresh();}

        private void waveformLabel_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                e.Graphics.Clear(backgroundColorInfo.backgroundColor1);

                if (soundSource.Length != 0)
                {
                    // draw waveform
                    float halfHeight = waveformLabel.Height / 2.0f;

                    long nSamples = soundSource.Length / soundSource.WaveFormat.BytesPerBlock / frameSize;

                    // calculate the width of the samples (wider than the label so it's not pixelated)
                    float samplesWidth = waveformLabel.Width * samplesPerPixel;
                    if (samplesWidth > nSamples) samplesWidth = nSamples;

                    waveformWidth = samplesWidth / samplesPerPixel;

                    // calculate how often to sample (depends on the width of the window and the number of samples)
                    float resampleIncrement = nSamples / (float)samplesWidth;

                    // calculate the height multiplier based on the max sample
                    float htMultiplier = (halfHeight * 1.0f / maxSample) * 1.5f;

                    for (int sampleX = 0; sampleX != samplesWidth; ++sampleX)
                    {
                        int i = (int)(sampleX * resampleIncrement + 0.5);

                        if (i >= maxSampleList.Count) break;

                        float x = sampleX / samplesPerPixel;

                        e.Graphics.DrawLine(maxWaveformPen, x, (maxSampleList[i]         * htMultiplier) + halfHeight,
                                                            x, (minSampleList[i]         * htMultiplier) + halfHeight);
                        e.Graphics.DrawLine(avgWaveformPen, x, (positiveAvgSampleList[i] * htMultiplier) + halfHeight,
                                                            x, (negativeAvgSampleList[i] * htMultiplier) + halfHeight);
                    }

                    // draw selection
                    if (selectionEnd != -1)
                    {
                        float startX = (float)(selectionStart / (double)soundSource.Length * waveformWidth);
                        float endX   = (float)(selectionEnd   / (double)soundSource.Length * waveformWidth);

                        // if end is before start, swap them
                        if (endX < startX) {var tempStart = startX; startX = endX; endX = tempStart;}

                        e.Graphics.FillRectangle(unselectedBrush, 0,    0, startX,                        waveformLabel.Height);
                        e.Graphics.FillRectangle(unselectedBrush, endX, 0, (float)(waveformWidth - endX), waveformLabel.Height);
                    }

                    // draw position line
                    int posX = (int)(soundSource.GetPosition().TotalMilliseconds /
                                     soundSource.GetLength().TotalMilliseconds * waveformWidth + 0.5);

                    e.Graphics.DrawLine(locationPen, posX, 0, posX, waveformLabel.Height);

                    // if playing, schedule the next update
                    if (soundOut.PlaybackState == CSCore.SoundOut.PlaybackState.Playing)
                    {
                        updateTimer.Change(50, -1);
                    }
                }

                // draw playback time
                TimeSpan positionTimeSpan = new TimeSpan((long)(soundSource.GetPosition(). Ticks * (double)speed));
                TimeSpan lengthTimeSpan   = new TimeSpan((long)(soundSource.GetLength().   Ticks * (double)speed));

                e.Graphics.DrawString(positionTimeSpan.ToString("m\\:ss\\.f") + " / " +
                                      lengthTimeSpan.ToString("m\\:ss\\.f"),
                                      Font, locationBrush, 0, 0);

                if (selectionEnd != -1)
                {
                    long   selectionDelta = Math.Abs(selectionEnd - selectionStart);
                    double selectionMsec  = soundSource.WaveFormat.BytesToMilliseconds(selectionDelta);

                    TimeSpan selectionTimeSpan = TimeSpan.FromMilliseconds(selectionMsec);

                    e.Graphics.DrawString("Selection " + selectionTimeSpan.ToString("m\\:ss\\.f"),
                                          Font, locationBrush, DisplayRectangle, selectionStringFormat);
                }

                e.Graphics.DrawString("x" + speed.ToString("0.0"), Font, locationBrush,
                    waveformLabel.Width, 0, rightToLeftStringFormat);
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed to draw waveform", ex, Console.LogID.Gui);
            }
        }

        private void saveAsMenu_Click(object sender, EventArgs e)
        {
            if (soundSource.Length == 0) return;

            string filetypes = "Wav File (*.wav)|*.wav";

            CSCore.MediaFoundation.MFMediaType[] supportedFormats;

            try
            {
                supportedFormats = CSCore.MediaFoundation.
                    MediaFoundationEncoder.GetEncoderMediaTypes(AudioSubTypes.MpegLayer3);

                if (supportedFormats.Length != 0) filetypes += "|MP3 File (*.mp3)|*.mp3";
            }
            catch { }

            try
            {
                supportedFormats = CSCore.MediaFoundation.
                    MediaFoundationEncoder.GetEncoderMediaTypes(AudioSubTypes.WmaVoice9);

                if (supportedFormats.Length != 0) filetypes += "|WMA File (*.wma)|*.wma";
            }
            catch { }

            try
            {
                supportedFormats = CSCore.MediaFoundation.
                    MediaFoundationEncoder.GetEncoderMediaTypes(AudioSubTypes.MPEG_HEAAC);

                if (supportedFormats.Length != 0) filetypes += "|ACC File (*.acc)|*.acc";
            }
            catch { }

            try
            {
                using (SaveFileDialog saveFileDialog = new SaveFileDialog())
                {
                    saveFileDialog.Filter           = filetypes;
                    saveFileDialog.FilterIndex      = 2;
                    saveFileDialog.RestoreDirectory = true;
                    saveFileDialog.AddExtension     = true;
                    saveFileDialog.OverwritePrompt  = true;

                    if (selectionEnd != -1) saveFileDialog.Title = "Save Selection As";
                    else                    saveFileDialog.Title = "Save Recording As";

                    if (saveFileDialog.ShowDialog() != DialogResult.OK) return;

                    // remember settings
                    long rememberedPosition = soundSource.Position;
                    int  rememberedSpeedInc = speedIncrements;

                    bool wasPlaying = soundOut.PlaybackState == CSCore.SoundOut.PlaybackState.Playing;

                    if (soundOut.PlaybackState != CSCore.SoundOut.PlaybackState.Stopped)
                    {
                        soundOutTempStop = true;
                        soundOut.Stop();
                    }

                    // set soundOutTempStop for when the sound stops at the end of recording it to the file
                    soundOutTempStop = true;

                    changeSpeedTo(10);

                    if (selectionEnd != -1) soundSource.Position = selectionStart;
                    else                    soundSource.Position = 0;

                    string filename  = saveFileDialog.FileName;
                    string extension = new FileInfo(filename).Extension.ToLower();

                    byte[] buf = new byte[soundSource.WaveFormat.BytesPerSecond];

                    try
                    {
                        switch (extension)
                        {
                            case ".wav":
                            {
                                using (var fileStream = File.OpenWrite(filename)) {soundSource.WriteToWaveStream(fileStream);}
                                break;
                            }
                            case ".mp3":
                            {
                                using (var encoder = CSCore.MediaFoundation.MediaFoundationEncoder.CreateMP3Encoder(soundSource.WaveFormat, filename))
                                {
                                    int i; while ((i = soundSource.Read(buf, 0, buf.Length)) > 0) encoder.Write(buf, 0, i);
                                }

                                break;
                            }
                            case ".acc":
                            {
                                using (var encoder = CSCore.MediaFoundation.MediaFoundationEncoder.CreateAACEncoder(soundSource.WaveFormat, filename))
                                {
                                    int i; while ((i = soundSource.Read(buf, 0, buf.Length)) > 0) encoder.Write(buf, 0, i);
                                }

                                break;
                            }
                            case ".wma":
                            {
                                using (var encoder = CSCore.MediaFoundation.MediaFoundationEncoder.CreateWMAEncoder(soundSource.WaveFormat, filename))
                                {
                                    int i; while ((i = soundSource.Read(buf, 0, buf.Length)) > 0) encoder.Write(buf, 0, i);
                                }

                                break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(this, "Save As failed (" + ex.Message + ")",
                            "Save As", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    // restore settings
                    soundSource.Position = rememberedPosition;

                    changeSpeedTo(rememberedSpeedInc);

                    if (wasPlaying) soundOut.Play();
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IRR Window failed in Save As", ex, Console.LogID.Gui);

                MessageBox.Show(this, "Save As failed (" + ex.Message + ")",
                                "Save As", MessageBoxButtons.OK, MessageBoxIcon.Information);
           }
        }
    }
}
