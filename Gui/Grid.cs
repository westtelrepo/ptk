﻿
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace WestTel.E911
{
    public class Grid : System.Windows.Forms.DataGridView
    {
        public Grid()
        {
            InitializeComponent();
            try {CreateHandle();} catch {}
        }

        public Grid(string name)
        {
            syncRoot = new object();
            InitializeComponent();

            Name = name;

            AutoGenerateColumns = false;

            timeTimer = new System.Threading.Timer(
                new System.Threading.TimerCallback(UpdateTime), null, 1000, System.Threading.Timeout.Infinite);

            try {CreateHandle();} catch {}

            DoubleBuffered = true;
        }

        public void setSyncRoot(object _syncRoot) {syncRoot = _syncRoot;}

        private DataGridViewCellStyle dataGridViewCellStyle1;
        private DataGridViewCellStyle dataGridViewCellStyle2;
        private DataGridViewCellStyle dataGridViewCellStyle3;

        private int lastRowRemoved = -1;

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();

            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Grid
            this.AllowUserToAddRows = false;
            this.AllowUserToDeleteRows = false;
            this.AllowUserToResizeColumns = false;
            this.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(255,247,209);
            this.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.BackgroundColor = System.Drawing.SystemColors.Window;
            this.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CausesValidation = false;
            this.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            this.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Ivory;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DefaultCellStyle = dataGridViewCellStyle3;
            this.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.GridColor = System.Drawing.Color.FromArgb(224,224,224);
            this.Location = new System.Drawing.Point(0, 0);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.MultiSelect = false;
            this.Name = "Grid";
            this.ReadOnly = true;
            this.RowHeadersVisible = false;
            this.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.RowTemplate.DividerHeight = 1;
            this.RowTemplate.Height = 37;
            this.RowTemplate.ReadOnly = true;
            this.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ShowCellErrors = false;
            this.ShowEditingIcon = false;
            this.ShowRowErrors = false;
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
        }
        #endregion

        private System.Threading.Timer timeTimer = null;

        private void UpdateTime(object obj)
        {
            try
            {
                BeginInvoke(new updateGridDelegate(updateGrid));
            }
            catch
            {
                timeTimer.Change(200, System.Threading.Timeout.Infinite);
            }
        }

        private delegate void updateGridDelegate();

        private void updateGrid()
        {
DataModel.logLockInfo(true, syncRoot);
            lock (syncRoot)
            {
                try
                {
                    if (Handle != null)
                    {
                        bool rowIsSelected = (CurrentRow != null && CurrentRow.Index >= 0);

                        if (RowCount != 0 && selectedRowKey != null && rowIsSelected)
                        {
                            if ( ! isEqual(getRowKey(CurrentRow), selectedRowKey))
                            {
                                foreach (DataGridViewRow GRow in Rows)
                                {
                                    if (isEqual(getRowKey(GRow), selectedRowKey))
                                    {
                                        CurrentCell   = GRow.Cells[0];
System.Diagnostics.Trace.WriteLine(Name + " - attempting to select " + selectedRowKey.ToString() + " Grid:130", "Info");
                                        rowIsSelected = true;
                                        break;
                                    }
                                }
                            }
                        }

                        // if no row was selected, and no row is selected, select the next best row
                        if ( ! rowIsSelected && Rows.Count != 0)
                        {
                            if (lastRowRemoved != -1)
                            {
                                try   {BeginInvoke(new handleRowsRemovedDelegate(handleRowsRemoved));}
                                catch {handleRowsRemoved();}
                            }

                            if (CurrentRow == null || CurrentRow.Index < 0)
                            {
                                DataGridViewRow GRow = Rows[Rows.GetFirstRow(DataGridViewElementStates.Visible)];

                                if (GRow != null && GRow.DataBoundItem != null)
                                {
                                    selectedRowKey = getRowKey(GRow);
System.Diagnostics.Trace.WriteLine(Name + " - selected key changed " + selectedRowKey.ToString() + " Grid:156", "Info");
                                    CurrentCell    = GRow.Cells[0];
System.Diagnostics.Trace.WriteLine(Name + " - attempting to select " + selectedRowKey.ToString() + " Grid:158", "Info");
                                }
                            }
                        }

                        RefreshRowSelection(null);
                        GridSelectionChanged(null);
                    }
                }
                catch {}
            }
DataModel.logLockInfo(false, syncRoot);

            timeTimer.Change(200, System.Threading.Timeout.Infinite);
        }

        public virtual void RefreshRowSelection(string context)
        {
            ClearSelection();

            if (CurrentRow != null)
            {
                CurrentRow.Selected = true;
if (context != null) System.Diagnostics.Trace.WriteLine(Name + " - row selected " + getRowKey(CurrentRow).ToString() + " " + context, "Info");
            }

            Refresh();
        }

        override protected void OnRowPrePaint(DataGridViewRowPrePaintEventArgs e)
        {
DataModel.logLockInfo(true, syncRoot);
            lock (syncRoot)
            {
                try
                {
                    // don't paint focus
                    e.PaintParts = DataGridViewPaintParts.All & ~DataGridViewPaintParts.Focus;

                    base.OnRowPrePaint(e);
                }
                catch (Exception ex)
                {
                    Logging.AppTrace("Error in Grid OnRowPrePaint():" + ex.Message);    
                }
            }
DataModel.logLockInfo(false, syncRoot);
        }

        override protected void OnPaint(PaintEventArgs e)
        {
DataModel.logLockInfo(true, syncRoot);
            lock (syncRoot)
            {
                try
                {
                    base.OnPaint(e);
                }
                catch (Exception ex)
                {
                    Logging.AppTrace("Error in Grid OnPaint():" + ex.Message);    
                }
            }
DataModel.logLockInfo(false, syncRoot);
        }

        private int hoveredRowIndex = -1;
        public bool isHovered(int rowIndex) {return (rowIndex == hoveredRowIndex);}

        protected override void OnCellMouseEnter(DataGridViewCellEventArgs e)
        {
            try
            {
                base.OnCellMouseEnter(e);

                hoveredRowIndex = e.RowIndex;

                if (e.RowIndex >= 0)
                {
                    InvalidateRow(e.RowIndex);
                    Update();
                }
            }
            catch {hoveredRowIndex = -1;}
        }

        protected override void OnCellMouseLeave(DataGridViewCellEventArgs e)
        {
            try
            {
                base.OnCellMouseLeave(e);

                if (hoveredRowIndex == e.RowIndex) hoveredRowIndex = -1;

                if (e.RowIndex >= 0)
                {
                    InvalidateRow(e.RowIndex);
                    Update();
                }
            }
            catch {hoveredRowIndex = -1;}
        }

        public void paintRowBackground(
            DataGridViewRowPrePaintEventArgs e,
            Settings.ColorInfo               colorInfo,
            Settings.ColorInfo               selectedColorInfo,
            bool                             isSelected
        )
        {
            e.Graphics.FillRectangle(
                colorInfo.getBackgroundBrush(e.RowBounds, LinearGradientMode.Horizontal), e.RowBounds);

            if (isSelected || isHovered(e.RowIndex))
            {
                if (isSelected)
                {
                    // selection arrows
                    PointF[] points = new PointF[3];

                    // left arrow
                    points[0] = new PointF(12, e.RowBounds.Y + e.RowBounds.Height / 2.0f);
                    points[1] = new PointF(points[0].X - 6, points[0].Y - 4);
                    points[2] = new PointF(points[0].X - 6, points[0].Y + 4);

                    e.Graphics.FillPolygon(colorInfo.getForegroundBrush(),                             points);
                    e.Graphics.DrawPolygon(new Pen(Color.FromArgb(160, colorInfo.foregroundColor), 1), points);

                    // TODO gui: make the right arrow configurable?
                    //points[0] = new PointF(e.RowBounds.Right - 12, e.RowBounds.Y + e.RowBounds.Height / 2.0f);
                    //points[1] = new PointF(points[0].X + 6, points[0].Y - 4);
                    //points[2] = new PointF(points[0].X + 6, points[0].Y + 4);

                    //e.Graphics.FillPolygon(colorInfo.getForegroundBrush(),                             points);
                    //e.Graphics.DrawPolygon(new Pen(Color.FromArgb(160, colorInfo.foregroundColor), 1), points);
                }

                // selection lines (at the top and bottom)
                Pen linePen = new Pen(Color.FromArgb(255, colorInfo.foregroundColor), 1);

                e.Graphics.DrawLine(linePen, new Point(e.RowBounds.Left,  e.RowBounds.Top),
                                             new Point(e.RowBounds.Right, e.RowBounds.Top));

                e.Graphics.DrawLine(linePen, new Point(e.RowBounds.Left,  e.RowBounds.Bottom - 1),
                                             new Point(e.RowBounds.Right, e.RowBounds.Bottom - 1));
            }
            else
            {
                // separator line (at the top)
                Pen linePen = new Pen(Color.FromArgb(30, colorInfo.foregroundColor), 1);

                e.Graphics.DrawLine(linePen, new Point(e.RowBounds.Left,  e.RowBounds.Top),
                                             new Point(e.RowBounds.Right, e.RowBounds.Top));

                paintSelectionOverlay(
                    e.Graphics, e.RowIndex, e.RowBounds, selectedColorInfo.shadowColor);
            }
        }

        public void paintSelectionOverlay(
            Graphics graphics, int rowIndex, Rectangle rowBounds, Color shadowColor)
        {
            // if row or cell is invalid, return
            if (rowIndex < 0) return;

            int iRow = rowIndex;

            // selection drop shadow
            bool prevIsSelected = (iRow == 0)              ? false : Rows[iRow - 1].Selected;
            bool nextIsSelected = (iRow == Rows.Count - 1) ? false : Rows[iRow + 1].Selected;

            if (prevIsSelected || nextIsSelected)
            {
                Rectangle shadowRect = rowBounds;

                // if the previous row is selected, draw a drop shadow under the selected row
                if (prevIsSelected)
                {
                    shadowRect.Height = 6;

                    // draw the shadow
                    graphics.FillRectangle(new LinearGradientBrush(
                        shadowRect, shadowColor, Color.FromArgb(0, shadowColor),
                        LinearGradientMode.Vertical), shadowRect);
                }
                // else if the next row is selected, draw a drop shadow above the selected row
                else if (nextIsSelected)
                {
                    shadowRect.Y      = shadowRect.Bottom - 6;
                    shadowRect.Height = 6;

                    // draw the shadow
                    graphics.FillRectangle(new LinearGradientBrush(
                        shadowRect, Color.FromArgb(0, shadowColor),
                        Color.FromArgb(shadowColor.A / 3, shadowColor),
                        LinearGradientMode.Vertical), shadowRect);
                }
            }
        }

        override protected void OnScroll(ScrollEventArgs e)
        {
DataModel.logLockInfo(true, syncRoot);
            lock (syncRoot)
            {
                try
                {
                    base.OnScroll(e);
                }
                catch (Exception ex)
                {
                    Logging.AppTrace("Error in Grid OnScroll():" + ex.Message);    
                }
            }
DataModel.logLockInfo(false, syncRoot);
        }

        override protected void OnNotifyMessage(Message m)
        {
DataModel.logLockInfo(true, syncRoot);
            lock (syncRoot)
            {
                try
                {
                    base.OnNotifyMessage(m);
                }
                catch (Exception ex)
                {
                    Logging.AppTrace("Error in Grid OnNotifyMessage():" + ex.Message);    
                }
            }
DataModel.logLockInfo(false, syncRoot);
        }

        override protected void OnBindingContextChanged(EventArgs e)
        {
DataModel.logLockInfo(true, syncRoot);
            lock (syncRoot)
            {
                try
                {
                    base.OnBindingContextChanged(e);
                }
                catch (Exception ex)
                {
                    Logging.AppTrace("Error in Grid OnBindingContextChanged():" + ex.Message);    
                    Logging.ExceptionLogger("Error in Console.GridEx.OnBindingContextChanged", ex, Console.LogID.Gui);
                }
            }
DataModel.logLockInfo(false, syncRoot);
        }

        virtual public void GridSelectionChanged(System.Data.DataRowView row) {}

        protected bool selectedByUser = false;

        protected object selectedRowKey = null;

        private System.Collections.Generic.List<DataGridViewRow> rowList = new System.Collections.Generic.List<DataGridViewRow>();

        override protected void OnRowsAdded(DataGridViewRowsAddedEventArgs e)
        {
DataModel.logLockInfo(true, syncRoot);
            lock (syncRoot)
            {
                try
                {
                    // do not call base.OnRowsAdded(e) here (causes flashing)

                    DataGridViewRow addedRow = Rows[e.RowIndex];

                    if ( ! rowList.Contains(addedRow) && isRowInTable(addedRow))
                    {
                        rowList.Add(addedRow); // TODO calls: rowList only grows, rows are never removed
                        newRowAdded(addedRow);
                        UpdateTime(null);
                    }
                }
                catch (Exception ex)
                {
                    Logging.AppTrace("Error in Grid OnRowsAdded():" + ex.Message);    
                    Logging.ExceptionLogger("Error in Grid.OnRowsAdded", ex, Console.LogID.Gui);
                }
            }
DataModel.logLockInfo(false, syncRoot);
        }

        override protected void OnRowsRemoved(DataGridViewRowsRemovedEventArgs e)
        {
DataModel.logLockInfo(true, syncRoot);
            lock (syncRoot)
            {
                try
                {
                    base.OnRowsRemoved(e);

                    lastRowRemoved = e.RowIndex;

                    try   {BeginInvoke(new handleRowsRemovedDelegate(handleRowsRemoved));}
                    catch {handleRowsRemoved();}
                }
                catch (Exception ex)
                {
                    Logging.AppTrace("Error in Grid OnRowsRemoved():" + ex.Message);    
                    Logging.ExceptionLogger("Error in Grid.OnRowsRemoved", ex, Console.LogID.Gui);
                }
            }
DataModel.logLockInfo(false, syncRoot);
        }

        private delegate void handleRowsRemovedDelegate();

        private void handleRowsRemoved()
        {
DataModel.logLockInfo(true, syncRoot);
            lock (syncRoot)
            {
                try
                {
                    int indexToSelect = -1;

                    // if one row, select it
                    if (Rows.Count == 1)
                    {
                        indexToSelect = 0;
                    }
                    // else if more than one, leave the selection alone, or if none selected, select the next row
                    else if (Rows.Count != 0)
                    {
                        if (CurrentCell == null || ! isEqual(getRowKey(Rows[CurrentCell.RowIndex]), selectedRowKey))
                        {
                            if (Rows.Count > lastRowRemoved) indexToSelect = lastRowRemoved;
                            else                             indexToSelect = Rows.Count - 1;
                        }
                    }

                    if (indexToSelect != -1)
                    {
                        DataGridViewRow GRow = Rows[indexToSelect];

                        if (GRow != null && GRow.DataBoundItem != null)
                        {
                            System.Data.DataRowView row = (System.Data.DataRowView)GRow.DataBoundItem;

                            selectedRowKey = getRowKey(GRow);
System.Diagnostics.Trace.WriteLine(Name + " - selected key changed " + selectedRowKey.ToString() + " Grid:373", "Info");
                            CurrentCell    = GRow.Cells[0];
System.Diagnostics.Trace.WriteLine(Name + " - attempting to select " + selectedRowKey.ToString() + " Grid:375", "Info");

                            GridSelectionChanged(row);

                            lastRowRemoved = -1;
                        }
                    }

                    RefreshRowSelection("Grid:383");
                }
                catch (Exception ex)
                {
                    Logging.AppTrace("Error in Grid handleRowsRemoved():" + ex.Message);    
                    Logging.ExceptionLogger("Error in Grid.handleRowsRemoved", ex, Console.LogID.Gui);
                }
            }
DataModel.logLockInfo(false, syncRoot);
        }

        virtual protected bool   isEqual      (object key1, object key2) {return Convert.ToDateTime(key1) == Convert.ToDateTime(key2);}
        virtual public    object getRowKey    (DataGridViewRow row) {if (row == null) return null; return row.Cells[0].Value;}
        virtual protected void   newRowAdded  (DataGridViewRow row) {selectedRowKey = getRowKey(row);System.Diagnostics.Trace.WriteLine(Name + " - selected key changed " + selectedRowKey.ToString() + " Grid:396", "Info");}
        virtual protected bool   isRowInTable (DataGridViewRow row) {return true;}

        override protected void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                selectedByUser = true;
                base.OnMouseDown(e);

                // allow users to update the Gui by clicking on the currently selected row
                GridSelectionChanged(null);
            }
        }

        override protected void OnKeyDown(KeyEventArgs e)
        {
            switch (e.KeyData)
            {
                case Keys.Left:
                case Keys.Right:
				    e.Handled = true;
                    handleLeftRightKey(e.KeyCode);
                    break;
                case Keys.Up:
                case Keys.Down:
                case Keys.PageDown:
                case Keys.PageUp:
                case Keys.Home:
                case Keys.End:
                    selectedByUser = true;
                    base.OnKeyDown(e);
                    break;
            }
        }

        virtual protected void handleLeftRightKey(Keys keyCode) {}

        override protected void OnSelectionChanged(EventArgs e)
        {
DataModel.logLockInfo(true, syncRoot);
            lock (syncRoot)
            {
                base.OnSelectionChanged(e);

                try
                {
                    if (selectedByUser)
                    {
                        selectedByUser = false;

                        if (CurrentRow != null && CurrentRow.Index >= 0) {selectedRowKey = getRowKey(CurrentRow);System.Diagnostics.Trace.WriteLine(Name + " - selected key changed " + selectedRowKey.ToString() + " Grid:446", "Info");}
                        else                                             {System.Diagnostics.Trace.WriteLine(Name + " - selected key unset   " + selectedRowKey.ToString() + " Grid:447", "Info");selectedRowKey = null;}

                        if (CurrentRow != null && CurrentRow.DataBoundItem != null)
                        {
                            CurrentRow.Selected = true;
System.Diagnostics.Trace.WriteLine(Name + " - row selected " + getRowKey(CurrentRow).ToString() + " Grid:452", "Info");
                            GridSelectionChanged((System.Data.DataRowView)CurrentRow.DataBoundItem);
                        }
                        else
                        {
                            GridSelectionChanged(null);
                        }
                    }
                }
                catch {} // TODO* gui: why do we get exceptions here sometimes?
            }
DataModel.logLockInfo(false, syncRoot);
        }

        override protected void OnDataError(bool displayErrorDialogIfNoHandler, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                base.OnDataError(false, e);
            }
            catch (Exception ex)
            {
                Logging.AppTrace("Error in Grid OnDataError():" + ex.Message);    
                Logging.ExceptionLogger("Error in Console.GridEx.OnDataError", ex, Console.LogID.Gui);
            }
        }

        private object syncRoot;
    }
}
