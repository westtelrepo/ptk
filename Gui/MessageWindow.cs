using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace WestTel.E911
{
	/// <summary>
	/// Summary description for MainForm.
	/// </summary>
	public class MessageWindow : BaseForm
	{
		private System.Threading.Timer checkSendButtonTimer = null;

		public CannedMessages messages;
		Controller controller = null;
		Brush printerBrush;
		Font printerFont;
		int fontSize = 10;
		float LinePosition = 10;
		float LeftMargin = 30;
		float TopMargin = 50;
		bool systemShutdown = false;
		
        public enum WindowType {Undefined = -1, TDD, SMS};
        private WindowType windowType = WindowType.Undefined;

        public bool SendButtonEnabled = true;

		#region Windows Control Declaraitons

        private ToolTip toolTip;
        private ToolTip toolTipInvalidChar;
        private IContainer components;

        public class GradientPanel : Panel
        {
            public GradientPanel()
            {
                SetStyle(ControlStyles.UserPaint, true);
                setWindowType(windowType);

                AutoScroll = true;
                SetAutoScrollMargin(0, 0);

                HScroll = false;
                VScroll = true;
            }

            public void setWindowType(WindowType type) {windowType = type;}

            protected override void OnPaintBackground(PaintEventArgs e)
            {
                base.OnPaintBackground(e);

                Brush brush = backgroundColorInfo[(int)windowType].
                    getBackgroundBrush(ClientRectangle, LinearGradientMode.Vertical);

                e.Graphics.FillRectangle(brush, ClientRectangle);
            }

            private WindowType windowType = WindowType.TDD;
        }

	#endregion

        //private System.Threading.Thread WaitThread;

		public bool KeepChecking = false;

		private System.Drawing.Printing.PrintDocument printDoc;
		private System.Windows.Forms.PrintDialog printSetup;
        private System.Windows.Forms.ContextMenu cmBlank;

		private bool SendingText = false;
        private Panel tddSep2;
        private TableLayoutPanel tableLayoutPanel1;
        private MenuStrip MainMenu;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem printMenu;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem closeMenu;
        private ToolStripMenuItem windowMenu;
        private ToolStripMenuItem messagesMenu;
        private ToolStripSeparator toolStripSeparator6;
        private ToolStripMenuItem muteMenu;
        private ToolStripSeparator toolStripSeparator5;
        private ToolStripMenuItem sendMenu;
        private ToolStripMenuItem helpToolStripMenuItem;
        private ToolStripMenuItem aboutMenu;
        private ToolStripMenuItem usersManualMenu;
        private ToolStripSeparator helpMenuSep;
        private ToolStrip toolBar;
        private ToolStripButton printButton;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripButton messagesButton;
        private ToolStripSeparator muteSensitivitySeparator;
        private ToolStripButton muteButton;
        private ToolStripButton unmuteButton;
        private Button sendButton;
        private RichTextBox RealTimeTextBox;
        private RichTextBox TextToSend;
        private Panel tddSep1;
        private GradientPanel Conversation;
        private StatusStrip statusStrip;

        private bool needToSendTDDModeOn = false;




// TODO TDD/SMS: Change TDD applet to "Text" applet, remove SMS applet ("Text" will be for TDD/SMS/RTT/etc).
//               Also make it slighly wider so the word "Text" in the window title does not get cut off at minimum width.
//               Use old SMS icon for new Text applet




        public static Settings.ColorInfo[] dispatcherColorInfo =
        {
            // TDD
            new Settings.ColorInfo(Color.FromArgb(unchecked((int)0xFF2196F3)), // Google Blue 500
                                   Color.FromArgb(unchecked((int)0xFF2196F3)), // Google Blue 500
                                   Color.Black, false),
            // SMS
            new Settings.ColorInfo(Color.FromArgb(unchecked((int)0xFF388E3C)), // Google Green 700
                                   Color.FromArgb(unchecked((int)0xFF388E3C)), // Google Green 700
                                   Color.Black, false)
        };

        public static Settings.ColorInfo[] callerColorInfo =
        {
            // TDD
            new Settings.ColorInfo(Color.FromArgb(unchecked((int)0xFFE0E0E0)), // Google Grey 300
                                   Color.FromArgb(unchecked((int)0xFFE0E0E0)), // Google Grey 300
                                   Color.Black, false),
            // SMS
            new Settings.ColorInfo(Color.FromArgb(unchecked((int)0xFFE0E0E0)), // Google Grey 300
                                   Color.FromArgb(unchecked((int)0xFFE0E0E0)), // Google Grey 300
                                   Color.Black, false),
        };

        public static Settings.ColorInfo[] backgroundColorInfo =
        {
            // TDD
            new Settings.ColorInfo(Color.White, Color.White, Color.Black, false),

            // SMS
            new Settings.ColorInfo(Color.White, Color.White, Color.Black, false),
        };

        public MessageWindow(Controller c, WindowType type)
		{
			controller = c;
            windowType = type;

			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
            setTranslatableText();

            this.     BackColor = SystemColors.Window;
            MainMenu. BackColor = SystemColors.Window;
            toolBar.  BackColor = SystemColors.Window;

            colorsUpdated();

            statusStrip.Visible = false;

            Conversation.setWindowType(windowType);

            if (windowType == WindowType.TDD)
            {
                Icon = Properties.Resources.TDDIcon;

                messagesButton. Image = Properties.Resources.TDDMessagesIcon.ToBitmap();
                messagesMenu.   Image = Properties.Resources.TDDMessagesIcon.ToBitmap();
            }
            else if (windowType == WindowType.SMS)
            {
                Icon = Properties.Resources.SMSIcon;

                messagesButton. Image = Properties.Resources.SMSMessagesIcon.ToBitmap();
                messagesMenu.   Image = Properties.Resources.SMSMessagesIcon.ToBitmap();
            }

            setEnabled(false);

            Name = windowType.ToString();

            windowMenu.Text = "&" + (Translate)(windowType.ToString());

            // set RealTimeTextBox and TDD separators visible only in TDD mode
            tddSep1.Visible = RealTimeTextBox.Visible = tddSep2.Visible = (windowType == WindowType.TDD);

			checkSendButtonTimer = new System.Threading.Timer(
                SendButtonCheck, null,System.Threading.Timeout.Infinite,System.Threading.Timeout.Infinite);

            if (dispatcherSaysRegExp.Length == 0) setDispatcherSaysPattern ("{time} (Postion {position-number})");
            if (callerSaysRegExp.Length     == 0) setCallerSaysPattern     ("{time} (Caller)");
		}

        private const string openBracketRegEx  = " *\\[?\\(? *";
        private const string closeBracketRegEx = " *\\)?\\]? *";

        // allow optional brackets or parens
        private const string positionRegExp = openBracketRegEx + "P?[0-9]*" + closeBracketRegEx;

        // allow optional spaces and brackets or parens (also optional am/pm)
        private const string timeRegExp = openBracketRegEx + "[0-9]+\\:[0-9]+(\\:[0-9]+)? *(a|p|A|P|am|pm|AM|PM)?" + closeBracketRegEx;

        private string callerSaysRegExp     = "";
        private string dispatcherSaysRegExp = "";

        public void setCallerSaysPattern(string pattern)
        {
            // escape some typical regular expression characters that might show up
            pattern = pattern.Replace(":", "\\:");
            pattern = pattern.Replace("-", "\\-");
            pattern = pattern.Replace("(", "\\(");
            pattern = pattern.Replace(")", "\\)");
            pattern = pattern.Replace("[", "\\[");
            pattern = pattern.Replace("]", "\\]");
            pattern = pattern.Replace("{", "\\{");
            pattern = pattern.Replace("}", "\\}");
            pattern = pattern.Replace(".", "\\.");
            pattern = pattern.Replace(",", "\\,");
            pattern = pattern.Replace("*", "\\*");
            pattern = pattern.Replace("+", "\\+");

            // if there is a space before the time, remove it for the regexp
            pattern = pattern.Replace(" \\{time\\}", "\\{time\\}");
            pattern = pattern.Replace("\\{time\\} ", "\\{time\\}");
            pattern = pattern.Replace("\\{time\\}",  timeRegExp);

            callerSaysRegExp = pattern;
        }

        public void setDispatcherSaysPattern(string pattern)
        {
            // escape some typical regular expression characters that might show up
            pattern = pattern.Replace(":", "\\:");
            pattern = pattern.Replace("-", "\\-");
            pattern = pattern.Replace("(", "\\(");
            pattern = pattern.Replace(")", "\\)");
            pattern = pattern.Replace("[", "\\[");
            pattern = pattern.Replace("]", "\\]");
            pattern = pattern.Replace("{", "\\{");
            pattern = pattern.Replace("}", "\\}");
            pattern = pattern.Replace(".", "\\.");
            pattern = pattern.Replace(",", "\\,");
            pattern = pattern.Replace("*", "\\*");
            pattern = pattern.Replace("+", "\\+");

            // if there is a space before or after, remove it for the regexp
            pattern = pattern.Replace(" \\{time\\}", "\\{time\\}");
            pattern = pattern.Replace("\\{time\\} ", "\\{time\\}");
            pattern = pattern.Replace("\\{time\\}",  timeRegExp);

            pattern = pattern.Replace(" \\{position\\-number\\}",  "\\{position\\-number\\}");
            pattern = pattern.Replace("\\{position\\-number\\} ", "\\{position\\-number\\}");
            pattern = pattern.Replace("\\{position\\-number\\}", positionRegExp);

            dispatcherSaysRegExp = pattern;
        }

        void messages_MessageSelected(string Message)
        {
            TextToSend.Text = Message;
        }

        void messages_MessagesClosing()
        {
            messagesButton.Checked = messagesMenu.Checked = false;
        }

        public bool Stop()
		{
			bool OkToStop = true;
			if (messages != null) messages.Close();
			try
			{
				systemShutdown = true;
				Logging.AppTrace("Stopping " + windowType.ToString() + " Window");
				
				KeepChecking = false;
				checkSendButtonTimer.Dispose();
				Close();
			}
			catch (Exception ex)
			{
				Logging.AppTrace("Error Stopping " + windowType.ToString() + " Window:" + ex.ToString());
				Logging.ExceptionLogger("Error in " + windowType.ToString() + "MessageWindow.Stop", ex, Console.LogID.Cleanup);
			}
			return OkToStop;
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageWindow));
            this.cmBlank = new System.Windows.Forms.ContextMenu();
            this.printDoc = new System.Drawing.Printing.PrintDocument();
            this.printSetup = new System.Windows.Forms.PrintDialog();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.toolTipInvalidChar = new System.Windows.Forms.ToolTip(this.components);
            this.tddSep2 = new System.Windows.Forms.Panel();
            this.tddSep1 = new System.Windows.Forms.Panel();
            this.TextToSend = new System.Windows.Forms.RichTextBox();
            this.RealTimeTextBox = new System.Windows.Forms.RichTextBox();
            this.sendButton = new System.Windows.Forms.Button();
            this.toolBar = new System.Windows.Forms.ToolStrip();
            this.printButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.messagesButton = new System.Windows.Forms.ToolStripButton();
            this.muteSensitivitySeparator = new System.Windows.Forms.ToolStripSeparator();
            this.muteButton = new System.Windows.Forms.ToolStripButton();
            this.unmuteButton = new System.Windows.Forms.ToolStripButton();
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.closeMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.windowMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.messagesMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.muteMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.sendMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usersManualMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuSep = new System.Windows.Forms.ToolStripSeparator();
            this.aboutMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Conversation = new WestTel.E911.MessageWindow.GradientPanel();
            this.toolBar.SuspendLayout();
            this.MainMenu.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // printDoc
            // 
            this.printDoc.DocumentName = "Conversation";
            this.printDoc.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDoc_PrintPage);
            // 
            // printSetup
            // 
            this.printSetup.Document = this.printDoc;
            // 
            // toolTip
            // 
            this.toolTip.ShowAlways = true;
            this.toolTip.StripAmpersands = true;
            // 
            // toolTipInvalidChar
            // 
            this.toolTipInvalidChar.ShowAlways = true;
            this.toolTipInvalidChar.StripAmpersands = true;
            // 
            // tddSep2
            // 
            this.tddSep2.BackColor = System.Drawing.SystemColors.Highlight;
            this.tableLayoutPanel1.SetColumnSpan(this.tddSep2, 2);
            this.tddSep2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tddSep2.Location = new System.Drawing.Point(0, 286);
            this.tddSep2.Margin = new System.Windows.Forms.Padding(0);
            this.tddSep2.Name = "tddSep2";
            this.tddSep2.Size = new System.Drawing.Size(416, 10);
            this.tddSep2.TabIndex = 32;
            this.tddSep2.Visible = false;
            // 
            // tddSep1
            // 
            this.tddSep1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(223)))), ((int)(((byte)(247)))));
            this.tableLayoutPanel1.SetColumnSpan(this.tddSep1, 2);
            this.tddSep1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tddSep1.Location = new System.Drawing.Point(0, 222);
            this.tddSep1.Margin = new System.Windows.Forms.Padding(0);
            this.tddSep1.Name = "tddSep1";
            this.tddSep1.Size = new System.Drawing.Size(416, 10);
            this.tddSep1.TabIndex = 31;
            this.tddSep1.Visible = false;
            // 
            // TextToSend
            // 
            this.TextToSend.BackColor = System.Drawing.SystemColors.Window;
            this.TextToSend.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextToSend.ContextMenu = this.cmBlank;
            this.TextToSend.DetectUrls = false;
            this.TextToSend.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TextToSend.Location = new System.Drawing.Point(4, 300);
            this.TextToSend.Margin = new System.Windows.Forms.Padding(4);
            this.TextToSend.Name = "TextToSend";
            this.TextToSend.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.TextToSend.Size = new System.Drawing.Size(326, 68);
            this.TextToSend.TabIndex = 30;
            this.TextToSend.TabStop = false;
            this.TextToSend.Text = "";
            this.TextToSend.TextChanged += new System.EventHandler(this.TextToSend_TextChanged);
            this.TextToSend.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextToSend_KeyDown);
            this.TextToSend.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextToSend_KeyPress);
            // 
            // RealTimeTextBox
            // 
            this.RealTimeTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.RealTimeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tableLayoutPanel1.SetColumnSpan(this.RealTimeTextBox, 2);
            this.RealTimeTextBox.ContextMenu = this.cmBlank;
            this.RealTimeTextBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.RealTimeTextBox.DetectUrls = false;
            this.RealTimeTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RealTimeTextBox.Location = new System.Drawing.Point(4, 236);
            this.RealTimeTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.RealTimeTextBox.Name = "RealTimeTextBox";
            this.RealTimeTextBox.ReadOnly = true;
            this.RealTimeTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.RealTimeTextBox.Size = new System.Drawing.Size(408, 46);
            this.RealTimeTextBox.TabIndex = 28;
            this.RealTimeTextBox.TabStop = false;
            this.RealTimeTextBox.Text = "";
            this.RealTimeTextBox.Visible = false;
            this.RealTimeTextBox.TextChanged += new System.EventHandler(this.RealTime_TextChanged);
            this.RealTimeTextBox.Enter += new System.EventHandler(this.updateFocus);
            // 
            // sendButton
            // 
            this.sendButton.BackColor = System.Drawing.SystemColors.Window;
            this.sendButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sendButton.Enabled = false;
            this.sendButton.FlatAppearance.BorderSize = 0;
            this.sendButton.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendButton.Location = new System.Drawing.Point(340, 302);
            this.sendButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 4);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(70, 66);
            this.sendButton.TabIndex = 1;
            this.sendButton.Text = "&Send";
            this.sendButton.UseVisualStyleBackColor = false;
            this.sendButton.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // toolBar
            // 
            this.toolBar.AllowMerge = false;
            this.tableLayoutPanel1.SetColumnSpan(this.toolBar, 2);
            this.toolBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolBar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolBar.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printButton,
            this.toolStripSeparator3,
            this.messagesButton,
            this.muteSensitivitySeparator,
            this.muteButton,
            this.unmuteButton});
            this.toolBar.Location = new System.Drawing.Point(0, 24);
            this.toolBar.Name = "toolBar";
            this.toolBar.Size = new System.Drawing.Size(416, 45);
            this.toolBar.Stretch = true;
            this.toolBar.TabIndex = 2;
            this.toolBar.Text = "Tool Bar";
            // 
            // printButton
            // 
            this.printButton.Enabled = false;
            this.printButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.printButton.Image = Properties.Resources.PrintIcon.ToBitmap();
            this.printButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.printButton.Name = "printButton";
            this.printButton.Padding = new System.Windows.Forms.Padding(2);
            this.printButton.Size = new System.Drawing.Size(36, 42);
            this.printButton.Text = "&Print";
            this.printButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.printButton.ToolTipText = "Print (Ctrl+P)";
            this.printButton.Click += new System.EventHandler(this.printMenu_ItemClick);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 45);
            // 
            // messagesButton
            // 
            this.messagesButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.messagesButton.Image = Properties.Resources.TDDMessagesIcon.ToBitmap();
            this.messagesButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.messagesButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.messagesButton.Name = "messagesButton";
            this.messagesButton.Padding = new System.Windows.Forms.Padding(2);
            this.messagesButton.Size = new System.Drawing.Size(62, 42);
            this.messagesButton.Text = "&Messages";
            this.messagesButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.messagesButton.ToolTipText = "Preprogrammed Messages";
            this.messagesButton.Click += new System.EventHandler(this.messageButton_ItemClick);
            // 
            // muteSensitivitySeparator
            // 
            this.muteSensitivitySeparator.Name = "muteSensitivitySeparator";
            this.muteSensitivitySeparator.Size = new System.Drawing.Size(6, 45);
            this.muteSensitivitySeparator.Visible = false;
            // 
            // muteButton
            // 
            this.muteButton.CheckOnClick = true;
            this.muteButton.Font = new System.Drawing.Font("Tahoma", 9F);
            this.muteButton.Image = Properties.Resources.ConsoleCall_MuteIcon.ToBitmap();
            this.muteButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.muteButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.muteButton.Name = "muteButton";
            this.muteButton.Padding = new System.Windows.Forms.Padding(2);
            this.muteButton.Size = new System.Drawing.Size(39, 42);
            this.muteButton.Text = "M&ute";
            this.muteButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.muteButton.Visible = false;
            this.muteButton.CheckStateChanged += new System.EventHandler(this.muteButton_CheckStateChanged);
            // 
            // unmuteButton
            // 
            this.unmuteButton.CheckOnClick = true;
            this.unmuteButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.unmuteButton.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.unmuteButton.Image = Properties.Resources.ConsoleCallMuteOffIcon.ToBitmap();
            this.unmuteButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.unmuteButton.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.unmuteButton.Name = "unmuteButton";
            this.unmuteButton.Padding = new System.Windows.Forms.Padding(2);
            this.unmuteButton.Size = new System.Drawing.Size(28, 42);
            this.unmuteButton.Text = "&Unmute";
            this.unmuteButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.unmuteButton.Visible = false;
            // 
            // MainMenu
            // 
            this.MainMenu.AllowMerge = false;
            this.tableLayoutPanel1.SetColumnSpan(this.MainMenu, 2);
            this.MainMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.windowMenu,
            this.helpToolStripMenuItem});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(416, 24);
            this.MainMenu.TabIndex = 1;
            this.MainMenu.Text = "Main Menu";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printMenu,
            this.toolStripSeparator1,
            this.closeMenu});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // printMenu
            // 
            this.printMenu.Enabled = false;
            this.printMenu.Image = new Icon(Properties.Resources.PrintIcon, 16, 16).ToBitmap();
            this.printMenu.Name = "printMenu";
            this.printMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.printMenu.Size = new System.Drawing.Size(149, 22);
            this.printMenu.Text = "&Print...";
            this.printMenu.Click += new System.EventHandler(this.printMenu_ItemClick);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(146, 6);
            // 
            // closeMenu
            // 
            this.closeMenu.Name = "closeMenu";
            this.closeMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.closeMenu.Size = new System.Drawing.Size(149, 22);
            this.closeMenu.Text = "&Close";
            this.closeMenu.Click += new System.EventHandler(this.menuClose_ItemClick);
            // 
            // windowMenu
            // 
            this.windowMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.messagesMenu,
            this.toolStripSeparator6,
            this.muteMenu,
            this.toolStripSeparator5,
            this.sendMenu});
            this.windowMenu.Name = "windowMenu";
            this.windowMenu.Size = new System.Drawing.Size(63, 20);
            this.windowMenu.Text = "&Window";
            // 
            // messagesMenu
            // 
            this.messagesMenu.Image = new Icon(Properties.Resources.TDDMessagesIcon, 16, 16).ToBitmap();
            this.messagesMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.messagesMenu.Name = "messagesMenu";
            this.messagesMenu.Size = new System.Drawing.Size(125, 22);
            this.messagesMenu.Text = "&Messages";
            this.messagesMenu.Click += new System.EventHandler(this.messageButton_ItemClick);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(122, 6);
            this.toolStripSeparator6.Visible = false;
            // 
            // muteMenu
            // 
            this.muteMenu.CheckOnClick = true;
            this.muteMenu.Image = new Icon(Properties.Resources.ConsoleCall_MuteIcon, 16, 16).ToBitmap();
            this.muteMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.muteMenu.Name = "muteMenu";
            this.muteMenu.Size = new System.Drawing.Size(125, 22);
            this.muteMenu.Text = "M&ute";
            this.muteMenu.Visible = false;
            this.muteMenu.CheckStateChanged += new System.EventHandler(this.muteButton_CheckStateChanged);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(122, 6);
            // 
            // sendMenu
            // 
            this.sendMenu.Enabled = false;
            this.sendMenu.Name = "sendMenu";
            this.sendMenu.Size = new System.Drawing.Size(125, 22);
            this.sendMenu.Text = "S&end";
            this.sendMenu.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usersManualMenu,
            this.helpMenuSep,
            this.aboutMenu});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // usersManualMenu
            // 
            this.usersManualMenu.Image = new Icon(Properties.Resources.UserManualIcon, 16, 16).ToBitmap();
            this.usersManualMenu.Name = "usersManualMenu";
            this.usersManualMenu.Size = new System.Drawing.Size(140, 22);
            this.usersManualMenu.Text = "&User Manual";
            this.usersManualMenu.Click += new System.EventHandler(this.usersManualMenu_Click);
            // 
            // helpMenuSep
            // 
            this.helpMenuSep.Name = "helpMenuSep";
            this.helpMenuSep.Size = new System.Drawing.Size(137, 6);
            // 
            // aboutMenu
            // 
            this.aboutMenu.Image = new Icon(Properties.Resources.WestTelIcon, 16, 16).ToBitmap();
            this.aboutMenu.Name = "aboutMenu";
            this.aboutMenu.Size = new System.Drawing.Size(140, 22);
            this.aboutMenu.Text = "&About XXX";
            this.aboutMenu.Click += new System.EventHandler(this.aboutMenu_ItemClick);
            // 
            // statusStrip
            // 
            this.statusStrip.BackColor = System.Drawing.SystemColors.Window;
            this.statusStrip.Location = new System.Drawing.Point(0, 372);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(416, 22);
            this.statusStrip.SizingGrip = false;
            this.statusStrip.TabIndex = 6;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.MainMenu, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.toolBar, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.sendButton, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.RealTimeTextBox, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.TextToSend, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.tddSep1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tddSep2, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.Conversation, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(416, 372);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // Conversation
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.Conversation, 2);
            this.Conversation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Conversation.Location = new System.Drawing.Point(0, 69);
            this.Conversation.Margin = new System.Windows.Forms.Padding(0);
            this.Conversation.Name = "Conversation";
            this.Conversation.Size = new System.Drawing.Size(280, 400);
            this.Conversation.TabIndex = 33;
            // 
            // MessageWindow
            // 
            this.ClientSize = new System.Drawing.Size(300, 520);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.statusStrip);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Icon = Properties.Resources.TDDIcon;
            this.KeyPreview = true;
            this.MainMenuStrip = this.MainMenu;
            this.Name = "MessageWindow";
            this.Activated += new System.EventHandler(this.Window_Activated);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.Window_Closing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Shown += MessageWindow_Shown;
            this.VisibleChanged += new System.EventHandler(this.Window_VisibleChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Window_KeyDown);
            this.Resize += MainForm_Resize;
            this.toolBar.ResumeLayout(false);
            this.toolBar.PerformLayout();
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
        #endregion

        private void setTranslatableText()
        {
            this.sendButton.Text            = (Translate)("&Send");
            this.toolBar.Text               = (Translate)("Tool Bar");
            this.printButton.Text           = (Translate)("&Print");
            this.printButton.ToolTipText    = (Translate)("Print") + " (Ctrl+P)";
            this.messagesButton.Text        = (Translate)("&Messages");
            this.messagesButton.ToolTipText = (Translate)("Preprogrammed Messages");
            this.muteButton.Text            = (Translate)("M&ute");
            this.unmuteButton.Text          = (Translate)("&Unmute");
            this.MainMenu.Text              = (Translate)("Main Menu");
            this.fileToolStripMenuItem.Text = (Translate)("&File");
            this.printMenu.Text             = (Translate)("&Print...");
            this.closeMenu.Text             = (Translate)("&Close");
            this.windowMenu.Text            = (Translate)("&Window");
            this.messagesMenu.Text          = (Translate)("&Messages");
            this.muteMenu.Text              = (Translate)("M&ute");
            this.sendMenu.Text              = (Translate)("S&end");
            this.helpToolStripMenuItem.Text = (Translate)("&Help");
            this.usersManualMenu.Text       = (Translate)("&User Manual");
            this.aboutMenu.Text             = (Translate)("&About XXX");
        }

        System.Threading.Timer colorsUpdatedTimer = null;

        public void colorsUpdated()
        {
            if ( ! Visible)
            {
                if (colorsUpdatedTimer == null) colorsUpdatedTimer = new System.Threading.Timer(colorsUpdated);
                colorsUpdatedTimer.Change(500, -1);
            }
            else
            {
                colorsUpdated(null);
            }
        }

        private delegate void ColorsUpdatedDelegate();

        private void colorsUpdated(object notused)
        {
            try   { BeginInvoke(new ColorsUpdatedDelegate(Refresh)); }
            catch { try {Refresh();} catch{} }
        }

		private void OnSendCheck() {SendButtonCheck(null);}

		private void MainForm_Load(object sender, EventArgs e)
		{
            try
            {
                // hardcode minimum and maximum size
                ClientSize  = new Size(300, 520);
                MinimumSize = SizeFromClientSize(ClientSize);
                MaximumSize = new Size(MinimumSize.Width, SystemInformation.VirtualScreen.Height);
                MaximumSize = SystemInformation.VirtualScreen.Size;

                Text = Application.ProductName + " - " + (Translate)(windowType.ToString());
			    aboutMenu.Text = aboutMenu.Text.Replace("XXX", Application.ProductName);

                // remove unnecessary separators
                removeUnnecessarySeparators(toolBar);
            }
            catch {}
		}

        private void MessageWindow_Shown(object sender, EventArgs e)
        {
            // scroll to the bottom on first show
            ScrollToConversationBottom();
        }

		private void MainForm_Resize(object sender, System.EventArgs e)
		{	
			// hide form on minimze
			if (WindowState == FormWindowState.Minimized) 
			{
				checkSendButtonTimer.Change(System.Threading.Timeout.Infinite,System.Threading.Timeout.Infinite);
			}
			else
            {
				checkSendButtonTimer.Change(500,500);
            }
		}

		private const int WM_QUERYENDSESSION = 0x11;

		protected override void WndProc(ref Message m)
		{
			if (m.Msg == WM_QUERYENDSESSION)
			{
				if (!controller.OkToStop) Controller.ExitApplication(controller);
			}
			base.WndProc (ref m);
		}

		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!systemShutdown)
			{
				if (messages != null) messages.Close();
				e.Cancel = true;
				Hide();
			}
		}

		public void StringReceived(string callId, string str)
        {
            // make sure that this callId has an Info object
            if ( ! infoMap.ContainsKey(callId)) infoMap.Add(callId, new Info(this, callId));

            Info info = infoMap[callId];

            if (windowType == WindowType.SMS)
            {
                // TODO TDD/SMS: indicate that the other party is typing somehow?
                Logging.AppTrace("ignoring SMS StringReceived(\"" + str + "\")");
            }
            else // WindowType.TDD
            {
                foreach (char ch in str)
                {
                    Logging.AppTrace("TDD Char = " + ch);

                    switch (ch)
                    {
                        case '\r': break; // skip newlines
                        case '\n': break; // skip newlines
                        case '\b':        // backspace (null is also backspace)
                        case '\0':        // TODO: there is no way that a null will get through to here, is that an issue?
                        {
                            if (info.receivedText.Length != 0)
                            {
                                info.receivedText = info.receivedText.Remove(info.receivedText.Length - 1);
                            }

                            break;
                        }
                        default:
                        {
				            info.receivedText += ch.ToString().ToUpper();
                            break;
                        }
			        }
                }
            }

            if (callId == currentCallId)
            {
                if (currentInfo == null)
                {
                    currentInfo = info;
                    currentInfo.restore();
                }

                RealTimeTextBox.Text = info.receivedText;
				RealTimeTextBox.ScrollToCaret();
            }
		}

		[DllImport("user32", CharSet=CharSet.Auto)] 
		private static extern IntPtr SendMessage(IntPtr hWnd, int msg, int wParam, int lParam); 

		private void PushSentToConversation()
		{
            if (TextToSend.TextLength == 0) return;

            // send what the dispatcher said to the controller
            // (the controller will then push it back to everyone)
            switch (windowType)
            {
			    case WindowType.TDD: controller.sendTDDStringToController(currentCallId, TextToSend.Text); break;
                case WindowType.SMS: controller.sendSMSStringToController(currentCallId, TextToSend.Text); break;
            }

            TextToSend.Clear();
            TextToSend.Focus();
		}

		private const int WMV_SCROLL = 277;
		private const int SB_BOTTOM  = 7;

		private void TextToSend_TextChanged(object sender, System.EventArgs e)
		{
            toolTipInvalidChar.Hide(TextToSend);

            // remember the cursor position
            int caret = TextToSend.SelectionStart;

            string validText   = "";
            string invalidText = "";

            // only allow valid characters
            foreach (char c in TextToSend.Text)
            {
                if (isValidChar(c)) validText   += c;
                else                invalidText += c;
            }

            if (invalidText.Length != 0)
            {
                // tell the user the text was corrected
                string msg = (invalidText.Length == 1) ? (Translate)("The character '")
                                                       : (Translate)("These characters '");

                msg += invalidText + (Translate)("' cannot be sent via ") + (Translate)(windowType.ToString());

                // if the text was changed via paste rather than a key press
                if (TextToSend.TextLength - TextToSendLengthAtKeyDown != 1)
                {
                    toolTipInvalidChar.ToolTipTitle = msg;

                    msg = (Translate)("\nSo this:\n    ")               + TextToSend.Text +
                          (Translate)("\n\nhas been changed to:\n    ") + validText + "\n";

                    int toolTipMSec = (msg.Length + toolTipInvalidChar.ToolTipTitle.Length) * 100;

                    toolTipInvalidChar.Show(msg, TextToSend, 10, TextToSend.Height + 10, toolTipMSec);
                }
                else
                {
                    toolTipInvalidChar.ToolTipTitle = "";

                    int toolTipMSec = msg.Length * 100;

                    toolTipInvalidChar.Show(msg, TextToSend, 10, TextToSend.Height - 40, toolTipMSec);
                }

                TextToSend.Text = validText;

                // restore the cursor position
                int cursorPos = caret - invalidText.Length;
                if (cursorPos < 0) cursorPos = 0;

                TextToSend.SelectionStart = cursorPos;
            }

            if (currentCallId != null && currentInfo == null)
            {
                if (infoMap.ContainsKey(currentCallId))
                {
                    currentInfo = infoMap[currentCallId];
                    currentInfo.restore();
                }
                else
                {
                    currentInfo = new Info(this, currentCallId);
                    infoMap.Add(currentCallId, currentInfo);
                }
            }

            if (currentInfo != null) currentInfo.textToSend = TextToSend.Text;

			SendCheck();
		}

        private const string validSymbolStr = "- $'!:(\")=?+./;,";

        private bool isValidChar(char c)
        {
            if (c >= 'a' && c <= 'z') return true;
            if (c >= 'A' && c <= 'Z') return true;
            if (c >= '0' && c <= '9') return true;

            // no need to check for "\r\n\0"
            return (validSymbolStr.Contains(c.ToString()));
        }

		private void SendCheck()
		{
			KeepChecking = (TextToSend.TextLength > 0);

			if (KeepChecking) checkSendButtonTimer.Change(500,500);
			else              checkSendButtonTimer.Change(System.Threading.Timeout.Infinite,System.Threading.Timeout.Infinite);
			
            SendButtonCheck(null);
		}

        delegate void ButtonCheckDelegate();

		private void SendButtonCheck(object blank)
		{
            try {sendButton.BeginInvoke(new ButtonCheckDelegate(ButtonCheck));}
            catch {try {ButtonCheck();} catch {}}
		}

        private void ButtonCheck()
        {
            if (currentInfo == null) return;

            sendButton.Enabled = sendMenu.Enabled = (SendButtonEnabled && ! SendingText && TextToSend.TextLength > 0);
        }

        private bool newConversation = true;

		private void btnSend_Click(object sender, System.EventArgs e)
		{
            if (currentInfo == null) return;

			try
			{
				SendingText = true;

				this.Cursor = Cursors.WaitCursor;

				if (newConversation)
				{
					// force CRLF first - fixes garbage on line issue
					newConversation = false;
				}

				// Force any received text to conversation window
				PushSentToConversation();
				SendingText = false;
				SendButtonCheck(null);
			}
			finally
			{
				this.Cursor = Cursors.Default;
			}
		}

		private void messageButton_ItemClick(object sender, EventArgs e)
		{
    		messagesButton.Checked = messagesMenu.Checked = true;

			if (messages == null || ! messages.Visible)
            {
                if (messages != null) messages.Dispose();

                switch (windowType)
                {
			        case WindowType.TDD: messages = new CannedMessages(controller, controller.settings.TDDCannedMessages, windowType); break;
                    case WindowType.SMS: messages = new CannedMessages(controller, controller.settings.SMSCannedMessages, windowType); break;
                }

                messages.CannedMessagesClosing += new CannedMessages.OnCannedMessagesClosing(messages_MessagesClosing);
                messages.CannedMessageSelected += new CannedMessages.OnCannedMessageSelected(messages_MessageSelected);

                messages.Show(this);
            }
            else
            {
                messages.BringToFront();
            }
		}

		public void MessageClosed()
		{
			messagesButton.Checked = messagesMenu.Checked = false;
		}

		private void updateFocus(object sender, System.EventArgs e)
		{
            if (TextToSend.Enabled) TextToSend.Focus();
            else                    toolBar.Focus();
		}

		private void menuClose_ItemClick(object sender, EventArgs e)
		{
			Hide();
		}

        // TODO TDD/SMS: remove Mute functionality once we're sure we don't need it (made invisible 25 May 2010 - elliott)
		private void muteButton_CheckStateChanged(object sender, EventArgs e)
		{
            if (sender == muteMenu)
            {
                muteButton.Checked = muteMenu.Checked;
                return;
            }

            if (currentCallId != null && currentInfo == null)
            {
                if ( ! infoMap.ContainsKey(currentCallId))
                {
                    infoMap.Add(currentCallId, new Info(this, currentCallId));
                }

                currentInfo = infoMap[currentCallId];
                currentInfo.restore();
            }

            if (currentInfo != null) currentInfo.muteState = muteButton.Checked;

            muteMenu.Checked = muteButton.Checked;

            if (currentInfo != null && ! currentInfo.inMuteChanged)
            {
                switch (windowType)
                {
			        case WindowType.TDD: controller.TDDChangeMute(currentCallId, muteButton.Checked); break;
                    case WindowType.SMS: break; // TODO SMS: can SMS mute?
                }
            }

			if (muteButton.Checked)
            {
                muteButton.Image = Properties.Resources.ConsoleCallMuteOffIcon.ToBitmap();
                muteMenu.  Image = new Icon(Properties.Resources.ConsoleCallMuteOffIcon, 16, 16).ToBitmap();
            }
			else
            {
                muteButton.Image = Properties.Resources.ConsoleCall_MuteIcon.ToBitmap();
                muteMenu.  Image = new Icon(Properties.Resources.ConsoleCall_MuteIcon, 16, 16).ToBitmap();
            }
		}

		private void Window_VisibleChanged(object sender, System.EventArgs e)
		{
            if (Visible)
            {
                // scroll to the bottom
                ScrollToConversationBottom();

                TextToSend.Focus();
            }
		}

        private class Info
        {
            public Info(MessageWindow _messageWindow, string _callId) {messageWindow = _messageWindow; callId = _callId;}

            public void remember()
            {
                selectionStart  = messageWindow.TextToSend.SelectionStart;
                selectionLength = messageWindow.TextToSend.SelectionLength;
            }

            public static void restoreEmpty(MessageWindow messageWindow)
            {
                messageWindow.Conversation.Controls.Clear();

                messageWindow.RealTimeTextBox.   Clear();
                messageWindow.TextToSend. Clear();
                messageWindow.TextToSend. SelectionLength = 0;

                messageWindow.printButton.Enabled = messageWindow.printMenu.Enabled = false;
            }

            public void restore()
            {
                messageWindow.Conversation.Controls.Clear();

                messageWindow.RealTimeTextBox.   Text = receivedText;
                messageWindow.TextToSend. Text = textToSend;

                messageWindow.printButton.Enabled = messageWindow.printMenu.Enabled = (conversationRawText.Length > 0);

                if      (selectionStart    != -1) messageWindow.TextToSend.SelectionStart = selectionStart;
                else if (textToSend.Length != 0)  messageWindow.TextToSend.SelectionStart = textToSend.Length;

                messageWindow.TextToSend.SelectionLength = selectionLength;

                // rebuild the conversation from raw
                if (conversationRawText.Length != 0)
                {
                    string raw = conversationRawText;
                    conversationRawText = ""; // clear it since it will be appended to in SomeoneSays()

                    messageWindow.SomeoneSays(callId, raw);
                }
            }

            private MessageWindow messageWindow = null;

            public string callId = null;

            public string receivedText    = "";
            public string textToSend      = "";
            public int    selectionStart  = -1;
            public int    selectionLength = 0;

            public string conversationRawText = "";
            public bool   muteState           = false;
            public bool   inMuteChanged       = false;
        }

        private System.Collections.Generic.Dictionary<string,Info> infoMap =
            new System.Collections.Generic.Dictionary<string,Info>();

        private Info   currentInfo   = null;
        private string currentCallId = null;

        public void setCurrentCallId(string callId)
        {
            if (callId == currentCallId) return;

            if (callId == null || callId.Length == 0)
            {
                // clear the window
                try { Info.restoreEmpty(this); } catch { }

                currentCallId = null;
                return;
            }

            // remember current info
            if (currentInfo != null) currentInfo.remember();

            currentCallId = callId;

            if (Visible && windowType == WindowType.TDD)
            {
                if (TextToSend.Enabled) controller.sendToController("TDDModeOn", currentCallId);
                else                    needToSendTDDModeOn = true;
            }

            // try to find Info for the new callId
            if ( ! infoMap.TryGetValue(callId, out currentInfo))
            {
                // if it wasn't found, clear the window
                try { Info.restoreEmpty(this); } catch { }
            }
            else
            {
                currentInfo.restore();
            }
        }

		private void aboutMenu_ItemClick(object sender, EventArgs e)
		{
			Splash.ShowAbout(controller.Position);
			TextToSend.Focus();
		}

		private void usersManualMenu_Click(object sender, EventArgs e)
		{
			Console.ShowUsersManual();
		}

		private void PrintHeader(Graphics graphic)
		{
			LinePosition = 10;
			printerFont = new Font("Lucida Console", fontSize );
			printerBrush = new SolidBrush(Color.Black);
			LinePosition = TopMargin;

			printerFont = new Font(printerFont,FontStyle.Bold);
			WriteToPrinter(graphic, (Translate)(windowType.ToString()) + (Translate)(" Conversation"));
			printerFont = new Font(printerFont,FontStyle.Regular);

			graphic.DrawLine(Pens.Black,new PointF(LeftMargin,LinePosition),new PointF(printDoc.DefaultPageSettings.PaperSize.Width - LeftMargin,LinePosition));
			LinePosition += 20;
		}

		private void printDoc_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
		{
			printerFont = new Font("Lucida Console", fontSize );

            // force page header print
			LinePosition = printSetup.PrinterSettings.DefaultPageSettings.PaperSize.Height + 10;
			printerFont = new Font(printerFont,FontStyle.Regular);

            foreach (Control control in Conversation.Controls)
            {
                if (control is BubbleLabel)
                {
                    // TODO TDD/SMS: make printing better
                    BubbleLabel label = (BubbleLabel)(control);
                    WriteToPrinter(e.Graphics, label.msgStr);
                }
            }
		}

		public void WriteToPrinter(Graphics graphic, string Text)
		{
			if (LinePosition > printSetup.PrinterSettings.DefaultPageSettings.PaperSize.Height)
				PrintHeader(graphic);

			graphic.DrawString(Text,printerFont, printerBrush, (float) LeftMargin, LinePosition);
			LinePosition += printerFont.Height + 5;
		}

		private void printMenu_ItemClick(object sender, EventArgs e)
		{
			if (printSetup.ShowDialog() == DialogResult.OK)
				printDoc.Print();

			TextToSend.Focus();
		}

        private int TextToSendLengthAtKeyDown = 0;

		private void TextToSend_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
            TextToSendLengthAtKeyDown = TextToSend.TextLength;

			Logging.AppTrace(windowType.ToString() + " KeyValue: " + e.KeyValue.ToString());

			switch (e.KeyCode)
			{
				case Keys.Enter:

					if ( ! (e.Alt || e.Shift || e.Control))
					{
						if (TextToSend.Text.Length > 0 && sendButton.Enabled) sendButton.PerformClick();

                        e.Handled = true;
					}

					break;

				case Keys.Escape:
					e.Handled = true;
					this.Close();
					break;
			}
		}

		private const char TabKey = (char)9;

		private void TextToSend_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			switch (e.KeyChar)
			{
				case TabKey:
					e.Handled = true;
					sendButton.Focus();
					break;
			}
		}

		private void Window_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			switch (e.KeyCode)
            {
                case Keys.Tab:
                {
                    if (sendButton.Enabled)
			        {
				        e.Handled = true;
				        sendButton.Focus();
			        }

                    break;
                }
                case Keys.Escape:
                {
                    e.Handled = true;
                    this.Close();
                    break;
                }
            }
		}

		private void Window_Activated(object sender, System.EventArgs e)
		{
			messagesButton.Checked = messagesMenu.Checked = (messages != null && messages.Visible);

            if (TextToSend.Enabled)
            {
                TextToSend.Focus();

                if (windowType == WindowType.TDD) controller.sendToController("TDDModeOn", currentCallId);
            }
            else
            {
                if (windowType == WindowType.TDD) needToSendTDDModeOn = true;
            }

            ScrollToConversationBottom();
		}

		private void RealTime_TextChanged(object sender, System.EventArgs e)
		{
            RichTextBox rtb = (RichTextBox) sender;
            SendMessage(rtb.Handle, WMV_SCROLL, SB_BOTTOM, 0);
		}

        public void setEnabled(bool enabled)
        {
            // enable the Conversation window if there is text (this must be first)
            Conversation.Enabled = enabled || (Conversation.Controls.Count != 0);

            if (TextToSend.Enabled == enabled) return;

            if ( ! enabled)
            {
                toolTip.SetToolTip(TextToSend,      (Translate)("Call is not active"));
                toolTip.SetToolTip(RealTimeTextBox, (Translate)("Call is not active"));
                toolTip.SetToolTip(sendButton,      (Translate)("Call is not active"));

                if (messages != null) messages.Close();

                BackColor = System.Drawing.SystemColors.Control;
            }
            else
            {
                toolTip.RemoveAll();

                BackColor = System.Drawing.SystemColors.Window;

                if (needToSendTDDModeOn && currentCallId != null)
                {
                    needToSendTDDModeOn = false;
                    controller.sendToController("TDDModeOn", currentCallId);
                }
            }

            TextToSend.     Enabled = enabled;
            RealTimeTextBox.       Enabled = enabled;

            messagesMenu.   Enabled = enabled;
            muteMenu.       Enabled = enabled;

            messagesButton. Enabled = enabled;
            muteButton.     Enabled = enabled;
        }

        private const char ETX = (char)3;  // end of text

        public void CallerSays(string callId, string str)
        {
            // make sure that this callId has an Info object
            if ( ! infoMap.ContainsKey(callId)) infoMap.Add(callId, new Info(this, callId));

            infoMap[callId].receivedText = "";

            if (callId == currentCallId) RealTimeTextBox.Clear();

            SomeoneSays(callId, str);
        }

        public void DispatcherSays(string callId, string str)
        {
            // make sure that this callId has an Info object
            if ( ! infoMap.ContainsKey(callId)) infoMap.Add(callId, new Info(this, callId));

            SomeoneSays(callId, str);
        }

        private void SomeoneSays(string callId, string str)
        {
            try
            {
                infoMap[callId].conversationRawText += str;

                if (callId == currentCallId)
                {
                    int curIndex = 0;

                    bool scrollIsAtBottom =
                        Conversation.Controls.Count == 0 || ! Conversation.VerticalScroll.Visible ||
                         (Conversation.VerticalScroll.Value >= Conversation.VerticalScroll.Maximum);

                    Conversation.SuspendLayout();

                    while (curIndex < str.Length)
                    {
                        int etxIndex = str.IndexOf(ETX, curIndex);
                        if (etxIndex == -1) etxIndex = str.Length - 1;

                        string subStr = str.Substring(curIndex, etxIndex - curIndex);

                        System.Text.RegularExpressions.Match callerMatch =
                            System.Text.RegularExpressions.Regex.Match(subStr, callerSaysRegExp);

                        bool isCaller = callerMatch.Success;

                        int speakerIndex      = -1;
                        int afterSpeakerIndex = -1;

                        if (isCaller)
                        {
                            string callerSaysPrefix = callerMatch.ToString();

                            speakerIndex      = str.IndexOf(callerSaysPrefix, curIndex, etxIndex - curIndex);
                            afterSpeakerIndex = speakerIndex + callerSaysPrefix.Length;
                        }
                        else
                        {
                            System.Text.RegularExpressions.Match dispatcherMatch =
                                System.Text.RegularExpressions.Regex.Match(subStr, dispatcherSaysRegExp);

                            if (dispatcherMatch.Success)
                            {
                                string dispatcherSaysPrefix = dispatcherMatch.ToString();

                                speakerIndex      = str.IndexOf(dispatcherSaysPrefix, curIndex, etxIndex - curIndex);
                                afterSpeakerIndex = speakerIndex + dispatcherSaysPrefix.Length;
                            }
                            else
                            {
                                speakerIndex = afterSpeakerIndex = curIndex;
                            }
                        }

                        string preSpeakerStr = str.Substring(curIndex,          speakerIndex      - curIndex);
                        string speakerStr    = str.Substring(speakerIndex,      afterSpeakerIndex - speakerIndex);
                        string msgStr        = str.Substring(afterSpeakerIndex, etxIndex          - afterSpeakerIndex).Trim();

                        curIndex = etxIndex + 1;

                        printButton.Enabled = printMenu.Enabled = true;

                        // add new bubble
                        BubbleLabel label = new BubbleLabel(
                            windowType, Conversation, preSpeakerStr, speakerStr, msgStr, isCaller);
                    }

                    Conversation.ResumeLayout(true);

                    FLASHWINFO flashInfo = new FLASHWINFO();
                    flashInfo.cbSize = Convert.ToUInt32(Marshal.SizeOf(flashInfo));
                    flashInfo.hwnd = Conversation.Parent.Parent.Handle;
                    flashInfo.dwTimeout = 0;

                    // scroll to the bottom
                    if (scrollIsAtBottom) ScrollToConversationBottom();

                    if ( ! scrollIsAtBottom || Conversation.Parent.Parent.Handle != GetForegroundWindow())
                    {
                        // when a new message comes in, flash the window twice
                        flashInfo.dwFlags = (UInt32)(FlashFlag.FLASHW_ALL | FlashFlag.FLASHW_TIMER);
                        flashInfo.uCount = 2;

                        FlashWindowEx(ref flashInfo);
                    }
                }
            }
            catch {}
        }

		private void ScrollToConversationBottom()
        {
            try
            {
                Conversation.VerticalScroll.Value = Conversation.VerticalScroll.Maximum;

                new System.Threading.Timer(
                    ScrollToConversationBottomTimeout, null, 30, System.Threading.Timeout.Infinite);

                new System.Threading.Timer(
                    ScrollToConversationBottomTimeout, null, 100, System.Threading.Timeout.Infinite);

                new System.Threading.Timer(
                    ScrollToConversationBottomTimeout, null, 400, System.Threading.Timeout.Infinite);
            }
            catch {}
        }

		private void ScrollToConversationBottomTimeout(object blank)
		{
            try {BeginInvoke(new ScrollToBottomDelegate(scrollToBottomFromDelegate)); }
            catch {try {scrollToBottomFromDelegate();} catch {} }
		}

        delegate void ScrollToBottomDelegate();

        private void scrollToBottomFromDelegate()
        {
            try   {Conversation.VerticalScroll.Value = Conversation.VerticalScroll.Maximum;}
            catch {}
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool FlashWindowEx(ref FLASHWINFO pwfi);

        [DllImport("user32.dll", CharSet=CharSet.Auto, ExactSpelling=true)]
        private static extern IntPtr GetForegroundWindow();

        [StructLayout(LayoutKind.Sequential)]
        public struct FLASHWINFO
        {
            public UInt32 cbSize;
            public IntPtr hwnd;
            public UInt32 dwFlags;
            public UInt32 uCount;
            public UInt32 dwTimeout;
        }

        enum FlashFlag
        {
            /// <summary> Stop flashing. The system restores the window to its original state </summary>
            FLASHW_STOP      = 0x00000000,
            /// <summary> Flash the window caption </summary>
            FLASHW_CAPTION   = 0x00000001,
            /// <summary> Flash the taskbar button </summary>
            FLASHW_TRAY      = 0x00000002,
            /// <summary> Flash both the window caption and taskbar button (FLASHW_CAPTION | FLASHW_TRAY flags) </summary>
            FLASHW_ALL       = 0x00000003,
            /// <summary> Flash continuously, until the FLASHW_STOP flag is set </summary>
            FLASHW_TIMER     = 0x00000004,
            /// <summary> Flash continuously until the window comes to the foreground </summary>
            FLASHW_TIMERNOFG = 0x0000000C,
        }

        private class BubbleLabel : UserControl
        {
            public BubbleLabel(
                WindowType _windowType,
                Panel      _conversation,
                string     _preSpeakerStr,
                string     _speakerStr,
                string     _msgStr,
                bool       _isCaller
            )
            {
                conversation  = _conversation;
                windowType    = _windowType;
                isCaller      = _isCaller;
                preSpeakerStr = _preSpeakerStr;
                speakerStr    = _speakerStr;
                msgStr        = _msgStr;

                Font = new Font("Meiryo UI", 13, FontStyle.Regular, GraphicsUnit.Pixel);

                // transparent background
                BackColor = Color.Transparent;

                Width = conversation.ClientSize.Width;

                // calculate text boxes
                Graphics graphics = CreateGraphics();

                SizeF textBoxSize = graphics.MeasureString(msgStr, Font, Width - 100, stringFormat);

                speakerFont = new Font(SystemFonts.DefaultFont.FontFamily, 9, FontStyle.Regular, GraphicsUnit.Pixel);

                SizeF speakerTextSize = graphics.MeasureString(preSpeakerStr + speakerStr, speakerFont, Width);

                // pad height
                Height = (int)(textBoxSize.Height + 14 + speakerTextSize.Height + 3 + 0.5) + 4;

                Dock = DockStyle.Top;

                conversation.Controls.Add(this);

                // the index of the most recent message should be zero because of DockStyle.Top above
                conversation.Controls.SetChildIndex(this, 0);
            }

            protected override void OnClientSizeChanged(EventArgs e)
            {
                base.OnClientSizeChanged(e);

                Width = conversation.ClientSize.Width;
                Refresh();
            }

            protected override void OnPaint(PaintEventArgs e)
            {
                base.OnPaint(e);

                e.Graphics.SmoothingMode     = SmoothingMode.HighQuality;
                e.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
                e.Graphics.PixelOffsetMode   = PixelOffsetMode.HighQuality;
                e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

                // determine textbox size
                SizeF speakerTextSize = e.Graphics.MeasureString(
                    preSpeakerStr + speakerStr, speakerFont, Width - 60);

                SizeF textBoxSize = e.Graphics.MeasureString(msgStr, Font, Width - 60, stringFormat);

                // if the height of the text is greater than the height of the bubble, adjust it
                for (int i = 1; i != 10 && textBoxSize.Height + 10 + speakerTextSize.Height + 3 + 4 > Height; ++i)
                {
                    textBoxSize = e.Graphics.
                        MeasureString(msgStr, Font, Width - 100 - 5*i, stringFormat);
                }

                float textBoxWidth = textBoxSize.Width;

                if (textBoxWidth < speakerTextSize.Width) textBoxWidth = speakerTextSize.Width;

                float top    = 2.0f;
                float bottom = Height - 2.0f;
                float right  = 0.0f;
                float left   = 0.0f;

                SolidBrush primaryTextBrush   = null;
                SolidBrush secondaryTextBrush = null;
                SolidBrush bubbleBrush        = null;

                GraphicsPath path = new GraphicsPath();

                Settings.ColorInfo colorInfo;

                if (isCaller)
                {
                    colorInfo = callerColorInfo[(int)windowType];

                    left  = 12;
                    right = left + textBoxWidth + 12;

                    // top corners
                    path.AddArc(left,          top, 30.0f, 25.0f, 180.0f, 90.0f);
                    path.AddArc(right - 30.0f, top, 30.0f, 25.0f, 270.0f, 90.0f);

                    // bottom right
                    path.AddArc(right - 30.0f, bottom - 25.0f, 30.0f, 25.0f, 0.0f, 90.0f);

                    // bubble origin (bottom left)
                    path.AddArc(left +  4.0f, bottom -  8.0f, 22.0f,        8.0f, 90.0f,  90.0f);
                    path.AddArc(left - 12.0f, bottom -  8.0f, 12.0f + 4.0f, 8.0f,  0.0f,  90.0f);
                    path.AddArc(left - 12.0f, bottom - 20.0f, 12.0f,       20.0f, 90.0f, -90.0f);
                }
                else
                {
                    colorInfo = dispatcherColorInfo[(int)windowType];

                    right = Width - 12;
                    left  = right - (textBoxWidth + 12);

                    // top corners
                    path.AddArc(left,          top, 30.0f, 25.0f, 180.0f, 90.0f);
                    path.AddArc(right - 30.0f, top, 30.0f, 25.0f, 270.0f, 90.0f);

                    // bubble origin (bottom right)
                    path.AddArc(right,                bottom - 20.0f, 12.0f,        20.0f, 180.0f, -90.0f);
                    path.AddArc(right -  4.0f,        bottom -  8.0f, 12.0f + 4.0f,  8.0f, 90.0f,   90.0f);
                    path.AddArc(right - 22.0f - 4.0f, bottom -  8.0f, 22.0f,         8.0f,  0.0f,   90.0f);

                    // bottom left
                    path.AddArc(left, bottom - 25.0f, 30.0f, 25.0f, 90.0f, 90.0f);
                }

                bubbleBrush = new SolidBrush(colorInfo.backgroundColor1);

                if (colorInfo.foregroundColor.GetBrightness() < 0.5f)
                {
                    // 87% per Google Color Guidelines
                    primaryTextBrush = new SolidBrush(Color.FromArgb((int)(255 * 0.87), colorInfo.foregroundColor));

                    // 54% per Google Color Guidelines
                    secondaryTextBrush = new SolidBrush(Color.FromArgb((int)(255 * 0.54), colorInfo.foregroundColor));
                }
                else
                {
                    // 100% per Google Color Guidelines
                    primaryTextBrush = new SolidBrush(Color.FromArgb(255, colorInfo.foregroundColor));

                    // 70% per Google Color Guidelines
                    secondaryTextBrush = new SolidBrush(Color.FromArgb((int)(255 * 0.7), colorInfo.foregroundColor));
                }

                // bubble
                e.Graphics.FillPath(bubbleBrush, path);

                // text
                e.Graphics.DrawString(msgStr, Font, primaryTextBrush,
                    new RectangleF(left + 7, top + 7, right - left + 7, 10000), stringFormat);

                e.Graphics.DrawString(preSpeakerStr + speakerStr, speakerFont,
                    secondaryTextBrush, right - speakerTextSize.Width - 5, bottom - speakerTextSize.Height - 6);
            }

            private Panel        conversation;
            private WindowType   windowType;
            private bool         isCaller;
            private string       preSpeakerStr;
            private string       speakerStr;
            public  string       msgStr;

            private Font         speakerFont;
            private StringFormat stringFormat = new StringFormat(StringFormatFlags.NoClip);
        }

        // TODO TDD/SMS: remove Mute functionality once we're sure we don't need it (made invisible 25 May 2010 - elliott)
        public void MuteChanged(string callId, bool mute)
        {
            // make sure that this callId has an Info object
            if ( ! infoMap.ContainsKey(callId))
            {
                // if mute is false (the default), we don't need to keep track of it
                if ( ! mute) return;

                infoMap.Add(callId, new Info(this, callId));
            }

            Info info = infoMap[callId];

            info.inMuteChanged = true;

            // if this callId is the current callId, change the button
            // NOTE: do as little as possible inside the if due to multi-threading
            if (callId == currentCallId) muteButton.Checked = mute;
            else                         info.muteState     = mute;

            info.inMuteChanged = false;
        }

        public bool getMuteState(string callId)
        {
            Info info;
            if (infoMap.TryGetValue(callId, out info)) return info.muteState;

            return false;
        }

        // TODO* TDD/SMS: Remove info objects from the infomap if the infomap gets too big...
        //                the infoMap shouldn't get very big generally since it only contains
        //                objects for calls that have TDD or SMS
    }
}
