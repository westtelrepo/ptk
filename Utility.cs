using System;
using System.Reflection;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using WestTel.E911;

namespace WestTel.E911
{
    /// <summary>
    /// Summary description for Utility.
    /// </summary>
    public class Utility
    {
        public Utility() {}

        public static string DecodeBase64(string encodedString)
        {
            if (encodedString.Length == 0) return encodedString;

            try
            {
                byte[] bytes = Convert.FromBase64CharArray(encodedString.ToCharArray(), 0, encodedString.Length);

                string decodedString = System.Text.Encoding.ASCII.GetString(bytes);

                if (decodedString.StartsWith(Environment.NewLine))
                {
                    decodedString = decodedString.Remove(0, Environment.NewLine.Length);
                }

                return decodedString;
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("Error in Utility.DecodeBase64", ex, Console.LogID.Gui);
                Logging.AppTrace("Error in Utility.DecodeBase64:" + ex.Message);

                return ex.Message;
            }
        }

        public static string EncodeBase64(string decodedString)
        {
            bool success = true;
            return EncodeBase64(decodedString, out success);
        }

        public static string EncodeBase64(string decodedString, out bool success)
        {
            success = true;

            if (decodedString.Length == 0) return decodedString;

            try
            {
                byte[] decodedBytes = System.Text.Encoding.ASCII.GetBytes(decodedString);

                return Convert.ToBase64String(decodedBytes, 0, decodedBytes.Length);
            }
            catch (Exception ex)
            {
                success = false;

                Logging.ExceptionLogger("Error in Utility.EncodeBase64", ex, Console.LogID.Gui);
                Logging.AppTrace("Error in Utility.EncodeBase64:" + ex.Message);

                return ex.Message;
            }
        }
    }
}
