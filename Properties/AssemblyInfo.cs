using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("WestTel PSAP Toolkit")]
[assembly: AssemblyDescription("WestTel PSAP Toolkit")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("WestTel International")]
[assembly: AssemblyProduct("WestTel PSAP Toolkit")]
[assembly: AssemblyCopyright("Copyright © 2002-2017 WestTel International")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]     

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(true)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("f11151a2-9ff4-44ed-8bdb-2c9f4fd46ad9")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.7.1.07")]
[assembly: AssemblyFileVersion("2.7.1.07")]
