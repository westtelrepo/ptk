using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Net;
using System.Security.AccessControl;
using System.Security.Principal;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using Microsoft.Win32;
using System.Windows.Forms;
using System.Xml;
using NamedPipeWrapper;

namespace WestTel.E911
{
    using CallRowType = Console.CallRowInfo.CallType;
    using CallRowState = Console.CallRowInfo.State;

    public static class Application
    {
        // TODO gui: get company and product name from Controller settings
        public static string CompanyName    = System.Windows.Forms.Application.CompanyName;
        public static string ProductName    = System.Windows.Forms.Application.ProductName;
        public static string ProductVersion = System.Windows.Forms.Application.ProductVersion;
    }

    public class Controller
    {
        public bool StartingUp = true;
        public bool OkToStop = false;
        public Settings settings = new Settings();

        public static System.Net.Sockets.ProtocolType protocol = System.Net.Sockets.ProtocolType.Tcp;

        private readonly NamedPipeClient<string> updateServiceNamedPipe =
            new NamedPipeClient<string>("911_Update_Service_Pipe");

        public static FileSystemAccessRule everyoneAccessRule = new FileSystemAccessRule(
            new SecurityIdentifier(WellKnownSidType.WorldSid, null),
            FileSystemRights.FullControl, AccessControlType.Allow);

        public bool SystemShutdown = false;
        // Child Forms
        private TaskbarNotifier tbn1 = new TaskbarNotifier();
        private TaskbarNotifier tbn2 = new TaskbarNotifier();
        private TaskbarNotifier tbn3 = new TaskbarNotifier();
        private TaskbarNotifier tbn4 = new TaskbarNotifier();
        private TaskbarNotifier tbn5 = new TaskbarNotifier();
        private TaskbarNotifier tbn6 = new TaskbarNotifier();
        private TaskbarNotifier tbn7 = new TaskbarNotifier();
        private TaskbarNotifier tbn8 = new TaskbarNotifier();
        public  Phonebook phonebook;
        private Console console;
        private MessageWindow tddWindow = null;
        private MessageWindow smsWindow = null;
        private IRRWindow irrWindow = null;
        public  ColorEditor colorEditor = null;
        private bool settingsReceived = false; // 12 Mar elliott 2008
        private PSAPStatus psapStatus;
        private AlarmDisplay alarmDisplay;
        private DebugWindow debugWindow = null;
        public  DataModel dataModel;
        public  XmlMessages XmlMessageServer;
        public  NoForm noForm;
        private ManualALIRequest manualALIRequestDialog = null;
        public  IrrClient irrClient;

        public int Position = 1;  //  Position assigned to this client

        public static Process    currentProcess = Process.GetCurrentProcess();
        public static Controller controller;
        AppDomain appDomain;

        private static string[] exeArgs;
        
        [STAThread]
        static void Main(string[] args)
        {
            exeArgs = args;

            System.Threading.Thread.CurrentThread.Name = "MainThread";

            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);

            Controller.controller = new Controller();
            Controller.controller.Start(args);
        }

        private static FileStream logFile = null;
        private bool showDebugData        = false;
        private bool showLockInfoData     = false;

        private void Start(string[] args)
        {
            // System.Windows.Forms.Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
            bool isOnlyInstance = false;
            System.Threading.Mutex mutex = null;

            try
            {
                mutex = new System.Threading.Mutex(
                    true, "Local\\" + Application.ProductName, out isOnlyInstance);
            }
            catch
			{
			    isOnlyInstance = false;
			}

            // load settings very early for language translation support
            settings.LoadLocalSettingsFiles();

            if ( ! isOnlyInstance)
            {
                MessageBox.Show(null, (Translate)("There is another occurrence of ") +
                    Application.ProductName + (Translate)(" already running.") +
                    (Translate)(" It may be because another user is logged in and running it.\n\n") +
                    (Translate)("This program will now exit."),
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning,
                    MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);

                return;
            }

            // check version of .Net
            bool goodNetVersionFound = false;

            try
            {
                RegistryKey netFrameworkSubKey =
                    Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\NET Framework Setup\\NDP");

                if (netFrameworkSubKey != null)
                {
                    string[] netVersions = netFrameworkSubKey.GetSubKeyNames();

                    char[] dotSep = {'.'};

                    foreach (string versionStr in netVersions)
                    {
                        string[] versionStringList = versionStr.Substring(1).Split(dotSep);

                        int majorVersion = 0;
                        int minorVersion = 0;

                        try
                        {
                            if (versionStringList.Length > 0)
                            {
                                char c = versionStringList[0][0];

                                if (c == 'v' || (c >= '0' && c <= '9'))
                                {
                                    majorVersion = Convert.ToInt32(versionStringList[0].Replace("v", ""));

                                    if (versionStringList.Length > 1)
                                    {
                                        minorVersion = Convert.ToInt32(versionStringList[1]);
                                    }
                                }
                            }
                        }
                        catch
                        {
                            continue;
                        }

                        int version_x_10 = (majorVersion * 10) + minorVersion;

                        // if the version is greater than or equal to 4.0
                        if (version_x_10 >= 40)
                        {
                            goodNetVersionFound = true;
                            break;
                        }
                    }
                }
            }
            catch {}

            if ( ! goodNetVersionFound)
            {
                MessageBox.Show(null, (Translate)("This program requires the .Net Framework 4.0 or greater.\n\n") +
                    (Translate)("This program will now exit."),
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning,
                    MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                return;
            }

#if ! DEBUG
            // show the splash screen for about 3 seconds
            try { Splash.ShowSplash(500, 2500, 200); } catch {}
#endif

            noForm = new NoForm(this, showDebugData);

            // initialize sys-tray popup windows
            try
            {
                tbn1.init(tbn_ExitMessageReceived);
                tbn2.init(tbn_ExitMessageReceived);
                tbn3.init(tbn_ExitMessageReceived);
                tbn4.init(tbn_ExitMessageReceived);
                tbn5.init(tbn_ExitMessageReceived);
                tbn6.init(tbn_ExitMessageReceived);
                tbn7.init(tbn_ExitMessageReceived);
                tbn8.init(tbn_ExitMessageReceived);
            }
            catch {}

            string StartupErrors = "";
			
            appDomain = AppDomain.CurrentDomain;

            appDomain.UnhandledException += new UnhandledExceptionEventHandler(ad_UnhandledException);

            string demoMode  = "false";
            bool   debugMode = false;
            bool   startRecordingDebugScript = false;

            try
            {
                bool unexpectedArgs = false;

                // parse args
                for (int i = 0; i < args.Length; ++i)
                {
                    if (args[i][0] != '/' && args[i][0] != '-') continue;

                    char[] trimChars = {'/','-'};
                    string arg = args[i].TrimStart(trimChars);

                    switch (arg.ToLower())
                    {
                        case "position":
                        {
                            if (args.Length > i+1 && args[i][0] != '/' && args[i][0] != '-')
                            {
                                ++i;

                                try
                                {
                                    Position = Convert.ToUInt16(args[i]);

                                    if (Position == 0)   throw new Exception((Translate)("Must not be") + " 0");
                                    if (Position > 1000) throw new Exception((Translate)("Must not be greater than") + " 1000");
                                }
                                catch
                                {
                                    Position = 1;

                                    MessageBox.Show(null, (Translate)("Command line argument") + " \"" + args[i-1] +
                                                          " " + args[i] + "\" " + (Translate)("is not a valid integer from") + " 1-1000.",
                                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information,
                                        MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                                }
                            }

                            break;
                        }
                        case "demo":
                        {
                            if (args.Length > i+1 && args[i][0] != '/' && args[i][0] != '-')
                            {
                                ++i;
                                demoMode = args[i];
                            }
                            else
                            {
                                demoMode = "Test";
                            }

                            break;
                        }
                        case "debug":
                        {
                            debugMode = true;
                            break;
                        }
                        // debugStartRecording
                        case "debugstartrecording":
                        {
                            debugMode                 = true;
                            startRecordingDebugScript = true;
                            break;
                        }
                        default:
                        {
                            unexpectedArgs = true;
                            break;
                        }
                    }
                }

                if (unexpectedArgs)
                {
                    MessageBox.Show(null, (Translate)("Unexpected command line arguments:\n\n") + string.Join(" ", args) +
                        (Translate)("\n\nExpected") + " \"/position\", \"/demo\", \"/debug\" or \"/debugStartRecording\".\n\n" +
                        (Translate)("The unexpected arguments will be ignored."),
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information,
                        MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                }
            }
            catch (Exception ex)
            {
                StartupErrors += ex.Message + " from " + ex.Source + " (while parsing the command line)";
                Logging.ExceptionLogger("Error in Device Startup - parsing command line", ex, Console.LogID.Init);
            }

            try
            {
                dataModel = new DataModel();

                if (showDebugData && showLockInfoData) DataModel.LogLockInfo += DebugWindowMsg;

                if (Console.toBoolean(settings.GetSetting("Logging", "false")))
                {
                    noForm.enableLogging();

                    string initialDirectory =
                        Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\WestTel";

                    if ( ! Directory.Exists(initialDirectory)) Directory.CreateDirectory(initialDirectory);

                    initialDirectory += "\\Logs";

                    if ( ! Directory.Exists(initialDirectory)) Directory.CreateDirectory(initialDirectory);

                    string mostRecentLogPath = initialDirectory + "\\PSAP Toolkit Log (most recent).txt";
                    string oldLogPath        = initialDirectory + "\\PSAP Toolkit Log (old).txt";
                    string oldestLogPath     = initialDirectory + "\\PSAP Toolkit Log (oldest).txt";

                    if (File.Exists(mostRecentLogPath))
                    {
                        if (File.Exists(oldLogPath)) File.Replace (mostRecentLogPath, oldLogPath, oldestLogPath, true);
                        else                         File.Move    (mostRecentLogPath, oldLogPath);
                    }

                    if (logFile == null)
                    {
                        logFile = new FileStream(
                            mostRecentLogPath, FileMode.Create, FileAccess.Write, FileShare.Read); 
                    }
                }
                else if (logFile != null)
                {
                    logFile.Close();
                    logFile = null;
                }
            }
            catch (Exception ex)
            {
                StartupErrors += ex.Message + " from " + ex.Source;
                Logging.ExceptionLogger("Error in Device Startup - ReadSettings", ex, Console.LogID.Init);
            }

            try
            {
                // setup update service named pipe
                updateServiceNamedPipe.ServerMessage += handleUpdateServiceMessage;
                updateServiceNamedPipe.Error         += handleUpdateServiceError;
                updateServiceNamedPipe.Disconnected  += handleUpdateServiceDisconnected;

                updateServiceNamedPipe.Start();
            }
            catch(Exception ex)
            {
                StartupErrors += ex.Message + " from " + ex.Source;
                Logging.ExceptionLogger("Error in Device Startup - UpdateServiceNamedPipe", ex, Console.LogID.Init);
            }

            try
            {
                // check for updates
                checkForUpdates();

                string exePath = System.Windows.Forms.Application.ExecutablePath.Replace('\\', '/');
                int lastSlash  = exePath.LastIndexOf('/');
                string exeDir  = exePath.Substring(0, lastSlash);
                string exeName = exePath.Substring(lastSlash + 1);

                FileSystemWatcher updateWatcher = new FileSystemWatcher(exeDir, "*.exe");
                updateWatcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.Size;

                updateWatcher.Changed += new FileSystemEventHandler(exeChanged);
                updateWatcher.Created += new FileSystemEventHandler(exeChanged);

                // begin watching
                updateWatcher.EnableRaisingEvents = true;
            }
            catch (Exception ex)
            {
                StartupErrors += ex.Message + " from " + ex.Source;
                Logging.ExceptionLogger("Error in Device Startup - CheckForUpdates", ex, Console.LogID.Init);
            }

            try
            {
                XmlMessageServer = new XmlMessages(this, dataModel);
// TODO: remove this when we're sure that the Start in Console is working well
//                XmlMessageServer.Start(Convert.ToInt32(settings.GetSetting("ListenPort", "50021")));
//DebugWindowMsg("XmlMessageServer.Start");

                Position = Convert.ToUInt16(settings.GetSetting("Position", Position.ToString()));

                XmlMessageServer.SettingsReceived += new XmlMessages.OnSettingsReceived (XmlMessageServer_SettingsReceived);
                XmlMessageServer.XMLDataReceived  += new XmlMessages.OnXMLDataReceived  (XmlMessageServer_DataReceived);

                XmlMessageServer.AlarmMessage     += new XmlMessages.OnAlarmMessage     (XmlMessageServer_AlarmMessage);
                XmlMessageServer.NewActive        += new XmlMessages.OnNewActive        (XmlMessageServer_NewActive);
                XmlMessageServer.UpdatepsapEvent  += new XmlMessages.OnUpdatepsapEvent  (XmlMessageServer_UpdatepsapEvent);
                XmlMessageServer.MessageReceived  += new XmlMessages.OnMessageReceived  (XmlMessageServer_MessageReceived);
                XmlMessageServer.CheckUpdate      += new XmlMessages.OnCheckUpdate      (XmlMessageServer_CheckUpdate);
                XmlMessageServer.ResetTransfer    += new XmlMessages.OnResetTransfer    (XmlMessageServer_ResetTransfer);

                if (showDebugData)
                {
                    debugWindow = new DebugWindow();
                    debugWindow.Show();
                }
            }
            catch (Exception ex)
            {
                StartupErrors += ex.Message + " from " + ex.Source;
                Logging.ExceptionLogger("Error in Device Startup - ReadSettings", ex, Console.LogID.Init);
            }

            noForm.SystemTray.Text = Application.ProductName;

            // System.Windows.Forms.Application.DoEvents();

            // show system tray icon
            noForm.SystemTray.Visible = true;

            try
            {
                console = new Console(this, demoMode, debugMode, startRecordingDebugScript);
            }
            catch (Exception ex)
            {
                StartupErrors += ex.Message + " from " + ex.Source;
                Logging.ExceptionLogger("Error in Device Startup - Console", ex, Console.LogID.Init);
            }

            try
            {
                if (StartupErrors.Length > 0)
                {
                    MessageBox.Show(null, (Translate)("There were problems starting up ") + Application.ProductName + ".\n" +
                        (Translate)("You may need to exit the program and correct these problems before using this program.\n") +
                        (Translate)("Please contact ") + Application.CompanyName + (Translate)(" for support.\n") +
                        (Translate)("Error Information:\n") +
                        StartupErrors,
                        Application.ProductName,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error,
                        MessageBoxDefaultButton.Button1,
                        MessageBoxOptions.DefaultDesktopOnly);
                }
            }
            catch {}

            System.Windows.Forms.Application.Run();
            GC.KeepAlive(mutex);                // important!

            controller.HideSystemTray();
            System.Windows.Forms.Application.Exit();

            if (currentProcess != null) currentProcess.Kill();
        }

        private void handleUpdateServiceMessage(NamedPipeConnection<string, string> connection, string msg)
        {
            try {DebugWindowMsg("Found Update Service " + msg);} catch {}

            try
            {
                if (msg.StartsWith("Info:"))
                {
                    sendAlertToController(msg.Remove(0, "Info:".Length),
                        Console.LogType.Info, Console.LogID.Update);
                }
                else if (msg.StartsWith("Warn:"))
                {
                    sendAlertToController(msg.Remove(0, "Warn:".Length),
                        Console.LogType.Warning, Console.LogID.Update);
                }
                // TODO: remove this (deprecated)
                else if (msg.StartsWith("Log:"))
                {
                    sendAlertToController(msg.Remove(0, "Log:".Length),
                        Console.LogType.Warning, Console.LogID.Update);
                }
                else if (msg.StartsWith("Update:"))
                {
                    msg = msg.Remove(0, "Update:".Length);

                    showAlert((Translate)("An update has been installed.") +
                        (Translate)(" Please restart when convenient. (") + msg + ")",
                        Console.LogType.Info, Console.LogID.Update);
                }
                else
                {
                    sendAlertToController(msg, Console.LogType.Warning, Console.LogID.Update);
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger(
                    "Error while trying to access an message from the Update Service", ex, Console.LogID.Update);
            }
        }

        private static bool handlingUpdateServiceError = false;

        private void handleUpdateServiceError(Exception ex)
        {
            if (handlingUpdateServiceError) return;
            handlingUpdateServiceError = true;

            try
            {
                Logging.ExceptionLogger(
                    "Exception thrown by Update Service named pipe", ex, Console.LogID.Update);

                ensureUpdateServiceIsRunning();

                updateServiceNamedPipe.Stop();

                new System.Threading.Timer(updateServiceNamedPipe.Start, this, 5000, Timeout.Infinite);
            }
            catch {}

            handlingUpdateServiceError = false;
        }

        private void handleUpdateServiceDisconnected(NamedPipeConnection<string, string> connection)
        {
            if (handlingUpdateServiceError) return;
            handlingUpdateServiceError = true;

            try
            {
                sendAlertToController("Update Service named pipe was disconnected " +
                    "(will now check if the service is running)",
                    Console.LogType.Info, Console.LogID.Update);

                ensureUpdateServiceIsRunning();

                updateServiceNamedPipe.Stop();
                Thread.Sleep(1000);

                updateServiceNamedPipe.Start();
            }
            catch {}

            handlingUpdateServiceError = false;
        }

        Thread updateThread = null;

        public void checkForUpdates()
        {
            try   {if (updateThread != null) updateThread.Abort();}
            catch {}

            try
            {
DebugWindowMsg("About to start Check Update Service Thread");
                updateThread = new Thread(new ThreadStart(checkUpdateServiceThread));
                updateThread.Name = "Check Update Service Thread";
                updateThread.Start();
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger(
                    "Error while starting the check-update service thread", ex, Console.LogID.Update);
            }
        }

        private void checkUpdateServiceThread()
        {
DebugWindowMsg("Check Update Service Thread started");

            // check for updates only once when the thread is started or restarted
            bool checkForUpdates = true;

            while (true)
            {
                ensureUpdateServiceIsRunning(true);

                if (checkForUpdates)
                {
                    try
                    {
                        // check for updates only once when the thread is started or restarted
                        checkForUpdates = false;

                        if (updateServiceNamedPipe != null)
                        {
                            bool TESTING = false;

                            if (TESTING)
                            {
                                string uriStr = "C:/Dev/911/install/PSAP Toolkit 2.7.0.21 (XML 6.76) Setup.exe";

                                MessageBox.Show(null, "Checking for updates at " + uriStr,
                                    "**** TESTING ****", MessageBoxButtons.OK, MessageBoxIcon.Exclamation,
                                    MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);

                                updateServiceNamedPipe.PushMessage("CheckForUpdate:" + uriStr);
                            }
                            else
                            {
                                string serverIPAddress =
                                    settings.GetSetting("ServerIPAddress", "192.168.62.11");

                                string uriStr = "http://" + serverIPAddress + "/gui/setup-911.exe";

                                updateServiceNamedPipe.PushMessage("CheckForUpdate:" + uriStr);
                            }
                        }

                        Thread.Sleep(10000);
                    }
                    catch (Exception ex)
                    {
                        Logging.ExceptionLogger(
                            "Error while checking for updates (sending over named pipe)", ex, Console.LogID.Update);
                    }
                }

                Thread.Sleep(60000); // once a minute
            }
        }

        private static bool   checkingUpdateService = false;
        private static string statusStr             = "";

        private void ensureUpdateServiceIsRunning(bool autoConnectNamedPipe = false)
        {
            if (checkingUpdateService) return;
            checkingUpdateService = true;

            try
            {
                ServiceController updateService = new ServiceController("911UpdateService");

                if (updateService == null)
                {
                    checkingUpdateService = false;
                    return;
                }

DebugWindowMsg("Update Service Status: " + updateService.Status.ToString());

                // check status
                string newStatusStr = "";

                switch (updateService.Status)
                {
                    case ServiceControllerStatus.Running:      newStatusStr = "";        break;
                    case ServiceControllerStatus.Stopped:      newStatusStr = "Stopped"; break;
                    case ServiceControllerStatus.Paused:       newStatusStr = "Paused";  break;
                    case ServiceControllerStatus.StopPending:  newStatusStr = "Stopped"; break;
                    case ServiceControllerStatus.StartPending: newStatusStr = "";        break;
                }

                if ( ! newStatusStr.Equals(statusStr))
                {
                    statusStr = newStatusStr;

                    if (statusStr.Length != 0)
                    {
                        Controller.sendAlertToController("The update service is currently " +
                            statusStr + " (attempting to restart it)",
                            Console.LogType.Warning, Console.LogID.Update);

                        updateServiceNamedPipe.Stop();

                        try
                        {
                            Thread.Sleep(1000);

                            statusStr = "Unknown";

                            // attempt to restart it
                            updateService.Start();
                            updateService.WaitForStatus(ServiceControllerStatus.Running, new TimeSpan(0,0,2));

                            if (autoConnectNamedPipe)
                            {
                                Thread.Sleep(1000);
                                updateServiceNamedPipe.Start();
                            }
                        }
                        catch (Exception ex)
                        {
                            Logging.ExceptionLogger(
                                "Error while attempting to start the service", ex, Console.LogID.Update);
                        }
                    }
                    else
                    {
                        Controller.sendAlertToController("The update service is currently Running",
                            Console.LogType.Info, Console.LogID.Update);
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger(
                    "Error while checking if Update Service is running", ex, Console.LogID.Update);
            }

            checkingUpdateService = false;
        }

        private void exeChanged(object source, FileSystemEventArgs e)
        {
            Thread.Sleep(2500);

            string exePath = System.Windows.Forms.Application.ExecutablePath.Replace('\\', '/');

            // try to restart up to 10 times
            for (int i = 0; i != 10; ++i)
            {
                if (File.Exists(exePath))
                {
                    restartApplication(exePath);
                    return;
                }

                Thread.Sleep(1000);
            }
        }

        public static void restartApplication(string exePath)
        {
            try
            {
                if ( ! File.Exists(exePath))
                {
                    MessageBox.Show(null, (Translate)("Failed to restart ") +
                        (Translate)("(could not find the executable") + " \"" + exePath + "\")",
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information,
                        MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);

                    return;
                }
            }
            catch {}

            // kill this process and restart it
            string exeArgsStr = "";

            try
            {
                foreach (string s in exeArgs) exeArgsStr += s + " ";

                if (controller != null) controller.HideSystemTray();
            }
            catch {}

DebugWindowMsg("Attempting to restart the application...");
            Process.Start(exePath, exeArgsStr);

            System.Windows.Forms.Application.Exit();

            if (currentProcess != null) currentProcess.Kill();
        }

        public void colorsUpdated()
        {
            try
            {
                if (console      != null) console.      colorsUpdated();
                if (psapStatus   != null) psapStatus.   colorsUpdated();
                if (alarmDisplay != null) alarmDisplay. colorsUpdated();
                if (tddWindow    != null) tddWindow.    colorsUpdated();
                if (smsWindow    != null) smsWindow.    colorsUpdated();
                if (irrWindow    != null) irrWindow.    colorsUpdated();
                if (phonebook    != null) phonebook.    colorsUpdated();
                if (colorEditor  != null) colorEditor.  colorsUpdated();

                tbn1.colorsUpdated();
                tbn2.colorsUpdated();
                tbn3.colorsUpdated();
                tbn4.colorsUpdated();
                tbn5.colorsUpdated();
                tbn6.colorsUpdated();
                tbn7.colorsUpdated();
                tbn8.colorsUpdated();
            }
            catch {}
        }

        public void resetTransfer(string callId, bool successful)
        {
            try
            {
                if (phonebook != null) phonebook.reset(callId, successful);
            }
            catch {}
        }

        private void buildForms(object sender, EventArgs eventArgs)
        {
            string StartupErrors = "";

            try
            {
                dataModel.TDDStringReceived += new DataModel.OnTDDStringReceived (TDDStringReceived);
                dataModel.TDDdispatcherSays += new DataModel.OnTDDdispatcherSays (TDDDispatcherSays);
                dataModel.TDDCallerSays     += new DataModel.OnTDDCallerSays     (TDDCallerSays);
                dataModel.TDDMuteChanged    += new DataModel.OnTDDMuteChanged    (TDDMuteChanged);

                dataModel.SMSStringReceived += new DataModel.OnSMSStringReceived (SMSStringReceived);
                dataModel.SMSdispatcherSays += new DataModel.OnSMSdispatcherSays (SMSDispatcherSays);
                dataModel.SMSCallerSays     += new DataModel.OnSMSCallerSays     (SMSCallerSays);

                dataModel.RingingCallTimedOut += new DataModel.OnRingingCallTimedOut(RingingCallTimedOut);

                console.Initialize();

                // console.SettingsReceived += new Console.OnSettingsReceived(console_SettingsReceived);
                object nPositions = settings.GeneralSettings["NumberOfPositions"];

                if (nPositions != null) console.setNumberOfPositions(Convert.ToInt32(nPositions));

                console.setPosition(Position);

                console.SetupForm();
            }
            catch (Exception ex)
            {
                StartupErrors += ex.Message + " from " + ex.Source;
                Logging.ExceptionLogger("Error in Device Startup - Console", ex, Console.LogID.Init);
            }

            // enable Console, TDD, SMS, Transfer, PSAP Status, and Alerts
            noForm.menuTrayConsole.Enabled = noForm.menuTrayTDD.Enabled = noForm.menuTraySMS.Enabled =
                noForm.menuTrayColorEditor.Enabled = noForm.menuTrayPhonebook.Enabled =
                noForm.menuTrayPSAPStatus.Enabled = noForm.menuViewAlarms.Enabled = true;

            phonebook = new Phonebook(this);
            phonebook.SetupForm();

            TDDCPSThreshhold = 4;

            if (settings.TDDSettings.Contains("CPSThreshold"))
            {
                TDDCPSThreshhold = Convert.ToInt32(settings.TDDSettings["CPSThreshold"]);
            }

// TODO xml: remove "CallerSaysPrefix" in favor of "CallerSaysPattern"
if (settings.TDDSettings.Contains("CallerSaysPrefix"))
{
    if (tddWindow != null) tddWindow.setCallerSaysPattern(settings.TDDSettings["CallerSaysPrefix"].ToString());
}

// TODO xml: remove "CallerSaysPrefix" in favor of "CallerSaysPattern"
if (settings.SMSSettings.Contains("CallerSaysPrefix"))
{
    if (smsWindow != null) smsWindow.setCallerSaysPattern(settings.SMSSettings["CallerSaysPrefix"].ToString());
}

            if (settings.TDDSettings.Contains("CallerSaysPattern"))
            {
                if (tddWindow != null) tddWindow.setCallerSaysPattern(settings.TDDSettings["CallerSaysPattern"].ToString());
            }

            if (settings.TDDSettings.Contains("DispatcherSaysPattern"))
            {
                if (tddWindow != null) tddWindow.setDispatcherSaysPattern(settings.TDDSettings["DispatcherSaysPattern"].ToString());
            }
            
            if (settings.SMSSettings.Contains("CallerSaysPattern"))
            {
                if (smsWindow != null) smsWindow.setCallerSaysPattern(settings.SMSSettings["CallerSaysPattern"].ToString());
            }

            if (settings.SMSSettings.Contains("DispatcherSaysPattern"))
            {
                if (smsWindow != null) smsWindow.setDispatcherSaysPattern(settings.SMSSettings["DispatcherSaysPattern"].ToString());
            }
            
   	        try 
			{ 
				alarmDisplay = new AlarmDisplay(this); 
				alarmDisplay.SetupForm();
			}
			catch (Exception ex)
			{ 
				StartupErrors += ex.Message + " from " + ex.Source; 
				Logging.ExceptionLogger("Error in Device Startup - Alarm Display", ex, Console.LogID.Init);
			}
            
   	        try 
			{ 
				psapStatus = new PSAPStatus(this); 
				psapStatus.SetupForm();
			}
			catch (Exception ex)
			{ 
				StartupErrors += ex.Message + " from " + ex.Source; 
				Logging.ExceptionLogger("Error in Device Startup - PSAP Status", ex, Console.LogID.Init);
			}

            console.handleFormsBuilt();

            try
            {
                if (StartupErrors.Length > 0)
                {
                    MessageBox.Show(null, (Translate)("There were problems starting up ") + Application.ProductName + ".\n" +
                        (Translate)("You may need to exit the program and correct these problems before using this program.\n") +
                        (Translate)("Please contact ") + Application.CompanyName + (Translate)(" for support.\n") +
                        (Translate)("Error Information:\n") +
                        StartupErrors,
                        Application.ProductName,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error,
                        MessageBoxDefaultButton.Button1,
                        MessageBoxOptions.DefaultDesktopOnly);
                }

                bool updated = false;

                try
                {
                    // NOTE: use non-translated company name here
                    RegistryKey regSettings = Registry.CurrentUser.CreateSubKey(
                        "Software\\" + System.Windows.Forms.Application.CompanyName + "\\PSAP Toolkit");

                    if (regSettings != null)
                    {
                        object versionObj = regSettings.GetValue("Version");

                        if (versionObj != null && versionObj.ToString() != Application.ProductVersion)
                        {
                            updated = true;
                            showNotice((Translate)("Updated to Version ") + Application.ProductVersion + ".", false);
                        }

                        regSettings.SetValue("Version", Application.ProductVersion);
                    }
                }
                catch {}

                // if all notice windows are currently hidden, show the shortcut popup
                if ( ! updated &&
                    tbn1.TaskbarState == TaskbarNotifier.TaskbarStates.hidden &&
                    tbn2.TaskbarState == TaskbarNotifier.TaskbarStates.hidden &&
                    tbn3.TaskbarState == TaskbarNotifier.TaskbarStates.hidden &&
                    tbn4.TaskbarState == TaskbarNotifier.TaskbarStates.hidden &&
                    tbn5.TaskbarState == TaskbarNotifier.TaskbarStates.hidden &&
                    tbn6.TaskbarState == TaskbarNotifier.TaskbarStates.hidden &&
                    tbn7.TaskbarState == TaskbarNotifier.TaskbarStates.hidden &&
                    tbn8.TaskbarState == TaskbarNotifier.TaskbarStates.hidden)
                {
                    showNotice(
                        (Translate)("Use the shortcut keys or right-click on the system tray icon to open the appropriate window."),
                        false);
                }
            }
            catch (Exception)
            {
            }
        }

        private void tbn_ExitMessageReceived()
        {
            ExitApplication(this);
        }

#region XmlMessageServer Event Handlers

        private const int LOG_PREFIX = 44;

        // XML Data Received
        private void XmlMessageServer_DataReceived(string Data, bool fromSocket)
        {
            if (console != null && fromSocket) console.MsgReceived();

            if (Data != null && (logFile != null || debugWindow != null))
            {
                Data = Data.Replace("\n","\r\n");

                if ( ! fromSocket) DebugWindowMsg(Data + "\r\n" + "".PadLeft(LOG_PREFIX) + "-----------------internally-generated----------------\r\n");
                else               DebugWindowMsg(Data + "\r\n" + "".PadLeft(LOG_PREFIX) + "-----------------------------------------------------\r\n");
            }
        }

        // debug window message
        private static object debugMsgLock = new object();

        public static void DebugWindowMsg(string msg)
        {
            lock(debugMsgLock)
            {
                if (logFile != null || (controller != null && controller.debugWindow != null))
                {
                    string s;

                    try
                    {
                        Thread curThread     = Thread.CurrentThread;
                        string curThreadName = (curThread.Name != null) ? curThread.Name : "unnamedThread";

#pragma warning disable 618 // disable warning about GetCurrentThreadId() being deprecated in favor of ManagedThreadId

                        s = (curThreadName + "[" + curThread.ManagedThreadId.      ToString().PadLeft(2,'0') + "/" +
                                                   AppDomain.GetCurrentThreadId(). ToString().PadLeft(5,'0') + "]");

#pragma warning restore 618

                        s += DateTime.Now.ToString(" (HH:mm:ss.fff) -- ");
                        s = s.PadLeft(LOG_PREFIX);

                        s += msg;
                    }
                    catch {s = msg;}

                    try
                    {
                        if (controller != null && controller.console != null)
                        {
                            controller.console.BeginInvoke(new DebugWindowMsgDelegate(debugWindowMsg), s);
                            return;
                        }
                        else if (controller != null && controller.debugWindow != null)
                        {
                            controller.debugWindow.BeginInvoke(new DebugWindowMsgDelegate(debugWindowMsg), s);
                            return;
                        }
                    }
                    catch {}

                    try {debugWindowMsg(s);} catch{}
                }
            }
        }

        private delegate void DebugWindowMsgDelegate(string s);

        private static void debugWindowMsg(string s)
        {
            s += "\r\n";
            Trace.Write(s);

            if (logFile != null)
            {
                logFile.Write(Encoding.ASCII.GetBytes(s), 0, s.Length);
                logFile.Flush(true);
            }

            if (controller != null && controller.debugWindow != null)
            {
                controller.debugWindow.textBox.AppendText(s);
            }
        }

        public void XmlMessageServer_SettingsReceived(XmlDocument xd)
        {
            if (settingsReceived) return;

            settingsReceived = true;

            // Settings received from server, load, parse, and inform applets of settings.
            settings.SettingsReceived(xd, console, dataModel);

            Application.CompanyName = (Translate)(settings.GetSetting("CompanyName", Application.CompanyName));
            Application.ProductName = (Translate)(settings.GetSetting("ProductName", Application.ProductName));

            bool firewallEnabled = Console.toBoolean(settings.GetSetting("Firewall", "off"));

            if (firewallEnabled)
            {
                foreach (string serverIPStr in settings.ServerList)
                {
                    XmlMessageServer.addServerIPAddresses(Dns.GetHostAddresses(serverIPStr));
                }
            }

            if (console != null)
            {
                console.SettingsReceived(settings);

                // these must be Invokes so that messages don't arrive before connections are made
                console.Invoke(new EventHandler(buildForms), new object[]{this, null});
            }

            if (psapStatus != null)
            {
                object showPSAPAdminColObj = settings.GeneralSettings["ShowPSAPappletAdmin"];
                bool   showPSAPAdminCol    = (showPSAPAdminColObj != null ? Console.toBoolean(showPSAPAdminColObj) : false);

                if (showPSAPAdminCol) psapStatus.showAdminColumn(showPSAPAdminCol);
            }

            if (irrClient == null)
            {
                string serverIPAddress = settings.GetSetting("ServerIPAddress", "192.168.62.11");

                string irrHost = settings.GetSetting("IrrIPAddress", serverIPAddress);
                int    irrPort = Convert.ToInt32(settings.GetSetting("IrrPort", "6000"));

              #if DEBUG
                irrHost = "192.168.1.6";
              #endif

                int sendBufferSize = 65536; // 64 KiB
                int recvBufferSize = 65536; // 64 KiB

                irrClient = new IrrClient(irrHost, irrPort, sendBufferSize, recvBufferSize);
            }
        }

        public bool hasReceivedSettings() {return settingsReceived;}

        private void XmlMessageServer_MessageReceived(MessageInfo messageInfo)
        {
            if (console != null) console.CheckForRowUpdate(messageInfo);
        }

        private void XmlMessageServer_NewActive(string positions)
        {
            console.NewActive(positions);
        }

        private void XmlMessageServer_CheckUpdate()
        {
            controller.checkForUpdates();
        }

        private void XmlMessageServer_ResetTransfer(string callId, bool successful)
        {
            controller.resetTransfer(callId, successful);
        }

        // this has to go this round about way due to threading dead-locks
        private void XmlMessageServer_UpdatepsapEvent(psapEventMessage psapMessage)
        {
            try
            {
                console.BeginInvoke(new UpdatepsapEventDelegate(UpdatepsapEvent), psapMessage);
            }
            catch
            {
                try {UpdatepsapEvent(psapMessage);} catch{}
            }
        }

        private delegate void UpdatepsapEventDelegate(psapEventMessage psapMessage);

        private void UpdatepsapEvent(psapEventMessage psapMessage)
        {
            dataModel.UpdatepsapEvent(psapMessage);
        }

        private void XmlMessageServer_AlarmMessage(string text, string type)
        {
            try
            {
                console.BeginInvoke(new XmlMessageServerAlarmMessageDelegate(XmlMessageServerAlarmMessage), text, type);
            }
            catch
            {
                try {XmlMessageServerAlarmMessage(text, type);} catch{}
            }
        }

        private delegate void XmlMessageServerAlarmMessageDelegate(string text, string type);

        private bool inShutdownMessage = false;

        private void XmlMessageServerAlarmMessage(string text, string type)
        {
            switch (type)
            {
                case "Popup":
                {
                    showNotice((Translate)(text), true);

                    if (alarmDisplay != null) alarmDisplay.refresh();

                    break;
                }
                case "Shutdown":
                {
                    if (inShutdownMessage) return;
                    inShutdownMessage = true;

                    if (console != null) console.killConnection();

                    controller.HideSystemTray();

                    MessageBox.Show(null, (Translate)(text) + "\n\n" + Application.ProductName + (Translate)(" will now exit."),
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1,
                        MessageBoxOptions.DefaultDesktopOnly); // keep on top of all windows

                    System.Windows.Forms.Application.Exit();

                    if (currentProcess != null) currentProcess.Kill();

                    break;
                }
            }
        }

#endregion


        public static void ExitApplication(Controller c)
        {
            if ( ! c.OkToStop)
            {
                c.SystemShutdown = true;

                c.OkToStop = true;

                try
                {
                    if (c.tddWindow != null) c.OkToStop &= c.tddWindow.Stop();
                }
                catch (Exception ex)
                {
                    Logging.AppTrace("Error stopping TDD:" + ex.ToString());
                    Logging.ExceptionLogger("Error in ExitApplication (TDD)", ex, Console.LogID.Cleanup);
                }
                finally
                {
                    if (c.OkToStop) c.tddWindow = null;
                }

                try
                {
                    if (c.smsWindow != null) c.OkToStop &= c.smsWindow.Stop();
                }
                catch (Exception ex)
                {
                    Logging.AppTrace("Error stopping SMS:" + ex.ToString());
                    Logging.ExceptionLogger("Error in ExitApplication (SMS)", ex, Console.LogID.Cleanup);
                }
                finally
                {
                    if (c.OkToStop) c.smsWindow = null;
                }

                if (c.OkToStop)
                {
                    c.SystemShutdown = true;
                    Logging.AppTrace("Stopping " + Application.ProductName);

                    try
                    {
                        Logging.AppTrace("Stopping Message Server");
                        c.XmlMessageServer.Stop();
                        c.XmlMessageServer = null;

                        if (c.console != null) c.console.Stop(); // stops message server (listener)
                    }
                    catch (Exception ex)
                    {
                        Logging.AppTrace("Error stopping Console:" + ex.ToString());
                        Logging.ExceptionLogger("Error in ExitApplication (Console)", ex, Console.LogID.Cleanup);
                    }
                    finally {c.console = null;}

                    try
                    {
                        if (c.tddWindow != null) c.tddWindow.Stop();
                    }
                    catch (Exception ex)
                    {
                        Logging.AppTrace("Error stopping TDD:" + ex.ToString());
                        Logging.ExceptionLogger("Error in ExitApplication (TDD)", ex, Console.LogID.Cleanup);
                    }
                    finally {c.tddWindow = null;}

                    try
                    {
                        if (c.smsWindow != null) c.smsWindow.Stop();
                    }
                    catch (Exception ex)
                    {
                        Logging.AppTrace("Error stopping SMS:" + ex.ToString());
                        Logging.ExceptionLogger("Error in ExitApplication (SMS)", ex, Console.LogID.Cleanup);
                    }
                    finally {c.smsWindow = null;}

                    try
                    {
                        if (c.irrWindow != null) c.irrWindow.Stop();
                    }
                    catch (Exception ex)
                    {
                        Logging.AppTrace("Error stopping IRR:" + ex.ToString());
                        Logging.ExceptionLogger("Error in ExitApplication (IRR)", ex, Console.LogID.Cleanup);
                    }
                    finally {c.irrWindow = null;}

                    try
                    {
                        if (c.phonebook != null) c.phonebook.Stop();
                    }
                    catch (Exception ex)
                    {
                        Logging.AppTrace("Error stopping Phonebook:" + ex.ToString());
                        Logging.ExceptionLogger("Error in ExitApplication (Phonebook)", ex, Console.LogID.Cleanup);
                    }
                    finally
                    {
                        c.phonebook = null;
                    }

                    try
                    {
                        if (c.alarmDisplay != null) c.alarmDisplay.Stop();
                    }
                    catch (Exception ex)
                    {
                        Logging.AppTrace("Error stopping Alarm Display:" + ex.ToString());
                        Logging.ExceptionLogger("Error in ExitApplication (AlarmDisplay)", ex, Console.LogID.Cleanup);
                    }
                    finally
                    {
                        c.alarmDisplay = null;
                    }

                    try
                    {
                        if (c.psapStatus != null) c.psapStatus.Stop();
                    }
                    catch (Exception ex)
                    {
                        Logging.AppTrace("Error stopping PSAP Status:" + ex.ToString());
                        Logging.ExceptionLogger("Error in ExitApplication (PSAPStatus)", ex, Console.LogID.Cleanup);
                    }
                    finally
                    {
                        c.psapStatus = null;
                    }

                    try
                    {
                        if (c.colorEditor != null) c.colorEditor.Stop();
                    }
                    catch (Exception ex)
                    {
                        Logging.AppTrace("Error stopping Color Editor:" + ex.ToString());
                        Logging.ExceptionLogger("Error in ExitApplication (Color Editor)", ex, Console.LogID.Cleanup);
                    }
                    finally
                    {
                        c.colorEditor = null;
                    }

                    try
                    {
                        c.HideSystemTray();

                        // Application.DoEvents(); // finish up

                        foreach (System.Diagnostics.ProcessThread pt in Controller.currentProcess.Threads)
                        {
                            Logging.AppTrace("Disposing of Process Thread:" + pt.Id + " with ");
                            if (pt.Container == null)
                                Logging.AppTrace("no components");
                            else
                                Logging.AppTrace(pt.Container.Components.Count + " Components.");

                            try { pt.Dispose(); }
                            catch {}
                        }
                        Logging.AppTrace("Finished Disposing of Process Threads.");
                    }
                    catch (Exception ex)
                    {
                        Logging.AppTrace("Error stopping Application:" + ex.ToString());
                        Logging.ExceptionLogger("Error in ExitApplication (General)", ex, Console.LogID.Cleanup);
                    }

                    if (Controller.logFile != null)
                    {
                        try
                        {
                            string str = "\r\n\r\n--END LOG--\r\n";
                            Controller.logFile.Write(Encoding.ASCII.GetBytes(str), 0, str.Length);
                            Controller.logFile.Close();
                        }
                        catch {}
                    }

                    // this.Close();

                    System.Windows.Forms.Application.Exit();

                    if (Controller.currentProcess != null) Controller.currentProcess.Kill();
                }
            }
        }

        private static void ad_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Controller.UnhandledException(e);
        }

        public static void UnhandledException(UnhandledExceptionEventArgs e)
        {
            Exception ex = (Exception)e.ExceptionObject;
            Logging.AppTrace("UnhandledException");
            Logging.ExceptionLogger("UnhandledException", ex, Console.LogID.Gui);

            MessageBox.Show(null,
                (Translate)("An error has occured and ") + Application.ProductName + (Translate)(" will now exit."),
                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning,
                MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);

            if (Controller.controller != null) Controller.controller.HideSystemTray();

            System.Windows.Forms.Application.Exit();

            Process current = Process.GetCurrentProcess();
            if (current != null) current.Kill();

            // Controller.ExitApplication();
        }

        private static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            Logging.AppTrace("Unhandled Thread Exception");
            Logging.ExceptionLogger("Application_ThreadException", e.Exception, Console.LogID.Gui);
        }

        public void Shutdown()
        {
            DialogResult result = MessageBox.Show(null,
                (Translate)("Are you sure you want to exit ") + Application.ProductName + "?",
                Application.ProductName,
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question,
#if DEBUG
                MessageBoxDefaultButton.Button1,
#else
                MessageBoxDefaultButton.Button2,
#endif
                MessageBoxOptions.DefaultDesktopOnly);

            if (result == DialogResult.Yes) ExitApplication(this);
        }

        private void ShowForm(Form form)
        {
            if (form == null) return;

            try   { form.BeginInvoke(new ShowFormDelegate(showForm), form); }
            catch { try {showForm(form);} catch{} }
        }

        private delegate void ShowFormDelegate(Form form);

        private void showForm(Form form)
        {
            form.Show();

            if (form.WindowState == FormWindowState.Minimized)
            {
                form.WindowState = FormWindowState.Normal;
            }

            form.Activate();
        }

        public void ShowConsole()
        {
            ShowConsole(false);
        }

        public void ShowConsole(bool FixFocus)
        {
            if ( ! noForm.menuTrayConsole.Enabled) return;

            ShowForm(console);
            console.Activate();

            if (FixFocus)
            {
                System.Windows.Forms.Application.DoEvents();
                console.FixFocus();
            }
            /*
            console.Show();
            console.WindowState = System.Windows.Forms.FormWindowState.Normal;
            console.BringToFront();	
             */
        }
        
        public void ShowLog()
        {
            if (logFile != null)
            {
                logFile.Flush(true);

                System.Diagnostics.Process.Start("explorer.exe", "/select, \"" + logFile.Name + "\"");
            }
        }

        public void ShowXML()
        {
            if (debugWindow != null)
            {
                debugWindow.Show();
                debugWindow.WindowState = FormWindowState.Normal;
                debugWindow.BringToFront();
            }
        }

        public void ShowTDD()
        {
            if ( ! noForm.menuTrayTDD.Enabled) return;

            string callId = "";

            if (console != null)
            {
                DataRowView row = console.getCurrentRow();

                if (row != null) callId = row["Call_ID"].ToString();
            }

            getTDD().setCurrentCallId(callId);
            ShowForm(getTDD());
        }

        public void ShowSMS()
        {
            if ( ! noForm.menuTraySMS.Enabled) return;

            string callId = "";

            if (console != null)
            {
                DataRowView row = console.getCurrentRow();

                if (row != null) callId = row["Call_ID"].ToString();
            }

            getSMS().setCurrentCallId(callId);
            ShowForm(getSMS());
        }

        public void ShowIRR()
        {
            DataRowView row = null;

            bool isActiveCall = false;

            if (console != null)
            {
                row = console.getActiveCall(Console.ActiveIncludes.OnHold);

                isActiveCall = (row != null);

                if ( ! isActiveCall) row = console.getCurrentRow();
            }

            getIRR().setCurrentCallId(row, isActiveCall);
            ShowForm(getIRR());
        }

        public void ShowPhonebook()
        {
            if ( ! noForm.menuTrayPhonebook.Enabled) return;

            Phonebook.reset();

            if (phonebook != null) ShowForm(phonebook);
        }
        public void ShowColorEditor()
        {
            if ( ! noForm.menuTrayColorEditor.Enabled) return;

            ShowForm(getColorEditor());
        }

        public void HideSystemTray()
        {
            noForm.SystemTray.Visible = false; // forces icon to go away
        }

        public MessageWindow getTDD()
        {
            if (tddWindow == null)
            {
                try
                {
                    tddWindow = new MessageWindow(this, MessageWindow.WindowType.TDD);
                    tddWindow.SetupForm();

                    // load settings

                    // Deprecated settings.TDDSettings.Contains("SendTextColor")
                    // Deprecated settings.TDDSettings.Contains("ReceivedTextColor")

// TODO xml: remove "CallerSaysPrefix" in favor of "CallerSaysPattern"
if (settings.TDDSettings.Contains("CallerSaysPrefix"))
{
    if (tddWindow != null) tddWindow.setCallerSaysPattern(settings.TDDSettings["CallerSaysPrefix"].ToString());
}

                    if (settings.TDDSettings.Contains("CallerSaysPattern"))
                    {
                        if (tddWindow != null) tddWindow.setCallerSaysPattern(settings.TDDSettings["CallerSaysPattern"].ToString());
                    }

                    if (settings.TDDSettings.Contains("DispatcherSaysPattern"))
                    {
                        if (tddWindow != null) tddWindow.setDispatcherSaysPattern(settings.TDDSettings["DispatcherSaysPattern"].ToString());
                    }
                }
                catch (Exception ex)
                {
                    Logging.ExceptionLogger("Error in Device Startup - TDD", ex, Console.LogID.Init);
                }
            }

            return tddWindow;
        }

        public MessageWindow getSMS()
        {
            if (smsWindow == null)
            {
                try
                {
                    smsWindow = new MessageWindow(this, MessageWindow.WindowType.SMS);
                    smsWindow.SetupForm();

                    // load settings
                    // Deprecated settings.SMSSettings.Contains("SendTextColor")
                    // Deprecated settings.SMSSettings.Contains("ReceivedTextColor")

// TODO xml: remove "CallerSaysPrefix" in favor of "CallerSaysPattern"
if (settings.SMSSettings.Contains("CallerSaysPrefix"))
{
    if (smsWindow != null) smsWindow.setCallerSaysPattern(settings.SMSSettings["CallerSaysPrefix"].ToString());
}

                    if (settings.SMSSettings.Contains("CallerSaysPattern"))
                    {
                        if (smsWindow != null) smsWindow.setCallerSaysPattern(settings.SMSSettings["CallerSaysPattern"].ToString());
                    }

                    if (settings.SMSSettings.Contains("DispatcherSaysPattern"))
                    {
                        if (smsWindow != null) smsWindow.setDispatcherSaysPattern(settings.SMSSettings["DispatcherSaysPattern"].ToString());
                    }
                }
                catch (Exception ex)
                {
                    Logging.ExceptionLogger("Error in Device Startup - SMS", ex, Console.LogID.Init);
                }
            }

            return smsWindow;
        }

        public IRRWindow getIRR()
        {
            if (irrWindow == null)
            {
                try
                {
                    irrWindow = new IRRWindow(this);
                    irrWindow.SetupForm();
                }
                catch (Exception ex)
                {
                    Logging.ExceptionLogger("Error in Device Startup - IRR", ex, Console.LogID.Init);
                }
            }

            return irrWindow;
        }

        public ColorEditor getColorEditor()
        {
            if (colorEditor == null)
            {
                try
                {
                    colorEditor = new ColorEditor(this);
                    colorEditor.SetupForm();

                    // populate the tree view
                    settings.load_colors_xml();
                }
                catch (Exception ex)
                {
                    Logging.ExceptionLogger("Error in Device Startup - Color Editor", ex, Console.LogID.Init);
                }
            }

            return colorEditor;
        }

        public void Callback()
        {
            try   {if (console != null) console.callbackButtonClick(console, null);}
            catch {}
        }

        public void Connect()
        {
            try   {if (console != null) console.connectButtonClick(console, null);}
            catch {}
        }

        public void Barge()
        {
            try   {if (console != null) console.bargeButtonClick(console, null);}
            catch {}
        }

        public void FlashHook()
        {
            try   {if (console != null) console.flashHookButtonClick(console, null);}
            catch {}
        }

        public void Hold()
        {
            try   {if (console != null) console.holdCheckedChanged(console, null);}
            catch {}
        }

        public void Disconnect()
        {
            try   {if (console != null) console.disconnectButtonClick(console, null);}
            catch {}
        }

        public void Abandon()
        {
            try   {if (console != null) console.abandonedButtonClick(console, null);}
            catch {}
        }

        public void Rebid()
        {
            try
            {
                if (console != null && console.getCurrentRow() != null)
                {
                    Rebid(console.getCurrentRow()["Call_ID"].ToString());
                }
            }
            catch {}
        }

        public void Rebid(string callId)
        {
            try
            {
                //FileStream   fileStream = File.OpenRead    ("C:\\Users\\elliott\\Downloads\\song.wav");
                //BinaryReader reader     = new BinaryReader (fileStream);

                //uint chunkId       = reader.ReadUInt32(); // 0x46464952 (RIFF)
                //uint fileSize      = reader.ReadUInt32(); // 0x00f14b2a (15,813,418 bytes)
                //uint riffType      = reader.ReadUInt32(); // 0x45564157 (WAVE)
                //uint fmtId         = reader.ReadUInt32(); // 0x20746d66 (fmt )
                //uint fmtSize       = reader.ReadUInt32(); // 0x00000010 (16 bytes)
                //uint audioFormat   = reader.ReadUInt16(); // 0x0001     (1 = PCM, Linear quantization)
                //uint numChannels   = reader.ReadUInt16(); // 0x0002     (2ch)
                //uint sampleRate    = reader.ReadUInt32(); // 0x00003e80 (16000)
                //uint byteRate      = reader.ReadUInt32(); // 0x0000fa00 (64000)
                //uint blockAlign    = reader.ReadUInt16(); // 0x0004     (4)
                //uint bitsPerSample = reader.ReadUInt16(); // 0x0010     (16)
                //int  fmtExtraSize  = 0;

                //byte[] fmtExtraBytes;

                //if (fmtSize == 18)
                //{
                //    // Read any extra values
                //    fmtExtraSize  = reader.ReadInt16();
                //    fmtExtraBytes = reader.ReadBytes(fmtExtraSize);
                //}

                //uint subChunkId = reader.ReadUInt32();

                //while (subChunkId != 0x61746164) // while not 'data'
                //{
                //    int subChunkSize  = reader.ReadInt32();
                //    reader.ReadBytes(subChunkSize);

                //    subChunkId = reader.ReadUInt32();
                //}

                //int dataSize = reader.ReadInt32(); // 0x00f14ae4 (15,813,348)

                //byte[] data = reader.ReadBytes(dataSize);

                //fileStream.Close();



                //byte[] soundBytes = File.ReadAllBytes("C:\\Users\\elliott\\Downloads\\song.wav");

                //int _numChannels   = BitConverter.ToInt16(soundBytes, 22);
                //int _sampleRate    = BitConverter.ToInt32(soundBytes, 24);
                //int _byteRate      = BitConverter.ToInt32(soundBytes, 28);
                //int _bitsPerSample = BitConverter.ToInt16(soundBytes, 34);

                //// change rate and pitch
                //float rate       = 0.5f;
                //float pitchRatio = 1.0f / rate;

                //int newSampleRate = Convert.ToInt32(_sampleRate  * rate + 0.5f);
                //int newByteRate   = newSampleRate * _numChannels * _bitsPerSample/8;

                //Array.Copy(BitConverter.GetBytes(newSampleRate), 0, soundBytes, 24, 4);
                //Array.Copy(BitConverter.GetBytes(newByteRate),   0, soundBytes, 28, 4);

                //Mike.Rules.WavFile.Start(ref soundBytes, newSampleRate, pitchRatio);

                //using (System.Media.SoundPlayer SP = new System.Media.SoundPlayer(new MemoryStream(soundBytes)))
                //{
                //    SP.PlaySync();
                //}
            }
            catch {}

            if (console != null && console.isRebidEnabled())
            {
                console.sendToController("ALIRebid", callId);
                console.subdueALIWindow(true);
            }
        }

        public void SendToCAD(DataRowView row)
        {
            string optionalAttributes = " Encoding_ALI=\"" + row["Encoding_ALI"].ToString() + "\" ALIRecord=\"" + row["ALIRecord"].ToString() + "\"";

            console.sendToController("SendToCAD", row["Call_ID"].ToString(), "", optionalAttributes);
        }

        private static int cadMsgId = 1;

        public void SendEraseToCAD()
        {
            string msg = console.getXMLHeader("CAD", cadMsgId++) + "<SendEraseToCAD /></Event>";

            console.sendToController(msg);
        }

        private static int radioMsgId = 1;

        public void SendRadioContactClosure(string state)
        {
            string msg = console.getXMLHeader("Radio", radioMsgId++) +
                "<RadioContactClosure SetState=\"" + state + "\" /></Event>";

            console.sendToController(msg);
        }

        public void ManualBid()
        {
            try
            {
                if (manualALIRequestDialog == null)
                {
                    manualALIRequestDialog = new ManualALIRequest(this);
                    manualALIRequestDialog.FormClosed += new FormClosedEventHandler(manualALIRequestDialog_FormClosed);
                }

                if ( ! manualALIRequestDialog.Visible) manualALIRequestDialog.Show(console);
                else                                   manualALIRequestDialog.BringToFront();
            }
            catch (Exception ex)
            {
                Controller.showAlert((Translate)("Failed to perform Manual ALI Request (") + ex.Message + ")",
                    Console.LogType.Warning, Console.LogID.Connection);
            }
        }

        private static int aliMsgId = 1;

        private void manualALIRequestDialog_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                DialogResult result = manualALIRequestDialog.DialogResult;

                if (result == DialogResult.OK)
                {
                    string phoneNumber = manualALIRequestDialog.phoneField.Text;
                    phoneNumber = phoneNumber.Replace("(", "");
                    phoneNumber = phoneNumber.Replace(")", "");
                    phoneNumber = phoneNumber.Replace(" ", "");
                    phoneNumber = phoneNumber.Replace("-", "");
                    phoneNumber = phoneNumber.Replace(".", "");

                    string aliManualBidMessage =
                        console.getXMLHeader("Ali", aliMsgId++) + "<ManualALIBid" +
                        " Position=\""   + Position.ToString().PadLeft(2, '0') +
                        "\" Callback=\"" + phoneNumber + "\" /></Event>";

                    console.sendToController(aliManualBidMessage);
                }

                manualALIRequestDialog = null;
            }
            catch (Exception ex)
            {
                Controller.showAlert((Translate)("Failed to perform Manual ALI Request (") + ex.Message + ")",
                    Console.LogType.Warning, Console.LogID.Connection);
            }
        }

        public void MapALI(string callId)
        {
            console.sendToController("MapALI", callId);
        }

        public enum TransferType {Tandem, Attended, Conference, Blind, FlashHook, Dial};

        public void TransferTo(string transferToNumber, TransferType transferType)
        {
            // TODO** calls: InitiateTransfer is using current row, should it use active call
            console.sendTransferToController("InitiateTransfer", console.getCurrentRow(), transferToNumber, transferType);
        }

        public void CancelTransfer(string transferNumber, TransferType transferType)
        {
            DataRowView row = console.getCurrentRow();

            if (row == null) return;

            console.sendTransferToController("CancelTransfer", row, transferNumber, transferType);
        }

        public void CompleteTransfer(string transferToNumber)
        {
            DataRowView row = console.getCurrentRow();

            if (row == null) return;

            string optionalAttributes = " TransferToNumber=\"" + transferToNumber.ToString() + "\"";

            console.sendToController("ActivateAttendedTransfer", row["Call_ID"].ToString(), "", optionalAttributes);
        }

        public void sendTDDStringToController(string callId, string msgString)
        {
            console.sendTDDStringToController("TDDSendString", callId, msgString);
        }

        public void sendSMSStringToController(string callId, string smsString)
        {
            string optionalAttributes = " Text=\"" + System.Security.SecurityElement.Escape(smsString) + "\"";

            console.sendToController("SMSsendString", callId, "", optionalAttributes);
        }

        public void sendToController(string messageName, string callId)
        {
            console.sendToController(messageName, callId);
        }
        
        public void TDDChangeMute(string callId, bool mute)
        {
            console.sendToController("TDDchangeMute", callId);
        }
        
        public void sendDialCommand(string numberToDial)
        {
            console.sendDialToController(numberToDial);
        }
        
        public void sendAnswerCommand()
        {
            console.sendAnswerOrHoldToController("Answer", console.getCurrentRow());
        }

        public void sendFlashHookCommand()
        {
            console.sendFlashHookToController(console.getCurrentRow());
        }

        public void sendHoldCommand()
        {
            console.sendAnswerOrHoldToController("Hold", console.getCurrentRow());
        }

        public void sendPickupCommand() // TODO calls: when should this be called?
        {
            console.sendPickupToController("Pickup");
        }

        public void sendHangupCommand()
        {
            console.sendHangupToController("Hangup", console.getCurrentRow());
        }

        private void notificationShow(string text, bool playSound)
        {
            try
            {
                bool tbn1Hidden = tbn1.TaskbarState == TaskbarNotifier.TaskbarStates.hidden;
                bool tbn2Hidden = tbn2.TaskbarState == TaskbarNotifier.TaskbarStates.hidden;
                bool tbn3Hidden = tbn3.TaskbarState == TaskbarNotifier.TaskbarStates.hidden;
                bool tbn4Hidden = tbn4.TaskbarState == TaskbarNotifier.TaskbarStates.hidden;
                bool tbn5Hidden = tbn5.TaskbarState == TaskbarNotifier.TaskbarStates.hidden;
                bool tbn6Hidden = tbn6.TaskbarState == TaskbarNotifier.TaskbarStates.hidden;
                bool tbn7Hidden = tbn7.TaskbarState == TaskbarNotifier.TaskbarStates.hidden;
                bool tbn8Hidden = tbn8.TaskbarState == TaskbarNotifier.TaskbarStates.hidden;

                // if any are hidden (or 1 is not far right), move the visible ones
                if (tbn1Hidden || tbn2Hidden || tbn3Hidden || tbn4Hidden ||
                    tbn5Hidden || tbn6Hidden || tbn7Hidden || tbn8Hidden ||
                    tbn1.Right < TaskbarNotifier.screen.WorkingArea.Right - tbn1.Width)
                {
                    if ( ! tbn1Hidden) tbn1.MoveOver();
                    if ( ! tbn2Hidden) tbn2.MoveOver();
                    if ( ! tbn3Hidden) tbn3.MoveOver();
                    if ( ! tbn4Hidden) tbn4.MoveOver();
                    if ( ! tbn5Hidden) tbn5.MoveOver();
                    if ( ! tbn6Hidden) tbn6.MoveOver();
                    if ( ! tbn7Hidden) tbn7.MoveOver();
                    if ( ! tbn8Hidden) tbn8.MoveOver();
                }

                const int animateUpMS   = 500;
                const int showMS        = 15000;
                const int animateDownMS = 1000;

                if      (tbn1Hidden) tbn1.Show(Application.ProductName, text, animateUpMS, showMS, animateDownMS);
                else if (tbn2Hidden) tbn2.Show(Application.ProductName, text, animateUpMS, showMS, animateDownMS);
                else if (tbn3Hidden) tbn3.Show(Application.ProductName, text, animateUpMS, showMS, animateDownMS);
                else if (tbn4Hidden) tbn4.Show(Application.ProductName, text, animateUpMS, showMS, animateDownMS);
                else if (tbn5Hidden) tbn5.Show(Application.ProductName, text, animateUpMS, showMS, animateDownMS);
                else if (tbn6Hidden) tbn6.Show(Application.ProductName, text, animateUpMS, showMS, animateDownMS);
                else if (tbn7Hidden) tbn7.Show(Application.ProductName, text, animateUpMS, showMS, animateDownMS);
                else if (tbn8Hidden) tbn8.Show(Application.ProductName, text, animateUpMS, showMS, animateDownMS);
                else
                {
                    tbn1.Hide();
                    tbn1.Show(Application.ProductName, text, animateUpMS, showMS, animateDownMS);
                }

                if (playSound) System.Media.SystemSounds.Asterisk.Play();
            }
            catch {}
        }

        private delegate void NotificationShowDelegate(string Text, bool playSound);
	
        public void showNotice(string text, bool playSound)
        {
            try
            {
                if (console != null)
                {
                    console.BeginInvoke(new NotificationShowDelegate(notificationShow), new object[] {text, playSound});
                }
                else
                {
                    notificationShow(text, playSound);
                }
            }
            catch {}
        }

        private int    TDDCharWindow        = 4;
        private int    TDDCPSThreshhold     = 4;
        private int    TDDCharCount         = 0;
        private double TDDFirstCharReceived = 0.0;

        private void TDDStringReceived(string callId, string str)
        {
            try   { console.BeginInvoke(new TDD_StringReceivedDelegate(TDD_StringReceived), callId, str); }
            catch { try {TDD_StringReceived(callId, str);} catch{} }
        }

        private delegate void TDD_StringReceivedDelegate(string callId, string str);

        private void TDD_StringReceived(string callId, string str)
        {
            if ( ! getTDD().Visible)
            {
                DataRowView row = console.getActiveCall(Console.ActiveIncludes.OffHold);

                bool forTheActiveCall = (row != null && row["Call_ID"].ToString() == callId);

                if (forTheActiveCall)
                {
                    long   nowTick = DateTime.Now.Ticks;
                    double nowSec  = nowTick / 10000000.0;

                    Logging.AppTrace("NowSec  = " + nowSec);

                    // if too long, reset character count
                    if (nowSec > TDDFirstCharReceived + TDDCharWindow)
                    {
                        TDDCharCount         = 0;
                        TDDFirstCharReceived = nowSec;
                    }

                    TDDCharCount += str.Length;

                    if (TDDCharCount >= TDDCPSThreshhold)
                    {
                        ShowTDD();

                        TDDFirstCharReceived = 0.0;
                    }
                }
            }

            getTDD().StringReceived(callId, str);
        }

        private void TDDDispatcherSays(string callId, string str)
        {
            try   { console.BeginInvoke(new TDD_DispatcherSaysDelegate(TDD_DispatcherSays), callId, str); }
            catch { try {TDD_DispatcherSays(callId, str);} catch{} }
        }

        private delegate void TDD_DispatcherSaysDelegate(string callId, string str);

        private void TDD_DispatcherSays(string callId, string str)
        {
            getTDD().DispatcherSays(callId, str);
        }

        private void TDDCallerSays(string callId, string str)
        {
            try   { console.BeginInvoke(new TDD_CallerSaysDelegate(TDD_CallerSays), callId, str); }
            catch { try {TDD_CallerSays(callId, str);} catch{} }
        }

        private delegate void TDD_CallerSaysDelegate(string callId, string str);

        private void TDD_CallerSays(string callId, string str)
        {
            getTDD().CallerSays(callId, str);
        }

        private void TDDMuteChanged(string callId, bool mute)
        {
            try   { console.BeginInvoke(new TDD_MuteChangedDelegate(TDD_MuteChanged), callId, mute); }
            catch { try {TDD_MuteChanged(callId, mute);} catch{} }
        }

        private delegate void TDD_MuteChangedDelegate(string callId, bool mute);

        private void TDD_MuteChanged(string callId, bool mute)
        {
            getTDD().MuteChanged(callId, mute);
        }

        private void SMSStringReceived(string callId, string str)
        {
            try   { console.BeginInvoke(new SMS_StringReceivedDelegate(SMS_StringReceived), callId, str); }
            catch { try {SMS_StringReceived(callId, str);} catch{} }
        }

        private delegate void SMS_StringReceivedDelegate(string callId, string str);

        private void SMS_StringReceived(string callId, string str)
        {
            if ( ! getSMS().Visible)
            {
                DataRowView row = console.getActiveCall(Console.ActiveIncludes.OffHold);

                bool forTheActiveCall = (row != null && row["Call_ID"].ToString() == callId);

                // TODO calls: figure out how to pop up SMS when a text comes in
                if (forTheActiveCall) ShowSMS();
            }

            getSMS().StringReceived(callId, str);
        }

        private void SMSDispatcherSays(string callId, string str)
        {
            try   { console.BeginInvoke(new SMS_DispatcherSaysDelegate(SMS_DispatcherSays), callId, str); }
            catch { try {SMS_DispatcherSays(callId, str);} catch{} }
        }

        private delegate void SMS_DispatcherSaysDelegate(string callId, string str);

        private void SMS_DispatcherSays(string callId, string str)
        {
            getSMS().DispatcherSays(callId, str);
        }

        private void SMSCallerSays(string callId, string str)
        {
            try   { console.BeginInvoke(new SMS_CallerSaysDelegate(SMS_CallerSays), callId, str); }
            catch { try {SMS_CallerSays(callId, str);} catch{} }
        }

        private delegate void SMS_CallerSaysDelegate(string callId, string str);

        private void SMS_CallerSays(string callId, string str)
        {
            getSMS().CallerSays(callId, str);
        }

        private void RingingCallTimedOut(DataRow row)
        {
            try   { console.BeginInvoke(new RingingCall_TimedOutDelegate(RingingCall_TimedOut), row); }
            catch { try {RingingCall_TimedOut(row);} catch{} }
        }

        private delegate void RingingCall_TimedOutDelegate(DataRow row);

        private void RingingCall_TimedOut(DataRow row)
        {
            if (row == null) return;

            string callData = "<CallData Call-ID=\"" + row["Call_ID"].ToString() + "\" />";

            // process an empty message so that the ringing will be stopped if there are no other ringing calls
            controller.XmlMessageServer.ProcessMessage(
                "<?xml version=\"1.0\" encoding=\"utf-8\"?><Event id=\"\" XMLspec=\"" +
                XMLMessage.SPEC_VERSION + "\" ><CallInfo> " + callData + " </CallInfo></Event>");
        }

        public void ViewAlarms()
        {
            if ( ! noForm.menuViewAlarms.Enabled || alarmDisplay == null) return;

            ShowForm(alarmDisplay);

            //alarmDisplay.WindowState = FormWindowState.Normal;
            //alarmDisplay.Show();
            //alarmDisplay.BringToFront();
        }

        public void ViewPSAPStatus()
        {
            if ( ! noForm.menuTrayPSAPStatus.Enabled || psapStatus == null) return;

            ShowForm(psapStatus);
        }

        public void updatePSAPStatus()
        {
            if (psapStatus != null) psapStatus.refresh();
        }

        public void clearConsole()
        {
            if (dataModel != null)
            {
DataModel.logLockInfo(true, DataModel.callEventsSyncRoot);
                lock (DataModel.callEventsSyncRoot)
                {
                    try
                    {
                        dataModel.ds.Tables["CallEvents"].Clear();
                        Debug.WriteLine("Clearing Console CallEvents");
                    }
                    catch {}
                }
DataModel.logLockInfo(false, DataModel.callEventsSyncRoot);

DataModel.logLockInfo(true, DataModel.alarmEventsSyncRoot);
                lock (DataModel.alarmEventsSyncRoot)
                {
                    try
                    {
                        dataModel.ds.Tables["AlarmEvents"].Clear();

                        Debug.WriteLine("Clearing Console AlarmEvents");
                    }
                    catch {}
                }
DataModel.logLockInfo(false, DataModel.alarmEventsSyncRoot);
            }

            if (tddWindow != null) tddWindow.setCurrentCallId(null);
            if (smsWindow != null) smsWindow.setCurrentCallId(null);
            if (irrWindow != null) irrWindow.setCurrentCallId(null, false);
        }

        static bool inAlert = false; // prohibit recursive calls to alerts

        public static void sendAlertToController(string text, Console.LogType logType, Console.LogID logId)
        {
            if (inAlert) return;
            inAlert = true;

            try
            {
                if (controller != null && controller.console != null)
                {
#if DEBUG
                    showAlert(text, logType, logId);
#endif
                    bool sent = controller.console.sendLogMsgToController(logType, logId, text, false, null);

                    if (sent)
                    {
                        inAlert = false;
                        return;
                    }
                }
            }
            catch {}

            saveAlertToSendLater(text, logType, logId);

            inAlert = false;
        }

        public static void showAlert(string text, Console.LogType logType, Console.LogID logId)
        {
            if (inAlert) return;

            inAlert = true;

            try
            {
                if (controller != null && controller.console != null)
                {
                    controller.console.BeginInvoke(new ShowAlertDelegate(ShowAlert), text, logType, logId);
                    inAlert = false;
                    return;
                }
            }
            catch {}

            try {ShowAlert(text, logType, logId);} catch{}

            inAlert = false;
        }

        private delegate void ShowAlertDelegate(string text, Console.LogType logType, Console.LogID logId);

        private static void ShowAlert(string text, Console.LogType logType, Console.LogID logId)
        {
            try
            {
                if (controller != null && controller.dataModel != null)
                {
                    inAlert = true;

                    controller.dataModel.AddAlarm(DateTime.Now.ToLocalTime().ToShortTimeString(), text);
                    controller.showNotice(text, true);

                    bool sent = false;

                    if (controller.console != null)
                    {
                        sent = controller.console.sendLogMsgToController(logType, logId, text, false, null);
                    }

                    inAlert = false;

                    if (sent) return;
                }
            }
            catch {}

            saveAlertToSendLater(text, logType, logId);
        }

        private class AlertObj
        {
            public AlertObj(string _text, Console.LogType _logType, Console.LogID _logId)
            {
                text    = _text;
                logType = _logType;
                logId   = _logId;
                count   = 1;
            }

            public string          text;
            public Console.LogType logType;
            public Console.LogID   logId;
            public int             count;
        }

        private static List<AlertObj> alertList = new List<AlertObj>();

        private static void saveAlertToSendLater(string text, Console.LogType logType, Console.LogID logId)
        {
            foreach (AlertObj alertObj in alertList)
            {
                if (alertObj.text.Equals(text))
                {
                    ++alertObj.count;
                    return;
                }
            }

            alertList.Add(new AlertObj(text, logType, logId));
        }

        private static bool inSendSavedAlerts = false;

        public static void sendSavedAlerts()
        {
            if (alertList.Count != 0 && ! inSendSavedAlerts)
            {
                inSendSavedAlerts = true;

                try
                {
                    for (int i = 0; i != alertList.Count; ++i)
                    {
                        AlertObj alertObj = alertList[i];

                        bool sent = false;

                        if (alertObj.count == 1)
                        {
                            sent = controller.console.sendLogMsgToController(
                                alertObj.logType, alertObj.logId, alertObj.text, false, null);
                        }
                        else
                        {
                            sent = controller.console.sendLogMsgToController(
                                alertObj.logType, alertObj.logId,
                                alertObj.text + " [count=" + alertObj.count.ToString() + "]",
                                false, null);
                        }

                        if ( ! sent) break;

                        // send up to 30 saved messages (20 if there are over 30), then drop the rest
                        if (i == 20 && alertList.Count > 30)
                        {
                            controller.console.sendLogMsgToController(
                                Console.LogType.Info, Console.LogID.Connection,
                                "Dropping " + alertList.Count.ToString() + " unsent messages",
                                false, null);

                            break;
                        }
                    }
                }
                catch {}

                alertList.Clear();

                inSendSavedAlerts = false;
            }
        }

        public static SortedDictionary<ushort, string> lineNumberDictionary = new SortedDictionary<ushort, string>();

        public static string getLineNumberStr(string lineNumber)
        {
            ushort lineNumUint16 = Convert.ToUInt16(lineNumber);
            string lineNumberStr;

            if (lineNumberDictionary.TryGetValue(lineNumUint16, out lineNumberStr)) return lineNumberStr;
            else if (lineNumUint16 == 0)                                            return "---";
            else                                                                    return "Line " + lineNumUint16.ToString();
        }
    }

    public class Settings
    {
        private Hashtable table = new Hashtable(10);

        private string   colorsXmlFilepath;
        private DateTime colorsXmlFileModifiedDate;

        private Hashtable generalSettings = new Hashtable(20);
        public Hashtable GeneralSettings
        {
            get { return generalSettings; }
        }

        private List<string> serverList = new List<string>();
        public List<string> ServerList
        {
            get { return serverList; }
        }

        private Hashtable sipPhoneSettings = new Hashtable(20);
        public Hashtable SIPPhoneSettings
        {
            get { return sipPhoneSettings; }
        }

        private Hashtable TDD_Settings = new Hashtable(20);
        public Hashtable TDDSettings
        {
            get { return TDD_Settings; }
        }

        private XmlNode TDD_CannedMessages;
        public XmlNode TDDCannedMessages
        {
            get { return TDD_CannedMessages; }
        }

        private Hashtable SMS_Settings = new Hashtable(20);
        public Hashtable SMSSettings
        {
            get { return SMS_Settings; }
        }

        private XmlNode SMS_CannedMessages;
        public XmlNode SMSCannedMessages
        {
            get { return SMS_CannedMessages; }
        }

        private Hashtable Phonebook_Settings = new Hashtable(20);
        public Hashtable PhonebookSettings
        {
            get { return Phonebook_Settings; }
        }

        private List<PhonebookEntry> phonebooklist = new List<PhonebookEntry>(20);
        public List<PhonebookEntry> PhonebookList
        {
            get { return phonebooklist; }
        }

        private List<GroupEntry> grouplist = new List<GroupEntry>(20);
        public List<GroupEntry> GroupList
        {
            get { return grouplist; }
        }

        public class ColorInfo
        {
            public static Color DEFAULT_SHADOW_COLOR = Color.FromArgb(120, Color.Black);

            public ColorInfo(Color bg1, Color bg2, Color fg, Color sh, bool _bold)
            {
                set(bg1, bg2, fg, sh, _bold);
            }

            public ColorInfo(Color bg1, Color bg2, Color fg, bool _bold)
            {
                set(bg1, bg2, fg, DEFAULT_SHADOW_COLOR, _bold);
            }

            public void set(Color bg1, Color bg2, Color fg, Color sh, bool _bold)
            {
                backgroundColor1ExplicitAlpha = (bg1.A != 0);
                backgroundColor2ExplicitAlpha = (bg2.A != 0);
                foregroundColorExplicitAlpha  = (fg.A  != 0);

                backgroundColor1 = bg1.A == 0 ? Color.FromArgb(255, bg1) : bg1;
                backgroundColor2 = bg2.A == 0 ? Color.FromArgb(255, bg2) : bg2;
                foregroundColor  = fg.A  == 0 ? Color.FromArgb(255, fg)  : fg;

                shadowColor = sh;

                bold = _bold;
            }

            public Brush getBackgroundBrush(Rectangle bounds, LinearGradientMode mode)
            {
                if (backgroundColor1.Equals(backgroundColor2))
                {
                    return new SolidBrush(backgroundColor1);
                }
                else
                {
                    return new LinearGradientBrush(
                        bounds, backgroundColor1, backgroundColor2, mode);
                }
            }

            public Brush getForegroundBrush()
            {
                return new SolidBrush(foregroundColor);
            }

            public Color backgroundColor1;
            public Color backgroundColor2;
            public Color foregroundColor;
            public Color shadowColor;

            public bool backgroundColor1ExplicitAlpha;
            public bool backgroundColor2ExplicitAlpha;
            public bool foregroundColorExplicitAlpha;

            public bool bold;
        }

        public string GetSetting(string Name, string Default)
        {
            if (table.ContainsKey(Name)) return table[Name].ToString();
            else                         return Default;
        }

        private string getAppDir()
        {
            string applicationDir;

            try
            {
                applicationDir = Path.GetDirectoryName(
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                applicationDir = applicationDir.Replace("file:\\", "");
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger(
                    "Failed to load local settings (couldn't get the application directory)",
                    ex, Console.LogID.Init);

                applicationDir = "";
            }

            return applicationDir;
        }

        private string getProgramDataDir()
        {
            string programDataDir;

            try
            {
                string parentDir = Environment.GetFolderPath(
                    Environment.SpecialFolder.CommonApplicationData);

                programDataDir = parentDir + "\\WestTel";

                // if --\ProgramData\WestTel\911.xml doesn't exisit, copy config files over from Program Files
                if ( ! File.Exists(programDataDir + "\\911.xml"))
                {
                    // first create the directory so that Everyone can read and write to it
                    DirectorySecurity dirSecurity = new DirectorySecurity();
                    dirSecurity.SetAccessRule(Controller.everyoneAccessRule);

                    FileSecurity fileSecurity = new FileSecurity();
                    fileSecurity.SetAccessRule(Controller.everyoneAccessRule);

                    Directory.CreateDirectory(programDataDir, dirSecurity);

                    try
                    {
                        string applicationDir = getAppDir();

                        if (applicationDir.Length != 0)
                        {
                            // copy 911.xml over
                            if (File.Exists(applicationDir + "\\911.xml"))
                            {
                                File.Copy(applicationDir + "\\911.xml", programDataDir + "\\911.xml");

                                File.SetAccessControl(programDataDir + "\\911.xml", fileSecurity);

                                try {File.Delete(applicationDir + "\\911.xml");} catch {}
                            }

                            // copy 911_Colors.xml over
                            if (File.Exists(applicationDir + "\\911_Colors.xml"))
                            {
                                File.Copy(applicationDir + "\\911_Colors.xml", programDataDir + "\\911_Colors.xml");

                                File.SetAccessControl(programDataDir + "\\911_Colors.xml", fileSecurity);

                                try {File.Delete(applicationDir + "\\911_Colors.xml");} catch {}
                            }
                        }
                    }
                    catch {}
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger(
                    "Failed to load local settings (couldn't get the ProgramData directory)",
                    ex, Console.LogID.Init);

                programDataDir = "";
            }

            return programDataDir;
        }

        public void LoadLocalSettingsFiles()
        {
            string programDataDir = getProgramDataDir();
            if (programDataDir.Length == 0) return;

            restoreColorsXmlFilepath();

            load_911_xml(programDataDir + "\\911.xml");
            load_colors_xml();

            try
            {
                FileSystemWatcher fileWatcher = new FileSystemWatcher(programDataDir, "*.xml");

                fileWatcher.NotifyFilter = NotifyFilters.LastWrite;

                fileWatcher.Changed += FileWatcher_Changed;

                fileWatcher.EnableRaisingEvents = true;
            }
            catch {}
        }

        private void rememberColorsXmlFilepath()
        {
            try
            {
                if (colorsXmlFilepath != null && colorsXmlFilepath.Length != 0)
                {
                    // NOTE: use non-translated company name here
                    Microsoft.Win32.RegistryKey regSettings = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(
                        "Software\\" + System.Windows.Forms.Application.CompanyName + "\\PSAP Toolkit");

                    if (regSettings != null)
                    {
                        regSettings.SetValue("colorsXmlFilepath", colorsXmlFilepath);
                    }
                }
            }
            catch {}
        }

        private void restoreColorsXmlFilepath()
        {
            colorsXmlFilepath = "";

            try
            {
                // NOTE: use non-translated company name here
                Microsoft.Win32.RegistryKey regSettings = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(
                    "Software\\" + System.Windows.Forms.Application.CompanyName + "\\PSAP Toolkit");

                if (regSettings != null)
                {
                    object colorsXmlFileObj = regSettings.GetValue("colorsXmlFilepath");

                    if (colorsXmlFileObj != null)
                    {
                        colorsXmlFilepath = colorsXmlFileObj.ToString();

                        if ( ! (new FileInfo(colorsXmlFilepath).Exists)) colorsXmlFilepath = "";
                    }
                }
            }
            catch {colorsXmlFilepath = "";}
        }

        private void FileWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            try
            {
                DateTime modifiedDate = new FileInfo(e.FullPath).LastWriteTime;

                if (e.Name == "911_Colors.xml" &&
                    colorsXmlFileModifiedDate.Subtract(modifiedDate).Duration().TotalMilliseconds > 100)
                {
                    new System.Threading.Timer(load_colors_xml, null, 50, Timeout.Infinite);
                }
            }
            catch {}
        }

        private static DialogResult showInputDialog(ref string serverIPAddressStr, ref string positionStr)
        {
            System.Drawing.Size size = new System.Drawing.Size(260, 163);
            Form inputBox = new Form();

            inputBox.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            inputBox.ClientSize = size;
            inputBox.BackColor = SystemColors.Window;
            inputBox.StartPosition = FormStartPosition.CenterScreen;
            inputBox.Text = Application.ProductName;

            // Server's IP address
            System.Windows.Forms.Label label1 = new Label();
            label1.Font = new Font(label1.Font, FontStyle.Bold);
            label1.Size = new System.Drawing.Size(size.Width - 20, 20);
            label1.Location = new System.Drawing.Point(10, 10);
            label1.Text = "Server IP Address";
            label1.TextAlign = ContentAlignment.MiddleCenter;
            inputBox.Controls.Add(label1);

            System.Windows.Forms.TextBox textBox1 = new TextBox();
            textBox1.Size = new System.Drawing.Size(size.Width - 20, 30);
            textBox1.Location = new System.Drawing.Point(10, 35);
            textBox1.Text = "192.168.62.11";
            inputBox.Controls.Add(textBox1);

            // Position
            System.Windows.Forms.Label label2 = new Label();
            label2.Font = new Font(label2.Font, FontStyle.Bold);
            label2.Size = new System.Drawing.Size(size.Width - 20, 20);
            label2.Location = new System.Drawing.Point(10, 70);
            label2.Text = "Position";
            label2.TextAlign = ContentAlignment.MiddleCenter;
            inputBox.Controls.Add(label2);

            System.Windows.Forms.TextBox textBox2 = new TextBox();
            textBox2.Size = new System.Drawing.Size(size.Width - 20, 30);
            textBox2.Location = new System.Drawing.Point(10, 95);
            textBox2.Text = "1";
            inputBox.Controls.Add(textBox2);

            Button okButton = new Button();
            okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            okButton.Name = "okButton";
            okButton.Size = new System.Drawing.Size(75, 23);
            okButton.Text = "&OK";
            okButton.Location = new System.Drawing.Point(size.Width - 85 - 80, 130);
            inputBox.Controls.Add(okButton);

            Button cancelButton = new Button();
            cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new System.Drawing.Size(75, 23);
            cancelButton.Text = "&Cancel";
            cancelButton.Location = new System.Drawing.Point(size.Width - 85, 130);
            inputBox.Controls.Add(cancelButton);

            inputBox.AcceptButton = okButton;
            inputBox.CancelButton = cancelButton; 

            inputBox.TopMost    = true;
            DialogResult result = inputBox.ShowDialog();

            serverIPAddressStr = textBox1.Text;
            positionStr        = textBox2.Text;

            return result;
        }

        private bool createSettingsFileIfNecessary(string xmlFilename)
        {
            try
            {
                bool xmlFileExists = File.Exists(xmlFilename);

                if (xmlFileExists) return true;

                string serverIPAddressStr = "", positionStr = "";
                DialogResult result = showInputDialog(ref serverIPAddressStr, ref positionStr);

                if (result == DialogResult.OK)
                {
                    if (serverIPAddressStr.Trim().Length == 0) serverIPAddressStr = "192.168.62.11";
                    if (positionStr.Trim().Length        == 0) positionStr        = "1";
                }
                else
                {
                    return false;
                }

                string xmlStr =
                    "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\r\n" +
                    "\r\n" +
                    "<Settings>\r\n" +
                    "\r\n" +
                    "    <!-- ============================================================================================================ -->\r\n" +
                    "    <!--  ServerIPAddress:  " + (Translate)("The IP Address used to connect to the Server") + "                                              -->\r\n" +
                    "    <!--  ServerPort:       " + (Translate)("The port used to connect to the Server") + " (Default: 50020)                                   -->\r\n" +
                    "    <!--  Protocol:         " + (Translate)("The protocol used to communicate with the Server") + " (Default: TCP)                           -->\r\n" +
                    "    <!--  SendBufferSize:   " + (Translate)("The size of the TCP or UDP send buffer in bytes") + " (Default: 64 KiB)                         -->\r\n" +
                    "    <!--  RecvBufferSize:   " + (Translate)("The size of the TCP or UDP receive buffer in bytes") + " (Default: 3 MiB)                       -->\r\n" +
                    "    <!--  TCPCloseTimeout:  " + (Translate)("Seconds to wait while not hearing from the Server to close the socket") + " (Default: 30)       -->\r\n" +
                    "    <!--  TCPReconnectWait: " + (Translate)("Seconds to wait after the socket is closed to try to reconnect") + " (Default: 10)              -->\r\n" +
                    "    <!--  ValidIPAddresses: " + (Translate)("List of IP Addresses that are valid to receive messages from") + "                              -->\r\n" +
                    "    <!--  Firewall:         " + (Translate)("Turn the firewall that uses ValidIPAddresses on or off") + " (Default: off)                     -->\r\n" +
                    "    <!--  ListenPort:       " + (Translate)("The port used to listen for the Server") + " (Default: 50021)                                   -->\r\n" +
                    "    <!--  IrrIPAddress:     " + (Translate)("The IP Address used to connect to the IRR Server") + " (Default: ServerIPAddress)               -->\r\n" +
                    "    <!--  IrrPort:          " + (Translate)("The port used to connect to the IRR Server") + " (Default: 6000)                                -->\r\n" +
                    "    <!--  Position:         " + (Translate)("Position number of this workstation") + " (Default: 1)                                          -->\r\n" +
                    "    <!--  Logging:          " + (Translate)("Enables Logging for debug and analysis purposes") + " (Default: false)                          -->\r\n" +
                    "    <!--  Translation:      " + (Translate)("Provides a translation file for languages other than English") + "                              -->\r\n" +
                    "    <!-- ============================================================================================================ -->\r\n" +
                    "\r\n" +
                    "    <Setting ServerIPAddress  = \"" + serverIPAddressStr + "\" />\r\n" +
                    "    <Setting ServerPort       = \"50020\" />\r\n" +
                    "    <Setting Protocol         = \"TCP\" />\r\n" +
                    "    <Setting SendBufferSize   = \"65536\" />\r\n" +
                    "    <Setting RecvBufferSize   = \"3145728\" />\r\n" +
                    "    <Setting TCPCloseTimeout  = \"30\" />\r\n" +
                    "    <Setting TCPReconnectWait = \"10\" />\r\n" +
                    "    <Setting ValidIPAddresses = \"\" />\r\n" +
                    "    <Setting Firewall         = \"off\" />\r\n" +
                    "    <Setting ListenPort       = \"50021\" />\r\n" +
                    "    <Setting IrrIPAddress     = \"" + serverIPAddressStr + "\" />\r\n" +
                    "    <Setting IrrPort          = \"6000\" />\r\n" +
                    "    <Setting Position         = \"" + positionStr + "\" />\r\n" +
                    "    <Setting Logging          = \"false\" />\r\n" +
                    "    <Setting Translation      = \"\" />\r\n" +
                    "\r\n" +
                    "</Settings>\r\n";

                byte[] xmlBytes = Encoding.ASCII.GetBytes(xmlStr);

                FileStream xmlFile = File.Create(xmlFilename);
				xmlFile.Write(xmlBytes, 0, xmlBytes.Length);
                xmlFile.Close();

                FileSecurity fileSecurity = new FileSecurity();
                fileSecurity.SetAccessRule(Controller.everyoneAccessRule);
                File.SetAccessControl(xmlFilename, fileSecurity);

                return true;
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger(
                    "Failed to create \"" + xmlFilename + "\"", ex, Console.LogID.Init);
            }

            return false;
        }

        private void load_911_xml(string xmlFilename)
        {
            bool success = createSettingsFileIfNecessary(xmlFilename);

            if ( ! success) return;

            try
            {
                XmlDocument XmlSettings = new XmlDocument();
                XmlSettings.Load(xmlFilename);

                if (XmlSettings.LastChild.ChildNodes.Count > 0)
                {
                    const int MAX_UNKNOWN_ATTRIBS = 30;

                    string[] unknownAttributes  = new string[MAX_UNKNOWN_ATTRIBS];
                    int      nUnknownAttributes = 0;

                    foreach (XmlNode node in XmlSettings.LastChild.ChildNodes)
                    {
                        if ( ! (node is XmlComment))
                        {
                            foreach (XmlAttribute attribute in node.Attributes)
                            {
                                string attribName = attribute.Name;

                                try
                                {
                                    switch (attribName)
                                    {
                                        case "ServerIPAddress":
                                            IPAddress.Parse(attribute.Value); // throw an exception if invalid
                                            table.Add(attribName, attribute.Value);
                                            break;
                                        case "ServerPort":
                                            Convert.ToUInt16(attribute.Value); // throw an exception if invalid
                                            table.Add(attribName, attribute.Value);
                                            break;
                                        case "Protocol":
                                            switch (attribute.Value.ToLower())
                                            {
                                                case "udp": Controller.protocol = System.Net.Sockets.ProtocolType.Udp; break;
                                                case "tcp": Controller.protocol = System.Net.Sockets.ProtocolType.Tcp; break;
                                                default:    throw new Exception("Expected 'UDP' or 'TCP' (found " + attribute.Value + ")");
                                            }
                                            table.Add(attribName, attribute.Value);
                                            break;
                                        case "SendBufferSize":
                                        case "RecvBufferSize":
                                            Convert.ToUInt32(attribute.Value); // throw an exception if invalid
                                            table.Add(attribName, attribute.Value);
                                            break;
                                        case "TCPCloseTimeout":
                                        case "TCPReconnectWait":
                                            Convert.ToUInt16(attribute.Value); // throw an exception if invalid
                                            table.Add(attribName, attribute.Value);
                                            break;
                                        case "ValidIPAddresses":
                                            char[] delimiters = {' ',',',';','\t'};
                                            string[] validIPs = attribute.Value.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                                            foreach (string ipStr in validIPs) IPAddress.Parse(ipStr); // throw an exception if invalid
                                            table.Add(attribName, attribute.Value);
                                            break;
                                        case "Firewall":
                                            Console.toBoolean(attribute.Value);
                                            table.Add(attribName, attribute.Value);
                                            break;
                                        case "ListenPort":
                                            Convert.ToUInt16(attribute.Value); // throw an exception if invalid
                                            table.Add(attribName, attribute.Value);
                                            break;
                                        case "IrrIPAddress":
                                            IPAddress.Parse(attribute.Value); // throw an exception if invalid
                                            table.Add(attribName, attribute.Value);
                                            break;
                                        case "IrrPort":
                                            Convert.ToUInt16(attribute.Value); // throw an exception if invalid
                                            table.Add(attribName, attribute.Value);
                                            break;
                                        case "Position":
                                            if (Convert.ToUInt16(attribute.Value) > 1000) // throw an exception if invalid
                                            {
                                                throw new Exception("Must not be greater than 1000 (found " + attribute.Value + ")");
                                            }
                                            table.Add(attribName, attribute.Value);
                                            break;
                                        case "Logging":
                                            Console.toBoolean(attribute.Value);
                                            table.Add(attribName, attribute.Value);
                                            break;
                                        case "Translation":
                                            string translationFilename = attribute.Value.Trim();
                                            if (translationFilename.Length != 0)
                                            {
                                                if (File.Exists(translationFilename))
                                                {
                                                    Translate.loadTranslationFile(translationFilename);

                                                    Application.CompanyName = (Translate)(Application.CompanyName);
                                                    Application.ProductName = (Translate)(Application.ProductName);
                                                }
                                                else
                                                {
                                                    throw new Exception("File not found \"" + attribute.Value + "\"");
                                                }
                                            }
                                            break;

                                        default:
                                            if (nUnknownAttributes < MAX_UNKNOWN_ATTRIBS) unknownAttributes[nUnknownAttributes++] = attribName;
                                            break;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Controller.showAlert((Translate)("Error in \"") + xmlFilename + "\" - attribute \"" +
                                                         attribName + "\" (" + ex.Message + ")", Console.LogType.Warning, Console.LogID.Init);
                                }
                            }
                        }
                    }

                    if (nUnknownAttributes > 0)
                    {
                        string infoMsg;

                        if (nUnknownAttributes == 1) infoMsg = (Translate)("There was an unexpected attribute");
                        else                         infoMsg = (Translate)("There were unexpected attributes");

                        infoMsg += " in \"" + xmlFilename + "\" (Unexpected:";

                        for (int i = 0; i < nUnknownAttributes; ++i)
                        {
                            infoMsg += " \"" + unknownAttributes[i] + "\"";
                        }

                        infoMsg += (Translate)(") Acceptable attributes:") +
                                   " \"ServerIPAddress\"" +
                                   " \"ServerPort\"" +
                                   " \"Protocol\"" +
                                   " \"SendBufferSize\"" +
                                   " \"RecvBufferSize\"" +
                                   " \"TCPCloseTimeout\"" +
                                   " \"TCPReconnectWait\"" +
                                   " \"ValidIPAddresses\"" +
                                   " \"Firewall\"" +
                                   " \"ListenPort\"" +
                                   " \"IrrIPAddress\"" +
                                   " \"IrrPort\"" +
                                   " \"Position\"" +
                                   " \"Logging\"" +
                                   " \"Translation\"";

                        Controller.showAlert(infoMsg, Console.LogType.Warning, Console.LogID.Init);
                    }
                }
            }
            catch (Exception ex)
            {
                Controller.showAlert((Translate)("Error in \"") + xmlFilename + "\"" +
                    " (" + ex.Message + ")", Console.LogType.Warning, Console.LogID.Init);

                return;
            }
        }

        private bool createColorsFileIfNecessary()
        {
            try
            {
                if (colorsXmlFilepath == null || colorsXmlFilepath.Length == 0)
                {
                    string programDataDir = getProgramDataDir();
                    if (programDataDir.Length == 0) return false;

                    colorsXmlFilepath = programDataDir + "\\911_Colors.xml";
                    rememberColorsXmlFilepath();
                }

                bool xmlFileExists = File.Exists(colorsXmlFilepath);

                // TODO: uncomment this when we no longer want to override the colors file every time
                // if (xmlFileExists) return true;

                string xmlStr =
                    "<?xml version='1.0' encoding='utf-8' ?>\r\n" +
                    "\r\n" +
                    "<Settings>\r\n" +
                    "\r\n" +
                    "  <!--~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-\r\n" +
                    "\r\n" +
                    "      A good place to find colors is Google's Color Guidelines\r\n" +
                    "      (https://www.google.com/design/spec/style/color.html)\r\n" +
                    "\r\n" +
                    "      The colors are specfied in hex as either RGB or ARGB (Alpha,Red,Green,Blue),\r\n" +
                    "      so that 0xFF0000 is Red, and 0x80FF0000 is partially transparent Red\r\n" +
                    "\r\n" +
                    "    -~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-->\r\n" +
                    "\r\n" +
                    "  <Window name='Console'>\r\n" +
                    "\r\n" +
                    "    <Ali>\r\n" +
                    "      <ActiveColor   background='0xFF151515' foreground='0xFFFFEB3B' bold='false' />\r\n" +
                    "      <InactiveColor background='0xFF151515' foreground='0x99FFEB3B' bold='false' />\r\n" +
                    "    </Ali>\r\n" +
                    "\r\n" +
                    "    <Calls>\r\n" +
                    "\r\n" +
                    "      <Background color='0xFFF0F0F0' />\r\n" +
                    "\r\n" +
                    "      <Row type='9-1-1'>\r\n" +
                    "        <ActiveColor   state='normal'   background='0xFFFFFFFF' foreground='0xFFB71C1C' bold='true' shadow='0x70000000' />\r\n" +
                    "        <ActiveColor   state='hovered'  background='0xFFFFFFFF' foreground='0xFFB71C1C' bold='true' shadow='0x70000000' />\r\n" +
                    "        <ActiveColor   state='selected' background='0xFFD32F2F' foreground='0xFFFFFFFF' bold='true' shadow='0x70000000' />\r\n" +
                    "        <InactiveColor state='normal'   background='0xFFFFFFFF' foreground='0xFFB71C1C' bold='false' shadow='0x70000000' />\r\n" +
                    "        <InactiveColor state='hovered'  background='0xFFFFFFFF' foreground='0xFFB71C1C' bold='false' shadow='0x70000000' />\r\n" +
                    "        <InactiveColor state='selected' background='0xFFD32F2F' foreground='0xFFE0E0E0' bold='false' shadow='0x70000000' />\r\n" +
                    "      </Row>\r\n" +
                    "\r\n" +
                    "      <Row type='Admin'>\r\n" +
                    "        <ActiveColor   state='normal'   background='0xFFFFFFFF' foreground='0xFF00578C' bold='true' shadow='0x70000000' />\r\n" +
                    "        <ActiveColor   state='hovered'  background='0xFFFFFFFF' foreground='0xFF00578C' bold='true' shadow='0x70000000' />\r\n" +
                    "        <ActiveColor   state='selected' background='0xFF0091EA' foreground='0xFFFFFFFF' bold='true' shadow='0x70000000' />\r\n" +
                    "        <InactiveColor state='normal'   background='0xFFFFFFFF' foreground='0xFF00578C' bold='false' shadow='0x70000000' />\r\n" +
                    "        <InactiveColor state='hovered'  background='0xFFFFFFFF' foreground='0xFF00578C' bold='false' shadow='0x70000000' />\r\n" +
                    "        <InactiveColor state='selected' background='0xFF0091EA' foreground='0xFFFFFFFF' bold='false' shadow='0x70000000' />\r\n" +
                    "      </Row>\r\n" +
                    "\r\n" +
                    "      <Row type='Abandoned'>\r\n" +
                    "        <ActiveColor   state='normal'   background='0xFFFFECB3' foreground='0xFF424242' bold='true' shadow='0x70000000' />\r\n" +
                    "        <ActiveColor   state='hovered'  background='0xFFFFECB3' foreground='0xFF424242' bold='true' shadow='0x70000000' />\r\n" +
                    "        <ActiveColor   state='selected' background='0xFFFF8F00' foreground='0xFFFFFFFF' bold='true' shadow='0x70000000' />\r\n" +
                    "        <InactiveColor state='normal'   background='0xFFFFECB3' foreground='0xFF424242' bold='false' shadow='0x70000000' />\r\n" +
                    "        <InactiveColor state='hovered'  background='0xFFFFECB3' foreground='0xFF424242' bold='false' shadow='0x70000000' />\r\n" +
                    "        <InactiveColor state='selected' background='0xFFFF8F00' foreground='0xFF000000' bold='false' shadow='0x70000000' />\r\n" +
                    "      </Row>\r\n" +
                    "\r\n" +
                    "      <Row type='ManualBid'>\r\n" +
                    "        <Color state='normal'   background='0xFFFAFAFA' foreground='0xFF388E3C' bold='false' shadow='0x70000000' />\r\n" +
                    "        <Color state='hovered'  background='0xFFFAFAFA' foreground='0xFF388E3C' bold='false' shadow='0x70000000' />\r\n" +
                    "        <Color state='selected' background='0xFF43A047' foreground='0xFF000000' bold='false' shadow='0x70000000' />\r\n" +
                    "      </Row>\r\n" +
                    "\r\n" +
                    "      <Row type='Unknown'>\r\n" +
                    "        <Color state='normal'   background='0xFFCCCCCC' foreground='0xFF000000' bold='false' shadow='0x70000000' />\r\n" +
                    "        <Color state='hovered'  background='0xFFCCCCCC' foreground='0xFF000000' bold='false' shadow='0x70000000' />\r\n" +
                    "        <Color state='selected' background='0xFFFAFAFA' foreground='0xFF424242' bold='false' shadow='0x70000000' />\r\n" +
                    "      </Row>\r\n" +
                    "\r\n" +
                    "    </Calls>\r\n" +
                    "\r\n" +
                    "  </Window>\r\n" +
                    "\r\n" +
                    "  <Window name='Phonebook'>\r\n" +
                    "\r\n" +
                    "    <Directory>\r\n" +
                    "\r\n" +
                    "      <Groups>\r\n" +
                    "\r\n" +
                    "        <Background color='0xFFFFE082' />\r\n" +
                    "\r\n" +
                    "        <Color state='normal'   background='0xFFFFE082' foreground='0xFF000000' bold='false' />\r\n" +
                    "        <Color state='hovered'  background='0xFFCA9800' foreground='0xFFFFFFFF' bold='false' />\r\n" +
                    "        <Color state='selected' background='0xFF0060BF' foreground='0xFFFFFFFF' bold='true' />\r\n" +
                    "\r\n" +
                    "      </Groups>\r\n" +
                    "\r\n" +
                    "      <List>\r\n" +
                    "\r\n" +
                    "        <Background color='0xFFFFFFFF' />\r\n" +
                    "\r\n" +
                    "        <Color state='normal'   background='0xFFFFFFFF' foreground='0xFF000000' bold='false' />\r\n" +
                    "        <Color state='hovered'  background='0xFFD8D8D8' foreground='0xFF000000' bold='false' />\r\n" +
                    "        <Color state='selected' background='0xFF003388' foreground='0xFFFFFFFF' bold='true' />\r\n" +
                    "\r\n" +
                    "      </List>\r\n" +
                    "\r\n" +
                    "    </Directory>\r\n" +
                    "\r\n" +
                    "    <Dialpad>\r\n" +
                    "      <Color state='normal'   background='0xFFFFFFFF' foreground='0xFF000000' bold='true' />\r\n" +
                    "      <Color state='hovered'  background='0xFFFFFFFF' foreground='0xFF000000' bold='true' />\r\n" +
                    "    </Dialpad>\r\n" +
                    "\r\n" +
                    "  </Window>\r\n" +
                    "\r\n" +
                    "  <Window name='TDD'>\r\n" +
                    "\r\n" +
                    "    <Background color='0xFFFFFFFF' />\r\n" +
                    "\r\n" +
                    "    <Dispatcher>\r\n" +
                    "      <Color background='0xFF2196F3' foreground='0xFFFFFFFF' bold='false' />\r\n" +
                    "    </Dispatcher>\r\n" +
                    "\r\n" +
                    "    <Caller>\r\n" +
                    "      <Color background='0xFFE0E0E0' foreground='0xFF000000' bold='false' />\r\n" +
                    "    </Caller>\r\n" +
                    "\r\n" +
                    "  </Window>\r\n" +
                    "\r\n" +
                    "  <Window name='SMS'>\r\n" +
                    "\r\n" +
                    "    <Background color='0xFFFFFFFF' />\r\n" +
                    "\r\n" +
                    "    <Dispatcher>\r\n" +
                    "      <Color background='0xFF2196F3' foreground='0xFFFFFFFF' bold='false' />\r\n" +
                    "    </Dispatcher>\r\n" +
                    "\r\n" +
                    "    <Caller>\r\n" +
                    "      <Color background='0xFFE0E0E0' foreground='0xFF000000' bold='false' />\r\n" +
                    "    </Caller>\r\n" +
                    "\r\n" +
                    "  </Window>\r\n" +
                    "\r\n" +
                    "  <Window name='IRR'>\r\n" +
                    "\r\n" +
                    "    <Background color='0xFF000000' />\r\n" +
                    "\r\n" +
                    "    <AvgWave>    <Color foreground='0xFF006400' /> </AvgWave>\r\n" +
                    "    <MaxWave>    <Color foreground='0x281E90FF' /> </MaxWave>\r\n" +
                    "    <Unselected> <Color background='0x60A9A9A9' /> </Unselected>\r\n" +
                    "    <Location>   <Color foreground='0x50FFFFFF' /> </Location>\r\n" +
                    "\r\n" +
                    "  </Window>\r\n" +
                    "\r\n" +
                    "  <Window name='PSAP Status'>\r\n" +
                    "\r\n" +
                    "    <Background color='0xFFCCCCCC' />\r\n" +
                    "\r\n" +
                    "    <Row type='Available'>\r\n" +
                    "      <Color state='normal'   background='0xFF43A047' foreground='0xFFFAFAFA' bold='false' />\r\n" +
                    "      <Color state='hovered'  background='0xFF43A047' foreground='0xFFFAFAFA' bold='false' />\r\n" +
                    "    </Row>\r\n" +
                    "\r\n" +
                    "    <Row type='Partial'>\r\n" +
                    "      <Color state='normal'   background='0xFFFF8F00' foreground='0xFFFAFAFA' bold='false' />\r\n" +
                    "      <Color state='hovered'  background='0xFFFF8F00' foreground='0xFFFAFAFA' bold='false' />\r\n" +
                    "    </Row>\r\n" +
                    "\r\n" +
                    "    <Row type='Unavailable'>\r\n" +
                    "      <Color state='normal'   background='0xFFD32F2F' foreground='0xFFFAFAFA' bold='false' />\r\n" +
                    "      <Color state='hovered'  background='0xFFD32F2F' foreground='0xFFFAFAFA' bold='false' />\r\n" +
                    "    </Row>\r\n" +
                    "\r\n" +
                    "  </Window>\r\n" +
                    "\r\n" +
                    "  <Window name='Alerts'>\r\n" +
                    "\r\n" +
                    "    <Background color='0xFFF0F0F0' />\r\n" +
                    "\r\n" +
                    "    <Row>\r\n" +
                    "      <Color state='normal'   background='0xFFFFE082' foreground='0xFF000000' bold='false' />\r\n" +
                    "      <Color state='hovered'  background='0xFFFFE082' foreground='0xFF000000' bold='false' />\r\n" +
                    "      <Color state='selected' background='0xFFFFFFFF' foreground='0xFFFF9700' bold='false' />\r\n" +
                    "    </Row>\r\n" +
                    "\r\n" +
                    "    <Row type='Alternating'>\r\n" +
                    "      <Color state='normal'   background='0xFFFFC107' foreground='0xFF000000' bold='false' />\r\n" +
                    "      <Color state='hovered'  background='0xFFFFC107' foreground='0xFF000000' bold='false' />\r\n" +
                    "      <Color state='selected' background='0xFFFFFFFF' foreground='0xFFFF9700' bold='false' />\r\n" +
                    "    </Row>\r\n" +
                    "\r\n" +
                    "  </Window>\r\n" +
                    "\r\n" +
                    "  <Window name='Alert Popup'>\r\n" +
                    "    <Color background='0xFFFFE082' foreground='0xFF000000' bold='false' />\r\n" +
                    "  </Window>\r\n" +
                    "\r\n" +
                    "</Settings>\r\n";

                byte[] xmlBytes = Encoding.ASCII.GetBytes(xmlStr);

                FileStream xmlFile = File.Create(colorsXmlFilepath);
				xmlFile.Write(xmlBytes, 0, xmlBytes.Length);
                xmlFile.Close();

                if (File.Exists(colorsXmlFilepath))
                {
                    FileSecurity fileSecurity = new FileSecurity();
                    fileSecurity.SetAccessRule(Controller.everyoneAccessRule);
                    File.SetAccessControl(colorsXmlFilepath, fileSecurity);

                    return true;
                }

                MessageBox.Show(null, (Translate)("Failed to create") + " \"" + colorsXmlFilepath + "\"",
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning,
                    MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("Failed to create \"" + colorsXmlFilepath + "\"",
                    ex, Console.LogID.Init);

                MessageBox.Show(null, (Translate)("Failed to create") + " \"" +
                    colorsXmlFilepath + "\"" + " (" + ex.Message + ")",
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning,
                    MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
            }

            return false;
        }

        public string getColorsXmlFilepath() {return colorsXmlFilepath;}

        public void resetColorsXml()
        {
            try
            {
                string programDataDir = getProgramDataDir();

                if (programDataDir.Length != 0)
                {
                    colorsXmlFilepath = programDataDir + "\\911_Colors.xml";
                    System.IO.File.Delete(colorsXmlFilepath);

                    loadColorsXmlFilepath("");
                }
            }
            catch {}
        }

        public void loadColorsXmlFilepath(string path)
        {
            colorsXmlFilepath = path;
            load_colors_xml();

            rememberColorsXmlFilepath();
        }

        public void load_colors_xml() {load_colors_xml((object)null);}

        public void load_colors_xml(object obj)
        {
            try
            {
                ColorEditor colorEditor =
                    (Controller.controller != null ? Controller.controller.colorEditor : null);

                if (colorEditor != null)
                {
                    // invoke the load on the Ui thread to avoid threading problems
                    colorEditor.BeginInvoke(new load_colors_delegate(load_colors_xml), colorEditor);
                    return;
                }
            }
            catch {}

            load_colors_xml((ColorEditor)null);
        }

        private delegate void load_colors_delegate(ColorEditor colorEditor);

        private void load_colors_xml(ColorEditor colorEditor)
        {
            bool success = createColorsFileIfNecessary();

            if ( ! success) return;

            string windowName = null;

            try
            {
                colorsXmlFileModifiedDate = new FileInfo(colorsXmlFilepath).LastWriteTime;

                XmlDocument XmlSettings = new XmlDocument();
                XmlSettings.Load(colorsXmlFilepath);

                if (XmlSettings.LastChild.ChildNodes.Count > 0)
                {
                    foreach (XmlNode windowNode in XmlSettings.LastChild.ChildNodes)
                    {
                        if ( ! (windowNode is XmlComment))
                        {
                            if (windowNode.Name.ToLower().Equals("window"))
                            {
                                if (windowNode.Attributes["name"] != null)
                                {
                                    windowName = windowNode.Attributes["name"].Value;

                                    switch (windowName.ToLower())
                                    {
                                        case "alert popup":
                                        case "alertpopup":  loadAlertPopupSettings (windowNode); break;
                                        case "console":     loadConsoleSettings    (windowNode); break;
                                        case "phonebook":   loadPhonebookSettings  (windowNode); break;
                                        case "tdd":         loadMessageSettings    (windowNode, MessageWindow.WindowType.TDD); break;
                                        case "sms":         loadMessageSettings    (windowNode, MessageWindow.WindowType.SMS); break;
                                        case "irr":         loadIrrSettings        (windowNode); break;
                                        case "psap status":
                                        case "psapstatus":  loadPsapStatusSettings (windowNode); break;
                                        case "alerts":      loadAlertsSettings     (windowNode); break;

                                        default:
                                        {
                                            throw new Exception("Expected 'Window.name' to be " +
                                                "'Alert Popup', 'Console', 'Phonebook', 'TDD', 'SMS', " +
                                                "'PSAP Status', or 'Alerts' (found '" + windowName + "')");
                                        }
                                    }

                                    windowName = null;
                                }
                                else
                                {
                                    throw new Exception("Expected 'name' attribute in 'Window'");
                                }
                            }
                            else
                            {
                                throw new Exception("Expected 'Window' (found '" + windowNode.Name + "')");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string contextStr = (windowName == null) ? "" : " - Window.name='" + windowName + "'";

                Controller.showAlert((Translate)("Error in \"") + colorsXmlFilepath + "\"" + contextStr +
                    " (" + ex.Message + ")", Console.LogType.Warning, Console.LogID.Init);
            }

            try   {if (Controller.controller != null) Controller.controller.colorsUpdated();}
            catch {}

            try
            {
                if (colorEditor != null)
                {
                    colorEditor.clear();

                    // populate color editor tree view
                    populateConsoleSettings    (colorEditor);
                    populatePhonebookSettings  (colorEditor);
                    populateMessageSettings    (colorEditor, MessageWindow.WindowType.TDD);
                    populateMessageSettings    (colorEditor, MessageWindow.WindowType.SMS);
                    populateIrrSettings        (colorEditor);
                    populatePsapStatusSettings (colorEditor);
                    populateAlertsSettings     (colorEditor);
                    populateAlertPopupSettings (colorEditor);
                }
            }
            catch (Exception ex)
            {
                Controller.showAlert((Translate)("Error populating the Color Editor") +
                    " (" + ex.Message + ")", Console.LogType.Warning, Console.LogID.Init);
            }
        }

        public bool writeColorsXmlFile(string filepath, ColorEditor colorEditor)
        {
            try
            {
                System.IO.FileStream fileStream = System.IO.File.OpenWrite(filepath);

                string xmlText =
                    "<?xml version='1.0' encoding='utf-8' ?>\r\n" +
                    "\r\n" +
                    "<Settings>\r\n" +
                    "\r\n" +
                    "  <!--~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-\r\n" +
                    "\r\n" +
                    "      A good place to find colors is Google's Color Guidelines\r\n" +
                    "      (https://www.google.com/design/spec/style/color.html)\r\n" +
                    "\r\n" +
                    "      The colors are specfied in hex as either RGB or ARGB (Alpha,Red,Green,Blue),\r\n" +
                    "      so that 0xFF0000 is Red, and 0x80FF0000 is partially transparent Red\r\n" +
                    "\r\n" +
                    "    -~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-->\r\n";

                // TODO color: respect and write out comments that were originally read in somehow?

                writeConsoleSettings    (ref xmlText);
                writePhonebookSettings  (ref xmlText);
                writeMessageSettings    (ref xmlText, MessageWindow.WindowType.TDD);
                writeMessageSettings    (ref xmlText, MessageWindow.WindowType.SMS);
                writeIrrSettings        (ref xmlText);
                writePsapStatusSettings (ref xmlText);
                writeAlertsSettings     (ref xmlText);
                writeAlertPopupSettings (ref xmlText);

                xmlText += "\r\n</Settings>\r\n";

                fileStream.Write(Encoding.ASCII.GetBytes(xmlText), 0, xmlText.Length);

                fileStream.Close();

                return true;
            }
            catch (Exception ex)
            {
                DialogResult result = MessageBox.Show(colorEditor, (Translate)("Could not save file:") +
                    "\n\n    \"" + filepath + "\"\n\n" + ex.Message + "\n\nClick OK to ignore.",
                    Application.ProductName, MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Information, MessageBoxDefaultButton.Button2);

                string userResponseStr = "";

                if (result == DialogResult.OK) userResponseStr = " - User chose ignore";

                Controller.sendAlertToController("Error writing \"" + filepath + "\"" +
                    " (" + ex.Message + ")" + userResponseStr, Console.LogType.Info, Console.LogID.Gui);

                return (result != DialogResult.OK);
            }
        }

        private void loadAlertPopupSettings(XmlNode windowNode)
        {
            TaskbarNotifier.windowColorInfo = loadColorInfo(windowNode.LastChild);
        }

        private void populateAlertPopupSettings(ColorEditor colorEditor)
        {
            try
            {
                colorEditor.addNode("Alert Popup", TaskbarNotifier.windowColorInfo);
            }
            catch (Exception ex)
            {
                Controller.showAlert((Translate)("Error building Color Editor - Alert Popup") +
                    " (" + ex.Message + ")", Console.LogType.Warning, Console.LogID.Init);
            }
        }

        private void writeAlertPopupSettings(ref string xmlText)
        {
            try
            {
                xmlText += "\r\n  <Window name='Alert Popup'>\r\n";
                writeColorInfo(ref xmlText, "    ", TaskbarNotifier.windowColorInfo);
                xmlText += "  </Window>\r\n";
            }
            catch (Exception ex)
            {
                Controller.showAlert((Translate)("Error writing Colors - Alert Popup") +
                    " (" + ex.Message + ")", Console.LogType.Warning, Console.LogID.Gui);
            }
        }

        private void loadConsoleSettings(XmlNode windowNode)
        {
            foreach (XmlNode xmlNode in windowNode.ChildNodes)
            {
                if ( ! (xmlNode is XmlComment))
                {
                    if (xmlNode.Name.ToLower().Equals("ali"))
                    {
                        foreach (XmlNode colorNode in xmlNode.ChildNodes)
                        {
                            if ( ! (colorNode is XmlComment))
                            {
                                bool isCurrent;

                                ColorInfo colorInfo = loadColorInfo(colorNode, out isCurrent);

                                if (isCurrent) Console.aliColorInfo        = colorInfo;
                                else           Console.aliColorInfoPending = colorInfo;
                            }
                        }
                    }
                    else if (xmlNode.Name.ToLower().Equals("calls"))
                    {
                        foreach (XmlNode node in xmlNode.ChildNodes)
                        {
                            if ( ! (node is XmlComment))
                            {
                                if (node.Name.ToLower().Equals("background"))
                                {
                                    Console.gridBackgroundColorInfo = loadBackgroundColor(node);
                                }
                                else if (node.Name.ToLower().Equals("row"))
                                {
                                    CallRowType callType = CallRowType.Unknown;

                                    if (node.Attributes["type"] != null)
                                    {
                                        string callTypeStr = node.Attributes["type"].Value;

                                        switch (callTypeStr.ToLower())
                                        {
                                            case "9-1-1":      callType = CallRowType._911;      break;
                                            case "admin":      callType = CallRowType.Admin;     break;
                                            case "abandoned":  callType = CallRowType.Abandoned; break;
                                            case "manualbid":  callType = CallRowType.ManualBid; break;
                                            case "unknown":    callType = CallRowType.Unknown;   break;

                                            default: throw new Exception("Expected 'Calls.Row.type' to be " +
                                                "'9-1-1', 'Admin', 'Abandoned', 'ManualBid', or 'Unknown' (found '" + callTypeStr + "')");
                                        }
                                    }
                                    else
                                    {
                                        throw new Exception("Expected 'type' attribute in 'Calls.Row'");
                                    }

                                    foreach (XmlNode colorNode in node.ChildNodes)
                                    {
                                        if ( ! (colorNode is XmlComment))
                                        {
                                            bool         isActive;
                                            CallRowState state;

                                            ColorInfo colorInfo = loadColorInfo(
                                                colorNode, out isActive, out state);

                                            Console.CallRowInfo callRowInfo =
                                                new Console.CallRowInfo(callType, isActive, state);

                                            Console.callColorDictionary[callRowInfo] = colorInfo;
                                        }
                                    }
                                }
                                else
                                {
                                    throw new Exception("Expected 'Calls.Background' or 'Calls.Row' " +
                                        "(found 'Calls." + xmlNode.Name + "')");
                                }
                            }
                        }

                        // if a case was not explicitly handled, resolve it
                        for (CallRowType callType = CallRowType._911;
                             callType <= CallRowType.Unknown; ++callType)
                        {
                            for (CallRowState state = CallRowState.FirstState;
                                 state != CallRowState.NumStates; ++state)
                            {
                                Console.CallRowInfo activeKey =
                                    new Console.CallRowInfo(callType, true, state);

                                Console.CallRowInfo inactiveKey =
                                    new Console.CallRowInfo(callType, false, state);

                                if (Console.callColorDictionary.ContainsKey(activeKey) !=
                                    Console.callColorDictionary.ContainsKey(inactiveKey))
                                {
                                    if (Console.callColorDictionary.ContainsKey(activeKey))
                                    {
                                        Console.callColorDictionary[inactiveKey] =
                                            Console.callColorDictionary[activeKey];
                                    }
                                    else
                                    {
                                        Console.callColorDictionary[activeKey] =
                                            Console.callColorDictionary[inactiveKey];
                                    }
                                }

                                Console.CallRowInfo hoveredKey =
                                    new Console.CallRowInfo(callType, true, CallRowState.Hovered);

                                Console.CallRowInfo normalKey =
                                    new Console.CallRowInfo(callType, true, CallRowState.Normal);

                                if ( ! Console.callColorDictionary.ContainsKey(hoveredKey))
                                {
                                    Console.callColorDictionary[hoveredKey] =
                                        Console.callColorDictionary[normalKey];
                                }
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("Expected 'Ali' or 'Calls' (found '" + xmlNode.Name + "')");
                    }
                }
            }
        }

        private void populateConsoleSettings(ColorEditor colorEditor)
        {
            try
            {
                TreeNode rootBranch = colorEditor.addNode("Console");

                TreeNode aliBranch = colorEditor.addNode(rootBranch, "Ali");
                colorEditor.addNode(aliBranch, "Current", Console.aliColorInfo);
                colorEditor.addNode(aliBranch, "Pending", Console.aliColorInfoPending);

                TreeNode callsBranch = colorEditor.addNode(rootBranch, "Calls");

                colorEditor.addNode(callsBranch, "Background", Console.gridBackgroundColorInfo);

                populateCallSettings(colorEditor, callsBranch, CallRowType._911,      "9-1-1",     true);
                populateCallSettings(colorEditor, callsBranch, CallRowType.Admin,     "Admin",     true);
                populateCallSettings(colorEditor, callsBranch, CallRowType.Abandoned, "Abandoned", true);
                populateCallSettings(colorEditor, callsBranch, CallRowType.ManualBid, "ManualBid", false);
                populateCallSettings(colorEditor, callsBranch, CallRowType.Unknown,   "Unknown",   false);
            }
            catch (Exception ex)
            {
                Controller.showAlert((Translate)("Error building Color Editor - Console") +
                    " (" + ex.Message + ")", Console.LogType.Warning, Console.LogID.Init);
            }
        }

        private void populateCallSettings(
            ColorEditor colorEditor,
            TreeNode    callsBranch,
            CallRowType callType,
            string      callTypeStr,
            bool        hasActive
        )
        {
            TreeNode rowTypeBranch  = colorEditor.addNode(callsBranch, callTypeStr);
            TreeNode activeBranch   = rowTypeBranch;
            TreeNode inactiveBranch = rowTypeBranch;

            if (hasActive)
            {
                activeBranch   = colorEditor.addNode(rowTypeBranch, "Active");
                inactiveBranch = colorEditor.addNode(rowTypeBranch, "Inactive");
            }

            for (CallRowState state = CallRowState.FirstState;
                 state != CallRowState.NumStates; ++state)
            {
                if (hasActive)
                {
                    colorEditor.addNode(activeBranch, state.ToString(),
                        Console.callColorDictionary[new Console.CallRowInfo(callType, true, state)]);
                }

                colorEditor.addNode(inactiveBranch, state.ToString(),
                    Console.callColorDictionary[new Console.CallRowInfo(callType, false, state)]);
            }
        }

        private void writeConsoleSettings(ref string xmlText)
        {
            try
            {
                xmlText += "\r\n  <Window name='Console'>\r\n";

                xmlText += "\r\n    <Ali>\r\n";
                writeColorInfo(ref xmlText, "      ", true,  Console.aliColorInfo);
                writeColorInfo(ref xmlText, "      ", false, Console.aliColorInfoPending);
                xmlText += "    </Ali>\r\n";

                xmlText += "\r\n    <Calls>\r\n";

                writeBackgroundColorInfo(ref xmlText, "\r\n      ", Console.gridBackgroundColorInfo);

                writeCallSettings(ref xmlText, CallRowType._911,      "9-1-1",     true);
                writeCallSettings(ref xmlText, CallRowType.Admin,     "Admin",     true);
                writeCallSettings(ref xmlText, CallRowType.Abandoned, "Abandoned", true);
                writeCallSettings(ref xmlText, CallRowType.ManualBid, "ManualBid", false);
                writeCallSettings(ref xmlText, CallRowType.Unknown,   "Unknown",   false);

                xmlText += "\r\n    </Calls>\r\n";

                xmlText += "\r\n  </Window>\r\n";
            }
            catch (Exception ex)
            {
                Controller.showAlert((Translate)("Error writing Colors - Console") +
                    " (" + ex.Message + ")", Console.LogType.Warning, Console.LogID.Gui);
            }
        }

        private void writeCallSettings(
            ref string  xmlText,
            CallRowType callType,
            string      callTypeStr,
            bool        hasActive
        )
        {
            xmlText += "\r\n      <Row type='" + callTypeStr + "'>\r\n";

            if (hasActive)
            {
                for (CallRowState state = CallRowState.FirstState; state != CallRowState.NumStates; ++state)
                {
                    writeColorInfo(ref xmlText, "        ", true, state,
                        Console.callColorDictionary[new Console.CallRowInfo(callType, true, state)]);
                }
            }

            for (CallRowState state = CallRowState.FirstState; state != CallRowState.NumStates; ++state)
            {
                if (hasActive)
                {
                    writeColorInfo(ref xmlText, "        ", false, state,
                        Console.callColorDictionary[new Console.CallRowInfo(callType, false, state)]);
                }
                else
                {
                    writeColorInfo(ref xmlText, "        ", state,
                        Console.callColorDictionary[new Console.CallRowInfo(callType, false, state)]);
                }
            }

            xmlText += "      </Row>\r\n";
        }

        private void loadPhonebookSettings(XmlNode windowNode)
        {
            foreach (XmlNode xmlNode in windowNode.ChildNodes)
            {
                if ( ! (xmlNode is XmlComment))
                {
                    switch (xmlNode.Name.ToLower())
                    {
                        case "background":
                        {
                            Settings.ColorInfo backgroundColorInfo = loadBackgroundColor(xmlNode);

                            Phonebook.directoryGroupBackgroundColorInfo = backgroundColorInfo;
                            Phonebook.directoryListBackgroundColorInfo  = backgroundColorInfo;

                            break;
                        }
                        case "directory":
                        {
                            loadPhonebookDirectorySettings(xmlNode);
                            break;
                        }
                        case "dialpad":
                        {
                            foreach (XmlNode colorNode in xmlNode.ChildNodes)
                            {
                                if ( ! (colorNode is XmlComment))
                                {
                                    bool         isActive;
                                    CallRowState state;

                                    ColorInfo colorInfo =
                                        loadColorInfo(colorNode, out isActive, out state);

                                    if (state == CallRowState.Normal)
                                    {
                                        Phonebook.dialpadColorInfo = colorInfo;
                                    }
                                    else if (state == CallRowState.Hovered)
                                    {
                                        Phonebook.dialpadHoverColorInfo = colorInfo;
                                    }
                                }
                            }

                            break;
                        }
                        default:
                        {
                            throw new Exception("Expected 'Background', 'Directory', or 'Dialpad' " +
                                "(found '" + xmlNode.Name + "')");
                        }
                    }
                }
            }
        }

        private void loadPhonebookDirectorySettings(XmlNode directoryNode)
        {
            foreach (XmlNode xmlNode in directoryNode.ChildNodes)
            {
                if ( ! (xmlNode is XmlComment))
                {
                    switch (xmlNode.Name.ToLower())
                    {
                        case "groups":
                        case "list":
                        {
                            // keep track of whether active was explicitly handled
                            bool activeHandled = false, inactiveHandled = false;

                            foreach (XmlNode colorNode in xmlNode.ChildNodes)
                            {
                                if ( ! (colorNode is XmlComment))
                                {
                                    switch (colorNode.Name.ToLower())
                                    {
                                        case "color":
                                        {
                                            bool         isActive;
                                            CallRowState state;

                                            ColorInfo colorInfo =
                                                loadColorInfo(colorNode, out isActive, out state);

                                            if (isActive) activeHandled   = true;
                                            else          inactiveHandled = true;

                                            if (xmlNode.Name.ToLower() == "groups")
                                            {
                                                if (isActive) Phonebook.directoryGroupsColorInfoActive   [(int)state] = colorInfo;
                                                else          Phonebook.directoryGroupsColorInfoInactive [(int)state] = colorInfo;
                                            }
                                            else
                                            {
                                                if (isActive) Phonebook.directoryListColorInfoActive     [(int)state] = colorInfo;
                                                else          Phonebook.directoryListColorInfoInactive   [(int)state] = colorInfo;
                                            }

                                            break;
                                        }
                                        case "background":
                                        {
                                            Settings.ColorInfo backgroundColorInfo =
                                                loadBackgroundColor(colorNode);

                                            if (xmlNode.Name.ToLower() == "groups")
                                            {
                                                Phonebook.directoryGroupBackgroundColorInfo = backgroundColorInfo;
                                            }
                                            else
                                            {
                                                Phonebook.directoryListBackgroundColorInfo  = backgroundColorInfo;
                                            }

                                            break;
                                        }
                                        default:
                                        {
                                            throw new Exception("Expected 'Color' or 'Background under 'Directory." +
                                                xmlNode.Name + "' (found '" + colorNode.Name + "')");
                                        }
                                    }
                                }
                            }

                            // if both active and inactive were not explicitly handled, make them the same
                            if (activeHandled != inactiveHandled)
                            {
                                for (CallRowState state = CallRowState.FirstState;
                                     state != CallRowState.NumStates; ++state)
                                {
                                    if (activeHandled)
                                    {
                                        if (xmlNode.Name.ToLower() == "groups")
                                        {
                                            Phonebook.directoryGroupsColorInfoInactive[(int)state] =
                                                Phonebook.directoryGroupsColorInfoActive[(int)state];
                                        }
                                        else
                                        {
                                            Phonebook.directoryListColorInfoInactive[(int)state] =
                                                Phonebook.directoryListColorInfoActive[(int)state];
                                        }
                                    }
                                    else
                                    {
                                        if (xmlNode.Name.ToLower() == "groups")
                                        {
                                            Phonebook.directoryGroupsColorInfoActive[(int)state] =
                                                Phonebook.directoryGroupsColorInfoInactive[(int)state];
                                        }
                                        else
                                        {
                                            Phonebook.directoryListColorInfoActive[(int)state] =
                                                Phonebook.directoryListColorInfoInactive[(int)state];
                                        }
                                    }
                                }
                            }

                            break;
                        }
                        default:
                        {
                            throw new Exception("Expected 'Directory.Groups' or 'Directory.List' " +
                                "(found 'Directory." + xmlNode.Name + "')");
                        }
                    }
                }
            }
        }

        private void populatePhonebookSettings(ColorEditor colorEditor)
        {
            try
            {
                TreeNode rootBranch      = colorEditor.addNode("Phonebook");
                TreeNode directoryBranch = colorEditor.addNode(rootBranch, "Directory");
                TreeNode groupsBranch    = colorEditor.addNode(directoryBranch, "Groups");
                TreeNode listBranch      = colorEditor.addNode(directoryBranch, "List");

                colorEditor.addNode(groupsBranch, "Background", Phonebook.directoryGroupBackgroundColorInfo);
                colorEditor.addNode(listBranch,   "Background", Phonebook.directoryListBackgroundColorInfo);

                for (CallRowState state = CallRowState.FirstState; state != CallRowState.NumStates; ++state)
                {
                    colorEditor.addNode(groupsBranch, state.ToString(),
                        Phonebook.directoryGroupsColorInfoActive[(int)state]);

                    colorEditor.addNode(listBranch, state.ToString(),
                        Phonebook.directoryListColorInfoActive[(int)state]);
                }

                TreeNode dialpadBranch = colorEditor.addNode(rootBranch, "Dialpad");

                colorEditor.addNode(dialpadBranch, "Normal",  Phonebook.dialpadColorInfo);
                colorEditor.addNode(dialpadBranch, "Hovered", Phonebook.dialpadHoverColorInfo);
            }
            catch (Exception ex)
            {
                Controller.showAlert((Translate)("Error building Color Editor - Phonebook") +
                    " (" + ex.Message + ")", Console.LogType.Warning, Console.LogID.Init);
            }
        }

        private void writePhonebookSettings(ref string xmlText)
        {
            try
            {
                xmlText += "\r\n  <Window name='Phonebook'>\r\n";

                // TODO color: should the phonebook have a general background color?
                //writeBackgroundColorInfo(ref xmlText, "\r\n    ", Phonebook.backgroundColorInfo);

                xmlText += "\r\n    <Directory>\r\n";

                // Groups
                xmlText += "\r\n      <Groups>\r\n";
                writeBackgroundColorInfo(ref xmlText, "\r\n        ",
                    Phonebook.directoryGroupBackgroundColorInfo);

                xmlText += "\r\n";

                for (CallRowState state = CallRowState.FirstState; state != CallRowState.NumStates; ++state)
                {
                    writeColorInfo(ref xmlText, "        ",
                        state, Phonebook.directoryGroupsColorInfoActive[(int)state]);
                }

                xmlText += "\r\n      </Groups>\r\n";

                // List
                xmlText += "\r\n      <List>\r\n";
                writeBackgroundColorInfo(ref xmlText, "\r\n        ",
                    Phonebook.directoryListBackgroundColorInfo);

                xmlText += "\r\n";

                for (CallRowState state = CallRowState.FirstState; state != CallRowState.NumStates; ++state)
                {
                    writeColorInfo(ref xmlText, "        ",
                        state, Phonebook.directoryListColorInfoActive[(int)state]);
                }

                xmlText += "\r\n      </List>\r\n";

                xmlText += "\r\n    </Directory>\r\n";

                xmlText += "\r\n    <Dialpad>\r\n";
                writeColorInfo(ref xmlText, "      ", CallRowState.Normal,  Phonebook.dialpadColorInfo);
                writeColorInfo(ref xmlText, "      ", CallRowState.Hovered, Phonebook.dialpadHoverColorInfo);
                xmlText += "    </Dialpad>\r\n";

                xmlText += "\r\n  </Window>\r\n";
            }
            catch (Exception ex)
            {
                Controller.showAlert((Translate)("Error writing Colors - Phonebook") +
                    " (" + ex.Message + ")", Console.LogType.Warning, Console.LogID.Gui);
            }
        }

        private void loadMessageSettings(XmlNode windowNode, MessageWindow.WindowType type)
        {
            foreach (XmlNode xmlNode in windowNode.ChildNodes)
            {
                if ( ! (xmlNode is XmlComment))
                {
                    int t = (int)type;

                    switch (xmlNode.Name.ToLower())
                    {
                        case "background":
                        {
                            MessageWindow.backgroundColorInfo[t] = loadBackgroundColor(xmlNode);
                            break;
                        }
                        case "dispatcher":
                        {
                            foreach (XmlNode colorNode in xmlNode.ChildNodes)
                            {
                                if ( ! (colorNode is XmlComment))
                                {
                                    MessageWindow.dispatcherColorInfo[t] = loadColorInfo(colorNode);
                                }
                            }

                            break;
                        }
                        case "caller":
                        {
                            foreach (XmlNode colorNode in xmlNode.ChildNodes)
                            {
                                if ( ! (colorNode is XmlComment))
                                {
                                    MessageWindow.callerColorInfo[t] = loadColorInfo(colorNode);
                                }
                            }

                            break;
                        }
                        default:
                        {
                            throw new Exception("Expected 'Background', 'Dispatcher', or 'Caller' " +
                                "(found '" + xmlNode.Name + "')");
                        }
                    }
                }
            }
        }

        private void populateMessageSettings(ColorEditor colorEditor, MessageWindow.WindowType type)
        {
            try
            {
                TreeNode rootBranch = colorEditor.addNode(type.ToString());

                colorEditor.addNode(rootBranch, "Background",
                    MessageWindow.backgroundColorInfo[(int)type]);

                colorEditor.addNode(rootBranch, "Dispatcher",
                    MessageWindow.dispatcherColorInfo[(int)type]);

                colorEditor.addNode(rootBranch, "Caller",
                    MessageWindow.callerColorInfo[(int)type]);
            }
            catch (Exception ex)
            {
                Controller.showAlert((Translate)("Error building Color Editor - " + type.ToString()) +
                    " (" + ex.Message + ")", Console.LogType.Warning, Console.LogID.Init);
            }
        }

        private void writeMessageSettings(ref string xmlText, MessageWindow.WindowType type)
        {
            try
            {
                xmlText += "\r\n  <Window name='" + type.ToString() + "'>\r\n";

                writeBackgroundColorInfo(ref xmlText, "\r\n    ",
                    MessageWindow.backgroundColorInfo[(int)type]);

                xmlText += "\r\n    <Dispatcher>\r\n";
                writeColorInfo(ref xmlText, "      ", MessageWindow.dispatcherColorInfo[(int)type]);
                xmlText += "    </Dispatcher>\r\n";

                xmlText += "\r\n    <Caller>\r\n";
                writeColorInfo(ref xmlText, "      ", MessageWindow.callerColorInfo[(int)type]);
                xmlText += "    </Caller>\r\n";

                xmlText += "\r\n  </Window>\r\n";
            }
            catch (Exception ex)
            {
                Controller.showAlert((Translate)("Error writing Colors - " + type.ToString() + "") +
                    " (" + ex.Message + ")", Console.LogType.Warning, Console.LogID.Gui);
            }
        }

        private void loadIrrSettings(XmlNode windowNode)
        {
            foreach (XmlNode xmlNode in windowNode.ChildNodes)
            {
                if ( ! (xmlNode is XmlComment))
                {
                    switch (xmlNode.Name.ToLower())
                    {
                        case "background":
                        {
                            IRRWindow.backgroundColorInfo = loadBackgroundColor(xmlNode);
                            break;
                        }
                        case "avgwave":
                        {
                            foreach (XmlNode colorNode in xmlNode.ChildNodes)
                            {
                                if ( ! (colorNode is XmlComment)) IRRWindow.avgWavColorInfo = loadColorInfo(colorNode);
                            }

                            break;
                        }
                        case "maxwave":
                        {
                            foreach (XmlNode colorNode in xmlNode.ChildNodes)
                            {
                                if ( ! (colorNode is XmlComment)) IRRWindow.maxWavColorInfo = loadColorInfo(colorNode);
                            }

                            break;
                        }
                        case "unselected":
                        {
                            foreach (XmlNode colorNode in xmlNode.ChildNodes)
                            {
                                if ( ! (colorNode is XmlComment)) IRRWindow.unselectedColorInfo = loadColorInfo(colorNode);
                            }

                            break;
                        }
                        case "location":
                        {
                            foreach (XmlNode colorNode in xmlNode.ChildNodes)
                            {
                                if ( ! (colorNode is XmlComment)) IRRWindow.locationColorInfo = loadColorInfo(colorNode);
                            }

                            break;
                        }
                        default:
                        {
                            throw new Exception("Expected 'Background', 'AvgWave', 'MaxWave', 'Unselected', or 'Location' " +
                                "(found '" + xmlNode.Name + "')");
                        }
                    }
                }
            }
        }

        private void populateIrrSettings(ColorEditor colorEditor)
        {
            try
            {
                TreeNode rootBranch = colorEditor.addNode("IRR");

                colorEditor.addNode(rootBranch, "Background", IRRWindow.backgroundColorInfo);
                colorEditor.addNode(rootBranch, "AvgWave",    IRRWindow.avgWavColorInfo);
                colorEditor.addNode(rootBranch, "MaxWave",    IRRWindow.maxWavColorInfo);
                colorEditor.addNode(rootBranch, "Unselected", IRRWindow.unselectedColorInfo);
                colorEditor.addNode(rootBranch, "Location",   IRRWindow.locationColorInfo);
            }
            catch (Exception ex)
            {
                Controller.showAlert((Translate)("Error building Color Editor - IRR") +
                    " (" + ex.Message + ")", Console.LogType.Warning, Console.LogID.Init);
            }
        }

        private void writeIrrSettings(ref string xmlText)
        {
            try
            {
                xmlText += "\r\n  <Window name='IRR'>\r\n";

                writeBackgroundColorInfo(ref xmlText, "\r\n    ", IRRWindow.backgroundColorInfo);

                // avg wave
                xmlText += "\r\n    <AvgWave>\r\n";
                writeColorInfo(ref xmlText, "      ", IRRWindow.avgWavColorInfo);
                xmlText += "    </AvgWave>\r\n";

                // max wave
                xmlText += "\r\n    <MaxWave>\r\n";
                writeColorInfo(ref xmlText, "      ", IRRWindow.maxWavColorInfo);
                xmlText += "    </MaxWave>\r\n";

                // unselected
                xmlText += "\r\n    <Unselected>\r\n";
                writeColorInfo(ref xmlText, "      ", IRRWindow.unselectedColorInfo);
                xmlText += "    </Unselected>\r\n";

                // location
                xmlText += "\r\n    <Location>\r\n";
                writeColorInfo(ref xmlText, "      ", IRRWindow.locationColorInfo);
                xmlText += "    </Location>\r\n";

                xmlText += "\r\n  </Window>\r\n";
            }
            catch (Exception ex)
            {
                Controller.showAlert((Translate)("Error writing Colors - IRR") +
                    " (" + ex.Message + ")", Console.LogType.Warning, Console.LogID.Gui);
            }
        }

        private void loadPsapStatusSettings(XmlNode windowNode)
        {
            foreach (XmlNode xmlNode in windowNode.ChildNodes)
            {
                if ( ! (xmlNode is XmlComment))
                {
                    if (xmlNode.Name.ToLower().Equals("background"))
                    {
                        PSAPStatus.backgroundColorInfo = loadBackgroundColor(xmlNode);
                    }
                    else if (xmlNode.Name.ToLower().Equals("row"))
                    {
                        PSAPStatus.StatusType statusType = PSAPStatus.StatusType.Available;

                        if (xmlNode.Attributes["type"] != null)
                        {
                            string statusTypeStr = xmlNode.Attributes["type"].Value;

                            switch (statusTypeStr.ToLower())
                            {
                                case "available":   statusType = PSAPStatus.StatusType.Available;   break;
                                case "partial":     statusType = PSAPStatus.StatusType.Partial;     break;
                                case "unavailable": statusType = PSAPStatus.StatusType.Unavailable; break;

                                default: throw new Exception("Expected 'Row.type' to be " +
                                    "'Available', 'Partial', or 'Unavailable' (found '" + statusTypeStr + "')");
                            }
                        }
                        else
                        {
                            throw new Exception("Expected 'type' attribute in 'Row'");
                        }

                        foreach (XmlNode colorNode in xmlNode.ChildNodes)
                        {
                            if ( ! (colorNode is XmlComment))
                            {
                                bool         isActive;
                                CallRowState state;

                                ColorInfo colorInfo =
                                    loadColorInfo(colorNode, out isActive, out state);

                                switch (statusType)
                                {
                                    case PSAPStatus.StatusType.Available:
                                    {
                                        if (state == CallRowState.Normal)
                                        {
                                            PSAPStatus.availableColorInfo = colorInfo;
                                        }
                                        else if (state == CallRowState.Hovered)
                                        {
                                            PSAPStatus.availableHoverColorInfo = colorInfo;
                                        }

                                        break;
                                    }
                                    case PSAPStatus.StatusType.Partial:
                                    {
                                        if (state == CallRowState.Normal)
                                        {
                                            PSAPStatus.partialColorInfo = colorInfo;
                                        }
                                        else if (state == CallRowState.Hovered)
                                        {
                                            PSAPStatus.partialHoverColorInfo = colorInfo;
                                        }

                                        break;
                                    }
                                    case PSAPStatus.StatusType.Unavailable:
                                    {
                                        if (state == CallRowState.Normal)
                                        {
                                            PSAPStatus.unavailableColorInfo = colorInfo;
                                        }
                                        else if (state == CallRowState.Hovered)
                                        {
                                            PSAPStatus.unavailableHoverColorInfo = colorInfo;
                                        }

                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("Expected 'Background' or 'Row' " +
                            "(found '" + xmlNode.Name + "')");
                    }
                }
            }
        }

        private void populatePsapStatusSettings(ColorEditor colorEditor)
        {
            try
            {
                TreeNode rootBranch = colorEditor.addNode("PSAP Status");

                colorEditor.addNode(rootBranch, "Background", PSAPStatus.backgroundColorInfo);

                TreeNode availableBranch   = colorEditor.addNode(rootBranch, "Available");
                TreeNode partialBranch     = colorEditor.addNode(rootBranch, "Partial");
                TreeNode unavailableBranch = colorEditor.addNode(rootBranch, "Unavailable");

                colorEditor.addNode(availableBranch,   "Normal",  PSAPStatus.availableColorInfo);
                colorEditor.addNode(availableBranch,   "Hovered", PSAPStatus.availableHoverColorInfo);

                colorEditor.addNode(partialBranch,     "Normal",  PSAPStatus.partialColorInfo);
                colorEditor.addNode(partialBranch,     "Hovered", PSAPStatus.partialHoverColorInfo);

                colorEditor.addNode(unavailableBranch, "Normal",  PSAPStatus.unavailableColorInfo);
                colorEditor.addNode(unavailableBranch, "Hovered", PSAPStatus.unavailableHoverColorInfo);
            }
            catch (Exception ex)
            {
                Controller.showAlert((Translate)("Error building Color Editor - PSAP Status") +
                    " (" + ex.Message + ")", Console.LogType.Warning, Console.LogID.Init);
            }
        }

        private void writePsapStatusSettings(ref string xmlText)
        {
            try
            {
                xmlText += "\r\n  <Window name='PSAP Status'>\r\n";

                writeBackgroundColorInfo(ref xmlText, "\r\n    ", PSAPStatus.backgroundColorInfo);

                // available
                xmlText += "\r\n    <Row type='Available'>\r\n";
                writeColorInfo(ref xmlText, "      ", CallRowState.Normal,  PSAPStatus.availableColorInfo);
                writeColorInfo(ref xmlText, "      ", CallRowState.Hovered, PSAPStatus.availableHoverColorInfo);
                xmlText += "    </Row>\r\n";

                // partial
                xmlText += "\r\n    <Row type='Partial'>\r\n";
                writeColorInfo(ref xmlText, "      ", CallRowState.Normal,  PSAPStatus.partialColorInfo);
                writeColorInfo(ref xmlText, "      ", CallRowState.Hovered, PSAPStatus.partialHoverColorInfo);
                xmlText += "    </Row>\r\n";

                // unavailable
                xmlText += "\r\n    <Row type='Unavailable'>\r\n";
                writeColorInfo(ref xmlText, "      ", CallRowState.Normal,  PSAPStatus.unavailableColorInfo);
                writeColorInfo(ref xmlText, "      ", CallRowState.Hovered, PSAPStatus.unavailableHoverColorInfo);
                xmlText += "    </Row>\r\n";

                xmlText += "\r\n  </Window>\r\n";
            }
            catch (Exception ex)
            {
                Controller.showAlert((Translate)("Error writing Colors - PSAP Status") +
                    " (" + ex.Message + ")", Console.LogType.Warning, Console.LogID.Gui);
            }
        }

        private void loadAlertsSettings(XmlNode windowNode)
        {
            foreach (XmlNode xmlNode in windowNode.ChildNodes)
            {
                if ( ! (xmlNode is XmlComment))
                {
                    if (xmlNode.Name.ToLower().Equals("background"))
                    {
                        AlarmDisplay.backgroundColorInfo = loadBackgroundColor(xmlNode);
                    }
                    else if (xmlNode.Name.ToLower().Equals("row"))
                    {
                        bool isAlternatingRow = false;

                        XmlAttribute rowType = xmlNode.Attributes["type"];

                        if (rowType != null &&
                            (rowType.Value.ToLower().Equals("alt") ||
                             rowType.Value.ToLower().Equals("alternating")))
                        {
                            isAlternatingRow = true;
                        }

                        foreach (XmlNode colorNode in xmlNode.ChildNodes)
                        {
                            if ( ! (colorNode is XmlComment))
                            {
                                bool         isActive;
                                CallRowState state;

                                ColorInfo colorInfo = loadColorInfo(
                                    colorNode, out isActive, out state);

                                if (isAlternatingRow) AlarmDisplay.altRowColorInfo [(int)state] = colorInfo;
                                else                  AlarmDisplay.rowColorInfo    [(int)state] = colorInfo;
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("Expected 'Background' or 'Row' " +
                            "(found '" + xmlNode.Name + "')");
                    }
                }
            }
        }

        private void populateAlertsSettings(ColorEditor colorEditor)
        {
            try
            {
                TreeNode rootBranch = colorEditor.addNode("Alerts");

                colorEditor.addNode(rootBranch, "Background", AlarmDisplay.backgroundColorInfo);

                TreeNode rowBranch = colorEditor.addNode(rootBranch, "Row");
                TreeNode altBranch = colorEditor.addNode(rootBranch, "Alternate Row");

                for (CallRowState state = CallRowState.FirstState; state != CallRowState.NumStates; ++state)
                {
                    colorEditor.addNode(rowBranch, state.ToString(),
                        AlarmDisplay.rowColorInfo[(int)state]);

                    colorEditor.addNode(altBranch, state.ToString(),
                        AlarmDisplay.altRowColorInfo[(int)state]);
                }
            }
            catch (Exception ex)
            {
                Controller.showAlert((Translate)("Error building Color Editor - Alerts") +
                    " (" + ex.Message + ")", Console.LogType.Warning, Console.LogID.Init);
            }
        }

        private void writeAlertsSettings(ref string xmlText)
        {
            try
            {
                xmlText += "\r\n  <Window name='Alerts'>\r\n";
                writeBackgroundColorInfo(ref xmlText, "\r\n    ", AlarmDisplay.backgroundColorInfo);

                // normal rows
                xmlText += "\r\n    <Row>\r\n";

                for (CallRowState state = CallRowState.FirstState; state != CallRowState.NumStates; ++state)
                {
                    writeColorInfo(ref xmlText, "      ", state, AlarmDisplay.rowColorInfo[(int)state]);
                }

                xmlText += "    </Row>\r\n";

                // alternating rows
                xmlText += "\r\n    <Row type='Alternating'>\r\n";

                for (CallRowState state = CallRowState.FirstState; state != CallRowState.NumStates; ++state)
                {
                    writeColorInfo(ref xmlText, "      ", state, AlarmDisplay.altRowColorInfo[(int)state]);
                }

                xmlText += "    </Row>\r\n";

                xmlText += "\r\n  </Window>\r\n";
            }
            catch (Exception ex)
            {
                Controller.showAlert((Translate)("Error writing Colors - Alerts") +
                    " (" + ex.Message + ")", Console.LogType.Warning, Console.LogID.Gui);
            }
        }

        private ColorInfo loadColorInfo(XmlNode xmlNode)
        {
            bool         isActive;
            CallRowState state;

            return loadColorInfo(xmlNode, out isActive, out state);
        }

        private ColorInfo loadColorInfo(XmlNode xmlNode, out bool isActive)
        {
            CallRowState state;

            return loadColorInfo(xmlNode, out isActive, out state);
        }

        private ColorInfo loadColorInfo(
            XmlNode colorNode, out bool isActive, out CallRowState state)
        {
            if ( ! colorNode.Name.ToLower().Equals("color")       &&
                 ! colorNode.Name.ToLower().Equals("activecolor") &&
                 ! colorNode.Name.ToLower().Equals("inactivecolor"))
            {
                throw new Exception("Expected 'Color', 'ActiveColor', or 'InactiveColor' " +
                                    "(found '" + colorNode.Name + "')");
            }

            isActive   = ! colorNode.Name.ToLower().Equals("inactivecolor");
            state      = CallRowState.Normal;
            bool bold  = false;

            Color background1 = Color.DodgerBlue;
            Color background2 = Color.Black;
            Color foreground  = Color.White;
            Color shadow      = ColorInfo.DEFAULT_SHADOW_COLOR;

            foreach (XmlAttribute attribute in colorNode.Attributes)
            {
                string attribName  = attribute.Name.ToLower();
                string attribValue = attribute.Value.ToLower();

                switch (attribName)
                {
                    case "current":
                    case "active":

                        switch (attribValue)
                        {
                            case "true":     isActive = true;  break;
                            case "false":    isActive = false; break;
                            case "1":        isActive = true;  break;
                            case "0":        isActive = false; break;
                            case "active":   isActive = true;  break;
                            case "inactive": isActive = false; break;
                            case "current":  isActive = true;  break;
                            case "pending":  isActive = false; break;

                            default: throw new Exception("Expected 'Color." + attribute.Name +
                                "' to be 'true' or 'false' (found '" + attribute.Value + "')");
                        }

                        break;

                    case "state":

                        switch (attribValue)
                        {
                            case "normal":   state = CallRowState.Normal;   break;
                            case "hovered":  state = CallRowState.Hovered;  break;
                            case "selected": state = CallRowState.Selected; break;

                            default: throw new Exception("Expected '" + colorNode.Name + "." + attribute.Name +
                                "' to be 'normal', 'hovered', or 'selected' (found '" + attribute.Value + "')");
                        }

                        break;

                    case "selected":

                        switch (attribValue)
                        {
                            case "1":
                            case "true":
                            case "selected": state = CallRowState.Selected; break;

                            case "0":
                            case "false":
                            case "unselected":
                            {
                                if (state == CallRowState.Selected) state = CallRowState.Normal;
                                break;
                            }

                            default: throw new Exception("Expected 'Color." + attribute.Name +
                                "' to be 'true' or 'false' (found '" + attribute.Value + "')");
                        }

                        break;

                    case "background":

                        background1 = Color.FromArgb(unchecked((int)Convert.ToUInt32(attribute.Value, 16)));
                        background2 = background1;
                        break;

                    case "background1":

                        background1 = Color.FromArgb(unchecked((int)Convert.ToUInt32(attribute.Value, 16)));
                        break;

                    case "background2":

                        background2 = Color.FromArgb(unchecked((int)Convert.ToUInt32(attribute.Value, 16)));
                        break;

                    case "foreground":

                        foreground = Color.FromArgb(unchecked((int)Convert.ToUInt32(attribute.Value, 16)));
                        break;

                    case "shadow":

                        shadow = Color.FromArgb(unchecked((int)Convert.ToUInt32(attribute.Value, 16)));
                        break;

                    case "bold":

                        switch (attribValue)
                        {
                            case "true":    bold = true;  break;
                            case "false":   bold = false; break;
                            case "1":       bold = true;  break;
                            case "0":       bold = false; break;
                            case "bold":    bold = true;  break;
                            case "normal":  bold = false; break;
                            case "regular": bold = false; break;

                            default: throw new Exception("Expected 'Color." + attribute.Name +
                                "' to be 'true' or 'false' (found '" + attribute.Value + "')");
                        }

                        break;

                    default: throw new Exception(
                        "Expected '" + colorNode.Name +"' attributes 'state', 'background', background1, " +
                        "'background2', 'foreground', 'shadow', or 'bold' (found '" + attribute.Value + "')");
                }
            }

            return new ColorInfo(background1, background2, foreground, shadow, bold);
        }

        private ColorInfo loadBackgroundColor(XmlNode backgroundNode)
        {
            if ( ! backgroundNode.Name.ToLower().Equals("background"))
            {
                throw new Exception("Expected 'Background' (found '" + backgroundNode.Name + "')");
            }

            Color background1 = Color.White;
            Color background2 = Color.White;

            foreach (XmlAttribute attribute in backgroundNode.Attributes)
            {
                string attribName  = attribute.Name.ToLower();
                string attribValue = attribute.Value.ToLower();

                switch (attribName)
                {
                    case "color":

                        background1 = Color.FromArgb(unchecked((int)Convert.ToUInt32(attribute.Value, 16)));
                        background2 = background1;
                        break;

                    case "color1":

                        background1 = Color.FromArgb(unchecked((int)Convert.ToUInt32(attribute.Value, 16)));
                        break;

                    case "color2":

                        background2 = Color.FromArgb(unchecked((int)Convert.ToUInt32(attribute.Value, 16)));
                        break;

                    default: throw new Exception(
                        "Expected 'Background' attributes 'color', 'color1', or 'color2' (found '" + attribute.Value + "')");
                }
            }

            return new ColorInfo(background1, background2, Color.Black, false);
        }

        private void writeBackgroundColorInfo(ref string xmlText, string spaces, ColorInfo colorInfo)
        {
            xmlText += spaces + "<Background";

            if (colorInfo.backgroundColor1.Equals(colorInfo.backgroundColor2))
            {
                xmlText += " color='0x" + colorInfo.backgroundColor1.ToArgb().ToString("X") + "'";
            }
            else
            {
                xmlText += " color1='0x" + colorInfo.backgroundColor1.ToArgb().ToString("X") + "'";
                xmlText += " color2='0x" + colorInfo.backgroundColor2.ToArgb().ToString("X") + "'";
            }

            xmlText += " />\r\n";
        }

        private void writeColorInfo(ref string xmlText, string spaces, ColorInfo colorInfo)
        {
            xmlText += spaces + "<Color";
            writeColorInfo(ref xmlText, colorInfo);
        }

        private void writeColorInfo(ref string xmlText, string spaces, CallRowState state, ColorInfo colorInfo)
        {
            xmlText += spaces + "<Color";
            writeCallRowState(ref xmlText, state);
            writeColorInfo(ref xmlText, colorInfo);
        }

        private void writeColorInfo(ref string xmlText, string spaces, bool active, ColorInfo colorInfo)
        {
            xmlText += spaces + "<" + (active ? "ActiveColor  " : "InactiveColor");
            writeColorInfo(ref xmlText, colorInfo);
        }

        private void writeColorInfo(ref string xmlText, string spaces, bool active, CallRowState state, ColorInfo colorInfo)
        {
            xmlText += spaces + "<" + (active ? "ActiveColor  " : "InactiveColor");
            writeCallRowState(ref xmlText, state);
            writeColorInfo(ref xmlText, colorInfo);
        }

        private void writeCallRowState(ref string xmlText, CallRowState state)
        {
            switch (state)
            {
                case CallRowState.Normal:   xmlText += " state='normal'  "; break;
                case CallRowState.Hovered:  xmlText += " state='hovered' "; break;
                case CallRowState.Selected: xmlText += " state='selected'"; break;
            }
        }

        private void writeColorInfo(ref string xmlText, ColorInfo colorInfo)
        {
            if (colorInfo.backgroundColor1.Equals(colorInfo.backgroundColor2))
            {
                xmlText += " background='0x" + colorInfo.backgroundColor1.ToArgb().ToString("X") + "'";
            }
            else
            {
                xmlText += " background1='0x" + colorInfo.backgroundColor1.ToArgb().ToString("X") + "'";
                xmlText += " background2='0x" + colorInfo.backgroundColor2.ToArgb().ToString("X") + "'";
            }

            xmlText += " foreground='0x" + colorInfo.foregroundColor. ToArgb().ToString("X") + "'";
            xmlText += " bold='"         + colorInfo.bold.            ToString().ToLower()   + "'";

            if ( ! colorInfo.shadowColor.Equals(ColorInfo.DEFAULT_SHADOW_COLOR))
            {
                xmlText += " shadow='0x" + colorInfo.shadowColor.ToArgb().ToString("X") + "'";
            }

            xmlText += " />\r\n";
        }

        public void SettingsReceived(XmlDocument xDocGeneral, Console console, DataModel dataModel)
        {
            try
            {
                console.clearSpeedDialButtons();

                if (xDocGeneral["Event"] == null || xDocGeneral["Event"]["Initialization"] == null) return;

                XmlElement initXML = xDocGeneral["Event"]["Initialization"];

                // General
                if (initXML["General"] != null)
                {
                    if (initXML["General"]["GeneralSetup"] != null)
                    {
                        foreach (XmlAttribute attr in initXML["General"]["GeneralSetup"].Attributes)
                        {
                            generalSettings.Add(attr.Name, attr.Value);
                        }
                    }

                    if (initXML["General"]["AppletSetup"] != null)
                    {
                        foreach (XmlAttribute attr in initXML["General"]["AppletSetup"].Attributes)
                        {
                            generalSettings.Add(attr.Name, attr.Value);
                        }
                    }

                    if (initXML["General"]["ButtonSetup"] != null)
                    {
                        foreach (XmlAttribute attr in initXML["General"]["ButtonSetup"].Attributes)
                        {
                            generalSettings.Add(attr.Name, attr.Value);
                        }
                    }

                    if (initXML["General"]["TabSetup"] != null)
                    {
                        foreach (XmlAttribute attr in initXML["General"]["TabSetup"].Attributes)
                        {
                            generalSettings.Add("TabSetup." + attr.Name, attr.Value);
                        }
                    }
                }

                // Server
                if (initXML["Server"] != null)
                {
                    foreach (XmlNode node in initXML["Server"].ChildNodes)
                    {
                        if (node is XmlComment) continue;

                        if (node.Name == "ServerSetup")
                        {
                            if (node.Attributes["IP_Address"] == null)
                            {
                                Controller.sendAlertToController(
                                    "XML.Server.ServerSetup.IP_Address was null",
                                    Console.LogType.Warning, Console.LogID.Init);
                            }
                            else
                            {
                                serverList.Add(node.Attributes["IP_Address"].Value);
                            }
                        }
                        else
                        {
                            Controller.sendAlertToController(
                                "XML.Server." + node.Name + " was unexpected (expected XML.Server.ServerSetup)",
                                Console.LogType.Warning, Console.LogID.Init);
                        }
                    }
                }

                // SIP Phone
                if (initXML["SIPphone"] != null)
                {
                    if (initXML["SIPphone"]["SIPphoneSetup"] != null)
                    {
                        foreach (XmlAttribute attr in initXML["SIPphone"]["SIPphoneSetup"].Attributes)
                        {
                            sipPhoneSettings.Add(attr.Name, attr.Value);
                        }
                    }
                }

                // TDD
                if (initXML["TDD"] != null)
                {
                    if (initXML["TDD"]["TDDSetup"] != null)
                    {
                        foreach (XmlAttribute attr in initXML["TDD"]["TDDSetup"].Attributes)
                        {
                            TDDSettings.Add(attr.Name, attr.Value);
                        }
                    }

                    TDD_CannedMessages = initXML["TDD"]["TDDMessageList"].Clone();
                }

                // SMS
                if (initXML["SMS"] != null)
                {
                    if (initXML["SMS"]["SMSsetup"] != null)
                    {
                        foreach (XmlAttribute attr in initXML["SMS"]["SMSsetup"].Attributes)
                        {
                            SMSSettings.Add(attr.Name, attr.Value);
                        }
                    }

                    SMS_CannedMessages = initXML["SMS"]["SMSMessageList"].Clone();
                }

                // Phonebook
                if (initXML["Phonebook"] != null)
                {
                    if (initXML["Phonebook"]["PhonebookSetup"] != null)
                    {
                        foreach (XmlAttribute attr in initXML["Phonebook"]["PhonebookSetup"].Attributes)
                        {
                            PhonebookSettings.Add(attr.Name, attr.Value);
                        }
                    }

                    // Phonebook List
                    if (initXML["Phonebook"]["PhonebookList"] != null)
                    {
                        XmlNodeList childNodes = initXML["Phonebook"]["PhonebookList"].ChildNodes;

                        foreach (XmlNode node in childNodes)
                        {
                            if ( ! (node is XmlComment) && ! node.HasChildNodes)
                            {
                                XmlAttribute nameAttr      = node.Attributes["Name"];
                                XmlAttribute numberAttr    = node.Attributes["Number"];
                                XmlAttribute groupAttr     = node.Attributes["Group"];
                                XmlAttribute codeAttr      = node.Attributes["Code"];
                                XmlAttribute iconFileAttr  = node.Attributes["icon_file"];
                                XmlAttribute speedDialAttr = node.Attributes["Speed_Dial_Action"];

                                string number          = numberAttr    != null ? numberAttr.    Value : "";
                                string name            = nameAttr      != null ? nameAttr.      Value : number;
                                string group           = groupAttr     != null ? groupAttr.     Value : "All";
                                string iconFile        = iconFileAttr  != null ? iconFileAttr.  Value : "";
                                string speedDialAction = speedDialAttr != null ? speedDialAttr. Value : "none";
                                int    code            = 0;

                                try
                                {
                                    if (codeAttr != null) code = Convert.ToInt32(codeAttr.Value);
                                }
                                catch (Exception ex)
                                {
                                    Controller.sendAlertToController(
                                        "Phonebook entry 'Code' was invalid for XML.Phonebook.PhonebookList." +
                                        name + " (" + ex.Message + ")", Console.LogType.Warning, Console.LogID.Init);
                                    code = 0;
                                }

                                if (numberAttr == null)
                                {
                                    Controller.sendAlertToController(
                                        "Phonebook number was null for XML.Phonebook.PhonebookList." + name,
                                        Console.LogType.Warning, Console.LogID.Init);

                                    continue;
                                }

                                PhonebookEntry phonebookEntry = new PhonebookEntry(
                                    name, number, group, code, createTempFile(iconFile), speedDialAction);

                                PhonebookList.Add(phonebookEntry);

                                // add speed dial button
                                switch (speedDialAction.ToLower())
                                {
                                    case "blind_transfer":
                                    case "attended_transfer":
                                    case "conference_transfer":
                                    case "tandem_transfer":
                                    case "dial_out":
                                    {
                                        console.addSpeedDialButton(phonebookEntry);
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    // Group List
                    if (initXML["Phonebook"]["GroupList"] != null)
                    {
                        XmlNodeList childNodes = initXML["Phonebook"]["GroupList"].ChildNodes;

                        foreach (XmlNode node in childNodes)
                        {
                            if ( ! (node is XmlComment) && ! node.HasChildNodes)
                            {
                                XmlAttribute nameAttr     = node.Attributes["Name"];
                                XmlAttribute priorityAttr = node.Attributes["Priority"];
                                XmlAttribute iconFileAttr = node.Attributes["icon_file"];

                                if (nameAttr == null)
                                {
                                    Controller.sendAlertToController(
                                        "Phonebook group entry 'Name' was null in XML.Phonebook.GroupList",
                                        Console.LogType.Warning, Console.LogID.Init);

                                    continue;
                                }

                                string name = nameAttr.Value;

                                string iconFile = iconFileAttr != null ? iconFileAttr.Value : "";
                                Int32  priority = int.MaxValue;

                                try
                                {
                                    if (priorityAttr != null) priority = Convert.ToInt32(priorityAttr.Value);
                                }
                                catch (Exception ex)
                                {
                                    Controller.sendAlertToController(
                                        "Phonebook group entry 'Priority' was invalid for XML.Phonebook.GroupList." +
                                        name + " (" + ex.Message + ")", Console.LogType.Warning, Console.LogID.Init);

                                    priority = int.MaxValue;
                                }

                                GroupList.Add(new GroupEntry(name, priority, createTempFile(iconFile)));
                            }
                        }
                    }
                }

                // Line View Rows
                if (initXML["LineView"] != null)
                {
                    Controller.lineNumberDictionary.Clear();

                    XmlNodeList childNodes = initXML["LineView"].ChildNodes;

                    int nodeIndex = 0;

                    foreach (XmlNode node in childNodes)
                    {
                        if ( ! (node is XmlComment) && ! node.HasChildNodes && node.Name == "LineViewRow")
                        {
                            ++nodeIndex;

                            XmlAttribute rowAttr     = node.Attributes["Row"];
                            XmlAttribute displayAttr = node.Attributes["Display"];

                            string row     = rowAttr     != null ? rowAttr.     Value : "";
                            string display = displayAttr != null ? displayAttr. Value : "Line " + row;

                            if (rowAttr == null)
                            {
                                Controller.sendAlertToController(
                                    "Row number was null for XML.LineView.LineViewRow element " + nodeIndex,
                                    Console.LogType.Warning, Console.LogID.Init);

                                continue;
                            }

                            Controller.lineNumberDictionary[Convert.ToUInt16(row)] = display;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error with Settings Received:" + ex.Message);
            }
        }

        private static Dictionary<string, string> iconFileDictionary =  new Dictionary<string, string>();
        private static int ICO_ID = 1000;

        private string createTempFile(string uriStr)
        {
            if (uriStr == null || uriStr.Length == 0 || uriStr.Equals("none")) return null;

            try
            {
                if (iconFileDictionary.ContainsKey(uriStr))
                {
                    return iconFileDictionary[uriStr];
                }

                string tempPath = Path.GetTempPath() + "WestTel\\icons\\phonebook\\";
                Directory.CreateDirectory(tempPath);

                int indexOfIcoName = uriStr.LastIndexOfAny(new char[]{'\\','/','='}) + 1;

                ++ICO_ID;
                tempPath += ICO_ID.ToString() + "_" + uriStr.Substring(indexOfIcoName);

                try { if (File.Exists(tempPath)) File.Delete(tempPath); } catch {}

                // if local (i.e. "C:\Dev\911\data\test\911.ico"), no need to download (used for testing)
                if (uriStr[1] == ':')
                {
                    File.Copy(uriStr, tempPath, true);
                }
                else
                {
                    using (WebClient webClient = new WebClient())
                    {
                        webClient.DownloadFile(uriStr, tempPath);
                    }
                }

                // store in dictionary
                iconFileDictionary.Add(uriStr, tempPath);

                return tempPath;
            }
            catch (Exception ex)
            {
                try
                {
                    Logging.ExceptionLogger("Failed to download [" + uriStr + "]", ex, Console.LogID.Gui);
                }
                catch {}
            }

            // store in dictionary
            iconFileDictionary.Add(uriStr, null);

            return null;
        }
    }


    public class Logging
    {
        public static void ExceptionLogger(string Comment, Exception ex, Console.LogID logId)
        {
            string str = "Comment: " + Comment + "\nMessage: " + ex.Message + "\nStack: " + ex.StackTrace;

            Controller.sendAlertToController(
                System.Security.SecurityElement.Escape(str), Console.LogType.Warning, logId);

            Controller.DebugWindowMsg(str);

            if (ex.InnerException != null) ExceptionLogger("InnerException", ex.InnerException, logId);
        }

        public static void AppTrace (string Text)
        {
            System.Diagnostics.Trace.WriteLine(Text);
        }
    }
}
