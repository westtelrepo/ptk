using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;

namespace WestTel.E911
{
	/// <summary>
	/// The listener thread for the remote connection
	/// </summary>

	public class Client 
	{             
		public class ClientDisconnected : ApplicationException
		{
			public ClientDisconnected() : base("Client Connection Terminated") {}
		}

        // example header "\nContent-Length:       5750\nContent-Type:             text/xml\n\n"

        public static string HDR_LENGTH_STR  = "\nContent-Length:"; // 16
        public static int    HDR_LENGTH_SIZE = 11;
        public static string HDR_TYPE_STR    = "\nContent-Type:";   // 14
        public static int    HDR_TYPE_SIZE   = 21;
        public static string HDR_DELIMITER   = "\n\n"; // 2

        public static int HDR_TOTAL_SIZE = 16 + HDR_LENGTH_SIZE + 14 + HDR_TYPE_SIZE + 2;


		// Private Attributes
        private bool useThreads = true;
		private Socket udpSocket = null;
		private EndPoint endpoint;
		private bool KeepAlive = true;
        private System.Threading.Timer timer = null;
		private string name;
		public static int ClientCount = 0;
		public int ID;

        //// Public Methods
        //public Socket UDPSocket
        //{
        //    get { return udpSocket;}
        //    set { udpSocket = value;}
        //}

		public EndPoint EndPoint
		{
			get {return endpoint;}
			set {endpoint = value;}
		}

		public string Name
		{
			get {return name;}
			set {name = value;}
		}

        private System.Collections.Generic.List<IPAddress> serverIPAddressList = null;
        private int                                        udpListenPort;
        private string                                     tcpRemoteIPAddress;
        private int                                        tcpRemotePort;
        private int                                        sendBufferSize;
        private int                                        recvBufferSize;

		public delegate void OnDataReceived(Client sender, string Data);
		public event OnDataReceived DataReceived;

		public delegate void OnClientError(Client sender, Exception ex);
		public event OnClientError ClientError;

		// public delegate void OnDisconnect(Client sender);
		// public event OnDisconnect Disconnect;

		/// <summary>
		/// Instantiates a new client connection class to a passed socket.
		/// </summary>
		/// <param name="Socket"></param>
		public Client(
            int    _udpListenPort,
            string _tcpRemoteIPAddress,
            int    _tcpRemotePort,
            int    _sendBufferSize,
            int    _recvBufferSize)  // : base()
		{
            udpListenPort      = _udpListenPort;
            tcpRemoteIPAddress = _tcpRemoteIPAddress;
            tcpRemotePort      = _tcpRemotePort;
            sendBufferSize     = _sendBufferSize;
            recvBufferSize     = _recvBufferSize;
        }

		// METHOD:
		//	SendToClient
		// DESCRIPTION:
		//	This method sends a string to the client.
		// PARAMETERS:
		//	Data (string)
		// RETURN:
		//	void
		//
		/// <summary>
		/// Sends the passed string to the remote client
		/// </summary>
		/// <param name="Data"></param>
		public void SendToClient(string Data)
		{
			try
			{
				byte[] buffer = System.Text.Encoding.ASCII.GetBytes(Data);
			}
			catch(Exception e)
			{
				ClientError(this, e);
				Logging.ExceptionLogger("Error in Messaging.Client.SendToClient", e, Console.LogID.Connection);
			}
		}

		//
		// METHOD:
		//	Start
		// DESCRIPTION:
		//	
		// PARAMETERS:
		//	
		// RETURN:
		//	void
		//
		/// <summary>
		/// Creates a new thread and starts listening for incoming messages
		/// </summary>
                 
		public void Start()
		{
            Client.ClientCount++;

            if (useThreads)
            {
                KeepAlive = true;

			    Thread thread = new Thread(new ThreadStart(Receiver));
                thread.Name = "Client " + Client.ClientCount.ToString() + " Thread";
			    //	thread.Name = "ThreadID:" + ID.ToString();

                thread.Start();
            }
            else
            {
                if (timer == null)
                {
                    timer = new System.Threading.Timer(new TimerCallback(Receiver), null, 50, Timeout.Infinite);
                }
            }
		}

		//
		// METHOD:
		//	Stop
		// DESCRIPTION:
		//	
		// PARAMETERS:
		//	
		// RETURN:
		//	void
		//               
		/// <summary>
		/// Stops the client from listening for incoming messages
		/// </summary>
    
		public void Stop()
		{
			// Stop listening for messages
			
			try
			{
                if (timer != null) timer.Dispose();
                timer = null;

				KeepAlive = false;

				if (udpSocket != null)
                {
                    udpSocket.Close();
                    udpSocket = null;
                }

				//if (udpSocket != null && udpSocket.Connected)
				//	udpSocket.Shutdown(SocketShutdown.Both);

				// udpSocket.Close();
				// udpSocket = null;
				//thread.Abort();;
			}
			catch (Exception ex)
            {
                ClientError(this, ex);
                udpSocket = null;
            }
		}

        private bool     firstMsgFromInvalidSource    = true;
        private UInt64   nMsgsFromInvalidSource       = 0;
        private DateTime invalidSourceMsgCounterStart = DateTime.Now;

        // Private Methods
		//
		// METHOD:
		//	Receiver
		// DESCRIPTION:
		//	This is the method that waits for a recieved buffer, processes it into an ASCII string, and calls Processor to handle whatever was sent.

		// PARAMETERS:
		//	
		// RETURN:
		//	void
		//
		/// <summary>
		///		Waits (via thread) for an incoming message and raises event when received
		/// </summary>

        private byte[] bytes = new byte[0x100000]; // 1 MiB
         
		private void Receiver(object unusedObj)
        {
            Receiver();

            if (timer != null) timer.Change(50, Timeout.Infinite);
         }

        private void Receiver()
		{
            try
            {
                if (udpSocket == null && Controller.protocol != ProtocolType.Tcp)
                {
                    udpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                    udpSocket.DontFragment = false;
                    udpSocket.ReceiveBufferSize = recvBufferSize;
                    udpSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);

                    udpSocket.Bind(new IPEndPoint(IPAddress.Any, udpListenPort));
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(null, e.Message, Application.ProductName,
                    System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }

            // Blocks until a message returns on this socket from a remote host.
            do
            {
                try
                {
                    int nBytesReceived = 0;

                    if (Controller.protocol == ProtocolType.Tcp)
                    {
                        if (Console.socket == null || ! Console.socket.Connected)
                        {
                            Thread.Sleep(100);
                            continue;
                        }
                    }

                    while (nBytesReceived < HDR_TOTAL_SIZE)
                    {
                        if (Controller.protocol == ProtocolType.Tcp)
                        {
                            // get up to HDR_TOTAL_SIZE bytes
                            nBytesReceived += Console.socket.Receive(
                                bytes, nBytesReceived, HDR_TOTAL_SIZE - nBytesReceived, SocketFlags.None);
                        }
                        else
                        {
                            // get datagrams until there is at least HDR_TOTAL_SIZE bytes
                            nBytesReceived += udpReadFromSocket(
                                bytes, nBytesReceived, bytes.Length - nBytesReceived);
                        }
                    }

                    // parse message header
                    string header = Encoding.ASCII.GetString(bytes, 0, HDR_TOTAL_SIZE);
                    string errmsg = null;

                    if (header.StartsWith(HDR_LENGTH_STR))
                    {
                        if (header.EndsWith(HDR_DELIMITER))
                        {
                            int dataSize = Convert.ToInt32(
                                header.Substring(HDR_LENGTH_STR.Length, HDR_LENGTH_SIZE));

                            // TODO xml: verify Type (assume XML for now)

                            while (nBytesReceived - HDR_TOTAL_SIZE < dataSize)
                            {
                                if (Controller.protocol == ProtocolType.Tcp)
                                {
                                    int dataAlreadyReceived = nBytesReceived - HDR_TOTAL_SIZE;

                                    nBytesReceived += Console.socket.Receive(
                                        bytes, nBytesReceived, dataSize - dataAlreadyReceived, SocketFlags.None);
                                }
                                else
                                {
                                    nBytesReceived += udpReadFromSocket(
                                        bytes, nBytesReceived, bytes.Length - nBytesReceived);
                                }
                            }

                            string data = Encoding.ASCII.GetString(bytes, HDR_TOTAL_SIZE, dataSize);
                            DataReceived(this, data);
                        }
                        else
                        {
                            errmsg = "expected it to end with \"" + HDR_DELIMITER + "\"";
                        }
                    }
                    else
                    {
                        errmsg = "expected it to start with \"" + HDR_LENGTH_STR + "\"";
                    }

                    if (errmsg != null)
                    {
                        string additionalMsg = "";

                        if (Controller.protocol == ProtocolType.Tcp)
                        {
                            int nBytesCleared = 0; 

                            // clear the socket
                            while (Console.socket.Available != 0)
                            {
                                // TODO xml: instead of clearing all the data on the socket, look for the next header?
                                nBytesCleared += Console.socket.Receive(bytes);
                            }

                            if (nBytesCleared != 0) additionalMsg = " (dropping " + nBytesCleared.ToString() + " bytes to clear the socket)";
                        }

                        string str = "Received invalid header \"" + header + "\" (" + errmsg + ")" + additionalMsg;
                        str = str.Replace("\n", "<\\n>");

                        Controller.sendAlertToController(str, Console.LogType.Warning, Console.LogID.Connection);
                        Controller.DebugWindowMsg(str);
                    }
                }
                catch (Exception ex)
                {
                    if (KeepAlive)
                    {
                        // if not TCP, try to re-establish the socket
                        if (Controller.protocol != ProtocolType.Tcp && (udpSocket == null || ! udpSocket.IsBound))
                        {
                            if (udpSocket != null) udpSocket.Close();

                            Thread.Sleep(1000);

                            udpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                            udpSocket.DontFragment = false;
                            udpSocket.ReceiveBufferSize = recvBufferSize;
                            udpSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);

                            udpSocket.Bind(new IPEndPoint(IPAddress.Any, udpListenPort));
                        }
                        else
                        {
                            Console.closeSocket();
                        }
                    }

                    System.Diagnostics.Debug.WriteLine(ex.Message);
    				Logging.ExceptionLogger("Error in Messaging.Client.Receiver", ex, Console.LogID.Connection);
                    Thread.Sleep(100);
                }
			}
            while (useThreads && KeepAlive);

		} // Receiver

        private int udpReadFromSocket(byte[] bytes, int offset, int size)
        {
            EndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
            int nBytesReceived = udpSocket.ReceiveFrom(bytes, offset, size, SocketFlags.None, ref remoteEndPoint);

            if (nBytesReceived == 0) throw new Exception("connection closed");

            // if this message is from an invalid source based on the firewall (count it and drop it on the floor)
            if (serverIPAddressList != null && ! serverIPAddressList.Contains(((IPEndPoint)remoteEndPoint).Address))
            {
                if (firstMsgFromInvalidSource)
                {
                    firstMsgFromInvalidSource = false;

                    string firstPartOfData = Encoding.ASCII.GetString(bytes, 0, System.Math.Min(100, nBytesReceived));
                    if (nBytesReceived > 100) firstPartOfData += " ...";

                    string warningStr = (Translate)("Ignoring unexpected IP Address: ") +
                        ((IPEndPoint)remoteEndPoint).Address.ToString() + (Translate)(" - message content:\n") + firstPartOfData;

                    Controller.showAlert(warningStr, Console.LogType.Warning, Console.LogID.Connection);
                }

                ++nMsgsFromInvalidSource;

                // if the counter has been going for a day or more, report it and reset the counter
                TimeSpan counterDuration = DateTime.Now.Subtract(invalidSourceMsgCounterStart);

                if (counterDuration.TotalSeconds >= 86400)
                {
                    string warningStr = nMsgsFromInvalidSource.ToString() +
                        " messages have been received from unexpected IP addresses over the past " +
                        counterDuration.Hours.ToString() + " hours";

                    if (counterDuration.Minutes != 0) warningStr += " " + counterDuration.Minutes.ToString() + " minutes";
                    if (counterDuration.Seconds != 0) warningStr += " " + counterDuration.Seconds.ToString() + " seconds";

                    Controller.sendAlertToController(warningStr, Console.LogType.Warning, Console.LogID.Connection);

                    nMsgsFromInvalidSource       = 0;
                    invalidSourceMsgCounterStart = DateTime.Now;
                }

                return 0;
            }

            return nBytesReceived;
        }

        public bool addServerIPAddress(IPAddress ipAddress)
        {
            if (serverIPAddressList == null) serverIPAddressList = new System.Collections.Generic.List<IPAddress>();

            if ( ! serverIPAddressList.Contains(ipAddress))
            {
                serverIPAddressList.Add(ipAddress);
                return true;
            }

            return false;
        }
    }
}