using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Diagnostics;
using WestTel.E911;


namespace WestTel.E911
{
	public enum LogLevel
	{
		Errors = 0,
		Information = 1,
		Connections = 2,
		Messages = 3
	}

	public class Server 
	{             

		// Private Attributes
		private ArrayList connections = new ArrayList();

		// private ConnectionManager cManager;

		// Public Events
		public delegate void OnClientDataReceived(string Data);
		public event OnClientDataReceived ClientDataReceived;

		// public delegate void OnClientConnection(string  Sender);
		// public event OnClientConnection ClientConnection;

		public delegate void OnClientError(Client sender, Exception Ex);
		public event OnClientError ClientError;

		public delegate void OnServerMessage(int Level, string Message);
		public event OnServerMessage ServerMessage;
		


		// Public Methods
		//
		// METHOD:
		//		Create
		// DESCRIPTION:
		//		This singlton returns the Server instance.  it is the only public constructor.
		// PARAMETERS:
		//		None
		// RETURN:
		//		Single instance of the server.
		//                    
		public static readonly Server Instance = new Server();

		// constructor
		private Server(){}  


		// METHOD:
		//		Start
		// DESCRIPTION:
		//		This method starts the connection manager.  It instantiates a new 
		//		tcplistener class and invokes it's start method.
		// PARAMETERS:
		//		None
		// RETURN:
		//		None
		//            
		public void Start(
            int    udpListenPort,
            string tcpRemoteIPAddress,
            int    tcpRemotePort,
            int    sendBufferSize,
            int    recvBufferSize
        )
		{
			ServerMessage((int) LogLevel.Information,"Starting Server");

            AddConnection(udpListenPort, tcpRemoteIPAddress, tcpRemotePort, sendBufferSize, recvBufferSize);

			// Create Connection Manager
			// cManager= ConnectionManager.Create(PortNumber);
            

			// Listen to Connection events
//			cManager.ConnectionReceived += new ConnectionManager.OnConnectionReceived(AddConnection);
			// Start listening for new connections
			// cManager.Start();
		}

		
		
		//
		// METHOD:
		//	Stop
		// DESCRIPTION:
		//	
		// PARAMETERS:
		//	
		// RETURN:
		//	void
		//                       
		public void Stop(  )
		{
			Logging.AppTrace("Stopping Server");

            foreach (Client client in connections) client.Stop();

            connections.Clear();
            
            // Stop accepting new connections
			// cManager.Stop();
		}


//		//
//		// METHOD:
//		//	Broadcast
//		// DESCRIPTION:
//		//	
//		// PARAMETERS:
//		//	string Message
//		// RETURN:
//		//	void
//		//                       
//		public void SendToClient(Client recipient, string Data)
//		{
//			recipient.SendToClient(Data);
//		}

	
		//
		// METHOD:
		//	AddConnection
		// DESCRIPTION:
		//	handles a new connection coming from the connection manager
		// PARAMETERS:
		//	Client Connection (socket)
		// RETURN:
		//	void
		//      
                 
		public void AddConnection(
            int    udpListenPort,
            string tcpRemoteIPAddress,
            int    tcpRemotePort,
            int    sendBufferSize,
            int    recvBufferSize
        )
		{
			try
			{
//				ClientConnection(Connection);

				Client client = new Client(
                    udpListenPort, tcpRemoteIPAddress, tcpRemotePort, sendBufferSize, recvBufferSize);

				client.DataReceived +=new Client.OnDataReceived(client_DataReceived);
				// client.Disconnect +=new Messaging.Client.OnDisconnect(client_Disconnect);
				client.ClientError +=new Client.OnClientError(client_ClientError);
				client.Start();
				connections.Add(client);
				//Trace.Write("+");
			}
			catch (Exception ex)
			{
				Logging.AppTrace("Error Adding Connection" + ex.Message);
				Logging.ExceptionLogger("Error in Server.AddConnection", ex, Console.LogID.Init);
			}
		}
        

		private void client_ClientError(Client sender, Exception ex)
		{
			ClientError(sender, ex);
			//ServerMessage("Client Error - " + sender.Name + ":" + ex.Message);
		}

		private void client_DataReceived(Client Sender, string Data)
		{
			ClientDataReceived(Data);
		}
		
        /*
		public void RemoveConnection(Client client)
		{
			//Trace.Write("~");
			if (client != null && client.Socket != null && client.Socket.Connected)
				client.Stop();
			connections.Remove(client);
			client = null;
		}
         */

        /*
		private void client_Disconnect(Client sender)
		{
			RemoveConnection(sender);
		}
         */

        public void addServerIPAddress(IPAddress ipAddress)
        {
            bool added = false;

            foreach (Client client in connections)
            {
                added |= client.addServerIPAddress(ipAddress);
            }

            // Bill asked that this be removed - 28 Nov 2012
            //if (added)
            //{
            //    Controller.sendAlertToController("Added " + ipAddress.ToString() + " as a valid IP address",
            //        Console.LogType.Info, Console.LogID.Connection);
            //}
        }
    }
	// END CLASS DEFINITION Server


}