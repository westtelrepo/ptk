using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Threading;

namespace WestTel.E911
{
    /// <summary>
    /// Represents the data used for the system.
    /// </summary>
    public class DataModel
    {
        public  DataSet   ds;
        private DataTable dt;
        private DataTable alarmEvents;
        private DataTable psapStatusEvents;

        public static object callEventsSyncRoot       = new object();
        public static object alarmEventsSyncRoot      = new object();
        public static object psapStatusEventsSyncRoot = new object();
        public static object messageQueueSyncRoot     = new object();

        public bool DebugMode = false;
        private long controllerOffsetTicks = 0;

        public delegate void OnTDDStringReceived (string callId, string str);
        public delegate void OnTDDdispatcherSays (string callId, string str);
        public delegate void OnTDDCallerSays     (string callId, string str);
        public delegate void OnTDDMuteChanged    (string callId, bool   mute);

        public event OnTDDStringReceived TDDStringReceived;
        public event OnTDDdispatcherSays TDDdispatcherSays;
        public event OnTDDCallerSays     TDDCallerSays;
        public event OnTDDMuteChanged    TDDMuteChanged;

        public delegate void OnSMSStringReceived (string callId, string str);
        public delegate void OnSMSdispatcherSays (string callId, string str);
        public delegate void OnSMSCallerSays     (string callId, string str);

        public event OnSMSStringReceived SMSStringReceived;
        public event OnSMSdispatcherSays SMSdispatcherSays;
        public event OnSMSCallerSays     SMSCallerSays;

        public delegate void OnRingingCallTimedOut(DataRow row);
        public event OnRingingCallTimedOut RingingCallTimedOut;

        public delegate void OnLogLockInfo(string s);
        public static event OnLogLockInfo LogLockInfo;

        private Dictionary<ushort, DataRow> lineNumberDictionary = new Dictionary<ushort, DataRow>();

        public DataModel()
        {
logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (callEventsSyncRoot)
            {
                try
                {
                    // load Schema for data models
                    ds = new DataSet();
                    System.IO.Stream stream = GetEmbededObjectStream(this,"WestTel.E911.Messaging.WestTelSchema.xsd");

                    if (stream != null) ds.ReadXmlSchema(stream);

                    dt               = ds.Tables["CallEvents"];
                    alarmEvents      = ds.Tables["AlarmEvents"];
                    psapStatusEvents = ds.Tables["PSAPStatusEvents"];
                }
                catch (Exception ex)
                {
                    if (DebugMode) Logging.AppTrace("Error in DataModel Constructor:" + ex.Message);
                    Logging.ExceptionLogger("Error in DataModel.DataModel", ex, Console.LogID.Init);
                }
            }
logLockInfo(false, DataModel.callEventsSyncRoot);
        }

        System.IO.Stream GetEmbededObjectStream(Object ObjectName, string Name)
		{
            return ObjectName.GetType().Module.Assembly.GetManifestResourceStream(Name);
		}

        private static volatile int callEventsLockCount       = 0;
        private static volatile int alarmEventsLockCount      = 0;
        private static volatile int psapStatusEventsLockCount = 0;
        private static volatile int messageQueueLockCount     = 0;

        public static void logLockInfo(bool locked, object syncRoot)
        {
            if (LogLockInfo != null)
            {
                try
                {
                    int increment = 1;
                    if ( ! locked) increment = -1;

                    if      (syncRoot == callEventsSyncRoot)       callEventsLockCount       += increment;
                    else if (syncRoot == alarmEventsSyncRoot)      alarmEventsLockCount      += increment;
                    else if (syncRoot == psapStatusEventsSyncRoot) psapStatusEventsLockCount += increment;
                    else if (syncRoot == messageQueueSyncRoot)     messageQueueLockCount     += increment;

                    StackFrame stackFrame = new StackFrame(1, true);

                    string s = stackFrame.GetMethod() + " - Locked: " + callEventsLockCount.ToString() + ", " + alarmEventsLockCount.ToString()
                                                                                                       + ", " + psapStatusEventsLockCount.ToString()
                                                                                                       + ", " + messageQueueLockCount.ToString();
                    s += "  (" + Thread.CurrentThread.Name + ")";

                    LogLockInfo(s);
                }
                catch {}
            }
        }

        public void RemovePsapEvent(psapEventMessage message)
        {
            try
            {
// TODO* gui:               Invoke(new removePsapEventDelegate(removePsapEvent), message);
                removePsapEvent(message);
            }
            catch {try{removePsapEvent(message);} catch{}}
        }

        private delegate void removePsapEventDelegate(psapEventMessage message);

        private void removePsapEvent(psapEventMessage message)
        {
            if (dt == null) return;

logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (callEventsSyncRoot)
            {
                try
                {
                    if (message.Call_ID != null && dt.Rows.Count != 0)
                    {
                        DataRow row = dt.Rows.Find(message.Call_ID);

                        if (row != null) dt.Rows.Remove(row);
                    }
                }
                catch (Exception ex)
                {
                    Logging.AppTrace("Error in removePsapEvent() finding row:" + ex.Message);
                    Logging.ExceptionLogger("Error in DataModel.removePsapEvent", ex, Console.LogID.Gui);
                }
            }
logLockInfo(false, DataModel.callEventsSyncRoot);
        }

        public void UpdatepsapEvent(psapEventMessage message)
        {
            if (dt == null) return;

            //dt.Reset();
    
            if (DebugMode) Trace.Write("[PE]");

            StatusCode statusCode = StatusCode.Abandoned;
            if (message.StatusCode != null) statusCode = (StatusCode)Convert.ToInt32(message.StatusCode);

            DataRow row = null;

logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (callEventsSyncRoot)
            {
                try
                {
                    if (message.Call_ID != null && dt.Rows.Count != 0)
                    {
                        while (dt.Rows.Count > 500)
                        {
                            if (DebugMode) Trace.Write("{X}");
                            dt.Rows.RemoveAt(0);
                        }

                        row = dt.Rows.Find(message.Call_ID);
                    }
                }
                catch (Exception ex)
                {
                    row = null;

                    Logging.AppTrace("Error in UpdatepsapEvent() finding row:" + ex.Message);
                    Logging.ExceptionLogger("Error in UpdatepsapEvent:CleanupOldCalls", ex, Console.LogID.Gui);
                }

                try
                {
                    // new row
                    if (row == null)
                    {
                        row = dt.NewRow();

                        row["Call_ID"]                 = 0;
                        row["RingDateTime"]            = "";
                        row["RingDateTimeLocal"]       = DBNull.Value;
                        row["EventDateTime"]           = "";
                        row["TransmitDateTime"]        = "";
                        row["CallDuration"]            = DBNull.Value;
                        row["Encoding_ALI"]            = "";
                        row["ALIRecord"]               = "";
                        row["LineNumber"]              = 0;
                        row["LineNumberStr"]           = "---";
                        row["ShowInLineView"]          = false;
                        row["ConferenceNumber"]        = "0";
                        row["ConferenceNumberStr"]     = "---";
                        row["ConferenceLegend"]        = DBNull.Value;
                        row["Trunk"]                   = 0;
                        row["CallOriginator"]          = "";
                        row["PositionsActive"]         = "";
                        row["PositionsHistory"]        = "";
                        row["PositionsOnHold"]         = "";
                        row["PositionStr"]             = "";
                        row["Callback"]                = "";
                        row["CallbackDisplay"]         = "";
                        row["CallbackVerified"]        = "false";
                        row["pANI"]                    = "**********";
                        row["Status"]                  = "";
                        row["StatusCode"]              = (int)StatusCode.Abandoned;
                        row["CallStateCode"]           = Console.CallState.Undefined;
                        row["ALIStateCode"]            = 0;
                        row["ALIButton"]               = "off";
                        row["CADButton"]               = "off";
                        row["EraseCADButton"]          = "off";
                        row["TransferWindow"]          = "disable";
                        row["TDDWindow"]               = "disable";
                        row["TDDSendButton"]           = "off";
                        row["SMSWindow"]               = "disable";
                        row["SMSSendButton"]           = "off";
                        row["TakeControlButton"]       = "off";
                        row["FlashHookButton"]         = "off";
                        row["TandemXferButton"]        = "off";
                        row["AttendedXferButton"]      = "off";
                        row["ConferenceXferButton"]    = "off";
                        row["BlindXferButton"]         = "off";
                        row["FlashTransferButton"]     = "off";
                        row["ANIPort"]                 = "";
                        row["CallType"]                = "";
                        row["CallTypeNumber"]          = "";
                        row["StreamList"]              = DBNull.Value;

                        dt.Rows.Add(row);

                        try
                        {
                            if (message.Call_ID != null) row["Call_ID"] = Convert.ToUInt64(message.Call_ID);
                        }
                        catch (Exception ex)
                        {
                            Logging.AppTrace("Error updating Call_ID from PSAP data: " + ex.Message);
                            Logging.ExceptionLogger("Error in UpdatepsapEvent:Call_ID", ex, Console.LogID.Gui);
                        }

                        try
                        {
                            if (message.TransmitDateTime != null)
                            {
                                DateTime controllerNow = DateTime.Parse(
                                    message.TransmitDateTime.Substring(0, message.TransmitDateTime.IndexOf("UTC")),
                                    null, System.Globalization.DateTimeStyles.AssumeUniversal);

                                controllerOffsetTicks = controllerNow.Subtract(DateTime.Now).Ticks;
                            }
                        }
                        catch {}

                        try
                        {
                            if (message.RingDateTime != null) 
                            {
                                row["RingDateTimeLocal"] = DateTime.Parse(
                                    message.RingDateTime.Substring(0, message.RingDateTime.IndexOf("UTC")),
                                    null, System.Globalization.DateTimeStyles.AssumeUniversal);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logging.AppTrace("Error updating RingDateTime from PSAP data: " + ex.Message);
                            Logging.ExceptionLogger("Error in UpdatepsapEvent:RingDateTime", ex, Console.LogID.Gui);
                        }
                    }
                    // edit row
                    else
                    {
                    }

                    if (statusCode != StatusCode.ALI_No_Update)
                    {
                        try
                        {
                            if (message.Status     != null) row["Status"]     = (Translate)(message.Status);
                            if (message.StatusCode != null) row["StatusCode"] = Convert.ToInt32(message.StatusCode);
                        }
                        catch (Exception ex)
                        {
                            Logging.AppTrace("Error updating StatusCode from PSAP data: " + ex.Message);
                            Logging.ExceptionLogger("Error in UpdatepsapEvent:StatusCode", ex, Console.LogID.Gui);
                        }
                    }

                    // 16 Mar 2008 elliott - added missing values to row
                    if (message.RingDateTime            != null) row["RingDateTime"]            = message.RingDateTime;
                    if (message.EventDateTime           != null) row["EventDateTime"]           = message.EventDateTime;
                    if (message.TransmitDateTime        != null) row["TransmitDateTime"]        = message.TransmitDateTime;

                    if (message.Encoding_ALI            != null) row["Encoding_ALI"]            = message.Encoding_ALI;
                    if (message.ALIRecord               != null) row["ALIRecord"]               = message.ALIRecord;

                    try
                    {
                        if (message.TDDReceived != null)
                        {
                            string tddStr = message.TDDReceived;

                            if (message.EncodingTDD == "Base64") tddStr = Utility.DecodeBase64(tddStr);

                            if (TDDStringReceived != null) TDDStringReceived(message.Call_ID, tddStr);
                            else                           Controller.sendAlertToController("TDDReceived was null", Console.LogType.Warning, Console.LogID.TDD);
                        }

                        if (message.TDDdispatcherSays != null)
                        {
                            string tddStr = message.TDDdispatcherSays;

                            if (message.EncodingTDD == "Base64") tddStr = Utility.DecodeBase64(tddStr);

                            if (TDDdispatcherSays != null) TDDdispatcherSays(message.Call_ID, tddStr);
                            else                           Controller.sendAlertToController("TDDdispatcherSays was null", Console.LogType.Warning, Console.LogID.TDD);
                        }

                        if (message.TDDCallerSays != null)
                        {
                            string tddStr = message.TDDCallerSays;

                            if (message.EncodingTDD == "Base64") tddStr = Utility.DecodeBase64(tddStr);

                            if (TDDCallerSays != null) TDDCallerSays(message.Call_ID, tddStr);
                            else                       Controller.sendAlertToController("TDDCallerSays was null", Console.LogType.Warning, Console.LogID.TDD);
                        }

                        if (message.TDDmute != null)
                        {
                            if (TDDMuteChanged != null) TDDMuteChanged(message.Call_ID, message.TDDmute.ToString() == "on" ? true : false);
                            else                        Controller.sendAlertToController("TDDMuteChanged was null", Console.LogType.Warning, Console.LogID.TDD);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logging.AppTrace("Error updating TDD from PSAP data: " + ex.Message);
                        Logging.ExceptionLogger("Error in UpdatepsapEvent:TDD", ex, Console.LogID.Gui);
                    }

                    try
                    {
                        if (message.SMSReceived != null)
                        {
                            string smsStr = message.SMSReceived;

                            if (message.EncodingSMS == "Base64") smsStr = Utility.DecodeBase64(smsStr);

                            if (SMSStringReceived != null) SMSStringReceived(message.Call_ID, smsStr);
                            else                           Controller.sendAlertToController("SMSReceived was null", Console.LogType.Warning, Console.LogID.SMS);
                        }

                        if (message.SMSdispatcherSays != null)
                        {
                            string smsStr = message.SMSdispatcherSays;

                            if (message.EncodingSMS == "Base64") smsStr = Utility.DecodeBase64(smsStr);

                            if (SMSdispatcherSays != null) SMSdispatcherSays(message.Call_ID, smsStr);
                            else                           Controller.sendAlertToController("SMSdispatcherSays was null", Console.LogType.Warning, Console.LogID.SMS);
                        }

                        if (message.SMSCallerSays != null)
                        {
                            string smsStr = message.SMSCallerSays;

                            if (message.EncodingSMS == "Base64") smsStr = Utility.DecodeBase64(smsStr);

                            if (SMSCallerSays != null) SMSCallerSays(message.Call_ID, smsStr);
                            else                       Controller.sendAlertToController("SMSCallerSays was null", Console.LogType.Warning, Console.LogID.SMS);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logging.AppTrace("Error updating SMS from PSAP data: " + ex.Message);
                        Logging.ExceptionLogger("Error in UpdatepsapEvent:SMS", ex, Console.LogID.Gui);
                    }

                    try
                    {
                        if (message.LineNumber != null) row["LineNumber"]    = Convert.ToUInt16(message.LineNumber);
                        if (message.LineNumber != null) row["LineNumberStr"] = Controller.getLineNumberStr(message.LineNumber);

                        // show only one row per line
                        ushort  lineNumber = Convert.ToUInt16(row["LineNumber"]);
                        DataRow oldLineRow;

                        if (lineNumberDictionary.TryGetValue(lineNumber, out oldLineRow)) oldLineRow["ShowInLineView"] = false;

                        lineNumberDictionary[lineNumber] = row;
                        row["ShowInLineView"] = true;
                    }
                    catch (Exception ex)
                    {
                        Logging.AppTrace("Error updating LineNumber from PSAP data: " + ex.Message);
                        Logging.ExceptionLogger("Error in UpdatepsapEvent:LineNumber", ex, Console.LogID.Gui);
                    }

                    if (message.ConferenceNumber != null) row["ConferenceNumber"]    = message.ConferenceNumber;
                    if (message.ConferenceNumber != null) row["ConferenceNumberStr"] = (message.ConferenceNumber != "0") ? message.ConferenceNumber : "---";
                    if (message.ConferenceLegend != null) row["ConferenceLegend"]    = new ArrayList(message.ConferenceLegend);

                    try
                    {
                        if (message.Trunk != null) row["Trunk"] = Convert.ToUInt16(message.Trunk);
                    }
                    catch (Exception ex)
                    {
                        Logging.AppTrace("Error updating Trunk from PSAP data: " + ex.Message);
                        Logging.ExceptionLogger("Error in UpdatepsapEvent:Trunk", ex, Console.LogID.Gui);
                    }

                    if (message.CallOriginator          != null) row["CallOriginator"]          = message.CallOriginator;
                    if (message.PositionsActive         != null) row["PositionsActive"]         = message.PositionsActive;
                    if (message.PositionsHistory        != null) row["PositionsHistory"]        = message.PositionsHistory;
                    if (message.PositionsOnHold         != null) row["PositionsOnHold"]         = message.PositionsOnHold;

                    if (message.Callback                != null) row["Callback"]                = message.Callback;
                    else if (message.pANI               != null) row["Callback"]                = message.pANI;

                    if (message.CallbackDisplay         != null) row["CallbackDisplay"]         = message.CallbackDisplay;
                    else if (row["CallbackDisplay"].ToString().Length == 0) row["CallbackDisplay"] = row["Callback"];

                    if (message.CallbackVerified        != null) row["CallbackVerified"]        = message.CallbackVerified;
                    if (message.pANI                    != null) row["pANI"]                    = message.pANI;

                    try
                    {
                        if (message.CallStateCode != null) row["CallStateCode"] = Convert.ToUInt16(message.CallStateCode);
                    }
                    catch (Exception ex)
                    {
                        Logging.AppTrace("Error updating CallStateCode from PSAP data: " + ex.Message);
                        Logging.ExceptionLogger("Error in UpdatepsapEvent:CallStateCode", ex, Console.LogID.Gui);
                    }

                    try
                    {
                        if (message.ALIStateCode != null) row["ALIStateCode"] = Convert.ToUInt16(message.ALIStateCode);
                    }
                    catch (Exception ex)
                    {
                        Logging.AppTrace("Error updating ALIStateCode from PSAP data: " + ex.Message);
                        Logging.ExceptionLogger("Error in UpdatepsapEvent:ALIStateCode", ex, Console.LogID.Gui);
                    }

                    if (message.ALIButton               != null) row["ALIButton"]               = message.ALIButton;
                    if (message.CADButton               != null) row["CADButton"]               = message.CADButton;
                    if (message.EraseCADButton          != null) row["EraseCADButton"]          = message.EraseCADButton;
                    if (message.TransferWindow          != null) row["TransferWindow"]          = message.TransferWindow;
                    if (message.TDDWindow               != null) row["TDDWindow"]               = message.TDDWindow;
                    if (message.TDDSendButton           != null) row["TDDSendButton"]           = message.TDDSendButton;
                    if (message.SMSWindow               != null) row["SMSWindow"]               = message.SMSWindow;
                    if (message.SMSSendButton           != null) row["SMSSendButton"]           = message.SMSSendButton;
                    if (message.TakeControlButton       != null) row["TakeControlButton"]       = message.TakeControlButton;
                    if (message.FlashHookButton         != null) row["FlashHookButton"]         = message.FlashHookButton;
                    if (message.TandemXferButton        != null) row["TandemXferButton"]        = message.TandemXferButton;
                    if (message.AttendedXferButton      != null) row["AttendedXferButton"]      = message.AttendedXferButton;
                    if (message.ConferenceXferButton    != null) row["ConferenceXferButton"]    = message.ConferenceXferButton;
                    if (message.BlindXferButton         != null) row["BlindXferButton"]         = message.BlindXferButton;
                    if (message.FlashTransferButton     != null) row["FlashTransferButton"]     = message.FlashTransferButton;

                    if (message.ANIPort                 != null) row["ANIPort"]                 = message.ANIPort;
                    if (message.CallType                != null) row["CallType"]                = (Translate)(message.CallType);

                    row["CallTypeNumber"] = row["CallbackDisplay"] + "\n" + row["CallType"];

                    if (message.streamList              != null) row["StreamList"]              = new ArrayList(message.streamList);

                    try
                    {
                        // set position string
                        if (row["PositionsHistory"].ToString().Length != 0)
                        {
                            // extract positions
                            char[] separator = {'\t'};

                            string[] allPositions =
                                row["PositionsHistory"].ToString().Split(separator, StringSplitOptions.RemoveEmptyEntries);

                            // sort positions
                            Array.Sort(allPositions, new PositionStrCompare());

                            // join positions
                            row["PositionStr"] = string.Join(",", allPositions);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logging.AppTrace("Error updating PositionHistory from PSAP data: " + ex.Message);
                        Logging.ExceptionLogger("Error in UpdatepsapEvent:PositionHistory", ex, Console.LogID.Gui);
                    }

                    try
                    {
                        ArrayList conferenceLegend = (ArrayList)
                            (row["ConferenceLegend"] == DBNull.Value ? null : row["ConferenceLegend"]);

                        // sort conference legend
                        if (conferenceLegend != null && conferenceLegend.Count != 0)
                        {
                            // sort items
                            conferenceLegend.Sort(new ConferenceInfoCompare());
                        }
                    }
                    catch (Exception ex)
                    {
                        Logging.AppTrace("Error updating ConferenceLegend from PSAP data: " + ex.Message);
                        Logging.ExceptionLogger("Error in UpdatepsapEvent:ConferenceLegend", ex, Console.LogID.Gui);
                    }

                    if (DebugMode) Trace.Write("{PE}");
                }
                catch (Exception ex)
                {
                    Logging.AppTrace("Error updating PSAP data: " + ex.Message);
                    Logging.ExceptionLogger("Error in UpdatepsapEvent:Update", ex, Console.LogID.Gui);
                }
            }
logLockInfo(false, DataModel.callEventsSyncRoot);
        }

        public class PositionStrCompare : IComparer<string>
        {
            int IComparer<string>.Compare(string s1, string s2)
            {
                return compare(s1, s2);
            }

            public static int compare(string s1, string s2)
            {
                if (s1 == s2) return 0;

                if (s1[0] != s2[0])
                {
                    // 'C' is always first
                    if (s1[0] == 'C') return -1;
                    if (s2[0] == 'C') return  1;

                    // 'P' is always next
                    if (s1[0] == 'P') return -1;
                    if (s2[0] == 'P') return  1;
                }

                int num1 = Convert.ToInt32(s1.Substring(1));
                int num2 = Convert.ToInt32(s2.Substring(1));

                if (num1 == num2) return 0;
                if (num1 <  num2) return -1;

                return 1;
            }
        }

        private class ConferenceInfoCompare : IComparer
        {
            int IComparer.Compare(object obj1, object obj2)
            {
                return PositionStrCompare.compare(((ConferenceInfo)obj1).key, ((ConferenceInfo)obj2).key);
            }
        }

        public void UpdateTime()
        {
            if (dt == null) return;

logLockInfo(true, DataModel.callEventsSyncRoot);
            lock (callEventsSyncRoot)
            {
                try
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        long duration = -1;

                        Console.CallState callState = (Console.CallState)Convert.ToInt32(dr["CallStateCode"]);

                        switch (callState)
                        {
                            case Console.CallState.Ringing:   case Console.CallState.Dialing:
                            case Console.CallState.Connected: case Console.CallState.OnHold:
                            {
                                if (dr["RingDateTimeLocal"] == DBNull.Value)
                                {
                                    Logging.AppTrace("TimeStamp is null");
                                }
                                else
                                {
                                    if (dr["RingDateTimeLocal"].ToString().Length == 0)
                                    {
                                        Logging.AppTrace("TimeStamp is empty");
                                    }
                                    else
                                    {
                                        // 20 Mar 2008 elliott - fixed duration calculation
                                        duration = DateTime.Now.Subtract((DateTime)dr["RingDateTimeLocal"]).Ticks + controllerOffsetTicks;
                                    }
                                }

                                break;
                            }
                            default:
                            {
                                if (dr["CallDuration"] == DBNull.Value &&
                                    dr["RingDateTime"].ToString().Contains("UTC") &&
                                    dr["EventDateTime"].ToString().Contains("UTC"))
                                {
                                    DateTime ringDateTime = DateTime.Parse(
                                        dr["RingDateTime"].ToString().Substring(0, dr["RingDateTime"].ToString().IndexOf("UTC")));

                                    DateTime eventDateTime = DateTime.Parse(
                                        dr["EventDateTime"].ToString().Substring(0, dr["EventDateTime"].ToString().IndexOf("UTC")));

                                    duration = eventDateTime.Subtract(ringDateTime).Ticks;
                                }

                                break;
                            }
                        }

                        if (duration >= 0)
                        {
                            TimeSpan durationDateTime = new TimeSpan(duration);

                            if (durationDateTime.Hours > 99) durationDateTime = new TimeSpan(99, 59, 59);

                            dr["CallDuration"] = durationDateTime;

                            // Stop ringing for any Ringing calls after 1 minute
                            if (callState == Console.CallState.Ringing && durationDateTime.TotalSeconds > 60.0)
                            {
                                if (RingingCallTimedOut != null) RingingCallTimedOut(dr);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logging.AppTrace("Error in DataModel.UpdateTime:" + ex.Message);
                    Logging.ExceptionLogger("Error in DataModel.UpdateTime", ex, Console.LogID.Gui);
                }
            }
logLockInfo(false, DataModel.callEventsSyncRoot);
        }

        public void AddAlarm(string Stamp, string Text)
        {
DataModel.logLockInfo(true, alarmEventsSyncRoot);
            lock (alarmEventsSyncRoot)
            {
                try
                {
                    if (alarmEvents != null)
                    {
                        while (alarmEvents.Rows.Count > 500) alarmEvents.Rows.RemoveAt(0);

                        DataRow row = alarmEvents.NewRow();

                        // 20 Mar 2008 elliott - added null check
                        if (Stamp == null) Stamp = DateTime.Now.ToString();

                        row["timeStamp"] = DateTime.Parse(Stamp);
                        row["alarmText"] = Text;

                        alarmEvents.Rows.Add(row); // TODO*: figure out why we're getting a CrossThread Exception here
                    }
                    else
                    {
                        // TODO* gui: store alarms until alarmEvents get's created
                    }
                }
                catch (Exception ex)
                {
                    Logging.AppTrace("Error in AddAlarm: " + ex.Message);
                    Logging.ExceptionLogger("Error in DataModel.AddAlarm", ex, Console.LogID.Gui);
                }
            }
DataModel.logLockInfo(false, alarmEventsSyncRoot);
        }

        public void AddPSAPStatusEvent(string name, UInt32 positionsOnline, UInt32 positionsOn911, UInt32 positionsOnAdmin)
        {
DataModel.logLockInfo(true, psapStatusEventsSyncRoot);
            lock (psapStatusEventsSyncRoot)
            {
                try
                {
                    if (psapStatusEvents != null)
                    {
                        if (name != null)
                        {
                            if (psapStatusEvents.Rows.Contains(name))
                            {
                                DataRow row = psapStatusEvents.Rows.Find(name);

                                row["PositionsOnline"]  = positionsOnline  == 0 ? "-" : positionsOnline.  ToString();
                                row["PositionsOn911"]   = positionsOn911   == 0 ? "-" : positionsOn911.   ToString();
                                row["PositionsOnAdmin"] = positionsOnAdmin == 0 ? "-" : positionsOnAdmin. ToString();
                            }
                            else
                            {
                                DataRow row = psapStatusEvents.NewRow();

                                row["Name"]             = name;
                                row["PositionsOnline"]  = positionsOnline  == 0 ? "-" : positionsOnline.  ToString();
                                row["PositionsOn911"]   = positionsOn911   == 0 ? "-" : positionsOn911.   ToString();
                                row["PositionsOnAdmin"] = positionsOnAdmin == 0 ? "-" : positionsOnAdmin. ToString();

                                psapStatusEvents.Rows.Add(row); // TODO* gui: figure out why we're getting a CrossThread Exception here
                            }
                        }
                    }
                    else
                    {
                        // TODO* calls: store until psapStatusEvents gets created
                    }
                }
                catch (Exception ex)
                {
                    Logging.AppTrace("Error in AddPSAPStatus: " + ex.Message);
                    Logging.ExceptionLogger("Error in DataModel.AddPSAPStatus", ex, Console.LogID.Gui);
                }
            }
DataModel.logLockInfo(false, psapStatusEventsSyncRoot);
        }
    }
}
