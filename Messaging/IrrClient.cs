﻿using System;
using System.Drawing;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Web.Script.Serialization;

namespace WestTel.E911
{
    public class IrrClient
    {
		public delegate void ReceivedJsonOk(JsonOk jsonOk);
		public event ReceivedJsonOk receivedJsonOk;

        public delegate void ReceivedJsonError(JsonError jsonError);
		public event ReceivedJsonError receivedJsonError;

        public delegate void ReceivedJsonPlayEnd(JsonPlayEnd jsonPlayEnd);
		public event ReceivedJsonPlayEnd receivedJsonPlayEnd;

        public delegate void ReceivedJsonNoGet(JsonNoGet jsonNoGet);
		public event ReceivedJsonNoGet receivedJsonNoGet;

        public delegate void ReceivedOpusPiece(string streamId, ulong pieceId, byte[] pieceBytes, ushort pieceSize);
		public event ReceivedOpusPiece receivedOpusPiece;


        // example message "5 0 1 2 3 4 5" starts with little endian payload size
        //                                 of 5 followed by a five byte payload
        //                                 (first byte is message type 1:opus or 2:json)

        private const int HEADER_SIZE      = 3;
        private const int MAX_PAYLOAD_SIZE = 256; // TODO IRR: what is the max payload size?

        private Socket socket;
        private string serverIpAddr;
        private int    serverPort;
        private int    sendBufferSize;
        private int    recvBufferSize;

        private bool   connectionPending = false;

        private enum MessageType { OPUS = 1, JSON = 2 }

        private JavaScriptSerializer jsonSend = new JavaScriptSerializer();

        public class JsonMessage
        {
            public string _c { get; set; } = "OK"; // command
            public long   _r { get; set; } // signed int64 message request/response ID
        }

        // Feature Detection
        //  {"_c":"?","id":"feature_name","_r":123} -> {"_c":"OK","_r":-123,"has":false}
        //  {"_c":"ENABLE","id":"feature_name","_r":123} -> {"_c":"ERROR","_r":-123,"e":EXIST"} | {"_c":"OK","_r":-123}
        //  {"_c":"someunknowncommand...?","_r":123} -> {"_c":"UNKNOWN","_r":-123}

        public class JsonFeatureQuery
        {
            public string _c { get; } = "?"; // command
            public string id { get; }        // feature name
            public long   _r { get; } = 0;   // signed int64 message request/response ID

            public JsonFeatureQuery(string _id, long r) {id = _id; _r = r;}
        }

        public class JsonFeatureEnable
        {
            public string _c { get; } = "ENABLE"; // command
            public string id { get; }             // feature name
            public long   _r { get; } = 0;        // signed int64 message request/response ID

            public JsonFeatureEnable(string _id, long r) {id = _id; _r = r;}
        }

        public class JsonOk
        {
            public string   _c         { get; }      = "OK";  // command
            public long     _r         { get; set; } = 0;     // signed int64 message request/response ID
            public bool     has        { get; set; } = false; // has a feature
            public string[] play       { get; set; }          // list of streams that are playing
//            public string[] current    { get; set; }        // list of streams that are playable // TODO IRR: current is also a bool
            public ulong    num_pieces { get; set; } = 0;     // number of pieces in a stream
            public bool     current    { get; set; } = false; // whether the current stream is currently playing
            public int      last_len   { get; set; } = 0;     // audio length (in 1/48000 seconds) of the last Opus piece (1..960)
            public double   unix_time  { get; set; } = 0;     // UNIX time in seconds usually to millisecond precision
            public string   callid     { get; set; }          // call ID string

            public JsonOk() {}
            public JsonOk(long r) {_r = r;}
        }

        public class JsonError
        {
            public string _c  { get; }      = "ERROR"; // command
            public long   _r  { get; set; } = 0;       // signed int64 message request/response ID
            public string e   { get; set; }            // error type
        }

        public class JsonUnknown
        {
            public string _c  { get; }      = "UNKNOWN"; // command
            public long   _r  { get; set; } = 0;         // signed int64 message request/response ID

            public JsonUnknown(long r) {_r = r;}
        }

        // PING
        //  {"_c":"PING","_r":123} -> {"_c":"OK","_r":-123}

        public class JsonPing
        {
            public string _c { get; }      = "PING"; // command
            public long   _r { get; set; } = 0;      // signed int64 message request/response ID

            public JsonPing(long r) {_r = r;}
        }

        // PLAY
        //  {"_c":"PLAY","stream":"id..."}
        //  {"_c":"PLAY","stream":"id...","_r":123} -> {"_c":"OK","_r":-123} | {"_c":"ERROR","e":"EXIST"|"CURRENT","_r":-123}

        public class JsonPlay
        {
            public string _c     { get; } = "PLAY"; // command
            public string stream { get; }           // stream ID
            public long   _r     { get; } = 0;      // signed int64 message request/response ID

            public JsonPlay(string _stream, long r) {stream = _stream; _r = r;}
        }

        // STOP
        //  {"_c":"STOP","stream":"id..."}
        //  {"_c":"STOP","stream":"id...","_r":123} -> {"_c":"OK","-r":-123}

        public class JsonStop
        {
            public string _c     { get; } = "STOP"; // command
            public string stream { get; }           // stream ID
            public long   _r     { get; } = 0;      // signed int64 message request/response ID

            public JsonStop(string _stream, long r) {stream = _stream; _r = r;}
        }

        // PLAYEND
        //  -> {"_c":"PLAYEND","stream":"id...","last":4096,"last_len":207}

        public class JsonPlayEnd
        {
            public string _c       { get; }      = "PLAYEND"; // command
            public string stream   { get; set; }              // stream ID
            public ulong  last     { get; set; } = 0;         // last piece
            public int    last_len { get; set; } = 0;         // audio length (in 1/48000 seconds) of the last Opus piece (1..960)
        }

        // LISTPLAY
        //  {"_c":"LISTPLAY","_r":123} -> {"_c":"OK","_r":-123,"play":["id1...","id2..."]}

        public class JsonListPlay
        {
            public string _c { get; } = "LISTPLAY"; // command
            public long   _r { get; } = 0;          // signed int64 message request/response ID

            public JsonListPlay(long r) {_r = r;}
        }

        // LISTCURRENT
        //  {"_c":"LISTCURRENT","_r":123} -> {"_c":"OK","_r":-123,"current":["id1...","id2..."]}

        public class JsonListCurrent
        {
            public string _c { get; } = "LISTCURRENT"; // command
            public long   _r { get; } = 0;             // signed int64 message request/response ID

            public JsonListCurrent(long r) {_r = r;}
        }

        // STREAMINFO
        //  {"_c":"STREAMINFO","_r":123,"stream":"id..."} -> {"_c":"OK","_r":-123,"num_pieces":1024,"current":true,"last_len":207,"unix_time":1234.5678,"callid":"9876"} | {"_c":"ERROR","_r":-123,"e":"EXIST"}
        public class JsonStreamInfo
        {
            public string _c     { get; } = "STREAMINFO"; // command
            public long   _r     { get; } = 0;            // signed int64 message request/response ID
            public string stream { get; }                 // stream ID

            public JsonStreamInfo(string _stream, long r) {stream = _stream; _r = r;}
        }

        // GET
        //  {"_c":"GET","stream":"id...","first":3,"last":10}

        public class JsonGet
        {
            public string _c     { get; } = "GET"; // command
            public string stream { get; }          // stream ID
            public ulong  first  { get; } = 0;     // first piece requested (inclusive)
            public ulong  last   { get; } = 0;     // last piece requested (inclusive)

            public JsonGet(string _stream, ulong _first, ulong _last) {stream = _stream; first = _first; last = _last;}
        }

        // NOGET
        //  {"_c":"NOGET","stream":"id,"piece_num":7..."}

        public class JsonNoGet
        {
            public string _c        { get; } = "NOGET"; // command
            public ulong  piece_num { get; }            // piece ID
            public string stream    { get; }            // stream ID
        }


        /// Constructor
        public IrrClient(string _serverIpAddr, int _serverPort, int _sendBufferSize, int _recvBufferSize)
        {
            serverIpAddr   = _serverIpAddr;
            serverPort     = _serverPort;
            sendBufferSize = _sendBufferSize;
            recvBufferSize = _recvBufferSize;

            try
            {
                socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                reconnect();
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("Exception in IrrClient()", ex, Console.LogID.Init);
            }
        }


        /// Starts the connect thread if not already connected
        private void connect()
        {
            if (socket == null || socket.Connected || connectionPending) return;

            Thread thread = new Thread(new ThreadStart(connectThread));
            thread.Name = "IRR Client Connect Thread";
            thread.Start();
        }

        private void reconnect(int delayMsec = 0)
        {
            try
            {
                if (socket == null || connectionPending) return;

                Socket _socket = socket;
                socket = null;
                _socket.Close();

                if (delayMsec != 0) Thread.Sleep(delayMsec);

                socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                socket.NoDelay             = true;
                socket.SendTimeout         = 2000;
                socket.ReceiveBufferSize   = recvBufferSize;
                socket.SendBufferSize      = sendBufferSize;
                socket.ExclusiveAddressUse = false;

                connect();
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IrrClient failed in reconnect()", ex, Console.LogID.Connection);
            }
        }

        private void connectThread()
        {
            connectionPending = true;

            while (socket != null && ! socket.Connected)
            {
                try
                {
                    // Connect using a ten second timeout
                    IAsyncResult result = socket.BeginConnect(serverIpAddr, serverPort, null, null);

                    result.AsyncWaitHandle.WaitOne(10000, true);

                    socket.EndConnect(result); // this will throw an exception if the Connect failed

                    // TODO IRR: send version
                    //byte[] msg = {(byte)'I', (byte)'R', (byte)'R', (byte)'1', 0};
                    bool success = true; // writeToSocket(msg, (ushort)msg.Length);

                    if (success)
                    {
                        connectionPending = false;

                        Controller.sendAlertToController(
                            "IRR client connected " + serverIpAddr + ":" + serverPort.ToString(),
                            Console.LogType.Info, Console.LogID.Connection);

                        Thread thread = new Thread(new ThreadStart(receiveThread));
                        thread.Name = "IRR Client Receive Thread";
                        thread.Start();

                        return;
                    }
                    else
                    {
                        socket.Shutdown(SocketShutdown.Both);
                        socket.Disconnect(true);
                    }
                }
                catch (Exception ex)
                {
                    Logging.ExceptionLogger("IrrClient failed in connect thread", ex, Console.LogID.Connection);
                    Thread.Sleep(100);
                }
            }
        }

        private void warnAndReconnect(string msg)
        {
            Controller.sendAlertToController(msg, Console.LogType.Warning, Console.LogID.Connection);
            reconnect(100);
        }

        private void receiveThread()
        {
            if (socket == null) return;

            byte[] headerBytes  = new byte[HEADER_SIZE];
            byte[] payloadBytes = new byte[MAX_PAYLOAD_SIZE];

            JavaScriptSerializer jsonRecv = new JavaScriptSerializer();

            try
            {
                while (true)
                {
                    if (socket == null || ! socket.Connected)
                    {
                        connect();
                        return;
                    }

                    int nBytesReceived = 0;

                    SocketError socketError;

                    while (nBytesReceived < HEADER_SIZE)
                    {
                        // get only the header
                        nBytesReceived += socket.Receive(headerBytes, nBytesReceived,
                            HEADER_SIZE - nBytesReceived, SocketFlags.None, out socketError);

                        if (socketError != SocketError.Success)
                        {
                            warnAndReconnect("IRR client socket error on read: " + socketError.ToString());
                            return;
                        }
                    }

                    ushort payloadSize = BitConverter.ToUInt16(headerBytes, 0);

                    if (payloadSize > MAX_PAYLOAD_SIZE)
                    {
                        warnAndReconnect("IRR client received payload too big: " + payloadSize.ToString() + " bytes");
                        return;
                    }

                    // get the message type, which is actually part of the payload
                    byte msgType = headerBytes[2];
                    --payloadSize;

                    if (msgType != (byte)MessageType.OPUS && msgType != (byte)MessageType.JSON)
                    {
                        warnAndReconnect("IRR client received unexpected message type: " + msgType.ToString());
                        return;
                    }

                    nBytesReceived = 0;

                    while (nBytesReceived < payloadSize)
                    {
                        nBytesReceived += socket.Receive(payloadBytes, nBytesReceived,
                            payloadSize - nBytesReceived, SocketFlags.None, out socketError);

                        if (socketError != SocketError.Success)
                        {
                            warnAndReconnect("IRR client socket error on read: " + socketError.ToString());
                            return;
                        }
                    }

                    if (msgType == (byte)MessageType.OPUS)
                    {
                        handleOpusPiece(payloadBytes, payloadSize);
                    }
                    else if (msgType == (byte)MessageType.JSON)
                    {
                        handleJson(payloadBytes, payloadSize, jsonRecv);
                    }
                    else
                    {
                        warnAndReconnect("Unexpected message type:" + msgType.ToString() +
                                       " expected 1 (opus) or 2 (json)");
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("Exception in IrrClient.receiveThread() processing packet",
                                        ex, Console.LogID.Connection);
                reconnect(1000);
            }
        }

        private void handleOpusPiece(byte[] payloadBytes, ushort payloadSize)
        {
            try
            {
                if (payloadSize < 8)
                {
                    throw new Exception("OPUS packet too small: " + payloadSize.ToString() +
                                        " bytes (expected at least an 8 byte piece ID)");
                }

                ushort payloadIndex = 0;

                ulong pieceId = BitConverter.ToUInt64(payloadBytes, payloadIndex);
                payloadIndex += sizeof(ulong);

                // get stream ID
                string streamId = null;

                for (ushort i = payloadIndex; i != payloadSize; ++i)
                {
                    if (payloadBytes[i] == 0)
                    {
                        streamId = System.Text.Encoding.ASCII.
                            GetString(payloadBytes, payloadIndex, i - payloadIndex);

                        payloadIndex = ++i;
                        break;
                    }
                }

                if (streamId == null)
                {
                    throw new Exception("OPUS piece:" + pieceId.ToString() + " had no stream ID");
                }

                try
                {
                    if (receivedOpusPiece != null)
                    {
                        ushort pieceSize = payloadSize;
                        pieceSize -= payloadIndex;

                        byte[] pieceBytes = new byte[pieceSize];
                        Array.Copy(payloadBytes, payloadIndex, pieceBytes, 0, pieceSize);

                        receivedOpusPiece(streamId, pieceId, pieceBytes, pieceSize);
                    }
                }
                catch (Exception ex)
                {
                    Logging.AppTrace("Error in handleOpusPiece:" + ex.Message);
                    Logging.ExceptionLogger("Error in IrrClient.handleOpusPiece()", ex, Console.LogID.Connection);
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("Exception in IrrClient.handleOpusPiece()", ex, Console.LogID.Connection);
                reconnect(1000);
            }
        }

        private void handleJson(byte[] payloadBytes, ushort payloadSize, JavaScriptSerializer jsonRecv)
        {
            string jsonStr = "";

            try
            {
                // JSON string
                jsonStr = System.Text.Encoding.ASCII.GetString(payloadBytes, 0, payloadSize);

                JsonMessage jsonMsg = jsonRecv.Deserialize<JsonMessage>(jsonStr);

                string jsonCmd = jsonMsg._c.ToUpper();
                long   jsonRe  = jsonMsg._r;

                switch (jsonCmd)
                {
                    case "PING":
                    {
                        if (jsonRe > 0)
                        {
                            writeJson(jsonRecv.Serialize(new JsonOk(-jsonRe)));
                        }

                        break;
                    }
                    case "OK":
                    {
                        // TODO IRR: keep track of Acks?

                        JsonOk jsonOk = jsonRecv.Deserialize<JsonOk>(jsonStr);

                        try
                        {
                            if (receivedJsonOk != null) receivedJsonOk(jsonOk);
                        }
                        catch (Exception ex)
                        {
                            Logging.AppTrace("Error in ReceivedJsonOk Message:" + ex.Message);
                            Logging.ExceptionLogger("Error in IrrClient.ReceiveThread (ReceivedJsonOk)", ex, Console.LogID.Gui);
                        }

                        break;
                    }
                    case "ERROR":
                    {
                        JsonError jsonError = jsonRecv.Deserialize<JsonError>(jsonStr);

                        try
                        {
                            if (receivedJsonError != null)
                            {
                                receivedJsonError(jsonError);
                            }
                            else
                            {
                                Controller.sendAlertToController("IRR Client received JSON error: " + jsonError.e,
                                    Console.LogType.Warning, Console.LogID.Connection);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logging.AppTrace("Error in ReceivedJsonError Message:" + ex.Message);
                            Logging.ExceptionLogger("Error in IrrClient.ReceiveThread (ReceivedJsonError)", ex, Console.LogID.Gui);
                        }

                        break;
                    }
                    case "UNKNOWN":
                    {
                        // TODO IRR: report unknown if we start using "Feature Detection"
                        JsonUnknown jsonUnknown = jsonRecv.Deserialize<JsonUnknown>(jsonStr);
                        break;
                    }
                    case "PLAYEND":
                    {
                        // TODO IRR: handle play end?
                        JsonPlayEnd jsonPlayEnd = jsonRecv.Deserialize<JsonPlayEnd>(jsonStr);

                        try
                        {
                            if (receivedJsonPlayEnd!= null) receivedJsonPlayEnd(jsonPlayEnd);
                        }
                        catch (Exception ex)
                        {
                            Logging.AppTrace("Error in ReceivedJsonPlayEnd Message:" + ex.Message);
                            Logging.ExceptionLogger("Error in IrrClient.ReceiveThread (ReceivedJsonPlayEnd)", ex, Console.LogID.Gui);
                        }

                        break;
                    }
                    case "NOGET":
                    {
                        JsonNoGet jsonNoGet = jsonRecv.Deserialize<JsonNoGet>(jsonStr);

                        try
                        {
                            if (receivedJsonNoGet != null) receivedJsonNoGet(jsonNoGet);
                        }
                        catch (Exception ex)
                        {
                            Logging.AppTrace("Error in ReceivedJsonNoGet Message:" + ex.Message);
                            Logging.ExceptionLogger("Error in IrrClient.ReceiveThread (ReceivedJsonNoGet)", ex, Console.LogID.Gui);
                        }

                        break;
                    }
                    default:
                    {
                        if (jsonRe > 0)
                        {
                            writeJson(jsonRecv.Serialize(new JsonUnknown(-jsonRe)));
                        }

                        // TODO IRR: report unexpected commands
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("Exception in IrrClient.handleJson() \"" +
                                        jsonStr + "\"", ex, Console.LogID.Connection);
                reconnect(1000);
            }
        }

        private byte[] writeData = new byte[HEADER_SIZE + MAX_PAYLOAD_SIZE];

        private bool writeJson(string jsonStr)
        {
            try
            {
                ushort payloadLength = (ushort)(jsonStr.Length + 1);

                BitConverter.GetBytes(payloadLength).CopyTo(writeData, 0);
                writeData[2] = (byte)MessageType.JSON;

                Encoding.ASCII.GetBytes(jsonStr).CopyTo(writeData, HEADER_SIZE);

                return writeToSocket(writeData, (ushort)(HEADER_SIZE + jsonStr.Length));
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IrrClient failed in writeJson()", ex, Console.LogID.Connection);
            }

            return false;
        }

        private bool writeToSocket(byte[] data, ushort dataSize)
        {
            if (socket == null) return false;

            if ( ! socket.Connected)
            {
                connect();
                return false;
            }

            try
            {
                SocketError socketError;

                int nBytesSent = socket.Send(data, 0, dataSize, SocketFlags.None, out socketError);

                if (socketError != SocketError.Success)
                {
                    warnAndReconnect("IRR client socket error on send: " + socketError.ToString());
                    return false;
                }

                if (nBytesSent != dataSize)
                {
                    warnAndReconnect("IRR client error, sent " + nBytesSent.ToString() +
                                   " bytes of " + dataSize.ToString());
                    return false;
                }
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("Exception in IrrClient.writeToSocket()", ex, Console.LogID.Connection);
                reconnect(100);

                return false;
            }

            return true;
        }

        static int JsonMessageId = 0;

        private int getNextId() {return ++JsonMessageId;}

        public int sendPlayCommand(string streamId)
        {
            int nextId = getNextId();

            try
            {
                writeJson(jsonSend.Serialize(new JsonPlay(streamId, nextId)));
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IrrClient failed to send PLAY", ex, Console.LogID.Connection);
            }

            return nextId;
        }

        public int sendStopCommand(string streamId)
        {
            int nextId = getNextId();

            try
            {
                writeJson(jsonSend.Serialize(new JsonStop(streamId, nextId)));
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IrrClient failed to send STOP", ex, Console.LogID.Connection);
            }

            return nextId;
        }

        public int sendStreamInfoCommand(string streamId)
        {
            int nextId = getNextId();

            try
            {
                writeJson(jsonSend.Serialize(new JsonStreamInfo(streamId, nextId)));
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IrrClient failed to send STREAMINFO", ex, Console.LogID.Connection);
            }

            return nextId;
        }

        public void sendGetCommand(string streamId, ulong firstPieceId, ulong lastPieceId)
        {
            try
            {
                writeJson(jsonSend.Serialize(new JsonGet(streamId, firstPieceId, lastPieceId)));
            }
            catch (Exception ex)
            {
                Logging.ExceptionLogger("IrrClient failed to send GET", ex, Console.LogID.Connection);
            }
        }
    }
}
