using System;
using System.Collections;
using System.Xml;
using System.Threading;
using System.Diagnostics;
using System.Net;


namespace WestTel.E911
{
    //public enum MessageType
    //{
    //    Ringing,
    //    RingNoAnswer60,
    //    Active,
    //    ALI,
    //    OnHold,
    //    OffHold,
    //    Conference,
    //    Transfer,
    //    RelatedNotesReady,
    //    Abandoned,
    //    Disconnect,
    //    NoAnswer,
    //    NoCorrelation,
    //    SilentEntryLogoff,
    //    Alarm,
    //    Unknown
    //}

    public enum StatusCode
    {
        Unknown                   = 00,
        Connected                 = 01,
        Disconnected              = 02,
        On_Hold                   = 05,
        Off_Hold                  = 06,
        Abandoned                 = 07,
        Ringing                   = 11,
        Dialing                   = 12, // internal to controller
        ALI_Bid_not_allowed       = 79, // internal to controller
        TDD_Mute_change           = 83,
        TDDdispatcherSays         = 84,
        TDDCallerSays             = 85,
        TDDReceived               = 86,
     // SMSdispatcherSays         = ??, // TODO SMS: should there be SMS StatusCode values
     // SMSCallerSays             = ??, // TODO SMS: should there be SMS StatusCode values
     // SMSReceived               = ??, // TODO SMS: should there be SMS StatusCode values
        Manual_ALI_Request        = 87,
        Manual_ALI_Request_Failed = 88,
        Auto_ALI_Request          = 89,
        Auto_ALI_Request_Failed   = 90,
        ALI_Request               = 91,
        ALI_Request_Failed        = 92,
        ALI_Received              = 93,
        Manual_ALI_Received       = 94,
        Auto_ALI_Received         = 95,
        ALI_No_Update             = 96,
        Sent_to_CAD               = 97,
        Conference_Join           = 98,
        Conference_Leave          = 99
    }

    public enum ALIStateCode
    {
        NO_DATA                      = 0,
        ALI_Bid_Not_Allowed          = 79,
        ALI_Bid_Retry_Status_Message = 80,
        Manual_ALI_Request           = 87, // subdued
        Manual_ALI_Request_Failed    = 88, // subdued
        Auto_ALI_Request             = 89, // subdued
        Auto_ALI_Request_Failed      = 90, // subdued
        ALI_Request                  = 91, // subdued
        ALI_Request_Failed           = 92, // subdued
        ALI_Received                 = 93,
        Manual_ALI_Received          = 94,
        Auto_ALI_Received            = 95
    }

    public class MessageInfo
	{
		public psapEventMessage psapMessage;
		public MessageInfo(psapEventMessage psapMessage)
		{
			this.psapMessage = psapMessage;
		}
    }

	/// <summary>	
	/// XML Instant Messenger Class.
	/// </summary>
	public class XmlMessages
	{
		private Server MessageServer = Server.Instance;
		private DataModel dataModel ;
		private ArrayList messageQueue = new ArrayList();

        private bool useThreads = true;
		private bool KeepAlive  = true;

        private System.Threading.Timer timer = null;

        private int CurrentPosition = 1;
        public void setCurrentPosition(int pos) {CurrentPosition = pos;}

		public XmlMessages(Controller c, DataModel _dataModel)
		{
			dataModel = _dataModel;

			//MessageServer.ClientConnection +=new Messaging.Server.OnClientConnection(MessageServer_ClientConnection);
			MessageServer.ClientDataReceived += new Server.OnClientDataReceived(MessageServer_ClientDataReceived);
			MessageServer.ServerMessage +=new Server.OnServerMessage(MessageServer_ServerMessage);
            MessageServer.ClientError +=new Server.OnClientError(MessageServer_ClientError);
		}

        public delegate void OnSettingsReceived(XmlDocument xd);
        public event OnSettingsReceived SettingsReceived;

        public delegate void OnXMLDataReceived(string Data, bool fromSocket);
        public event OnXMLDataReceived XMLDataReceived;

        public delegate void OnLogXML(string xml, string name);
        public event OnLogXML LogXML;

		public delegate void OnServerMessage(string DateTime, int Level, string Message);
		public event OnServerMessage ServerMessage;
        
		public delegate void OnAlarmMessage(string Text, string type);
		public event OnAlarmMessage AlarmMessage;

		public delegate void OnNewActive(string positions);
		public event OnNewActive NewActive;

        public delegate void OnCheckUpdate();
		public event OnCheckUpdate CheckUpdate;

		public delegate void OnResetTransfer(string callId, bool successful);
		public event OnResetTransfer ResetTransfer;

		public delegate void OnUpdatepsapEvent(psapEventMessage psapMessage);
		public event OnUpdatepsapEvent UpdatepsapEvent;

		public delegate void OnMessageReceived(MessageInfo messageInfo);
		public event OnMessageReceived MessageReceived;

		public string CurrentDateTime()
		{
			return (System.DateTime.Now.ToString("yy:MM:dd:hh:mm:ss") + ":" + System.DateTime.Now.Millisecond.ToString("000"));
		}

        static bool started = false;

		public void Start(
            int    udpListenPort,
            string tcpRemoteIPAddress,
            int    tcpRemotePort,
            int    sendBufferSize,
            int    recvBufferSize
        )
		{
            if (started) return;

            started = true;

			try
            {
                MessageServer.Start(udpListenPort, tcpRemoteIPAddress, tcpRemotePort, sendBufferSize, recvBufferSize);
            }
            catch
            {
                Controller.sendAlertToController("MessageServer failed to start",
                    Console.LogType.Alarm, Console.LogID.Init); // TODO log: make a new message ID?
            }

            if (useThreads)
            {
                KeepAlive = true;
			    Thread ProcThread = new Thread(new ThreadStart(MessageProcessor));
			    ProcThread.Name = "Messages Thread";
//			    ProcThread.Priority = ThreadPriority.AboveNormal;
			    ProcThread.Start();
            }
            else
            {
                if (timer == null)
                {
                    timer = new System.Threading.Timer(new TimerCallback(MessageProcessor), null, 50, Timeout.Infinite);
                }
            }
		}

		public void Stop()
		{ 
			try
			{
                if (timer != null) timer.Dispose();
                timer = null;

				KeepAlive = false;
				MessageServer.Stop();
			}
			catch (Exception ex)
			{
				Logging.AppTrace("Error Stoping XMLIMServer:" + ex.Message);
				Logging.ExceptionLogger("Error in XMLIMServer.Stop", ex, Console.LogID.Cleanup);
			}

            started = false;
		}

		private void MessageProcessor(object unusedObj)
        {
            try {MessageProcessor();} catch {}

            if (timer != null) timer.Change(50, Timeout.Infinite);
        }

		private void MessageProcessor()
		{
Controller.DebugWindowMsg("XmlMessageServer message processor thread started");
			do 
			{
		        try
		        {
                    if (messageQueue.Count == 0)
                    {
					    if (useThreads) Thread.Sleep(10); // pause briefly
                        continue;
                    }

DataModel.logLockInfo(true, DataModel.messageQueueSyncRoot);
                    lock (DataModel.messageQueueSyncRoot)
                    {
                        // loop through a max of 10 messages
                        for (int i = 0; i < 10 && messageQueue.Count != 0; i++)
                        {
                            string nextMessage = (string)messageQueue[0];
                            messageQueue.RemoveAt(0);

                            if (nextMessage == null) break;

                            // Trace.Write("Depth:" + messageQueue.Count.ToString());
                            ProcessMessageFromSocket(nextMessage);
                        }
                    }
DataModel.logLockInfo(false, DataModel.messageQueueSyncRoot);
                }
			    catch (Exception ex)
			    {
				    Logging.AppTrace("Error in MessageProcessor:" + ex.Message);	
				    Logging.ExceptionLogger("Error in XMLIMServer.MessageProcessor", ex, Console.LogID.Connection);
			    }
			}
            while (useThreads && KeepAlive);

Controller.DebugWindowMsg("XmlMessageServer message processor thread ended");
		}

		private void MessageServer_ClientConnection (System.Net.Sockets.Socket newClient)
		{
			if (ServerMessage != null) ServerMessage(CurrentDateTime(),(int) LogLevel.Connections, "Connection from " + newClient.RemoteEndPoint.ToString());
		}

		private void MessageServer_ClientDataReceived (string Data)
		{
            try
            {
                //Trace.Write(">");
DataModel.logLockInfo(true, DataModel.messageQueueSyncRoot);
                lock (DataModel.messageQueueSyncRoot)
                {
                    messageQueue.Add(Data);
                }
DataModel.logLockInfo(false, DataModel.messageQueueSyncRoot);
            }
            catch {}
		}

        public  void ProcessMessage           (string Data) {ProcessMessage(Data, false);}
        private void ProcessMessageFromSocket (string Data) {ProcessMessage(Data, true);}

        private void ProcessMessage(string Data, bool fromSocket)
		{
            try
            {
			    // XMLMessage.LogData(Data);
                dataReceived(Data, fromSocket);

			    // peek at the message here if different tasks are based on data 
			    psapEventMessage psapMessage;

                if (Data != null && Data.StartsWith("<?xml"))
			    {
				    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(Data);

                    if (xd["Event"] == null)
                    {
                        if (LogXML != null) LogXML(Data.ToString(), "XML (No 'Event' Found)");

                        // report up to 150 characters
                        int dataLength = Data.Length;

                        if (dataLength > 200) Data = Data.Remove(150) + " ... [length:" + dataLength.ToString() + "]";

                        Data = System.Security.SecurityElement.Escape(Data);

                        Controller.sendAlertToController("Invalid XML received (no Event tag found) -> \"" + Data + "\"",
                            Console.LogType.Warning, Console.LogID.Connection);

                        return;
                    }

                    string xmlVersion = "[null]";

                    if (xd["Event"].Attributes["XMLspec"] != null)
                    {
                        xmlVersion = "v" + xd["Event"].GetAttribute("XMLspec");
                    }

                    if (xmlVersion != "v" + XMLMessage.SPEC_VERSION)
                    {
                        if (LogXML != null) LogXML(Data.ToString(), "XML (Invalid XML Spec)");

                        if (Controller.controller != null) Controller.controller.checkForUpdates();

                        MessageBox.Show(null, (Translate)("Invalid XML Spec") + " (Controller: " + xmlVersion +
                                        " Workstation: v" + XMLMessage.SPEC_VERSION +
                                        "). " + (Translate)("Please update the software so the XML Specs match.\n\n") +
#if DEBUG
                                        (Translate)("This application would close in Release mode."),
#else
                                        (Translate)("This application will now exit."),
#endif
                                        Application.ProductName,
                                        System.Windows.Forms.MessageBoxButtons.OK,
                                        System.Windows.Forms.MessageBoxIcon.Information,
                                        System.Windows.Forms.MessageBoxDefaultButton.Button1,
                                        System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly);
#if ! DEBUG
                        System.Windows.Forms.Application.Exit();

                        Process current = Process.GetCurrentProcess();
                        if (current != null) current.Kill();
#endif
                        return;
                    }

                    if (xd["Event"]["Initialization"] != null)
                    {
                        if (LogXML != null) LogXML(Data.ToString(), "Init");

                        if (SettingsReceived != null) SettingsReceived(xd);
                    }
                    else
                    {
				        psapMessage = XMLMessage.XMLToEventMessage(xd);

                        if (psapMessage.Event == null)
                        {
                            if (LogXML != null) LogXML(Data.ToString(), "XML (Null Event)");
                            return;
                        }

				        switch (psapMessage.Event)
				        {
                            case "ACK":
                            {
                                if (LogXML != null) LogXML(Data.ToString(), psapMessage.Event);

                                // TODO xml: check message ids? - eventNode["ACK"].GetAttribute("id")
                                break;
                            }
                            case "KillSocket":
                            case "PSAPappletData":
					        case "DeleteRecord":
                            {
                                if (LogXML != null) LogXML(Data.ToString(), psapMessage.Event);

                                break;
                            }
					        case "Alarm":
					        {
                                if (LogXML != null) LogXML(Data.ToString(), psapMessage.Event);

            					if (AlarmMessage != null) AlarmMessage(psapMessage.AlarmText, psapMessage.AlarmType);

						        break;
					        }
                            case "CallInfo":
                            {
                                if (LogXML != null) LogXML(Data.ToString(), psapMessage.Status);

                                if (UpdatepsapEvent != null) UpdatepsapEvent(psapMessage);
					            else                         dataModel.UpdatepsapEvent(psapMessage);

                                Console.CallState callState = (Console.CallState)Convert.ToInt32(psapMessage.CallStateCode);

                                if (callState == Console.CallState.Connected)
                                {
                                    try
                                    {
                                        if (NewActive != null) NewActive(psapMessage.PositionsActive);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logging.AppTrace("Error in New Connect Message:" + ex.Message);
                                        Logging.ExceptionLogger("Error in XMLIMServer.ProcessMessage (NewActive)", ex, Console.LogID.Gui);
                                    }
                                }

                                break;
                            }
                            case "ClearConsole":
                            case "CheckUpdate":
                            {
                                try
                                {
                                    if (LogXML != null) LogXML(Data.ToString(), psapMessage.Event);

                                    if (CheckUpdate != null) CheckUpdate();
                                }
                                catch (Exception ex)
                                {
                                    Logging.AppTrace("Error in CheckUpdate:" + ex.Message);
                                    Logging.ExceptionLogger("Error in XMLIMServer.ProcessMessage (" + psapMessage.Event + ")", ex, Console.LogID.Gui);
                                }

                                break;
                            }
                            case "ResetTransferWindow":
                            {
                                try
                                {
                                    if (LogXML != null) LogXML(Data.ToString(), psapMessage.Event);

                                    if (ResetTransfer != null)
                                    {
                                        ResetTransfer(psapMessage.Call_ID, Console.toBoolean(psapMessage.TransferSuccessful));
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logging.AppTrace("Error in ResetTransferWindow:" + ex.Message);
                                    Logging.ExceptionLogger("Error in XMLIMServer.ProcessMessage (ResetTransferWindow)", ex, Console.LogID.Gui);
                                }

                                break;
                            }
					        default:
					        {
                                if (LogXML != null) LogXML(Data.ToString(), "XML (Unknown Type: " + psapMessage.Event + ")");

						        Logging.AppTrace("Unknown message type in ProcessMessage:" + psapMessage.Event);

                                Controller.sendAlertToController(
                                    "Unknown message type in ProcessMessage:" + psapMessage.Event,
                                    Console.LogType.Warning, Console.LogID.Connection);

						        break;
					        }
				        }

                        if (MessageReceived != null) MessageReceived(new MessageInfo(psapMessage));
                    }
                }
                else
                {
                    if (LogXML != null) LogXML(Data.ToString(), "Unknown (Invalid XML)");

                    if (Data == null)
                    {
                        Data = "[NULL]";
                    }
                    else if (Data.Length > 200)
                    {
                        // report up to 150 characters
                        int dataLength = Data.Length;

                        Data = Data.Remove(150) + " ... [length:" + dataLength.ToString() + "]";
                    }

                    Data = System.Security.SecurityElement.Escape(Data);

                    Controller.sendAlertToController("Invalid message received (did not start with '<?xml') -> \"" + Data + "\"",
                        Console.LogType.Warning, Console.LogID.Connection);
                }
			}
            catch (Exception ex)
            {
                if (Data == null)
                {
                    Data = "[NULL]";
                }
                else if (Data.Length > 200)
                {
                    // report up to 150 characters
                    int dataLength = Data.Length;

                    Data = Data.Remove(150) + " ... [length:" + dataLength.ToString() + "]";
                }

                Data = System.Security.SecurityElement.Escape(Data);

                Logging.ExceptionLogger("Exception encountered while processing message -> \"" + Data + "\"",
                    ex, Console.LogID.Connection);
            }
		}

        private void dataReceived(string Data, bool fromSocket)
        {
            if (XMLDataReceived != null) XMLDataReceived(Data, fromSocket);
        }

        private void Disconnect(Client client)
		{
			//if (ServerMessage !=null) ServerMessage(CurrentDateTime() ,(int) LogLevel.Connections,"Closing connection from " + client.Socket.RemoteEndPoint.ToString());
			client.Stop();
		}

		private void MessageServer_ServerMessage(int Level, string Message)
		{
			if (ServerMessage != null) ServerMessage(CurrentDateTime(),Level, Message);
			// deal with message from server (log?)
		}

		private void MessageServer_ClientError(Client sender, Exception Ex)
		{
			if (Ex.GetType().Name == "ClientDisconnected")
			{
				if (ServerMessage !=null) ServerMessage(CurrentDateTime(),(int) LogLevel.Connections,"Client Disconnected - Cleaning up connection for " + sender.EndPoint.ToString());
				Disconnect(sender);
			}
			else
				if (ServerMessage !=null) ServerMessage (CurrentDateTime(),(int) LogLevel.Errors," - Unknown Client Error:" + Ex.GetType().ToString());
		}

        public void addServerIPAddresses(IPAddress[] ipAddresses)
        {
            foreach (IPAddress ipAddress in ipAddresses) MessageServer.addServerIPAddress(ipAddress);
        }
    } // class

} // namespace

