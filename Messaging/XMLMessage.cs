using System;
using System.Collections;
using System.Xml;
using System.IO;


namespace WestTel.E911
{
	/// <summary>
	/// Type of Incoming Event message
	/// </summary>
	public enum acEvent
	{
		RINGING,
		CONNECT,
		ALI_RESPONSE,
		DISCONNECT,
		ABANDON,
		NO_CORRELATION,
		TRANSFER,
		ALARM,
		HOLD_ON,
		HOLD_OFF,
		CONFERENCE,
		SIXTY_SEC_NO_ANS,
		RELATED_RECORDS
	}

    public class ConferenceInfo
    {
        public ConferenceInfo(string _key, string _callback, string _name, bool _connected, bool _hold)
        {
            key       = _key;
            callback  = _callback;
            name      = _name;
            connected = _connected;
            hold      = _hold;
        }

        public string key;
        public string callback;
        public string name;
        public bool   connected;
        public bool   hold;
    }

    public class StreamInfo
    {
        public StreamInfo(string _id, double _epoch, string _caller)
        {
            id     = _id;
            epoch  = _epoch;
            caller = _caller;
        }

        public string id;
        public double epoch;
        public string caller;
    }

    public class PSAPStatusInfo
    {
        public PSAPStatusInfo(string _name, uint _positionsOnline, uint _positionsOn911, uint _positionsOnAdmin)
        {
            name             = _name;
            positionsOnline  = _positionsOnline;
            positionsOn911   = _positionsOn911;
            positionsOnAdmin = _positionsOnAdmin;
        }

        public string name;
        public uint   positionsOnline;
        public uint   positionsOn911;
        public uint   positionsOnAdmin;
    }

	/// <summary>
	/// Incoming Event Message
	/// </summary>
	public struct psapEventMessage
	{
		/*
		acEvent=�RINGING�
		acTimeStamp="2003-12-15 12:13:37.504" 
		trunk="03"
		posn="00"
		telno="3030000002" 
		aniKey_id=�121337121503�
		aniTimestamp=�121339121503�
		psap_id=�Clear Creek County�
		acHostname=�linuxa�
		acIpAddr=�192.168.1.46�
		*/

        /* Version 2.0 */
		public string Event;
        public string RingDateTime;
        public string EventDateTime;
        public string TransmitDateTime;
        public string Call_ID;

        public string Encoding_ALI;
        public string ALIRecord;

        public string EncodingTDD;
        public string TDDReceived;
        public string TDDdispatcherSays;
        public string TDDCallerSays;

        public string EncodingSMS;
        public string SMSReceived;
        public string SMSdispatcherSays;
        public string SMSCallerSays;

        public string LineNumber;
        public string LineNumberStr;
        public string ConferenceNumber;
        public ArrayList ConferenceLegend;
        public string Trunk;
        public string CallOriginator;
        public string Position;
        public string PositionsActive;
        public string PositionsHistory;
        public string PositionsOnHold;
        public string Callback;
        public string CallbackDisplay;
        public string CallbackVerified;
        public string pANI;
        public string Status;
        public string StatusCode;
        public string CallStateCode;
        public string ALIStateCode;
        public string ALIButton;
        public string CADButton;
        public string EraseCADButton;
        public string TransferWindow;
        public string TDDWindow;
        public string TDDSendButton;
        public string TDDmute;
        public string SMSWindow;
        public string SMSSendButton;
        public string TakeControlButton;
        public string FlashHookButton;
        public string TandemXferButton;
        public string AttendedXferButton;
        public string ConferenceXferButton;
        public string BlindXferButton;
        public string FlashTransferButton;
        public string ANIPort;
        public string CallType;

        public string TransferTo;
        public string ListenPort;  // used for heartbeat 
        public string AlarmType;
        public string AlarmText;  
        public string MsgNumber;

        // TODO arch: replace all ArrayList uses with typed List<T>
        public ArrayList streamList;

        public ArrayList psapStatusList;

        public string TransferSuccessful;


        /* Version 1.0 
		public string ACTimeStamp;
		public string Trunk;
		public string Position;
		public string TelephoneNumber;
		public string callbackTelno;
		public string ANIKeyID;
		public string ANITimeStamp;
		public string PSAPID;
		public string ACHostName;
		public string ACHostIPAddress;
		public string AlarmText;
         
        */
       
	}

    /// <summary>
	/// Static methods for reading and writing XML Messages
	/// </summary>
	public class XMLMessage
	{
        public static string SPEC_VERSION = "6.76";

        private static string getAttribute(XmlElement e, string s)
        {
            if (e.HasAttribute(s)) return e.GetAttribute(s);

            return null;
        }

		/// <summary>
		/// Converts incoming XML text to internally used Message structure.
		/// </summary>
		/// <param name="XMLData">XML Message Source</param>
		/// <returns>Expedient.Components.Messaging.Message (structure)</returns>
		public static psapEventMessage XMLToEventMessage(XmlDocument xd)
		{
			psapEventMessage message = new psapEventMessage();

            try
			{
                XmlNode m = xd.SelectSingleNode("//Event");

                if (m != null)  // if valid message XML doc
                {
                    message.Event = m.ChildNodes[0].Name; // Named Field, not attribute;

                    XmlElement callInfo = m["CallInfo"];

                    if (callInfo != null)
                    {
                        XmlElement callData = callInfo["CallData"];

                        if (callData != null)
                        {
                            message.RingDateTime      = getAttribute(callData, "RingDateTime");
                            message.EventDateTime     = getAttribute(callData, "EventDateTime");
                            message.TransmitDateTime  = getAttribute(callData, "TransmitDateTime");

                            message.Call_ID           = getAttribute(callData, "Call-ID");
                            message.LineNumber        = getAttribute(callData, "LineNumber");
                            message.LineNumberStr     = getAttribute(callData, "LineNumberStr");
                            message.ConferenceNumber  = getAttribute(callData, "ConferenceNumber");
                            message.Trunk             = getAttribute(callData, "Trunk");
                            message.CallOriginator    = getAttribute(callData, "CallOriginator");
                            message.Position          = getAttribute(callData, "Position");
                            message.PositionsActive   = getAttribute(callData, "PositionsActive");
                            message.PositionsHistory  = getAttribute(callData, "PositionsHistory");
                            message.PositionsOnHold   = getAttribute(callData, "PositionsOnHold");

                            message.Encoding_ALI      = getAttribute(callData, "Encoding_ALI");
                            message.ALIRecord         = getAttribute(callData, "ALIRecord");

                            message.Callback          = getAttribute(callData, "Callback");
                            message.CallbackDisplay   = getAttribute(callData, "CallbackDisplay");
                            message.CallbackVerified  = getAttribute(callData, "CallbackVerified");
                            message.pANI              = getAttribute(callData, "pANI");

                            message.TDDmute           = getAttribute(callData, "TDDmute");
                            message.ANIPort           = getAttribute(callData, "ANIPort");
                            message.CallStateCode     = getAttribute(callData, "CallStateCode");
                            message.Status            = getAttribute(callData, "CallStatus");
                            message.StatusCode        = getAttribute(callData, "CallStatusCode");
                            message.CallType          = getAttribute(callData, "CallType");
                            message.ALIStateCode      = getAttribute(callData, "ALIStateCode");
                        }

                        XmlElement tddData = callInfo["TDDdata"];

                        if (tddData != null)
                        {
                            message.EncodingTDD       = getAttribute(tddData, "EncodingTDD");
                            message.TDDReceived       = getAttribute(tddData, "TDDReceived");
                            message.TDDdispatcherSays = getAttribute(tddData, "TDDdispatcherSays");
                            message.TDDCallerSays     = getAttribute(tddData, "TDDCallerSays");
                        }

                        XmlElement smsData = callInfo["SMSdata"];

                        if (smsData != null)
                        {
                            message.EncodingSMS       = getAttribute(smsData, "EncodingSMS");
                            message.SMSReceived       = getAttribute(smsData, "SMSReceived");
                            message.SMSdispatcherSays = getAttribute(smsData, "SMSdispatcherSays");
                            message.SMSCallerSays     = getAttribute(smsData, "SMSCallerSays");
                        }

                        XmlElement windowData = callInfo["WindowData"];

                        if (windowData != null)
                        {
                            message.TransferWindow       = getAttribute(windowData, "TransferWindow");
                            message.TDDWindow            = getAttribute(windowData, "TDDWindow");
                            message.TDDSendButton        = getAttribute(windowData, "TDDSendButton");
                            message.SMSWindow            = getAttribute(windowData, "SMSWindow");
                            message.SMSSendButton        = getAttribute(windowData, "SMSSendButton");
                            message.TakeControlButton    = getAttribute(windowData, "TakeControlButton");
                            message.FlashHookButton      = getAttribute(windowData, "FlashHookButton");
                            message.TandemXferButton     = getAttribute(windowData, "TandemXferButton");
                            message.AttendedXferButton   = getAttribute(windowData, "AttendedXferButton");
                            message.ConferenceXferButton = getAttribute(windowData, "ConferenceXferButton");
                            message.BlindXferButton      = getAttribute(windowData, "BlindXferButton");
                            message.FlashTransferButton  = getAttribute(windowData, "FlashTransferButton");
                            message.CADButton            = getAttribute(windowData, "CADButton");
                            message.EraseCADButton       = getAttribute(windowData, "EraseCADButton");
                            message.ALIButton            = getAttribute(windowData, "ALIButton");
                        }

                        XmlElement conferenceData = callInfo["ConferenceData"];

                        if (conferenceData != null)
                        {
                            if (message.ConferenceLegend != null) message.ConferenceLegend.Clear();

                            XmlNodeList childNodes = conferenceData.ChildNodes;

                            foreach (XmlNode node in childNodes)
                            {
                                if ( ! (node is XmlComment) && ! node.HasChildNodes)
                                {
                                    XmlAttribute legendAttr    = node.Attributes["Legend"];
                                    XmlAttribute callbackAttr  = node.Attributes["Callback"];
                                    XmlAttribute nameAttr      = node.Attributes["Name"];
                                    XmlAttribute connectedAttr = node.Attributes["Connected"];
                                    XmlAttribute holdAttr      = node.Attributes["OnHold"];

                                    string key       = (legendAttr    != null && legendAttr.    Value.Length != 0) ? legendAttr.   Value : " ";
                                    string callback  = (callbackAttr  != null && callbackAttr.  Value.Length != 0) ? callbackAttr. Value : "Unknown";
                                    string name      = (nameAttr      != null && nameAttr.      Value.Length != 0) ? nameAttr.     Value : "Unknown";
                                    bool   connected = (connectedAttr != null && connectedAttr. Value.Length != 0) ? Console.toBoolean(connectedAttr. Value) : false;
                                    bool   hold      = (holdAttr      != null && holdAttr.      Value.Length != 0) ? Console.toBoolean(holdAttr.      Value) : false;

                                    if (legendAttr == null)
                                    {
                                        Controller.sendAlertToController("Found null 'Legend' attribute in 'CallInfo.ConferenceData'",
                                            Console.LogType.Warning, Console.LogID.Connection);

                                        continue;
                                    }

                                    if (message.ConferenceLegend == null) message.ConferenceLegend = new ArrayList();

                                    // TODO re-arch: use 'connected' and 'hold' instead of PositionsHold, PositionsHistory and PositionsActive
                                    message.ConferenceLegend.Add(new ConferenceInfo(key, callback, name, connected, hold));
                                }
                            }
                        }

                        XmlElement streamData = callInfo["StreamInfo"];

                        if (streamData != null)
                        {
                            if (message.streamList != null) message.streamList.Clear();

                            XmlNodeList childNodes = streamData.ChildNodes;

                            foreach (XmlNode node in childNodes)
                            {
                                if ( ! (node is XmlComment) && ! node.HasChildNodes)
                                {
                                    XmlAttribute idAttr     = node.Attributes["ID"];
                                    XmlAttribute epochAttr  = node.Attributes["Epoch"];
                                    XmlAttribute callerAttr = node.Attributes["Caller"];

                                    if (idAttr == null)
                                    {
                                        Controller.sendAlertToController("Found null 'ID' attribute in 'CallInfo.StreamInfo'",
                                            Console.LogType.Warning, Console.LogID.Connection);

                                        continue;
                                    }

                                    string id       = (idAttr     != null && idAttr.     Value.Length != 0) ? idAttr.     Value : "Unknown";
                                    string epochStr = (epochAttr  != null && epochAttr.  Value.Length != 0) ? epochAttr.  Value : "0.0";
                                    string caller   = (callerAttr != null && callerAttr. Value.Length != 0) ? callerAttr. Value : "Unknown";

                                    double epoch = 0.0;
                                    try {epoch = Convert.ToDouble(epochStr);} catch {}

                                    if (message.streamList == null) message.streamList = new ArrayList();

                                    message.streamList.Add(new StreamInfo(id, epoch, caller));
                                }
                            }
                        }
                    }
                    else if (m["ClearConsole"] != null)
                    {
                        message.Position = getAttribute(m["ClearConsole"], "Position");
                    }
                    else if (m["PSAPappletData"] != null)
                    {
                        if (m["PSAPappletData"] != null)
                        {
                            XmlNodeList childNodes = m["PSAPappletData"].ChildNodes;

                            foreach (XmlNode node in childNodes)
                            {
                                if ( ! (node is XmlComment) && ! node.HasChildNodes)
                                {
                                    XmlAttribute nameAttr             = node.Attributes["Name"];
                                    XmlAttribute positionsOnlineAttr  = node.Attributes["PositionsOnline"];
                                    XmlAttribute positionsOn911Attr   = node.Attributes["PositionsOn911"];
                                    XmlAttribute positionsOnAdminAttr = node.Attributes["PositionsOnAdmin"];

                                    string name                = (nameAttr             != null && nameAttr.             Value.Length != 0) ? nameAttr.             Value : "[unnamed]";
                                    string positionsOnlineStr  = (positionsOnlineAttr  != null && positionsOnlineAttr.  Value.Length != 0) ? positionsOnlineAttr.  Value : null;
                                    string positionsOn911Str   = (positionsOn911Attr   != null && positionsOn911Attr.   Value.Length != 0) ? positionsOn911Attr.   Value : null;
                                    string positionsOnAdminStr = (positionsOnAdminAttr != null && positionsOnAdminAttr. Value.Length != 0) ? positionsOnAdminAttr. Value : null;

                                    if (nameAttr == null)
                                    {
                                        Controller.sendAlertToController("Found null 'Name' attribute in 'PSAPappletData'",
                                            Console.LogType.Warning, Console.LogID.Connection);

                                        continue;
                                    }

                                    uint positionsOnline  = 0;
                                    uint positionsOn911   = 0;
                                    uint positionsOnAdmin = 0;

                                    try {positionsOnline  = Convert.ToUInt32(positionsOnlineStr);}  catch {}
                                    try {positionsOn911   = Convert.ToUInt32(positionsOn911Str);}   catch {}
                                    try {positionsOnAdmin = Convert.ToUInt32(positionsOnAdminStr);} catch {}

                                    if (message.psapStatusList == null) message.psapStatusList = new ArrayList();

                                    message.psapStatusList.Add(new PSAPStatusInfo(name, positionsOnline, positionsOn911, positionsOnAdmin));
                                }
                            }
                        }
                    }
                    else if (m["Alarm"] != null)
                    {
                        message.AlarmType = getAttribute(m["Alarm"], "Type");
                        message.AlarmText = getAttribute(m["Alarm"], "Text");
                    }
                    else if (m["DeleteRecord"] != null)
                    {
                        message.Call_ID = getAttribute(m["DeleteRecord"], "Call-ID");
                    }
                    else if (m["ResetTransferWindow"] != null)
                    {
                        message.Call_ID            = getAttribute(m["ResetTransferWindow"], "Call-ID");
                        message.TransferSuccessful = getAttribute(m["ResetTransferWindow"], "TransferSuccessful");
                    }

                    // Logging.AppTrace ("Message Event= " + message.acEvent);
                }

                return message;
				
			}
			catch (Exception ex)
			{
				Logging.AppTrace ("Error in XMLToMessage:" + ex.Message);
				Logging.ExceptionLogger("Error in Messaging.XMLMessage.XMLMessage", ex, Console.LogID.Gui);
			}

            return new psapEventMessage();
		}

		public static void LogData(string Data)
		{
			string path = @"c:\WestTel\XMLLog.XML";
			if (!File.Exists(path)) 
			{
				// Create a file to write to.
				using (StreamWriter sw = File.CreateText(path)) 
				{
					sw.Write("<Events>");
				}
			}
			using (StreamWriter sw = File.AppendText(path)) 
			{
				sw.Write(Data);
			}
		}

		private static string GetNamedAttribute(XmlNode node, string Name)
		{
			string ReturnValue = null;
			try
			{
				if (node.Attributes.GetNamedItem(Name) != null) 
					ReturnValue = node.Attributes.GetNamedItem(Name).Value;
			}
			catch (Exception ex)
			{
				Logging.AppTrace ("Exception in GetNamedAttribute: " + ex.Message );
				Logging.ExceptionLogger("Error in XMLMessage.GetNamedAttribute", ex, Console.LogID.Gui);
			}

			return ReturnValue;
		}
	}
}
